.class public final Lcom/facebook/notifications/preferences/InlineNotificationNuxResetServerPreference$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/DqY;


# direct methods
.method public constructor <init>(LX/DqY;)V
    .locals 0

    .prologue
    .line 2048415
    iput-object p1, p0, Lcom/facebook/notifications/preferences/InlineNotificationNuxResetServerPreference$1;->a:LX/DqY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 10

    .prologue
    .line 2048406
    iget-object v0, p0, Lcom/facebook/notifications/preferences/InlineNotificationNuxResetServerPreference$1;->a:LX/DqY;

    .line 2048407
    iget-object v1, v0, LX/DqY;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->K:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->J:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2048408
    iget-object v1, v0, LX/DqY;->c:LX/0kL;

    new-instance v2, LX/27k;

    const-string v3, "Starting status fetch from server."

    invoke-direct {v2, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2048409
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2048410
    new-instance v4, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    const-string v5, "4127"

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 2048411
    const-string v5, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2048412
    iget-object v4, v0, LX/DqY;->a:LX/0aG;

    const-string v5, "interstitials_fetch_and_update"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, LX/DqY;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x949eab2

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2048413
    new-instance v5, LX/DqX;

    invoke-direct {v5, v0}, LX/DqX;-><init>(LX/DqY;)V

    iget-object v6, v0, LX/DqY;->d:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2048414
    const/4 v0, 0x1

    return v0
.end method
