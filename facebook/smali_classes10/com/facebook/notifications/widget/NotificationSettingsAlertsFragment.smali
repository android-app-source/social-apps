.class public Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/fig/listitem/FigListItem;

.field public d:Lcom/facebook/fig/listitem/FigListItem;

.field public e:Lcom/facebook/fig/listitem/FigListItem;

.field public f:Lcom/facebook/fig/listitem/FigListItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2050185
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static l(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V
    .locals 2

    .prologue
    .line 2050178
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->d:Lcom/facebook/fig/listitem/FigListItem;

    if-eqz v0, :cond_0

    .line 2050179
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->m(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Landroid/net/Uri;

    move-result-object v0

    .line 2050180
    invoke-static {v0}, Landroid/media/RingtoneManager;->isDefault(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2050181
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081174

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2050182
    :goto_0
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->d:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2050183
    :cond_0
    return-void

    .line 2050184
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2050176
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->p:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2050177
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z
    .locals 3

    .prologue
    .line 2050175
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->o:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static o(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z
    .locals 3

    .prologue
    .line 2050186
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->m:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static p(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z
    .locals 3

    .prologue
    .line 2050174
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->n:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2050171
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2050172
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p1

    check-cast p1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object p1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2050173
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2050164
    const/16 v0, 0x698

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2050165
    const-string v0, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2050166
    if-nez v0, :cond_1

    const/4 p1, 0x0

    .line 2050167
    :goto_0
    iget-object p2, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p2

    sget-object p3, LX/0hM;->p:LX/0Tn;

    invoke-interface {p2, p3, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object p1

    invoke-interface {p1}, LX/0hN;->commit()V

    .line 2050168
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->l(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    .line 2050169
    :cond_0
    return-void

    .line 2050170
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5dc62bc9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2050139
    const v0, 0x7f030c22

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2050140
    const v1, 0x7f0d1dde

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->d:Lcom/facebook/fig/listitem/FigListItem;

    .line 2050141
    const v1, 0x7f0d1ddf

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->e:Lcom/facebook/fig/listitem/FigListItem;

    .line 2050142
    const v1, 0x7f0d1de0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->f:Lcom/facebook/fig/listitem/FigListItem;

    .line 2050143
    const v1, 0x7f0d1de1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->c:Lcom/facebook/fig/listitem/FigListItem;

    .line 2050144
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->l(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    .line 2050145
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->d:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v3, LX/DsA;

    invoke-direct {v3, p0}, LX/DsA;-><init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050146
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->n(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2050147
    new-instance v1, LX/DsB;

    invoke-direct {v1, p0}, LX/DsB;-><init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    .line 2050148
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050149
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050150
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->f:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->o(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2050151
    new-instance v1, LX/DsC;

    invoke-direct {v1, p0}, LX/DsC;-><init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    .line 2050152
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->f:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050153
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->f:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050154
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->p(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2050155
    new-instance v1, LX/DsD;

    invoke-direct {v1, p0}, LX/DsD;-><init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V

    .line 2050156
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050157
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2050158
    const/16 v1, 0x2b

    const v3, -0x6bd4bd9d

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6be52e51

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2050159
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2050160
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2050161
    if-eqz v0, :cond_0

    .line 2050162
    const v2, 0x7f081172

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2050163
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x30bbc53a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
