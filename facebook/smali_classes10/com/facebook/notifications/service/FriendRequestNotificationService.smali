.class public Lcom/facebook/notifications/service/FriendRequestNotificationService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:LX/2c4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/17Y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/Dry;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/Drl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2048587
    const-string v0, "FriendRequestNotificationService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2048588
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2048589
    iput-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->j:LX/0Ot;

    .line 2048590
    return-void
.end method

.method private static a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 2048591
    const/high16 v0, 0x8000000

    invoke-static {p0, p1, p2, v0}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 3
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048592
    sget-object v0, LX/Dqm;->CONFIRM_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048593
    const-string v1, "FRIENDING_ACTION"

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048594
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;
    .locals 2
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048595
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/notifications/service/FriendRequestNotificationService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2048596
    const-string v1, "NOTIFICATION"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2048597
    const-string v1, "INTENT_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048598
    const-string v1, "NOTICATIONID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2048599
    if-eqz p4, :cond_0

    .line 2048600
    const-string v1, "EXTRA_PUSH_NOTIFICATION_LOG_OBJECT"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2048601
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 2048616
    new-instance v0, Lcom/facebook/notifications/service/FriendRequestNotificationService$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/notifications/service/FriendRequestNotificationService$1;-><init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(JLjava/lang/String;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2048602
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2048603
    iget-object v1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->d:LX/17Y;

    invoke-interface {v1, p0, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2048604
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2048605
    iget-object v1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2048606
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2048607
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a:LX/2c4;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2048608
    return-void
.end method

.method private static a(Lcom/facebook/notifications/service/FriendRequestNotificationService;LX/2c4;LX/2do;Ljava/util/concurrent/ExecutorService;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Landroid/content/Context;LX/Dry;LX/Drl;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/service/FriendRequestNotificationService;",
            "LX/2c4;",
            "LX/2do;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Landroid/content/Context;",
            "LX/Dry;",
            "LX/Drl;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2048609
    iput-object p1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a:LX/2c4;

    iput-object p2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b:LX/2do;

    iput-object p3, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->d:LX/17Y;

    iput-object p5, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->f:LX/0Sh;

    iput-object p7, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    iput-object p8, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->h:LX/Dry;

    iput-object p9, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->i:LX/Drl;

    iput-object p10, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->j:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;

    invoke-static {v10}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v1

    check-cast v1, LX/2c4;

    invoke-static {v10}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v2

    check-cast v2, LX/2do;

    invoke-static {v10}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    const-class v7, Landroid/content/Context;

    invoke-interface {v10, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v10}, LX/Dry;->a(LX/0QB;)LX/Dry;

    move-result-object v8

    check-cast v8, LX/Dry;

    invoke-static {v10}, LX/Drl;->b(LX/0QB;)LX/Drl;

    move-result-object v9

    check-cast v9, LX/Drl;

    const/16 v11, 0xa71

    invoke-static {v10, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/service/FriendRequestNotificationService;LX/2c4;LX/2do;Ljava/util/concurrent/ExecutorService;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Landroid/content/Context;LX/Dry;LX/Drl;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2048610
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->d:LX/17Y;

    sget-object v1, LX/0ax;->cW:Ljava/lang/String;

    sget-object v2, LX/5Oz;->FRIEND_REQUEST_TRAY_NOTIFICATION:LX/5Oz;

    sget-object v3, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v3}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2048611
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2048612
    iget-object v1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2048613
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2048614
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a:LX/2c4;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2048615
    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/Runnable;)V
    .locals 10
    .param p4    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 2048579
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->f:LX/0Sh;

    new-instance v1, Lcom/facebook/notifications/service/FriendRequestNotificationService$4;

    invoke-direct {v1, p0, p2, p3}, Lcom/facebook/notifications/service/FriendRequestNotificationService$4;-><init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;J)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2048580
    new-instance v1, LX/Dqk;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/Dqk;-><init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;Ljava/lang/String;JLjava/lang/Runnable;)V

    .line 2048581
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2dj;

    sget-object v6, LX/2h8;->PYMK_PUSH_NOTIF:LX/2h8;

    move-wide v4, p2

    move-object v8, v7

    invoke-virtual/range {v3 .. v8}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2048582
    return-void
.end method

.method private a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V
    .locals 5
    .param p4    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048583
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->f:LX/0Sh;

    new-instance v1, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;

    invoke-direct {v1, p0, p2, p3}, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;-><init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;LX/2na;Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2048584
    new-instance v1, LX/Dqj;

    invoke-direct {v1, p0, p1, p3, p4}, LX/Dqj;-><init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 2048585
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, LX/2hA;->FRIEND_REQUEST_PUSH_NOTIFICATION:LX/2hA;

    invoke-virtual {v0, v2, v3, p2, v4}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2048586
    return-void
.end method

.method public static b(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 3
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048521
    sget-object v0, LX/Dqm;->REJECT_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048522
    const-string v1, "FRIENDING_ACTION"

    sget-object v2, LX/2na;->REJECT:LX/2na;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048523
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2048524
    const-string v0, "NOTIFICATION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/SystemTrayNotification;

    .line 2048525
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2048526
    const-string v1, "NOTICATIONID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2048527
    const-string v1, "INTENT_TYPE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/Dqm;

    .line 2048528
    sget-object v3, LX/Dql;->a:[I

    invoke-virtual {v1}, LX/Dqm;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 2048529
    :goto_0
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->c(Landroid/content/Intent;)V

    .line 2048530
    return-void

    .line 2048531
    :pswitch_1
    const-string v1, "FRIENDING_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/2na;

    iget-object v3, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f8c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2048532
    :pswitch_2
    const-string v1, "FRIENDING_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/2na;

    iget-object v3, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f88

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2048533
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2048534
    :pswitch_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 2048535
    :pswitch_5
    const-string v1, "FRIENDING_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/2na;

    invoke-direct {p0, v2, v1, v0, v4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 2048536
    invoke-direct {p0, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2048537
    :pswitch_6
    const-string v1, "FRIENDING_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/2na;

    invoke-direct {p0, v2, v1, v0, v4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 2048538
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 2048539
    :pswitch_7
    const-string v1, "FRIENDING_ACTION"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/2na;

    invoke-direct {p0, v2, v1, v0, v4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;LX/2na;Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 2048540
    invoke-direct {p0, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2048541
    :pswitch_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v3, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f85

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/String;JLjava/lang/Runnable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static c(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 1
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048542
    sget-object v0, LX/Dqm;->SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048543
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2048544
    const-string v0, "EXTRA_PUSH_NOTIFICATION_LOG_OBJECT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2048545
    :goto_0
    return-void

    .line 2048546
    :cond_0
    const-string v0, "EXTRA_PUSH_NOTIFICATION_LOG_OBJECT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    .line 2048547
    iget-object v1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->h:LX/Dry;

    .line 2048548
    iget-object v2, v1, LX/Dry;->a:LX/0Zb;

    const-string v3, "push_action_clicked"

    .line 2048549
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "rich_push_notifications"

    .line 2048550
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2048551
    move-object p0, p0

    .line 2048552
    move-object v3, p0

    .line 2048553
    const-string p0, "action_type"

    .line 2048554
    iget-object p1, v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->a:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-object p1, p1

    .line 2048555
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "notification_id"

    .line 2048556
    iget-object p1, v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->c:Ljava/lang/String;

    move-object p1, p1

    .line 2048557
    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "action_position"

    .line 2048558
    iget p1, v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->b:I

    move p1, p1

    .line 2048559
    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "notification_type"

    .line 2048560
    iget-object p1, v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->d:Ljava/lang/String;

    move-object p1, p1

    .line 2048561
    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2048562
    goto :goto_0
.end method

.method public static d(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 3
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048563
    sget-object v0, LX/Dqm;->REJECT_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048564
    const-string v1, "FRIENDING_ACTION"

    sget-object v2, LX/2na;->REJECT:LX/2na;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048565
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 3
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048566
    sget-object v0, LX/Dqm;->CONFIRM_AND_SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048567
    const-string v1, "FRIENDING_ACTION"

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048568
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 3
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048569
    sget-object v0, LX/Dqm;->CONFIRM_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048570
    const-string v1, "FRIENDING_ACTION"

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2048571
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;
    .locals 1
    .param p4    # Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048572
    sget-object v0, LX/Dqm;->SEND_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    invoke-static {p0, p1, p2, v0, p4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;LX/Dqm;Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/content/Intent;

    move-result-object v0

    .line 2048573
    invoke-static {p1, p3, v0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x1a789e69

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048574
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b(Landroid/content/Intent;)V

    .line 2048575
    const/16 v1, 0x25

    const v2, -0x202afaf0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x1dd884fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048576
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2048577
    invoke-static {p0, p0}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2048578
    const/16 v1, 0x25

    const v2, 0x4aebb8d4    # 7724138.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
