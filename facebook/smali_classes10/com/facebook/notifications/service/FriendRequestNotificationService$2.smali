.class public final Lcom/facebook/notifications/service/FriendRequestNotificationService$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2na;

.field public final synthetic b:Ljava/lang/Long;

.field public final synthetic c:Lcom/facebook/notifications/service/FriendRequestNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;LX/2na;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2048476
    iput-object p1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->c:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iput-object p2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->a:LX/2na;

    iput-object p3, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->b:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2048477
    sget-object v0, LX/Dql;->b:[I

    iget-object v1, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->a:LX/2na;

    invoke-virtual {v1}, LX/2na;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2048478
    :goto_0
    return-void

    .line 2048479
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->c:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2048480
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->c:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, Lcom/facebook/notifications/service/FriendRequestNotificationService$2;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
