.class public Lcom/facebook/notifications/service/SystemTrayLogService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:LX/Dqo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2048658
    const-string v0, "SystemTrayLogService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2048659
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/notifications/service/SystemTrayLogService;

    invoke-static {v0}, LX/Dqo;->b(LX/0QB;)LX/Dqo;

    move-result-object v0

    check-cast v0, LX/Dqo;

    iput-object v0, p0, Lcom/facebook/notifications/service/SystemTrayLogService;->a:LX/Dqo;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x7843bb13

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048663
    if-eqz p1, :cond_0

    .line 2048664
    iget-object v1, p0, Lcom/facebook/notifications/service/SystemTrayLogService;->a:LX/Dqo;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, LX/Dqo;->a(Landroid/os/Bundle;Landroid/content/Context;)V

    .line 2048665
    :cond_0
    const/16 v1, 0x25

    const v2, 0x775b12ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x75f38df2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048660
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2048661
    invoke-static {p0, p0}, Lcom/facebook/notifications/service/SystemTrayLogService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2048662
    const/16 v1, 0x25

    const v2, -0x1f5e36f5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
