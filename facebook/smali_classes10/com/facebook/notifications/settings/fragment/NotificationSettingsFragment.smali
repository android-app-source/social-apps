.class public Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Dr8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DrJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Dqp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/33Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/33a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1Qq;

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0g8;

.field public o:LX/Dr9;

.field private final p:LX/CS9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2049182
    const-class v0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    sput-object v0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2049282
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2049283
    new-instance v0, LX/DrL;

    invoke-direct {v0, p0}, LX/DrL;-><init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->p:LX/CS9;

    return-void
.end method

.method public static d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V
    .locals 7

    .prologue
    .line 2049261
    invoke-direct {p0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049262
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->a:LX/Dr8;

    .line 2049263
    new-instance v1, LX/BCK;

    invoke-direct {v1}, LX/BCK;-><init>()V

    move-object v1, v1

    .line 2049264
    const-string v2, "notif_option_set_context"

    invoke-static {}, LX/1rw;->b()LX/3T1;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2049265
    const-string v2, "image_height"

    iget-object v3, v0, LX/Dr8;->b:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2049266
    const-string v2, "image_width"

    iget-object v3, v0, LX/Dr8;->b:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2049267
    const-string v2, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2049268
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2049269
    iget-object v2, v0, LX/Dr8;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2049270
    iget-object v2, v0, LX/Dr8;->c:LX/1Ck;

    const-string v3, "NOTIFICATION_SETTINGS_LOAD"

    new-instance v4, LX/Dr7;

    invoke-direct {v4, p0}, LX/Dr7;-><init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2049271
    :goto_0
    return-void

    .line 2049272
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->a:LX/Dr8;

    iget-object v1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->m:LX/0Px;

    .line 2049273
    new-instance v2, LX/BCJ;

    invoke-direct {v2}, LX/BCJ;-><init>()V

    move-object v2, v2

    .line 2049274
    const-string v3, "image_height"

    iget-object v4, v0, LX/Dr8;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2049275
    const-string v3, "image_width"

    iget-object v4, v0, LX/Dr8;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2049276
    const-string v3, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2049277
    const-string v3, "ids"

    invoke-virtual {v2, v3, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2049278
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2049279
    iget-object v3, v0, LX/Dr8;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    invoke-static {v2}, LX/0tX;->d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2049280
    iget-object v3, v0, LX/Dr8;->c:LX/1Ck;

    const-string v4, "NOTIFICATION_SETTINGS_LOAD"

    new-instance v5, LX/Dr6;

    invoke-direct {v5, v1, p0}, LX/Dr6;-><init>(Ljava/util/List;Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2049281
    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 2049260
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->m:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2049257
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-static {v0}, LX/Dr8;->a(LX/0QB;)LX/Dr8;

    move-result-object v3

    check-cast v3, LX/Dr8;

    const-class v4, LX/DrJ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DrJ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    new-instance v7, LX/Dqp;

    const/16 v6, 0xafd

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v7, v8, v6}, LX/Dqp;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    move-object v6, v7

    check-cast v6, LX/Dqp;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v7

    check-cast v7, LX/1DS;

    invoke-static {v0}, LX/33Z;->a(LX/0QB;)LX/33Z;

    move-result-object v8

    check-cast v8, LX/33Z;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v9

    check-cast v9, LX/1Db;

    const/16 v10, 0x2b02

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/33a;->b(LX/0QB;)LX/33a;

    move-result-object v11

    check-cast v11, LX/33a;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v3, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->a:LX/Dr8;

    iput-object v4, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->b:LX/DrJ;

    iput-object v5, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d:LX/Dqp;

    iput-object v7, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->e:LX/1DS;

    iput-object v8, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->f:LX/33Z;

    iput-object v9, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->g:LX/1Db;

    iput-object v10, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->h:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->i:LX/33a;

    iput-object v0, v2, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->j:Ljava/util/concurrent/Executor;

    .line 2049258
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2049259
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/BCR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2049249
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->o:LX/Dr9;

    .line 2049250
    iget-object v1, v0, LX/Dr9;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2049251
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2049252
    iget-object v2, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->o:LX/Dr9;

    new-instance v3, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCR;

    invoke-direct {v3, v0, v1}, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;-><init>(LX/BCR;I)V

    .line 2049253
    iget-object v0, v2, LX/Dr9;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2049254
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2049255
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->l:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2049256
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x410d50de

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049224
    const v0, 0x7f030c26

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2049225
    const v0, 0x7f0d1de3

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2049226
    new-instance v3, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2049227
    new-instance v3, LX/Dr9;

    invoke-direct {v3}, LX/Dr9;-><init>()V

    iput-object v3, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->o:LX/Dr9;

    .line 2049228
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    invoke-static {v4}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v4

    new-instance p1, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment$3;

    invoke-direct {p1, p0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment$3;-><init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    .line 2049229
    new-instance p2, LX/DrI;

    invoke-direct {p2, v3, v4, p1}, LX/DrI;-><init>(Landroid/content/Context;LX/1PY;Ljava/lang/Runnable;)V

    .line 2049230
    move-object v3, p2

    .line 2049231
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1Qj;->m_(Z)V

    .line 2049232
    iget-object v4, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->e:LX/1DS;

    iget-object p1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->h:LX/0Ot;

    iget-object p2, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->o:LX/Dr9;

    invoke-virtual {v4, p1, p2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v4

    .line 2049233
    iput-object v3, v4, LX/1Ql;->f:LX/1PW;

    .line 2049234
    move-object v3, v4

    .line 2049235
    invoke-virtual {v3}, LX/1Ql;->e()LX/1Qq;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->l:LX/1Qq;

    .line 2049236
    new-instance v3, LX/0g7;

    invoke-direct {v3, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v3, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    .line 2049237
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    invoke-interface {v0, v5}, LX/0g8;->b(Z)V

    .line 2049238
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->f(Landroid/view/View;)V

    .line 2049239
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    iget-object v3, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->l:LX/1Qq;

    invoke-interface {v0, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2049240
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->n:LX/0g8;

    iget-object v3, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->g:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->a(LX/1St;)V

    .line 2049241
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2049242
    const-string v3, "extra_option_row_set_ids"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2049243
    const-string v3, "extra_option_row_set_ids"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->m:LX/0Px;

    .line 2049244
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2049245
    iget-object v0, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->i:LX/33a;

    sget-object v3, LX/8D0;->WEBVIEW:LX/8D0;

    .line 2049246
    const-string v4, "native_settings_launched"

    invoke-static {v4, v3}, LX/33a;->a(Ljava/lang/String;LX/8D0;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2049247
    iget-object v5, v0, LX/33a;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2049248
    :cond_1
    const/16 v0, 0x2b

    const v3, 0x13ab4e55

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6742ee99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2049221
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2049222
    iget-object v1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->f:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->p:LX/CS9;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2049223
    const/16 v1, 0x2b

    const v2, 0x8e827e8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3043e22e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2049218
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2049219
    iget-object v1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->f:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->p:LX/CS9;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2049220
    const/16 v1, 0x2b

    const v2, -0x17521728

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x123bc59b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2049183
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2049184
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "fragment_title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2049185
    const-class v2, LX/1ZF;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2049186
    if-eqz v2, :cond_1

    .line 2049187
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081171

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-interface {v2, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2049188
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d:LX/Dqp;

    const/4 v2, 0x0

    .line 2049189
    iget-object v4, v1, LX/Dqp;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0hM;->L:LX/0Tn;

    invoke-interface {v4, v5, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    move v1, v2

    .line 2049190
    if-eqz v1, :cond_6

    .line 2049191
    iget-object v1, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d:LX/Dqp;

    const/4 v5, 0x0

    .line 2049192
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v2, "AndroidLegacyPushSettingsServerActionToken;"

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2049193
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2049194
    sget-object v2, LX/Dqp;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    :goto_0
    if-ge v4, v8, :cond_4

    sget-object v2, LX/Dqp;->a:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2049195
    const/4 v10, 0x1

    .line 2049196
    iget-object v11, v1, LX/Dqp;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/0hM;->a:LX/0Tn;

    invoke-virtual {v9, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v9

    check-cast v9, LX/0Tn;

    invoke-interface {v11, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v9

    if-nez v9, :cond_8

    move v9, v10

    :goto_1
    move v9, v9

    .line 2049197
    if-eqz v9, :cond_3

    .line 2049198
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2049199
    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2049200
    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2049201
    invoke-virtual {v1}, LX/Dqp;->c()V

    .line 2049202
    const/4 v2, 0x0

    .line 2049203
    :goto_2
    move-object v1, v2

    .line 2049204
    if-nez v1, :cond_5

    .line 2049205
    invoke-static {p0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    .line 2049206
    :goto_3
    const/16 v1, 0x2b

    const v2, -0x3d83789c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2049207
    :cond_5
    new-instance v2, LX/DrM;

    invoke-direct {v2, p0}, LX/DrM;-><init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    iget-object v4, p0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_3

    .line 2049208
    :cond_6
    invoke-static {p0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    goto :goto_3

    .line 2049209
    :cond_7
    const-string v2, "|"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v7, v4, v5

    invoke-static {v2, v4}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2049210
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2049211
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 2049212
    new-instance v4, LX/4HO;

    invoke-direct {v4}, LX/4HO;-><init>()V

    invoke-virtual {v4, v2}, LX/4HO;->c(Ljava/lang/String;)LX/4HO;

    move-result-object v4

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4HO;->a(Ljava/lang/String;)LX/4HO;

    move-result-object v4

    .line 2049213
    invoke-static {}, LX/BC3;->a()LX/BC2;

    move-result-object v5

    .line 2049214
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2049215
    iget-object v4, v1, LX/Dqp;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2049216
    iget-object v4, v1, LX/Dqp;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2049217
    goto :goto_2

    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_1
.end method
