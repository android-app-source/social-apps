.class public final Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/DrB;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DrB;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2049018
    iput-object p1, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->a:LX/DrB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049019
    iput-object p2, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->b:Ljava/util/List;

    .line 2049020
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2049021
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->a:LX/DrB;

    iget-object v0, v0, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 2049022
    sget-object v0, LX/DrH;->a:LX/0Tn;

    invoke-interface {v2, v0}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    .line 2049023
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->b:Ljava/util/List;

    .line 2049024
    invoke-static {}, LX/DrH;->a()LX/0Tn;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, v1, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049025
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2049026
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    .line 2049027
    invoke-static {v1}, LX/DrH;->a(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2049028
    invoke-static {v1}, LX/DrH;->b(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2049029
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2049030
    invoke-static {v1}, LX/DrH;->c(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2049031
    invoke-static {v1}, LX/DrH;->d(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;->k()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049032
    invoke-static {v1}, LX/DrH;->e(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;->a()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049033
    :cond_0
    invoke-static {v1}, LX/DrH;->f(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2049034
    invoke-static {v1}, LX/DrH;->g(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->b()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049035
    invoke-static {v1}, LX/DrH;->h(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->c()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049036
    invoke-static {v1}, LX/DrH;->i(I)LX/0Tn;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->d()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2049037
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2049038
    :cond_1
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2049039
    return-void
.end method
