.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Dqr;


# direct methods
.method public constructor <init>(LX/Dqr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049348
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2049349
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->a:LX/Dqr;

    .line 2049350
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;
    .locals 4

    .prologue
    .line 2049351
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;

    monitor-enter v1

    .line 2049352
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2049353
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2049354
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049355
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2049356
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;

    invoke-static {v0}, LX/Dqr;->a(LX/0QB;)LX/Dqr;

    move-result-object v3

    check-cast v3, LX/Dqr;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;-><init>(LX/Dqr;)V

    .line 2049357
    move-object v0, p0

    .line 2049358
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2049359
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049360
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2049361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2049333
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    check-cast p3, LX/1Pr;

    .line 2049334
    new-instance v0, LX/Drg;

    .line 2049335
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 2049336
    invoke-interface {v1}, LX/BCR;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Drg;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drh;

    .line 2049337
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 2049338
    invoke-interface {v1}, LX/BCR;->c()LX/BCP;

    move-result-object v1

    invoke-interface {v1}, LX/BCP;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BCO;

    .line 2049339
    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2049340
    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->e()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v5

    .line 2049341
    iget-object v6, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->a:LX/Dqr;

    invoke-virtual {v6, v5}, LX/Dqr;->b(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v6

    .line 2049342
    iget-object v7, v0, LX/Drh;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    move v6, v7

    .line 2049343
    if-nez v6, :cond_0

    .line 2049344
    new-instance v6, LX/DrP;

    new-instance v7, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    invoke-direct {v7, v1}, Lcom/facebook/notifications/settings/data/NotifOptionNode;-><init>(LX/BCO;)V

    invoke-direct {v6, v7, v0}, LX/DrP;-><init>(Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/Drh;)V

    .line 2049345
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->a:LX/Dqr;

    invoke-virtual {v1, v5}, LX/Dqr;->a(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v1

    invoke-virtual {p1, v1, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049346
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2049347
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2049328
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049329
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049330
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2049331
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049332
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
