.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Dqr;


# direct methods
.method public constructor <init>(LX/Dqr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049284
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2049285
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->a:LX/Dqr;

    .line 2049286
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;
    .locals 4

    .prologue
    .line 2049287
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;

    monitor-enter v1

    .line 2049288
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2049289
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2049290
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049291
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2049292
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;

    invoke-static {v0}, LX/Dqr;->a(LX/0QB;)LX/Dqr;

    move-result-object v3

    check-cast v3, LX/Dqr;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;-><init>(LX/Dqr;)V

    .line 2049293
    move-object v0, p0

    .line 2049294
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2049295
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049296
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2049297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2049298
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049299
    const/4 v0, 0x0

    move v1, v0

    .line 2049300
    :goto_0
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049301
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2049302
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049303
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCO;

    .line 2049304
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->e()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v2

    .line 2049305
    iget-object v3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->a:LX/Dqr;

    invoke-virtual {v3, v2}, LX/Dqr;->b(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2049306
    new-instance v3, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    invoke-direct {v3, v0}, Lcom/facebook/notifications/settings/data/NotifOptionNode;-><init>(LX/BCO;)V

    .line 2049307
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->a:LX/Dqr;

    invoke-virtual {v0, v2}, LX/Dqr;->a(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049308
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2049309
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2049310
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049311
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049312
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2049313
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049314
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
