.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049402
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2049403
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    .line 2049404
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;
    .locals 4

    .prologue
    .line 2049405
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;

    monitor-enter v1

    .line 2049406
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2049407
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2049408
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049409
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2049410
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;)V

    .line 2049411
    move-object v0, p0

    .line 2049412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2049413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2049415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z
    .locals 2

    .prologue
    .line 2049416
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049417
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2049418
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    check-cast p3, LX/1Pr;

    .line 2049419
    invoke-static {p2}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->b(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2049420
    new-instance v0, LX/Dri;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Dri;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drj;

    .line 2049421
    iget-boolean v1, v0, LX/Drj;->b:Z

    move v1, v1

    .line 2049422
    if-nez v1, :cond_0

    .line 2049423
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 2049424
    invoke-interface {v1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2049425
    iput-object v1, v0, LX/Drj;->a:Ljava/lang/String;

    .line 2049426
    const/4 v1, 0x1

    .line 2049427
    iput-boolean v1, v0, LX/Drj;->b:Z

    .line 2049428
    :cond_0
    move-object v0, v0

    .line 2049429
    move-object v1, v0

    .line 2049430
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    .line 2049431
    :goto_1
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049432
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2049433
    new-instance v3, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049434
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049435
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCO;

    invoke-direct {v3, v0}, Lcom/facebook/notifications/settings/data/NotifOptionNode;-><init>(LX/BCO;)V

    .line 2049436
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    new-instance v4, LX/DrQ;

    invoke-static {p2}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->b(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z

    move-result v5

    invoke-direct {v4, v3, v5, v1}, LX/DrQ;-><init>(Lcom/facebook/notifications/settings/data/NotifOptionNode;ZLX/Dre;)V

    invoke-virtual {p1, v0, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049437
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2049438
    :cond_1
    const/4 v2, 0x1

    .line 2049439
    new-instance v0, LX/Drd;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Drd;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drf;

    .line 2049440
    iget-boolean v1, v0, LX/Drf;->b:Z

    move v1, v1

    .line 2049441
    if-nez v1, :cond_2

    .line 2049442
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 2049443
    invoke-interface {v1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/Drf;->a(Ljava/lang/String;Z)V

    .line 2049444
    iput-boolean v2, v0, LX/Drf;->b:Z

    .line 2049445
    :cond_2
    move-object v0, v0

    .line 2049446
    move-object v1, v0

    goto :goto_0

    .line 2049447
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2049448
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049449
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049450
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2049451
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049452
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2049453
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049454
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
