.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

.field private final b:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

.field private final c:LX/Dqq;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;LX/Dqq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049392
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2049393
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    .line 2049394
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->b:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    .line 2049395
    iput-object p3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->c:LX/Dqq;

    .line 2049396
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;
    .locals 6

    .prologue
    .line 2049362
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;

    monitor-enter v1

    .line 2049363
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2049364
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2049365
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049366
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2049367
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    invoke-static {v0}, LX/Dqq;->a(LX/0QB;)LX/Dqq;

    move-result-object v5

    check-cast v5, LX/Dqq;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;LX/Dqq;)V

    .line 2049368
    move-object v0, p0

    .line 2049369
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2049370
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049371
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2049372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2049373
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    const/4 v2, 0x0

    .line 2049374
    invoke-static {p2}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2049375
    iget v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->b:I

    move v0, v0

    .line 2049376
    if-eqz v0, :cond_1

    .line 2049377
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->b:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049378
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->a:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049379
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049380
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v0

    .line 2049381
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRootGroupPartDefinition;->c:LX/Dqq;

    .line 2049382
    iget-object p0, v1, LX/Dqq;->a:Ljava/util/Map;

    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2049383
    iget-object p0, v1, LX/Dqq;->a:Ljava/util/Map;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2049384
    :goto_0
    move-object v0, p0

    .line 2049385
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2049386
    return-object v2

    :cond_2
    iget-object p0, v1, LX/Dqq;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2049387
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049388
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049389
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2049390
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 2049391
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
