.class public Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/7g9;
.implements LX/Eq6;


# static fields
.field private static final t:Ljava/lang/String;


# instance fields
.field private A:Landroid/animation/ValueAnimator;

.field public B:Landroid/animation/ValueAnimator;

.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0jo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/ErU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/0gc;

.field public v:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

.field public w:Lcom/facebook/resources/ui/FbTextView;

.field private x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

.field private y:[J

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172918
    const-class v0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2172917
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;LX/03V;LX/0jo;Ljava/lang/Boolean;LX/ErU;)V
    .locals 0

    .prologue
    .line 2173020
    iput-object p1, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->p:LX/03V;

    iput-object p2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->q:LX/0jo;

    iput-object p3, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->r:Ljava/lang/Boolean;

    iput-object p4, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->s:LX/ErU;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v3}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v1

    check-cast v1, LX/0jo;

    invoke-static {v3}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {v3}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v3

    check-cast v3, LX/ErU;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->a(Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;LX/03V;LX/0jo;Ljava/lang/Boolean;LX/ErU;)V

    return-void
.end method

.method private b(Z)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2173012
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1431

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v0

    .line 2173013
    :goto_0
    if-eqz p1, :cond_1

    move v0, v1

    .line 2173014
    :goto_1
    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v2, v3, v1

    const/4 v1, 0x1

    aput v0, v3, v1

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2173015
    new-instance v1, LX/ErS;

    invoke-direct {v1, p0}, LX/ErS;-><init>(Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2173016
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2173017
    return-object v0

    :cond_0
    move v2, v1

    .line 2173018
    goto :goto_0

    .line 2173019
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b1431

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 2172989
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2172990
    const-string v1, "target_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2172991
    if-ne v1, v2, :cond_0

    .line 2172992
    :goto_0
    return-void

    .line 2172993
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->q:LX/0jo;

    invoke-interface {v2, v1}, LX/0jo;->a(I)LX/0jq;

    move-result-object v2

    .line 2172994
    const/4 v1, 0x0

    .line 2172995
    if-eqz v2, :cond_1

    .line 2172996
    invoke-interface {v2, v0}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2172997
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    instance-of v0, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    if-nez v0, :cond_3

    .line 2172998
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->p:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->t:Ljava/lang/String;

    const-string v2, "Failed to create a fragment"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172999
    iput-boolean v3, v1, LX/0VK;->d:Z

    .line 2173000
    move-object v1, v1

    .line 2173001
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2173002
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->finish()V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2173003
    check-cast v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->v:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 2173004
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->v:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 2173005
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v2

    .line 2173006
    if-nez v0, :cond_4

    .line 2173007
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2173008
    :cond_4
    const-string v2, "is_show_caspian_style"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2173009
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2173010
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->u:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2173011
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->u:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2172980
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2172981
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2172982
    if-nez v1, :cond_0

    .line 2172983
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f080bdc

    .line 2172984
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2172985
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2172986
    new-instance v1, LX/ErR;

    invoke-direct {v1, p0}, LX/ErR;-><init>(Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2172987
    return-void

    .line 2172988
    :cond_1
    const v1, 0x7f080bda

    goto :goto_0
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2172969
    const v0, 0x7f0d0e11

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    .line 2172970
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2172971
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 2172972
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_enable_invite_through_messenger"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2172973
    iget-object v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-virtual {v2, v4}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->setVisibility(I)V

    .line 2172974
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1431

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v4, v4, v4, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2172975
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2172976
    :goto_0
    return-void

    .line 2172977
    :cond_0
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2172978
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2172979
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->setVisibility(I)V

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2172966
    const v0, 0x7f0d0e10

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 2172967
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->o()V

    .line 2172968
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2172956
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_enable_invite_through_messenger"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2172957
    :cond_0
    :goto_0
    return-void

    .line 2172958
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->y:[J

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->y:[J

    array-length v0, v0

    const/16 v1, 0x19

    if-le v0, v1, :cond_2

    .line 2172959
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2172960
    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->z:Z

    if-nez v0, :cond_0

    .line 2172961
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->p()Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2172962
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-virtual {v0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b()V

    goto :goto_0

    .line 2172963
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2172964
    iput-boolean v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->z:Z

    .line 2172965
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-virtual {v0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a()V

    goto :goto_0
.end method

.method private p()Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 2172950
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->A:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->B:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_1

    .line 2172951
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->b(Z)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->A:Landroid/animation/ValueAnimator;

    .line 2172952
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->b(Z)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->B:Landroid/animation/ValueAnimator;

    .line 2172953
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->B:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 2172954
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->A:Landroid/animation/ValueAnimator;

    new-instance v1, LX/ErT;

    invoke-direct {v1, p0}, LX/ErT;-><init>(Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2172955
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->A:Landroid/animation/ValueAnimator;

    return-object v0
.end method


# virtual methods
.method public final a([J)V
    .locals 0

    .prologue
    .line 2172947
    iput-object p1, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->y:[J

    .line 2172948
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->o()V

    .line 2172949
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2172936
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2172937
    invoke-static {p0, p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2172938
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->s:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->a()V

    .line 2172939
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->u:LX/0gc;

    .line 2172940
    const v0, 0x7f0400c8

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->overridePendingTransition(II)V

    .line 2172941
    const v0, 0x7f0304d6

    invoke-virtual {p0, v0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->setContentView(I)V

    .line 2172942
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->l()V

    .line 2172943
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->b()V

    .line 2172944
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->m()V

    .line 2172945
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->n()V

    .line 2172946
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2172933
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2172934
    const v0, 0x7f0400b6

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->overridePendingTransition(II)V

    .line 2172935
    return-void
.end method

.method public final mr_()V
    .locals 4

    .prologue
    .line 2172924
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2172925
    const-string v1, "profiles"

    iget-object v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->y:[J

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2172926
    const-string v1, "extra_redirect_to_messenger"

    iget-object v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->x:Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-virtual {v2}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->getIsInvitingThroughMessenger()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2172927
    const-string v1, "event_id"

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172928
    const-string v1, "extra_invite_action_mechanism"

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_invite_action_mechanism"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172929
    const-string v1, "extra_invite_configuration_bundle"

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_invite_configuration_bundle"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2172930
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 2172931
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->finish()V

    .line 2172932
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2172922
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->v:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->A()V

    .line 2172923
    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2172919
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 2172920
    const v0, 0x7f0400d9

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;->overridePendingTransition(II)V

    .line 2172921
    return-void
.end method
