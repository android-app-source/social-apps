.class public Lcom/facebook/events/invite/EventsExtendedInviteActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

.field private B:Landroid/widget/ListView;

.field public C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field private D:Landroid/view/View;

.field private E:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private H:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private I:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private K:LX/ErU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private L:LX/ErW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Q:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private R:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final S:LX/EqG;

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/1OX;

.field public r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field public v:LX/BXD;

.field private w:LX/0Tn;

.field public x:Ljava/lang/String;

.field public y:LX/2EJ;

.field public z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2171414
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2171415
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    .line 2171416
    new-instance v0, LX/EqF;

    invoke-direct {v0, p0}, LX/EqF;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->q:LX/1OX;

    .line 2171417
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    .line 2171418
    new-instance v0, LX/EqG;

    invoke-direct {v0, p0}, LX/EqG;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->S:LX/EqG;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2171419
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2171420
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2171421
    new-instance v1, LX/EqH;

    invoke-direct {v1, p0}, LX/EqH;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2171422
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2171423
    new-instance v0, LX/0hs;

    invoke-direct {v0, p0, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2171424
    invoke-virtual {v0, p2}, LX/0hs;->b(I)V

    .line 2171425
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2171426
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->H:LX/0wM;

    const v2, 0x7f0208ed

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2171427
    iput v3, v0, LX/0hs;->t:I

    .line 2171428
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2171429
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2171430
    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsExtendedInviteActivity;LX/1nQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0wM;Landroid/view/inputmethod/InputMethodManager;LX/0iA;LX/ErU;LX/ErW;LX/0Or;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/3kp;LX/3kp;LX/3kp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/invite/EventsExtendedInviteActivity;",
            "LX/1nQ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0wM;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/0iA;",
            "LX/ErU;",
            "LX/ErW;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/3kp;",
            "LX/3kp;",
            "LX/3kp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2171431
    iput-object p1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->E:LX/1nQ;

    iput-object p2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->F:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->G:LX/0Uh;

    iput-object p4, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->H:LX/0wM;

    iput-object p5, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->I:Landroid/view/inputmethod/InputMethodManager;

    iput-object p6, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    iput-object p7, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->K:LX/ErU;

    iput-object p8, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    iput-object p9, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->M:LX/0Or;

    iput-object p10, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->N:LX/0ad;

    iput-object p11, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->O:Lcom/facebook/content/SecureContextHelper;

    iput-object p12, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->P:LX/3kp;

    iput-object p13, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->Q:LX/3kp;

    iput-object p14, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->R:LX/3kp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-static {v14}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {v14}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v14}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v14}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v14}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v14}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v6

    check-cast v6, LX/0iA;

    invoke-static {v14}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v7

    check-cast v7, LX/ErU;

    invoke-static {v14}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v8

    check-cast v8, LX/ErW;

    const/16 v9, 0x15e7

    invoke-static {v14, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v14}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v14}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v14}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v12

    check-cast v12, LX/3kp;

    invoke-static {v14}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v13

    check-cast v13, LX/3kp;

    invoke-static {v14}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v14

    check-cast v14, LX/3kp;

    invoke-static/range {v0 .. v14}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a(Lcom/facebook/events/invite/EventsExtendedInviteActivity;LX/1nQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0wM;Landroid/view/inputmethod/InputMethodManager;LX/0iA;LX/ErU;LX/ErW;LX/0Or;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/3kp;LX/3kp;LX/3kp;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;LX/8QL;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2171432
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->y:LX/2EJ;

    if-nez v0, :cond_0

    .line 2171433
    new-instance v0, LX/BXD;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BXD;-><init>(LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    .line 2171434
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0306f3

    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171435
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v3

    .line 2171436
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->B:Landroid/widget/ListView;

    .line 2171437
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->B:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2171438
    new-instance v0, LX/EqB;

    invoke-direct {v0, p0}, LX/EqB;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171439
    new-instance v1, LX/EqC;

    invoke-direct {v1, p0}, LX/EqC;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171440
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080be2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    invoke-virtual {v6}, LX/BXD;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->B:Landroid/widget/ListView;

    invoke-virtual {v2, v3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v2

    const v3, 0x7f080be0

    invoke-virtual {v2, v3, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080be1

    invoke-virtual {v0, v2, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->y:LX/2EJ;

    .line 2171441
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->B:Landroid/widget/ListView;

    new-instance v1, LX/EqD;

    invoke-direct {v1, p0}, LX/EqD;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2171442
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->B:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    invoke-virtual {v1, p1}, LX/BXD;->a(LX/8QL;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 2171443
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->y:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2171444
    return-void

    .line 2171445
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BXD;->a(LX/0Px;)V

    .line 2171446
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->y:LX/2EJ;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080be2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->v:LX/BXD;

    invoke-virtual {v4}, LX/BXD;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2171447
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->I:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2171448
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2171449
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171450
    :goto_0
    return-void

    .line 2171451
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 2171452
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2171453
    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v3

    .line 2171454
    iget-object p1, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-interface {p1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2171455
    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    goto :goto_1

    .line 2171456
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171457
    iget-object v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v1}, LX/ErB;->e()V

    .line 2171458
    iget-object v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    if-eqz v1, :cond_2

    .line 2171459
    iget-object v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2171460
    :cond_2
    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2171461
    const v0, 0x7f0d0f21

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->D:Landroid/view/View;

    .line 2171462
    const v0, 0x7f0d0f22

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2171463
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v1, LX/8us;->WHILE_EDITING:LX/8us;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setClearButtonMode(LX/8us;)V

    .line 2171464
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->D:Landroid/view/View;

    new-instance v1, LX/EqI;

    invoke-direct {v1, p0}, LX/EqI;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2171465
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 2171466
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/EqJ;

    invoke-direct {v1, p0}, LX/EqJ;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2171467
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/EqK;

    invoke-direct {v1, p0}, LX/EqK;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2171468
    return-void
.end method

.method public static b$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2171469
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->I:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2171470
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2171471
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "EVENTS_EXTENDED_INVITE_FRAGMENT_TAG"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2171472
    if-nez v0, :cond_0

    .line 2171473
    new-instance v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-direct {v0}, Lcom/facebook/events/invite/EventsExtendedInviteFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171474
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2171475
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    const-string v3, "EVENTS_EXTENDED_INVITE_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2171476
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->S:LX/EqG;

    .line 2171477
    iput-object v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->e:LX/EqG;

    .line 2171478
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->q:LX/1OX;

    .line 2171479
    iput-object v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->k:LX/1OX;

    .line 2171480
    return-void

    .line 2171481
    :cond_0
    check-cast v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    goto :goto_0
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2171482
    const v0, 0x7f0d0f24

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    .line 2171483
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    new-instance v2, LX/EqL;

    invoke-direct {v2, p0}, LX/EqL;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171484
    iput-object v2, v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->h:LX/BWy;

    .line 2171485
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->N:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/347;->t:S

    invoke-interface {v0, v2, v3, v4, v1}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2171486
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    new-instance v2, LX/EqM;

    invoke-direct {v2, p0}, LX/EqM;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(LX/BWx;)V

    .line 2171487
    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Z)V

    .line 2171488
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2171489
    goto :goto_0
.end method

.method public static synthetic m(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V
    .locals 0

    .prologue
    .line 2171405
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public static n(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V
    .locals 5

    .prologue
    .line 2171406
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2171407
    const-string v1, "event_id"

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171408
    const-string v1, "extra_events_note_text"

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171409
    const-string v1, "extra_events_guests_ids"

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171410
    iget-object v3, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    iget-object v4, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object v2, v3

    .line 2171411
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2171412
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->O:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1aa

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2171413
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2171396
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->Q:LX/3kp;

    .line 2171397
    iput v2, v0, LX/3kp;->b:I

    .line 2171398
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->Q:LX/3kp;

    sget-object v1, LX/7vb;->i:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2171399
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->Q:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->s:Z

    if-nez v0, :cond_0

    .line 2171400
    const/4 v0, 0x2

    const v1, 0x7f081eb4

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a(II)V

    .line 2171401
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->Q:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171402
    iput-boolean v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->s:Z

    .line 2171403
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4274"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171404
    :cond_0
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2171387
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->R:LX/3kp;

    .line 2171388
    iput v2, v0, LX/3kp;->b:I

    .line 2171389
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->R:LX/3kp;

    sget-object v1, LX/7vb;->k:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2171390
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->R:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->t:Z

    if-nez v0, :cond_0

    .line 2171391
    const/4 v0, 0x2

    const v1, 0x7f081eb5

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a(II)V

    .line 2171392
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->R:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171393
    iput-boolean v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->t:Z

    .line 2171394
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4276"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171395
    :cond_0
    return-void
.end method

.method private q()Z
    .locals 3

    .prologue
    .line 2171385
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    sget-object v1, LX/EqY;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/EqY;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/EqY;

    .line 2171386
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/EqY;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4274"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 3

    .prologue
    .line 2171383
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    sget-object v1, LX/Eqa;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/Eqa;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/Eqa;

    .line 2171384
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/Eqa;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4276"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)Z
    .locals 3

    .prologue
    .line 2171381
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    sget-object v1, LX/3kw;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kw;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kw;

    .line 2171382
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3kw;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4158"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 2171376
    new-instance v0, LX/EqN;

    invoke-direct {v0, p0}, LX/EqN;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171377
    new-instance v1, LX/EqA;

    invoke-direct {v1, p0}, LX/EqA;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171378
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080bdd

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080bde

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080bdf

    invoke-virtual {v2, v3, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080be0

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2171379
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2171380
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2171315
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2171316
    invoke-static {p0, p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2171317
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->K:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->a()V

    .line 2171318
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->M:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->w:LX/0Tn;

    .line 2171319
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->F:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->w:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->u:Z

    .line 2171320
    if-eqz p1, :cond_0

    .line 2171321
    const-string v0, "NOTE_TEXT_KEY"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    .line 2171322
    :cond_0
    const v0, 0x7f030573

    invoke-virtual {p0, v0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->setContentView(I)V

    .line 2171323
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a()V

    .line 2171324
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->b()V

    .line 2171325
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->l()V

    .line 2171326
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->m()V

    .line 2171327
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_invite_configuration_bundle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2171328
    iget-boolean v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->u:Z

    if-nez v1, :cond_2

    .line 2171329
    if-eqz v0, :cond_1

    const-string v1, "extra_invite_entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2171330
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->o()V

    .line 2171331
    :cond_2
    :goto_0
    return-void

    .line 2171332
    :cond_3
    const-string v1, "extra_invite_entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Blb;->fromString(Ljava/lang/String;)LX/Blb;

    move-result-object v0

    .line 2171333
    sget-object v1, LX/EqE;->a:[I

    invoke-virtual {v0}, LX/Blb;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2171334
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->o()V

    goto :goto_0

    .line 2171335
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v4, 0x1aa

    const/16 v3, 0x82

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2171365
    if-eq p1, v3, :cond_0

    if-ne p1, v4, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2171366
    const/4 v0, -0x1

    if-eq p2, v0, :cond_3

    .line 2171367
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 2171368
    goto :goto_0

    .line 2171369
    :cond_3
    const-string v0, "extra_events_note_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2171370
    const-string v0, "extra_events_note_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    .line 2171371
    :cond_4
    if-ne p1, v4, :cond_5

    .line 2171372
    const-string v0, "extra_events_guests_ids"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2171373
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    .line 2171374
    :cond_5
    if-ne p1, v3, :cond_1

    .line 2171375
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Z)V

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_2
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2171361
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2171362
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->t()V

    .line 2171363
    :goto_0
    return-void

    .line 2171364
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x484fb20

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171353
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->E:LX/1nQ;

    if-eqz v1, :cond_0

    .line 2171354
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    sget-object v2, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->c(LX/ErX;)V

    .line 2171355
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    sget-object v2, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->c(LX/ErX;)V

    .line 2171356
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    sget-object v2, LX/ErX;->CONTACTS:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->c(LX/ErX;)V

    .line 2171357
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->E:LX/1nQ;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    invoke-virtual {v3}, LX/ErW;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2171358
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->L:LX/ErW;

    invoke-virtual {v1}, LX/ErW;->b()V

    .line 2171359
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2171360
    const/16 v1, 0x23

    const v2, -0x7f3591b9

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x585ec5cc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171350
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2171351
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->b$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;Landroid/view/View;)V

    .line 2171352
    const/16 v1, 0x23

    const v2, -0x65ba7682

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x6fa1f7b9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2171339
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2171340
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 2171341
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->G:LX/0Uh;

    const/16 v2, 0x3f1

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2171342
    const v0, 0x7f081ead

    .line 2171343
    :goto_0
    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setHint(I)V

    .line 2171344
    :cond_0
    const v0, -0x3e698500

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 2171345
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->u:Z

    if-nez v0, :cond_2

    .line 2171346
    const v0, 0x7f081eab

    goto :goto_0

    .line 2171347
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->G:LX/0Uh;

    const/16 v2, 0x3f2

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2171348
    const v0, 0x7f081ead

    goto :goto_0

    .line 2171349
    :cond_3
    const v0, 0x7f081eac

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2171336
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2171337
    const-string v0, "NOTE_TEXT_KEY"

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171338
    return-void
.end method
