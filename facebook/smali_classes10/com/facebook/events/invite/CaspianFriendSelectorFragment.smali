.class public Lcom/facebook/events/invite/CaspianFriendSelectorFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# static fields
.field public static final G:Ljava/lang/String;


# instance fields
.field public A:LX/ErW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/3Oq;
    .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/8tF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0v6;

.field public I:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;"
        }
    .end annotation
.end field

.field public final J:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public N:Ljava/lang/String;

.field public O:Ljava/lang/String;

.field public P:I

.field public Q:I

.field public R:LX/8tE;

.field private S:LX/Eq6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2RC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/Era;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/ErU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2171174
    const-class v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2171134
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 2171135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->J:Ljava/util/ArrayList;

    .line 2171136
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->Q:I

    return-void
.end method

.method public static O(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2171137
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->C:LX/3Oq;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static P(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 5

    .prologue
    .line 2171138
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    if-nez v0, :cond_1

    .line 2171139
    :cond_0
    :goto_0
    return-void

    .line 2171140
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->K:Ljava/util/List;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2171141
    :cond_2
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2171142
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/invite/EventInviteeToken;

    .line 2171143
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    .line 2171144
    iget-object v4, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2171145
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2171146
    const/4 v3, 0x0

    .line 2171147
    iput-boolean v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 2171148
    :cond_4
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->K:Ljava/util/List;

    .line 2171149
    iget-object v4, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2171150
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2171151
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2171152
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 2171153
    :cond_5
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    .line 2171154
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(LX/0P1;)V

    .line 2171155
    goto :goto_0
.end method

.method public static R(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 1

    .prologue
    .line 2171156
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2171157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(LX/0P1;)V

    .line 2171158
    :cond_0
    return-void
.end method

.method public static W(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)Z
    .locals 1

    .prologue
    .line 2171159
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private X()[J
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2171160
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v4

    .line 2171161
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    new-array v5, v1, [J

    .line 2171162
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2171163
    add-int/lit8 v3, v2, 0x1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v5, v2

    .line 2171164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_0

    .line 2171165
    :cond_0
    return-object v5
.end method

.method public static c(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;Z)V
    .locals 4

    .prologue
    .line 2171166
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d:LX/1Ck;

    const-string v1, "setup_friends"

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c:LX/0TD;

    new-instance v3, LX/Eq4;

    invoke-direct {v3, p0, p1}, LX/Eq4;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;Z)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Eq5;

    invoke-direct {v3, p0}, LX/Eq5;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2171167
    return-void
.end method

.method public static c(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z
    .locals 1

    .prologue
    .line 2171168
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z
    .locals 1

    .prologue
    .line 2171169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D()V
    .locals 2

    .prologue
    .line 2171170
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D()V

    .line 2171171
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->S:LX/Eq6;

    if-eqz v0, :cond_0

    .line 2171172
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->S:LX/Eq6;

    invoke-direct {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->X()[J

    move-result-object v1

    invoke-interface {v0, v1}, LX/Eq6;->a([J)V

    .line 2171173
    :cond_0
    return-void
.end method

.method public final F()Z
    .locals 1

    .prologue
    .line 2171117
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)LX/44w;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)",
            "LX/44w",
            "<",
            "LX/0Rf",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2171118
    const-string v1, "all_friends_suggestion_section"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2171119
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2171120
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2171121
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2171122
    iget-object v4, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2171123
    iget-object v4, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2171124
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2171125
    :goto_1
    move-object v1, v1

    .line 2171126
    if-nez v1, :cond_3

    .line 2171127
    :cond_2
    :goto_2
    return-object v0

    .line 2171128
    :cond_3
    new-instance v2, LX/623;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->b(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2171129
    new-instance v1, LX/44w;

    invoke-direct {v1, v0, v2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    .line 2171130
    :cond_4
    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2171131
    :cond_5
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 2171132
    new-instance v2, LX/623;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->b(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    invoke-virtual {v3}, LX/0P1;->values()LX/0Py;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2171133
    new-instance v1, LX/44w;

    invoke-direct {v1, v0, v2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2170996
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b()LX/8tB;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 2170997
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/8QK;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2170998
    :cond_0
    :goto_0
    return-void

    .line 2170999
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(LX/8QL;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2171000
    const/4 v2, 0x1

    .line 2171001
    invoke-static {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->W(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2171002
    iget v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P:I

    if-le p1, v0, :cond_4

    .line 2171003
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->b(LX/ErX;I)V

    .line 2171004
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(I)V

    goto :goto_0

    .line 2171005
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    move v0, v0

    .line 2171006
    if-eqz v0, :cond_2

    .line 2171007
    const/4 v2, 0x1

    .line 2171008
    invoke-static {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->W(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2171009
    iget v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P:I

    if-le p1, v0, :cond_6

    .line 2171010
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->a(LX/ErX;I)V

    .line 2171011
    :goto_2
    invoke-static {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->W(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2171012
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->selectAll()V

    .line 2171013
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->g:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    .line 2171014
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->b(LX/ErX;I)V

    goto :goto_1

    .line 2171015
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->b(LX/ErX;I)V

    goto :goto_1

    .line 2171016
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->a(LX/ErX;I)V

    goto :goto_2

    .line 2171017
    :cond_7
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1, v2}, LX/ErW;->a(LX/ErX;I)V

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2171018
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2171019
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/2RC;->a(LX/0QB;)LX/2RC;

    move-result-object v5

    check-cast v5, LX/2RC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const-class v7, LX/Era;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Era;

    invoke-static {v0}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v8

    check-cast v8, LX/ErU;

    invoke-static {v0}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v9

    check-cast v9, LX/ErW;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v10

    check-cast v10, LX/1nQ;

    invoke-static {v0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v11

    check-cast v11, LX/3Oq;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    const-class p1, LX/8tF;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/8tF;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v3, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->u:LX/0tX;

    iput-object v4, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->w:LX/2RC;

    iput-object v6, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    iput-object v7, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->y:LX/Era;

    iput-object v8, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->z:LX/ErU;

    iput-object v9, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    iput-object v10, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->B:LX/1nQ;

    iput-object v11, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->C:LX/3Oq;

    iput-object v12, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->D:Ljava/lang/Boolean;

    iput-object p1, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->E:LX/8tF;

    iput-object v0, v2, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->F:LX/2RQ;

    .line 2171020
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->E:LX/8tF;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v1}, LX/8tB;->e()LX/8vE;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tF;->a(LX/8vE;)LX/8tE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    .line 2171021
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2171022
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 2171023
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2171024
    :cond_0
    :goto_0
    return v0

    .line 2171025
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2171026
    const-string v0, "all_friends_suggestion_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171027
    const v0, 0x7f081ecd

    .line 2171028
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x11499fae

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171029
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2171030
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2171031
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2171032
    const-string v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    .line 2171033
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2171034
    const-string v3, "group_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->O:Ljava/lang/String;

    .line 2171035
    const/4 v2, 0x7

    iput v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P:I

    .line 2171036
    new-instance v2, LX/0v6;

    const-string v3, "CaspianFriendSelector"

    invoke-direct {v2, v3}, LX/0v6;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    .line 2171037
    if-nez p1, :cond_1

    .line 2171038
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2171039
    const-string v6, "profiles"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v6

    .line 2171040
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2171041
    if-eqz v6, :cond_0

    .line 2171042
    array-length v8, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v8, :cond_0

    aget-wide v9, v6, v5

    .line 2171043
    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2171044
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2171045
    :cond_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 2171046
    iget-object v6, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    invoke-virtual {v5, v6}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 2171047
    invoke-virtual {v5, v7}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 2171048
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->p:LX/0Rf;

    .line 2171049
    :cond_1
    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->D:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->O:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2171050
    :cond_2
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->c(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;Z)V

    .line 2171051
    :goto_1
    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 2171052
    invoke-static {}, LX/7oV;->q()LX/7o8;

    move-result-object v2

    .line 2171053
    const-string v3, "event_id"

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "first_count"

    iget v6, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171054
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2171055
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2171056
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Eq0;

    invoke-direct {v3, p0}, LX/Eq0;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171057
    invoke-static {}, LX/7oV;->r()LX/7o9;

    move-result-object v2

    .line 2171058
    const-string v3, "event_id"

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171059
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2171060
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2171061
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Eq1;

    invoke-direct {v3, p0}, LX/Eq1;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171062
    :goto_2
    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->u:LX/0tX;

    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0v6;)V

    .line 2171063
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v2, v1}, LX/BWl;->c(Landroid/view/View;)Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    new-instance v2, LX/Epx;

    invoke-direct {v2, p0}, LX/Epx;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2171064
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/Epy;

    invoke-direct {v2, p0}, LX/Epy;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2171065
    const/16 v1, 0x2b

    const v2, -0x4f39d752

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2171066
    :cond_3
    new-instance v2, LX/7oU;

    invoke-direct {v2}, LX/7oU;-><init>()V

    move-object v2, v2

    .line 2171067
    const-string v3, "group_id"

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->O:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171068
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2171069
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2171070
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Epz;

    invoke-direct {v3, p0}, LX/Epz;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171071
    goto/16 :goto_1

    .line 2171072
    :cond_4
    new-instance v2, LX/7oF;

    invoke-direct {v2}, LX/7oF;-><init>()V

    move-object v2, v2

    .line 2171073
    const-string v3, "first_count"

    iget v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171074
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2171075
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2171076
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Eq2;

    invoke-direct {v3, p0}, LX/Eq2;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171077
    new-instance v2, LX/7oG;

    invoke-direct {v2}, LX/7oG;-><init>()V

    move-object v2, v2

    .line 2171078
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2171079
    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->H:LX/0v6;

    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2171080
    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Eq3;

    invoke-direct {v3, p0}, LX/Eq3;-><init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    iget-object v5, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171081
    goto/16 :goto_2
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2171082
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onAttach(Landroid/content/Context;)V

    .line 2171083
    const-class v0, LX/Eq6;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eq6;

    iput-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->S:LX/Eq6;

    .line 2171084
    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1805cd35

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171085
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v2, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->b(LX/ErX;)V

    .line 2171086
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v2, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->b(LX/ErX;)V

    .line 2171087
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->B:LX/1nQ;

    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    invoke-virtual {v3}, LX/ErW;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2171088
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    invoke-virtual {v1}, LX/ErW;->b()V

    .line 2171089
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onDestroy()V

    .line 2171090
    const/16 v1, 0x2b

    const v2, -0x4d6d730a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x77452a0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171091
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onDestroyView()V

    .line 2171092
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->z:LX/ErU;

    invoke-virtual {v1}, LX/ErU;->d()V

    .line 2171093
    const/16 v1, 0x2b

    const v2, -0x9e4324c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2171094
    iget v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->Q:I

    return v0
.end method

.method public final u()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2171095
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171096
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->u()LX/0Px;

    move-result-object v0

    .line 2171097
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "all_friends_suggestion_section"

    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 2171098
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171099
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2171100
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 2171101
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->N:LX/3NY;

    move-object v2, v2

    .line 2171102
    invoke-virtual {v0, v1, v2}, LX/3Mm;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 2171103
    return-void
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 2171104
    const-class v0, LX/7g9;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7g9;

    .line 2171105
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->S:LX/Eq6;

    if-eqz v1, :cond_0

    .line 2171106
    iget-object v1, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->S:LX/Eq6;

    invoke-direct {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->X()[J

    move-result-object v2

    invoke-interface {v1, v2}, LX/Eq6;->a([J)V

    .line 2171107
    invoke-interface {v0}, LX/7g9;->mr_()V

    .line 2171108
    :goto_0
    return-void

    .line 2171109
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2171110
    const-string v1, "profiles"

    invoke-direct {p0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->X()[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2171111
    const-string v1, "event_id"

    iget-object v2, p0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171112
    const-string v1, "extra_invite_action_mechanism"

    .line 2171113
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2171114
    const-string v3, "extra_invite_action_mechanism"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171115
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2171116
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method
