.class public Lcom/facebook/events/invite/InviteePickerRow;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final e:Landroid/widget/RadioButton;

.field private final f:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2173172
    const-class v0, Lcom/facebook/events/invite/InviteePickerRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/invite/InviteePickerRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2173173
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2173174
    const v0, 0x7f0304d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2173175
    const v0, 0x7f0d0e18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->b:Landroid/widget/TextView;

    .line 2173176
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->e:Landroid/widget/RadioButton;

    .line 2173177
    const v0, 0x7f0d0e19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->c:Landroid/widget/TextView;

    .line 2173178
    const v0, 0x7f0d0e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2173179
    const v0, 0x7f0d0e1a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->f:Landroid/view/View;

    .line 2173180
    return-void
.end method


# virtual methods
.method public final a(LX/8QL;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2173181
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2173182
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2173183
    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2173184
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->f:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2173185
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->e:Landroid/widget/RadioButton;

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 2173186
    :goto_0
    invoke-virtual {p1}, LX/8QL;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2173187
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->e:Landroid/widget/RadioButton;

    if-nez p2, :cond_0

    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2173188
    :goto_2
    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2173189
    iget-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/events/invite/InviteePickerRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2173190
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2173191
    return-void

    .line 2173192
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->f:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2173193
    iget-object v2, p0, Lcom/facebook/events/invite/InviteePickerRow;->e:Landroid/widget/RadioButton;

    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2173194
    goto :goto_1

    .line 2173195
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/invite/InviteePickerRow;->e:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto :goto_2
.end method
