.class public Lcom/facebook/events/invite/EventsExtendedInviteFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/ErU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/ErW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field public d:Z

.field public e:LX/EqG;

.field public f:LX/Equ;

.field public g:LX/Er0;

.field public h:LX/ErB;

.field public i:LX/ErM;

.field private j:LX/0Tn;

.field public k:LX/1OX;

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field public n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/1OX;

.field private final p:LX/Eqe;

.field public q:I

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public v:Landroid/support/v4/view/ViewPager;

.field public w:LX/Eqv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Er1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/ErC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/ErN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2172011
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2172012
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    .line 2172013
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->l:Ljava/util/Set;

    .line 2172014
    new-instance v0, LX/Eqd;

    invoke-direct {v0, p0}, LX/Eqd;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->o:LX/1OX;

    .line 2172015
    new-instance v0, LX/Eqe;

    invoke-direct {v0, p0}, LX/Eqe;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->p:LX/Eqe;

    .line 2172016
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    .line 2172017
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172018
    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    .line 2172019
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172020
    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsExtendedInviteFragment;Z)V
    .locals 3

    .prologue
    .line 2172005
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 2172006
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2172007
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 2172008
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2172009
    :cond_0
    return-void

    .line 2172010
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2171996
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2171997
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    const-class v3, LX/Eqv;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Eqv;

    const-class v4, LX/Er1;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Er1;

    const-class v5, LX/ErC;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/ErC;

    const-class v6, LX/ErN;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ErN;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    invoke-static {v0}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v10

    check-cast v10, LX/ErU;

    invoke-static {v0}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v11

    check-cast v11, LX/ErW;

    const/16 v12, 0x15e7

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v0

    check-cast v0, LX/3kp;

    iput-object v3, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->w:LX/Eqv;

    iput-object v4, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->x:LX/Er1;

    iput-object v5, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->y:LX/ErC;

    iput-object v6, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->z:LX/ErN;

    iput-object v7, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v8, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->B:LX/0wM;

    iput-object v9, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->C:LX/0iA;

    iput-object v10, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->D:LX/ErU;

    iput-object v11, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->E:LX/ErW;

    iput-object v12, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->F:LX/0Or;

    iput-object v0, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->G:LX/3kp;

    .line 2171998
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2171999
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->m:Ljava/lang/String;

    .line 2172000
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->F:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->j:LX/0Tn;

    .line 2172001
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->j:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->b:Z

    .line 2172002
    if-eqz p1, :cond_0

    .line 2172003
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "SELECTED_FRIENDS_TOKENS_IDS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->l:Ljava/util/Set;

    .line 2172004
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 14

    .prologue
    const/4 v2, 0x1

    .line 2171950
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2171951
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_1

    .line 2171952
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    const-string v1, ""

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7HQ;->b(LX/7B6;)Z

    .line 2171953
    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->d:Z

    if-eqz v0, :cond_0

    .line 2171954
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->E:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1}, LX/ErW;->b(LX/ErX;)V

    .line 2171955
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->d:Z

    .line 2171956
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2171957
    invoke-static {p0, v2}, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a(Lcom/facebook/events/invite/EventsExtendedInviteFragment;Z)V

    .line 2171958
    :cond_1
    :goto_0
    return-void

    .line 2171959
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->d:Z

    if-nez v0, :cond_3

    .line 2171960
    iput-boolean v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->d:Z

    .line 2171961
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->E:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1}, LX/ErW;->a(LX/ErX;)V

    .line 2171962
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_5

    .line 2171963
    const v0, 0x7f0d0f2e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2171964
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2171965
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->x:LX/Er1;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->e:LX/EqG;

    .line 2171966
    new-instance v5, LX/Er0;

    invoke-static {v0}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v3

    check-cast v3, LX/ErW;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-direct {v5, v3, v4, v1, v2}, LX/Er0;-><init>(LX/ErW;LX/0kL;Ljava/util/Set;LX/EqG;)V

    .line 2171967
    move-object v0, v5

    .line 2171968
    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    .line 2171969
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    iget v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    .line 2171970
    iput v1, v0, LX/Er0;->d:I

    .line 2171971
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2171972
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2171973
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/Eqk;

    invoke-direct {v1, p0}, LX/Eqk;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2171974
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->k:LX/1OX;

    if-eqz v0, :cond_4

    .line 2171975
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->k:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2171976
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->z:LX/ErN;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->m:Ljava/lang/String;

    .line 2171977
    new-instance v3, LX/ErM;

    .line 2171978
    new-instance v5, LX/ErI;

    invoke-static {v0}, LX/8i9;->b(LX/0QB;)LX/8i9;

    move-result-object v4

    check-cast v4, LX/8i9;

    invoke-direct {v5, v4}, LX/ErI;-><init>(LX/8i9;)V

    .line 2171979
    move-object v4, v5

    .line 2171980
    check-cast v4, LX/ErI;

    const-class v5, LX/ErL;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/ErL;

    .line 2171981
    new-instance v10, LX/Er3;

    const-class v6, LX/Eqc;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Eqc;

    invoke-static {v0}, LX/ErO;->b(LX/0QB;)LX/ErO;

    move-result-object v7

    check-cast v7, LX/ErO;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-direct {v10, v6, v7, v8, v9}, LX/Er3;-><init>(LX/Eqc;LX/ErO;LX/03V;LX/1Ck;)V

    .line 2171982
    move-object v6, v10

    .line 2171983
    check-cast v6, LX/Er3;

    invoke-static {v0}, LX/7Hg;->b(LX/0QB;)LX/7Hg;

    move-result-object v7

    check-cast v7, LX/7Hg;

    invoke-static {v0}, LX/7Hl;->b(LX/0QB;)LX/7Hl;

    move-result-object v8

    check-cast v8, LX/7Hl;

    .line 2171984
    new-instance v9, LX/ErP;

    invoke-direct {v9}, LX/ErP;-><init>()V

    .line 2171985
    move-object v9, v9

    .line 2171986
    move-object v9, v9

    .line 2171987
    check-cast v9, LX/ErP;

    const-class v10, LX/7Hk;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/7Hk;

    invoke-static {v0}, LX/7Ho;->b(LX/0QB;)LX/7Ho;

    move-result-object v11

    check-cast v11, LX/7Ho;

    invoke-static {v0}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v12

    check-cast v12, LX/2Sd;

    move-object v13, v1

    invoke-direct/range {v3 .. v13}, LX/ErM;-><init>(LX/ErI;LX/ErL;LX/Er3;LX/7Hg;LX/7Hl;LX/ErP;LX/7Hk;LX/7Ho;LX/2Sd;Ljava/lang/String;)V

    .line 2171988
    move-object v0, v3

    .line 2171989
    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    .line 2171990
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    new-instance v1, LX/Eqi;

    invoke-direct {v1, p0}, LX/Eqi;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    .line 2171991
    iput-object v1, v0, LX/7HQ;->k:LX/7HP;

    .line 2171992
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    new-instance v1, LX/Eqj;

    invoke-direct {v1, p0}, LX/Eqj;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    invoke-virtual {v0, v1}, LX/7HQ;->a(LX/2Sp;)V

    .line 2171993
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    invoke-virtual {v0, v1}, LX/ErM;->a(LX/0Px;)V

    .line 2171994
    :cond_5
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a(Lcom/facebook/events/invite/EventsExtendedInviteFragment;Z)V

    .line 2171995
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7HQ;->b(LX/7B6;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2172021
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2172022
    const-string v1, "extra_enable_extended_invite"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2172023
    const-string v1, "profiles"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2172024
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2172025
    const-string v1, "extra_events_note_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172026
    :cond_0
    const-string v1, "event_id"

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172027
    const-string v1, "extra_invite_action_mechanism"

    .line 2172028
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2172029
    const-string v3, "extra_invite_action_mechanism"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172030
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2172031
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2172032
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e1b94e7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171949
    const v1, 0x7f030578

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5f0a7450

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7cc6caa1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171936
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2171937
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->f:LX/Equ;

    if-eqz v1, :cond_0

    .line 2171938
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->f:LX/Equ;

    .line 2171939
    iget-object v2, v1, LX/Equ;->z:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2171940
    iget-object v2, v1, LX/Equ;->d:LX/0Vd;

    iget-object v4, v1, LX/Equ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2, v4}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2171941
    iget-object v2, v1, LX/Equ;->e:LX/0Vd;

    iget-object v4, v1, LX/Equ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2, v4}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2171942
    iget-object v2, v1, LX/Equ;->b:LX/0Vd;

    iget-object v4, v1, LX/Equ;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2, v4}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2171943
    iget-object v2, v1, LX/Equ;->f:LX/0Vd;

    iget-object v4, v1, LX/Equ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2, v4}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2171944
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    if-eqz v1, :cond_1

    .line 2171945
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    .line 2171946
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 2171947
    invoke-virtual {v1, v2}, LX/7HQ;->a(LX/0P1;)V

    .line 2171948
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x6a10fa0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0xa960f11

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171930
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2171931
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->D:LX/ErU;

    invoke-virtual {v1}, LX/ErU;->d()V

    .line 2171932
    iput-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2171933
    iput-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2171934
    iput-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    .line 2171935
    const/16 v1, 0x2b

    const v2, 0x5cf6349d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ff447dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171921
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2171922
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v1}, LX/ErB;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->b:Z

    if-eqz v1, :cond_0

    .line 2171923
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->f:LX/Equ;

    invoke-virtual {v1}, LX/Equ;->a()V

    .line 2171924
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    .line 2171925
    iget-object v4, v1, LX/ErB;->g:[LX/Er9;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object p0, v4, v2

    .line 2171926
    if-eqz p0, :cond_1

    .line 2171927
    invoke-virtual {p0}, LX/Er9;->c()V

    .line 2171928
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2171929
    :cond_2
    const/16 v1, 0x2b

    const v2, 0x57585f71

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2171918
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2171919
    const-string v0, "SELECTED_FRIENDS_TOKENS_IDS"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2171920
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2171825
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2171826
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->y:LX/ErC;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->p:LX/Eqe;

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->e:LX/EqG;

    iget-object v4, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->k:LX/1OX;

    iget-object v5, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->o:LX/1OX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/ErC;->a(Ljava/util/Set;LX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)LX/ErB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    .line 2171827
    const v0, 0x7f0d0f30

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    .line 2171828
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2171829
    const v0, 0x7f0d0f2f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2171830
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2171831
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-le v0, v7, :cond_4

    .line 2171832
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setPadding(IIII)V

    .line 2171833
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillParentWidth(Z)V

    .line 2171834
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Eqf;

    invoke-direct {v1, p0}, LX/Eqf;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    .line 2171835
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 2171836
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2171837
    const-string v1, "extra_invite_configuration_bundle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2171838
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v1}, LX/ErB;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2171839
    const-string v1, "extra_invite_entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Blb;->fromString(Ljava/lang/String;)LX/Blb;

    move-result-object v0

    .line 2171840
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->v:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v2, v0}, LX/ErB;->a(LX/Blb;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2171841
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->G:LX/3kp;

    .line 2171842
    iput v7, v1, LX/3kp;->b:I

    .line 2171843
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->G:LX/3kp;

    sget-object v2, LX/7vb;->j:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 2171844
    sget-object v1, LX/Blb;->FACEBOOK:LX/Blb;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->G:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171845
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->C:LX/0iA;

    sget-object v1, LX/EqZ;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/EqZ;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/EqZ;

    .line 2171846
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/EqZ;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4275"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2171847
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->c:Z

    if-nez v0, :cond_0

    .line 2171848
    const/4 v3, -0x1

    .line 2171849
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2171850
    const v1, 0x7f081eb4

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2171851
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2171852
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->B:LX/0wM;

    const v2, 0x7f0208ed

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2171853
    iput v3, v0, LX/0hs;->t:I

    .line 2171854
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    sget-object v3, LX/Blb;->CONTACTS:LX/Blb;

    invoke-virtual {v2, v3}, LX/ErB;->a(LX/Blb;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2171855
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2171856
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->G:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171857
    iput-boolean v7, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->c:Z

    .line 2171858
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->C:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4275"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171859
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->w:LX/Eqv;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->m:Ljava/lang/String;

    new-instance v2, LX/Eqg;

    invoke-direct {v2, p0}, LX/Eqg;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V

    invoke-virtual {v0, v1, v2}, LX/Eqv;->a(Ljava/lang/String;LX/Eqg;)LX/Equ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->f:LX/Equ;

    .line 2171860
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2171861
    const-string v1, "extra_cohost_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->n:Ljava/util/ArrayList;

    .line 2171862
    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v0}, LX/ErB;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v7

    .line 2171863
    :goto_2
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2171864
    const-string v2, "extra_original_event_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2171865
    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->f:LX/Equ;

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->l:Ljava/util/Set;

    .line 2171866
    iget-object v4, v2, LX/Equ;->y:LX/0TD;

    new-instance v5, LX/Eqq;

    invoke-direct {v5, v2}, LX/Eqq;-><init>(LX/Equ;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v4, v4

    .line 2171867
    iput-object v4, v2, LX/Equ;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171868
    iget-object v4, v2, LX/Equ;->o:LX/0v6;

    .line 2171869
    invoke-static {}, LX/7oV;->q()LX/7o8;

    move-result-object v5

    .line 2171870
    const-string v6, "event_id"

    iget-object v7, v2, LX/Equ;->q:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "first_count"

    const-string v8, "7"

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171871
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    .line 2171872
    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, LX/Eqr;

    invoke-direct {v6, v2}, LX/Eqr;-><init>(LX/Equ;)V

    iget-object v7, v2, LX/Equ;->y:LX/0TD;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 2171873
    iput-object v4, v2, LX/Equ;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171874
    iget-object v4, v2, LX/Equ;->o:LX/0v6;

    .line 2171875
    invoke-static {}, LX/7oV;->r()LX/7o9;

    move-result-object v5

    .line 2171876
    const-string v6, "event_id"

    iget-object v7, v2, LX/Equ;->q:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171877
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    .line 2171878
    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-static {v5}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 2171879
    iput-object v4, v2, LX/Equ;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171880
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v5, 0x0

    iget-object v6, v2, LX/Equ;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v2, LX/Equ;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171881
    new-instance v4, LX/Eqo;

    invoke-direct {v4, v2}, LX/Eqo;-><init>(LX/Equ;)V

    move-object v4, v4

    .line 2171882
    iput-object v4, v2, LX/Equ;->d:LX/0Vd;

    .line 2171883
    iget-object v4, v2, LX/Equ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, v2, LX/Equ;->d:LX/0Vd;

    iget-object v6, v2, LX/Equ;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171884
    iget-object v4, v2, LX/Equ;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, v2, LX/Equ;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171885
    new-instance v4, LX/Eqp;

    invoke-direct {v4, v2}, LX/Eqp;-><init>(LX/Equ;)V

    move-object v4, v4

    .line 2171886
    iput-object v4, v2, LX/Equ;->e:LX/0Vd;

    .line 2171887
    iget-object v4, v2, LX/Equ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, v2, LX/Equ;->e:LX/0Vd;

    iget-object v6, v2, LX/Equ;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171888
    if-eqz v0, :cond_1

    .line 2171889
    iget-object v4, v2, LX/Equ;->o:LX/0v6;

    invoke-static {v2}, LX/Equ;->g(LX/Equ;)LX/0zO;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2171890
    iget-object v5, v2, LX/Equ;->z:LX/1Ck;

    const-string v6, "FETCH_CONTACTS_TASK"

    new-instance v7, LX/Eqm;

    invoke-direct {v7, v2, v4}, LX/Eqm;-><init>(LX/Equ;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-static {v2}, LX/Equ;->h(LX/Equ;)LX/0Vd;

    move-result-object v4

    invoke-virtual {v5, v6, v7, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2171891
    :cond_1
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2171892
    iget-object v4, v2, LX/Equ;->o:LX/0v6;

    .line 2171893
    invoke-static {}, LX/7oV;->n()LX/7nw;

    move-result-object v5

    .line 2171894
    const-string v6, "event_id"

    iget-object v7, v2, LX/Equ;->q:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171895
    const-string v6, "tokens"

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2171896
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    move-object v5, v5

    .line 2171897
    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171898
    invoke-static {v2}, LX/Equ;->i(LX/Equ;)LX/0Vd;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->b:LX/0Vd;

    .line 2171899
    iget-object v4, v2, LX/Equ;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, v2, LX/Equ;->b:LX/0Vd;

    iget-object v6, v2, LX/Equ;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171900
    :cond_2
    if-eqz v1, :cond_3

    .line 2171901
    new-instance v4, LX/7nt;

    invoke-direct {v4}, LX/7nt;-><init>()V

    move-object v4, v4

    .line 2171902
    const-string v5, "event_id"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "first_count"

    const-string v7, "2147483647"

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171903
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    move-object v4, v4

    .line 2171904
    iget-object v5, v2, LX/Equ;->o:LX/0v6;

    invoke-virtual {v5, v4}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171905
    const-string v5, "user_id"

    sget-object v6, LX/4Zz;->ALL:LX/4Zz;

    .line 2171906
    sget-object v7, LX/4a0;->NOT_SET:LX/4a0;

    invoke-virtual {v4, v5, v6, v7}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v7

    move-object v4, v7

    .line 2171907
    invoke-static {}, LX/7oV;->n()LX/7nw;

    move-result-object v5

    .line 2171908
    const-string v6, "event_id"

    iget-object v7, v2, LX/Equ;->q:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2171909
    const-string v6, "tokens"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 2171910
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    move-object v4, v5

    .line 2171911
    iget-object v5, v2, LX/Equ;->o:LX/0v6;

    invoke-virtual {v5, v4}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2171912
    invoke-static {v2}, LX/Equ;->i(LX/Equ;)LX/0Vd;

    move-result-object v4

    iput-object v4, v2, LX/Equ;->f:LX/0Vd;

    .line 2171913
    iget-object v4, v2, LX/Equ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v5, v2, LX/Equ;->f:LX/0Vd;

    iget-object v6, v2, LX/Equ;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2171914
    :cond_3
    iget-object v4, v2, LX/Equ;->v:LX/0tX;

    iget-object v5, v2, LX/Equ;->o:LX/0v6;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0v6;)V

    .line 2171915
    return-void

    .line 2171916
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->u:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v8

    .line 2171917
    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1
.end method
