.class public Lcom/facebook/events/invite/EventsExtendedInviteUserToken;
.super Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/invite/EventsExtendedInviteUserToken;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172861
    new-instance v0, LX/ErQ;

    invoke-direct {v0}, LX/ErQ;-><init>()V

    sput-object v0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2172858
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Landroid/os/Parcel;)V

    .line 2172859
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;->e:Ljava/lang/String;

    .line 2172860
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/UserKey;Lcom/facebook/user/model/Name;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 2172854
    if-nez p5, :cond_2

    move v4, v0

    :goto_0
    if-eqz p5, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    move v5, v0

    :cond_1
    move-object v0, p0

    move-object v1, p2

    move-object v2, p4

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;ZZ)V

    .line 2172855
    iput-object p3, p0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;->e:Ljava/lang/String;

    .line 2172856
    return-void

    :cond_2
    move v4, v5

    .line 2172857
    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2172853
    const/4 v0, 0x0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2172848
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2172852
    const/4 v0, 0x1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2172849
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2172850
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2172851
    return-void
.end method
