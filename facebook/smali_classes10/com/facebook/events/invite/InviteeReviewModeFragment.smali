.class public Lcom/facebook/events/invite/InviteeReviewModeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Ere;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Eq8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ErW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2173255
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2173256
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2173257
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;

    new-instance v3, LX/Ere;

    invoke-direct {v3}, LX/Ere;-><init>()V

    move-object v3, v3

    move-object v3, v3

    check-cast v3, LX/Ere;

    invoke-static {v0}, LX/Eq8;->a(LX/0QB;)LX/Eq8;

    move-result-object v4

    check-cast v4, LX/Eq8;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v0

    check-cast v0, LX/ErW;

    iput-object v3, v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    iput-object v4, v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;->b:LX/Eq8;

    iput-object v5, v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;->d:LX/0kL;

    iput-object v0, v2, Lcom/facebook/events/invite/InviteeReviewModeFragment;->e:LX/ErW;

    .line 2173258
    if-nez p1, :cond_0

    .line 2173259
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2173260
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->h:Ljava/lang/String;

    .line 2173261
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->b:LX/Eq8;

    .line 2173262
    iget-object v2, v1, LX/Eq8;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2173263
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2173264
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->b:LX/Eq8;

    .line 2173265
    iget-object v1, v0, LX/Eq8;->a:Ljava/util/LinkedList;

    move-object v0, v1

    .line 2173266
    iput-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    .line 2173267
    :goto_1
    return-void

    .line 2173268
    :cond_0
    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2173269
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->c:LX/03V;

    const-string v1, "Events"

    const-string v2, "Event id passed to InviteeReviewModeFragment differs from the id in bundle"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173270
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->d:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081ec0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    const/16 v2, 0x11

    .line 2173271
    iput v2, v1, LX/27k;->b:I

    .line 2173272
    move-object v1, v1

    .line 2173273
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2173274
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7f7b8905

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2173275
    const v1, 0x7f030997

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4410b67a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d635ba9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2173276
    iget-object v1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->e:LX/ErW;

    sget-object v2, LX/ErX;->REVIEW:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->b(LX/ErX;)V

    .line 2173277
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2173278
    const/16 v1, 0x2b

    const v2, -0x27c03932

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5cdbcd60

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2173279
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2173280
    iget-object v1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->e:LX/ErW;

    sget-object v2, LX/ErX;->REVIEW:LX/ErX;

    invoke-virtual {v1, v2}, LX/ErW;->a(LX/ErX;)V

    .line 2173281
    const/16 v1, 0x2b

    const v2, 0x43982b73

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2173282
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2173283
    const-string v0, "event_id"

    iget-object v1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173284
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2173285
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2173286
    const v0, 0x7f0d1871

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2173287
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object p1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2173288
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2173289
    new-instance p1, LX/623;

    const-string p2, "Selected"

    invoke-direct {p1, p2, v0}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2173290
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2173291
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2173292
    iget-object p1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    .line 2173293
    iput-object v0, p1, LX/Ere;->e:Ljava/util/List;

    .line 2173294
    const p2, -0xfb8b851

    invoke-static {p1, p2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2173295
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    iget-object p1, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    .line 2173296
    iput-object p1, v0, LX/Ere;->d:Ljava/util/List;

    .line 2173297
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2173298
    iget-object v0, p0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/Erf;

    invoke-direct {p1, p0}, LX/Erf;-><init>(Lcom/facebook/events/invite/InviteeReviewModeFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2173299
    return-void
.end method
