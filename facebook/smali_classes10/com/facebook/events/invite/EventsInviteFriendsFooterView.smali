.class public Lcom/facebook/events/invite/EventsInviteFriendsFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/widget/SwitchCompat;

.field private e:Landroid/content/Context;

.field private f:LX/0hs;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2172900
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2172901
    invoke-direct {p0, p1}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Landroid/content/Context;)V

    .line 2172902
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2172862
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2172863
    invoke-direct {p0, p1}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Landroid/content/Context;)V

    .line 2172864
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2172897
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2172898
    invoke-direct {p0, p1}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Landroid/content/Context;)V

    .line 2172899
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2172887
    const-class v0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-static {v0, p0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2172888
    const v0, 0x7f0304d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2172889
    iput-object p1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->e:Landroid/content/Context;

    .line 2172890
    const v0, 0x7f0d0e14

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->d:Lcom/facebook/widget/SwitchCompat;

    .line 2172891
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2172892
    const v0, 0x7f0d0e12

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2172893
    const v1, 0x7f0d0e13

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2172894
    const v2, 0x7f02074a

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2172895
    const v0, 0x7f081ecb

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2172896
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsInviteFriendsFooterView;LX/0iA;LX/3kp;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 2172886
    iput-object p1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a:LX/0iA;

    iput-object p2, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b:LX/3kp;

    iput-object p3, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->c:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;

    invoke-static {v2}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-static {v2}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v1

    check-cast v1, LX/3kp;

    invoke-static {v2}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a(Lcom/facebook/events/invite/EventsInviteFriendsFooterView;LX/0iA;LX/3kp;Ljava/lang/Boolean;)V

    return-void
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 2172884
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a:LX/0iA;

    sget-object v1, LX/2Fg;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/2Fg;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/2Fg;

    .line 2172885
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2Fg;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3936"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2172882
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->d:Lcom/facebook/widget/SwitchCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setEnabled(Z)V

    .line 2172883
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2172880
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->d:Lcom/facebook/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setEnabled(Z)V

    .line 2172881
    return-void
.end method

.method public getIsInvitingThroughMessenger()Z
    .locals 1

    .prologue
    .line 2172879
    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->d:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->d:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x7b9c87f6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2172865
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2172866
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b:LX/3kp;

    sget-object v2, LX/7vb;->d:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 2172867
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b:LX/3kp;

    .line 2172868
    iput v4, v1, LX/3kp;->b:I

    .line 2172869
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->g:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2172870
    new-instance v1, LX/0hs;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->e:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->f:LX/0hs;

    .line 2172871
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->f:LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081ec9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2172872
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->f:LX/0hs;

    const/4 v2, -0x1

    .line 2172873
    iput v2, v1, LX/0hs;->t:I

    .line 2172874
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->f:LX/0hs;

    invoke-virtual {v1, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2172875
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->g:Z

    .line 2172876
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->b:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->a()V

    .line 2172877
    iget-object v1, p0, Lcom/facebook/events/invite/EventsInviteFriendsFooterView;->a:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "3936"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2172878
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3695a40

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
