.class public Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Z

.field public q:Landroid/view/View;

.field private r:Landroid/view/ViewStub;

.field public s:Lcom/facebook/resources/ui/FbEditText;

.field private t:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2171681
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2171639
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2171640
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2171641
    new-instance v1, LX/EqU;

    invoke-direct {v1, p0}, LX/EqU;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2171642
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const/4 v2, 0x1

    .line 2171643
    iput v2, v1, LX/108;->a:I

    .line 2171644
    move-object v1, v1

    .line 2171645
    const v2, 0x7f081ecf

    invoke-virtual {p0, v2}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2171646
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2171647
    move-object v1, v1

    .line 2171648
    const/4 v2, -0x2

    .line 2171649
    iput v2, v1, LX/108;->h:I

    .line 2171650
    move-object v1, v1

    .line 2171651
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2171652
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2171653
    new-instance v1, LX/EqV;

    invoke-direct {v1, p0}, LX/EqV;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2171654
    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;LX/0iA;LX/3kp;)V
    .locals 0

    .prologue
    .line 2171680
    iput-object p1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->t:LX/0iA;

    iput-object p2, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->u:LX/3kp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;

    invoke-static {v1}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-static {v1}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v1

    check-cast v1, LX/3kp;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->a(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;LX/0iA;LX/3kp;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2171682
    const v0, 0x7f0d0f25

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    .line 2171683
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_events_note_text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171684
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_events_note_text"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2171685
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/EqW;

    invoke-direct {v1, p0}, LX/EqW;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2171686
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 2171687
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {p0, v0}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2171688
    return-void
.end method

.method private l()Z
    .locals 3

    .prologue
    .line 2171678
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->t:LX/0iA;

    sget-object v1, LX/3kx;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kx;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kx;

    .line 2171679
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3kx;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4159"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2171671
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2171672
    invoke-static {p0, p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2171673
    const v0, 0x7f030576

    invoke-virtual {p0, v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->setContentView(I)V

    .line 2171674
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->a()V

    .line 2171675
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->b()V

    .line 2171676
    const v0, 0x7f0d0f2c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->r:Landroid/view/ViewStub;

    .line 2171677
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 2171655
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onAttachedToWindow()V

    .line 2171656
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->u:LX/3kp;

    const/4 v1, 0x2

    .line 2171657
    iput v1, v0, LX/3kp;->b:I

    .line 2171658
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->u:LX/3kp;

    sget-object v1, LX/7vb;->g:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2171659
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->u:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->p:Z

    if-nez v0, :cond_2

    .line 2171660
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2171661
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->r:Landroid/view/ViewStub;

    const v1, 0x7f03057c

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2171662
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->r:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    .line 2171663
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    const v1, 0x7f0d0f36

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/EqX;

    invoke-direct {v1, p0}, LX/EqX;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2171664
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2171665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->p:Z

    .line 2171666
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->u:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171667
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->t:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4159"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171668
    :cond_1
    :goto_0
    return-void

    .line 2171669
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2171670
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3b1a80f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171636
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2171637
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2171638
    const/16 v1, 0x23

    const v2, 0x72cefee6    # 8.1999444E30f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
