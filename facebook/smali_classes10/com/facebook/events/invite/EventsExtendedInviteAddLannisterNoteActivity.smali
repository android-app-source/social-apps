.class public Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final A:LX/1OX;

.field private final B:LX/EqQ;

.field private final C:LX/63W;

.field public p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public q:Lcom/facebook/resources/ui/FbEditText;

.field public r:Landroid/view/View;

.field public s:LX/ErF;

.field public t:LX/ErH;

.field private u:Z

.field private v:Ljava/lang/String;

.field public w:[Ljava/lang/String;

.field private x:LX/ErG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2171535
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2171536
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->w:[Ljava/lang/String;

    .line 2171537
    new-instance v0, LX/EqP;

    invoke-direct {v0, p0}, LX/EqP;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->A:LX/1OX;

    .line 2171538
    new-instance v0, LX/EqQ;

    invoke-direct {v0, p0}, LX/EqQ;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->B:LX/EqQ;

    .line 2171539
    new-instance v0, LX/EqR;

    invoke-direct {v0, p0}, LX/EqR;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->C:LX/63W;

    return-void
.end method

.method public static synthetic a(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2171611
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2171595
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2171596
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2171597
    new-instance v1, LX/EqS;

    invoke-direct {v1, p0}, LX/EqS;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2171598
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const/4 v2, 0x1

    .line 2171599
    iput v2, v1, LX/108;->a:I

    .line 2171600
    move-object v1, v1

    .line 2171601
    const v2, 0x7f081ece

    invoke-virtual {p0, v2}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2171602
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2171603
    move-object v1, v1

    .line 2171604
    const/4 v2, -0x2

    .line 2171605
    iput v2, v1, LX/108;->h:I

    .line 2171606
    move-object v1, v1

    .line 2171607
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2171608
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2171609
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->C:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2171610
    return-void
.end method

.method private static a(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;LX/ErG;LX/0iA;LX/3kp;)V
    .locals 0

    .prologue
    .line 2171594
    iput-object p1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->x:LX/ErG;

    iput-object p2, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->y:LX/0iA;

    iput-object p3, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->z:LX/3kp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    const-class v0, LX/ErG;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/ErG;

    invoke-static {v2}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {v2}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v2

    check-cast v2, LX/3kp;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->a(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;LX/ErG;LX/0iA;LX/3kp;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2171583
    const v0, 0x7f0d0f28

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2171584
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2171585
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2171586
    const v0, 0x7f0d0f29

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2171587
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->r:Landroid/view/View;

    .line 2171588
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2171589
    new-instance v0, LX/ErH;

    invoke-direct {v0}, LX/ErH;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->t:LX/ErH;

    .line 2171590
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->t:LX/ErH;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2171591
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getBaseContext()Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2171592
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->A:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2171593
    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2171612
    const v0, 0x7f0d0f25

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    .line 2171613
    if-eqz p1, :cond_0

    .line 2171614
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, "NOTE_TEXT_KEY"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2171615
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/EqT;

    invoke-direct {v1, p0}, LX/EqT;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2171616
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 2171617
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0, v1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2171618
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    .line 2171572
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2171573
    const v1, 0x7f081ebd

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 2171574
    const v1, 0x7f081ebe

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2171575
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2171576
    const/4 v1, -0x1

    .line 2171577
    iput v1, v0, LX/0hs;->t:I

    .line 2171578
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b142c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2171579
    iput v1, v0, LX/0hs;->r:I

    .line 2171580
    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b142d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v2, v2, -0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbEditText;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbEditText;->getHeight()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 2171581
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2171582
    return-void
.end method

.method private m()Z
    .locals 3

    .prologue
    .line 2171570
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->y:LX/0iA;

    sget-object v1, LX/3l8;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3l8;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3l8;

    .line 2171571
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3l8;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4244"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2171569
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->q:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2171546
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2171547
    invoke-static {p0, p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2171548
    const v0, 0x7f030574

    invoke-virtual {p0, v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->setContentView(I)V

    .line 2171549
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->v:Ljava/lang/String;

    .line 2171550
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_events_guests_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171551
    invoke-virtual {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_events_guests_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->w:[Ljava/lang/String;

    .line 2171552
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->a()V

    .line 2171553
    invoke-direct {p0, p1}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->d(Landroid/os/Bundle;)V

    .line 2171554
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->b()V

    .line 2171555
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->x:LX/ErG;

    iget-object v1, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->v:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->w:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->B:LX/EqQ;

    .line 2171556
    new-instance v4, LX/ErF;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/ErF;-><init>(LX/03V;LX/0tX;LX/1Ck;Ljava/lang/String;Ljava/util/List;LX/EqQ;)V

    .line 2171557
    move-object v0, v4

    .line 2171558
    iput-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->s:LX/ErF;

    .line 2171559
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->s:LX/ErF;

    invoke-virtual {v0}, LX/ErF;->a()V

    .line 2171560
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->z:LX/3kp;

    const/4 v1, 0x2

    .line 2171561
    iput v1, v0, LX/3kp;->b:I

    .line 2171562
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->z:LX/3kp;

    sget-object v1, LX/7vb;->h:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2171563
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->z:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->u:Z

    if-nez v0, :cond_1

    .line 2171564
    invoke-direct {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->l()V

    .line 2171565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->u:Z

    .line 2171566
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->z:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171567
    iget-object v0, p0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->y:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4244"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171568
    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3843cb2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2171543
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2171544
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2171545
    const/16 v1, 0x23

    const v2, 0x75cacd32

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2171540
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2171541
    const-string v0, "NOTE_TEXT_KEY"

    invoke-static {p0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->n(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171542
    return-void
.end method
