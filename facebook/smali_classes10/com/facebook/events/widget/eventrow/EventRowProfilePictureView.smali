.class public Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/1aX;

.field private e:Landroid/net/Uri;

.field private f:Ljava/lang/String;

.field private g:Landroid/text/style/MetricAffectingSpan;

.field private h:Landroid/text/style/MetricAffectingSpan;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1972765
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1972766
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/util/AttributeSet;)V

    .line 1972767
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1972758
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1972759
    invoke-direct {p0, p2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/util/AttributeSet;)V

    .line 1972760
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1972761
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1972762
    invoke-direct {p0, p2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/util/AttributeSet;)V

    .line 1972763
    return-void
.end method

.method private a(IILX/0xr;)LX/DBJ;
    .locals 4

    .prologue
    .line 1972764
    new-instance v0, LX/DBJ;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v1, v2, p3, v3}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v1

    if-lez p1, :cond_0

    :goto_0
    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, LX/DBJ;-><init>(Landroid/graphics/Typeface;II)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 1972769
    const-class v0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    invoke-static {v0, p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1972770
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1972771
    const/4 v0, 0x3

    new-array v0, v0, [I

    const v1, 0x7f010706

    aput v1, v0, v3

    const v1, 0x7f010707

    aput v1, v0, v5

    const v1, 0x7f010708

    aput v1, v0, v6

    .line 1972772
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1972773
    :goto_0
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 1972774
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 1972775
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1972776
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1972777
    const v0, 0x7f0b15f3

    sget-object v5, LX/0xr;->MEDIUM:LX/0xr;

    invoke-direct {p0, v3, v0, v5}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(IILX/0xr;)LX/DBJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->g:Landroid/text/style/MetricAffectingSpan;

    .line 1972778
    const v0, 0x7f0b15f4

    sget-object v3, LX/0xr;->REGULAR:LX/0xr;

    invoke-direct {p0, v4, v0, v3}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(IILX/0xr;)LX/DBJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->h:Landroid/text/style/MetricAffectingSpan;

    .line 1972779
    if-nez v1, :cond_1

    const v0, 0x7f020664

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1972780
    :goto_1
    const v1, 0x7f0a06d4

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1972781
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1972782
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1972783
    move-object v1, v3

    .line 1972784
    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1972785
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    .line 1972786
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V

    .line 1972787
    return-void

    .line 1972788
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1972789
    goto :goto_1
.end method

.method private static a(Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;LX/6RZ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;",
            "LX/6RZ;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1972768
    iput-object p1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a:LX/6RZ;

    iput-object p2, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    const/16 v2, 0x509

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;LX/6RZ;LX/0Or;)V

    return-void
.end method

.method private setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1972751
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getPaddingLeft()I

    move-result v0

    .line 1972752
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getPaddingTop()I

    move-result v1

    .line 1972753
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getPaddingRight()I

    move-result v2

    .line 1972754
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->getPaddingBottom()I

    move-result v3

    .line 1972755
    invoke-virtual {p0, p1}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1972756
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setPadding(IIII)V

    .line 1972757
    return-void
.end method

.method private setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1972746
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->e:Landroid/net/Uri;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1972747
    :goto_0
    return-void

    .line 1972748
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->e:Landroid/net/Uri;

    .line 1972749
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1972750
    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method private setStartDate(Ljava/util/Date;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 1972736
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1972737
    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a:LX/6RZ;

    invoke-virtual {v1, p1}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1972738
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1972739
    iget-object v2, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1972740
    :goto_0
    return-void

    .line 1972741
    :cond_0
    iput-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->f:Ljava/lang/String;

    .line 1972742
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1972743
    iget-object v3, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->g:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1972744
    iget-object v3, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->h:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1972745
    invoke-virtual {p0, v2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1972732
    const-class v0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    invoke-static {v0, p3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1972733
    invoke-direct {p0, p1}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 1972734
    invoke-direct {p0, p2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setStartDate(Ljava/util/Date;)V

    .line 1972735
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1972729
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 1972730
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/net/Uri;Ljava/util/Date;Ljava/lang/String;)V

    .line 1972731
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7ac57dd0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1972716
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onAttachedToWindow()V

    .line 1972717
    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1972718
    const/16 v1, 0x2d

    const v2, -0x3d0fd6ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5c18b8f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1972726
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onDetachedFromWindow()V

    .line 1972727
    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1972728
    const/16 v1, 0x2d

    const v2, 0x277a1b72

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1972723
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onFinishTemporaryDetach()V

    .line 1972724
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1972725
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1972720
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onStartTemporaryDetach()V

    .line 1972721
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1972722
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1972719
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
