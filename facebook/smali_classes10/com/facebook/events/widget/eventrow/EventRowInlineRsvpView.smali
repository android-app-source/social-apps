.class public Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/events/model/Event;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/view/View$OnClickListener;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Landroid/view/View$OnClickListener;

.field public m:LX/DBM;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1972713
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1972714
    invoke-direct {p0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a()V

    .line 1972715
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1972710
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1972711
    invoke-direct {p0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a()V

    .line 1972712
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1972709
    new-instance v0, LX/DBK;

    invoke-direct {v0, p0, p1}, LX/DBK;-><init>(Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1972708
    new-instance v0, LX/DBL;

    invoke-direct {v0, p0, p1}, LX/DBL;-><init>(Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1972695
    const-class v0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    invoke-static {v0, p0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1972696
    const v0, 0x7f030510

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1972697
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->f:Landroid/view/View$OnClickListener;

    .line 1972698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->g:Landroid/view/View$OnClickListener;

    .line 1972699
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->h:Landroid/view/View$OnClickListener;

    .line 1972700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->i:Landroid/view/View$OnClickListener;

    .line 1972701
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->j:Landroid/view/View$OnClickListener;

    .line 1972702
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->k:Landroid/view/View$OnClickListener;

    .line 1972703
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->l:Landroid/view/View$OnClickListener;

    .line 1972704
    const v0, 0x7f0d0e64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1972705
    const v0, 0x7f0d0e65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1972706
    const v0, 0x7f0d0e66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1972707
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    invoke-static {v0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v0

    check-cast v0, LX/BiT;

    iput-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a:LX/BiT;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1972691
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972692
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972693
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972694
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 1972674
    iput-object p1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->b:Lcom/facebook/events/model/Event;

    .line 1972675
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1972676
    const/4 v0, 0x1

    move v0, v0

    .line 1972677
    if-eqz v0, :cond_0

    .line 1972678
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f6

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972679
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972680
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972681
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972682
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f7

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972683
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972684
    :goto_0
    return-void

    .line 1972685
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972686
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f5

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972687
    iget-object v0, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f9

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1972688
    invoke-direct {p0}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->b()V

    goto :goto_0
.end method

.method public setRsvpActionListener(LX/DBM;)V
    .locals 0

    .prologue
    .line 1972689
    iput-object p1, p0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->m:LX/DBM;

    .line 1972690
    return-void
.end method
