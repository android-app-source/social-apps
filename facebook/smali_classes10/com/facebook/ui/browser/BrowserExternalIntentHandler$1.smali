.class public final Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/D37;


# direct methods
.method public constructor <init>(LX/D37;J)V
    .locals 0

    .prologue
    .line 1959514
    iput-object p1, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->b:LX/D37;

    iput-wide p2, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1959515
    iget-object v0, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->b:LX/D37;

    iget-object v0, v0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1C0;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1959516
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fb4a_iab_core_feature"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1959517
    const-string v2, "iab_pref_disabled"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1959518
    iget-object v2, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->b:LX/D37;

    iget-object v2, v2, LX/D37;->l:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1959519
    iget-object v1, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->b:LX/D37;

    iget-object v1, v1, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1C0;->j:LX/0Tn;

    iget-wide v4, p0, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;->a:J

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1959520
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1959521
    return-void
.end method
