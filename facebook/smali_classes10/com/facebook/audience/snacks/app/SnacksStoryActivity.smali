.class public Lcom/facebook/audience/snacks/app/SnacksStoryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/AL7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2155211
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2155212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->q:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2155207
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2155208
    invoke-static {v0}, LX/470;->a(Landroid/view/Window;)V

    .line 2155209
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    invoke-static {v0, v1}, LX/470;->b(Landroid/view/Window;I)V

    .line 2155210
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;

    invoke-static {v0}, LX/AL7;->a(LX/0QB;)LX/AL7;

    move-result-object v0

    check-cast v0, LX/AL7;

    iput-object v0, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->p:LX/AL7;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2155204
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2155205
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2155206
    :cond_0
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    const v3, 0x1020002

    .line 2155195
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2155196
    new-instance v1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    invoke-direct {v1}, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;-><init>()V

    .line 2155197
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2155198
    move-object v0, v1

    .line 2155199
    iget-boolean v1, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->q:Z

    if-nez v1, :cond_0

    .line 2155200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->q:Z

    .line 2155201
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "snacks_bucket"

    invoke-virtual {v1, v3, v0, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2155202
    :goto_0
    return-void

    .line 2155203
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2155191
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2155192
    invoke-virtual {p0, p1}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->setIntent(Landroid/content/Intent;)V

    .line 2155193
    invoke-direct {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->l()V

    .line 2155194
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155213
    invoke-static {p0, p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2155214
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2155215
    invoke-direct {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->a()V

    .line 2155216
    invoke-direct {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->l()V

    .line 2155217
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2155188
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2155189
    const/4 v0, 0x0

    const v1, 0x7f040015

    invoke-virtual {p0, v0, v1}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->overridePendingTransition(II)V

    .line 2155190
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2155185
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2155186
    const/4 v0, 0x0

    const v1, 0x7f040015

    invoke-virtual {p0, v0, v1}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->overridePendingTransition(II)V

    .line 2155187
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x59e3d3db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155183
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2155184
    const/16 v1, 0x23

    const v2, 0x2e0c7b7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7e557f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155180
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2155181
    invoke-direct {p0}, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->b()V

    .line 2155182
    const/16 v1, 0x23

    const v2, 0x5d9f33a2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x559dbccf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155177
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2155178
    iget-object v1, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->p:LX/AL7;

    invoke-virtual {v1, p0}, LX/AL7;->a(Landroid/app/Activity;)V

    .line 2155179
    const/16 v1, 0x23

    const v2, -0x18ab25c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x30334fb0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155174
    iget-object v1, p0, Lcom/facebook/audience/snacks/app/SnacksStoryActivity;->p:LX/AL7;

    invoke-virtual {v1}, LX/AL7;->a()V

    .line 2155175
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2155176
    const/16 v1, 0x23

    const v2, 0x26fe3976

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
