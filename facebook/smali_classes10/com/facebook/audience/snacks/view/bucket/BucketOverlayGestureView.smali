.class public Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public a:LX/AKu;

.field public b:LX/AKt;

.field private c:LX/3rW;

.field private final d:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2155251
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2155252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155249
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155245
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155246
    new-instance v0, LX/Efi;

    invoke-direct {v0, p0}, LX/Efi;-><init>(Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->d:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 2155247
    new-instance v0, LX/3rW;

    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->d:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->c:LX/3rW;

    .line 2155248
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2155242
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->c:LX/3rW;

    invoke-virtual {v0, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2155243
    const/4 v0, 0x1

    .line 2155244
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x12589e96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2155226
    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    if-nez v2, :cond_0

    .line 2155227
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x659eae6

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2155228
    :goto_0
    return v0

    .line 2155229
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 2155230
    if-nez v2, :cond_1

    .line 2155231
    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    invoke-virtual {v2, v0}, LX/AKu;->a(Z)V

    .line 2155232
    :cond_1
    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    .line 2155233
    iget-boolean v3, v2, LX/AKu;->k:Z

    move v2, v3

    .line 2155234
    if-eqz v2, :cond_2

    .line 2155235
    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    invoke-virtual {v2, p1}, LX/AKu;->a(Landroid/view/MotionEvent;)V

    .line 2155236
    const v2, 0x6d11ffa0

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2155237
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x17ee3fa1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setEventListener(LX/AKt;)V
    .locals 0

    .prologue
    .line 2155240
    iput-object p1, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->b:LX/AKt;

    .line 2155241
    return-void
.end method

.method public setGestureController(LX/AKu;)V
    .locals 0

    .prologue
    .line 2155238
    iput-object p1, p0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    .line 2155239
    return-void
.end method
