.class public Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/Efy;

.field private final f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2155930
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2155931
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155928
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155929
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155923
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155924
    new-instance v0, LX/Eg7;

    invoke-direct {v0, p0}, LX/Eg7;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->f:Landroid/view/View$OnClickListener;

    .line 2155925
    const-class v0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-static {v0, p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2155926
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->c()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->d:LX/0Px;

    .line 2155927
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2155902
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->removeAllViews()V

    .line 2155903
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2155904
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2155905
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 2155906
    iget v3, v0, LX/1zt;->e:I

    move v3, v3

    .line 2155907
    if-eqz v3, :cond_0

    .line 2155908
    iget v3, v0, LX/1zt;->e:I

    move v0, v3

    .line 2155909
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2155910
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2155911
    const/16 v0, 0x11

    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2155912
    new-instance v4, LX/EgC;

    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EgC;-><init>(Landroid/content/Context;)V

    .line 2155913
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 2155914
    iget v5, v0, LX/1zt;->e:I

    move v5, v5

    .line 2155915
    iput v5, v4, LX/EgC;->b:I

    .line 2155916
    iget-object v5, v0, LX/1zt;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2155917
    iput-object v5, v4, LX/EgC;->c:Ljava/lang/String;

    .line 2155918
    invoke-virtual {v0}, LX/1zt;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/EgC;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2155919
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, LX/EgC;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2155920
    invoke-virtual {p0, v4, v3}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2155921
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2155922
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v0

    check-cast v0, LX/1zf;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a:LX/1zf;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2155932
    invoke-static {p1}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x44e10000    # 1800.0f

    .line 2155933
    const/4 p1, 0x1

    iput-boolean p1, v0, LX/7hT;->w:Z

    .line 2155934
    iput v1, v0, LX/7hT;->x:F

    .line 2155935
    iput v2, v0, LX/7hT;->y:F

    .line 2155936
    move-object v0, v0

    .line 2155937
    new-instance v1, LX/EgA;

    invoke-direct {v1, p0}, LX/EgA;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;)V

    .line 2155938
    iput-object v1, v0, LX/7hT;->e:LX/7hO;

    .line 2155939
    move-object v0, v0

    .line 2155940
    invoke-virtual {v0}, LX/7hT;->d()LX/7hT;

    .line 2155941
    return-void
.end method

.method public static b(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;II)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2155885
    iput p1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->b:I

    .line 2155886
    iput p2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->c:I

    .line 2155887
    invoke-virtual {p0, v1}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2155888
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    div-int/lit8 v2, v0, 0x2

    .line 2155889
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getChildCount()I

    move-result v3

    move v0, v1

    .line 2155890
    :goto_0
    if-ge v0, v3, :cond_0

    .line 2155891
    invoke-virtual {p0, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2155892
    invoke-static {v1}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v4

    invoke-virtual {v4}, LX/7hT;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2155893
    :cond_0
    return-void

    .line 2155894
    :cond_1
    int-to-float v4, v0

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    int-to-double v6, v3

    div-double/2addr v4, v6

    double-to-float v4, v4

    .line 2155895
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 2155896
    int-to-float v5, p1

    int-to-float v6, v2

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    .line 2155897
    int-to-float v6, p2

    int-to-float v7, v2

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v4, v8

    mul-float/2addr v4, v7

    add-float/2addr v4, v6

    .line 2155898
    invoke-static {v1}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v6

    invoke-virtual {v6}, LX/7hT;->e()LX/7hT;

    move-result-object v6

    int-to-float v7, p1

    invoke-virtual {v6, v7, v5}, LX/7hT;->a(FF)LX/7hT;

    move-result-object v6

    int-to-float v7, p2

    invoke-virtual {v6, v7, v4}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v6

    invoke-virtual {v6}, LX/7hT;->d()LX/7hT;

    .line 2155899
    invoke-static {v1}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v6

    new-instance v7, LX/Eg9;

    invoke-direct {v7, p0, v1, v5, v4}, LX/Eg9;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;Landroid/view/View;FF)V

    .line 2155900
    iput-object v7, v6, LX/7hT;->e:LX/7hO;

    .line 2155901
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2155871
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getChildCount()I

    move-result v1

    .line 2155872
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2155873
    invoke-virtual {p0, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2155874
    invoke-static {v2}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v3

    invoke-virtual {v3}, LX/7hT;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2155875
    :cond_0
    return-void

    .line 2155876
    :cond_1
    invoke-static {v2}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v3

    new-instance v4, LX/EgB;

    invoke-direct {v4, p0}, LX/EgB;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;)V

    .line 2155877
    iput-object v4, v3, LX/7hT;->e:LX/7hO;

    .line 2155878
    invoke-static {v2}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v4

    iget v5, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->b:I

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, LX/7hT;->a(FF)LX/7hT;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v2

    iget v4, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->c:I

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2155879
    const/4 v5, 0x1

    iput-boolean v5, v2, LX/7hT;->t:Z

    .line 2155880
    iput v3, v2, LX/7hT;->u:F

    .line 2155881
    iput v4, v2, LX/7hT;->v:F

    .line 2155882
    move-object v2, v2

    .line 2155883
    invoke-virtual {v2}, LX/7hT;->d()LX/7hT;

    .line 2155884
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2155867
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->setVisibility(I)V

    .line 2155868
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->d:LX/0Px;

    invoke-direct {p0, v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a(LX/0Px;)V

    .line 2155869
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/Eg8;

    invoke-direct {v1, p0, p1, p2}, LX/Eg8;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2155870
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2155866
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2155863
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use setOnReactionClickListener instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnReactionClickListener(LX/Efy;)V
    .locals 0

    .prologue
    .line 2155864
    iput-object p1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->e:LX/Efy;

    .line 2155865
    return-void
.end method
