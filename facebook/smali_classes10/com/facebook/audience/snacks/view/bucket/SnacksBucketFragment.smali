.class public Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/AK0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/support/v4/view/ViewPager;

.field private c:Landroid/view/View;

.field private d:Lcom/facebook/resources/ui/FbFrameLayout;

.field public e:I

.field private f:LX/AKu;

.field public g:LX/Efq;

.field private final h:LX/3sJ;

.field private final i:LX/Efs;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2155517
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2155518
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    .line 2155519
    new-instance v0, LX/Efr;

    invoke-direct {v0, p0}, LX/Efr;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->h:LX/3sJ;

    .line 2155520
    new-instance v0, LX/Efs;

    invoke-direct {v0, p0}, LX/Efs;-><init>(Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->i:LX/Efs;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    invoke-static {p0}, LX/AK0;->a(LX/0QB;)LX/AK0;

    move-result-object p0

    check-cast p0, LX/AK0;

    iput-object p0, p1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->a:LX/AK0;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155446
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2155447
    const-class v0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    invoke-static {v0, p0}, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2155448
    const/4 v0, 0x0

    .line 2155449
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2155450
    if-eqz v1, :cond_1

    .line 2155451
    const-string p1, "extra_snack_is_my_snack"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2155452
    const-string p1, "extra_snack_is_my_snack"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->k:Z

    .line 2155453
    :cond_0
    iget-boolean p1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->k:Z

    if-eqz p1, :cond_2

    .line 2155454
    iput v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    .line 2155455
    :cond_1
    :goto_0
    return-void

    .line 2155456
    :cond_2
    const-string p1, "extra_snack_user_thread_id"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2155457
    const-string p1, "extra_snack_user_thread_id"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->j:Ljava/lang/String;

    .line 2155458
    :cond_3
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->a:LX/AK0;

    .line 2155459
    iget-object p1, v1, LX/AK0;->g:LX/AJv;

    .line 2155460
    iget-object v1, p1, LX/AJv;->a:LX/0Px;

    move-object p1, v1

    .line 2155461
    move-object v1, p1

    .line 2155462
    iput-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->l:LX/0Px;

    .line 2155463
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->j:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 2155464
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->l:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155465
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2155466
    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->j:Ljava/lang/String;

    goto :goto_0

    :cond_4
    move v1, v0

    .line 2155467
    :goto_1
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2155468
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155469
    iget-object p1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, p1

    .line 2155470
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object p1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2155471
    iput v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    goto :goto_0

    .line 2155472
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7f442b3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155516
    const v1, 0x7f031373

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x71cbf43f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4489112f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155506
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    .line 2155507
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    iget-object v2, v1, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v4, v2, :cond_1

    .line 2155508
    iget-object v2, v1, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Efp;

    .line 2155509
    if-eqz v2, :cond_0

    .line 2155510
    invoke-virtual {v2}, LX/Efp;->b()V

    .line 2155511
    iget-object v5, v2, LX/Efp;->b:LX/Efo;

    .line 2155512
    invoke-virtual {v5}, LX/Efo;->f()V

    .line 2155513
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2155514
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2155515
    const/16 v1, 0x2b

    const v2, -0x6db64234

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x48ce8714

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155499
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    .line 2155500
    iget v2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    move v2, v2

    .line 2155501
    iget-object v4, v1, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Efp;

    .line 2155502
    if-eqz v4, :cond_0

    .line 2155503
    invoke-virtual {v4}, LX/Efp;->b()V

    .line 2155504
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2155505
    const/16 v1, 0x2b

    const v2, -0x50daef0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x22a0bb49

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2155492
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2155493
    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    .line 2155494
    iget v2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    move v2, v2

    .line 2155495
    iget-object p0, v1, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efp;

    .line 2155496
    if-eqz p0, :cond_0

    .line 2155497
    invoke-virtual {p0}, LX/Efp;->a()V

    .line 2155498
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4cc4ffb6    # 1.03284144E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155473
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2155474
    const v0, 0x7f0d2cf5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    .line 2155475
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2155476
    const v0, 0x7f0d2ced

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->c:Landroid/view/View;

    .line 2155477
    const v0, 0x7f0d2cf4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->d:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2155478
    new-instance v0, LX/AKu;

    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->d:Lcom/facebook/resources/ui/FbFrameLayout;

    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v1, v2, v3}, LX/AKu;-><init>(Lcom/facebook/resources/ui/FbFrameLayout;Landroid/view/View;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->f:LX/AKu;

    .line 2155479
    iget-boolean v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->k:Z

    if-eqz v0, :cond_0

    .line 2155480
    new-instance v0, LX/Eg2;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->a:LX/AK0;

    .line 2155481
    iget-object v3, v2, LX/AK0;->g:LX/AJv;

    .line 2155482
    iget-object v2, v3, LX/AJv;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    move-object v3, v2

    .line 2155483
    move-object v2, v3

    .line 2155484
    iget-object v3, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->i:LX/Efs;

    iget-object v4, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->f:LX/AKu;

    invoke-direct {v0, v1, v2, v3, v4}, LX/Eg2;-><init>(Landroid/content/Context;Lcom/facebook/audience/snacks/model/SnacksMyUserThread;LX/Efs;LX/AKu;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    .line 2155485
    :goto_0
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2155486
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->h:LX/3sJ;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2155487
    iget-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    .line 2155488
    iget v1, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    move v1, v1

    .line 2155489
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2155490
    return-void

    .line 2155491
    :cond_0
    new-instance v0, LX/Eft;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->l:LX/0Px;

    iget-object v3, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->i:LX/Efs;

    iget-object v4, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->f:LX/AKu;

    invoke-direct {v0, v1, v2, v3, v4}, LX/Eft;-><init>(Landroid/content/Context;LX/0Px;LX/Efs;LX/AKu;)V

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    goto :goto_0
.end method
