.class public Lcom/facebook/audience/direct/app/SnacksInboxFragment;
.super Lcom/facebook/ui/drawers/DrawerContentFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->SNACKS_INBOX_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0gI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7hN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0he;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field public h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

.field private i:Z

.field private final j:LX/Ef4;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2153785
    invoke-direct {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;-><init>()V

    .line 2153786
    new-instance v0, LX/Ef4;

    invoke-direct {v0, p0}, LX/Ef4;-><init>(Lcom/facebook/audience/direct/app/SnacksInboxFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->j:LX/Ef4;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/audience/direct/app/SnacksInboxFragment;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AFW;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2153745
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->i:Z

    if-nez v1, :cond_1

    .line 2153746
    :cond_0
    :goto_0
    return-void

    .line 2153747
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    .line 2153748
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFW;

    .line 2153749
    iget-object v5, v0, LX/AFW;->a:LX/7h0;

    move-object v0, v5

    .line 2153750
    iget-boolean v5, v0, LX/7h0;->h:Z

    move v0, v5

    .line 2153751
    if-nez v0, :cond_5

    .line 2153752
    add-int/lit8 v0, v1, 0x1

    .line 2153753
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 2153754
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->g:Z

    if-nez v0, :cond_4

    .line 2153755
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->g:Z

    .line 2153756
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    .line 2153757
    iget-object v2, v0, LX/0he;->j:LX/0hf;

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/0he;->j:LX/0hf;

    sget-object v4, LX/0hf;->UNKNOWN:LX/0hf;

    if-ne v2, v4, :cond_6

    .line 2153758
    :cond_3
    :goto_3
    goto :goto_0

    .line 2153759
    :cond_4
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    .line 2153760
    iget-boolean v2, v0, LX/0he;->n:Z

    if-nez v2, :cond_9

    .line 2153761
    :goto_4
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2

    .line 2153762
    :cond_6
    iput v1, v0, LX/0he;->m:I

    .line 2153763
    iput v3, v0, LX/0he;->l:I

    .line 2153764
    iget-object v2, v0, LX/0he;->h:LX/7gT;

    iget v4, v0, LX/0he;->l:I

    iget v5, v0, LX/0he;->m:I

    iget v6, v0, LX/0he;->k:I

    iget-object p0, v0, LX/0he;->i:LX/7gU;

    invoke-virtual {p0}, LX/7gU;->getGesture()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v0, LX/0he;->j:LX/0hf;

    if-eqz p1, :cond_7

    iget-object p1, v0, LX/0he;->j:LX/0hf;

    invoke-virtual {p1}, LX/0hf;->getSource()Ljava/lang/String;

    move-result-object p1

    .line 2153765
    :goto_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2153766
    sget-object v3, LX/7gS;->NUMBER_OF_ROWS:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2153767
    sget-object v3, LX/7gS;->NUMBER_OF_UNSEEN_ROWS:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2153768
    sget-object v3, LX/7gS;->BADGING_NUMBER:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2153769
    sget-object v3, LX/7gS;->GESTURE:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2153770
    sget-object v3, LX/7gS;->SOURCE:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2153771
    sget-object v3, LX/7gR;->OPEN_DIRECT:LX/7gR;

    invoke-static {v2, v3, v1}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 2153772
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v5, v0, LX/0he;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x1

    iget v5, v0, LX/0he;->m:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    iget v5, v0, LX/0he;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    iget-object v5, v0, LX/0he;->i:LX/7gU;

    invoke-virtual {v5}, LX/7gU;->getGesture()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x4

    iget-object v2, v0, LX/0he;->j:LX/0hf;

    if-eqz v2, :cond_8

    iget-object v2, v0, LX/0he;->j:LX/0hf;

    invoke-virtual {v2}, LX/0hf;->getSource()Ljava/lang/String;

    move-result-object v2

    :goto_6
    aput-object v2, v4, v5

    .line 2153773
    sget-object v2, LX/0hf;->UNKNOWN:LX/0hf;

    iput-object v2, v0, LX/0he;->j:LX/0hf;

    goto/16 :goto_3

    .line 2153774
    :cond_7
    const/4 p1, 0x0

    goto :goto_5

    .line 2153775
    :cond_8
    const-string v2, ""

    goto :goto_6

    .line 2153776
    :cond_9
    iput v1, v0, LX/0he;->m:I

    .line 2153777
    iput v3, v0, LX/0he;->l:I

    .line 2153778
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0he;->n:Z

    .line 2153779
    iget-object v2, v0, LX/0he;->h:LX/7gT;

    iget v4, v0, LX/0he;->l:I

    iget v5, v0, LX/0he;->m:I

    .line 2153780
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2153781
    sget-object v1, LX/7gS;->NUMBER_OF_ROWS:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2153782
    sget-object v1, LX/7gS;->NUMBER_OF_UNSEEN_ROWS:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2153783
    sget-object v1, LX/7gR;->REFRESH_DIRECT:LX/7gR;

    invoke-static {v2, v1, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 2153784
    goto/16 :goto_4
.end method

.method public static m(Lcom/facebook/audience/direct/app/SnacksInboxFragment;)V
    .locals 2

    .prologue
    .line 2153741
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    .line 2153742
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    .line 2153743
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0he;->n:Z

    .line 2153744
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2153734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->i:Z

    .line 2153735
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2153736
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    .line 2153737
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    .line 2153738
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    .line 2153739
    :goto_0
    return-void

    .line 2153740
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    goto :goto_0
.end method

.method private o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2153676
    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->i:Z

    .line 2153677
    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->g:Z

    .line 2153678
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2153731
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->a(Landroid/os/Bundle;)V

    .line 2153732
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    invoke-static {v0}, LX/0gI;->a(LX/0QB;)LX/0gI;

    move-result-object v3

    check-cast v3, LX/0gI;

    invoke-static {v0}, LX/7hN;->b(LX/0QB;)LX/7hN;

    move-result-object v4

    check-cast v4, LX/7hN;

    invoke-static {v0}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v5

    check-cast v5, LX/0he;

    const/16 p1, 0x122d

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object v0

    check-cast v0, LX/0hg;

    iput-object v3, v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    iput-object v4, v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->b:LX/7hN;

    iput-object v5, v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    iput-object p1, v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->d:LX/0Or;

    iput-object v0, v2, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->e:LX/0hg;

    .line 2153733
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2153727
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    invoke-virtual {v0}, LX/0he;->a()V

    .line 2153728
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->c()V

    .line 2153729
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->n()V

    .line 2153730
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2153723
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->o()V

    .line 2153724
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->d()V

    .line 2153725
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    invoke-virtual {v0}, LX/0he;->b()V

    .line 2153726
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2153718
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onAttach(Landroid/content/Context;)V

    .line 2153719
    instance-of v0, p1, LX/0fD;

    if-eqz v0, :cond_0

    .line 2153720
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->f:Z

    .line 2153721
    :goto_0
    return-void

    .line 2153722
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->f:Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x28ce5a25

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153717
    const v1, 0x7f03136a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1bd32a5b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7254214e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153709
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->e:LX/0hg;

    .line 2153710
    iget-object v2, v1, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0xbc0006

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2153711
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    iget-object v2, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->j:LX/Ef4;

    .line 2153712
    iget-object v4, v1, LX/0gI;->e:LX/7gh;

    invoke-virtual {v4, v2}, LX/7gh;->b(Ljava/lang/Object;)V

    .line 2153713
    iget-boolean v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->f:Z

    if-nez v1, :cond_0

    .line 2153714
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    invoke-virtual {v1}, LX/0he;->b()V

    .line 2153715
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroy()V

    .line 2153716
    const/16 v1, 0x2b

    const v2, -0x3cbc8d86

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47af240f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153706
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->o()V

    .line 2153707
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onPause()V

    .line 2153708
    const/16 v1, 0x2b

    const v2, 0x30557437

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x41295efa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153703
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onResume()V

    .line 2153704
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->n()V

    .line 2153705
    const/16 v1, 0x2b

    const v2, -0x759c2c8a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x7
    .end annotation

    .prologue
    .line 2153679
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2153680
    const v0, 0x7f0d2cdd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    .line 2153681
    iget-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->f:Z

    if-eqz v0, :cond_0

    .line 2153682
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    new-instance v1, LX/Ef6;

    invoke-direct {v1, p0}, LX/Ef6;-><init>(Lcom/facebook/audience/direct/app/SnacksInboxFragment;)V

    .line 2153683
    iput-object v1, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->k:LX/Ef5;

    .line 2153684
    :goto_0
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->c:LX/0he;

    invoke-virtual {v0}, LX/0he;->a()V

    .line 2153685
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a:LX/0gI;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->j:LX/Ef4;

    .line 2153686
    iget-object p0, v0, LX/0gI;->e:LX/7gh;

    invoke-virtual {p0, v1}, LX/7gh;->a(Ljava/lang/Object;)V

    .line 2153687
    return-void

    .line 2153688
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a()V

    .line 2153689
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->b:LX/7hN;

    const v1, 0x7f082aa7

    const/4 v2, -0x1

    const/4 p1, 0x0

    const/4 p2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, LX/7hN;->a(IIZLX/107;)Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2153690
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    new-instance v1, LX/Ef7;

    invoke-direct {v1, p0}, LX/Ef7;-><init>(Lcom/facebook/audience/direct/app/SnacksInboxFragment;)V

    .line 2153691
    iput-object v1, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->k:LX/Ef5;

    .line 2153692
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->e:LX/0hg;

    .line 2153693
    iget-boolean v1, v0, LX/0hg;->g:Z

    if-eqz v1, :cond_1

    .line 2153694
    iget-object v1, v0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xbc0003

    const/4 p1, 0x2

    invoke-interface {v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2153695
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0hg;->g:Z

    .line 2153696
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2153697
    if-eqz v1, :cond_2

    const-string v0, "extra_sharesheet_survey_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "extra_sharesheet_integration_point_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2153698
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v2, "extra_sharesheet_integration_point_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2153699
    iput-object v2, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2153700
    move-object v0, v0

    .line 2153701
    const-string v2, "extra_sharesheet_survey_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/os/Bundle;)LX/0gt;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2153702
    :cond_2
    goto :goto_0
.end method
