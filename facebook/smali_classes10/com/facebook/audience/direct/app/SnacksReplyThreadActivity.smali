.class public Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/0he;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/AL7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2153792
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2153793
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->r:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2153836
    invoke-virtual {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2153837
    invoke-static {v0}, LX/470;->a(Landroid/view/Window;)V

    .line 2153838
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    invoke-static {v0, v1}, LX/470;->b(Landroid/view/Window;I)V

    .line 2153839
    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;LX/0he;LX/AL7;)V
    .locals 0

    .prologue
    .line 2153835
    iput-object p1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->p:LX/0he;

    iput-object p2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->q:LX/AL7;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;

    invoke-static {v1}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v0

    check-cast v0, LX/0he;

    invoke-static {v1}, LX/AL7;->a(LX/0QB;)LX/AL7;

    move-result-object v1

    check-cast v1, LX/AL7;

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->a(Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;LX/0he;LX/AL7;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2153832
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2153833
    invoke-virtual {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2153834
    :cond_0
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    const v3, 0x1020002

    .line 2153822
    invoke-virtual {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->q:LX/AL7;

    .line 2153823
    new-instance v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    invoke-direct {v2}, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;-><init>()V

    .line 2153824
    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2153825
    iput-object v1, v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->l:LX/AL7;

    .line 2153826
    move-object v0, v2

    .line 2153827
    iget-boolean v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->r:Z

    if-nez v1, :cond_0

    .line 2153828
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->r:Z

    .line 2153829
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "snacks_reply_thread"

    invoke-virtual {v1, v3, v0, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2153830
    :goto_0
    return-void

    .line 2153831
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2153818
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2153819
    invoke-virtual {p0, p1}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->setIntent(Landroid/content/Intent;)V

    .line 2153820
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->l()V

    .line 2153821
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2153813
    invoke-static {p0, p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2153814
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2153815
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->a()V

    .line 2153816
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->l()V

    .line 2153817
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2153810
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2153811
    const/4 v0, 0x0

    const v1, 0x7f040015

    invoke-virtual {p0, v0, v1}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->overridePendingTransition(II)V

    .line 2153812
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2153807
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2153808
    const/4 v0, 0x0

    const v1, 0x7f040015

    invoke-virtual {p0, v0, v1}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->overridePendingTransition(II)V

    .line 2153809
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1d6ac38

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153803
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->p:LX/0he;

    .line 2153804
    sget-object v2, LX/0hf;->ROW:LX/0hf;

    iput-object v2, v1, LX/0he;->j:LX/0hf;

    .line 2153805
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2153806
    const/16 v1, 0x23

    const v2, 0x3e872282

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7b958520

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153800
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2153801
    invoke-direct {p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->b()V

    .line 2153802
    const/16 v1, 0x23

    const v2, 0x25a6e298

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x70653c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153797
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2153798
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->q:LX/AL7;

    invoke-virtual {v1, p0}, LX/AL7;->a(Landroid/app/Activity;)V

    .line 2153799
    const/16 v1, 0x23

    const v2, -0x4201f37e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x79cb1c31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153794
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadActivity;->q:LX/AL7;

    invoke-virtual {v1}, LX/AL7;->a()V

    .line 2153795
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2153796
    const/16 v1, 0x23

    const v2, 0x5101f81f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
