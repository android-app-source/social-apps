.class public Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/0he;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/support/v4/view/ViewPager;

.field private d:Landroid/view/View;

.field private e:Lcom/facebook/resources/ui/FbFrameLayout;

.field public f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/MinimalReplyThread;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field private i:LX/AKu;

.field public j:LX/Efh;

.field public k:Lcom/facebook/audience/model/ReplyThreadConfig;

.field public l:LX/AL7;

.field private final m:LX/3sJ;

.field private final n:LX/EfA;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2153862
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2153863
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2153864
    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    .line 2153865
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    .line 2153866
    new-instance v0, LX/Ef9;

    invoke-direct {v0, p0}, LX/Ef9;-><init>(Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->m:LX/3sJ;

    .line 2153867
    new-instance v0, LX/EfA;

    invoke-direct {v0, p0}, LX/EfA;-><init>(Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->n:LX/EfA;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    invoke-static {p0}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v1

    check-cast v1, LX/0he;

    invoke-static {p0}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object p0

    check-cast p0, LX/0hg;

    iput-object v1, p1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a:LX/0he;

    iput-object p0, p1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->b:LX/0hg;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2153920
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2153921
    const-class v0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    invoke-static {v0, p0}, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2153922
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2153923
    if-eqz v0, :cond_2

    .line 2153924
    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2153925
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-static {}, Lcom/facebook/audience/model/MinimalReplyThread;->newBuilder()LX/7gm;

    move-result-object v2

    const-string p1, "id"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2153926
    iput-object p1, v2, LX/7gm;->a:Ljava/lang/String;

    .line 2153927
    move-object v2, v2

    .line 2153928
    invoke-virtual {v2}, LX/7gm;->a()Lcom/facebook/audience/model/MinimalReplyThread;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    .line 2153929
    :cond_0
    const-string v1, "extra_reply_threads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2153930
    const-string v1, "extra_reply_threads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2153931
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2153932
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    .line 2153933
    :cond_1
    const-string v1, "extra_reply_thread_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2153934
    const-string v1, "extra_reply_thread_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/ReplyThreadConfig;

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->k:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2153935
    :cond_2
    :goto_0
    return-void

    .line 2153936
    :cond_3
    const/4 v2, 0x1

    .line 2153937
    invoke-static {}, Lcom/facebook/audience/model/ReplyThreadConfig;->newBuilder()LX/7gu;

    move-result-object v0

    .line 2153938
    iput-boolean v2, v0, LX/7gu;->a:Z

    .line 2153939
    move-object v0, v0

    .line 2153940
    const/4 v1, 0x0

    .line 2153941
    iput-boolean v1, v0, LX/7gu;->b:Z

    .line 2153942
    move-object v0, v0

    .line 2153943
    iput-boolean v2, v0, LX/7gu;->c:Z

    .line 2153944
    move-object v0, v0

    .line 2153945
    sget-object v1, LX/7gw;->DIRECT:LX/7gw;

    .line 2153946
    iput-object v1, v0, LX/7gu;->d:LX/7gw;

    .line 2153947
    move-object v0, v0

    .line 2153948
    invoke-virtual {v0}, LX/7gu;->a()Lcom/facebook/audience/model/ReplyThreadConfig;

    move-result-object v0

    move-object v0, v0

    .line 2153949
    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->k:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2153950
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a:LX/0he;

    .line 2153951
    sget-object v1, LX/7gU;->CLICK:LX/7gU;

    iput-object v1, v0, LX/0he;->i:LX/7gU;

    .line 2153952
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a:LX/0he;

    .line 2153953
    sget-object v1, LX/0hf;->NOTIFICATION:LX/0hf;

    iput-object v1, v0, LX/0he;->j:LX/0hf;

    .line 2153954
    sget-object v1, LX/7gV;->DIRECT:LX/7gV;

    iput-object v1, v0, LX/0he;->g:LX/7gV;

    .line 2153955
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6249d05f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153908
    const v1, 0x7f031370

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x392ae5ee

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56021846

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153909
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    .line 2153910
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    iget-object v2, v1, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v4, v2, :cond_1

    .line 2153911
    iget-object v2, v1, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Efg;

    .line 2153912
    if-eqz v2, :cond_0

    .line 2153913
    invoke-virtual {v2}, LX/Efg;->b()V

    .line 2153914
    iget-object v5, v2, LX/Efg;->b:LX/EfL;

    invoke-virtual {v5}, LX/EfL;->d()V

    .line 2153915
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2153916
    :cond_1
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->b:LX/0hg;

    .line 2153917
    iget-object v2, v1, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0xbc0005

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2153918
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2153919
    const/16 v1, 0x2b

    const v2, -0x7b54cd5b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c7afc59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153900
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    .line 2153901
    iget v2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v2, v2

    .line 2153902
    iget-object v4, v1, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Efg;

    .line 2153903
    if-eqz v4, :cond_0

    .line 2153904
    invoke-virtual {v4}, LX/Efg;->b()V

    .line 2153905
    iget-object v1, v4, LX/Efg;->b:LX/EfL;

    invoke-virtual {v1}, LX/EfL;->e()V

    .line 2153906
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2153907
    const/16 v1, 0x2b

    const v2, -0x8394da2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x576238b4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153893
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2153894
    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    .line 2153895
    iget v2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v2, v2

    .line 2153896
    iget-object p0, v1, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efg;

    .line 2153897
    if-eqz p0, :cond_0

    .line 2153898
    invoke-virtual {p0}, LX/Efg;->a()V

    .line 2153899
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x10374938

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2153868
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2153869
    const v0, 0x7f0d2cef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    .line 2153870
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2153871
    const v0, 0x7f0d2ced

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->d:Landroid/view/View;

    .line 2153872
    const v0, 0x7f0d2cee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->e:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2153873
    new-instance v0, LX/AKu;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->e:Lcom/facebook/resources/ui/FbFrameLayout;

    iget-object v2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->d:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v1, v2, v3}, LX/AKu;-><init>(Lcom/facebook/resources/ui/FbFrameLayout;Landroid/view/View;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->i:LX/AKu;

    .line 2153874
    const v0, 0x7f0d2cf0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    .line 2153875
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2153876
    iget-boolean v1, v0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    move v0, v1

    .line 2153877
    if-nez v0, :cond_1

    .line 2153878
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setVisibility(I)V

    .line 2153879
    :goto_0
    new-instance v0, LX/Efh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    iget-object v3, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->n:LX/EfA;

    iget-object v4, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->i:LX/AKu;

    iget-object v5, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->l:LX/AL7;

    iget-object v6, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->k:Lcom/facebook/audience/model/ReplyThreadConfig;

    invoke-direct/range {v0 .. v6}, LX/Efh;-><init>(Landroid/content/Context;LX/0Px;LX/EfA;LX/AKu;LX/AL7;Lcom/facebook/audience/model/ReplyThreadConfig;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    .line 2153880
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a:LX/0he;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2153881
    iget v2, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v2, v2

    .line 2153882
    invoke-virtual {v0, v1, v2}, LX/0he;->c(II)V

    .line 2153883
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2153884
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->m:LX/3sJ;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2153885
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    .line 2153886
    iget v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v1, v1

    .line 2153887
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2153888
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    .line 2153889
    iget v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v1, v1

    .line 2153890
    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setCurrentSegmentIndex(I)V

    .line 2153891
    return-void

    .line 2153892
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iget-object v1, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a(I)V

    goto :goto_0
.end method
