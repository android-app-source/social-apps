.class public Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final A:LX/0he;

.field private final B:LX/1xf;

.field public final C:LX/AFQ;

.field public final D:LX/0fO;

.field private final E:LX/AFJ;

.field public final F:LX/AFO;

.field public final G:LX/33B;

.field private m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public o:Lcom/facebook/resources/ui/FbTextView;

.field private p:Lcom/facebook/fbui/glyph/GlyphView;

.field private q:Lcom/facebook/resources/ui/FbTextView;

.field private r:Lcom/facebook/fbui/glyph/GlyphView;

.field private s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Lcom/facebook/resources/ui/FbTextView;

.field public u:Lcom/facebook/fbui/glyph/GlyphView;

.field public v:LX/1Ad;

.field public w:LX/AFW;

.field public x:Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;

.field private y:Lcom/facebook/resources/ui/FbTextView;

.field public z:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2154764
    const-class v0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/1xf;LX/AFQ;LX/1Ad;LX/0fO;LX/AFJ;LX/0he;LX/AFO;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2154741
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2154742
    new-instance v0, LX/EfR;

    invoke-direct {v0, p0}, LX/EfR;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->G:LX/33B;

    .line 2154743
    iput-object p2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->B:LX/1xf;

    .line 2154744
    iput-object p3, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->C:LX/AFQ;

    .line 2154745
    iput-object p5, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->D:LX/0fO;

    .line 2154746
    iput-object p4, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->v:LX/1Ad;

    .line 2154747
    const v0, 0x7f0d2cd0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2154748
    const v0, 0x7f0d2cd1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2154749
    const v0, 0x7f0d2cd8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2154750
    const v0, 0x7f0d2cd9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2154751
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2154752
    const v0, 0x7f0d2cd3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2154753
    const v0, 0x7f0d2cd4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2154754
    const v0, 0x7f0d2cd5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2154755
    const v0, 0x7f0d2cd6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2154756
    const v0, 0x7f0d2cdb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2154757
    const v0, 0x7f0d2cd2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->x:Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;

    .line 2154758
    const v0, 0x7f0d2cd7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 2154759
    const v0, 0x7f0d2cda

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->z:Landroid/view/View;

    .line 2154760
    iput-object p6, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->E:LX/AFJ;

    .line 2154761
    iput-object p7, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->A:LX/0he;

    .line 2154762
    iput-object p8, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->F:LX/AFO;

    .line 2154763
    return-void
.end method

.method public static varargs a(I[Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2154737
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 2154738
    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2154739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2154740
    :cond_0
    return-void
.end method

.method private static varargs a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V
    .locals 4

    .prologue
    .line 2154732
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2154733
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 2154734
    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2154735
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2154736
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;)V
    .locals 11

    .prologue
    const/4 v8, 0x3

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2154690
    iget-object v0, p1, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 2154691
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2154692
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2154693
    iget-object v0, p1, LX/7h0;->b:LX/7gy;

    move-object v0, v0

    .line 2154694
    sget-object v1, LX/7gy;->INCOMING_REPLY:LX/7gy;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/7gy;->INCOMING_STORY:LX/7gy;

    if-ne v0, v1, :cond_2

    .line 2154695
    :cond_0
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v6

    invoke-static {v7, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154696
    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v4

    invoke-static {v4, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154697
    iget-boolean v0, p1, LX/7h0;->h:Z

    move v0, v0

    .line 2154698
    if-eqz v0, :cond_1

    .line 2154699
    const v0, 0x7f0a00e6

    new-array v1, v5, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    .line 2154700
    :goto_0
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->B:LX/1xf;

    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    .line 2154701
    iget-wide v9, p1, LX/7h0;->c:J

    move-wide v2, v9

    .line 2154702
    invoke-virtual {v0, v1, v2, v3}, LX/1xf;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2154703
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2154704
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->y:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082ab3

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2154705
    :goto_1
    return-void

    .line 2154706
    :cond_1
    const v0, 0x7f0a00d2

    new-array v1, v5, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    goto :goto_0

    .line 2154707
    :cond_2
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->y:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082ab4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2154708
    iget-object v0, p1, LX/7h0;->a:LX/7gz;

    move-object v0, v0

    .line 2154709
    sget-object v1, LX/7gz;->SENDING:LX/7gz;

    if-ne v0, v1, :cond_3

    .line 2154710
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v6

    invoke-static {v7, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154711
    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v4

    invoke-static {v4, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154712
    const v0, 0x7f0a00e6

    new-array v1, v6, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v5

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    .line 2154713
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082aab

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1

    .line 2154714
    :cond_3
    sget-object v1, LX/7gz;->SENT:LX/7gz;

    if-ne v0, v1, :cond_4

    .line 2154715
    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v4

    invoke-static {v7, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154716
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v6

    invoke-static {v4, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154717
    const v0, 0x7f0a00e6

    new-array v1, v6, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v5

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    .line 2154718
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082aaa

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2154719
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->B:LX/1xf;

    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    .line 2154720
    iget-wide v9, p1, LX/7h0;->c:J

    move-wide v2, v9

    .line 2154721
    invoke-virtual {v0, v1, v2, v3}, LX/1xf;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2154722
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2154723
    :cond_4
    sget-object v1, LX/7gz;->FAILED:LX/7gz;

    if-ne v0, v1, :cond_5

    .line 2154724
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v6

    invoke-static {v7, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154725
    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v4

    invoke-static {v4, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154726
    const v0, 0x7f0a00e6

    new-array v1, v5, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    .line 2154727
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082aad

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 2154728
    :cond_5
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->r:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v5

    invoke-static {v7, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154729
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->p:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v1, v0, v5

    invoke-static {v4, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154730
    const v0, 0x7f0a00d2

    new-array v1, v5, [Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;I[Lcom/facebook/resources/ui/FbTextView;)V

    .line 2154731
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082aac

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1
.end method

.method public static a(LX/AFW;LX/AFW;)Z
    .locals 8

    .prologue
    .line 2154616
    iget-object v0, p0, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154617
    iget-object v1, p1, LX/AFW;->a:LX/7h0;

    move-object v1, v1

    .line 2154618
    iget-wide v6, v0, LX/7h0;->c:J

    move-wide v2, v6

    .line 2154619
    iget-wide v6, v1, LX/7h0;->c:J

    move-wide v4, v6

    .line 2154620
    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2154621
    iget-object v2, v0, LX/7h0;->a:LX/7gz;

    move-object v2, v2

    .line 2154622
    iget-object v3, v1, LX/7h0;->a:LX/7gz;

    move-object v3, v3

    .line 2154623
    if-ne v2, v3, :cond_0

    .line 2154624
    iget v2, v0, LX/7h0;->j:I

    move v2, v2

    .line 2154625
    iget v3, v1, LX/7h0;->j:I

    move v3, v3

    .line 2154626
    if-ne v2, v3, :cond_0

    .line 2154627
    iget-object v2, v0, LX/7h0;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2154628
    iget-object v3, v1, LX/7h0;->e:Landroid/net/Uri;

    move-object v3, v3

    .line 2154629
    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2154630
    iget-object v2, v0, LX/7h0;->b:LX/7gy;

    move-object v2, v2

    .line 2154631
    iget-object v3, v1, LX/7h0;->b:LX/7gy;

    move-object v3, v3

    .line 2154632
    if-ne v2, v3, :cond_0

    .line 2154633
    iget-object v2, v0, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v2

    .line 2154634
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    .line 2154635
    iget-object v3, v1, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v3

    .line 2154636
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2154637
    iget-boolean v2, v0, LX/7h0;->l:Z

    move v2, v2

    .line 2154638
    iget-boolean v3, v1, LX/7h0;->l:Z

    move v3, v3

    .line 2154639
    if-ne v2, v3, :cond_0

    .line 2154640
    iget-object v2, v0, LX/7h0;->n:LX/7gy;

    move-object v0, v2

    .line 2154641
    iget-object v2, v1, LX/7h0;->n:LX/7gy;

    move-object v1, v2

    .line 2154642
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V
    .locals 2

    .prologue
    .line 2154678
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154679
    iget-boolean v1, v0, LX/7h0;->l:Z

    move v1, v1

    .line 2154680
    if-nez v1, :cond_0

    .line 2154681
    iget-boolean v1, v0, LX/7h0;->h:Z

    move v0, v1

    .line 2154682
    if-nez v0, :cond_0

    .line 2154683
    iget-object v0, p1, LX/AFW;->b:LX/0Px;

    move-object v0, v0

    .line 2154684
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2154685
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2154686
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->F:LX/AFO;

    .line 2154687
    iget-object p0, v0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2154688
    invoke-virtual {v1, v0}, LX/AFO;->a(Ljava/lang/String;)V

    .line 2154689
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V
    .locals 3

    .prologue
    .line 2154664
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154665
    iget-boolean v1, v0, LX/7h0;->l:Z

    move v0, v1

    .line 2154666
    if-eqz v0, :cond_0

    .line 2154667
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154668
    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/EfS;

    invoke-direct {v2, p0, v0, p1}, LX/EfS;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;LX/AFW;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2154669
    :goto_0
    return-void

    .line 2154670
    :cond_0
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154671
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->D:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->r()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2154672
    iget-boolean v1, v0, LX/7h0;->h:Z

    move v1, v1

    .line 2154673
    if-eqz v1, :cond_1

    .line 2154674
    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/EfT;

    invoke-direct {v2, p0, v0}, LX/EfT;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2154675
    :goto_1
    goto :goto_0

    .line 2154676
    :cond_1
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/EfU;

    invoke-direct {v1, p0, p1}, LX/EfU;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2154677
    :cond_2
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/EfV;

    invoke-direct {v1, p0, p1}, LX/EfV;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public static f(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V
    .locals 10

    .prologue
    .line 2154643
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 2154644
    iget-boolean v1, v0, LX/7h0;->l:Z

    move v1, v1

    .line 2154645
    if-nez v1, :cond_1

    .line 2154646
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->E:LX/AFJ;

    .line 2154647
    iget-object v2, v0, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v2

    .line 2154648
    iget-object v3, p1, LX/AFW;->b:LX/0Px;

    move-object v3, v3

    .line 2154649
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2154650
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2154651
    iget-object v8, v4, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v4, v8

    .line 2154652
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2154653
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2154654
    :cond_0
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2154655
    invoke-virtual {v1, v2, v3}, LX/AFJ;->a(Lcom/facebook/audience/model/AudienceControlData;LX/0Px;)J

    move-result-wide v6

    .line 2154656
    :goto_1
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->A:LX/0he;

    .line 2154657
    iget-object v2, v0, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v2

    .line 2154658
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 2154659
    iget-object v0, p1, LX/AFW;->b:LX/0Px;

    move-object v0, v0

    .line 2154660
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    invoke-virtual/range {v1 .. v7}, LX/0he;->a(Ljava/lang/String;IZIJ)V

    .line 2154661
    return-void

    .line 2154662
    :cond_1
    iget-wide v8, v0, LX/7h0;->m:J

    move-wide v6, v8

    .line 2154663
    goto :goto_1
.end method
