.class public Lcom/facebook/audience/direct/ui/SnacksInboxView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/Efa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1EX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/support/v7/widget/RecyclerView;

.field private h:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public i:I

.field private j:LX/3rW;

.field public k:LX/Ef5;

.field private final l:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final m:LX/1PH;

.field private final n:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2155041
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155039
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155040
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155033
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155034
    new-instance v0, LX/Efb;

    invoke-direct {v0, p0}, LX/Efb;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 2155035
    new-instance v0, LX/Efc;

    invoke-direct {v0, p0}, LX/Efc;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->m:LX/1PH;

    .line 2155036
    new-instance v0, LX/Efd;

    invoke-direct {v0, p0}, LX/Efd;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->n:Landroid/view/View$OnClickListener;

    .line 2155037
    invoke-direct {p0, p1}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a(Landroid/content/Context;)V

    .line 2155038
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2155023
    const-class v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;

    invoke-static {v0, p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2155024
    const v0, 0x7f03136b

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2155025
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->setOrientation(I)V

    .line 2155026
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->i:I

    .line 2155027
    new-instance v0, LX/3rW;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->j:LX/3rW;

    .line 2155028
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->b()V

    .line 2155029
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->d()V

    .line 2155030
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e()V

    .line 2155031
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->c()V

    .line 2155032
    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/ui/SnacksInboxView;LX/Efa;LX/1EX;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 2154938
    iput-object p1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    iput-object p2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->b:LX/1EX;

    iput-object p3, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;

    new-instance v1, LX/Efa;

    const-class v0, LX/EfW;

    invoke-interface {v3, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/EfW;

    invoke-direct {v1, v0}, LX/Efa;-><init>(LX/EfW;)V

    move-object v0, v1

    check-cast v0, LX/Efa;

    invoke-static {v3}, LX/1EX;->b(LX/0QB;)LX/1EX;

    move-result-object v1

    check-cast v1, LX/1EX;

    invoke-static {v3}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a(Lcom/facebook/audience/direct/ui/SnacksInboxView;LX/Efa;LX/1EX;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxView;LX/0Px;LX/3wP;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AFW;",
            ">;",
            "LX/3wP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2155012
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 2155013
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    .line 2155014
    iput-object p1, v1, LX/Efa;->b:LX/0Px;

    .line 2155015
    if-nez p2, :cond_1

    .line 2155016
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2155017
    :goto_0
    if-nez v0, :cond_0

    .line 2155018
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2155019
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f()V

    .line 2155020
    return-void

    .line 2155021
    :cond_1
    new-instance p1, LX/3wO;

    invoke-direct {p1, p2, v1}, LX/3wO;-><init>(LX/3wP;LX/1OM;)V

    invoke-static {p2, p1}, LX/3wP;->a(LX/3wP;LX/3wK;)V

    .line 2155022
    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2155009
    const v0, 0x7f0d2ce0

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2155010
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->m:LX/1PH;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2155011
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2154995
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2154996
    const v1, 0x7f082ab5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2154997
    const v2, 0x7f082ab6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2154998
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2154999
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2155000
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2155001
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2155002
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2155003
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2155004
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x21

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2155005
    const v0, 0x7f0d2cdf

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2155006
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2155007
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Efe;

    invoke-direct {v1, p0}, LX/Efe;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2155008
    return-void
.end method

.method public static c(LX/AFW;LX/AFW;)Z
    .locals 1

    .prologue
    .line 2154993
    invoke-static {p0, p1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(LX/AFW;LX/AFW;)Z

    move-result v0

    move v0, v0

    .line 2154994
    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2154987
    const v0, 0x7f0d2ce1

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    .line 2154988
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    .line 2154989
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2154990
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2154991
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2154992
    return-void
.end method

.method public static d(LX/AFW;LX/AFW;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2154975
    iget-object v0, p0, LX/AFW;->b:LX/0Px;

    move-object v3, v0

    .line 2154976
    iget-object v0, p1, LX/AFW;->b:LX/0Px;

    move-object v4, v0

    .line 2154977
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2154978
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 2154979
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2154980
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2154981
    iget-object v5, v0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2154982
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2154983
    iget-object p0, v0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2154984
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2154985
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2154986
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2154968
    const v0, 0x7f0d2cde

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2154969
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f082aa7

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2154970
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2154971
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2154972
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2154973
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2154974
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2154958
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->b:LX/1EX;

    .line 2154959
    iget-object v1, v0, LX/1EX;->d:LX/1El;

    .line 2154960
    invoke-virtual {v1}, LX/1El;->a()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2154961
    if-nez v1, :cond_0

    iget-object v1, v0, LX/1EX;->c:LX/1EY;

    .line 2154962
    invoke-virtual {v1}, LX/1EY;->a()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 2154963
    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2154964
    if-eqz v0, :cond_1

    .line 2154965
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2154966
    :goto_3
    return-void

    .line 2154967
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2154956
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->e:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2154957
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 4
    .param p1    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AFW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2154946
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    .line 2154947
    iget-object v1, v0, LX/Efa;->b:LX/0Px;

    move-object v0, v1

    .line 2154948
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    .line 2154949
    iget-object v1, v0, LX/Efa;->b:LX/0Px;

    move-object v0, v1

    .line 2154950
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 2154951
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxView;LX/0Px;LX/3wP;)V

    .line 2154952
    :goto_0
    return-void

    .line 2154953
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a:LX/Efa;

    .line 2154954
    iget-object v3, v2, LX/Efa;->b:LX/0Px;

    move-object v2, v3

    .line 2154955
    invoke-direct {v1, p0, p1, v2}, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;-><init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;LX/0Px;LX/0Px;)V

    const v2, -0x3ea1b635

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2154943
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->j:LX/3rW;

    invoke-virtual {v0, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2154944
    const/4 v0, 0x1

    .line 2154945
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setInboxListener(LX/Ef5;)V
    .locals 0
    .param p1    # LX/Ef5;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2154941
    iput-object p1, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->k:LX/Ef5;

    .line 2154942
    return-void
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 2154939
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2154940
    return-void
.end method
