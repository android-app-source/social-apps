.class public Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/EfQ;

.field public d:Lcom/facebook/audience/model/ReplyThread;

.field private e:Lcom/facebook/audience/ui/ReplyRecyclerView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2153956
    const-class v0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EfQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2153957
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2153958
    iput-object p1, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->b:Landroid/content/Context;

    .line 2153959
    iput-object p2, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->c:LX/EfQ;

    .line 2153960
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2153961
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2153962
    new-instance v1, Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2153963
    new-instance v0, LX/EfM;

    invoke-direct {v0, v1}, LX/EfM;-><init>(Landroid/view/View;)V

    .line 2153964
    :goto_0
    return-object v0

    .line 2153965
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 2153966
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030181

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2153967
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->c:LX/EfQ;

    .line 2153968
    new-instance p1, LX/EfP;

    invoke-static {v1}, LX/1xf;->a(LX/0QB;)LX/1xf;

    move-result-object v2

    check-cast v2, LX/1xf;

    const/16 p0, 0x12cb

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {v1}, LX/1EX;->b(LX/0QB;)LX/1EX;

    move-result-object p0

    check-cast p0, LX/1EX;

    invoke-direct {p1, v2, v0, p2, p0}, LX/EfP;-><init>(LX/1xf;Landroid/widget/LinearLayout;LX/0Or;LX/1EX;)V

    .line 2153969
    move-object v0, p1

    .line 2153970
    goto :goto_0

    .line 2153971
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2153972
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2153973
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2153974
    check-cast p1, LX/EfM;

    .line 2153975
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->e:Lcom/facebook/audience/ui/ReplyRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/audience/ui/ReplyRecyclerView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->e:Lcom/facebook/audience/ui/ReplyRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/audience/ui/ReplyRecyclerView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->e:Lcom/facebook/audience/ui/ReplyRecyclerView;

    .line 2153976
    iget-boolean p0, v3, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    move v3, p0

    .line 2153977
    iget-object v4, p1, LX/1a1;->a:Landroid/view/View;

    new-instance p0, Landroid/view/ViewGroup$LayoutParams;

    invoke-static {p1, v0, v2, v3}, LX/EfM;->b(LX/EfM;IIZ)I

    move-result p2

    invoke-direct {p0, v1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2153978
    :cond_0
    :goto_1
    return-void

    .line 2153979
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    .line 2153980
    iget-object v1, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, v1

    .line 2153981
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0

    .line 2153982
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2153983
    check-cast p1, LX/EfP;

    .line 2153984
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    .line 2153985
    iget-object v1, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, v1

    .line 2153986
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 2153987
    iget-object v1, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2153988
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2153989
    invoke-static {p1, v2, v0}, LX/EfP;->a(LX/EfP;Landroid/content/res/Resources;Lcom/facebook/audience/model/Reply;)Landroid/text/SpannableString;

    move-result-object v3

    .line 2153990
    iget-object v4, p1, LX/EfP;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2153991
    iget-object v3, v0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v3, v3

    .line 2153992
    sget-object v4, LX/7gq;->FAILED:LX/7gq;

    if-ne v3, v4, :cond_3

    .line 2153993
    iget-object v3, p1, LX/EfP;->l:Lcom/facebook/resources/ui/FbTextView;

    new-instance v4, LX/EfN;

    invoke-direct {v4, p1, v1}, LX/EfN;-><init>(LX/EfP;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153994
    :goto_2
    iget-object v1, v0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    move-object v1, v1

    .line 2153995
    if-nez v1, :cond_4

    iget-object v1, p1, LX/EfP;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v1

    const v3, 0x7f0b1b6a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2153996
    :goto_3
    iget-object v2, p1, LX/EfP;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v3, LX/EfP;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2153997
    iput-object v0, p1, LX/EfP;->q:Lcom/facebook/audience/model/Reply;

    .line 2153998
    goto :goto_1

    .line 2153999
    :cond_3
    iget-object v1, p1, LX/EfP;->l:Lcom/facebook/resources/ui/FbTextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2154000
    :cond_4
    iget-object v1, v0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    move-object v1, v1

    .line 2154001
    goto :goto_3
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 2154002
    invoke-super {p0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2154003
    check-cast p1, Lcom/facebook/audience/ui/ReplyRecyclerView;

    iput-object p1, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->e:Lcom/facebook/audience/ui/ReplyRecyclerView;

    .line 2154004
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2154005
    if-nez p1, :cond_0

    .line 2154006
    const/4 v0, 0x2

    .line 2154007
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2154008
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    .line 2154009
    iget-object p0, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, p0

    .line 2154010
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
