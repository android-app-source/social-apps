.class public Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;
.super Landroid/widget/ViewSwitcher;
.source ""


# instance fields
.field private a:Landroid/view/animation/Animation;

.field private b:Landroid/view/animation/Animation;

.field private c:Landroid/view/animation/Animation;

.field private d:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2155171
    invoke-direct {p0, p1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;)V

    .line 2155172
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->a()V

    .line 2155173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2155168
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2155169
    invoke-direct {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->a()V

    .line 2155170
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2155162
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400d0

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->a:Landroid/view/animation/Animation;

    .line 2155163
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400e0

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->b:Landroid/view/animation/Animation;

    .line 2155164
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400d7

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->c:Landroid/view/animation/Animation;

    .line 2155165
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400e8

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->d:Landroid/view/animation/Animation;

    .line 2155166
    invoke-static {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->b$redex0(Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;)V

    .line 2155167
    return-void
.end method

.method public static b$redex0(Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;)V
    .locals 1

    .prologue
    .line 2155152
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->c:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 2155153
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 2155154
    return-void
.end method

.method public static c(Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;)V
    .locals 1

    .prologue
    .line 2155159
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->a:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 2155160
    iget-object v0, p0, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->b:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 2155161
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2155155
    invoke-virtual {p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->showNext()V

    .line 2155156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2155157
    new-instance v1, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher$1;

    invoke-direct {v1, p0}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher$1;-><init>(Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;)V

    const v2, -0x64b650f6

    invoke-static {v0, v1, p1, p2, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2155158
    return-void
.end method
