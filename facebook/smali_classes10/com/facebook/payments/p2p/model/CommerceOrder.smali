.class public Lcom/facebook/payments/p2p/model/CommerceOrder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/CommerceOrderDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/CommerceOrder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDescription:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field private mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private mImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_url"
    .end annotation
.end field

.field private mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field private mSellerInfo:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "seller_info"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051317
    const-class v0, Lcom/facebook/payments/p2p/model/CommerceOrderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051269
    new-instance v0, LX/Dt9;

    invoke-direct {v0}, LX/Dt9;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/CommerceOrder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2051310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051311
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    .line 2051312
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    .line 2051313
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    .line 2051314
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    .line 2051315
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    .line 2051316
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051304
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    .line 2051305
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    .line 2051306
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    .line 2051307
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    .line 2051308
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    .line 2051309
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2051296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051297
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    .line 2051298
    iput-object p2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    .line 2051299
    iput-object p3, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    .line 2051300
    iput-object p4, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    .line 2051301
    iput-object p5, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    .line 2051302
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051295
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2051294
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;
    .locals 2

    .prologue
    .line 2051278
    new-instance v0, LX/Dtj;

    invoke-direct {v0}, LX/Dtj;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    .line 2051279
    iput-object v1, v0, LX/Dtj;->a:Ljava/lang/String;

    .line 2051280
    move-object v0, v0

    .line 2051281
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    .line 2051282
    iput-object v1, v0, LX/Dtj;->b:Ljava/lang/String;

    .line 2051283
    move-object v0, v0

    .line 2051284
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    .line 2051285
    iput-object v1, v0, LX/Dtj;->c:Ljava/lang/String;

    .line 2051286
    move-object v0, v0

    .line 2051287
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    .line 2051288
    iput-object v1, v0, LX/Dtj;->d:Ljava/lang/String;

    .line 2051289
    move-object v0, v0

    .line 2051290
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    .line 2051291
    iput-object v1, v0, LX/Dtj;->e:Ljava/lang/String;

    .line 2051292
    move-object v0, v0

    .line 2051293
    invoke-virtual {v0}, LX/Dtj;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051277
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051276
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "description"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sellerInfo"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "imageUrl"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051270
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051271
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051272
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051273
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mSellerInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051274
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/CommerceOrder;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051275
    return-void
.end method
