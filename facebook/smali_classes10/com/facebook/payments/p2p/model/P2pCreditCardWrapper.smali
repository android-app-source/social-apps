.class public Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/P2pCreditCardWrapperDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCommercePaymentEligible:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "commerce_payment_eligible"
    .end annotation
.end field

.field private final mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mIsDefaultReceiving:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_default_receiving"
    .end annotation
.end field

.field private final mIsMobileCSCVerified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mobile_csc_verified"
    .end annotation
.end field

.field private final mIsWebCSCVerified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "web_csc_verified"
    .end annotation
.end field

.field private final mIsZipVerified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "zip_verified"
    .end annotation
.end field

.field private final mMethodCategory:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "method_category"
    .end annotation
.end field

.field private final mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "credit_card"
    .end annotation
.end field

.field private final mPersonalTransferEligible:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "personal_transfer_eligible"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051470
    const-class v0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapperDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051469
    new-instance v0, LX/DtD;

    invoke-direct {v0}, LX/DtD;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2051458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051459
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mId:Ljava/lang/String;

    .line 2051460
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    .line 2051461
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsMobileCSCVerified:Z

    .line 2051462
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsWebCSCVerified:Z

    .line 2051463
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsZipVerified:Z

    .line 2051464
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mMethodCategory:Ljava/lang/String;

    .line 2051465
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mCommercePaymentEligible:Z

    .line 2051466
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mPersonalTransferEligible:Z

    .line 2051467
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsDefaultReceiving:Z

    .line 2051468
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051448
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mId:Ljava/lang/String;

    .line 2051449
    const-class v0, Lcom/facebook/payments/p2p/model/P2pCreditCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/P2pCreditCard;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    .line 2051450
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsMobileCSCVerified:Z

    .line 2051451
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsWebCSCVerified:Z

    .line 2051452
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsZipVerified:Z

    .line 2051453
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mMethodCategory:Ljava/lang/String;

    .line 2051454
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mCommercePaymentEligible:Z

    .line 2051455
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mPersonalTransferEligible:Z

    .line 2051456
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsDefaultReceiving:Z

    .line 2051457
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2051443
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mId:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2051444
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2051445
    return-object p0

    .line 2051446
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/payments/p2p/model/P2pCreditCard;
    .locals 1

    .prologue
    .line 2051442
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2051441
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsMobileCSCVerified:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2051423
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsWebCSCVerified:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051440
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2051439
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsZipVerified:Z

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051438
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mMethodCategory:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2051437
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mCommercePaymentEligible:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 2051436
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mPersonalTransferEligible:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 2051435
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsDefaultReceiving:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051434
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "credit_card"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mobile_csc_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsMobileCSCVerified:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "web_csc_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsWebCSCVerified:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "zip_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsZipVerified:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "method_category"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mMethodCategory:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "commerce_payment_eligible"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mCommercePaymentEligible:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "personal_transfer_eligible"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mPersonalTransferEligible:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "is_default_receiving"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsDefaultReceiving:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051424
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051425
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mP2pCreditCard:Lcom/facebook/payments/p2p/model/P2pCreditCard;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051426
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsMobileCSCVerified:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051427
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsWebCSCVerified:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051428
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsZipVerified:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051429
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mMethodCategory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051430
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mCommercePaymentEligible:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051431
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mPersonalTransferEligible:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051432
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->mIsDefaultReceiving:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051433
    return-void
.end method
