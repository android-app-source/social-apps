.class public Lcom/facebook/payments/p2p/model/PaymentTransaction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;


# instance fields
.field public final b:Ljava/lang/String;

.field public c:LX/DtM;

.field public d:Lcom/facebook/payments/p2p/model/Sender;

.field public e:Lcom/facebook/payments/p2p/model/Receiver;

.field public f:Ljava/lang/String;

.field public g:LX/DtQ;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/payments/p2p/model/Amount;

.field public k:Lcom/facebook/payments/p2p/model/Amount;

.field public l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

.field public m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

.field public n:Lcom/facebook/payments/p2p/model/CommerceOrder;

.field public o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051819
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051820
    new-instance v0, LX/DtI;

    invoke-direct {v0}, LX/DtI;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2051803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051804
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    .line 2051805
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    .line 2051806
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051807
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    .line 2051808
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    .line 2051809
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    .line 2051810
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    .line 2051811
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    .line 2051812
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051813
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051814
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051815
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2051816
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2051817
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    .line 2051818
    return-void
.end method

.method public constructor <init>(LX/DtJ;)V
    .locals 1

    .prologue
    .line 2051772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051773
    iget-object v0, p1, LX/DtJ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2051774
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    .line 2051775
    iget-object v0, p1, LX/DtJ;->b:LX/DtM;

    move-object v0, v0

    .line 2051776
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    .line 2051777
    iget-object v0, p1, LX/DtJ;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2051778
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    .line 2051779
    iget-object v0, p1, LX/DtJ;->f:LX/DtQ;

    move-object v0, v0

    .line 2051780
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    .line 2051781
    iget-object v0, p1, LX/DtJ;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2051782
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    .line 2051783
    iget-object v0, p1, LX/DtJ;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2051784
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    .line 2051785
    iget-object v0, p1, LX/DtJ;->c:Lcom/facebook/payments/p2p/model/Sender;

    move-object v0, v0

    .line 2051786
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    .line 2051787
    iget-object v0, p1, LX/DtJ;->d:Lcom/facebook/payments/p2p/model/Receiver;

    move-object v0, v0

    .line 2051788
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051789
    iget-object v0, p1, LX/DtJ;->i:Lcom/facebook/payments/p2p/model/Amount;

    move-object v0, v0

    .line 2051790
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051791
    iget-object v0, p1, LX/DtJ;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v0, v0

    .line 2051792
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051793
    iget-object v0, p1, LX/DtJ;->k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-object v0, v0

    .line 2051794
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051795
    iget-object v0, p1, LX/DtJ;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-object v0, v0

    .line 2051796
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2051797
    iget-object v0, p1, LX/DtJ;->m:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v0, v0

    .line 2051798
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2051799
    iget-object v0, p1, LX/DtJ;->n:Ljava/lang/String;

    move-object v0, v0

    .line 2051800
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    .line 2051801
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->q()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2051802
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051756
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    .line 2051757
    const-class v0, LX/DtM;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtM;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    .line 2051758
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    .line 2051759
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DtQ;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    .line 2051760
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    .line 2051761
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    .line 2051762
    const-class v0, Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Sender;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    .line 2051763
    const-class v0, Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Receiver;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051764
    const-class v0, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Amount;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051765
    const-class v0, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Amount;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051766
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051767
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2051768
    const-class v0, Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/CommerceOrder;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2051769
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    .line 2051770
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->q()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2051771
    return-void
.end method

.method public static newBuilder()LX/DtJ;
    .locals 1

    .prologue
    .line 2051754
    new-instance v0, LX/DtJ;

    invoke-direct {v0}, LX/DtJ;-><init>()V

    return-object v0
.end method

.method private q()Lcom/facebook/payments/p2p/model/PaymentTransaction;
    .locals 1

    .prologue
    .line 2051821
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2051822
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    :goto_1
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    .line 2051823
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    .line 2051824
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    :goto_3
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    .line 2051825
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    :goto_4
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    .line 2051826
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    :goto_5
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    .line 2051827
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    :goto_6
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051828
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    :goto_7
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    .line 2051829
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    :goto_8
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051830
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    :goto_9
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051831
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    :goto_a
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051832
    return-object p0

    .line 2051833
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2051834
    :cond_1
    sget-object v0, LX/DtM;->UNKNOWN:LX/DtM;

    goto :goto_1

    .line 2051835
    :cond_2
    const-string v0, "0"

    goto :goto_2

    .line 2051836
    :cond_3
    const-string v0, "0"

    goto :goto_3

    .line 2051837
    :cond_4
    const-string v0, "0"

    goto :goto_4

    .line 2051838
    :cond_5
    sget-object v0, Lcom/facebook/payments/p2p/model/Sender;->a:Lcom/facebook/payments/p2p/model/Sender;

    goto :goto_5

    .line 2051839
    :cond_6
    sget-object v0, Lcom/facebook/payments/p2p/model/Receiver;->a:Lcom/facebook/payments/p2p/model/Receiver;

    goto :goto_6

    .line 2051840
    :cond_7
    sget-object v0, LX/DtQ;->UNKNOWN_STATUS:LX/DtQ;

    goto :goto_7

    .line 2051841
    :cond_8
    sget-object v0, Lcom/facebook/payments/p2p/model/Amount;->a:Lcom/facebook/payments/p2p/model/Amount;

    goto :goto_8

    .line 2051842
    :cond_9
    sget-object v0, Lcom/facebook/payments/p2p/model/Amount;->b:Lcom/facebook/payments/p2p/model/Amount;

    goto :goto_9

    .line 2051843
    :cond_a
    sget-object v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    goto :goto_a
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2051753
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->q()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051752
    const/4 v0, 0x0

    return v0
.end method

.method public final p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2051686
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    invoke-virtual {v0}, LX/DtM;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 2051687
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    instance-of v0, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-object v1, v0

    .line 2051688
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    invoke-virtual {v0}, LX/DtQ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v4

    .line 2051689
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    invoke-virtual {v0}, LX/DtQ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v5

    .line 2051690
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    instance-of v0, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051691
    :goto_1
    new-instance v6, LX/Dtn;

    invoke-direct {v6}, LX/Dtn;-><init>()V

    .line 2051692
    iput-object v3, v6, LX/Dtn;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2051693
    move-object v6, v6

    .line 2051694
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    if-nez v3, :cond_2

    move-object v3, v2

    .line 2051695
    :goto_2
    iput-object v3, v6, LX/Dtn;->b:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2051696
    move-object v6, v6

    .line 2051697
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    if-nez v3, :cond_3

    move-object v3, v2

    .line 2051698
    :goto_3
    iput-object v3, v6, LX/Dtn;->c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2051699
    move-object v6, v6

    .line 2051700
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    if-nez v3, :cond_4

    move-object v3, v2

    .line 2051701
    :goto_4
    iput-object v3, v6, LX/Dtn;->d:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    .line 2051702
    move-object v3, v6

    .line 2051703
    iget-object v6, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2051704
    iput-wide v6, v3, LX/Dtn;->e:J

    .line 2051705
    move-object v3, v3

    .line 2051706
    iget-object v6, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2051707
    iput-wide v6, v3, LX/Dtn;->f:J

    .line 2051708
    move-object v3, v3

    .line 2051709
    iget-object v6, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    .line 2051710
    iput-object v6, v3, LX/Dtn;->g:Ljava/lang/String;

    .line 2051711
    move-object v3, v3

    .line 2051712
    iput-object v1, v3, LX/Dtn;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2051713
    move-object v3, v3

    .line 2051714
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    if-nez v1, :cond_5

    move-object v1, v2

    .line 2051715
    :goto_5
    iput-object v1, v3, LX/Dtn;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2051716
    move-object v1, v3

    .line 2051717
    iput-object v4, v1, LX/Dtn;->k:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 2051718
    move-object v1, v1

    .line 2051719
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    if-nez v3, :cond_6

    .line 2051720
    :goto_6
    iput-object v2, v1, LX/Dtn;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2051721
    move-object v1, v1

    .line 2051722
    iput-object v5, v1, LX/Dtn;->m:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 2051723
    move-object v1, v1

    .line 2051724
    iput-object v0, v1, LX/Dtn;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051725
    move-object v0, v1

    .line 2051726
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2051727
    iput-wide v2, v0, LX/Dtn;->o:J

    .line 2051728
    move-object v0, v0

    .line 2051729
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    .line 2051730
    iput-object v1, v0, LX/Dtn;->h:Ljava/lang/String;

    .line 2051731
    move-object v0, v0

    .line 2051732
    invoke-virtual {v0}, LX/Dtn;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, v2

    .line 2051733
    goto/16 :goto_0

    :cond_1
    move-object v0, v2

    .line 2051734
    goto :goto_1

    .line 2051735
    :cond_2
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/CommerceOrder;->c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v3

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/Receiver;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/Sender;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    goto :goto_6
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051751
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "payment_type"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sender"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "receiver"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "creation_time"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "transfer_status"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "completed_time"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "updated_time"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "amount"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "amount_fb_discount"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "transfer_context"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "platform_item"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "commerce_order"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "order_id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051736
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051737
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2051738
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051739
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2051740
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051741
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051742
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051743
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051744
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051745
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051746
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2051747
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2051748
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051749
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051750
    return-void
.end method
