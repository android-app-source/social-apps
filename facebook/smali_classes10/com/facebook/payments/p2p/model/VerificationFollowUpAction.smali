.class public Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2052161
    new-instance v0, LX/DtR;

    invoke-direct {v0}, LX/DtR;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/DtS;)V
    .locals 1

    .prologue
    .line 2052151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2052152
    iget-object v0, p1, LX/DtS;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2052153
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->a:Ljava/lang/String;

    .line 2052154
    iget-object v0, p1, LX/DtS;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2052155
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->b:Ljava/lang/String;

    .line 2052156
    iget-object v0, p1, LX/DtS;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2052157
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->c:Ljava/lang/String;

    .line 2052158
    iget-object v0, p1, LX/DtS;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2052159
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->d:Ljava/lang/String;

    .line 2052160
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2052162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2052163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->a:Ljava/lang/String;

    .line 2052164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->b:Ljava/lang/String;

    .line 2052165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->c:Ljava/lang/String;

    .line 2052166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->d:Ljava/lang/String;

    .line 2052167
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2052150
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2052149
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "actionType"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "actionText"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "actionUrl"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "actionButtonText"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2052144
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052145
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052146
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052147
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052148
    return-void
.end method
