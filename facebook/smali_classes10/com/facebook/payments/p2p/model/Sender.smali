.class public Lcom/facebook/payments/p2p/model/Sender;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;
.implements LX/DtN;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/SenderDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/Sender;",
        ">;",
        "LX/DtN;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/Sender;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/p2p/model/Sender;


# instance fields
.field private mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mIsMessengerUser:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_messenger_user"
    .end annotation
.end field

.field private mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2052070
    const-class v0, Lcom/facebook/payments/p2p/model/SenderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2052068
    new-instance v0, Lcom/facebook/payments/p2p/model/Sender;

    const-string v1, "0"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/p2p/model/Sender;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/payments/p2p/model/Sender;->a:Lcom/facebook/payments/p2p/model/Sender;

    .line 2052069
    new-instance v0, LX/DtP;

    invoke-direct {v0}, LX/DtP;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/Sender;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2052063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2052064
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    .line 2052065
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    .line 2052066
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    .line 2052067
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2052057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2052058
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    .line 2052059
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    .line 2052060
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    .line 2052061
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Sender;->f()Lcom/facebook/payments/p2p/model/Sender;

    .line 2052062
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2052055
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/p2p/model/Sender;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2052056
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2052049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2052050
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    .line 2052051
    iput-object p2, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    .line 2052052
    iput-boolean p3, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    .line 2052053
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Sender;->f()Lcom/facebook/payments/p2p/model/Sender;

    .line 2052054
    return-void
.end method

.method private f()Lcom/facebook/payments/p2p/model/Sender;
    .locals 1

    .prologue
    .line 2052044
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    .line 2052045
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    .line 2052046
    return-object p0

    .line 2052047
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 2052048
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2052071
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Sender;->f()Lcom/facebook/payments/p2p/model/Sender;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2052025
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2052043
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2052026
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2052027
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 2

    .prologue
    .line 2052028
    new-instance v0, LX/Dto;

    invoke-direct {v0}, LX/Dto;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    .line 2052029
    iput-object v1, v0, LX/Dto;->b:Ljava/lang/String;

    .line 2052030
    move-object v0, v0

    .line 2052031
    iget-boolean v1, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    .line 2052032
    iput-boolean v1, v0, LX/Dto;->c:Z

    .line 2052033
    move-object v0, v0

    .line 2052034
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    .line 2052035
    iput-object v1, v0, LX/Dto;->d:Ljava/lang/String;

    .line 2052036
    move-object v0, v0

    .line 2052037
    invoke-virtual {v0}, LX/Dto;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2052038
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "is_messenger_user"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2052039
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052040
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2052041
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/Sender;->mIsMessengerUser:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2052042
    return-void
.end method
