.class public Lcom/facebook/payments/p2p/model/PartialPaymentCard;
.super Lcom/facebook/payments/p2p/model/PaymentCard;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/PartialPaymentCard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051639
    new-instance v0, LX/DtE;

    invoke-direct {v0}, LX/DtE;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/PartialPaymentCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;IILcom/facebook/payments/p2p/model/Address;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 2051640
    invoke-static {}, Lcom/facebook/payments/p2p/model/PaymentCard;->newBuilder()LX/DtG;

    move-result-object v0

    .line 2051641
    iput-wide p1, v0, LX/DtG;->a:J

    .line 2051642
    move-object v0, v0

    .line 2051643
    iput-object p3, v0, LX/DtG;->b:Ljava/lang/String;

    .line 2051644
    move-object v0, v0

    .line 2051645
    iput p4, v0, LX/DtG;->c:I

    .line 2051646
    move-object v0, v0

    .line 2051647
    iput p5, v0, LX/DtG;->d:I

    .line 2051648
    move-object v0, v0

    .line 2051649
    iput-object p6, v0, LX/DtG;->e:Lcom/facebook/payments/p2p/model/Address;

    .line 2051650
    move-object v0, v0

    .line 2051651
    iput-object p7, v0, LX/DtG;->f:Ljava/lang/String;

    .line 2051652
    move-object v0, v0

    .line 2051653
    iput-boolean p8, v0, LX/DtG;->g:Z

    .line 2051654
    move-object v0, v0

    .line 2051655
    iput-boolean p9, v0, LX/DtG;->i:Z

    .line 2051656
    move-object v0, v0

    .line 2051657
    invoke-direct {p0, v0}, Lcom/facebook/payments/p2p/model/PaymentCard;-><init>(LX/DtG;)V

    .line 2051658
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2051659
    invoke-direct {p0, p1}, Lcom/facebook/payments/p2p/model/PaymentCard;-><init>(Landroid/os/Parcel;)V

    .line 2051660
    return-void
.end method


# virtual methods
.method public final l()LX/DtH;
    .locals 2

    .prologue
    .line 2051661
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Cannot access category for locally constructed PaymentCard"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2051662
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Cannot access CommercePaymentEligible for locally constructed PaymentCard"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2051663
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Cannot access PersonaTransferEligible for locally constructed PaymentCard"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2051664
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Cannot access IsDefaultReceiving for locally constructed PaymentCard"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method
