.class public Lcom/facebook/payments/p2p/model/PaymentCard;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Lcom/facebook/payments/p2p/model/Address;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field private final h:Z

.field private final i:Z

.field public final j:LX/DtH;

.field private final k:Z

.field private final l:Z

.field private final m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051628
    new-instance v0, LX/DtF;

    invoke-direct {v0}, LX/DtF;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/PaymentCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/DtG;)V
    .locals 6

    .prologue
    .line 2051532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051533
    iget-wide v4, p1, LX/DtG;->a:J

    move-wide v0, v4

    .line 2051534
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2051535
    iget-wide v4, p1, LX/DtG;->a:J

    move-wide v0, v4

    .line 2051536
    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    .line 2051537
    iget-object v0, p1, LX/DtG;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2051538
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    .line 2051539
    iget v0, p1, LX/DtG;->c:I

    move v0, v0

    .line 2051540
    iput v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    .line 2051541
    iget v0, p1, LX/DtG;->d:I

    move v0, v0

    .line 2051542
    iput v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    .line 2051543
    iget-object v0, p1, LX/DtG;->e:Lcom/facebook/payments/p2p/model/Address;

    move-object v0, v0

    .line 2051544
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    .line 2051545
    iget-object v0, p1, LX/DtG;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2051546
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    .line 2051547
    iget-boolean v0, p1, LX/DtG;->g:Z

    move v0, v0

    .line 2051548
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    .line 2051549
    iget-boolean v0, p1, LX/DtG;->h:Z

    move v0, v0

    .line 2051550
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    .line 2051551
    iget-boolean v0, p1, LX/DtG;->i:Z

    move v0, v0

    .line 2051552
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    .line 2051553
    iget-object v0, p1, LX/DtG;->j:LX/DtH;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/DtG;->j:LX/DtH;

    :goto_1
    move-object v0, v0

    .line 2051554
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    .line 2051555
    iget-boolean v0, p1, LX/DtG;->k:Z

    move v0, v0

    .line 2051556
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    .line 2051557
    iget-boolean v0, p1, LX/DtG;->l:Z

    move v0, v0

    .line 2051558
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    .line 2051559
    iget-boolean v0, p1, LX/DtG;->m:Z

    move v0, v0

    .line 2051560
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    .line 2051561
    return-void

    .line 2051562
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, LX/DtH;->UNKNOWN:LX/DtH;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2051593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051594
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    .line 2051595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    .line 2051596
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    .line 2051597
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    .line 2051598
    const-class v0, Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Address;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    .line 2051599
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    .line 2051600
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    .line 2051601
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    .line 2051602
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    .line 2051603
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DtH;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    .line 2051604
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    .line 2051605
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    .line 2051606
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    .line 2051607
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;)V
    .locals 4

    .prologue
    .line 2051608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051609
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2051610
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->b()Lcom/facebook/payments/p2p/model/P2pCreditCard;

    move-result-object v0

    .line 2051611
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    .line 2051612
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    .line 2051613
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->d()I

    move-result v1

    iput v1, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    .line 2051614
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->e()I

    move-result v1

    iput v1, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    .line 2051615
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->f()Lcom/facebook/payments/p2p/model/Address;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    .line 2051616
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    .line 2051617
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    .line 2051618
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    .line 2051619
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    .line 2051620
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DtH;->fromString(Ljava/lang/String;)LX/DtH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    .line 2051621
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    .line 2051622
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    .line 2051623
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    .line 2051624
    return-void
.end method

.method public static newBuilder()LX/DtG;
    .locals 1

    .prologue
    .line 2051625
    new-instance v0, LX/DtG;

    invoke-direct {v0}, LX/DtG;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2051626
    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v0, v2

    .line 2051627
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051638
    invoke-static {p0}, LX/6zL;->b(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 2051629
    sget-object v0, LX/6zU;->CREDIT_CARD:LX/6zU;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051635
    iget v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    move v0, v0

    .line 2051636
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2051637
    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2051634
    const-string v0, "%s \u00b7"

    invoke-virtual {p0, p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051632
    iget v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    move v0, v0

    .line 2051633
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051631
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2051630
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/PaymentCard;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2051563
    if-ne p0, p1, :cond_1

    .line 2051564
    :cond_0
    :goto_0
    return v0

    .line 2051565
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2051566
    :cond_3
    check-cast p1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2051567
    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    iget-wide v4, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 2051568
    goto :goto_0

    .line 2051569
    :cond_4
    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2051570
    goto :goto_0

    .line 2051571
    :cond_5
    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    iget v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2051572
    goto :goto_0

    .line 2051573
    :cond_6
    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    iget v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2051574
    goto :goto_0

    .line 2051575
    :cond_7
    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/p2p/model/Address;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2051576
    goto :goto_0

    .line 2051577
    :cond_8
    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 2051578
    goto :goto_0

    .line 2051579
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    iget-boolean v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2051580
    goto :goto_0

    .line 2051581
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    iget-boolean v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 2051582
    goto :goto_0

    .line 2051583
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    iget-boolean v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 2051584
    goto :goto_0

    .line 2051585
    :cond_c
    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    invoke-virtual {v2, v3}, LX/DtH;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 2051586
    goto :goto_0

    .line 2051587
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->m()Z

    move-result v3

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 2051588
    goto :goto_0

    .line 2051589
    :cond_e
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->n()Z

    move-result v3

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 2051590
    goto/16 :goto_0

    .line 2051591
    :cond_f
    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->o()Z

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2051592
    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051503
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 1

    .prologue
    .line 2051504
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2051505
    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 2051506
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2051507
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051508
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    move-object v0, v0

    .line 2051509
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/Address;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/facebook/common/locale/Country;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2051510
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2051511
    invoke-static {p0}, LX/6zL;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Z

    move-result v0

    return v0
.end method

.method public l()LX/DtH;
    .locals 1

    .prologue
    .line 2051512
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 2051531
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 2051513
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 2051514
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2051515
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "credential_id"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "number"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "expire_month"

    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "expire_year"

    iget v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "address"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/Address;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "association"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mobile_csc_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "web_csc_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "zip_verified"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "method_category"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "commerce_payment_eligible"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "personal_transfer_eligible"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "is_default_receiving"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 2051516
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2051517
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2051518
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051519
    iget v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051520
    iget v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051521
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051522
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051523
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051524
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051525
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051526
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2051527
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051528
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->l:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051529
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/PaymentCard;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051530
    return-void
.end method
