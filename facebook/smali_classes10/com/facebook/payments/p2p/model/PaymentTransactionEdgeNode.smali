.class public Lcom/facebook/payments/p2p/model/PaymentTransactionEdgeNode;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/PaymentTransactionEdgeNodeDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final mPaymentTransaction:Lcom/facebook/payments/p2p/model/PaymentTransaction;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "node"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051890
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentTransactionEdgeNodeDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2051892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051893
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransactionEdgeNode;->mPaymentTransaction:Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2051894
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051891
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "node"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/PaymentTransactionEdgeNode;->mPaymentTransaction:Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
