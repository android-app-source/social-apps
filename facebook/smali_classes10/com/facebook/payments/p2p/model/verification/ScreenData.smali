.class public Lcom/facebook/payments/p2p/model/verification/ScreenData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/verification/ScreenDataDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/verification/ScreenData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCardIssuer:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "card_issuer"
    .end annotation
.end field

.field private final mCardLastFour:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "card_last_four"
    .end annotation
.end field

.field private final mDobDay:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_day"
    .end annotation
.end field

.field private final mDobMonth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_month"
    .end annotation
.end field

.field private final mDobYear:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_year"
    .end annotation
.end field

.field private final mErrorMessage:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_message"
    .end annotation
.end field

.field private final mFirstName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_name"
    .end annotation
.end field

.field private final mHasThrownException:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_thrown_exception"
    .end annotation
.end field

.field private final mIntroReason:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intro_reason"
    .end annotation
.end field

.field private final mIsSender:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_sender"
    .end annotation
.end field

.field private final mLastName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_name"
    .end annotation
.end field

.field private final mPreviousAttemptFailed:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "previous_attempt_failed"
    .end annotation
.end field

.field private final mSenderShortName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sender_short_name"
    .end annotation
.end field

.field private final mVerificationSucceeded:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "verification_succeeded"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2057366
    const-class v0, Lcom/facebook/payments/p2p/model/verification/ScreenDataDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057367
    new-instance v0, LX/Dua;

    invoke-direct {v0}, LX/Dua;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2057368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057369
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIntroReason:Ljava/lang/String;

    .line 2057370
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mFirstName:Ljava/lang/String;

    .line 2057371
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mLastName:Ljava/lang/String;

    .line 2057372
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mSenderShortName:Ljava/lang/String;

    .line 2057373
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardIssuer:Ljava/lang/String;

    .line 2057374
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardLastFour:Ljava/lang/String;

    .line 2057375
    iput-object v1, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mErrorMessage:Ljava/lang/String;

    .line 2057376
    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobYear:I

    .line 2057377
    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobMonth:I

    .line 2057378
    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobDay:I

    .line 2057379
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIsSender:Z

    .line 2057380
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mPreviousAttemptFailed:Z

    .line 2057381
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mHasThrownException:Z

    .line 2057382
    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mVerificationSucceeded:Z

    .line 2057383
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIntroReason:Ljava/lang/String;

    .line 2057386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mFirstName:Ljava/lang/String;

    .line 2057387
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mLastName:Ljava/lang/String;

    .line 2057388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mSenderShortName:Ljava/lang/String;

    .line 2057389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardIssuer:Ljava/lang/String;

    .line 2057390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardLastFour:Ljava/lang/String;

    .line 2057391
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mErrorMessage:Ljava/lang/String;

    .line 2057392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobYear:I

    .line 2057393
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobMonth:I

    .line 2057394
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobDay:I

    .line 2057395
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIsSender:Z

    .line 2057396
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mPreviousAttemptFailed:Z

    .line 2057397
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mHasThrownException:Z

    .line 2057398
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mVerificationSucceeded:Z

    .line 2057399
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057401
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIntroReason:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057359
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057400
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057403
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mSenderShortName:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057402
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057364
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardIssuer:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057365
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardLastFour:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057363
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 2057362
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobYear:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2057361
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobMonth:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 2057360
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobDay:I

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2057358
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mPreviousAttemptFailed:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2057357
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mHasThrownException:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2057356
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mVerificationSucceeded:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2057355
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "intro_reason"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIntroReason:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "first_name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "last_name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mLastName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sender_short_name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mSenderShortName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "card_issuer"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardIssuer:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "card_last_four"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardLastFour:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "error_message"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dob_year"

    iget v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobYear:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "dob_month"

    iget v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobMonth:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "dob_day"

    iget v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobDay:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "is_sender"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIsSender:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "previous_attempt_failed"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mPreviousAttemptFailed:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "has_thrown_exception"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mHasThrownException:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "verification_succeeded"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mVerificationSucceeded:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057340
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIntroReason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057341
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057342
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057343
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mSenderShortName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057344
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardIssuer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057345
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mCardLastFour:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057346
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057347
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2057348
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2057349
    iget v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mDobDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2057350
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mIsSender:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2057351
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mPreviousAttemptFailed:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2057352
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mHasThrownException:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2057353
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/verification/ScreenData;->mVerificationSucceeded:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2057354
    return-void
.end method
