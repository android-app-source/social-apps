.class public Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/p2p/model/verification/UserInput;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2057509
    const-class v0, Lcom/facebook/payments/p2p/model/verification/UserInput;

    new-instance v1, Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;

    invoke-direct {v1}, Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2057510
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2057511
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/p2p/model/verification/UserInput;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2057512
    if-nez p0, :cond_0

    .line 2057513
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2057514
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2057515
    invoke-static {p0, p1, p2}, Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;->b(Lcom/facebook/payments/p2p/model/verification/UserInput;LX/0nX;LX/0my;)V

    .line 2057516
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2057517
    return-void
.end method

.method private static b(Lcom/facebook/payments/p2p/model/verification/UserInput;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2057518
    const-string v0, "first_name"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mFirstName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057519
    const-string v0, "last_name"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mLastName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057520
    const-string v0, "card_first_six"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mCardFirstSix:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057521
    const-string v0, "dob_year"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobYear:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057522
    const-string v0, "dob_month"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobMonth:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057523
    const-string v0, "dob_day"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobDay:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057524
    const-string v0, "ssn_last_four"

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mSsnLastFour:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2057525
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2057526
    check-cast p1, Lcom/facebook/payments/p2p/model/verification/UserInput;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;LX/0nX;LX/0my;)V

    return-void
.end method
