.class public Lcom/facebook/payments/p2p/model/verification/UserInput;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/verification/UserInputDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/verification/UserInput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mCardFirstSix:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "card_first_six"
    .end annotation
.end field

.field public final mDobDay:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_day"
    .end annotation
.end field

.field public final mDobMonth:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_month"
    .end annotation
.end field

.field public final mDobYear:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dob_year"
    .end annotation
.end field

.field public final mFirstName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_name"
    .end annotation
.end field

.field public final mLastName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_name"
    .end annotation
.end field

.field public final mSsnLastFour:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ssn_last_four"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2057479
    const-class v0, Lcom/facebook/payments/p2p/model/verification/UserInputDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2057441
    const-class v0, Lcom/facebook/payments/p2p/model/verification/UserInputSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057478
    new-instance v0, LX/Dub;

    invoke-direct {v0}, LX/Dub;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/verification/UserInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Duc;)V
    .locals 1

    .prologue
    .line 2057462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057463
    iget-object v0, p1, LX/Duc;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2057464
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mFirstName:Ljava/lang/String;

    .line 2057465
    iget-object v0, p1, LX/Duc;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2057466
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mLastName:Ljava/lang/String;

    .line 2057467
    iget-object v0, p1, LX/Duc;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2057468
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mCardFirstSix:Ljava/lang/String;

    .line 2057469
    iget-object v0, p1, LX/Duc;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2057470
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobYear:Ljava/lang/String;

    .line 2057471
    iget-object v0, p1, LX/Duc;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2057472
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobMonth:Ljava/lang/String;

    .line 2057473
    iget-object v0, p1, LX/Duc;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2057474
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobDay:Ljava/lang/String;

    .line 2057475
    iget-object v0, p1, LX/Duc;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2057476
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mSsnLastFour:Ljava/lang/String;

    .line 2057477
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057454
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mFirstName:Ljava/lang/String;

    .line 2057455
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mLastName:Ljava/lang/String;

    .line 2057456
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mCardFirstSix:Ljava/lang/String;

    .line 2057457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobYear:Ljava/lang/String;

    .line 2057458
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobMonth:Ljava/lang/String;

    .line 2057459
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobDay:Ljava/lang/String;

    .line 2057460
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mSsnLastFour:Ljava/lang/String;

    .line 2057461
    return-void
.end method

.method public static newBuilder()LX/Duc;
    .locals 1

    .prologue
    .line 2057452
    new-instance v0, LX/Duc;

    invoke-direct {v0}, LX/Duc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057451
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2057450
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "first_name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "last_name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mLastName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "card_first_six"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mCardFirstSix:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dob_year"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobYear:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dob_month"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobMonth:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dob_day"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobDay:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ssn_last_four"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mSsnLastFour:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057442
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057443
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057444
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mCardFirstSix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057445
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobYear:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057446
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobMonth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057447
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mDobDay:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057448
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/verification/UserInput;->mSsnLastFour:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057449
    return-void
.end method
