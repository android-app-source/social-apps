.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7706b448
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054556
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054557
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2054558
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054559
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2054560
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054561
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054562
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;
    .locals 4

    .prologue
    .line 2054563
    if-nez p0, :cond_0

    .line 2054564
    const/4 p0, 0x0

    .line 2054565
    :goto_0
    return-object p0

    .line 2054566
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    if-eqz v0, :cond_1

    .line 2054567
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    goto :goto_0

    .line 2054568
    :cond_1
    new-instance v0, LX/Dtn;

    invoke-direct {v0}, LX/Dtn;-><init>()V

    .line 2054569
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2054570
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->b:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054571
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->d()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054572
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->d:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    .line 2054573
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->ki_()J

    move-result-wide v2

    iput-wide v2, v0, LX/Dtn;->e:J

    .line 2054574
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->kj_()J

    move-result-wide v2

    iput-wide v2, v0, LX/Dtn;->f:J

    .line 2054575
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->g:Ljava/lang/String;

    .line 2054576
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->h:Ljava/lang/String;

    .line 2054577
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2054578
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->m()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054579
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->k:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 2054580
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054581
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->m:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 2054582
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtn;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2054583
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r()J

    move-result-wide v2

    iput-wide v2, v0, LX/Dtn;->o:J

    .line 2054584
    invoke-virtual {v0}, LX/Dtn;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    move-result-object p0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 2054585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054586
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2054587
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2054588
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2054589
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2054590
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2054591
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2054592
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2054593
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2054594
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2054595
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2054596
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 2054597
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2054598
    const/16 v4, 0xf

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2054599
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2054600
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2054601
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2054602
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2054603
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->i:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054604
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054605
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2054606
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2054607
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2054608
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2054609
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2054610
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2054611
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2054612
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 2054613
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054614
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054615
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2054511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054512
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2054513
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054514
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2054515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054516
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2054518
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054519
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2054520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054521
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054522
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2054523
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    .line 2054524
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2054525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054526
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    .line 2054527
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2054528
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2054529
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2054530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054531
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2054532
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2054533
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054534
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2054535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054536
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054537
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2054538
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054539
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2054540
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054541
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054542
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2054543
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2054544
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2054545
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2054546
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2054547
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054548
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054616
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2054549
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2054550
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->i:J

    .line 2054551
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j:J

    .line 2054552
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s:J

    .line 2054553
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054617
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2054618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2054619
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2054620
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;-><init>()V

    .line 2054621
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054622
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054623
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054624
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2054625
    const v0, 0x713f0c46

    return v0
.end method

.method public final synthetic e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054626
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2054627
    const v0, 0x2a704793

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054628
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k:Ljava/lang/String;

    .line 2054629
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054554
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->l:Ljava/lang/String;

    .line 2054555
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final ki_()J
    .locals 2

    .prologue
    .line 2054492
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2054493
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->i:J

    return-wide v0
.end method

.method public final kj_()J
    .locals 2

    .prologue
    .line 2054483
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2054484
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j:J

    return-wide v0
.end method

.method public final synthetic l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054485
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054486
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054487
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->o:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->o:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 2054488
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->o:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    return-object v0
.end method

.method public final synthetic o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054489
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054490
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->q:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->q:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 2054491
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->q:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054494
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 2054495
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2054496
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s:J

    return-wide v0
.end method

.method public final s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054497
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054498
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    return-object v0
.end method

.method public final t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054499
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2054500
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    return-object v0
.end method

.method public final u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054501
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    .line 2054502
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    return-object v0
.end method

.method public final v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054503
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2054504
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    return-object v0
.end method

.method public final w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054505
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054506
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    return-object v0
.end method

.method public final x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054507
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2054508
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    return-object v0
.end method

.method public final y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054509
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2054510
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    return-object v0
.end method
