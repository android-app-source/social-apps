.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2053299
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;

    new-instance v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2053300
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2053301
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2053302
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2053303
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2053304
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2053305
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2053306
    if-eqz v2, :cond_0

    .line 2053307
    const-string p0, "outgoing_peer_to_peer_payment_requests"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2053308
    invoke-static {v1, v2, p1, p2}, LX/Du6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2053309
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2053310
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2053311
    check-cast p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$Serializer;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
