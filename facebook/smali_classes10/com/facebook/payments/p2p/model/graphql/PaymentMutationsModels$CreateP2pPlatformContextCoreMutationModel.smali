.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4b3c1416
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2057208
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2057205
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2057175
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2057176
    return-void
.end method

.method private a()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2057206
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    .line 2057207
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    return-object v0
.end method

.method private j()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2057195
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    .line 2057196
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2057197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2057198
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2057199
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2057200
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2057201
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2057202
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2057203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2057204
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2057182
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2057183
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2057184
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    .line 2057185
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2057186
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;

    .line 2057187
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$GroupCommerceProductItemModel;

    .line 2057188
    :cond_0
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2057189
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    .line 2057190
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2057191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;

    .line 2057192
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel$P2pPlatformContextModel;

    .line 2057193
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2057194
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2057179
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentMutationsModels$CreateP2pPlatformContextCoreMutationModel;-><init>()V

    .line 2057180
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2057181
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2057178
    const v0, 0x6d4fdbe0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2057177
    const v0, -0xd15527d

    return v0
.end method
