.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x22a83f20
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:J

.field private l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053974
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053967
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2053968
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2053969
    return-void
.end method

.method private l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053970
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053971
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053972
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->h:Ljava/lang/String;

    .line 2053973
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053926
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    .line 2053927
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    return-object v0
.end method

.method private o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054015
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    .line 2054016
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    return-object v0
.end method

.method private p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053975
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    .line 2053976
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    return-object v0
.end method

.method private q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053977
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    .line 2053978
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    return-object v0
.end method

.method private r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053979
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053980
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    return-object v0
.end method

.method private s()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2053981
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q:Ljava/util/List;

    .line 2053982
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 2053983
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053984
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2053985
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2053986
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2053987
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2053988
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2053989
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2053990
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2053991
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2053992
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2053993
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2053994
    invoke-direct/range {p0 .. p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->s()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v17

    .line 2053995
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2053996
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 2053997
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2053998
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2053999
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2054000
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2054001
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2054002
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->k:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2054003
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2054004
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2054005
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2054006
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2054007
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2054008
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2054009
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2054010
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2054011
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2054012
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2054013
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054014
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2053928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053929
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2053930
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053931
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2053932
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053933
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053934
    :cond_0
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2053935
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    .line 2053936
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2053937
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053938
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    .line 2053939
    :cond_1
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2053940
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    .line 2053941
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2053942
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053943
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel$PeerToPeerPlatformProductItemModel;

    .line 2053944
    :cond_2
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2053945
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    .line 2053946
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2053947
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053948
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    .line 2053949
    :cond_3
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2053950
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    .line 2053951
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2053952
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053953
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->o:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    .line 2053954
    :cond_4
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2053955
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053956
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2053957
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053958
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->p:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2053959
    :cond_5
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2053960
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2053961
    if-eqz v2, :cond_6

    .line 2053962
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2053963
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->q:Ljava/util/List;

    move-object v1, v0

    .line 2053964
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2053965
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053966
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2053900
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2053901
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->f:J

    .line 2053902
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->k:J

    .line 2053903
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r:Z

    .line 2053904
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->s:Z

    .line 2053905
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->t:Z

    .line 2053906
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->u:Z

    .line 2053907
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2053908
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;-><init>()V

    .line 2053909
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2053910
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053911
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053912
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->g:Ljava/lang/String;

    .line 2053913
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053914
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->i:Ljava/lang/String;

    .line 2053915
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2053916
    const v0, 0x2fcae0d2

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053917
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->j:Ljava/lang/String;

    .line 2053918
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2053919
    const v0, -0x168212e3

    return v0
.end method

.method public final synthetic j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053920
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2053921
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2053922
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->r:Z

    return v0
.end method

.method public final ke_()J
    .locals 2

    .prologue
    .line 2053923
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2053924
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->k:J

    return-wide v0
.end method

.method public final synthetic kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053925
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->n()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    return-object v0
.end method
