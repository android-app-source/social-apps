.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2052835
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;

    new-instance v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2052836
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2052837
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2052838
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2052839
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2052840
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2052841
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2052842
    if-eqz v2, :cond_0

    .line 2052843
    const-string p0, "incoming_messenger_payments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2052844
    invoke-static {v1, v2, p1, p2}, LX/Du2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2052845
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2052846
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2052847
    check-cast p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$Serializer;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
