.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x52c03e91
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053665
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053664
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2053662
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2053663
    return-void
.end method

.method private j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053660
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    .line 2053661
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053658
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->f:Ljava/lang/String;

    .line 2053659
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053656
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->g:Ljava/lang/String;

    .line 2053657
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053618
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j:Ljava/lang/String;

    .line 2053619
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053654
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->k:Ljava/lang/String;

    .line 2053655
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2053638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053639
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2053640
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2053641
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2053642
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2053643
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2053644
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2053645
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2053646
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2053647
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2053648
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2053649
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2053650
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2053651
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2053652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2053653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2053630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053631
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2053632
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    .line 2053633
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2053634
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    .line 2053635
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$StreetAddressInfoModel;

    .line 2053636
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2053637
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053629
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2053625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2053626
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->h:Z

    .line 2053627
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;->i:Z

    .line 2053628
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2053622
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$MailingAddressInfoModel;-><init>()V

    .line 2053623
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2053624
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2053621
    const v0, 0x586f3e65

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2053620
    const v0, 0x19a3b2c9

    return v0
.end method
