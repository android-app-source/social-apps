.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4be10e6e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054863
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054864
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2054822
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054823
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2054860
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054861
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054862
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;
    .locals 8

    .prologue
    .line 2054840
    if-nez p0, :cond_0

    .line 2054841
    const/4 p0, 0x0

    .line 2054842
    :goto_0
    return-object p0

    .line 2054843
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    if-eqz v0, :cond_1

    .line 2054844
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    goto :goto_0

    .line 2054845
    :cond_1
    new-instance v0, LX/Dtr;

    invoke-direct {v0}, LX/Dtr;-><init>()V

    .line 2054846
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtr;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    .line 2054847
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2054848
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2054849
    iget-object v3, v0, LX/Dtr;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2054850
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 2054851
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 2054852
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2054853
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2054854
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2054855
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2054856
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2054857
    new-instance v3, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    invoke-direct {v3, v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;-><init>(LX/15i;)V

    .line 2054858
    move-object p0, v3

    .line 2054859
    goto :goto_0
.end method

.method private j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054838
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    .line 2054839
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2054865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054866
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2054867
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2054868
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2054869
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054870
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2054830
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054831
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2054832
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    .line 2054833
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2054834
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    .line 2054835
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    .line 2054836
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054837
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054829
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2054826
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;-><init>()V

    .line 2054827
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054828
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2054825
    const v0, -0x531b9ab8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2054824
    const v0, 0x4984e12

    return v0
.end method
