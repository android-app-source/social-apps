.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3da17e95
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2052741
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2052725
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2052739
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2052740
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2052733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2052734
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2052735
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2052736
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2052737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2052738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2052742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2052743
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2052744
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    .line 2052745
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2052746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;

    .line 2052747
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    .line 2052748
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2052749
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllMessengerPayments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2052731
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    .line 2052732
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2052728
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;-><init>()V

    .line 2052729
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2052730
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2052727
    const v0, 0x33d2c196

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2052726
    const v0, -0x6747e1ce

    return v0
.end method
