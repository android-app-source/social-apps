.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5bc83dd9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054950
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054949
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2054947
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054948
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2054944
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054945
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054946
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .locals 13

    .prologue
    .line 2054911
    if-nez p0, :cond_0

    .line 2054912
    const/4 p0, 0x0

    .line 2054913
    :goto_0
    return-object p0

    .line 2054914
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    if-eqz v0, :cond_1

    .line 2054915
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    goto :goto_0

    .line 2054916
    :cond_1
    new-instance v2, LX/Dtq;

    invoke-direct {v2}, LX/Dtq;-><init>()V

    .line 2054917
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/Dtq;->a:Ljava/lang/String;

    .line 2054918
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/Dtq;->b:Ljava/lang/String;

    .line 2054919
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/Dtq;->c:Ljava/lang/String;

    .line 2054920
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2054921
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2054922
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    invoke-static {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2054923
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2054924
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/Dtq;->d:LX/0Px;

    .line 2054925
    const/4 v8, 0x1

    const/4 v12, 0x0

    const/4 v6, 0x0

    .line 2054926
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2054927
    iget-object v5, v2, LX/Dtq;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2054928
    iget-object v7, v2, LX/Dtq;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2054929
    iget-object v9, v2, LX/Dtq;->c:Ljava/lang/String;

    invoke-virtual {v4, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2054930
    iget-object v10, v2, LX/Dtq;->d:LX/0Px;

    invoke-static {v4, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 2054931
    const/4 v11, 0x4

    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 2054932
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 2054933
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2054934
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2054935
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 2054936
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2054937
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2054938
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2054939
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2054940
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2054941
    new-instance v5, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-direct {v5, v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;-><init>(LX/15i;)V

    .line 2054942
    move-object p0, v5

    .line 2054943
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2054899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054900
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2054901
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2054902
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2054903
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 2054904
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2054905
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2054906
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2054907
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2054908
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2054909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054910
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2054891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054892
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2054893
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2054894
    if-eqz v1, :cond_0

    .line 2054895
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2054896
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->h:Ljava/util/List;

    .line 2054897
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054898
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054951
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2054888
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;-><init>()V

    .line 2054889
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054890
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054878
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e:Ljava/lang/String;

    .line 2054879
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054886
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->f:Ljava/lang/String;

    .line 2054887
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054884
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->g:Ljava/lang/String;

    .line 2054885
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2054883
    const v0, 0x465891d4

    return v0
.end method

.method public final e()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2054881
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel$PhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->h:Ljava/util/List;

    .line 2054882
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2054880
    const v0, -0x4457d1f6

    return v0
.end method
