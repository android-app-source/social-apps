.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x19784d53
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055236
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055235
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2055233
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055234
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2055230
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055231
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055232
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;
    .locals 10

    .prologue
    .line 2055206
    if-nez p0, :cond_0

    .line 2055207
    const/4 p0, 0x0

    .line 2055208
    :goto_0
    return-object p0

    .line 2055209
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    if-eqz v0, :cond_1

    .line 2055210
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    goto :goto_0

    .line 2055211
    :cond_1
    new-instance v2, LX/Dtv;

    invoke-direct {v2}, LX/Dtv;-><init>()V

    .line 2055212
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2055213
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2055214
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    invoke-static {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2055215
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2055216
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/Dtv;->a:LX/0Px;

    .line 2055217
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2055218
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2055219
    iget-object v5, v2, LX/Dtv;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2055220
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2055221
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 2055222
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2055223
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2055224
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2055225
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2055226
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2055227
    new-instance v5, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    invoke-direct {v5, v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;-><init>(LX/15i;)V

    .line 2055228
    move-object p0, v5

    .line 2055229
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2055200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055201
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2055202
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2055203
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2055204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055205
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2055185
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->e:Ljava/util/List;

    .line 2055186
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2055192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055193
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2055194
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2055195
    if-eqz v1, :cond_0

    .line 2055196
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    .line 2055197
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->e:Ljava/util/List;

    .line 2055198
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055199
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2055189
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;-><init>()V

    .line 2055190
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055191
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2055188
    const v0, -0x34e39ad5    # -1.0249515E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2055187
    const v0, 0x4b5afc90    # 1.4351504E7f

    return v0
.end method
