.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a368b7d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054425
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2054424
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2054422
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2054423
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054420
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->e:Ljava/lang/String;

    .line 2054421
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054416
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->f:Ljava/lang/String;

    .line 2054417
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2054418
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->i:Ljava/lang/String;

    .line 2054419
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2054402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054403
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2054404
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2054405
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2054406
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2054407
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2054408
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2054409
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->g:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 2054410
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->h:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 2054411
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2054412
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->j:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 2054413
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->k:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 2054414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054415
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2054399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2054400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2054401
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2054388
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2054389
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->g:I

    .line 2054390
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->h:I

    .line 2054391
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->j:I

    .line 2054392
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;->k:I

    .line 2054393
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2054396
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentShippingOptionModel;-><init>()V

    .line 2054397
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2054398
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2054395
    const v0, -0x6c018190

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2054394
    const v0, -0x66396062

    return v0
.end method
