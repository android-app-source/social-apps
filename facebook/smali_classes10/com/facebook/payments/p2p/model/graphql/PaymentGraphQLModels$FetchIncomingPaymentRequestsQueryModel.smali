.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x793420e5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2052973
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2052972
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2052970
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2052971
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2052964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2052965
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2052966
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2052967
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2052968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2052969
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2052974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2052975
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2052976
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    .line 2052977
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2052978
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;

    .line 2052979
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    .line 2052980
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2052981
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2052957
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    .line 2052958
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2052959
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;-><init>()V

    .line 2052960
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2052961
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2052962
    const v0, -0x593e020a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2052963
    const v0, -0x6747e1ce

    return v0
.end method
