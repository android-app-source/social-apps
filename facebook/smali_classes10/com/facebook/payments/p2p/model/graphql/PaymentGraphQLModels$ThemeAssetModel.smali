.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4d89ad5b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetCompatibilityEnum;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055163
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055162
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2055160
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055161
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2055157
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055158
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055159
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;
    .locals 12

    .prologue
    .line 2055127
    if-nez p0, :cond_0

    .line 2055128
    const/4 p0, 0x0

    .line 2055129
    :goto_0
    return-object p0

    .line 2055130
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    if-eqz v0, :cond_1

    .line 2055131
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    goto :goto_0

    .line 2055132
    :cond_1
    new-instance v1, LX/Dtt;

    invoke-direct {v1}, LX/Dtt;-><init>()V

    .line 2055133
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    move-result-object v0

    iput-object v0, v1, LX/Dtt;->a:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 2055134
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2055135
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 2055136
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2055137
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2055138
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/Dtt;->b:LX/0Px;

    .line 2055139
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v0

    iput-object v0, v1, LX/Dtt;->c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    .line 2055140
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 2055141
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2055142
    iget-object v5, v1, LX/Dtt;->a:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    invoke-virtual {v4, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2055143
    iget-object v7, v1, LX/Dtt;->b:LX/0Px;

    invoke-virtual {v4, v7}, LX/186;->c(Ljava/util/List;)I

    move-result v7

    .line 2055144
    iget-object v9, v1, LX/Dtt;->c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2055145
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 2055146
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 2055147
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2055148
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2055149
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2055150
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2055151
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2055152
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2055153
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2055154
    new-instance v5, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    invoke-direct {v5, v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;-><init>(LX/15i;)V

    .line 2055155
    move-object p0, v5

    .line 2055156
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055097
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    .line 2055098
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2055117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055118
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2055119
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 2055120
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2055121
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2055122
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2055123
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2055124
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2055125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2055109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055110
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2055111
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    .line 2055112
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2055113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    .line 2055114
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    .line 2055115
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055116
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055107
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 2055108
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetCompatibilityEnum;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2055105
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetCompatibilityEnum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->f:Ljava/util/List;

    .line 2055106
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2055102
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;-><init>()V

    .line 2055103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055104
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055101
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeAssetModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2055100
    const v0, -0x785d4f8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2055099
    const v0, 0x49a18d3c    # 1323431.5f

    return v0
.end method
