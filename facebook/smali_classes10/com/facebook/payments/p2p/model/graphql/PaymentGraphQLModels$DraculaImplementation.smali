.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2052428
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2052429
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2052426
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2052427
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2052398
    if-nez p1, :cond_0

    .line 2052399
    :goto_0
    return v0

    .line 2052400
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2052401
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2052402
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052403
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052404
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052405
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2052406
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052407
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052408
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052409
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2052410
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052411
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052412
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052413
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2052414
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052415
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052416
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052417
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2052418
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052419
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052420
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052421
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2052422
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2052423
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2052424
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2052425
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x36ea12ce -> :sswitch_2
        -0x1c8e5324 -> :sswitch_5
        -0xb023911 -> :sswitch_4
        0x1b1a8c51 -> :sswitch_3
        0x58c3722f -> :sswitch_0
        0x78a2e54a -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2052397
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2052394
    sparse-switch p0, :sswitch_data_0

    .line 2052395
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2052396
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x36ea12ce -> :sswitch_0
        -0x1c8e5324 -> :sswitch_0
        -0xb023911 -> :sswitch_0
        0x1b1a8c51 -> :sswitch_0
        0x58c3722f -> :sswitch_0
        0x78a2e54a -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2052393
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2052360
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->b(I)V

    .line 2052361
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2052388
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2052389
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2052390
    :cond_0
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2052391
    iput p2, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->b:I

    .line 2052392
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2052387
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2052386
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2052383
    iget v0, p0, LX/1vt;->c:I

    .line 2052384
    move v0, v0

    .line 2052385
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2052380
    iget v0, p0, LX/1vt;->c:I

    .line 2052381
    move v0, v0

    .line 2052382
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2052377
    iget v0, p0, LX/1vt;->b:I

    .line 2052378
    move v0, v0

    .line 2052379
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2052374
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2052375
    move-object v0, v0

    .line 2052376
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2052365
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2052366
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2052367
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2052368
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2052369
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2052370
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2052371
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2052372
    invoke-static {v3, v9, v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2052373
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2052362
    iget v0, p0, LX/1vt;->c:I

    .line 2052363
    move v0, v0

    .line 2052364
    return v0
.end method
