.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x106e3aee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055328
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2055327
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2055325
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055326
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2055322
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2055323
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055324
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;
    .locals 11

    .prologue
    .line 2055293
    if-nez p0, :cond_0

    .line 2055294
    const/4 p0, 0x0

    .line 2055295
    :goto_0
    return-object p0

    .line 2055296
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    if-eqz v0, :cond_1

    .line 2055297
    check-cast p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    goto :goto_0

    .line 2055298
    :cond_1
    new-instance v0, LX/Dtw;

    invoke-direct {v0}, LX/Dtw;-><init>()V

    .line 2055299
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v1

    iput-object v1, v0, LX/Dtw;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    .line 2055300
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Dtw;->b:Ljava/lang/String;

    .line 2055301
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Dtw;->c:Ljava/lang/String;

    .line 2055302
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Dtw;->d:Ljava/lang/String;

    .line 2055303
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 2055304
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2055305
    iget-object v3, v0, LX/Dtw;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2055306
    iget-object v5, v0, LX/Dtw;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2055307
    iget-object v7, v0, LX/Dtw;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2055308
    iget-object v8, v0, LX/Dtw;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2055309
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 2055310
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 2055311
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2055312
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 2055313
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 2055314
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2055315
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2055316
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2055317
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2055318
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2055319
    new-instance v3, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-direct {v3, v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;-><init>(LX/15i;)V

    .line 2055320
    move-object p0, v3

    .line 2055321
    goto :goto_0
.end method

.method private j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055291
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    .line 2055292
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2055279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055280
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2055281
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2055282
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2055283
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2055284
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2055285
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2055286
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2055287
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2055288
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2055289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055290
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2055271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2055272
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2055273
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    .line 2055274
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2055275
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2055276
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    .line 2055277
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2055278
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055270
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2055267
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;-><init>()V

    .line 2055268
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2055269
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055258
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel$AssetsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055265
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->f:Ljava/lang/String;

    .line 2055266
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055263
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->g:Ljava/lang/String;

    .line 2055264
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2055262
    const v0, -0x28de8a8f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2055260
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->h:Ljava/lang/String;

    .line 2055261
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2055259
    const v0, 0x22399a14

    return v0
.end method
