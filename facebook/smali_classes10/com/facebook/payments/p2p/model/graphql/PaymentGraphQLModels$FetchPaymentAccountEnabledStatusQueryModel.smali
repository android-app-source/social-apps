.class public final Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5097c4a9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053522
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2053523
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2053514
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2053515
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2053516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053517
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2053518
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2053519
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2053520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2053521
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2053506
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2053507
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2053508
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    .line 2053509
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2053510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;

    .line 2053511
    iput-object v0, v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    .line 2053512
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2053513
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2053504
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    .line 2053505
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->e:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2053501
    new-instance v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;

    invoke-direct {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;-><init>()V

    .line 2053502
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2053503
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2053500
    const v0, -0x6d000470

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2053499
    const v0, -0x6747e1ce

    return v0
.end method
