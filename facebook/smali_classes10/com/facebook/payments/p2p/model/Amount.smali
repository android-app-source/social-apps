.class public Lcom/facebook/payments/p2p/model/Amount;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/AmountDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/Amount;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/Amount;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/p2p/model/Amount;

.field public static final b:Lcom/facebook/payments/p2p/model/Amount;


# instance fields
.field private final mAmountWithOffset:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "amount_with_offset"
    .end annotation
.end field

.field private mCurrency:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency"
    .end annotation
.end field

.field private final mOffset:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "offset"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051230
    const-class v0, Lcom/facebook/payments/p2p/model/AmountDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2051222
    new-instance v0, Lcom/facebook/payments/p2p/model/Amount;

    const-string v1, "USD"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/payments/p2p/model/Amount;->a:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051223
    new-instance v0, Lcom/facebook/payments/p2p/model/Amount;

    const-string v1, "USD"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/payments/p2p/model/Amount;->b:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051224
    new-instance v0, LX/Dt8;

    invoke-direct {v0}, LX/Dt8;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/Amount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2051225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051226
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    .line 2051227
    iput v1, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    .line 2051228
    iput v1, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    .line 2051229
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    .line 2051239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    .line 2051240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    .line 2051241
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Amount;->f()Lcom/facebook/payments/p2p/model/Amount;

    .line 2051242
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2051231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051232
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    .line 2051233
    iput p2, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    .line 2051234
    iput p3, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    .line 2051235
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Amount;->f()Lcom/facebook/payments/p2p/model/Amount;

    .line 2051236
    return-void
.end method

.method private f()Lcom/facebook/payments/p2p/model/Amount;
    .locals 1

    .prologue
    .line 2051218
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    .line 2051219
    return-object p0

    .line 2051220
    :cond_0
    const-string v0, "USD"

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2051221
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Amount;->f()Lcom/facebook/payments/p2p/model/Amount;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2051217
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2051216
    iget v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2051215
    iget v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051199
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .locals 2

    .prologue
    .line 2051205
    new-instance v0, LX/Dtl;

    invoke-direct {v0}, LX/Dtl;-><init>()V

    iget v1, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    .line 2051206
    iput v1, v0, LX/Dtl;->a:I

    .line 2051207
    move-object v0, v0

    .line 2051208
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    .line 2051209
    iput-object v1, v0, LX/Dtl;->b:Ljava/lang/String;

    .line 2051210
    move-object v0, v0

    .line 2051211
    iget v1, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    .line 2051212
    iput v1, v0, LX/Dtl;->c:I

    .line 2051213
    move-object v0, v0

    .line 2051214
    invoke-virtual {v0}, LX/Dtl;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051204
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "currency"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "offset"

    iget v2, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "amount_with_offset"

    iget v2, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051200
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mCurrency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051201
    iget v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051202
    iget v0, p0, Lcom/facebook/payments/p2p/model/Amount;->mAmountWithOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051203
    return-void
.end method
