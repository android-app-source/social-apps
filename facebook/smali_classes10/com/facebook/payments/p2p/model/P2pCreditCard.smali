.class public Lcom/facebook/payments/p2p/model/P2pCreditCard;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/P2pCreditCardDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/P2pCreditCard;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/P2pCreditCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAddress:Lcom/facebook/payments/p2p/model/Address;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "address"
    .end annotation
.end field

.field private final mAssociation:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "association"
    .end annotation
.end field

.field private final mCredentialId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "credential_id"
    .end annotation
.end field

.field private final mExpirationMonth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "expire_month"
    .end annotation
.end field

.field private final mExpirationYear:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "expire_year"
    .end annotation
.end field

.field private final mLastFourDigits:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "number"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051393
    const-class v0, Lcom/facebook/payments/p2p/model/P2pCreditCardDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051392
    new-instance v0, LX/DtC;

    invoke-direct {v0}, LX/DtC;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2051355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051356
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mCredentialId:J

    .line 2051357
    iput-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    .line 2051358
    iput v3, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationMonth:I

    .line 2051359
    iput v3, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationYear:I

    .line 2051360
    iput-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAddress:Lcom/facebook/payments/p2p/model/Address;

    .line 2051361
    iput-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    .line 2051362
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2051384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051385
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mCredentialId:J

    .line 2051386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    .line 2051387
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationMonth:I

    .line 2051388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationYear:I

    .line 2051389
    const-class v0, Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Address;

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAddress:Lcom/facebook/payments/p2p/model/Address;

    .line 2051390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    .line 2051391
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2051378
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2051379
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2051380
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2051381
    return-object p0

    :cond_0
    move v0, v2

    .line 2051382
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2051383
    goto :goto_1
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 2051377
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mCredentialId:J

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051376
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2051375
    iget v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationMonth:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051374
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2051373
    iget v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationYear:I

    return v0
.end method

.method public final f()Lcom/facebook/payments/p2p/model/Address;
    .locals 1

    .prologue
    .line 2051372
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAddress:Lcom/facebook/payments/p2p/model/Address;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051371
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2051370
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "credential_id"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mCredentialId:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "number"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "expire_month"

    iget v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationMonth:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "expire_year"

    iget v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationYear:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "address"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAddress:Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/Address;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "association"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2051363
    iget-wide v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mCredentialId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2051364
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mLastFourDigits:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051365
    iget v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051366
    iget v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mExpirationYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2051367
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAddress:Lcom/facebook/payments/p2p/model/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2051368
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/P2pCreditCard;->mAssociation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051369
    return-void
.end method
