.class public Lcom/facebook/payments/p2p/model/Receiver;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;
.implements LX/DtN;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/ReceiverDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/payments/p2p/model/Receiver;",
        ">;",
        "LX/DtN;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/Receiver;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/p2p/model/Receiver;


# instance fields
.field private mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mIsMessengerUser:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_messenger_user"
    .end annotation
.end field

.field private mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051998
    const-class v0, Lcom/facebook/payments/p2p/model/ReceiverDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2051996
    new-instance v0, Lcom/facebook/payments/p2p/model/Receiver;

    const-string v1, "0"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/p2p/model/Receiver;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/payments/p2p/model/Receiver;->a:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051997
    new-instance v0, LX/DtO;

    invoke-direct {v0}, LX/DtO;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/Receiver;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2051991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051992
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    .line 2051993
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    .line 2051994
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    .line 2051995
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051986
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    .line 2051987
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    .line 2051988
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    .line 2051989
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Receiver;->f()Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051990
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2051983
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/p2p/model/Receiver;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2051984
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2051977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051978
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    .line 2051979
    iput-object p2, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    .line 2051980
    iput-boolean p3, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    .line 2051981
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Receiver;->f()Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051982
    return-void
.end method

.method private f()Lcom/facebook/payments/p2p/model/Receiver;
    .locals 1

    .prologue
    .line 2051952
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    .line 2051953
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    .line 2051954
    return-object p0

    .line 2051955
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 2051956
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2051976
    invoke-direct {p0}, Lcom/facebook/payments/p2p/model/Receiver;->f()Lcom/facebook/payments/p2p/model/Receiver;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051975
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051974
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2051973
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051972
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 2

    .prologue
    .line 2051962
    new-instance v0, LX/Dto;

    invoke-direct {v0}, LX/Dto;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    .line 2051963
    iput-object v1, v0, LX/Dto;->b:Ljava/lang/String;

    .line 2051964
    move-object v0, v0

    .line 2051965
    iget-boolean v1, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    .line 2051966
    iput-boolean v1, v0, LX/Dto;->c:Z

    .line 2051967
    move-object v0, v0

    .line 2051968
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    .line 2051969
    iput-object v1, v0, LX/Dto;->d:Ljava/lang/String;

    .line 2051970
    move-object v0, v0

    .line 2051971
    invoke-virtual {v0}, LX/Dto;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051961
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "is_messenger_user"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051957
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051958
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051959
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/model/Receiver;->mIsMessengerUser:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2051960
    return-void
.end method
