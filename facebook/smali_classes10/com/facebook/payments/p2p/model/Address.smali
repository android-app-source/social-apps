.class public Lcom/facebook/payments/p2p/model/Address;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/model/AddressDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/model/Address;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mPostalCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "postal_code"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2051173
    const-class v0, Lcom/facebook/payments/p2p/model/AddressDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051153
    new-instance v0, LX/Dt7;

    invoke-direct {v0}, LX/Dt7;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/model/Address;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2051170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    .line 2051172
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2051167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    .line 2051169
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2051164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051165
    iput-object p1, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    .line 2051166
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051174
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2051163
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2051158
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 2051159
    :goto_0
    return v0

    .line 2051160
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2051161
    :cond_2
    check-cast p1, Lcom/facebook/payments/p2p/model/Address;

    .line 2051162
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2051157
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2051156
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "postal_code"

    iget-object v2, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2051154
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/Address;->mPostalCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2051155
    return-void
.end method
