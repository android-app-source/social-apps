.class public final Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EtW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/net/Uri;

.field public f:Lcom/facebook/common/callercontext/CallerContext;

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final synthetic o:LX/EtW;


# direct methods
.method public constructor <init>(LX/EtW;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 2178233
    iput-object p1, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->o:LX/EtW;

    .line 2178234
    move-object v0, p1

    .line 2178235
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2178236
    iput v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->g:I

    .line 2178237
    iput v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->h:I

    .line 2178238
    iput v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->i:I

    .line 2178239
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->j:I

    .line 2178240
    iput v1, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->k:I

    .line 2178241
    iput v1, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->l:I

    .line 2178242
    iput v1, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->m:I

    .line 2178243
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2178232
    const-string v0, "FigListItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2178189
    if-ne p0, p1, :cond_1

    .line 2178190
    :cond_0
    :goto_0
    return v0

    .line 2178191
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2178192
    goto :goto_0

    .line 2178193
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    .line 2178194
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2178195
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2178196
    if-eq v2, v3, :cond_0

    .line 2178197
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2178198
    goto :goto_0

    .line 2178199
    :cond_5
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2178200
    :cond_6
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2178201
    goto :goto_0

    .line 2178202
    :cond_8
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2178203
    :cond_9
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2178204
    goto :goto_0

    .line 2178205
    :cond_b
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2178206
    :cond_c
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2178207
    goto :goto_0

    .line 2178208
    :cond_e
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    if-nez v2, :cond_d

    .line 2178209
    :cond_f
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->e:Landroid/net/Uri;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->e:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2178210
    goto :goto_0

    .line 2178211
    :cond_11
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->e:Landroid/net/Uri;

    if-nez v2, :cond_10

    .line 2178212
    :cond_12
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2178213
    goto/16 :goto_0

    .line 2178214
    :cond_14
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_13

    .line 2178215
    :cond_15
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->g:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->g:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 2178216
    goto/16 :goto_0

    .line 2178217
    :cond_16
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->h:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->h:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 2178218
    goto/16 :goto_0

    .line 2178219
    :cond_17
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->i:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->i:I

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 2178220
    goto/16 :goto_0

    .line 2178221
    :cond_18
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->j:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->j:I

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 2178222
    goto/16 :goto_0

    .line 2178223
    :cond_19
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->k:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->k:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 2178224
    goto/16 :goto_0

    .line 2178225
    :cond_1a
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->l:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->l:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 2178226
    goto/16 :goto_0

    .line 2178227
    :cond_1b
    iget v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->m:I

    iget v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->m:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 2178228
    goto/16 :goto_0

    .line 2178229
    :cond_1c
    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2178230
    goto/16 :goto_0

    .line 2178231
    :cond_1d
    iget-object v2, p1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2178185
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    .line 2178186
    iget-object v1, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    .line 2178187
    return-object v0

    .line 2178188
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
