.class public Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;
.super Lcom/facebook/messaging/business/search/model/PlatformSearchData;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019196
    new-instance v0, LX/Dd4;

    invoke-direct {v0}, LX/Dd4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Dd5;)V
    .locals 1

    .prologue
    .line 2019174
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;-><init>(LX/Dd3;)V

    .line 2019175
    iget-object v0, p1, LX/Dd5;->a:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019176
    iget-object v0, p1, LX/Dd5;->a:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->e:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2019177
    iget-object v0, p1, LX/Dd5;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->f:Ljava/lang/String;

    .line 2019178
    iget-object v0, p1, LX/Dd5;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->g:Ljava/lang/String;

    .line 2019179
    iget-object v0, p1, LX/Dd5;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->h:Ljava/lang/String;

    .line 2019180
    iget-object v0, p1, LX/Dd5;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->i:Ljava/lang/String;

    .line 2019181
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2019182
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;-><init>(Landroid/os/Parcel;)V

    .line 2019183
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->e:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 2019184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->f:Ljava/lang/String;

    .line 2019185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->g:Ljava/lang/String;

    .line 2019186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->h:Ljava/lang/String;

    .line 2019187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->i:Ljava/lang/String;

    .line 2019188
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2019189
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2019190
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->e:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2019191
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019192
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019193
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019194
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019195
    return-void
.end method
