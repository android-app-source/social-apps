.class public abstract Lcom/facebook/messaging/business/search/model/PlatformSearchData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/user/model/Name;

.field public final b:Lcom/facebook/user/model/PicSquare;

.field public final c:LX/Dd2;

.field public final d:Z


# direct methods
.method public constructor <init>(LX/Dd3;)V
    .locals 1

    .prologue
    .line 2019150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019151
    iget-object v0, p1, LX/Dd3;->a:Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->a:Lcom/facebook/user/model/Name;

    .line 2019152
    iget-object v0, p1, LX/Dd3;->b:Lcom/facebook/user/model/PicSquare;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->b:Lcom/facebook/user/model/PicSquare;

    .line 2019153
    iget-object v0, p1, LX/Dd3;->d:LX/Dd2;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->c:LX/Dd2;

    .line 2019154
    iget-boolean v0, p1, LX/Dd3;->c:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->d:Z

    .line 2019155
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2019156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019157
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->a:Lcom/facebook/user/model/Name;

    .line 2019158
    const-class v0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquare;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->b:Lcom/facebook/user/model/PicSquare;

    .line 2019159
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dd2;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->c:LX/Dd2;

    .line 2019160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->d:Z

    .line 2019161
    return-void

    .line 2019162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2019163
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2019164
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->a:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2019165
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->b:Lcom/facebook/user/model/PicSquare;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2019166
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->c:LX/Dd2;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2019167
    iget-boolean v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2019168
    return-void

    .line 2019169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
