.class public Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;
.super Lcom/facebook/messaging/business/search/model/PlatformSearchData;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/4nY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019235
    new-instance v0, LX/Dd6;

    invoke-direct {v0}, LX/Dd6;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Dd7;)V
    .locals 2

    .prologue
    .line 2019225
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;-><init>(LX/Dd3;)V

    .line 2019226
    iget-object v0, p1, LX/Dd7;->a:Ljava/lang/String;

    const-string v1, "User id can\'t be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019227
    iget-object v0, p1, LX/Dd7;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->e:Ljava/lang/String;

    .line 2019228
    iget-object v0, p1, LX/Dd7;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->f:Ljava/lang/String;

    .line 2019229
    iget-object v0, p1, LX/Dd7;->c:LX/4nY;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->g:LX/4nY;

    .line 2019230
    iget-object v0, p1, LX/Dd7;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->h:Ljava/lang/String;

    .line 2019231
    iget-object v0, p1, LX/Dd7;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->i:Ljava/lang/String;

    .line 2019232
    iget-object v0, p1, LX/Dd7;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->j:Ljava/lang/String;

    .line 2019233
    iget-object v0, p1, LX/Dd7;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->k:Ljava/lang/String;

    .line 2019234
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2019211
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;-><init>(Landroid/os/Parcel;)V

    .line 2019212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->e:Ljava/lang/String;

    .line 2019213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->f:Ljava/lang/String;

    .line 2019214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 2019215
    if-nez v0, :cond_0

    .line 2019216
    :goto_0
    move-object v0, v1

    .line 2019217
    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->g:LX/4nY;

    .line 2019218
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->h:Ljava/lang/String;

    .line 2019219
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->i:Ljava/lang/String;

    .line 2019220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->j:Ljava/lang/String;

    .line 2019221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->k:Ljava/lang/String;

    .line 2019222
    return-void

    .line 2019223
    :cond_0
    :try_start_0
    invoke-static {v0}, LX/4nY;->valueOf(Ljava/lang/String;)LX/4nY;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 2019224
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2019201
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2019202
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019203
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019204
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->g:LX/4nY;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019205
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019206
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019207
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019208
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019209
    return-void

    .line 2019210
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->g:LX/4nY;

    invoke-virtual {v0}, LX/4nY;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
