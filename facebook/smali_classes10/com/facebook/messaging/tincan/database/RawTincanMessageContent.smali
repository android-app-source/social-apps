.class public Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J

.field public final c:[B

.field public final d:[B
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042457
    new-instance v0, LX/Dol;

    invoke-direct {v0}, LX/Dol;-><init>()V

    sput-object v0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJ[B[B)V
    .locals 1
    .param p6    # [B
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2042458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042459
    iput-wide p1, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->a:J

    .line 2042460
    iput-wide p3, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->b:J

    .line 2042461
    iput-object p5, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->c:[B

    .line 2042462
    iput-object p6, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->d:[B

    .line 2042463
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2042464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042465
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->a:J

    .line 2042466
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->b:J

    .line 2042467
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->c:[B

    .line 2042468
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->d:[B

    .line 2042469
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2042470
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2042471
    iget-wide v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2042472
    iget-wide v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2042473
    iget-object v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 2042474
    iget-object v0, p0, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;->d:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 2042475
    return-void
.end method
