.class public Lcom/facebook/messaging/notify/SimpleMessageNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/SimpleMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/push/PushProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031969
    new-instance v0, LX/DiI;

    invoke-direct {v0}, LX/DiI;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031970
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->a:Ljava/lang/String;

    .line 2031972
    const-class v0, Lcom/facebook/push/PushProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->b:Lcom/facebook/push/PushProperty;

    .line 2031973
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V
    .locals 0

    .prologue
    .line 2031974
    invoke-direct {p0, p3}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031975
    iput-object p1, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->a:Ljava/lang/String;

    .line 2031976
    iput-object p2, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->b:Lcom/facebook/push/PushProperty;

    .line 2031977
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031978
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031979
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031980
    iget-object v0, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031981
    iget-object v0, p0, Lcom/facebook/messaging/notify/SimpleMessageNotification;->b:Lcom/facebook/push/PushProperty;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031982
    return-void
.end method
