.class public abstract Lcom/facebook/messaging/notify/NewMessageNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/model/messages/Message;

.field public final c:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/messaging/model/threads/GroupMessageInfo;

.field public final e:LX/DiC;

.field public final f:Lcom/facebook/push/PushProperty;

.field public final g:LX/Dhq;

.field public final h:Z

.field public final i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

.field public final k:LX/DiB;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031804
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031805
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    .line 2031806
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2031807
    const-class v0, Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->d:Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    .line 2031808
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DiC;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    .line 2031809
    const-class v0, Lcom/facebook/push/PushProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    .line 2031810
    const-class v0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    .line 2031811
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 2031812
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DiB;

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    .line 2031813
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031814
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->h:Z

    .line 2031815
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/threads/GroupMessageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/DiC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/Dhq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2031816
    sget-object v0, LX/3RF;->NEW_MESSAGE:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031817
    iput-object p1, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    .line 2031818
    iput-object p2, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2031819
    iput-object p3, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031820
    iput-object p4, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->d:Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    .line 2031821
    iput-object p5, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    .line 2031822
    iput-object p6, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    .line 2031823
    iput-object p7, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 2031824
    iput-boolean p8, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->h:Z

    .line 2031825
    iput-object p9, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    .line 2031826
    iput-object p10, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    .line 2031827
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2031828
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    sget-object v1, LX/3B4;->MQTT:LX/3B4;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-boolean v0, v0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031829
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031830
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2031831
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031832
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031833
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031834
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->d:Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031835
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2031836
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031837
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031838
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2031839
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031840
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031841
    return-void

    .line 2031842
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2031843
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
