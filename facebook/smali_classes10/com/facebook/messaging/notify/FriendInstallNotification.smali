.class public Lcom/facebook/messaging/notify/FriendInstallNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/FriendInstallNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/push/PushProperty;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031505
    new-instance v0, LX/Dhw;

    invoke-direct {v0}, LX/Dhw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/FriendInstallNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031497
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031498
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    .line 2031499
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->b:Ljava/lang/String;

    .line 2031500
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->c:Ljava/lang/String;

    .line 2031501
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->d:Ljava/lang/String;

    .line 2031502
    const-class v0, Lcom/facebook/push/PushProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->e:Lcom/facebook/push/PushProperty;

    .line 2031503
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->f:Z

    .line 2031504
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V
    .locals 1

    .prologue
    .line 2031506
    sget-object v0, LX/3RF;->FRIEND_INSTALL:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031507
    iput-object p1, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    .line 2031508
    iput-object p2, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->b:Ljava/lang/String;

    .line 2031509
    iput-object p3, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->c:Ljava/lang/String;

    .line 2031510
    iput-object p4, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->d:Ljava/lang/String;

    .line 2031511
    iput-object p5, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->e:Lcom/facebook/push/PushProperty;

    .line 2031512
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/0lF;Lcom/facebook/push/PushProperty;)Lcom/facebook/messaging/notify/FriendInstallNotification;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2031492
    const-string v1, "i_id"

    invoke-virtual {p2, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2031493
    :cond_0
    :goto_0
    return-object v0

    .line 2031494
    :cond_1
    const-string v1, "i_id"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 2031495
    if-eqz v1, :cond_0

    .line 2031496
    new-instance v0, Lcom/facebook/messaging/notify/FriendInstallNotification;

    move-object v2, p1

    move-object v3, p0

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/notify/FriendInstallNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031491
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031483
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031484
    iget-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031485
    iget-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031486
    iget-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031487
    iget-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031488
    iget-object v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->e:Lcom/facebook/push/PushProperty;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031489
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/FriendInstallNotification;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031490
    return-void
.end method
