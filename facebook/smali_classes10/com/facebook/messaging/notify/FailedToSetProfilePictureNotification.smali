.class public Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031461
    new-instance v0, LX/Dhv;

    invoke-direct {v0}, LX/Dhv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031474
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031475
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->a:Ljava/lang/String;

    .line 2031476
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    .line 2031477
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->c:Ljava/lang/String;

    .line 2031478
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->d:Z

    .line 2031479
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2031469
    sget-object v0, LX/3RF;->FAILED_TO_SET_PROFILE_PICTURE:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031470
    iput-object p1, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->a:Ljava/lang/String;

    .line 2031471
    iput-object p2, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    .line 2031472
    iput-object p3, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->c:Ljava/lang/String;

    .line 2031473
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031468
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031462
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031463
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031464
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031465
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031466
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031467
    return-void
.end method
