.class public Lcom/facebook/messaging/notify/PromotionNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/PromotionNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031918
    new-instance v0, LX/DiG;

    invoke-direct {v0}, LX/DiG;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/PromotionNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031919
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031920
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->a:Ljava/lang/String;

    .line 2031921
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->b:Ljava/lang/String;

    .line 2031922
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->c:Ljava/lang/String;

    .line 2031923
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->d:Ljava/lang/String;

    .line 2031924
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->e:Z

    .line 2031925
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2031926
    sget-object v0, LX/3RF;->PROMOTION:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031927
    iput-object p1, p0, Lcom/facebook/messaging/notify/PromotionNotification;->a:Ljava/lang/String;

    .line 2031928
    iput-object p2, p0, Lcom/facebook/messaging/notify/PromotionNotification;->b:Ljava/lang/String;

    .line 2031929
    iput-object p3, p0, Lcom/facebook/messaging/notify/PromotionNotification;->c:Ljava/lang/String;

    .line 2031930
    iput-object p4, p0, Lcom/facebook/messaging/notify/PromotionNotification;->d:Ljava/lang/String;

    .line 2031931
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031932
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031933
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031934
    iget-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031935
    iget-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031936
    iget-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031937
    iget-object v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031938
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/PromotionNotification;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031939
    return-void
.end method
