.class public Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031781
    new-instance v0, LX/Di8;

    invoke-direct {v0}, LX/Di8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031773
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031774
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->a:Ljava/lang/String;

    .line 2031775
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->b:Ljava/lang/String;

    .line 2031776
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    .line 2031777
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->d:Ljava/lang/String;

    .line 2031778
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->e:I

    .line 2031779
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->f:Z

    .line 2031780
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2031766
    sget-object v0, LX/3RF;->MULTIPLE_ACCOUNTS_NEW_MESSAGES:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031767
    iput-object p1, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->a:Ljava/lang/String;

    .line 2031768
    iput-object p2, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->b:Ljava/lang/String;

    .line 2031769
    iput-object p3, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    .line 2031770
    iput-object p4, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->d:Ljava/lang/String;

    .line 2031771
    iput p5, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->e:I

    .line 2031772
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031765
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031757
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031758
    iget-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031759
    iget-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031760
    iget-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031761
    iget-object v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031762
    iget v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031763
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031764
    return-void
.end method
