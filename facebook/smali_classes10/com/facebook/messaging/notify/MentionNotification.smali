.class public Lcom/facebook/messaging/notify/MentionNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/MentionNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/model/messages/Message;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031612
    new-instance v0, LX/Di1;

    invoke-direct {v0}, LX/Di1;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/MentionNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031608
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031609
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MentionNotification;->a:Ljava/lang/String;

    .line 2031610
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/notify/MentionNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2031611
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;LX/3RF;)V
    .locals 0

    .prologue
    .line 2031599
    invoke-direct {p0, p3}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031600
    iput-object p1, p0, Lcom/facebook/messaging/notify/MentionNotification;->a:Ljava/lang/String;

    .line 2031601
    iput-object p2, p0, Lcom/facebook/messaging/notify/MentionNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2031602
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031607
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031603
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031604
    iget-object v0, p0, Lcom/facebook/messaging/notify/MentionNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031605
    iget-object v0, p0, Lcom/facebook/messaging/notify/MentionNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031606
    return-void
.end method
