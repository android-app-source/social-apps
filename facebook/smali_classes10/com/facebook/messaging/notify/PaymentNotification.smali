.class public Lcom/facebook/messaging/notify/PaymentNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/PaymentNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/DiF;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/facebook/push/PushProperty;

.field public final i:I

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031876
    new-instance v0, LX/DiD;

    invoke-direct {v0}, LX/DiD;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/PaymentNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/DiE;)V
    .locals 1

    .prologue
    .line 2031877
    sget-object v0, LX/3RF;->P2P_PAYMENT:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031878
    iget-object v0, p1, LX/DiE;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    .line 2031879
    iget-object v0, p1, LX/DiE;->b:LX/DiF;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->b:LX/DiF;

    .line 2031880
    iget-object v0, p1, LX/DiE;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->c:Ljava/lang/String;

    .line 2031881
    iget-object v0, p1, LX/DiE;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    .line 2031882
    iget-object v0, p1, LX/DiE;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->e:Ljava/lang/String;

    .line 2031883
    iget-object v0, p1, LX/DiE;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->f:Ljava/lang/String;

    .line 2031884
    iget-object v0, p1, LX/DiE;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->g:Ljava/lang/String;

    .line 2031885
    iget-object v0, p1, LX/DiE;->h:Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->h:Lcom/facebook/push/PushProperty;

    .line 2031886
    iget v0, p1, LX/DiE;->i:I

    iput v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->i:I

    .line 2031887
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031888
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031889
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    .line 2031890
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DiF;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->b:LX/DiF;

    .line 2031891
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->c:Ljava/lang/String;

    .line 2031892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    .line 2031893
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->e:Ljava/lang/String;

    .line 2031894
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->f:Ljava/lang/String;

    .line 2031895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->g:Ljava/lang/String;

    .line 2031896
    const-class v0, Lcom/facebook/push/PushProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->h:Lcom/facebook/push/PushProperty;

    .line 2031897
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->k:Z

    .line 2031898
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->i:I

    .line 2031899
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2031900
    sget-object v0, LX/3RH;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2031901
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031902
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031903
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031904
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031905
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->b:LX/DiF;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2031906
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031907
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031908
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031909
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031910
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031911
    iget-object v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->h:Lcom/facebook/push/PushProperty;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031912
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031913
    iget v0, p0, Lcom/facebook/messaging/notify/PaymentNotification;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031914
    return-void
.end method
