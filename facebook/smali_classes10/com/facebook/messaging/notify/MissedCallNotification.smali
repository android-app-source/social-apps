.class public Lcom/facebook/messaging/notify/MissedCallNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/MissedCallNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Ljava/lang/Boolean;

.field public final f:Ljava/lang/String;

.field public final g:LX/Dhq;

.field public final h:LX/Di7;

.field public final i:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031730
    new-instance v0, LX/Di6;

    invoke-direct {v0}, LX/Di6;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/MissedCallNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2031718
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031719
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->a:Ljava/lang/String;

    .line 2031720
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->b:Ljava/lang/String;

    .line 2031721
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->c:Ljava/lang/String;

    .line 2031722
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->d:J

    .line 2031723
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->e:Ljava/lang/Boolean;

    .line 2031724
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    .line 2031725
    new-instance v0, LX/Dhq;

    invoke-direct {v0}, LX/Dhq;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->g:LX/Dhq;

    .line 2031726
    invoke-static {}, LX/Di7;->values()[LX/Di7;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    .line 2031727
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031728
    return-void

    .line 2031729
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;LX/Dhq;LX/Di7;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    .line 2031742
    sget-object v0, LX/3RF;->MISSED_CALL:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031743
    iput-object p1, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->a:Ljava/lang/String;

    .line 2031744
    iput-object p2, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->b:Ljava/lang/String;

    .line 2031745
    iput-object p3, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->c:Ljava/lang/String;

    .line 2031746
    iput-wide p4, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->d:J

    .line 2031747
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->e:Ljava/lang/Boolean;

    .line 2031748
    iput-object p7, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    .line 2031749
    iput-object p8, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->g:LX/Dhq;

    .line 2031750
    iput-object p9, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    .line 2031751
    iput-object p10, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031752
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031753
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2031731
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031732
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031733
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031734
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031735
    iget-wide v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2031736
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031737
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031738
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    invoke-virtual {v0}, LX/Di7;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031739
    iget-object v0, p0, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031740
    return-void

    .line 2031741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
