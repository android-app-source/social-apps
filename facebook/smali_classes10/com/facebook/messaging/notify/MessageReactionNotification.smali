.class public Lcom/facebook/messaging/notify/MessageReactionNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/MessageReactionNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031616
    new-instance v0, LX/Di2;

    invoke-direct {v0}, LX/Di2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/MessageReactionNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031617
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031618
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031619
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->b:Ljava/lang/String;

    .line 2031620
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->c:Z

    .line 2031621
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2031622
    sget-object v0, LX/3RF;->MESSAGE_REACTION:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031623
    iput-object p1, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031624
    iput-object p2, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->b:Ljava/lang/String;

    .line 2031625
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031626
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031627
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031628
    iget-object v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031629
    iget-object v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031630
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/MessageReactionNotification;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031631
    return-void
.end method
