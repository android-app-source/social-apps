.class public Lcom/facebook/messaging/notify/EventReminderNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/EventReminderNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public final f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031400
    new-instance v0, LX/Dhs;

    invoke-direct {v0}, LX/Dhs;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/EventReminderNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031401
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->a:Ljava/lang/String;

    .line 2031403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->b:Ljava/lang/String;

    .line 2031404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->c:Ljava/lang/String;

    .line 2031405
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->g:Z

    .line 2031406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->e:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2031407
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->d:Ljava/lang/String;

    .line 2031409
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLightweightEventType;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 2031410
    sget-object v0, LX/3RF;->EVENT_REMINDER:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031411
    iput-object p1, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->a:Ljava/lang/String;

    .line 2031412
    iput-object p2, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->b:Ljava/lang/String;

    .line 2031413
    iput-object p3, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->c:Ljava/lang/String;

    .line 2031414
    iput-object p4, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->d:Ljava/lang/String;

    .line 2031415
    iput-object p5, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->e:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2031416
    iput-object p6, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031417
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031418
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031419
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031420
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031421
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031422
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031423
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031424
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->e:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031425
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031426
    iget-object v0, p0, Lcom/facebook/messaging/notify/EventReminderNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031427
    return-void
.end method
