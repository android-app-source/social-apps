.class public Lcom/facebook/messaging/notify/NewDefaultMessageNotification;
.super Lcom/facebook/messaging/notify/NewMessageNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/NewMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031844
    new-instance v0, LX/DiA;

    invoke-direct {v0}, LX/DiA;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/NewDefaultMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2031845
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/NewMessageNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031846
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/threads/GroupMessageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/DiC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/Dhq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2031847
    invoke-direct/range {p0 .. p10}, Lcom/facebook/messaging/notify/NewMessageNotification;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)V

    .line 2031848
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2031849
    const/16 v0, 0x2710

    return v0
.end method
