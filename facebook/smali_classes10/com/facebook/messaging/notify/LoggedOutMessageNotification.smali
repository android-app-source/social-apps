.class public Lcom/facebook/messaging/notify/LoggedOutMessageNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/LoggedOutMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Lcom/facebook/push/PushProperty;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031595
    new-instance v0, LX/Di0;

    invoke-direct {v0}, LX/Di0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031575
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031576
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->a:Ljava/lang/String;

    .line 2031577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->b:Ljava/lang/String;

    .line 2031578
    const-class v0, Lcom/facebook/push/PushProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    iput-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->c:Lcom/facebook/push/PushProperty;

    .line 2031579
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d:Z

    .line 2031580
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V
    .locals 1

    .prologue
    .line 2031581
    sget-object v0, LX/3RF;->LOGGED_OUT_MESSAGE:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031582
    iput-object p1, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->a:Ljava/lang/String;

    .line 2031583
    iput-object p2, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->b:Ljava/lang/String;

    .line 2031584
    iput-object p3, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->c:Lcom/facebook/push/PushProperty;

    .line 2031585
    return-void
.end method


# virtual methods
.method public final d()LX/3B4;
    .locals 1

    .prologue
    .line 2031586
    iget-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->c:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031587
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2031588
    iget-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->c:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031589
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031590
    iget-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031591
    iget-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031592
    iget-object v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->c:Lcom/facebook/push/PushProperty;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031593
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031594
    return-void
.end method
