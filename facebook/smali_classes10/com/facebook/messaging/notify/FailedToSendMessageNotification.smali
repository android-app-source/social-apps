.class public Lcom/facebook/messaging/notify/FailedToSendMessageNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/FailedToSendMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/Dhu;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031441
    new-instance v0, LX/Dht;

    invoke-direct {v0}, LX/Dht;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031442
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031444
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->c:Z

    .line 2031445
    const-class v0, LX/Dhu;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhu;

    iput-object v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    .line 2031446
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V
    .locals 1

    .prologue
    .line 2031447
    sget-object v0, LX/3RF;->FAILED_TO_SEND:LX/3RF;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(LX/3RF;)V

    .line 2031448
    iput-object p1, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031449
    iput-object p2, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    .line 2031450
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031451
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031452
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031453
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031454
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031455
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031456
    return-void

    .line 2031457
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
