.class public abstract Lcom/facebook/messaging/notify/MessagingNotification;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public a:Z

.field public final j:LX/3RF;


# direct methods
.method public constructor <init>(LX/3RF;)V
    .locals 0

    .prologue
    .line 2031365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031366
    iput-object p1, p0, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    .line 2031367
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031369
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/3RF;

    iput-object v0, p0, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    .line 2031370
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/MessagingNotification;->a:Z

    .line 2031371
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031372
    iget-object v0, p0, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2031373
    iget-boolean v0, p0, Lcom/facebook/messaging/notify/MessagingNotification;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2031374
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2031375
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/notify/MessagingNotification;->a:Z

    .line 2031376
    return-void
.end method
