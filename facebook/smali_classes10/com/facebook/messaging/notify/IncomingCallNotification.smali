.class public Lcom/facebook/messaging/notify/IncomingCallNotification;
.super Lcom/facebook/messaging/notify/MessagingNotification;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/notify/IncomingCallNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Ljava/lang/Boolean;

.field public final f:Ljava/lang/String;

.field public final g:LX/Dhq;

.field public final h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final i:LX/Dhy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031546
    new-instance v0, LX/Dhx;

    invoke-direct {v0}, LX/Dhx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/notify/IncomingCallNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2031522
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;-><init>(Landroid/os/Parcel;)V

    .line 2031523
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->a:Ljava/lang/String;

    .line 2031524
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->b:Ljava/lang/String;

    .line 2031525
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->c:Ljava/lang/String;

    .line 2031526
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->d:J

    .line 2031527
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->e:Ljava/lang/Boolean;

    .line 2031528
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->f:Ljava/lang/String;

    .line 2031529
    new-instance v0, LX/Dhq;

    invoke-direct {v0}, LX/Dhq;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->g:LX/Dhq;

    .line 2031530
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2031531
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dhy;

    iput-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->i:LX/Dhy;

    .line 2031532
    return-void

    .line 2031533
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031545
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2031534
    invoke-super {p0, p1}, Lcom/facebook/messaging/notify/MessagingNotification;->a(Landroid/os/Parcel;)V

    .line 2031535
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031536
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031537
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031538
    iget-wide v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2031539
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031540
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031541
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031542
    iget-object v0, p0, Lcom/facebook/messaging/notify/IncomingCallNotification;->i:LX/Dhy;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2031543
    return-void

    .line 2031544
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
