.class public final Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047734
    new-instance v0, LX/Dpw;

    invoke-direct {v0}, LX/Dpw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2047729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047730
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->a:Ljava/lang/String;

    .line 2047731
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->b:Z

    .line 2047732
    return-void

    .line 2047733
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2047735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047736
    iput-object p1, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->a:Ljava/lang/String;

    .line 2047737
    iput-boolean p2, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->b:Z

    .line 2047738
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2047728
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2047724
    iget-object v0, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2047725
    iget-boolean v0, p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2047726
    return-void

    .line 2047727
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
