.class public Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Ddk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/3OP;

.field private c:Lcom/facebook/widget/text/BetterButton;

.field private d:Landroid/widget/ProgressBar;

.field private e:Lcom/facebook/fbui/glyph/GlyphView;

.field private f:Landroid/animation/AnimatorSet;

.field public g:LX/Ddl;

.field private h:I

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2020077
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2020078
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a()V

    .line 2020079
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2020129
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2020130
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a()V

    .line 2020131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2020126
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2020127
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a()V

    .line 2020128
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2020115
    const-class v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;

    invoke-static {v0, p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2020116
    const v0, 0x7f030ce2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2020117
    const v0, 0x7f0d202e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    .line 2020118
    const v0, 0x7f0d202d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2020119
    const v0, 0x7f0d202c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    .line 2020120
    sget-object v0, LX/Ddl;->SEND:LX/Ddl;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    .line 2020121
    new-instance v0, LX/Ddg;

    invoke-direct {v0, p0}, LX/Ddg;-><init>(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;)V

    .line 2020122
    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2020123
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2020124
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->i:LX/0ad;

    sget v1, LX/Do6;->h:I

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->h:I

    .line 2020125
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 2020111
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2020112
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2020113
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2020114
    return-void
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 2020107
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2020108
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2020109
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2020110
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2020105
    const/16 v0, 0x8

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Landroid/view/View;ZI)V

    .line 2020106
    return-void
.end method

.method private a(Landroid/view/View;ZI)V
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2020097
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    .line 2020098
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2020099
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    sub-float/2addr v1, v0

    invoke-direct {v2, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2020100
    new-instance v0, LX/Ddi;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Ddi;-><init>(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;Landroid/view/View;ZI)V

    invoke-virtual {v2, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2020101
    const-wide/16 v0, 0x12c

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2020102
    invoke-virtual {p1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2020103
    return-void

    :cond_0
    move v0, v1

    .line 2020104
    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->i:LX/0ad;

    return-void
.end method

.method private static a(LX/3OP;)Z
    .locals 1

    .prologue
    .line 2020080
    instance-of v0, p0, LX/DAW;

    if-eqz v0, :cond_0

    .line 2020081
    check-cast p0, LX/DAW;

    .line 2020082
    iget-boolean v0, p0, LX/DAW;->b:Z

    move v0, v0

    .line 2020083
    :goto_0
    return v0

    .line 2020084
    :cond_0
    instance-of v0, p0, LX/3OO;

    if-eqz v0, :cond_1

    .line 2020085
    check-cast p0, LX/3OO;

    .line 2020086
    iget-boolean v0, p0, LX/3OO;->w:Z

    move v0, v0

    .line 2020087
    goto :goto_0

    .line 2020088
    :cond_1
    instance-of v0, p0, LX/DAU;

    if-eqz v0, :cond_2

    .line 2020089
    check-cast p0, LX/DAU;

    .line 2020090
    iget-boolean v0, p0, LX/DAU;->g:Z

    move v0, v0

    .line 2020091
    goto :goto_0

    .line 2020092
    :cond_2
    instance-of v0, p0, LX/DAO;

    if-eqz v0, :cond_3

    .line 2020093
    check-cast p0, LX/DAO;

    .line 2020094
    iget-boolean v0, p0, LX/DAO;->b:Z

    move v0, v0

    .line 2020095
    goto :goto_0

    .line 2020096
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V
    .locals 4

    .prologue
    .line 2020006
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->g:LX/Ddl;

    if-ne p1, v0, :cond_1

    .line 2020007
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2020008
    :cond_1
    iput-object p1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->g:LX/Ddl;

    .line 2020009
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_2

    .line 2020010
    invoke-direct {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e()V

    .line 2020011
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->b(LX/Ddl;Z)V

    .line 2020012
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c(LX/Ddl;Z)V

    .line 2020013
    sget-object v0, LX/Ddj;->a:[I

    invoke-virtual {p1}, LX/Ddl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2020014
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setState with unexpected state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Ddl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2020015
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2020016
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 2020017
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2020018
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->h:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2020019
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 2020020
    :pswitch_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    .line 2020021
    new-instance v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton$2;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton$2;-><init>(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private b(IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2020022
    if-eqz p2, :cond_2

    .line 2020023
    iget-object v3, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v3, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Landroid/view/View;Z)V

    .line 2020024
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    if-nez p1, :cond_1

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Landroid/view/View;Z)V

    .line 2020025
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2020026
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2020027
    goto :goto_1

    .line 2020028
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2020029
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2
.end method

.method private b(LX/Ddl;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2020030
    sget-object v0, LX/Ddj;->a:[I

    invoke-virtual {p1}, LX/Ddl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2020031
    :goto_0
    return-void

    .line 2020032
    :pswitch_0
    if-eqz p2, :cond_0

    .line 2020033
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-direct {p0, v0, v3}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Landroid/view/View;Z)V

    .line 2020034
    :cond_0
    const v0, 0x7f08030c

    invoke-direct {p0, v0, v3}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(IZ)V

    goto :goto_0

    .line 2020035
    :pswitch_1
    if-eqz p2, :cond_1

    .line 2020036
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-direct {p0, v0, v2, v4}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(Landroid/view/View;ZI)V

    .line 2020037
    :cond_1
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->c:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    goto :goto_0

    .line 2020038
    :pswitch_3
    const v0, 0x7f08043f

    invoke-direct {p0, v0, v2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(IZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2020039
    sget-object v0, LX/Ddj;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->g:LX/Ddl;

    invoke-virtual {v1}, LX/Ddl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2020040
    :cond_0
    :goto_0
    return-void

    .line 2020041
    :pswitch_0
    sget-object v0, LX/Ddl;->LOADING:LX/Ddl;

    invoke-static {p0, v0, v2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    .line 2020042
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a:LX/Ddk;

    if-eqz v0, :cond_0

    .line 2020043
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a:LX/Ddk;

    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->b:LX/3OP;

    invoke-interface {v0, v1}, LX/Ddk;->onClick(LX/3OP;)V

    goto :goto_0

    .line 2020044
    :pswitch_1
    sget-object v0, LX/Ddl;->SEND:LX/Ddl;

    invoke-static {p0, v0, v2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(LX/Ddl;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2020045
    sget-object v0, LX/Ddj;->a:[I

    invoke-virtual {p1}, LX/Ddl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2020046
    :goto_0
    return-void

    .line 2020047
    :pswitch_0
    invoke-direct {p0, v3, p2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->b(IZ)V

    goto :goto_0

    .line 2020048
    :pswitch_1
    const v0, 0x7f02081a

    const v1, 0x7f0a02d9

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(II)V

    .line 2020049
    invoke-direct {p0, v2, p2}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->b(IZ)V

    goto :goto_0

    .line 2020050
    :pswitch_2
    const v0, 0x7f0207da

    const v1, 0x7f0a02da

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(II)V

    .line 2020051
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2020052
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 2020053
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    const-string v1, "rotation"

    new-array v2, v6, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2020054
    invoke-virtual {p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 2020055
    iget-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->d:Landroid/widget/ProgressBar;

    const-string v3, "progress"

    new-array v4, v6, [I

    aput v1, v4, v5

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2020056
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    .line 2020057
    iget-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2020058
    iget-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    new-instance v3, LX/Ddh;

    invoke-direct {v3, p0}, LX/Ddh;-><init>(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2020059
    iget-object v2, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v1, v3, v5

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2020060
    return-void

    :array_0
    .array-data 4
        0x0
        0x44340000    # 720.0f
    .end array-data
.end method

.method private setProgressMillis(J)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2020061
    sget-object v1, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v1, v1

    .line 2020062
    invoke-virtual {v1}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 2020063
    sub-long/2addr v2, p1

    .line 2020064
    const-wide/16 v4, 0x0

    cmp-long v1, p1, v4

    if-nez v1, :cond_1

    .line 2020065
    sget-object v1, LX/Ddl;->SEND:LX/Ddl;

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    .line 2020066
    :cond_0
    :goto_0
    return-void

    .line 2020067
    :cond_1
    iget v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->h:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 2020068
    sget-object v1, LX/Ddl;->SENT:LX/Ddl;

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    goto :goto_0

    .line 2020069
    :cond_2
    sget-object v1, LX/Ddl;->LOADING:LX/Ddl;

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    .line 2020070
    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 2020071
    instance-of v6, v0, Landroid/animation/ObjectAnimator;

    if-eqz v6, :cond_3

    .line 2020072
    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 2020073
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V

    .line 2020074
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public setOnUndoButtonActionListener(LX/Ddk;)V
    .locals 0
    .param p1    # LX/Ddk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2020075
    iput-object p1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a:LX/Ddk;

    .line 2020076
    return-void
.end method

.method public setRow(LX/3OP;)V
    .locals 2

    .prologue
    .line 2020001
    iput-object p1, p0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->b:LX/3OP;

    .line 2020002
    invoke-static {p1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a(LX/3OP;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2020003
    sget-object v0, LX/Ddl;->SENT:LX/Ddl;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a$redex0(Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;LX/Ddl;Z)V

    .line 2020004
    :goto_0
    return-void

    .line 2020005
    :cond_0
    invoke-virtual {p1}, LX/3OP;->r()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setProgressMillis(J)V

    goto :goto_0
.end method
