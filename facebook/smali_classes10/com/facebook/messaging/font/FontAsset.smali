.class public Lcom/facebook/messaging/font/FontAsset;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/font/FontAsset;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2021188
    new-instance v0, LX/DeB;

    invoke-direct {v0}, LX/DeB;-><init>()V

    sput-object v0, Lcom/facebook/messaging/font/FontAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2021183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    .line 2021185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    .line 2021186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    .line 2021187
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2021177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021178
    iput-object p1, p0, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    .line 2021179
    iput-object p2, p0, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    .line 2021180
    iput-object p3, p0, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    .line 2021181
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2021182
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2021171
    if-ne p0, p1, :cond_1

    .line 2021172
    :cond_0
    :goto_0
    return v0

    .line 2021173
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2021174
    goto :goto_0

    .line 2021175
    :cond_3
    check-cast p1, Lcom/facebook/messaging/font/FontAsset;

    .line 2021176
    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2021164
    iget v0, p0, Lcom/facebook/messaging/font/FontAsset;->d:I

    if-nez v0, :cond_0

    .line 2021165
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/font/FontAsset;->d:I

    .line 2021166
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/font/FontAsset;->d:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2021167
    iget-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2021168
    iget-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2021169
    iget-object v0, p0, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2021170
    return-void
.end method
