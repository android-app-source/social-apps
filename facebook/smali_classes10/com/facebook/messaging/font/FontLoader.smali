.class public Lcom/facebook/messaging/font/FontLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile g:Lcom/facebook/messaging/font/FontLoader;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/DeE;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DeF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DeD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2021301
    const-class v0, Lcom/facebook/messaging/font/FontLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/font/FontLoader;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2021298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021299
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/font/FontLoader;->b:Ljava/util/Map;

    .line 2021300
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/font/FontLoader;
    .locals 7

    .prologue
    .line 2021277
    sget-object v0, Lcom/facebook/messaging/font/FontLoader;->g:Lcom/facebook/messaging/font/FontLoader;

    if-nez v0, :cond_1

    .line 2021278
    const-class v1, Lcom/facebook/messaging/font/FontLoader;

    monitor-enter v1

    .line 2021279
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/font/FontLoader;->g:Lcom/facebook/messaging/font/FontLoader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2021280
    if-eqz v2, :cond_0

    .line 2021281
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2021282
    new-instance p0, Lcom/facebook/messaging/font/FontLoader;

    invoke-direct {p0}, Lcom/facebook/messaging/font/FontLoader;-><init>()V

    .line 2021283
    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/DeF;->a(LX/0QB;)LX/DeF;

    move-result-object v4

    check-cast v4, LX/DeF;

    invoke-static {v0}, LX/DeD;->a(LX/0QB;)LX/DeD;

    move-result-object v5

    check-cast v5, LX/DeD;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    .line 2021284
    iput-object v3, p0, Lcom/facebook/messaging/font/FontLoader;->c:LX/0TD;

    iput-object v4, p0, Lcom/facebook/messaging/font/FontLoader;->d:LX/DeF;

    iput-object v5, p0, Lcom/facebook/messaging/font/FontLoader;->e:LX/DeD;

    iput-object v6, p0, Lcom/facebook/messaging/font/FontLoader;->f:LX/03V;

    .line 2021285
    move-object v0, p0

    .line 2021286
    sput-object v0, Lcom/facebook/messaging/font/FontLoader;->g:Lcom/facebook/messaging/font/FontLoader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2021287
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2021288
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2021289
    :cond_1
    sget-object v0, Lcom/facebook/messaging/font/FontLoader;->g:Lcom/facebook/messaging/font/FontLoader;

    return-object v0

    .line 2021290
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2021291
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/messaging/font/FontLoader;LX/DeE;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2021292
    new-instance v0, LX/34X;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, LX/DeI;

    invoke-direct {v2, p0, p1}, LX/DeI;-><init>(Lcom/facebook/messaging/font/FontLoader;LX/DeE;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2021293
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/font/FontLoader;->d:LX/DeF;

    invoke-virtual {v1, v0}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2021294
    :catch_0
    move-exception v0

    .line 2021295
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error in fetchFontResource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2021296
    iget-object v2, p0, Lcom/facebook/messaging/font/FontLoader;->f:LX/03V;

    sget-object v3, Lcom/facebook/messaging/font/FontLoader;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2021297
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
