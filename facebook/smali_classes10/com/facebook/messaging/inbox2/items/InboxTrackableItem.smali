.class public final Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:LX/DfY;

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Z

.field public j:I

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027555
    new-instance v0, LX/Dfc;

    invoke-direct {v0}, LX/Dfc;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/DfY;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 2027556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2027557
    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->j:I

    .line 2027558
    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->k:I

    .line 2027559
    iput-wide p1, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->a:J

    .line 2027560
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->b:Ljava/lang/String;

    .line 2027561
    iput-object p4, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->c:Ljava/lang/String;

    .line 2027562
    iput-object p5, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->d:Ljava/lang/String;

    .line 2027563
    iput-object p6, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->e:LX/DfY;

    .line 2027564
    iput-object p7, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->f:Ljava/lang/String;

    .line 2027565
    iput-object p8, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->g:Ljava/lang/String;

    .line 2027566
    iput-boolean p9, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->i:Z

    .line 2027567
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 2027568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2027569
    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->j:I

    .line 2027570
    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->k:I

    .line 2027571
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->a:J

    .line 2027572
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->b:Ljava/lang/String;

    .line 2027573
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->c:Ljava/lang/String;

    .line 2027574
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->d:Ljava/lang/String;

    .line 2027575
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DfY;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->e:LX/DfY;

    .line 2027576
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->f:Ljava/lang/String;

    .line 2027577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->g:Ljava/lang/String;

    .line 2027578
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->h:Ljava/lang/String;

    .line 2027579
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->i:Z

    .line 2027580
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->j:I

    .line 2027581
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->k:I

    .line 2027582
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2027583
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2027584
    iget-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2027585
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027586
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027587
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027588
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->e:LX/DfY;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2027589
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027590
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027591
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027592
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2027593
    iget v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027594
    iget v0, p0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027595
    return-void
.end method
