.class public Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/User;

.field public final b:D

.field public final c:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027551
    new-instance v0, LX/Dfb;

    invoke-direct {v0}, LX/Dfb;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2027546
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027547
    const-class v0, Lcom/facebook/user/model/User;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->a:Lcom/facebook/user/model/User;

    .line 2027548
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->b:D

    .line 2027549
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->c:D

    .line 2027550
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/user/model/User;DD)V
    .locals 0

    .prologue
    .line 2027541
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2027542
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->a:Lcom/facebook/user/model/User;

    .line 2027543
    iput-wide p4, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->b:D

    .line 2027544
    iput-wide p6, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->c:D

    .line 2027545
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027540
    sget-object v0, LX/DfY;->V2_RANKED_USER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2027535
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027536
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->a:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2027537
    iget-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->b:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2027538
    iget-wide v0, p0, Lcom/facebook/messaging/inbox2/items/InboxRankedUserItem;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2027539
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2027534
    sget-object v0, LX/Dfa;->V2_RANKED_USER:LX/Dfa;

    return-object v0
.end method
