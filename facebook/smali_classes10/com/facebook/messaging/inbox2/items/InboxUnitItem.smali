.class public abstract Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final d:LX/51l;

.field public static e:I

.field public static f:I

.field public static g:I


# instance fields
.field private final a:LX/Dfd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

.field public final i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2027341
    const-class v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 2027342
    new-instance v1, LX/51y;

    invoke-direct {v1, v0}, LX/51y;-><init>(I)V

    move-object v0, v1

    .line 2027343
    sput-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    .line 2027344
    const/4 v0, 0x0

    sput v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->e:I

    .line 2027345
    const/4 v0, 0x1

    sput v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->f:I

    .line 2027346
    const/4 v0, 0x2

    sput v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2027347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2027348
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 2027349
    const-class v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 2027350
    const-string v0, "node"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2027351
    const-string v2, "node_item"

    invoke-static {v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2027352
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/Dfd;

    .line 2027353
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2027354
    iput-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2027355
    iput-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    .line 2027356
    const-class v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    .line 2027357
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2027358
    invoke-direct {p0, p1, v0, v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Dfd;)V

    .line 2027359
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;LX/Dfd;)V
    .locals 1

    .prologue
    .line 2027362
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Dfd;)V

    .line 2027363
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2027364
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V
    .locals 1

    .prologue
    .line 2027360
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Dfd;)V

    .line 2027361
    return-void
.end method

.method private constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/Dfd;)V
    .locals 0
    .param p2    # Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2027366
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2027367
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    .line 2027368
    iput-object p2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    .line 2027369
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    .line 2027370
    return-void
.end method


# virtual methods
.method public abstract a()LX/DfY;
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2027338
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->nm_()Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    move-result-object v0

    .line 2027339
    iput p1, v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->j:I

    .line 2027340
    return-void
.end method

.method public a(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 2027331
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2027332
    const-string v1, "node"

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2027333
    const-string v1, "node_item"

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2027334
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2027335
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2027336
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2027337
    return-void
.end method

.method public abstract b()LX/Dfa;
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 2027328
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->nm_()Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    move-result-object v0

    .line 2027329
    iget-wide v2, v0, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;->a:J

    move-wide v0, v2

    .line 2027330
    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2027327
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 3

    .prologue
    .line 2027320
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2027321
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    if-eqz v1, :cond_1

    .line 2027322
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2027323
    :cond_0
    :goto_0
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0

    .line 2027324
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2027325
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    if-eqz v1, :cond_0

    .line 2027326
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    iget-object v1, v1, LX/Dfd;->analyticsString:Ljava/lang/String;

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2027314
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    if-eqz v0, :cond_0

    .line 2027315
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2027316
    :goto_0
    return-object v0

    .line 2027317
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    if-eqz v0, :cond_1

    .line 2027318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a:LX/Dfd;

    iget-object v1, v1, LX/Dfd;->analyticsString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2027319
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final nm_()Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;
    .locals 11

    .prologue
    .line 2027310
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    if-nez v0, :cond_0

    .line 2027311
    new-instance v1, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->e()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a()LX/DfY;

    move-result-object v7

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->n()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    if-nez v0, :cond_1

    const/4 v9, 0x0

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->o()Z

    move-result v10

    invoke-direct/range {v1 .. v10}, Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/DfY;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    .line 2027312
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b:Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;

    return-object v0

    .line 2027313
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->n()Ljava/lang/String;

    move-result-object v9

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 2027308
    invoke-virtual {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027309
    return-void
.end method
