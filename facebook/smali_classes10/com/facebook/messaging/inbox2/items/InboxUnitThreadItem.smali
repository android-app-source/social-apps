.class public Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field private final b:I

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027609
    new-instance v0, LX/Dfe;

    invoke-direct {v0}, LX/Dfe;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2027610
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027611
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2027612
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->b:I

    .line 2027613
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->c:Z

    .line 2027614
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;Lcom/facebook/messaging/model/threads/ThreadSummary;IZ)V
    .locals 0

    .prologue
    .line 2027615
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2027616
    iput-object p3, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2027617
    iput p4, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->b:I

    .line 2027618
    iput-boolean p5, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->c:Z

    .line 2027619
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027620
    sget-object v0, LX/DfY;->V2_THREAD:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2027621
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027622
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2027623
    iget v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027624
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2027625
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 2

    .prologue
    .line 2027626
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    if-eqz v0, :cond_0

    .line 2027627
    sget-object v0, LX/Dfa;->THREAD_WITH_MONTAGE:LX/Dfa;

    .line 2027628
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->b:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    sget-object v0, LX/Dfa;->THREAD_SINGLE:LX/Dfa;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Dfa;->THREAD_MULTI:LX/Dfa;

    goto :goto_0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 2027629
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2027630
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2027631
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->k()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/51h;->a(J)LX/51h;

    .line 2027632
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2027633
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    if-eqz v0, :cond_0

    .line 2027634
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2027635
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
