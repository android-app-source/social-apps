.class public final Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2025628
    const-class v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel;

    new-instance v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2025629
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2025627
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2025614
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2025615
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2025616
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2025617
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2025618
    if-eqz v2, :cond_0

    .line 2025619
    const-string p0, "header_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2025620
    invoke-static {v1, v2, p1}, LX/DfQ;->a(LX/15i;ILX/0nX;)V

    .line 2025621
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2025622
    if-eqz v2, :cond_1

    .line 2025623
    const-string p0, "title_badge"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2025624
    invoke-static {v1, v2, p1}, LX/DfR;->a(LX/15i;ILX/0nX;)V

    .line 2025625
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2025626
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2025613
    check-cast p1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel$Serializer;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$MessengerInbox2SubscriptionNuxUnitConfigFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
