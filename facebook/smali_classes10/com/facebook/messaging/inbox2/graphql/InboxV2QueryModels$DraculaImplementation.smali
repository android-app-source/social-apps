.class public final Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2023529
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2023530
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2023531
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2023532
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2023533
    if-nez p1, :cond_0

    .line 2023534
    :goto_0
    return v0

    .line 2023535
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2023536
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2023537
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023538
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023539
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023540
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023541
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2023542
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023543
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023544
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023545
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023546
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2023547
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023548
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023549
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023550
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023551
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2023552
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023553
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023554
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023555
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023556
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2023557
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    move-result-object v1

    .line 2023558
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2023559
    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(III)I

    move-result v2

    .line 2023560
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2023561
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023562
    invoke-virtual {p3, v4, v2, v0}, LX/186;->a(III)V

    .line 2023563
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2023564
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023565
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023566
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023567
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023568
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2023569
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2023570
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2023571
    const v3, -0x2717cc77

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2023572
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2023573
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2023574
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2023575
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2023576
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023577
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023578
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2023579
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2023580
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2023581
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023582
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2023583
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2023584
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023585
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023586
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023587
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023588
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2023589
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2023590
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2023591
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2023592
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2023593
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7a332bbb -> :sswitch_6
        -0x74f08cbc -> :sswitch_4
        -0x2717cc77 -> :sswitch_7
        -0x23ed579f -> :sswitch_0
        -0x2353c199 -> :sswitch_2
        0x10c4b99a -> :sswitch_5
        0x260287c8 -> :sswitch_8
        0x28d3de8d -> :sswitch_1
        0x44c0742b -> :sswitch_3
        0x6d7d659e -> :sswitch_9
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2023600
    new-instance v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2023594
    sparse-switch p2, :sswitch_data_0

    .line 2023595
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2023596
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2023597
    const v1, -0x2717cc77

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2023598
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7a332bbb -> :sswitch_0
        -0x74f08cbc -> :sswitch_1
        -0x2717cc77 -> :sswitch_1
        -0x23ed579f -> :sswitch_1
        -0x2353c199 -> :sswitch_1
        0x10c4b99a -> :sswitch_1
        0x260287c8 -> :sswitch_1
        0x28d3de8d -> :sswitch_1
        0x44c0742b -> :sswitch_1
        0x6d7d659e -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2023511
    if-eqz p1, :cond_0

    .line 2023512
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;

    move-result-object v1

    .line 2023513
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;

    .line 2023514
    if-eq v0, v1, :cond_0

    .line 2023515
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2023516
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2023599
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2023522
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2023523
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2023524
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2023525
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2023526
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a:LX/15i;

    .line 2023527
    iput p2, p0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->b:I

    .line 2023528
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2023521
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2023520
    new-instance v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2023517
    iget v0, p0, LX/1vt;->c:I

    .line 2023518
    move v0, v0

    .line 2023519
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2023508
    iget v0, p0, LX/1vt;->c:I

    .line 2023509
    move v0, v0

    .line 2023510
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2023505
    iget v0, p0, LX/1vt;->b:I

    .line 2023506
    move v0, v0

    .line 2023507
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2023502
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2023503
    move-object v0, v0

    .line 2023504
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2023493
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2023494
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2023495
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2023496
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2023497
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2023498
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2023499
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2023500
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2023501
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2023490
    iget v0, p0, LX/1vt;->c:I

    .line 2023491
    move v0, v0

    .line 2023492
    return v0
.end method
