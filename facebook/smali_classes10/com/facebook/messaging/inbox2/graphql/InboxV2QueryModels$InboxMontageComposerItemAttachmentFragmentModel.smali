.class public final Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xfe9ea4b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2024326
    const-class v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2024302
    const-class v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2024303
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2024304
    return-void
.end method

.method private a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2024305
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->e:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->e:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    .line 2024306
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->e:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2024307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2024308
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2024309
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2024310
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2024311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2024312
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2024313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2024314
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2024315
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    .line 2024316
    invoke-direct {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->a()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2024317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;

    .line 2024318
    iput-object v0, v1, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;->e:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel$SectionModel;

    .line 2024319
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2024320
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2024321
    new-instance v0, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxMontageComposerItemAttachmentFragmentModel;-><init>()V

    .line 2024322
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2024323
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2024324
    const v0, -0x6c2af02a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2024325
    const v0, 0x77e548ce

    return v0
.end method
