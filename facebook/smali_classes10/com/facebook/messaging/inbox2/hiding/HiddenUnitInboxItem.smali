.class public Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:I

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027371
    new-instance v0, LX/DfV;

    invoke-direct {v0}, LX/DfV;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2027372
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027373
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->a:Ljava/lang/String;

    .line 2027374
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->b:Z

    .line 2027375
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->c:Ljava/lang/String;

    .line 2027376
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->j:I

    .line 2027377
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->k:Z

    .line 2027378
    return-void

    .line 2027379
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027380
    sget-object v0, LX/DfY;->V2_HIDDEN_UNIT:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2027381
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027382
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027383
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2027384
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027385
    iget v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027386
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027387
    return-void

    .line 2027388
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2027389
    sget-object v0, LX/Dfa;->V2_HIDDEN_UNIT:LX/Dfa;

    return-object v0
.end method
