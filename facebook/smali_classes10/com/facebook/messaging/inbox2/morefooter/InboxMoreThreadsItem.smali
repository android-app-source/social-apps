.class public Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/Dfn;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027721
    new-instance v0, LX/Dfl;

    invoke-direct {v0}, LX/Dfl;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2027727
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027728
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dfn;

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->a:LX/Dfn;

    .line 2027729
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->b:Ljava/lang/String;

    .line 2027730
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027731
    sget-object v0, LX/DfY;->V2_MORE_FOOTER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2027723
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027724
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->a:LX/Dfn;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2027725
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027726
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2027722
    sget-object v0, LX/Dfa;->V2_MORE_FOOTER:LX/Dfa;

    return-object v0
.end method
