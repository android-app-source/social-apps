.class public Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027735
    new-instance v0, LX/Dfo;

    invoke-direct {v0}, LX/Dfo;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2027736
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027737
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;->a:I

    .line 2027738
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027739
    sget-object v0, LX/DfY;->V2_SEE_ALL_FOOTER:LX/DfY;

    return-object v0
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2027740
    sget-object v0, LX/Dfa;->V2_SEE_ALL_FOOTER:LX/Dfa;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2027741
    invoke-virtual {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027742
    iget v0, p0, Lcom/facebook/messaging/inbox2/morefooter/InboxUnitSeeAllItem;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027743
    return-void
.end method
