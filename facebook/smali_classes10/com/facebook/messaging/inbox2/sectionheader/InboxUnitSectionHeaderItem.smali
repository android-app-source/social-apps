.class public Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final j:Z

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2027763
    new-instance v0, LX/Dfp;

    invoke-direct {v0}, LX/Dfp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2027756
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2027757
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->a:Ljava/lang/String;

    .line 2027758
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->b:Ljava/lang/String;

    .line 2027759
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->c:I

    .line 2027760
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->j:Z

    .line 2027761
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->k:Z

    .line 2027762
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2027755
    sget-object v0, LX/DfY;->V2_SECTION_HEADER:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2027747
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2027748
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027749
    iget-object v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2027750
    iget v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2027751
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2027752
    iget-boolean v0, p0, Lcom/facebook/messaging/inbox2/sectionheader/InboxUnitSectionHeaderItem;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2027753
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2027754
    sget-object v0, LX/Dfa;->V2_SECTION_HEADER:LX/Dfa;

    return-object v0
.end method
