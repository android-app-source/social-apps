.class public Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2040513
    new-instance v0, LX/Dng;

    invoke-direct {v0}, LX/Dng;-><init>()V

    sput-object v0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2040514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2040515
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    .line 2040516
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    .line 2040517
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->c:Z

    .line 2040518
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->d:Z

    .line 2040519
    return-void
.end method

.method public constructor <init>(ZZZZ)V
    .locals 0

    .prologue
    .line 2040520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2040521
    iput-boolean p1, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    .line 2040522
    iput-boolean p2, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    .line 2040523
    iput-boolean p3, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->c:Z

    .line 2040524
    iput-boolean p4, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->d:Z

    .line 2040525
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2040526
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2040527
    const-class v0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "isDisableSound"

    iget-boolean v2, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isDisableVibrate"

    iget-boolean v2, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isDisableLightScreen"

    iget-boolean v2, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isNotifyAggressively"

    iget-boolean v2, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2040528
    iget-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2040529
    iget-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2040530
    iget-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2040531
    iget-boolean v0, p0, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2040532
    return-void
.end method
