.class public final Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x72ba28dc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2027910
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2027909
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2027907
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2027908
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2027905
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    .line 2027906
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    return-object v0
.end method

.method private j()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2027903
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->f:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->f:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    .line 2027904
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->f:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2027901
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2027902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2027868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2027869
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2027870
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->j()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2027871
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x7f1bf1b8

    invoke-static {v3, v2, v4}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2027872
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2027873
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2027874
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2027875
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2027876
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2027877
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2027886
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2027887
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->j()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2027888
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->j()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    .line 2027889
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->j()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2027890
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;

    .line 2027891
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->f:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel$ContactsModel;

    .line 2027892
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2027893
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7f1bf1b8

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2027894
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2027895
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;

    .line 2027896
    iput v3, v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->g:I

    move-object v1, v0

    .line 2027897
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2027898
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2027899
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2027900
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2027878
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2027879
    const/4 v0, 0x2

    const v1, 0x7f1bf1b8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;->g:I

    .line 2027880
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2027881
    new-instance v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel$NodesModel;-><init>()V

    .line 2027882
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2027883
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2027884
    const v0, 0x48c329fe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2027885
    const v0, -0x789e4c80

    return v0
.end method
