.class public final Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f9c3b40
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2027974
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2027956
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2027972
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2027973
    return-void
.end method

.method private a()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerInviteUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2027970
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->e:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->e:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    .line 2027971
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->e:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2027975
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2027976
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->a()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2027977
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2027978
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2027979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2027980
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2027962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2027963
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->a()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2027964
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->a()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    .line 2027965
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->a()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2027966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;

    .line 2027967
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;->e:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel$MessengerInviteUnitsModel;

    .line 2027968
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2027969
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2027959
    new-instance v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$CombinedInvitesQueryModel;-><init>()V

    .line 2027960
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2027961
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2027958
    const v0, 0x6c47b6e6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2027957
    const v0, -0x6747e1ce

    return v0
.end method
