.class public final Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x19b52238
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2028120
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2028119
    const-class v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2028117
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2028118
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2028115
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->e:Ljava/lang/String;

    .line 2028116
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhones"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2028113
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, 0x4eb61177

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->f:LX/3Sb;

    .line 2028114
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private l()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRepresentedProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2028111
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->g:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->g:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    .line 2028112
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->g:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2028069
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2028070
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2028081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2028082
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2028083
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 2028084
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->l()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2028085
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2028086
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2028087
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2028088
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2028089
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2028090
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2028091
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2028092
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2028093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2028094
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2028095
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2028096
    if-eqz v1, :cond_3

    .line 2028097
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;

    .line 2028098
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->f:LX/3Sb;

    move-object v1, v0

    .line 2028099
    :goto_0
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->l()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2028100
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->l()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    .line 2028101
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->l()Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2028102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;

    .line 2028103
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->g:Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel$RepresentedProfileModel;

    .line 2028104
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2028105
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2028106
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2028107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;

    .line 2028108
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2028109
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2028110
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2028080
    new-instance v0, LX/Dfq;

    invoke-direct {v0, p1}, LX/Dfq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2028079
    invoke-direct {p0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2028077
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2028078
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2028076
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2028073
    new-instance v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$ContactFieldsModel;-><init>()V

    .line 2028074
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2028075
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2028072
    const v0, -0xad19497

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2028071
    const v0, -0x64104400

    return v0
.end method
