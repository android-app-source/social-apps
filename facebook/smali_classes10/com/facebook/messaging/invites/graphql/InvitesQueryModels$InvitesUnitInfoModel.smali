.class public final Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ff41a72
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2029108
    const-class v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2029109
    const-class v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2029110
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2029111
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2029131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2029132
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2029133
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 2029134
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2029135
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2029136
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2029137
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2029138
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2029139
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2029140
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2029141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2029142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2029112
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2029113
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2029114
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2029115
    if-eqz v1, :cond_3

    .line 2029116
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    .line 2029117
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->f:LX/3Sb;

    move-object v1, v0

    .line 2029118
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2029119
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    .line 2029120
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2029121
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    .line 2029122
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->g:Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    .line 2029123
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2029124
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2029125
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2029126
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    .line 2029127
    iput-object v0, v1, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2029128
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2029129
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2029100
    new-instance v0, LX/Dg5;

    invoke-direct {v0, p1}, LX/Dg5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2029130
    invoke-virtual {p0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2029105
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2029106
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2029107
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2029102
    new-instance v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;-><init>()V

    .line 2029103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2029104
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2029101
    const v0, -0x7f727853

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2029099
    const v0, -0x64104400

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2029097
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->e:Ljava/lang/String;

    .line 2029098
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhones"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2029095
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, -0x1819211b

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->f:LX/3Sb;

    .line 2029096
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final l()Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2029093
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->g:Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->g:Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    .line 2029094
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->g:Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel$RepresentedProfileModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2029091
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2029092
    iget-object v0, p0, Lcom/facebook/messaging/invites/graphql/InvitesQueryModels$InvitesUnitInfoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method
