.class public final Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2028209
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2028210
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2028207
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2028208
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2028183
    if-nez p1, :cond_0

    .line 2028184
    :goto_0
    return v0

    .line 2028185
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2028186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2028187
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2028188
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2028189
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2028190
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2028191
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2028192
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2028193
    const v2, 0x220ae97c

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2028194
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2028195
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2028196
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2028197
    :sswitch_2
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 2028198
    const v1, 0x69f66839

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2028199
    const/4 v1, 0x2

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2028200
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2028201
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2028202
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2028203
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2028204
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2028205
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2028206
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x220ae97c -> :sswitch_2
        0x4b7c0623 -> :sswitch_3
        0x4eb61177 -> :sswitch_1
        0x7f1bf1b8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2028174
    if-nez p0, :cond_0

    move v0, v1

    .line 2028175
    :goto_0
    return v0

    .line 2028176
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2028177
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2028178
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2028179
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2028180
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2028181
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2028182
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2028167
    const/4 v7, 0x0

    .line 2028168
    const/4 v1, 0x0

    .line 2028169
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2028170
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2028171
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2028172
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2028173
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2028166
    new-instance v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2028159
    sparse-switch p2, :sswitch_data_0

    .line 2028160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2028161
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2028162
    const v1, 0x220ae97c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2028163
    :goto_0
    :sswitch_1
    return-void

    .line 2028164
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2028165
    const v1, 0x69f66839

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x220ae97c -> :sswitch_2
        0x4b7c0623 -> :sswitch_1
        0x4eb61177 -> :sswitch_0
        0x7f1bf1b8 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2028153
    if-eqz p1, :cond_0

    .line 2028154
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2028155
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    .line 2028156
    if-eq v0, v1, :cond_0

    .line 2028157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2028158
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2028152
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2028211
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2028212
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2028121
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2028122
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2028123
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2028124
    iput p2, p0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->b:I

    .line 2028125
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2028126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2028127
    new-instance v0, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2028128
    iget v0, p0, LX/1vt;->c:I

    .line 2028129
    move v0, v0

    .line 2028130
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2028149
    iget v0, p0, LX/1vt;->c:I

    .line 2028150
    move v0, v0

    .line 2028151
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2028131
    iget v0, p0, LX/1vt;->b:I

    .line 2028132
    move v0, v0

    .line 2028133
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2028134
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2028135
    move-object v0, v0

    .line 2028136
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2028137
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2028138
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2028139
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2028140
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2028141
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2028142
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2028143
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2028144
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/invites/graphql/CombinedInvitesQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2028145
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2028146
    iget v0, p0, LX/1vt;->c:I

    .line 2028147
    move v0, v0

    .line 2028148
    return v0
.end method
