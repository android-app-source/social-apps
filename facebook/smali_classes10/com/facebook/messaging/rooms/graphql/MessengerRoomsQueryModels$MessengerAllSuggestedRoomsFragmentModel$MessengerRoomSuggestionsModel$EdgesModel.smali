.class public final Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7c0f4eb0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040580
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040579
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2040602
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2040603
    return-void
.end method

.method private a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040600
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    .line 2040601
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2040594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040595
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2040596
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2040597
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2040598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040599
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2040586
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040587
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2040588
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    .line 2040589
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2040590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;

    .line 2040591
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    .line 2040592
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040593
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2040583
    new-instance v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel$MessengerRoomSuggestionsModel$EdgesModel;-><init>()V

    .line 2040584
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2040585
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2040582
    const v0, 0x244ee41e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2040581
    const v0, -0x6ab5c7d0

    return v0
.end method
