.class public final Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7898ad70
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040761
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040762
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2040763
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2040764
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040765
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->f:Ljava/lang/String;

    .line 2040766
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2040767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040768
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2040769
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2040770
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2040771
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2040772
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2040773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040774
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2040775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040777
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040778
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->e:Ljava/lang/String;

    .line 2040779
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2040780
    new-instance v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    invoke-direct {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;-><init>()V

    .line 2040781
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2040782
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2040783
    const v0, 0x276aa2fe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2040784
    const v0, 0x3e684b8

    return v0
.end method
