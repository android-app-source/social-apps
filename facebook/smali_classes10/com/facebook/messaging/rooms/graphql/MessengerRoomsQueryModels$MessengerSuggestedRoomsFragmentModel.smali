.class public final Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41757162
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040907
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040906
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2040904
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2040905
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2040892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040893
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2040894
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2040895
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2040896
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2040897
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2040898
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2040899
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2040900
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2040901
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2040902
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040903
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2040890
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->e:Ljava/util/List;

    .line 2040891
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2040877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040878
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2040879
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2040880
    if-eqz v1, :cond_2

    .line 2040881
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    .line 2040882
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2040883
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2040884
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040885
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2040886
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    .line 2040887
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->g:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040888
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040889
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2040874
    new-instance v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;-><init>()V

    .line 2040875
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2040876
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2040873
    const v0, 0x79558c23    # 6.9300096E34f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2040872
    const v0, 0x69101312

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040866
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->f:Ljava/lang/String;

    .line 2040867
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040870
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->g:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->g:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040871
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->g:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040868
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->h:Ljava/lang/String;

    .line 2040869
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method
