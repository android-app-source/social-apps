.class public final Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5Wg;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4778ac28
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040811
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2040812
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2040813
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2040814
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040835
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->f:Ljava/lang/String;

    .line 2040836
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2040815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040816
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2040817
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2040818
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2040819
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2040820
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2040821
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2040822
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2040823
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2040824
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2040825
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2040826
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2040827
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2040828
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2040829
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2040830
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2040831
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2040832
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2040833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040834
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2040838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2040839
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2040840
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 2040841
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2040842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040843
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 2040844
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2040845
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    .line 2040846
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2040847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040848
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->h:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    .line 2040849
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2040850
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    .line 2040851
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2040852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040853
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->i:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    .line 2040854
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2040855
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 2040856
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2040857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040858
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 2040859
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2040860
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    .line 2040861
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2040862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    .line 2040863
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    .line 2040864
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2040865
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040837
    invoke-virtual {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2040807
    new-instance v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;

    invoke-direct {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;-><init>()V

    .line 2040808
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2040809
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2040810
    const v0, -0x401c3d58

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2040806
    const v0, -0x20950cd6

    return v0
.end method

.method public final j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040804
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 2040805
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040802
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->g:Ljava/lang/String;

    .line 2040803
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040800
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->h:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->h:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    .line 2040801
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->h:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$ImageModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040798
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->i:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->i:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    .line 2040799
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->i:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel$JoinableModeModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040796
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 2040797
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->j:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040794
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k:Ljava/lang/String;

    .line 2040795
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2040792
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    .line 2040793
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerSuggestedRoomsFragmentModel$SuggestedThreadModel;->l:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$FriendThreadQueueParticipantsModel$ThreadQueueParticipantsModel;

    return-object v0
.end method
