.class public final Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d5f1b90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2041202
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2041201
    const-class v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2041178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2041179
    return-void
.end method

.method private a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2041193
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    iput-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    .line 2041194
    iget-object v0, p0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2041195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2041196
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2041197
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2041198
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2041199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2041200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2041185
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2041186
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2041187
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    .line 2041188
    invoke-direct {p0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->a()Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2041189
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;

    .line 2041190
    iput-object v0, v1, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;->e:Lcom/facebook/messaging/rooms/graphql/MessengerRoomsQueryModels$MessengerAllSuggestedRoomsFragmentModel;

    .line 2041191
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2041192
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2041182
    new-instance v0, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;

    invoke-direct {v0}, Lcom/facebook/messaging/rooms/graphql/MessengerRoomsSuggestionMutationModels$MessengerHideRoomSuggestionMutationModel;-><init>()V

    .line 2041183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2041184
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2041181
    const v0, 0x450043cd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2041180
    const v0, -0x2b1c9f83

    return v0
.end method
