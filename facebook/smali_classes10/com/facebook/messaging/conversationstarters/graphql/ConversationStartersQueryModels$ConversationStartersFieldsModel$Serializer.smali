.class public final Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2020818
    const-class v0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;

    new-instance v1, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2020819
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2020817
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2020776
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2020777
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2020778
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2020779
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2020780
    if-eqz v2, :cond_0

    .line 2020781
    const-string p0, "call_to_action_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020782
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2020783
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2020784
    if-eqz v2, :cond_1

    .line 2020785
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020786
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2020787
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020788
    if-eqz v2, :cond_2

    .line 2020789
    const-string p0, "item_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020790
    invoke-static {v1, v2, p1}, LX/De3;->a(LX/15i;ILX/0nX;)V

    .line 2020791
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020792
    if-eqz v2, :cond_3

    .line 2020793
    const-string p0, "item_description_icon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020794
    invoke-static {v1, v2, p1}, LX/De2;->a(LX/15i;ILX/0nX;)V

    .line 2020795
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020796
    if-eqz v2, :cond_4

    .line 2020797
    const-string p0, "item_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020798
    invoke-static {v1, v2, p1}, LX/De4;->a(LX/15i;ILX/0nX;)V

    .line 2020799
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020800
    if-eqz v2, :cond_5

    .line 2020801
    const-string p0, "item_thread"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020802
    invoke-static {v1, v2, p1, p2}, LX/De6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2020803
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020804
    if-eqz v2, :cond_6

    .line 2020805
    const-string p0, "item_user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020806
    invoke-static {v1, v2, p1}, LX/De7;->a(LX/15i;ILX/0nX;)V

    .line 2020807
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2020808
    if-eqz v2, :cond_7

    .line 2020809
    const-string p0, "mcs_item_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020810
    invoke-static {v1, v2, p1}, LX/De8;->a(LX/15i;ILX/0nX;)V

    .line 2020811
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2020812
    if-eqz v2, :cond_8

    .line 2020813
    const-string p0, "show_presence"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020814
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2020815
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2020816
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2020775
    check-cast p1, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Serializer;->a(Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
