.class public final Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2020526
    const-class v0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;

    new-instance v1, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2020527
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2020525
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2020528
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2020529
    const/4 v10, 0x0

    .line 2020530
    const/4 v9, 0x0

    .line 2020531
    const/4 v8, 0x0

    .line 2020532
    const/4 v7, 0x0

    .line 2020533
    const/4 v6, 0x0

    .line 2020534
    const/4 v5, 0x0

    .line 2020535
    const/4 v4, 0x0

    .line 2020536
    const/4 v3, 0x0

    .line 2020537
    const/4 v2, 0x0

    .line 2020538
    const/4 v1, 0x0

    .line 2020539
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, p0, :cond_2

    .line 2020540
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2020541
    const/4 v1, 0x0

    .line 2020542
    :goto_0
    move v1, v1

    .line 2020543
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2020544
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2020545
    new-instance v1, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;

    invoke-direct {v1}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$ConversationStartersFieldsModel;-><init>()V

    .line 2020546
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2020547
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2020548
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2020549
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2020550
    :cond_0
    return-object v1

    .line 2020551
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2020552
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 2020553
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2020554
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2020555
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 2020556
    const-string p0, "call_to_action_url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2020557
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2020558
    :cond_3
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2020559
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2020560
    :cond_4
    const-string p0, "item_description"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2020561
    invoke-static {p1, v0}, LX/De3;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2020562
    :cond_5
    const-string p0, "item_description_icon"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2020563
    invoke-static {p1, v0}, LX/De2;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2020564
    :cond_6
    const-string p0, "item_image"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2020565
    invoke-static {p1, v0}, LX/De4;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2020566
    :cond_7
    const-string p0, "item_thread"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2020567
    invoke-static {p1, v0}, LX/De6;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2020568
    :cond_8
    const-string p0, "item_user"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2020569
    invoke-static {p1, v0}, LX/De7;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2020570
    :cond_9
    const-string p0, "mcs_item_title"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 2020571
    invoke-static {p1, v0}, LX/De8;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2020572
    :cond_a
    const-string p0, "show_presence"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 2020573
    const/4 v1, 0x1

    .line 2020574
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v2

    goto/16 :goto_1

    .line 2020575
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2020576
    const/4 v11, 0x0

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2020577
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2020578
    const/4 v9, 0x2

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2020579
    const/4 v8, 0x3

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2020580
    const/4 v7, 0x4

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 2020581
    const/4 v6, 0x5

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2020582
    const/4 v5, 0x6

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2020583
    const/4 v4, 0x7

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2020584
    if-eqz v1, :cond_c

    .line 2020585
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2020586
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
