.class public final Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2020965
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2020966
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2020963
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2020964
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2020944
    if-nez p1, :cond_0

    .line 2020945
    :goto_0
    return v0

    .line 2020946
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2020947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2020948
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2020949
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2020950
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2020951
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2020952
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2020953
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2020954
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2020955
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2020956
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2020957
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2020958
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2020959
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2020960
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2020961
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2020962
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4f7b48a1 -> :sswitch_2
        -0xc7b52ea -> :sswitch_0
        0x33ef1533 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2020943
    new-instance v0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2020940
    sparse-switch p0, :sswitch_data_0

    .line 2020941
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2020942
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4f7b48a1 -> :sswitch_0
        -0xc7b52ea -> :sswitch_0
        0x33ef1533 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2020939
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2020937
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->b(I)V

    .line 2020938
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2020967
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2020968
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2020969
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2020970
    iput p2, p0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->b:I

    .line 2020971
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2020936
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2020935
    new-instance v0, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2020932
    iget v0, p0, LX/1vt;->c:I

    .line 2020933
    move v0, v0

    .line 2020934
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2020929
    iget v0, p0, LX/1vt;->c:I

    .line 2020930
    move v0, v0

    .line 2020931
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2020926
    iget v0, p0, LX/1vt;->b:I

    .line 2020927
    move v0, v0

    .line 2020928
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2020923
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2020924
    move-object v0, v0

    .line 2020925
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2020914
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2020915
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2020916
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2020917
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2020918
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2020919
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2020920
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2020921
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/conversationstarters/graphql/ConversationStartersQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2020922
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2020911
    iget v0, p0, LX/1vt;->c:I

    .line 2020912
    move v0, v0

    .line 2020913
    return v0
.end method
