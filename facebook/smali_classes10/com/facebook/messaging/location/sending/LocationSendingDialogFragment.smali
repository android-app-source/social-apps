.class public Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final r:[Ljava/lang/String;


# instance fields
.field public m:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/476;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Dde;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/DgO;

.field public t:Landroid/view/MenuItem;

.field public u:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

.field public w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

.field private x:LX/475;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2029485
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2029483
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    .line 2029484
    return-void
.end method

.method public static c(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;Z)V
    .locals 2

    .prologue
    .line 2029478
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2029479
    if-eqz v0, :cond_0

    const-string v1, "omni_m_action_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2029480
    const-string v1, "omni_m_action_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2029481
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->q:LX/Dde;

    invoke-virtual {v1, v0, p1}, LX/Dde;->a(Ljava/lang/String;Z)V

    .line 2029482
    :cond_0
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 2029465
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2029466
    const-string v0, "button_style"

    sget-object v2, LX/DgH;->SEND:LX/DgH;

    invoke-virtual {v2}, LX/DgH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DgH;->valueOf(Ljava/lang/String;)LX/DgH;

    move-result-object v0

    .line 2029467
    new-instance v2, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;-><init>()V

    .line 2029468
    iput-object v0, v2, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->k:LX/DgH;

    .line 2029469
    invoke-static {v2}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->k(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029470
    const-string v0, "initial_pinned_location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 2029471
    if-eqz v0, :cond_0

    .line 2029472
    iput-object v0, v2, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    .line 2029473
    :cond_0
    const-string v0, "initial_nearby_place"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2029474
    if-eqz v0, :cond_1

    .line 2029475
    iput-object v0, v2, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->n:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2029476
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d19ee

    const-string v3, "main_location_sending"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2029477
    return-void
.end method

.method public static r(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)Z
    .locals 1

    .prologue
    .line 2029464
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2029460
    invoke-static {p0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2029461
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->t:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 2029462
    const/4 v0, 0x1

    .line 2029463
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->S_()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2029388
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2029389
    new-instance v1, LX/DgS;

    invoke-direct {v1, p0}, LX/DgS;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2029390
    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2029446
    const/16 v0, 0x676

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2029447
    const-string v0, "extra_permission_results"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 2029448
    const/4 p3, 0x0

    .line 2029449
    sget-object p1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r:[Ljava/lang/String;

    aget-object p1, p1, p3

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    .line 2029450
    if-eqz p1, :cond_0

    .line 2029451
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    .line 2029452
    :cond_0
    :goto_0
    return-void

    .line 2029453
    :pswitch_0
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->u:LX/4ob;

    invoke-virtual {p1}, LX/4ob;->d()V

    .line 2029454
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->t:Landroid/view/MenuItem;

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2029455
    :pswitch_1
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p2, LX/Cuy;->d:LX/0Tn;

    const/4 p3, 0x0

    invoke-interface {p1, p2, p3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2029456
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->n:LX/1Ml;

    invoke-virtual {p1}, LX/1Ml;->d()V

    .line 2029457
    :goto_1
    goto :goto_0

    .line 2029458
    :pswitch_2
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    sget-object p2, LX/Cuy;->d:LX/0Tn;

    invoke-interface {p1, p2, p3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p1

    invoke-interface {p1}, LX/0hN;->commit()V

    goto :goto_0

    .line 2029459
    :cond_1
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    sget-object p2, LX/Cuy;->d:LX/0Tn;

    const/4 p3, 0x1

    invoke-interface {p1, p2, p3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p1

    invoke-interface {p1}, LX/0hN;->commit()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2029434
    instance-of v0, p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    if-eqz v0, :cond_1

    .line 2029435
    check-cast p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029436
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->v:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029437
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->v:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    new-instance v1, LX/DgP;

    invoke-direct {v1, p0}, LX/DgP;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    .line 2029438
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->d:LX/DgO;

    .line 2029439
    :cond_0
    :goto_0
    return-void

    .line 2029440
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    if-eqz v0, :cond_0

    .line 2029441
    check-cast p1, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    .line 2029442
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    .line 2029443
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    new-instance v1, LX/DgR;

    invoke-direct {v1, p0}, LX/DgR;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    .line 2029444
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->d:LX/DgQ;

    .line 2029445
    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5fe1e0aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029431
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2029432
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v6

    check-cast v6, LX/1Ml;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const-class p1, LX/476;

    invoke-interface {v1, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/476;

    invoke-static {v1}, LX/Dde;->a(LX/0QB;)LX/Dde;

    move-result-object v1

    check-cast v1, LX/Dde;

    iput-object v5, v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->n:LX/1Ml;

    iput-object v7, v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->o:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->p:LX/476;

    iput-object v1, v4, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->q:LX/Dde;

    .line 2029433
    const/16 v1, 0x2b

    const v2, -0x1be72820

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b1f8f2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029429
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2029430
    const v2, 0x7f030a47

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6ee44793

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b3219c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029422
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onDestroy()V

    .line 2029423
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->x:LX/475;

    invoke-virtual {v1}, LX/475;->b()V

    .line 2029424
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2029425
    if-eqz v1, :cond_0

    const-string v2, "omni_m_action_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2029426
    const-string v2, "omni_m_action_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2029427
    iget-object v2, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->q:LX/Dde;

    invoke-virtual {v2, v1}, LX/Dde;->a(Ljava/lang/String;)V

    .line 2029428
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7b8c5e9d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x218f9c03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029418
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onResume()V

    .line 2029419
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "main_location_sending"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->n:LX/1Ml;

    sget-object v2, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r:[Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2029420
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->o()V

    .line 2029421
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1119abc1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2029391
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->p:LX/476;

    .line 2029392
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2029393
    invoke-virtual {v0, v1}, LX/476;->a(Landroid/view/View;)LX/475;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->x:LX/475;

    .line 2029394
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->x:LX/475;

    invoke-virtual {v0}, LX/475;->a()V

    .line 2029395
    const v0, 0x7f0d19ef

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->u:LX/4ob;

    .line 2029396
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->u:LX/4ob;

    new-instance v1, LX/DgJ;

    invoke-direct {v1, p0}, LX/DgJ;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    .line 2029397
    iput-object v1, v0, LX/4ob;->c:LX/4oa;

    .line 2029398
    const v0, 0x7f0d143c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 2029399
    const v1, 0x7f082c01

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 2029400
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2029401
    if-eqz v1, :cond_0

    .line 2029402
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2029403
    const-string v2, "show_dismiss_button"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2029404
    new-instance v1, LX/DgK;

    invoke-direct {v1, p0}, LX/DgK;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2029405
    :goto_0
    const v1, 0x7f110034

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 2029406
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0d323a

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->t:Landroid/view/MenuItem;

    .line 2029407
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->t:Landroid/view/MenuItem;

    new-instance v1, LX/DgM;

    invoke-direct {v1, p0}, LX/DgM;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    invoke-static {v0, v1}, LX/3rl;->a(Landroid/view/MenuItem;LX/3rk;)Landroid/view/MenuItem;

    .line 2029408
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->t:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    .line 2029409
    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setMaxWidth(I)V

    .line 2029410
    const v1, 0x7f082ed9

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2029411
    new-instance v1, LX/DgN;

    invoke-direct {v1, p0}, LX/DgN;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V

    .line 2029412
    iput-object v1, v0, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2029413
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->n:LX/1Ml;

    sget-object v1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2029414
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2029415
    :goto_1
    return-void

    .line 2029416
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2029417
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->o()V

    goto :goto_1
.end method
