.class public Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DgD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DgO;

.field public e:Lcom/facebook/messaging/location/sending/LocationSendingView;

.field public f:LX/Dgb;

.field public g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

.field public h:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/messaging/location/sending/NearbyPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/DgH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/messaging/location/sending/NearbyPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2029574
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2029575
    sget-object v0, LX/Dgb;->UNSET:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-static {v3}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v1

    check-cast v1, LX/6Zb;

    new-instance v0, LX/DgD;

    invoke-static {v3}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v2

    check-cast v2, LX/0y3;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    invoke-direct {v0, v2, p0}, LX/DgD;-><init>(LX/0y3;LX/0ad;)V

    move-object v2, v0

    check-cast v2, LX/DgD;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    iput-object v1, p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->a:LX/6Zb;

    iput-object v2, p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->b:LX/DgD;

    iput-object v3, p1, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->c:LX/03V;

    return-void
.end method

.method public static c(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;Lcom/facebook/android/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 2029569
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->j:Lcom/facebook/android/maps/model/LatLng;

    .line 2029570
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->b()V

    .line 2029571
    sget-object v0, LX/Dgb;->PINNED_LOCATION:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    .line 2029572
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b()V

    .line 2029573
    return-void
.end method

.method public static e(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V
    .locals 2

    .prologue
    .line 2029564
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->h:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2029565
    :goto_0
    return-void

    .line 2029566
    :cond_0
    sget-object v0, LX/Dgb;->USER_LOCATION:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    .line 2029567
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->h:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a(Landroid/location/Location;)V

    .line 2029568
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->a()V

    goto :goto_0
.end method

.method public static k(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V
    .locals 2

    .prologue
    .line 2029557
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->k:LX/DgH;

    if-nez v0, :cond_1

    .line 2029558
    :cond_0
    :goto_0
    return-void

    .line 2029559
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->k:LX/DgH;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setButtonStyle(LX/DgH;)V

    .line 2029560
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_2

    .line 2029561
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->c(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029562
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->n:Lcom/facebook/messaging/location/sending/NearbyPlace;

    if-eqz v0, :cond_0

    .line 2029563
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->n:Lcom/facebook/messaging/location/sending/NearbyPlace;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->b(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2029552
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2029553
    const-class v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2029554
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2029555
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->a:LX/6Zb;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->b:LX/DgD;

    invoke-virtual {v0, p0, v1}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2029556
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/messaging/location/sending/NearbyPlace;)V
    .locals 1

    .prologue
    .line 2029547
    sget-object v0, LX/Dgb;->NEARBY_PLACE:LX/Dgb;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    .line 2029548
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->i:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2029549
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    .line 2029550
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/location/sending/LocationSendingView;->a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    .line 2029551
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2029535
    instance-of v0, p1, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    if-eqz v0, :cond_1

    .line 2029536
    check-cast p1, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    .line 2029537
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->l:Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    .line 2029538
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->l:Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    new-instance v1, LX/DgV;

    invoke-direct {v1, p0}, LX/DgV;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029539
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->c:LX/DgQ;

    .line 2029540
    :cond_0
    :goto_0
    return-void

    .line 2029541
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    if-eqz v0, :cond_0

    .line 2029542
    check-cast p1, Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    .line 2029543
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    .line 2029544
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    new-instance v1, LX/DgX;

    invoke-direct {v1, p0}, LX/DgX;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029545
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->f:LX/DgW;

    .line 2029546
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ac3d6aa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029534
    const v1, 0x7f030a48

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x29e605c5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42e93a88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029530
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2029531
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2029532
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->a:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2029533
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x19c3a676

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c461538

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029522
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2029523
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2029524
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->b:LX/DgD;

    iget-object v2, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->a:LX/6Zb;

    .line 2029525
    iget-object v4, v1, LX/DgD;->a:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->a()LX/0yG;

    move-result-object v4

    sget-object v5, LX/0yG;->OKAY:LX/0yG;

    if-eq v4, v5, :cond_0

    iget-object v4, v1, LX/DgD;->b:LX/0ad;

    sget-short v5, LX/DgB;->a:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2029526
    iput-object p0, v1, LX/DgD;->d:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029527
    iput-object v2, v1, LX/DgD;->c:LX/6Zb;

    .line 2029528
    iget-object v4, v1, LX/DgD;->c:LX/6Zb;

    new-instance v5, LX/2si;

    invoke-direct {v5}, LX/2si;-><init>()V

    const-string v6, "surface_location_sharing"

    const-string v7, "mechanism_location_sharing_button"

    invoke-virtual {v4, v5, v6, v7}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2029529
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x68bdc946

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2029516
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2029517
    const p1, 0x7f0d19f2

    invoke-virtual {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/messaging/location/sending/LocationSendingView;

    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    .line 2029518
    iget-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e:Lcom/facebook/messaging/location/sending/LocationSendingView;

    new-instance p2, LX/DgU;

    invoke-direct {p2, p0}, LX/DgU;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029519
    iput-object p2, p1, Lcom/facebook/messaging/location/sending/LocationSendingView;->a:LX/DgT;

    .line 2029520
    invoke-static {p0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->k(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029521
    return-void
.end method
