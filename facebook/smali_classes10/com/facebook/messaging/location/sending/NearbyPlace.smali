.class public Lcom/facebook/messaging/location/sending/NearbyPlace;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/location/sending/NearbyPlace;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:Lcom/facebook/android/maps/model/LatLng;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2029790
    new-instance v0, LX/Dgj;

    invoke-direct {v0}, LX/Dgj;-><init>()V

    sput-object v0, Lcom/facebook/messaging/location/sending/NearbyPlace;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Dgk;)V
    .locals 1

    .prologue
    .line 2029776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029777
    iget-object v0, p1, LX/Dgk;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2029778
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->a:Ljava/lang/String;

    .line 2029779
    iget-object v0, p1, LX/Dgk;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2029780
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    .line 2029781
    iget-object v0, p1, LX/Dgk;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 2029782
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->c:Landroid/net/Uri;

    .line 2029783
    iget-object v0, p1, LX/Dgk;->d:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v0

    .line 2029784
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->d:Lcom/facebook/android/maps/model/LatLng;

    .line 2029785
    iget-object v0, p1, LX/Dgk;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2029786
    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->e:Ljava/lang/String;

    .line 2029787
    iget-object v0, p1, LX/Dgk;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2029788
    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    .line 2029789
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2029760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029761
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->a:Ljava/lang/String;

    .line 2029762
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    .line 2029763
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->c:Landroid/net/Uri;

    .line 2029764
    const-class v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->d:Lcom/facebook/android/maps/model/LatLng;

    .line 2029765
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->e:Ljava/lang/String;

    .line 2029766
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    .line 2029767
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2029775
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2029768
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2029769
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2029770
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2029771
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->d:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2029772
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2029773
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2029774
    return-void
.end method
