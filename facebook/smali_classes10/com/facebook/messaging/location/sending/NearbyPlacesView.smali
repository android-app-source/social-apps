.class public Lcom/facebook/messaging/location/sending/NearbyPlacesView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/DgQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/support/v7/widget/RecyclerView;

.field private d:LX/Dgm;

.field private e:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2029998
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2029999
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b()V

    .line 2030000
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2030042
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2030043
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b()V

    .line 2030044
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2030039
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2030040
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b()V

    .line 2030041
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/location/sending/NearbyPlacesView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2030033
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    .line 2030034
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->d:LX/Dgm;

    .line 2030035
    iget-object p1, v1, LX/Dgm;->a:LX/0Px;

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/messaging/location/sending/NearbyPlace;

    move-object v0, p1

    .line 2030036
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a:LX/DgQ;

    if-eqz v1, :cond_0

    .line 2030037
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a:LX/DgQ;

    invoke-interface {v1, v0}, LX/DgQ;->a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    .line 2030038
    :cond_0
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2030027
    const v0, 0x7f030bcd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2030028
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b:Landroid/view/View;

    .line 2030029
    const v0, 0x7f0d1d66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    .line 2030030
    const v0, 0x7f0d1d65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->e:LX/4ob;

    .line 2030031
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->e()V

    .line 2030032
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2030022
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->getContext()Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1P1;-><init>(IZ)V

    .line 2030023
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2030024
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->f()V

    .line 2030025
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->d:LX/Dgm;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2030026
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2030020
    new-instance v0, LX/Dgm;

    new-instance v1, LX/Dgv;

    invoke-direct {v1, p0}, LX/Dgv;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesView;)V

    invoke-direct {v0, v1}, LX/Dgm;-><init>(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->d:LX/Dgm;

    .line 2030021
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2030016
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2030017
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2030018
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->e:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2030019
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/location/sending/NearbyPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2030009
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->d:LX/Dgm;

    .line 2030010
    iput-object p1, v0, LX/Dgm;->a:LX/0Px;

    .line 2030011
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2030012
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2030013
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2030014
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->e:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2030015
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2030003
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2030004
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2030005
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->e:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2030006
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2030007
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2030008
    return-void
.end method

.method public setNearbyPlaceClickListener(LX/DgQ;)V
    .locals 0

    .prologue
    .line 2030001
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a:LX/DgQ;

    .line 2030002
    return-void
.end method
