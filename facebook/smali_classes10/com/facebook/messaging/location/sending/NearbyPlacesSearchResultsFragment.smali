.class public Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;

.field public c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

.field public d:LX/DgQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2029974
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-static {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b(LX/0QB;)Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    move-result-object p0

    check-cast p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iput-object p0, p1, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    return-void
.end method

.method public static d(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V
    .locals 2

    .prologue
    .line 2029975
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2029976
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2029977
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2029978
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2029979
    const-class v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2029980
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->b:Landroid/os/Handler;

    .line 2029981
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7bab119b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029982
    const v1, 0x7f030bba

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3e30de39

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x361c85f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029983
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2029984
    invoke-static {p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->d(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V

    .line 2029985
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->a()V

    .line 2029986
    const/16 v1, 0x2b

    const v2, -0x6b557cbc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2029987
    check-cast p1, Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    iput-object p1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    .line 2029988
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    new-instance p1, LX/Dgt;

    invoke-direct {p1, p0}, LX/Dgt;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V

    .line 2029989
    iput-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a:LX/DgQ;

    .line 2029990
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    new-instance p1, LX/Dgu;

    invoke-direct {p1, p0}, LX/Dgu;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V

    .line 2029991
    iput-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    .line 2029992
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b()V

    .line 2029993
    return-void
.end method
