.class public Lcom/facebook/messaging/location/sending/LocationSendingView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/DgT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Landroid/widget/ImageButton;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private g:LX/DgH;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2029637
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2029638
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2029635
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2029636
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2029624
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2029625
    sget-object v0, LX/DgH;->SEND:LX/DgH;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->g:LX/DgH;

    .line 2029626
    const v0, 0x7f030a4a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2029627
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setOrientation(I)V

    .line 2029628
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->b:Landroid/widget/ImageButton;

    .line 2029629
    const v0, 0x7f0d19f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->c:Landroid/widget/TextView;

    .line 2029630
    const v0, 0x7f0d19f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->d:Landroid/widget/ImageView;

    .line 2029631
    const v0, 0x7f0d19f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->e:Landroid/widget/TextView;

    .line 2029632
    const v0, 0x7f0d19f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    .line 2029633
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->d()V

    .line 2029634
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2029617
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2029618
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2029619
    sget-object v0, LX/Dga;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->g:LX/DgH;

    invoke-virtual {v1}, LX/DgH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2029620
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->g:LX/DgH;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2029621
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2029622
    :goto_0
    return-void

    .line 2029623
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2029612
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setConfirmEnabled(Z)V

    .line 2029613
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->b:Landroid/widget/ImageButton;

    new-instance v1, LX/DgY;

    invoke-direct {v1, p0}, LX/DgY;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2029614
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->c:Landroid/widget/TextView;

    new-instance v1, LX/DgZ;

    invoke-direct {v1, p0}, LX/DgZ;-><init>(Lcom/facebook/messaging/location/sending/LocationSendingView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2029615
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->c()V

    .line 2029616
    return-void
.end method

.method public static e(Lcom/facebook/messaging/location/sending/LocationSendingView;)V
    .locals 1

    .prologue
    .line 2029609
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->a:LX/DgT;

    if-eqz v0, :cond_0

    .line 2029610
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->a:LX/DgT;

    invoke-interface {v0}, LX/DgT;->a()V

    .line 2029611
    :cond_0
    return-void
.end method

.method private setConfirmEnabled(Z)V
    .locals 1

    .prologue
    .line 2029606
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2029607
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2029608
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2029601
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setConfirmEnabled(Z)V

    .line 2029602
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->d:Landroid/widget/ImageView;

    const v1, 0x7f021223

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2029603
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->e:Landroid/widget/TextView;

    const v1, 0x7f082ed7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2029604
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2029605
    return-void
.end method

.method public final a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V
    .locals 2

    .prologue
    .line 2029583
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setConfirmEnabled(Z)V

    .line 2029584
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->d:Landroid/widget/ImageView;

    const v1, 0x7f021023

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2029585
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029586
    iget-object v0, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2029587
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2029588
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029589
    :goto_0
    return-void

    .line 2029590
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2029596
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->setConfirmEnabled(Z)V

    .line 2029597
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->d:Landroid/widget/ImageView;

    const v1, 0x7f021023

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2029598
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->e:Landroid/widget/TextView;

    const v1, 0x7f082ede

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2029599
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2029600
    return-void
.end method

.method public setButtonStyle(LX/DgH;)V
    .locals 0

    .prologue
    .line 2029593
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->g:LX/DgH;

    .line 2029594
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/LocationSendingView;->c()V

    .line 2029595
    return-void
.end method

.method public setConfirmClickListener(LX/DgT;)V
    .locals 0

    .prologue
    .line 2029591
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/LocationSendingView;->a:LX/DgT;

    .line 2029592
    return-void
.end method
