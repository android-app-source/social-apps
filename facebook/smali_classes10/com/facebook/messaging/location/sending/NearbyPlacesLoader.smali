.class public Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public final c:Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/Dgs;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/Dgr;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2029945
    const-class v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    const-string v1, "nearby_places"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2029946
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    .line 2029947
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2029948
    move-object v0, v0

    .line 2029949
    const/high16 v1, 0x44960000    # 1200.0f

    .line 2029950
    iput v1, v0, LX/1S7;->c:F

    .line 2029951
    move-object v0, v0

    .line 2029952
    const-wide/16 v2, 0x3a98

    .line 2029953
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2029954
    move-object v0, v0

    .line 2029955
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;LX/0Or;LX/1Ck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2029926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029927
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->c:Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

    .line 2029928
    iput-object p2, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->d:LX/0Or;

    .line 2029929
    iput-object p3, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->e:LX/1Ck;

    .line 2029930
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;
    .locals 4

    .prologue
    .line 2029931
    new-instance v2, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    .line 2029932
    new-instance v3, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;-><init>(LX/0tX;Landroid/content/res/Resources;)V

    .line 2029933
    move-object v0, v3

    .line 2029934
    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

    const/16 v1, 0xc81

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v3, v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;LX/0Or;LX/1Ck;)V

    .line 2029935
    return-object v2
.end method

.method public static b(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;Ljava/lang/String;)V
    .locals 4
    .param p0    # Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2029936
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->e:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2029937
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    .line 2029938
    sget-object v1, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v2, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2029939
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->e:LX/1Ck;

    sget-object v2, LX/Dgs;->GET_LOCATION:LX/Dgs;

    new-instance v3, LX/Dgo;

    invoke-direct {v3, p0, p1}, LX/Dgo;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2029940
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2029941
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->e:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2029942
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2029943
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;Ljava/lang/String;)V

    .line 2029944
    return-void
.end method
