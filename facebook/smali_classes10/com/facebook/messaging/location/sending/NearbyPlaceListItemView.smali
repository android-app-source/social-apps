.class public Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2029820
    const-class v0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2029821
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2029822
    const v0, 0x7f030bb6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2029823
    const v0, 0x7f0212b4

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->setBackgroundResource(I)V

    .line 2029824
    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b1f61

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2029825
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->setPadding(IIII)V

    .line 2029826
    const v0, 0x7f0d1d18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2029827
    const v0, 0x7f0d1d1a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->c:Landroid/widget/TextView;

    .line 2029828
    const v0, 0x7f0d1d1b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->d:Landroid/widget/TextView;

    .line 2029829
    const v0, 0x7f0d1d19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->e:Landroid/widget/TextView;

    .line 2029830
    return-void
.end method
