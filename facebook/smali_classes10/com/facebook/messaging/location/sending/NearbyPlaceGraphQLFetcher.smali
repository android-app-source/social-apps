.class public Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0tX;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2029791
    const-class v0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2029792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029793
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->b:LX/0tX;

    .line 2029794
    iput-object p2, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->c:Landroid/content/res/Resources;

    .line 2029795
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2029796
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2029797
    invoke-static {}, LX/Dgz;->a()LX/Dgy;

    move-result-object v4

    .line 2029798
    new-instance v7, LX/4DI;

    invoke-direct {v7}, LX/4DI;-><init>()V

    .line 2029799
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2029800
    invoke-virtual {v7, p2}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    .line 2029801
    :cond_0
    new-instance v9, LX/3Aj;

    invoke-direct {v9}, LX/3Aj;-><init>()V

    .line 2029802
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 2029803
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 2029804
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v10

    float-to-double v11, v10

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 2029805
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2029806
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v10

    float-to-double v11, v10

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 2029807
    :cond_1
    invoke-virtual {v7, v9}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 2029808
    move-object v5, v7

    .line 2029809
    const-string v6, "search_params"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2029810
    const-string v5, "profile_picture_size"

    .line 2029811
    iget-object v6, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->c:Landroid/content/res/Resources;

    const v7, 0x7f0b1f62

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 2029812
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2029813
    move-object v0, v4

    .line 2029814
    invoke-static {}, LX/Dgz;->a()LX/Dgy;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2029815
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2029816
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2029817
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2029818
    move-object v0, v0

    .line 2029819
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
