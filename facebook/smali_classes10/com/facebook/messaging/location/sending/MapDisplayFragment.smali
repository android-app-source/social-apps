.class public Lcom/facebook/messaging/location/sending/MapDisplayFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Lcom/facebook/uicontrib/fab/FabView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/maps/FbMapViewDelegate;

.field private d:Landroid/widget/ImageView;

.field private e:LX/Dgi;

.field public f:LX/DgW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/6al;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2029749
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2029750
    sget-object v0, LX/Dgi;->USER:LX/Dgi;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->e:LX/Dgi;

    return-void
.end method

.method private a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 4

    .prologue
    .line 2029722
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    if-nez v0, :cond_0

    .line 2029723
    :goto_0
    return-void

    .line 2029724
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    new-instance v1, LX/699;

    invoke-direct {v1}, LX/699;-><init>()V

    .line 2029725
    iput-object p1, v1, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2029726
    move-object v1, v1

    .line 2029727
    const v2, 0x7f021023

    invoke-static {v2}, LX/690;->a(I)LX/68w;

    move-result-object v2

    .line 2029728
    iput-object v2, v1, LX/699;->c:LX/68w;

    .line 2029729
    move-object v1, v1

    .line 2029730
    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, LX/699;->a(FF)LX/699;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6al;->a(LX/699;)LX/6ax;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/location/sending/MapDisplayFragment;LX/692;)V
    .locals 2

    .prologue
    .line 2029731
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->f:LX/DgW;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->e:LX/Dgi;

    sget-object v1, LX/Dgi;->PROGRAMATIC:LX/Dgi;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-object v1, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->h:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2029732
    :cond_0
    :goto_0
    return-void

    .line 2029733
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->f:LX/DgW;

    iget-object v1, p1, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v0, v1}, LX/DgW;->a(Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/location/sending/MapDisplayFragment;LX/6al;)V
    .locals 2

    .prologue
    .line 2029734
    iput-object p1, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    .line 2029735
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    new-instance v1, LX/Dgf;

    invoke-direct {v1, p0}, LX/Dgf;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V

    invoke-virtual {v0, v1}, LX/6al;->a(LX/6a3;)V

    .line 2029736
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->c()LX/6az;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6az;->c(Z)V

    .line 2029737
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    new-instance v1, LX/Dgg;

    invoke-direct {v1, p0}, LX/Dgg;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V

    invoke-virtual {v0, v1}, LX/6al;->a(LX/67n;)V

    .line 2029738
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6al;->a(Z)V

    .line 2029739
    return-void
.end method

.method private b(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 4

    .prologue
    .line 2029751
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    if-nez v0, :cond_0

    .line 2029752
    :goto_0
    return-void

    .line 2029753
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b(Z)V

    .line 2029754
    sget-object v0, LX/Dgi;->PROGRAMATIC:LX/Dgi;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->e:LX/Dgi;

    .line 2029755
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {p1, v1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v1

    const/16 v2, 0x1f4

    new-instance v3, LX/Dgh;

    invoke-direct {v3, p0}, LX/Dgh;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/6al;->a(LX/6aM;ILX/6aj;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2029740
    if-eqz p1, :cond_0

    .line 2029741
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2029742
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 2029743
    :goto_0
    return-void

    .line 2029744
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2029745
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2029746
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    if-nez v0, :cond_0

    .line 2029747
    :goto_0
    return-void

    .line 2029748
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->a()V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V
    .locals 2

    .prologue
    .line 2029677
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    const-string v1, "mMapDelegate became null after animation was started and before that animation was finished. That\'s super weird."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2029678
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->e()LX/692;

    move-result-object v0

    .line 2029679
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->h:Lcom/facebook/android/maps/model/LatLng;

    .line 2029680
    sget-object v0, LX/Dgi;->USER:LX/Dgi;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->e:LX/Dgi;

    .line 2029681
    return-void

    .line 2029682
    :cond_0
    iget-object v0, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 2029713
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c()V

    .line 2029714
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2029715
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029716
    return-void
.end method

.method public final a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V
    .locals 2

    .prologue
    .line 2029717
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c()V

    .line 2029718
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2029719
    iget-object v0, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->d:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029720
    iget-object v0, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->d:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029721
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2029711
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Dge;

    invoke-direct {v1, p0, p1}, LX/Dge;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;Z)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2029712
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2029708
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c()V

    .line 2029709
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2029710
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d06664c    # 1.40928192E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029707
    const v1, 0x7f030a77

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x93da12d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd03cb24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029704
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2029705
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->a()V

    .line 2029706
    const/16 v1, 0x2b

    const v2, 0x54ca3a43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 2029701
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onLowMemory()V

    .line 2029702
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->b()V

    .line 2029703
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x46ddc498

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029698
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2029699
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->c()V

    .line 2029700
    const/16 v1, 0x2b

    const v2, -0x59559a81

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x37459121

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029695
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2029696
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->d()V

    .line 2029697
    const/16 v1, 0x2b

    const v2, -0x26eedac3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2029692
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2029693
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->b(Landroid/os/Bundle;)V

    .line 2029694
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2029683
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2029684
    const v0, 0x7f0d1a9b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2029685
    const v0, 0x7f0d1a9a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->d:Landroid/widget/ImageView;

    .line 2029686
    const v0, 0x7f0d197f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2029687
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p2}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2029688
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->c:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Dgc;

    invoke-direct {v1, p0}, LX/Dgc;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2029689
    const v0, 0x7f0d144a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a:Lcom/facebook/uicontrib/fab/FabView;

    .line 2029690
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v1, LX/Dgd;

    invoke-direct {v1, p0}, LX/Dgd;-><init>(Lcom/facebook/messaging/location/sending/MapDisplayFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2029691
    return-void
.end method
