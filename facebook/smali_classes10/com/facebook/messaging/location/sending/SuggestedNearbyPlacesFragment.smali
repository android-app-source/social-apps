.class public Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

.field public c:LX/DgQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2030077
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    invoke-static {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b(LX/0QB;)Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/location/sending/NearbyPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2030078
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2030079
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a(LX/0Px;)V

    .line 2030080
    :goto_0
    return-void

    .line 2030081
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    const v1, 0x7f082edc

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2030071
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    new-instance v1, LX/Dgw;

    invoke-direct {v1, p0}, LX/Dgw;-><init>(Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;)V

    .line 2030072
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a:LX/DgQ;

    .line 2030073
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2030074
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    new-instance v1, LX/Dgx;

    invoke-direct {v1, p0}, LX/Dgx;-><init>(Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;)V

    .line 2030075
    iput-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    .line 2030076
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2030068
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2030069
    const-class v0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2030070
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2030065
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b()V

    .line 2030066
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a()V

    .line 2030067
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4743b966

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2030064
    const v1, 0x7f03142e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x34c6fc8b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x10d3f28d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2030061
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2030062
    iget-object v1, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->a()V

    .line 2030063
    const/16 v1, 0x2b

    const v2, -0x1275cb53

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2030055
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2030056
    const v0, 0x7f0d2e3d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    iput-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    .line 2030057
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->c()V

    .line 2030058
    invoke-direct {p0}, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->d()V

    .line 2030059
    iget-object v0, p0, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->b()V

    .line 2030060
    return-void
.end method
