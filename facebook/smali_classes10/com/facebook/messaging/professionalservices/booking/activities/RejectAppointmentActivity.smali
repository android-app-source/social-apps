.class public Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# instance fields
.field private p:LX/67X;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/DnV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/0h5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/63L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032449
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2032443
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2032444
    const-string v1, "arg_rejection_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2032445
    const-string v1, "arg_recipient"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032446
    const-string v1, "arg_page_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032447
    const-string v1, "arg_request_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032448
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2032436
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2032437
    const-string v1, "arg_rejection_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2032438
    const-string v1, "arg_recipient"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032439
    const-string v1, "arg_page_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032440
    const-string v1, "arg_request_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032441
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2032442
    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;LX/67X;LX/DnV;)V
    .locals 0

    .prologue
    .line 2032381
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->p:LX/67X;

    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    invoke-static {v1}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v0

    check-cast v0, LX/67X;

    invoke-static {v1}, LX/DnV;->b(LX/0QB;)LX/DnV;

    move-result-object v1

    check-cast v1, LX/DnV;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->a(Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;LX/67X;LX/DnV;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2032428
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032429
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032430
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->r:LX/0h5;

    if-eqz v0, :cond_0

    .line 2032431
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2032432
    :cond_0
    :goto_0
    return-void

    .line 2032433
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032434
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    if-eqz v0, :cond_0

    .line 2032435
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032423
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2032424
    invoke-static {p0, p0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2032425
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032426
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2032427
    :cond_0
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032422
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->p:LX/67X;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2032419
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2032420
    invoke-direct {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->b(Ljava/lang/String;)V

    .line 2032421
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2032392
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2032393
    const v0, 0x7f0311c2

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->setContentView(I)V

    .line 2032394
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032395
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->b()LX/3u1;

    move-result-object v0

    .line 2032396
    if-eqz v0, :cond_0

    .line 2032397
    new-instance v1, LX/63L;

    invoke-direct {v1, v0}, LX/63L;-><init>(LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    .line 2032398
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/63L;->setHasBackButton(Z)V

    .line 2032399
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2032400
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arg_rejection_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2032401
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "arg_recipient"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2032402
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "arg_page_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2032403
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "arg_request_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2032404
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    const v5, 0x7f0d29b1

    .line 2032405
    new-instance v6, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-direct {v6}, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;-><init>()V

    .line 2032406
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2032407
    const-string p1, "arg_rejection_type"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2032408
    const-string p1, "arg_recipient"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032409
    const-string p1, "arg_page_id"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032410
    const-string p1, "arg_request_id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032411
    invoke-virtual {v6, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032412
    move-object v0, v6

    .line 2032413
    invoke-virtual {v4, v5, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2032414
    return-void

    .line 2032415
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032416
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2032417
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->r:LX/0h5;

    .line 2032418
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->r:LX/0h5;

    new-instance v1, LX/Dig;

    invoke-direct {v1, p0}, LX/Dig;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2032386
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->q:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032387
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2032388
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2032389
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    if-eqz v1, :cond_0

    .line 2032390
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->s:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2032391
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2032382
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2032383
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->onBackPressed()V

    .line 2032384
    const/4 v0, 0x1

    .line 2032385
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
