.class public Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# instance fields
.field private p:LX/67X;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/01T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/DnV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private u:LX/DkQ;

.field private v:LX/0h5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:LX/63L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032372
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032357
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032358
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032359
    invoke-virtual {p1}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2032360
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2032361
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032362
    iget-boolean v0, p2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 2032363
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2032364
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2032365
    const-string v1, "extra_appointment_query_config"

    .line 2032366
    iget-object v2, p1, LX/DkQ;->a:Landroid/os/Bundle;

    move-object v2, v2

    .line 2032367
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2032368
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2032369
    const-string v1, "thread_booking_requests"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032370
    return-object v0

    .line 2032371
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032346
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032347
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032348
    invoke-virtual {p1}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2032349
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2032350
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2032351
    const-string v1, "extra_appointment_query_config"

    .line 2032352
    iget-object v2, p1, LX/DkQ;->a:Landroid/os/Bundle;

    move-object v2, v2

    .line 2032353
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2032354
    const-string v1, "thread_booking_requests"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032355
    return-object v0

    .line 2032356
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2032302
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032303
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2032304
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2032305
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->u:LX/DkQ;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "thread_booking_requests"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "referrer"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2032306
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032307
    sget-object v3, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2032308
    new-instance v3, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    invoke-direct {v3}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;-><init>()V

    .line 2032309
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2032310
    const-string p0, "arg_appointment_query_config"

    .line 2032311
    iget-object p1, v0, LX/DkQ;->a:Landroid/os/Bundle;

    move-object p1, p1

    .line 2032312
    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2032313
    const-string p0, "thread_booking_requests"

    invoke-virtual {v4, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032314
    const-string p0, "referrer"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032315
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032316
    move-object v0, v3

    .line 2032317
    :goto_0
    return-object v0

    .line 2032318
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->u:LX/DkQ;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "thread_booking_requests"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2032319
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032320
    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2032321
    new-instance v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;-><init>()V

    .line 2032322
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2032323
    const-string v4, "arg_appointment_query_config"

    .line 2032324
    iget-object p0, v0, LX/DkQ;->a:Landroid/os/Bundle;

    move-object p0, p0

    .line 2032325
    invoke-virtual {v3, v4, p0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2032326
    const-string v4, "thread_booking_requests"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032327
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032328
    move-object v0, v2

    .line 2032329
    goto :goto_0

    .line 2032330
    :cond_1
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2032331
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->u:LX/DkQ;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "thread_booking_requests"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2032332
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032333
    invoke-virtual {v0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v3

    .line 2032334
    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "Invalid query scenario "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2032335
    new-instance v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;-><init>()V

    .line 2032336
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2032337
    const-string v4, "arg_appointment_query_config"

    .line 2032338
    iget-object p0, v0, LX/DkQ;->a:Landroid/os/Bundle;

    move-object p0, p0

    .line 2032339
    invoke-virtual {v3, v4, p0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2032340
    const-string v4, "thread_booking_requests"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032341
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032342
    move-object v0, v2

    .line 2032343
    goto/16 :goto_0

    .line 2032344
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid query scenario "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2032345
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;LX/67X;LX/01T;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/DnV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;",
            "LX/67X;",
            "LX/01T;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/DnV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2032301
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->p:LX/67X;

    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->q:LX/01T;

    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->s:LX/0Or;

    iput-object p5, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    invoke-static {v5}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v1

    check-cast v1, LX/67X;

    invoke-static {v5}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v2

    check-cast v2, LX/01T;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x19e

    invoke-static {v5, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v5}, LX/DnV;->b(LX/0QB;)LX/DnV;

    move-result-object v5

    check-cast v5, LX/DnV;

    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;LX/67X;LX/01T;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/DnV;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2032293
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032294
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032295
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->v:LX/0h5;

    if-eqz v0, :cond_0

    .line 2032296
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->v:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2032297
    :cond_0
    :goto_0
    return-void

    .line 2032298
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032299
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    if-eqz v0, :cond_0

    .line 2032300
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032373
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2032374
    invoke-static {p0, p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2032375
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032376
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2032377
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2032275
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2032276
    instance-of v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    if-eqz v0, :cond_1

    .line 2032277
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    .line 2032278
    new-instance v0, LX/Dic;

    invoke-direct {v0, p0}, LX/Dic;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;)V

    .line 2032279
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->g:LX/Dic;

    .line 2032280
    new-instance v0, LX/Did;

    invoke-direct {v0, p0, p1}, LX/Did;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    .line 2032281
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->j:LX/Did;

    .line 2032282
    :cond_0
    :goto_0
    return-void

    .line 2032283
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    if-eqz v0, :cond_2

    .line 2032284
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    .line 2032285
    new-instance v0, LX/Die;

    invoke-direct {v0, p0}, LX/Die;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;)V

    .line 2032286
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->k:LX/Die;

    .line 2032287
    goto :goto_0

    .line 2032288
    :cond_2
    instance-of v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    if-eqz v0, :cond_0

    .line 2032289
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    .line 2032290
    new-instance v0, LX/Dif;

    invoke-direct {v0, p0}, LX/Dif;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;)V

    .line 2032291
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->p:LX/Dif;

    .line 2032292
    goto :goto_0
.end method

.method public final b()LX/3u1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032274
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->p:LX/67X;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2032271
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2032272
    invoke-direct {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->b(Ljava/lang/String;)V

    .line 2032273
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032255
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2032256
    const v0, 0x7f0300fe

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->setContentView(I)V

    .line 2032257
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_appointment_query_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, LX/DkQ;->a(Landroid/os/Bundle;)LX/DkQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->u:LX/DkQ;

    .line 2032258
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2032259
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->b()LX/3u1;

    move-result-object v0

    .line 2032260
    if-eqz v0, :cond_0

    .line 2032261
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    .line 2032262
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/63L;->setHasBackButton(Z)V

    .line 2032263
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2032264
    const v1, 0x7f0d058d

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2032265
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d058d

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->u:LX/DkQ;

    invoke-virtual {v2}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2032266
    :cond_1
    return-void

    .line 2032267
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032268
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2032269
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->v:LX/0h5;

    .line 2032270
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->v:LX/0h5;

    new-instance v1, LX/Dib;

    invoke-direct {v1, p0}, LX/Dib;-><init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2032251
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d058d

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2032252
    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032253
    :goto_0
    return-void

    .line 2032254
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2032245
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->t:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032246
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2032247
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2032248
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    if-eqz v1, :cond_0

    .line 2032249
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->w:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2032250
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2032241
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2032242
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->onBackPressed()V

    .line 2032243
    const/4 v0, 0x1

    .line 2032244
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
