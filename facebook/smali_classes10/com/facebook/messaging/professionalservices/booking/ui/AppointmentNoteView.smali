.class public Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public b:LX/Dls;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/Dlr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2038765
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2038766
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038767
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a()V

    .line 2038768
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2038769
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2038770
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038771
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a()V

    .line 2038772
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2038744
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2038745
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038746
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a()V

    .line 2038747
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2038750
    const v0, 0x7f030108

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2038751
    const v0, 0x7f0d059e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    .line 2038752
    const v0, 0x7f0d059f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    .line 2038753
    const v0, 0x7f0d05a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    .line 2038754
    const v0, 0x7f0d05a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    .line 2038755
    const v0, 0x7f0d05a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    .line 2038756
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V

    .line 2038757
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    new-instance v1, LX/Dlp;

    invoke-direct {v1, p0}, LX/Dlp;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2038758
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    new-instance v1, LX/Dlq;

    invoke-direct {v1, p0}, LX/Dlq;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2038759
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2038760
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038761
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2038762
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2038763
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038764
    return-void
.end method

.method public setActionButtonListener(LX/Dlr;)V
    .locals 0
    .param p1    # LX/Dlr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2038748
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->c:LX/Dlr;

    .line 2038749
    return-void
.end method

.method public setAppointmentNoteListener(LX/Dls;)V
    .locals 0
    .param p1    # LX/Dls;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2038742
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->b:LX/Dls;

    .line 2038743
    return-void
.end method

.method public setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2038708
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->ERROR:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p1, v0, :cond_1

    .line 2038709
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038710
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038711
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2038712
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    const v1, 0x7f082be5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2038713
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038714
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2038715
    :goto_0
    return-void

    .line 2038716
    :cond_1
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p1, v0, :cond_2

    .line 2038717
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038718
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038719
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2038720
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2038721
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    const v1, 0x7f082be7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2038722
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038723
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 2038724
    :cond_2
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p1, v0, :cond_3

    .line 2038725
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038726
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038727
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2038728
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2038729
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    const v1, 0x7f082be6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2038730
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038731
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 2038732
    :cond_3
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p1, v0, :cond_4

    .line 2038733
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038734
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038735
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2038736
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2038737
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->f:Landroid/widget/TextView;

    const v1, 0x7f082be7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2038738
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038739
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2038740
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->h:Landroid/widget/RelativeLayout;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    goto :goto_0

    .line 2038741
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid view state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
