.class public Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Dka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DnP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Dih;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private k:Lcom/facebook/fbui/glyph/GlyphView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/RelativeLayout;

.field private o:Lcom/facebook/fbui/glyph/GlyphView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/LinearLayout;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2039442
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2039443
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a()V

    .line 2039444
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039445
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2039446
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a()V

    .line 2039447
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039448
    new-instance v0, LX/DmT;

    invoke-direct {v0, p0, p1}, LX/DmT;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    .line 2039449
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    mul-long v2, p1, v4

    const v1, 0x1001a

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 2039450
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a:LX/11S;

    sget-object v2, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    mul-long/2addr v4, p1

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2039451
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080086

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2039452
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2039453
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->setOrientation(I)V

    .line 2039454
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2039455
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1e61

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2039456
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2039457
    const v0, 0x7f0301b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2039458
    const v0, 0x7f0d0722

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2039459
    const v0, 0x7f0d0723

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->k:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2039460
    const v0, 0x7f0d0724

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->l:Landroid/widget/TextView;

    .line 2039461
    const v0, 0x7f0d0725

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->m:Landroid/widget/TextView;

    .line 2039462
    const v0, 0x7f0d0726

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    .line 2039463
    const v0, 0x7f0d0727

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2039464
    const v0, 0x7f0d0728

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    .line 2039465
    const v0, 0x7f0d0729

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    .line 2039466
    const v0, 0x7f0d072a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    .line 2039467
    const v0, 0x7f0d072c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->s:Landroid/widget/LinearLayout;

    .line 2039468
    const v0, 0x7f0d072b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->t:Landroid/view/View;

    .line 2039469
    const v0, 0x7f0d072d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->u:Landroid/widget/TextView;

    .line 2039470
    const v0, 0x7f0d072e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->v:Landroid/widget/TextView;

    .line 2039471
    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;LX/11S;LX/Dka;LX/DnP;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/Dih;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;",
            "LX/11S;",
            "LX/Dka;",
            "LX/DnP;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/Dih;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2039472
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a:LX/11S;

    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->b:LX/Dka;

    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->c:LX/DnP;

    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-static {v6}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-static {v6}, LX/Dka;->b(LX/0QB;)LX/Dka;

    move-result-object v2

    check-cast v2, LX/Dka;

    invoke-static {v6}, LX/DnP;->a(LX/0QB;)LX/DnP;

    move-result-object v3

    check-cast v3, LX/DnP;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x19e

    invoke-static {v6, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v6}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v6

    check-cast v6, LX/Dih;

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;LX/11S;LX/Dka;LX/DnP;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/Dih;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039307
    new-instance v0, LX/DmU;

    invoke-direct {v0, p0, p1}, LX/DmU;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V

    return-object v0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 2039473
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->c()V

    .line 2039474
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d()V

    .line 2039475
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2039476
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    .line 2039477
    if-eqz v0, :cond_0

    if-ne v0, v4, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-nez v1, :cond_1

    .line 2039478
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039479
    :goto_0
    return-void

    .line 2039480
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v1, v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039481
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f02085b

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039482
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0137

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039483
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039484
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2039485
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039486
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2039487
    if-nez v0, :cond_0

    .line 2039488
    :goto_0
    return-void

    .line 2039489
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2039490
    invoke-static {p1}, LX/DkQ;->a(Ljava/lang/String;)LX/DkQ;

    move-result-object v2

    .line 2039491
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039492
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039493
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Dih;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039494
    new-instance v0, LX/DmV;

    invoke-direct {v0, p0, p1}, LX/DmV;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2039409
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    if-eq v0, v2, :cond_1

    .line 2039410
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2039411
    :cond_0
    :goto_0
    return-void

    .line 2039412
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-lez v0, :cond_3

    .line 2039413
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2039414
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2039415
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2039416
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039417
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039418
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->u:Landroid/widget/TextView;

    new-instance v1, LX/DmS;

    invoke-direct {v1, p0}, LX/DmS;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039419
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039420
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02085b

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039421
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getDetailBannerTitleForAdmin()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039422
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-lez v0, :cond_2

    .line 2039423
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039424
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->k()V

    goto :goto_0

    .line 2039425
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039426
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039427
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j()V

    goto :goto_0

    .line 2039428
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_0

    .line 2039429
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2039430
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2039431
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->t:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2039432
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039433
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039434
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_5

    .line 2039435
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a014a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2039436
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02085b

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039437
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-direct {p0, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039438
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j()V

    .line 2039439
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039440
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2039441
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method private e(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039495
    new-instance v0, LX/DmW;

    invoke-direct {v0, p0, p1}, LX/DmW;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2039403
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    if-nez v0, :cond_0

    .line 2039404
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->setVisibility(I)V

    .line 2039405
    :goto_0
    return-void

    .line 2039406
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-lez v0, :cond_1

    .line 2039407
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f()V

    goto :goto_0

    .line 2039408
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g()V

    goto :goto_0
.end method

.method private f()V
    .locals 0

    .prologue
    .line 2039400
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->h()V

    .line 2039401
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i()V

    .line 2039402
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2039395
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2039396
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->h()V

    .line 2039397
    :goto_0
    return-void

    .line 2039398
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039399
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i()V

    goto :goto_0
.end method

.method private getDetailBannerTitleForAdmin()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2039386
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    if-lez v0, :cond_1

    .line 2039387
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0138

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2039388
    :goto_1
    return-object v0

    .line 2039389
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    goto :goto_0

    .line 2039390
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-ne v0, v5, :cond_2

    .line 2039391
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082bb8

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2039392
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-le v0, v5, :cond_4

    .line 2039393
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0138

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_2
    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    goto :goto_2

    .line 2039394
    :cond_4
    const-string v0, ""

    goto :goto_1
.end method

.method private h()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2039381
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039382
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020778

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039383
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0136

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v5, v5, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039384
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039385
    return-void
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 2039362
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_2

    .line 2039363
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2039364
    :cond_1
    :goto_0
    return-void

    .line 2039365
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2039366
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_4

    .line 2039367
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020778

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039368
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-direct {p0, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039369
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-le v0, v6, :cond_5

    .line 2039370
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039371
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039372
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_6

    .line 2039373
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->l()V

    goto :goto_0

    .line 2039374
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_3

    .line 2039375
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02085b

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    goto :goto_1

    .line 2039376
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2039377
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_1

    .line 2039378
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2039379
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->t:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2039380
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->b(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private j()V
    .locals 5

    .prologue
    .line 2039358
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2039359
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2039360
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2039361
    return-void
.end method

.method private k()V
    .locals 6

    .prologue
    .line 2039354
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2039355
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0060

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2039356
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2039357
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2039347
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2039348
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2039349
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039350
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->v:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039351
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->u:Landroid/widget/TextView;

    new-instance v1, LX/DmX;

    invoke-direct {v1, p0}, LX/DmX;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039352
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->v:Landroid/widget/TextView;

    new-instance v1, LX/DmY;

    invoke-direct {v1, p0}, LX/DmY;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039353
    return-void
.end method

.method public static m(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V
    .locals 4

    .prologue
    .line 2039329
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->c:LX/DnP;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "booking_request/%s/create_appointment"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039330
    new-instance v1, LX/Djn;

    invoke-direct {v1}, LX/Djn;-><init>()V

    .line 2039331
    const/4 v2, 0x0

    .line 2039332
    iput-boolean v2, v1, LX/Djn;->a:Z

    .line 2039333
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    .line 2039334
    iput-object v2, v1, LX/Djn;->b:Ljava/lang/String;

    .line 2039335
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    .line 2039336
    iput-object v2, v1, LX/Djn;->e:Ljava/lang/String;

    .line 2039337
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    .line 2039338
    iput-object v2, v1, LX/Djn;->g:Ljava/lang/String;

    .line 2039339
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    .line 2039340
    iput-object v2, v1, LX/Djn;->k:Ljava/lang/String;

    .line 2039341
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    .line 2039342
    iput-object v2, v1, LX/Djn;->l:Ljava/lang/String;

    .line 2039343
    const-string v2, "extra_create_booking_appointment_model"

    invoke-virtual {v1}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2039344
    const-string v1, "referrer"

    const-string v2, "banner"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2039345
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039346
    return-void
.end method

.method private n()V
    .locals 8

    .prologue
    const/16 v1, 0x8

    .line 2039325
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 2039326
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 2039327
    iget-object v6, v0, LX/Dih;->a:LX/0Zb;

    const-string v7, "profservices_booking_admin_banner_impression"

    invoke-static {v7, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "sent_count"

    invoke-virtual {v7, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "requested_count"

    invoke-virtual {v7, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "request_id"

    invoke-virtual {v7, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "booking_status"

    invoke-virtual {v7, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2039328
    :cond_1
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    const/16 v1, 0x8

    .line 2039321
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 2039322
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 2039323
    iget-object v6, v0, LX/Dih;->a:LX/0Zb;

    const-string v7, "profservices_booking_consumer_banner_impression"

    invoke-static {v7, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "pending_count"

    invoke-virtual {v7, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "confirmed_count"

    invoke-virtual {v7, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "request_id"

    invoke-virtual {v7, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "booking_status"

    invoke-virtual {v7, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2039324
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2039308
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 2039309
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->h:Ljava/lang/String;

    .line 2039310
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->i:Ljava/lang/String;

    .line 2039311
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    if-nez v0, :cond_0

    .line 2039312
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->setVisibility(I)V

    .line 2039313
    :goto_0
    return-void

    .line 2039314
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->j:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039315
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039316
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2039317
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e()V

    .line 2039318
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->o()V

    goto :goto_0

    .line 2039319
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->b()V

    .line 2039320
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->n()V

    goto :goto_0
.end method
