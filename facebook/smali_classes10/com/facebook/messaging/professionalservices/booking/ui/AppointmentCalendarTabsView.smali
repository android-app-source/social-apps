.class public Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Dlk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

.field public c:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field public d:LX/Dll;

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2038567
    invoke-static {}, LX/Dlk;->values()[LX/Dlk;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2038598
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2038599
    new-instance v0, LX/Dli;

    invoke-direct {v0, p0}, LX/Dli;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->e:Landroid/view/View$OnClickListener;

    .line 2038600
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b()V

    .line 2038601
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2038594
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2038595
    new-instance v0, LX/Dli;

    invoke-direct {v0, p0}, LX/Dli;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->e:Landroid/view/View$OnClickListener;

    .line 2038596
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b()V

    .line 2038597
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2038590
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2038591
    new-instance v0, LX/Dli;

    invoke-direct {v0, p0}, LX/Dli;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->e:Landroid/view/View$OnClickListener;

    .line 2038592
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b()V

    .line 2038593
    return-void
.end method

.method private a(LX/Dlk;LX/Dlk;)V
    .locals 3

    .prologue
    .line 2038602
    iget v0, p1, LX/Dlk;->tabTextViewId:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2038603
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2038604
    iget v0, p2, LX/Dlk;->tabTextViewId:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2038605
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2038606
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;LX/Dlk;)V
    .locals 1

    .prologue
    .line 2038585
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->d:LX/Dll;

    iget-object v0, v0, LX/Dll;->a:LX/Dlk;

    .line 2038586
    invoke-direct {p0, v0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->a(LX/Dlk;LX/Dlk;)V

    .line 2038587
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->d:LX/Dll;

    .line 2038588
    iput-object p1, v0, LX/Dll;->a:LX/Dlk;

    .line 2038589
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2038571
    const v0, 0x7f030105

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2038572
    new-instance v0, LX/Dll;

    invoke-direct {v0}, LX/Dll;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->d:LX/Dll;

    .line 2038573
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dlk;

    .line 2038574
    iget v2, v0, LX/Dlk;->tabTextViewId:I

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    .line 2038575
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2038576
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2038577
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->d:LX/Dll;

    iget-object v0, v0, LX/Dll;->a:LX/Dlk;

    iget v0, v0, LX/Dlk;->tabTextViewId:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2038578
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2038579
    const v0, 0x7f0d0599

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/view/ControlledView;

    .line 2038580
    iget-object v1, v0, Lcom/facebook/view/ControlledView;->a:Lcom/facebook/view/ViewController;

    move-object v0, v1

    .line 2038581
    check-cast v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    .line 2038582
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    new-instance v1, LX/Dlj;

    invoke-direct {v1, p0}, LX/Dlj;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;)V

    .line 2038583
    iput-object v1, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    .line 2038584
    return-void
.end method


# virtual methods
.method public setViewPager(Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;)V
    .locals 2

    .prologue
    .line 2038568
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->c:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2038569
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->b:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->c:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->a(Landroid/support/v4/view/ViewPager;)V

    .line 2038570
    return-void
.end method
