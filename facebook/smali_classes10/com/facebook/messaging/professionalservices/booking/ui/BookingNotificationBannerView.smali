.class public Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/Dmn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

.field private c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2039566
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2039567
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a()V

    .line 2039568
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039563
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2039564
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a()V

    .line 2039565
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2039569
    const v0, 0x7f0301ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2039570
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->setOrientation(I)V

    .line 2039571
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2039572
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1e61

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2039573
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2039574
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2039575
    const v0, 0x7f0d0735

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    .line 2039576
    const v0, 0x7f0d0736

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    .line 2039577
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    invoke-static {v0}, LX/Dmn;->b(LX/0QB;)LX/Dmn;

    move-result-object v0

    check-cast v0, LX/Dmn;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a:LX/Dmn;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    .line 2039546
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a:LX/Dmn;

    invoke-virtual {v0, p1, p2, p3}, LX/Dmn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;

    move-result-object v0

    .line 2039547
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/Dmp;->a:LX/Dmo;

    if-nez v1, :cond_1

    .line 2039548
    :cond_0
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->setVisibility(I)V

    .line 2039549
    :goto_0
    return-void

    .line 2039550
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->a:LX/Dmo;

    iget v2, v2, LX/Dmo;->a:I

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setIconDrawable(I)V

    .line 2039551
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->a:LX/Dmo;

    iget-object v2, v2, LX/Dmo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setRowText(Ljava/lang/String;)V

    .line 2039552
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->a:LX/Dmo;

    iget v2, v2, LX/Dmo;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setStatusOrCtaColor(I)V

    .line 2039553
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->a:LX/Dmo;

    iget-object v2, v2, LX/Dmo;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setStatusOrCtaText(Ljava/lang/String;)V

    .line 2039554
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->a:LX/Dmo;

    iget-object v2, v2, LX/Dmo;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039555
    iget-object v1, v0, LX/Dmp;->b:LX/Dmo;

    if-eqz v1, :cond_2

    .line 2039556
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setVisibility(I)V

    .line 2039557
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->b:LX/Dmo;

    iget v2, v2, LX/Dmo;->a:I

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setIconDrawable(I)V

    .line 2039558
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->b:LX/Dmo;

    iget-object v2, v2, LX/Dmo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setRowText(Ljava/lang/String;)V

    .line 2039559
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->b:LX/Dmo;

    iget v2, v2, LX/Dmo;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setStatusOrCtaColor(I)V

    .line 2039560
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v2, v0, LX/Dmp;->b:LX/Dmo;

    iget-object v2, v2, LX/Dmo;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setStatusOrCtaText(Ljava/lang/String;)V

    .line 2039561
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    iget-object v0, v0, LX/Dmp;->b:LX/Dmo;

    iget-object v0, v0, LX/Dmo;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2039562
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->setVisibility(I)V

    goto :goto_0
.end method
