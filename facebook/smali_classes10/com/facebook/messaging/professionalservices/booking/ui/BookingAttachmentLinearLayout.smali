.class public Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/Dlo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/DnT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private f:Landroid/view/LayoutInflater;

.field private g:LX/DmZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2039511
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2039512
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a()V

    .line 2039513
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039498
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2039499
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a()V

    .line 2039500
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039508
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2039509
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a()V

    .line 2039510
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2039503
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2039504
    const v0, 0x7f0301b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2039505
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->f:Landroid/view/LayoutInflater;

    .line 2039506
    const v0, 0x7f0d0731

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2039507
    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;LX/Dlo;Lcom/facebook/auth/viewercontext/ViewerContext;LX/DnT;LX/0wM;)V
    .locals 0

    .prologue
    .line 2039514
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a:LX/Dlo;

    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->c:LX/DnT;

    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->d:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;

    const-class v0, LX/Dlo;

    invoke-interface {v3, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Dlo;

    invoke-static {v3}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v3}, LX/DnT;->b(LX/0QB;)LX/DnT;

    move-result-object v2

    check-cast v2, LX/DnT;

    invoke-static {v3}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->a(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;LX/Dlo;Lcom/facebook/auth/viewercontext/ViewerContext;LX/DnT;LX/0wM;)V

    return-void
.end method


# virtual methods
.method public setProgressBarListener(LX/DmZ;)V
    .locals 0

    .prologue
    .line 2039501
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAttachmentLinearLayout;->g:LX/DmZ;

    .line 2039502
    return-void
.end method
