.class public Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DnJ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/DnT;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0wM;

.field public final g:LX/0Uh;

.field public final h:LX/Djv;

.field private final i:LX/Dlo;

.field public final j:LX/Dih;

.field public k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/DjJ;

.field public m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

.field public n:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2040196
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;LX/DnT;LX/0Or;LX/Dlo;LX/0wM;LX/0Uh;LX/Djv;LX/Dih;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/DnT;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/Dlo;",
            "LX/0wM;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/Djv;",
            "LX/Dih;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2040281
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2040282
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->n:Ljava/lang/String;

    .line 2040283
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    .line 2040284
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->d:LX/DnT;

    .line 2040285
    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->e:LX/0Or;

    .line 2040286
    iput-object p5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->i:LX/Dlo;

    .line 2040287
    iput-object p6, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->f:LX/0wM;

    .line 2040288
    iput-object p7, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->g:LX/0Uh;

    .line 2040289
    iput-object p8, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->h:LX/Djv;

    .line 2040290
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2040291
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->b:LX/0Px;

    .line 2040292
    iput-object p9, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->j:LX/Dih;

    .line 2040293
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)I
    .locals 3

    .prologue
    .line 2040241
    sget-object v0, LX/Dn4;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2040242
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid booking status "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2040243
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    const v1, 0x7f0a07f0

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 2040244
    :goto_0
    return v0

    .line 2040245
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    const v1, 0x7f0a07f1

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    .line 2040246
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    const v1, 0x7f0a07f2

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2040247
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2040248
    sget-object v1, LX/DnJ;->SERVICE_INFO:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 2040249
    const v1, 0x7f030e13

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040250
    new-instance v0, LX/DnI;

    invoke-direct {v0, v1}, LX/DnI;-><init>(Landroid/view/View;)V

    .line 2040251
    :goto_0
    return-object v0

    .line 2040252
    :cond_0
    sget-object v1, LX/DnJ;->APPOINTMENT_TIME:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_1

    .line 2040253
    const v1, 0x7f030e11

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040254
    new-instance v0, LX/Dn6;

    invoke-direct {v0, p0, v1}, LX/Dn6;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 2040255
    :cond_1
    sget-object v1, LX/DnJ;->BOOKING_STATUS:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_2

    .line 2040256
    const v1, 0x7f030e11

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040257
    new-instance v0, LX/Dn7;

    invoke-direct {v0, p0, v1}, LX/Dn7;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 2040258
    :cond_2
    sget-object v1, LX/DnJ;->APPOINTMENT_HEADER:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_4

    .line 2040259
    const v1, 0x7f031044

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040260
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_3

    .line 2040261
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2040262
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2040263
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2040264
    :cond_3
    new-instance v0, LX/Dn9;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->i:LX/Dlo;

    invoke-direct {v0, p0, v1, v2}, LX/Dn9;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;LX/Dlo;)V

    goto :goto_0

    .line 2040265
    :cond_4
    sget-object v1, LX/DnJ;->SERVICE_INFO_V2:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_5

    .line 2040266
    const v1, 0x7f030107

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040267
    new-instance v0, LX/DnB;

    invoke-direct {v0, p0, v1}, LX/DnB;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 2040268
    :cond_5
    sget-object v1, LX/DnJ;->SERVICE_DATE:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_6

    .line 2040269
    const v1, 0x7f030107

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040270
    new-instance v0, LX/DnC;

    invoke-direct {v0, p0, v1}, LX/DnC;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2040271
    :cond_6
    sget-object v1, LX/DnJ;->SERVICE_TIME:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_7

    .line 2040272
    const v1, 0x7f030107

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040273
    new-instance v0, LX/DnD;

    invoke-direct {v0, p0, v1}, LX/DnD;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2040274
    :cond_7
    sget-object v1, LX/DnJ;->SERVICE_PRICE:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_8

    .line 2040275
    const v1, 0x7f030107

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040276
    new-instance v0, LX/DnE;

    invoke-direct {v0, p0, v1}, LX/DnE;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2040277
    :cond_8
    sget-object v1, LX/DnJ;->APPOINTMENT_NOTE:LX/DnJ;

    invoke-virtual {v1}, LX/DnJ;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_9

    .line 2040278
    const v1, 0x7f030e12

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2040279
    new-instance v0, LX/DnH;

    new-instance v2, LX/DnF;

    invoke-direct {v2, p0}, LX/DnF;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;)V

    new-instance v3, LX/DnG;

    invoke-direct {v3, p0}, LX/DnG;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;)V

    invoke-direct {v0, v1, v2, v3}, LX/DnH;-><init>(Landroid/view/View;LX/Dlr;LX/Dls;)V

    goto/16 :goto_0

    .line 2040280
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 2040199
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v0}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v0

    .line 2040200
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 2040201
    sget-object v2, LX/DnJ;->APPOINTMENT_HEADER:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 2040202
    check-cast p1, LX/Dn8;

    .line 2040203
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p1, v0}, LX/Dn8;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    .line 2040204
    :cond_0
    :goto_0
    return-void

    .line 2040205
    :cond_1
    sget-object v2, LX/DnJ;->SERVICE_INFO_V2:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-eq v1, v2, :cond_2

    sget-object v2, LX/DnJ;->SERVICE_DATE:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-eq v1, v2, :cond_2

    sget-object v2, LX/DnJ;->SERVICE_TIME:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-eq v1, v2, :cond_2

    sget-object v2, LX/DnJ;->SERVICE_PRICE:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 2040206
    :cond_2
    check-cast p1, LX/DnA;

    .line 2040207
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p1, v0}, LX/DnA;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    goto :goto_0

    .line 2040208
    :cond_3
    sget-object v2, LX/DnJ;->SERVICE_INFO:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 2040209
    check-cast p1, LX/DnI;

    .line 2040210
    if-nez v0, :cond_4

    .line 2040211
    const-string v0, ""

    const-string v1, ""

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, LX/DnI;->a(LX/DnI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2040212
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->l:LX/DjJ;

    if-eqz v0, :cond_0

    .line 2040213
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->l:LX/DjJ;

    .line 2040214
    iget-object v1, v0, LX/DjJ;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const-string v2, "admin_appointment_detail_validity"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2040215
    goto :goto_0

    .line 2040216
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2040217
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2040218
    invoke-static {v0}, LX/DnS;->c(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    .line 2040219
    invoke-static {v0}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    .line 2040220
    invoke-static {p1, v1, v2, v3}, LX/DnI;->a(LX/DnI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2040221
    goto :goto_0

    .line 2040222
    :cond_5
    sget-object v2, LX/DnJ;->APPOINTMENT_TIME:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-eq v1, v2, :cond_6

    sget-object v2, LX/DnJ;->BOOKING_STATUS:LX/DnJ;

    invoke-virtual {v2}, LX/DnJ;->toInt()I

    move-result v2

    if-ne v1, v2, :cond_8

    .line 2040223
    :cond_6
    check-cast p1, LX/Dn5;

    .line 2040224
    if-nez v0, :cond_7

    .line 2040225
    iget-object v0, p1, LX/Dn5;->m:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040226
    iget-object v0, p1, LX/Dn5;->n:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040227
    goto/16 :goto_0

    .line 2040228
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p1, v0}, LX/Dn5;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    goto/16 :goto_0

    .line 2040229
    :cond_8
    sget-object v0, LX/DnJ;->APPOINTMENT_NOTE:LX/DnJ;

    invoke-virtual {v0}, LX/DnJ;->toInt()I

    move-result v0

    if-ne v1, v0, :cond_9

    .line 2040230
    check-cast p1, LX/DnH;

    .line 2040231
    iget-object v0, p1, LX/DnH;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040232
    iget-object v2, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-object v1, v2

    .line 2040233
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V

    .line 2040234
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082be4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2040235
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040236
    iget-object v2, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2040237
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->u()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$UserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$UserModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2040238
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082be3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2040239
    iget-object v3, p1, LX/DnH;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;

    invoke-virtual {v3, v0, v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2040240
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2040198
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DnJ;

    invoke-virtual {v0}, LX/DnJ;->toInt()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2040197
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
