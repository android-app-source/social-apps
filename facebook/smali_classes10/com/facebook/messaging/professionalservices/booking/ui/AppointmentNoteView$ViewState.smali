.class public final enum Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public static final enum ERROR:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public static final enum NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public static final enum SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public static final enum VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2038695
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v1, "NO_NOTE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038696
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v1, "EDIT_NOTE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038697
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v1, "VIEW_NOTE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038698
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v1, "SAVING_NOTE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038699
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->ERROR:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038700
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->ERROR:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->$VALUES:[Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2038701
    new-instance v0, LX/Dlt;

    invoke-direct {v0}, LX/Dlt;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2038707
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;
    .locals 1

    .prologue
    .line 2038706
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;
    .locals 1

    .prologue
    .line 2038705
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->$VALUES:[Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2038704
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2038702
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2038703
    return-void
.end method
