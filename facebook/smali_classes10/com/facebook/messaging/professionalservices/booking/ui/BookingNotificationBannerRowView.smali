.class public Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/fbui/glyph/GlyphView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2039543
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2039544
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->d()V

    .line 2039545
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039525
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2039526
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->d()V

    .line 2039527
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2039538
    const v0, 0x7f0301b9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2039539
    const v0, 0x7f0d0732

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->j:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2039540
    const v0, 0x7f0d0733

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->k:Landroid/widget/TextView;

    .line 2039541
    const v0, 0x7f0d0734

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->l:Landroid/widget/TextView;

    .line 2039542
    return-void
.end method


# virtual methods
.method public setIconDrawable(I)V
    .locals 1

    .prologue
    .line 2039536
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2039537
    return-void
.end method

.method public setRowText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2039534
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039535
    return-void
.end method

.method public setStatusOrCtaColor(I)V
    .locals 1

    .prologue
    .line 2039532
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2039533
    return-void
.end method

.method public setStatusOrCtaOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2039530
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039531
    return-void
.end method

.method public setStatusOrCtaText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2039528
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerRowView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039529
    return-void
.end method
