.class public Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/6RZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/1aX;

.field private f:Landroid/text/style/MetricAffectingSpan;

.field private g:Landroid/text/style/MetricAffectingSpan;

.field private h:Landroid/net/Uri;

.field private i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2038654
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2038651
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2038652
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a()V

    .line 2038653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2038648
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2038649
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a()V

    .line 2038650
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2038645
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2038646
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a()V

    .line 2038647
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2038633
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2038634
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2038635
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0a86

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->g:Landroid/text/style/MetricAffectingSpan;

    .line 2038636
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0a85

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->f:Landroid/text/style/MetricAffectingSpan;

    .line 2038637
    const v1, 0x7f0a07f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2038638
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2038639
    iput-object v1, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2038640
    move-object v0, v2

    .line 2038641
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2038642
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->e:LX/1aX;

    .line 2038643
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V

    .line 2038644
    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;LX/0Or;LX/6RZ;LX/0W9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/6RZ;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2038632
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->b:LX/6RZ;

    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->c:LX/0W9;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;

    const/16 v0, 0x509

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    invoke-static {v1}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;LX/0Or;LX/6RZ;LX/0W9;)V

    return-void
.end method

.method private setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 2038607
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getPaddingLeft()I

    move-result v0

    .line 2038608
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getPaddingTop()I

    move-result v1

    .line 2038609
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getPaddingRight()I

    move-result v2

    .line 2038610
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->getPaddingBottom()I

    move-result v3

    .line 2038611
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2038612
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setPadding(IIII)V

    .line 2038613
    return-void
.end method

.method private setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2038627
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->h:Landroid/net/Uri;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2038628
    :goto_0
    return-void

    .line 2038629
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->h:Landroid/net/Uri;

    .line 2038630
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2038631
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->e:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method private setStartDate(Ljava/util/Date;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 2038617
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->b:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->c:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2038618
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->b:LX/6RZ;

    invoke-virtual {v1, p1}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2038619
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2038620
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2038621
    :goto_0
    return-void

    .line 2038622
    :cond_0
    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->i:Ljava/lang/String;

    .line 2038623
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2038624
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->f:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2038625
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->g:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2038626
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 2038614
    invoke-direct {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 2038615
    invoke-direct {p0, p2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->setStartDate(Ljava/util/Date;)V

    .line 2038616
    return-void
.end method
