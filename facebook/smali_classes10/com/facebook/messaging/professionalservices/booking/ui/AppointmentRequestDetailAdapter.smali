.class public Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DmC;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/6Zi;

.field public final f:LX/Dih;

.field private final g:LX/1nG;

.field public final h:LX/01T;

.field public final i:LX/17Y;

.field private final j:LX/CK5;

.field public final k:LX/0Uh;

.field public final l:LX/DnT;

.field private final m:LX/0wM;

.field public n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DmC;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/Djf;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2038929
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2038930
    sget-object v0, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    sget-object v1, LX/DmC;->SERVICE_HEADER:LX/DmC;

    sget-object v2, LX/DmC;->PAGE_IDENTITY:LX/DmC;

    sget-object v3, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    sget-object v4, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    sget-object v5, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    sget-object v6, LX/DmC;->SERVICE_TIME:LX/DmC;

    sget-object v7, LX/DmC;->SERVICE_INFO:LX/DmC;

    sget-object v8, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    sget-object v9, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    sget-object v10, LX/DmC;->SEND_MESSAGE:LX/DmC;

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DnT;Lcom/facebook/content/SecureContextHelper;LX/6Zi;LX/Dih;LX/1nG;LX/01T;LX/17Y;LX/CK5;LX/0Uh;LX/0wM;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2038931
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2038932
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2038933
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->o:LX/0Px;

    .line 2038934
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    .line 2038935
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->l:LX/DnT;

    .line 2038936
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2038937
    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->e:LX/6Zi;

    .line 2038938
    iput-object p5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->f:LX/Dih;

    .line 2038939
    iput-object p6, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->g:LX/1nG;

    .line 2038940
    iput-object p7, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->h:LX/01T;

    .line 2038941
    iput-object p8, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->i:LX/17Y;

    .line 2038942
    iput-object p9, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->j:LX/CK5;

    .line 2038943
    iput-object p10, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    .line 2038944
    iput-object p11, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    .line 2038945
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2038946
    const v0, 0x25d6af

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2038947
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2038948
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2038949
    return-void
.end method

.method public static b(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 19

    .prologue
    .line 2038950
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->n()Ljava/lang/String;

    move-result-object v8

    .line 2038951
    invoke-static/range {p1 .. p1}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    .line 2038952
    invoke-static/range {p1 .. p1}, LX/DnS;->e(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v9

    .line 2038953
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a()D

    move-result-wide v4

    .line 2038954
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->b()D

    move-result-wide v6

    .line 2038955
    new-instance v10, LX/3Af;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-direct {v10, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2038956
    new-instance v18, LX/7TY;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2038957
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v11, 0x7f082bc2

    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    const v11, 0x7f020743

    invoke-virtual {v3, v11}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v3

    new-instance v11, LX/Dm5;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v2}, LX/Dm5;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;)V

    invoke-interface {v3, v11}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2038958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082bc3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f020964

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v11

    new-instance v2, LX/Dm6;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, LX/Dm6;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;DDLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2038959
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->j:LX/CK5;

    invoke-virtual {v2}, LX/CK5;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2038960
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082bc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f0207bb

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v11, LX/Dlu;

    move-object/from16 v12, p0

    move-object v13, v9

    move-wide v14, v4

    move-wide/from16 v16, v6

    invoke-direct/range {v11 .. v17}, LX/Dlu;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;DD)V

    invoke-interface {v2, v11}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2038961
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082bc5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f0208fd

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, LX/Dlv;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v9}, LX/Dlv;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2038962
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, LX/3Af;->a(LX/1OM;)V

    .line 2038963
    invoke-virtual {v10}, LX/3Af;->show()V

    .line 2038964
    return-void
.end method

.method private e(I)LX/DmC;
    .locals 1

    .prologue
    .line 2038965
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->o:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DmC;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2038966
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2038967
    sget-object v1, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 2038968
    const v1, 0x7f030ea8

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038969
    new-instance v0, LX/Dm8;

    invoke-direct {v0, v1}, LX/Dm8;-><init>(Landroid/view/View;)V

    .line 2038970
    :goto_0
    return-object v0

    .line 2038971
    :cond_0
    sget-object v1, LX/DmC;->SERVICE_HEADER:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_1

    .line 2038972
    const v1, 0x7f031049

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038973
    new-instance v0, LX/Dm9;

    invoke-direct {v0, v1}, LX/Dm9;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2038974
    :cond_1
    sget-object v1, LX/DmC;->PAGE_IDENTITY:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_2

    .line 2038975
    const v1, 0x7f031048

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038976
    new-instance v0, LX/DmA;

    invoke-direct {v0, v1}, LX/DmA;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2038977
    :cond_2
    sget-object v1, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_3

    .line 2038978
    const v1, 0x7f03104a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038979
    new-instance v0, LX/DmB;

    invoke-direct {v0, v1}, LX/DmB;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2038980
    :cond_3
    sget-object v1, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_4

    .line 2038981
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038982
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2038983
    new-instance v0, LX/Dlx;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dlx;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto :goto_0

    .line 2038984
    :cond_4
    sget-object v1, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_5

    .line 2038985
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038986
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2038987
    new-instance v0, LX/Dly;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dly;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto :goto_0

    .line 2038988
    :cond_5
    sget-object v1, LX/DmC;->SERVICE_TIME:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_6

    .line 2038989
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038990
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2038991
    new-instance v0, LX/Dlz;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dlz;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto/16 :goto_0

    .line 2038992
    :cond_6
    sget-object v1, LX/DmC;->SERVICE_INFO:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_7

    .line 2038993
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038994
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2038995
    new-instance v0, LX/Dm0;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dm0;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto/16 :goto_0

    .line 2038996
    :cond_7
    sget-object v1, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_8

    .line 2038997
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038998
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2038999
    invoke-static {v1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039000
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2039001
    new-instance v0, LX/Dm2;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dm2;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto/16 :goto_0

    .line 2039002
    :cond_8
    sget-object v1, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_9

    .line 2039003
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2039004
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2039005
    invoke-static {v1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039006
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2039007
    new-instance v0, LX/Dm3;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dm3;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto/16 :goto_0

    .line 2039008
    :cond_9
    sget-object v1, LX/DmC;->SEND_MESSAGE:LX/DmC;

    invoke-virtual {v1}, LX/DmC;->toInt()I

    move-result v1

    if-ne p2, v1, :cond_a

    .line 2039009
    const v1, 0x7f03010a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2039010
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2039011
    invoke-static {v1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039012
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 2039013
    new-instance v0, LX/Dm4;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->m:LX/0wM;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Dm4;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V

    goto/16 :goto_0

    .line 2039014
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2039015
    invoke-direct {p0, p2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->e(I)LX/DmC;

    move-result-object v0

    .line 2039016
    sget-object v3, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    if-ne v0, v3, :cond_2

    .line 2039017
    check-cast p1, LX/Dm8;

    .line 2039018
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v0}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2039019
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v1}, LX/DnS;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    .line 2039020
    if-eqz v0, :cond_1

    .line 2039021
    invoke-virtual {p1, v0}, LX/Dm8;->a(Ljava/lang/String;)V

    .line 2039022
    :cond_0
    :goto_0
    return-void

    .line 2039023
    :cond_1
    if-eqz v1, :cond_0

    .line 2039024
    invoke-virtual {p1, v1}, LX/Dm8;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2039025
    :cond_2
    sget-object v3, LX/DmC;->SERVICE_HEADER:LX/DmC;

    if-ne v0, v3, :cond_7

    .line 2039026
    check-cast p1, LX/Dm9;

    .line 2039027
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2039028
    :goto_1
    if-eqz v0, :cond_5

    .line 2039029
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2039030
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v1}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    .line 2039031
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v3}, LX/DnS;->c(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    .line 2039032
    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v5, 0x61e

    invoke-virtual {v4, v5, v2}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2039033
    invoke-virtual {p1, v0, v3}, LX/Dm9;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2039034
    goto :goto_1

    .line 2039035
    :cond_4
    const/16 p2, 0x8

    const/4 p0, 0x0

    .line 2039036
    iget-object v2, p1, LX/Dm9;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039037
    if-nez v1, :cond_c

    .line 2039038
    iget-object v2, p1, LX/Dm9;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2039039
    :goto_2
    if-nez v3, :cond_d

    .line 2039040
    iget-object v2, p1, LX/Dm9;->n:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039041
    :goto_3
    goto :goto_0

    .line 2039042
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->n()Ljava/lang/String;

    move-result-object v3

    .line 2039043
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v0}, LX/DnS;->b(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f082b8e

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v5}, LX/DnS;->b(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2039044
    :goto_4
    invoke-virtual {p1, v3, v0}, LX/Dm9;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2039045
    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    .line 2039046
    :cond_7
    sget-object v1, LX/DmC;->PAGE_IDENTITY:LX/DmC;

    if-ne v0, v1, :cond_8

    .line 2039047
    check-cast p1, LX/DmA;

    .line 2039048
    iget-object v0, p1, LX/DmA;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, "https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xpa1/v/t1.0-9/13567406_249395952108793_5930585327317329635_n.png.webp?_nc_ad=z-m&oh=92f13af3d501c9485406cec29e89fbb2&oe=57F95E16&__gda__=1477055888_42b0dcfdcf07dd0f936d38a69f598065"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2039049
    iget-object v0, p1, LX/DmA;->m:Landroid/widget/TextView;

    const-string v1, "Victoria Belle Spa"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039050
    iget-object v0, p1, LX/DmA;->n:Landroid/widget/TextView;

    const-string v1, "@victoriabelle"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2039051
    :cond_8
    sget-object v1, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    if-ne v0, v1, :cond_9

    .line 2039052
    check-cast p1, LX/DmB;

    .line 2039053
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v0}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2039054
    iget-object v1, p1, LX/DmB;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2039055
    goto/16 :goto_0

    .line 2039056
    :cond_9
    sget-object v1, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SERVICE_TIME:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SERVICE_INFO:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    if-eq v0, v1, :cond_a

    sget-object v1, LX/DmC;->SEND_MESSAGE:LX/DmC;

    if-ne v0, v1, :cond_b

    .line 2039057
    :cond_a
    check-cast p1, LX/Dlw;

    .line 2039058
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p1, v0}, LX/Dlw;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    goto/16 :goto_0

    .line 2039059
    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2039060
    :cond_c
    iget-object v2, p1, LX/Dm9;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2039061
    iget-object v2, p1, LX/Dm9;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2

    .line 2039062
    :cond_d
    iget-object v2, p1, LX/Dm9;->n:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039063
    iget-object v2, p1, LX/Dm9;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2039064
    invoke-direct {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->e(I)LX/DmC;

    move-result-object v0

    invoke-virtual {v0}, LX/DmC;->toInt()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2039065
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
