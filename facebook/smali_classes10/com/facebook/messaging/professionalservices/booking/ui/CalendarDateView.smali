.class public Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field private a:Landroid/text/style/TextAppearanceSpan;

.field private b:Landroid/text/style/TextAppearanceSpan;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2039755
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2039756
    invoke-direct {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a(Landroid/content/Context;)V

    .line 2039757
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2039768
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2039769
    invoke-direct {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a(Landroid/content/Context;)V

    .line 2039770
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2039771
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2039772
    invoke-direct {p0, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a(Landroid/content/Context;)V

    .line 2039773
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2039764
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->setGravity(I)V

    .line 2039765
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0a7d

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a:Landroid/text/style/TextAppearanceSpan;

    .line 2039766
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0a7e

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->b:Landroid/text/style/TextAppearanceSpan;

    .line 2039767
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    .line 2039758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2039759
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2039760
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a:Landroid/text/style/TextAppearanceSpan;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2039761
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->b:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2039762
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->setText(Ljava/lang/CharSequence;)V

    .line 2039763
    return-void
.end method
