.class public Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/DnM;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private final d:[LX/DnN;

.field public e:LX/DjO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2040317
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2040318
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2040319
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2040320
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    .line 2040321
    invoke-static {}, LX/DnN;->values()[LX/DnN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->d:[LX/DnN;

    .line 2040322
    const v0, 0x7f010593

    const v1, 0x7f0e0a87

    invoke-static {p1, v0, v1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->c:Landroid/content/Context;

    .line 2040323
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2040324
    const/4 v2, 0x0

    .line 2040325
    sget-object v0, LX/DnN;->SERVICE_ITEM:LX/DnN;

    invoke-virtual {v0}, LX/DnN;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2040326
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2040327
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->d:[LX/DnN;

    aget-object v1, v1, p2

    iget v1, v1, LX/DnN;->layoutResId:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2040328
    new-instance v1, LX/DnM;

    invoke-direct {v1, v0}, LX/DnM;-><init>(Landroid/view/View;)V

    return-object v1

    .line 2040329
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected view type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2040330
    check-cast p1, LX/DnM;

    const/4 v1, 0x0

    .line 2040331
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2040332
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2040333
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2040334
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2040335
    iget-object v3, p1, LX/DnM;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2040336
    :cond_0
    iget-object v1, p1, LX/DnM;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040337
    iget-object v1, p1, LX/DnM;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040338
    iget-object v0, p1, LX/DnM;->l:Landroid/view/View;

    new-instance v1, LX/DnL;

    invoke-direct {v1, p0, p2}, LX/DnL;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2040339
    return-void

    :cond_1
    move v0, v1

    .line 2040340
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2040341
    sget-object v0, LX/DnN;->SERVICE_ITEM:LX/DnN;

    invoke-virtual {v0}, LX/DnN;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2040342
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
