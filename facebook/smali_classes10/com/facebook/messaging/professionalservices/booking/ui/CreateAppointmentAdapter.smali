.class public Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/content/Context;

.field private c:LX/0Uh;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Dn0;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

.field public f:LX/Bm9;

.field public g:LX/BmO;

.field public h:LX/BmO;

.field public i:Landroid/view/View$OnClickListener;

.field public j:Landroid/view/View$OnClickListener;

.field private k:[LX/Dn0;

.field public l:Ljava/util/Calendar;

.field private final m:LX/DnT;

.field private n:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2039959
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DnT;LX/0Uh;LX/0SG;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2039960
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2039961
    const v0, 0x7f010593

    const v1, 0x7f0e0a87

    invoke-static {p1, v0, v1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    .line 2039962
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->m:LX/DnT;

    .line 2039963
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->c:LX/0Uh;

    .line 2039964
    iput-object p4, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->n:LX/0SG;

    .line 2039965
    invoke-static {}, LX/Dn0;->values()[LX/Dn0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->k:[LX/Dn0;

    .line 2039966
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2039934
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->k:[LX/Dn0;

    aget-object v0, v0, p2

    .line 2039935
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2039936
    iget v2, v0, LX/Dn0;->layoutResId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2039937
    sget-object v2, LX/Dn0;->OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

    if-ne v0, v2, :cond_0

    .line 2039938
    new-instance v0, LX/Dmw;

    invoke-direct {v0, v1}, LX/Dmw;-><init>(Landroid/view/View;)V

    .line 2039939
    :goto_0
    return-object v0

    .line 2039940
    :cond_0
    sget-object v2, LX/Dn0;->OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

    if-ne v0, v2, :cond_1

    .line 2039941
    new-instance v0, LX/Dmr;

    invoke-direct {v0, v1}, LX/Dmr;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039942
    :cond_1
    sget-object v2, LX/Dn0;->OPTIONAL_USER_AVAILABILITY:LX/Dn0;

    if-ne v0, v2, :cond_2

    .line 2039943
    new-instance v0, LX/Dmr;

    invoke-direct {v0, v1}, LX/Dmr;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039944
    :cond_2
    sget-object v2, LX/Dn0;->OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

    if-ne v0, v2, :cond_3

    .line 2039945
    new-instance v0, LX/Dmr;

    invoke-direct {v0, v1}, LX/Dmr;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039946
    :cond_3
    sget-object v2, LX/Dn0;->OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

    if-ne v0, v2, :cond_4

    .line 2039947
    new-instance v0, LX/Dmt;

    invoke-direct {v0, v1}, LX/Dmt;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039948
    :cond_4
    sget-object v2, LX/Dn0;->OPTIONAL_DIVIDER:LX/Dn0;

    if-ne v0, v2, :cond_5

    .line 2039949
    new-instance v0, LX/Dmu;

    invoke-direct {v0, v1}, LX/Dmu;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039950
    :cond_5
    sget-object v2, LX/Dn0;->HEADER_TEXT:LX/Dn0;

    if-ne v0, v2, :cond_6

    .line 2039951
    new-instance v0, LX/Dmv;

    invoke-direct {v0, v1}, LX/Dmv;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039952
    :cond_6
    sget-object v2, LX/Dn0;->SERVICE_SUMMARY:LX/Dn0;

    if-ne v0, v2, :cond_7

    .line 2039953
    new-instance v0, LX/Dmx;

    invoke-direct {v0, v1}, LX/Dmx;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039954
    :cond_7
    sget-object v2, LX/Dn0;->DATE_PICKER:LX/Dn0;

    if-ne v0, v2, :cond_8

    .line 2039955
    new-instance v0, LX/Dms;

    invoke-direct {v0, v1}, LX/Dms;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039956
    :cond_8
    sget-object v2, LX/Dn0;->TIME_SPAN_PICKER:LX/Dn0;

    if-ne v0, v2, :cond_9

    .line 2039957
    new-instance v0, LX/Dmz;

    invoke-direct {v0, v1}, LX/Dmz;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2039958
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown viewType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2039837
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2039838
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dn0;

    .line 2039839
    sget-object v1, LX/Dn0;->OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

    if-ne v0, v1, :cond_1

    .line 2039840
    check-cast p1, LX/Dmw;

    .line 2039841
    iget-object v0, p1, LX/Dmw;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2039842
    iget-object v0, p1, LX/Dmw;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->m:LX/DnT;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-wide v2, v2, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->h:J

    invoke-virtual {v1, v2, v3}, LX/DnT;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2039843
    :cond_0
    :goto_0
    return-void

    .line 2039844
    :cond_1
    sget-object v1, LX/Dn0;->OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

    if-ne v0, v1, :cond_2

    .line 2039845
    check-cast p1, LX/Dmr;

    .line 2039846
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082b92

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2039847
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2039848
    :cond_2
    sget-object v1, LX/Dn0;->OPTIONAL_USER_AVAILABILITY:LX/Dn0;

    if-ne v0, v1, :cond_3

    .line 2039849
    check-cast p1, LX/Dmr;

    .line 2039850
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2039851
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2039852
    :cond_3
    sget-object v1, LX/Dn0;->OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

    if-ne v0, v1, :cond_4

    .line 2039853
    check-cast p1, LX/Dmr;

    .line 2039854
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bcf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2039855
    iget-object v0, p1, LX/Dmr;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2039856
    :cond_4
    sget-object v1, LX/Dn0;->OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

    if-ne v0, v1, :cond_5

    .line 2039857
    check-cast p1, LX/Dmt;

    .line 2039858
    iget-object v0, p1, LX/Dmt;->l:Landroid/widget/TextView;

    .line 2039859
    new-instance v1, LX/Dmq;

    invoke-direct {v1, p0}, LX/Dmq;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039860
    goto :goto_0

    .line 2039861
    :cond_5
    sget-object v1, LX/Dn0;->HEADER_TEXT:LX/Dn0;

    if-ne v0, v1, :cond_6

    .line 2039862
    check-cast p1, LX/Dmv;

    .line 2039863
    iget-object v0, p1, LX/Dmv;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bcc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2039864
    :cond_6
    sget-object v1, LX/Dn0;->SERVICE_SUMMARY:LX/Dn0;

    if-ne v0, v1, :cond_7

    .line 2039865
    check-cast p1, LX/Dmx;

    .line 2039866
    const/16 p2, 0x8

    const/4 v4, 0x0

    .line 2039867
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2039868
    if-eqz v0, :cond_b

    .line 2039869
    iget-object v0, p1, LX/Dmx;->l:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039870
    iget-object v0, p1, LX/Dmx;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039871
    iget-object v0, p1, LX/Dmx;->n:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039872
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 2039873
    iget-object v0, p1, LX/Dmx;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2039874
    iget-object v0, p1, LX/Dmx;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2039875
    :goto_2
    iget-object v0, p1, LX/Dmx;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039876
    iget-object v0, p1, LX/Dmx;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039877
    :goto_3
    iget-object v0, p1, LX/Dmx;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039878
    goto/16 :goto_0

    .line 2039879
    :cond_7
    sget-object v1, LX/Dn0;->DATE_PICKER:LX/Dn0;

    if-ne v0, v1, :cond_8

    .line 2039880
    check-cast p1, LX/Dms;

    .line 2039881
    iget-object v0, p1, LX/Dms;->l:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->f:LX/Bm9;

    .line 2039882
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 2039883
    iget-object v0, p1, LX/Dms;->l:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->n:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2039884
    iput-wide v2, v0, Lcom/facebook/events/ui/date/DatePickerView;->f:J

    .line 2039885
    goto/16 :goto_0

    .line 2039886
    :cond_8
    sget-object v1, LX/Dn0;->TIME_SPAN_PICKER:LX/Dn0;

    if-ne v0, v1, :cond_0

    .line 2039887
    check-cast p1, LX/Dmz;

    .line 2039888
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 2039889
    iget-object v0, p1, LX/Dmz;->l:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->g:LX/BmO;

    .line 2039890
    iput-object v2, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 2039891
    iget-object v0, p1, LX/Dmz;->p:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->h:LX/BmO;

    .line 2039892
    iput-object v2, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 2039893
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    .line 2039894
    :goto_4
    if-eqz v0, :cond_e

    .line 2039895
    iget-object v0, p1, LX/Dmz;->m:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021508

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039896
    iget-object v0, p1, LX/Dmz;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2039897
    iget-object v0, p1, LX/Dmz;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2039898
    iget-object v0, p1, LX/Dmz;->p:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setVisibility(I)V

    .line 2039899
    iget-object v0, p1, LX/Dmz;->p:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 2039900
    :cond_9
    :goto_5
    goto/16 :goto_0

    .line 2039901
    :cond_a
    iget-object v0, p1, LX/Dmx;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2039902
    :cond_b
    iget-object v0, p1, LX/Dmx;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021508

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039903
    iget-object v0, p1, LX/Dmx;->n:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2039904
    iget-object v0, p1, LX/Dmx;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_d
    move v0, v1

    .line 2039905
    goto :goto_4

    .line 2039906
    :cond_e
    iget-object v0, p1, LX/Dmz;->m:Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039907
    iget-object v0, p1, LX/Dmz;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2039908
    iget-object v0, p1, LX/Dmz;->o:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2039909
    iget-object v0, p1, LX/Dmz;->p:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0, v4}, Lcom/facebook/events/ui/date/TimePickerView;->setVisibility(I)V

    .line 2039910
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->l:Ljava/util/Calendar;

    if-eqz v0, :cond_9

    .line 2039911
    iget-object v0, p1, LX/Dmz;->p:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->l:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    goto :goto_5
.end method

.method public final a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V
    .locals 4

    .prologue
    .line 2039914
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2039915
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2039916
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-nez v1, :cond_3

    .line 2039917
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2039918
    sget-object v1, LX/Dn0;->OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039919
    :goto_0
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2039920
    sget-object v1, LX/Dn0;->OPTIONAL_USER_AVAILABILITY:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039921
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2039922
    sget-object v1, LX/Dn0;->OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039923
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->c:LX/0Uh;

    const/16 v2, 0x61e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2039924
    sget-object v1, LX/Dn0;->OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039925
    :cond_2
    sget-object v1, LX/Dn0;->OPTIONAL_DIVIDER:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039926
    :cond_3
    sget-object v1, LX/Dn0;->HEADER_TEXT:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039927
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    if-eqz v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->e:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2039928
    :cond_5
    sget-object v1, LX/Dn0;->SERVICE_SUMMARY:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039929
    :cond_6
    sget-object v1, LX/Dn0;->DATE_PICKER:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039930
    sget-object v1, LX/Dn0;->TIME_SPAN_PICKER:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2039931
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->d:LX/0Px;

    .line 2039932
    return-void

    .line 2039933
    :cond_7
    sget-object v1, LX/Dn0;->OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2039913
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dn0;

    invoke-virtual {v0}, LX/Dn0;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2039912
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
