.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2034948
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    new-instance v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2034949
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2034950
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 2034951
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2034952
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    .line 2034953
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2034954
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2034955
    if-eqz v2, :cond_0

    .line 2034956
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034957
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2034958
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2034959
    if-eqz v2, :cond_1

    .line 2034960
    const-string v2, "booking_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034961
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2034962
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2034963
    if-eqz v2, :cond_2

    .line 2034964
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034965
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2034966
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2034967
    if-eqz v2, :cond_3

    .line 2034968
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034969
    invoke-static {v1, v2, p1, p2}, LX/Dl0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2034970
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2034971
    if-eqz v2, :cond_4

    .line 2034972
    const-string v3, "product_item"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034973
    invoke-static {v1, v2, p1, p2}, LX/Dl1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2034974
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2034975
    if-eqz v2, :cond_8

    .line 2034976
    const-string v3, "request_note"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034977
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2034978
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2034979
    if-eqz v3, :cond_7

    .line 2034980
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034981
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2034982
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_6

    .line 2034983
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2034984
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2034985
    const/4 v8, 0x0

    invoke-virtual {v1, v5, v8}, LX/15i;->g(II)I

    move-result v8

    .line 2034986
    if-eqz v8, :cond_5

    .line 2034987
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034988
    invoke-static {v1, v8, p1, p2}, LX/Dkt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2034989
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2034990
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2034991
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2034992
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2034993
    :cond_8
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2034994
    cmp-long v4, v2, v6

    if-eqz v4, :cond_9

    .line 2034995
    const-string v4, "requested_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2034996
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2034997
    :cond_9
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2034998
    if-eqz v2, :cond_a

    .line 2034999
    const-string v3, "service_general_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035000
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2035001
    :cond_a
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2035002
    if-eqz v2, :cond_b

    .line 2035003
    const-string v3, "special_request"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035004
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2035005
    :cond_b
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2035006
    cmp-long v4, v2, v6

    if-eqz v4, :cond_c

    .line 2035007
    const-string v4, "start_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035008
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2035009
    :cond_c
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2035010
    if-eqz v2, :cond_d

    .line 2035011
    const-string v3, "status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035012
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2035013
    :cond_d
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2035014
    if-eqz v2, :cond_e

    .line 2035015
    const-string v3, "suggested_time_range"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035016
    invoke-static {v1, v2, p1}, LX/Dl3;->a(LX/15i;ILX/0nX;)V

    .line 2035017
    :cond_e
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2035018
    if-eqz v2, :cond_f

    .line 2035019
    const-string v3, "user"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035020
    invoke-static {v1, v2, p1}, LX/Dl4;->a(LX/15i;ILX/0nX;)V

    .line 2035021
    :cond_f
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2035022
    if-eqz v2, :cond_10

    .line 2035023
    const-string v3, "user_availability"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2035024
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2035025
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2035026
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2035027
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$Serializer;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
