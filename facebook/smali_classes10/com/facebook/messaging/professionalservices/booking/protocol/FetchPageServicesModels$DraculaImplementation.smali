.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2037665
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2037666
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2037704
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2037705
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2037668
    if-nez p1, :cond_0

    move v0, v1

    .line 2037669
    :goto_0
    return v0

    .line 2037670
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2037671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2037672
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    .line 2037673
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2037674
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2037675
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2037676
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2037677
    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2037678
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2037679
    const v3, -0x758acc48

    const/4 v6, 0x0

    .line 2037680
    if-nez v2, :cond_1

    move v5, v6

    .line 2037681
    :goto_1
    move v2, v5

    .line 2037682
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2037683
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2037684
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2037685
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2037686
    :sswitch_2
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    .line 2037687
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2037688
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2037689
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2037690
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2037691
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2037692
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2037693
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2037694
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2037695
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2037696
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    .line 2037697
    if-nez p1, :cond_2

    const/4 v5, 0x0

    .line 2037698
    :goto_2
    if-ge v6, p1, :cond_3

    .line 2037699
    invoke-virtual {p0, v2, v6}, LX/15i;->q(II)I

    move-result p2

    .line 2037700
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v5, v6

    .line 2037701
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2037702
    :cond_2
    new-array v5, p1, [I

    goto :goto_2

    .line 2037703
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p3, v5, v6}, LX/186;->a([IZ)I

    move-result v5

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x758acc48 -> :sswitch_2
        -0x3ccf8461 -> :sswitch_3
        -0x1ee1dead -> :sswitch_1
        -0xf38064d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2037667
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2037660
    if-eqz p0, :cond_0

    .line 2037661
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2037662
    if-eq v0, p0, :cond_0

    .line 2037663
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2037664
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2037644
    sparse-switch p2, :sswitch_data_0

    .line 2037645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2037646
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    .line 2037647
    invoke-static {v0, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2037648
    :goto_0
    :sswitch_1
    return-void

    .line 2037649
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2037650
    const v1, -0x758acc48

    .line 2037651
    if-eqz v0, :cond_0

    .line 2037652
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2037653
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2037654
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2037655
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2037656
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2037657
    :cond_0
    goto :goto_0

    .line 2037658
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    .line 2037659
    invoke-static {v0, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x758acc48 -> :sswitch_3
        -0x3ccf8461 -> :sswitch_1
        -0x1ee1dead -> :sswitch_2
        -0xf38064d -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2037638
    if-eqz p1, :cond_0

    .line 2037639
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;

    move-result-object v1

    .line 2037640
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;

    .line 2037641
    if-eq v0, v1, :cond_0

    .line 2037642
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2037643
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2037637
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2037706
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2037707
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2037632
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2037633
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2037634
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a:LX/15i;

    .line 2037635
    iput p2, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->b:I

    .line 2037636
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2037631
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2037630
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2037627
    iget v0, p0, LX/1vt;->c:I

    .line 2037628
    move v0, v0

    .line 2037629
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2037624
    iget v0, p0, LX/1vt;->c:I

    .line 2037625
    move v0, v0

    .line 2037626
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2037606
    iget v0, p0, LX/1vt;->b:I

    .line 2037607
    move v0, v0

    .line 2037608
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2037621
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2037622
    move-object v0, v0

    .line 2037623
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2037612
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2037613
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2037614
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2037615
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2037616
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2037617
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2037618
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2037619
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2037620
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2037609
    iget v0, p0, LX/1vt;->c:I

    .line 2037610
    move v0, v0

    .line 2037611
    return v0
.end method
