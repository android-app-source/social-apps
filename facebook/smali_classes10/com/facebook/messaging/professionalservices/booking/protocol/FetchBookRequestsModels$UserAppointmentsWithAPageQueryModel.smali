.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29e8a591
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2036469
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2036470
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2036471
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2036472
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2036473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2036474
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2036475
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2036476
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2036477
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2036478
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2036479
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2036480
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2036481
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    .line 2036482
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2036483
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;

    .line 2036484
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    .line 2036485
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2036486
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2036487
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    .line 2036488
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2036489
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;-><init>()V

    .line 2036490
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2036491
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2036492
    const v0, 0x1a90c9bf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2036493
    const v0, -0x6747e1ce

    return v0
.end method
