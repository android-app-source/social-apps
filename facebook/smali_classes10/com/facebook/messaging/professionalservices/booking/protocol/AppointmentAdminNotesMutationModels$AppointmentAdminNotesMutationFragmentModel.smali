.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ea425f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2033778
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2033802
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2033800
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2033801
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2033794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2033795
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2033796
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2033797
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2033798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2033799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2033786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2033787
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2033788
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    .line 2033789
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2033790
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;

    .line 2033791
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    .line 2033792
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2033793
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRequestNote"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2033784
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    .line 2033785
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->e:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2033781
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;-><init>()V

    .line 2033782
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2033783
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2033780
    const v0, -0x134e8540

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2033779
    const v0, 0x7df28983

    return v0
.end method
