.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4e5906a4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2034694
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2034693
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2034691
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2034692
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2034678
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2034679
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2034680
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2034681
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2034682
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2034683
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2034684
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2034685
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2034686
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2034687
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2034688
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2034689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2034690
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2034660
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2034661
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2034662
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    .line 2034663
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2034664
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    .line 2034665
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->f:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    .line 2034666
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2034667
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    .line 2034668
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2034669
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    .line 2034670
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->h:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    .line 2034671
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2034672
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    .line 2034673
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2034674
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    .line 2034675
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->i:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    .line 2034676
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2034677
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034659
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2034656
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2034657
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->g:J

    .line 2034658
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2034653
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;-><init>()V

    .line 2034654
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2034655
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2034652
    const v0, -0xf4eb14f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2034651
    const v0, -0x55958a16

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034641
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->e:Ljava/lang/String;

    .line 2034642
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034649
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->f:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->f:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    .line 2034650
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->f:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 2034647
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2034648
    iget-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->g:J

    return-wide v0
.end method

.method public final m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034645
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->h:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->h:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    .line 2034646
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->h:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034643
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->i:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->i:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    .line 2034644
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->i:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    return-object v0
.end method
