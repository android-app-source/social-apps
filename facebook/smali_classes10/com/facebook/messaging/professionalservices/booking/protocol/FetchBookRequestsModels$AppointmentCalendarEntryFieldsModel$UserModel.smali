.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x183b9f40
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2034639
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2034624
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2034625
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2034626
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034627
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->e:Ljava/lang/String;

    .line 2034628
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2034610
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2034611
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2034612
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2034613
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x11ffd973

    invoke-static {v3, v2, v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2034614
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2034615
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2034616
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2034617
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2034618
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2034619
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2034629
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2034630
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2034631
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x11ffd973

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2034632
    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2034633
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    .line 2034634
    iput v3, v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->g:I

    .line 2034635
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2034636
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2034637
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2034638
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2034640
    new-instance v0, LX/Dki;

    invoke-direct {v0, p1}, LX/Dki;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034620
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2034621
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2034622
    const/4 v0, 0x2

    const v1, -0x11ffd973

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->g:I

    .line 2034623
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2034598
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2034599
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2034600
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2034601
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;-><init>()V

    .line 2034602
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2034603
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2034604
    const v0, -0x64112c16

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2034605
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034606
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->f:Ljava/lang/String;

    .line 2034607
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034608
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2034609
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
