.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2035595
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2035596
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2035593
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2035594
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2035542
    if-nez p1, :cond_0

    move v0, v1

    .line 2035543
    :goto_0
    return v0

    .line 2035544
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2035545
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2035546
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2035547
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2035548
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035549
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035550
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2035551
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2035552
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2035553
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035554
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035555
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2035556
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2035557
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2035558
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035559
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035560
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2035561
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    .line 2035562
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2035563
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035564
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035565
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2035566
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2035567
    const v2, -0x3972da7b

    const/4 v5, 0x0

    .line 2035568
    if-nez v0, :cond_1

    move v3, v5

    .line 2035569
    :goto_1
    move v0, v3

    .line 2035570
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035571
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035572
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2035573
    :sswitch_5
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    .line 2035574
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2035575
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2035576
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035577
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2035578
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2035579
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2035580
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2035581
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2035582
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2035583
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2035584
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2035585
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2035586
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 2035587
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2035588
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2035589
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2035590
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2035591
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 2035592
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5ae19418 -> :sswitch_3
        -0x3972da7b -> :sswitch_5
        -0x38a43f39 -> :sswitch_0
        -0x11ffd973 -> :sswitch_1
        -0xd29f48c -> :sswitch_4
        -0x47543d7 -> :sswitch_6
        0x2b956099 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2035533
    if-nez p0, :cond_0

    move v0, v1

    .line 2035534
    :goto_0
    return v0

    .line 2035535
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2035536
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2035537
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2035538
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2035539
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2035540
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2035541
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2035526
    const/4 v7, 0x0

    .line 2035527
    const/4 v1, 0x0

    .line 2035528
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2035529
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2035530
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2035531
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2035532
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2035525
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2035469
    if-eqz p0, :cond_0

    .line 2035470
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2035471
    if-eq v0, p0, :cond_0

    .line 2035472
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2035473
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2035509
    sparse-switch p2, :sswitch_data_0

    .line 2035510
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2035511
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    .line 2035512
    invoke-static {v0, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2035513
    :goto_0
    :sswitch_1
    return-void

    .line 2035514
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2035515
    const v1, -0x3972da7b

    .line 2035516
    if-eqz v0, :cond_0

    .line 2035517
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2035518
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2035519
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2035520
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2035521
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2035522
    :cond_0
    goto :goto_0

    .line 2035523
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    .line 2035524
    invoke-static {v0, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5ae19418 -> :sswitch_0
        -0x3972da7b -> :sswitch_3
        -0x38a43f39 -> :sswitch_1
        -0x11ffd973 -> :sswitch_1
        -0xd29f48c -> :sswitch_2
        -0x47543d7 -> :sswitch_1
        0x2b956099 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2035503
    if-eqz p1, :cond_0

    .line 2035504
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v1

    .line 2035505
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    .line 2035506
    if-eq v0, v1, :cond_0

    .line 2035507
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2035508
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2035502
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2035500
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2035501
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2035464
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2035465
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2035466
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a:LX/15i;

    .line 2035467
    iput p2, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->b:I

    .line 2035468
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2035474
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2035475
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2035476
    iget v0, p0, LX/1vt;->c:I

    .line 2035477
    move v0, v0

    .line 2035478
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2035479
    iget v0, p0, LX/1vt;->c:I

    .line 2035480
    move v0, v0

    .line 2035481
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2035482
    iget v0, p0, LX/1vt;->b:I

    .line 2035483
    move v0, v0

    .line 2035484
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2035485
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2035486
    move-object v0, v0

    .line 2035487
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2035488
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2035489
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2035490
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2035491
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2035492
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2035493
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2035494
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2035495
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2035496
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2035497
    iget v0, p0, LX/1vt;->c:I

    .line 2035498
    move v0, v0

    .line 2035499
    return v0
.end method
