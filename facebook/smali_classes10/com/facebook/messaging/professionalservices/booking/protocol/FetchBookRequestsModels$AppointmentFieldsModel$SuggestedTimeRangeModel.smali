.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4a2dffad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2035351
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2035350
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2035348
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2035349
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2035342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2035343
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2035344
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2035345
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->f:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2035346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2035347
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2035326
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2035327
    iget-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2035339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2035340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2035341
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2035335
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2035336
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->e:J

    .line 2035337
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->f:J

    .line 2035338
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2035332
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;-><init>()V

    .line 2035333
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2035334
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2035331
    const v0, 0x355aed14

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2035330
    const v0, 0x78e1ce50

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 2035328
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2035329
    iget-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->f:J

    return-wide v0
.end method
