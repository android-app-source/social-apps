.class public final Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2037773
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    new-instance v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2037774
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2037753
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2037755
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2037756
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2037757
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2037758
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2037759
    if-eqz v2, :cond_0

    .line 2037760
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037761
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2037762
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2037763
    if-eqz v2, :cond_2

    .line 2037764
    const-string p0, "services_card"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2037766
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2037767
    if-eqz p0, :cond_1

    .line 2037768
    const-string v0, "product_catalog"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037769
    invoke-static {v1, p0, p1, p2}, LX/DlP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2037770
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2037771
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2037772
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2037754
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$Serializer;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
