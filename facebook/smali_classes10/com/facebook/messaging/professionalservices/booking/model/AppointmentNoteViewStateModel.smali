.class public Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2033609
    new-instance v0, LX/Djj;

    invoke-direct {v0}, LX/Djj;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2033604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033605
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    .line 2033606
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2033607
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    .line 2033608
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033600
    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    .line 2033601
    iput-object p2, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2033602
    iput-object p3, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    .line 2033603
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2033598
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2033594
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2033595
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2033596
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033597
    return-void
.end method
