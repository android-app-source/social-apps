.class public Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:J

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Z

.field public final o:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2033728
    new-instance v0, LX/Djm;

    invoke-direct {v0}, LX/Djm;-><init>()V

    sput-object v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Djn;)V
    .locals 2

    .prologue
    .line 2033690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033691
    iget-boolean v0, p1, LX/Djn;->a:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    .line 2033692
    iget-object v0, p1, LX/Djn;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    .line 2033693
    iget-object v0, p1, LX/Djn;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->c:Ljava/lang/String;

    .line 2033694
    iget-object v0, p1, LX/Djn;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->d:Ljava/lang/String;

    .line 2033695
    iget-object v0, p1, LX/Djn;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->e:Ljava/lang/String;

    .line 2033696
    iget-object v0, p1, LX/Djn;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    .line 2033697
    iget-object v0, p1, LX/Djn;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    .line 2033698
    iget-wide v0, p1, LX/Djn;->h:J

    iput-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->h:J

    .line 2033699
    iget-object v0, p1, LX/Djn;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    .line 2033700
    iget-object v0, p1, LX/Djn;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    .line 2033701
    iget-object v0, p1, LX/Djn;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->k:Ljava/lang/String;

    .line 2033702
    iget-object v0, p1, LX/Djn;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    .line 2033703
    iget-object v0, p1, LX/Djn;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    .line 2033704
    iget-boolean v0, p1, LX/Djn;->n:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    .line 2033705
    iget-object v0, p1, LX/Djn;->o:Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    .line 2033706
    iget-object v0, p1, LX/Djn;->p:Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    .line 2033707
    iget-object v0, p1, LX/Djn;->q:Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    .line 2033708
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2033709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033710
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    .line 2033711
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    .line 2033712
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->c:Ljava/lang/String;

    .line 2033713
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->d:Ljava/lang/String;

    .line 2033714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->e:Ljava/lang/String;

    .line 2033715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    .line 2033716
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    .line 2033717
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->h:J

    .line 2033718
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    .line 2033719
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    .line 2033720
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->k:Ljava/lang/String;

    .line 2033721
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    .line 2033722
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    .line 2033723
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    .line 2033724
    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    .line 2033725
    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    .line 2033726
    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    .line 2033727
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2033689
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2033674
    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2033675
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033676
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033677
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033678
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033679
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033680
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033681
    iget-wide v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2033682
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033683
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033684
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033685
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033686
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2033687
    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2033688
    return-void
.end method
