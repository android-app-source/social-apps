.class public Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/Dix;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032751
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2032752
    return-void
.end method

.method public static a(LX/Diw;)Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;
    .locals 3

    .prologue
    .line 2032746
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2032747
    const-string v1, "arg_confirmation_type"

    invoke-virtual {p0}, LX/Diw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032748
    new-instance v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;-><init>()V

    .line 2032749
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032750
    return-object v1
.end method

.method private l()Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 4

    .prologue
    .line 2032719
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032720
    const-string v1, "arg_confirmation_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2032721
    sget-object v1, LX/Diw;->ADMIN_CANCEL:LX/Diw;

    invoke-virtual {v1}, LX/Diw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/Diw;->USER_CANCEL:LX/Diw;

    invoke-virtual {v1}, LX/Diw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2032722
    :cond_0
    new-instance v0, LX/6dy;

    const-string v1, ""

    const v2, 0x7f080020

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f082bbd

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2032723
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 2032724
    move-object v0, v0

    .line 2032725
    const v1, 0x7f080021

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2032726
    iput-object v1, v0, LX/6dy;->e:Ljava/lang/String;

    .line 2032727
    move-object v0, v0

    .line 2032728
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 2032729
    :goto_0
    return-object v0

    .line 2032730
    :cond_1
    sget-object v1, LX/Diw;->ADMIN_DECLINE:LX/Diw;

    invoke-virtual {v1}, LX/Diw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2032731
    new-instance v0, LX/6dy;

    const-string v1, ""

    const v2, 0x7f080020

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f082bbe

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2032732
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 2032733
    move-object v0, v0

    .line 2032734
    const v1, 0x7f080021

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2032735
    iput-object v1, v0, LX/6dy;->e:Ljava/lang/String;

    .line 2032736
    move-object v0, v0

    .line 2032737
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    goto :goto_0

    .line 2032738
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2032743
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->m:LX/Dix;

    if-eqz v0, :cond_0

    .line 2032744
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->m:LX/Dix;

    invoke-interface {v0}, LX/Dix;->a()V

    .line 2032745
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x797ad97

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2032739
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2032740
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->l()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v1

    .line 2032741
    iput-object v1, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2032742
    const/16 v1, 0x2b

    const v2, -0x363c7725

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
