.class public Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dk0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/16I;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/Dlh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/Dih;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private k:Landroid/content/Context;

.field public l:LX/Djz;

.field private m:LX/DkQ;

.field private n:Lcom/facebook/widget/FbSwipeRefreshLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/Dlg;

.field private p:LX/Dip;

.field private q:LX/Diq;

.field public r:LX/1P1;

.field public s:Z

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2032707
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2032708
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->s:Z

    return-void
.end method

.method public static a(I)Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;
    .locals 3

    .prologue
    .line 2032702
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;-><init>()V

    .line 2032703
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2032704
    const-string v2, "arg_fragment_index"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2032705
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2032706
    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 2032681
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->d:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2032682
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->e(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    .line 2032683
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0}, LX/Dlg;->e()V

    .line 2032684
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0}, LX/Dlg;->g()V

    .line 2032685
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2032686
    :goto_0
    return-void

    .line 2032687
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    .line 2032688
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Dlg;->n:Z

    .line 2032689
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2032690
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->s:Z

    .line 2032691
    if-eqz p1, :cond_1

    .line 2032692
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->l:LX/Djz;

    .line 2032693
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Djz;->f:Z

    .line 2032694
    const/4 v1, 0x0

    iput-object v1, v0, LX/Djz;->g:Ljava/lang/String;

    .line 2032695
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->l:LX/Djz;

    new-instance v1, LX/Div;

    invoke-direct {v1, p0, p1}, LX/Div;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;Z)V

    .line 2032696
    iget-object v2, v0, LX/Djz;->a:LX/1Ck;

    const-string v3, "fetch_appointments_for_calendar"

    new-instance p0, LX/Djw;

    invoke-direct {p0, v0}, LX/Djw;-><init>(LX/Djz;)V

    new-instance p1, LX/Djx;

    invoke-direct {p1, v0, v1}, LX/Djx;-><init>(LX/Djz;LX/Div;)V

    invoke-virtual {v2, v3, p0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2032697
    goto :goto_0
.end method

.method public static c(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V
    .locals 1

    .prologue
    .line 2032700
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->a(Z)V

    .line 2032701
    return-void
.end method

.method public static d(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V
    .locals 1

    .prologue
    .line 2032698
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->a(Z)V

    .line 2032699
    return-void
.end method

.method public static e(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V
    .locals 2

    .prologue
    .line 2032709
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->n:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2032710
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->n:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2032711
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032646
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2032647
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    const-class v3, LX/Dk0;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Dk0;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v6

    check-cast v6, LX/16I;

    const/16 v7, 0x19e

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const-class v8, LX/Dlh;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Dlh;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v0

    check-cast v0, LX/Dih;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->a:LX/Dk0;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->d:LX/16I;

    iput-object v7, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->e:LX/0Or;

    iput-object v8, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->f:LX/Dlh;

    iput-object v9, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->g:LX/0TD;

    iput-object v10, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->h:Ljava/util/concurrent/Executor;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->i:LX/Dih;

    .line 2032648
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010593

    const v2, 0x7f0e0a87

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->k:Landroid/content/Context;

    .line 2032649
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032650
    const-string v1, "arg_fragment_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2032651
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->b:Ljava/lang/String;

    .line 2032652
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2032653
    const-string v2, "arg_appointments_query_scenario"

    sget-object v3, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_UPCOMING_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2032654
    const-string v2, "arg_appointments_query_param_page_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032655
    new-instance v2, LX/DkQ;

    invoke-direct {v2, v1}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    move-object v0, v2

    .line 2032656
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->m:LX/DkQ;

    .line 2032657
    const-string v0, "upcoming"

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->u:Ljava/lang/String;

    .line 2032658
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->a:LX/Dk0;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->m:LX/DkQ;

    .line 2032659
    new-instance v3, LX/Djz;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, LX/Djz;-><init>(LX/DkQ;LX/0tX;LX/1Ck;Landroid/content/Context;LX/03V;)V

    .line 2032660
    move-object v0, v3

    .line 2032661
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->l:LX/Djz;

    .line 2032662
    const-string v0, "key_appointments"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    .line 2032663
    return-void

    .line 2032664
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032665
    const-string v1, "arg_fragment_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2032666
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->b:Ljava/lang/String;

    .line 2032667
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2032668
    const-string v2, "arg_appointments_query_scenario"

    sget-object v3, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_PAST_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2032669
    const-string v2, "arg_appointments_query_param_page_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032670
    new-instance v2, LX/DkQ;

    invoke-direct {v2, v1}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    move-object v0, v2

    .line 2032671
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->m:LX/DkQ;

    .line 2032672
    const-string v0, "past"

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->u:Ljava/lang/String;

    goto :goto_0

    .line 2032673
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid fragment type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2032623
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2032624
    if-ne p1, v0, :cond_0

    .line 2032625
    if-ne p2, v0, :cond_0

    .line 2032626
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->d(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    .line 2032627
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x32e6f5ce

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2032628
    const v0, 0x7f030104

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2032629
    const v0, 0x7f0d0596

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2032630
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->r:LX/1P1;

    .line 2032631
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->r:LX/1P1;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2032632
    new-instance v0, LX/Dip;

    invoke-direct {v0, p0}, LX/Dip;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->p:LX/Dip;

    .line 2032633
    new-instance v0, LX/Diq;

    invoke-direct {v0, p0}, LX/Diq;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->q:LX/Diq;

    .line 2032634
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->f:LX/Dlh;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->k:Landroid/content/Context;

    .line 2032635
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2032636
    const-string v5, "arg_fragment_index"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->p:LX/Dip;

    iget-object v6, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->q:LX/Diq;

    invoke-virtual {v0, v3, v4, v5, v6}, LX/Dlh;->a(Landroid/content/Context;ILX/Dip;LX/Diq;)LX/Dlg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    .line 2032637
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2032638
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/Dir;

    invoke-direct {v3, p0}, LX/Dir;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2032639
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2032640
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->d(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    .line 2032641
    :goto_0
    const v0, -0x18fbac04

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2032642
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    .line 2032643
    invoke-virtual {v0}, LX/Dlg;->f()V

    .line 2032644
    invoke-virtual {v0, v3}, LX/Dlg;->a(Ljava/util/List;)V

    .line 2032645
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2032674
    const-string v0, "key_appointments"

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2032675
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2032676
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032677
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2032678
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->n:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2032679
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->n:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/Dis;

    invoke-direct {v1, p0}, LX/Dis;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2032680
    return-void
.end method
