.class public Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DlS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/support/v7/widget/RecyclerView;

.field public h:Landroid/view/ViewStub;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2033278
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2033279
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033280
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    new-instance p1, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p1, v6}, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;-><init>(Landroid/content/Context;)V

    move-object v6, p1

    check-cast v6, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    invoke-static {v0}, LX/DlS;->b(LX/0QB;)LX/DlS;

    move-result-object p1

    check-cast p1, LX/DlS;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->a:LX/0tX;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->c:LX/1Ck;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    iput-object p1, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->e:LX/DlS;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->f:LX/0kL;

    .line 2033281
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    new-instance v1, LX/DjO;

    invoke-direct {v1, p0}, LX/DjO;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;)V

    .line 2033282
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->e:LX/DjO;

    .line 2033283
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->c:LX/1Ck;

    const-string v1, "fetch_page_services"

    new-instance v2, LX/DjQ;

    invoke-direct {v2, p0}, LX/DjQ;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;)V

    new-instance v3, LX/DjR;

    invoke-direct {v3, p0}, LX/DjR;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2033284
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3e10f165

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033285
    const v1, 0x7f030ea7

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x74ab79cb

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xdb4643c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033286
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2033287
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2033288
    if-eqz v1, :cond_0

    .line 2033289
    const v2, 0x7f082bba

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2033290
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2033291
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x73600234

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033292
    const v0, 0x7f0d23dd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->g:Landroid/support/v7/widget/RecyclerView;

    .line 2033293
    const v0, 0x7f0d1c6d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->h:Landroid/view/ViewStub;

    .line 2033294
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2033295
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/DjS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1e65

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v2}, LX/DjS;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2033296
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->g:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2033297
    return-void
.end method
