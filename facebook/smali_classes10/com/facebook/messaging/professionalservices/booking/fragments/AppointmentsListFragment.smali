.class public Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DkO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/DmR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Dih;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:Landroid/content/Context;

.field public g:LX/Dic;

.field public h:LX/DmQ;

.field private i:LX/DkN;

.field public j:LX/Did;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/widget/FbSwipeRefreshLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/DkQ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032804
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2032805
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;I)V
    .locals 1

    .prologue
    .line 2032801
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->g:LX/Dic;

    if-eqz v0, :cond_0

    .line 2032802
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->g:LX/Dic;

    invoke-virtual {v0, p1}, LX/Dic;->a(I)V

    .line 2032803
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;Z)V
    .locals 1

    .prologue
    .line 2032798
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2032799
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2032800
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V
    .locals 5

    .prologue
    .line 2032766
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->i:LX/DkN;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032767
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->j:LX/Did;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2032768
    const v0, 0x7f082b8f

    invoke-static {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;I)V

    .line 2032769
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->i:LX/DkN;

    new-instance v1, LX/Diz;

    invoke-direct {v1, p0}, LX/Diz;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    .line 2032770
    iget-object v2, v0, LX/DkN;->b:LX/1Ck;

    const-string v3, "fetch_appointments"

    new-instance v4, LX/Dk6;

    invoke-direct {v4, v0}, LX/Dk6;-><init>(LX/DkN;)V

    new-instance p0, LX/Dk7;

    invoke-direct {p0, v0, v1}, LX/Dk7;-><init>(LX/DkN;LX/Diz;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2032771
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032806
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2032807
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    const-class v3, LX/DkO;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DkO;

    const-class v4, LX/DmR;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DmR;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object p1

    check-cast p1, LX/Dih;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a:LX/DkO;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b:LX/DmR;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->c:Ljava/lang/String;

    iput-object p1, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->d:LX/Dih;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->e:LX/03V;

    .line 2032808
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010593

    const v2, 0x7f0e0a87

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->f:Landroid/content/Context;

    .line 2032809
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032810
    const-string v1, "arg_appointment_query_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/DkQ;->a(Landroid/os/Bundle;)LX/DkQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->l:LX/DkQ;

    .line 2032811
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b:LX/DmR;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->l:LX/DkQ;

    .line 2032812
    new-instance v3, LX/DmQ;

    invoke-static {v0}, LX/DnT;->b(LX/0QB;)LX/DnT;

    move-result-object v6

    check-cast v6, LX/DnT;

    invoke-static {v0}, LX/DnP;->a(LX/0QB;)LX/DnP;

    move-result-object v7

    check-cast v7, LX/DnP;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    move-object v4, v1

    move-object v5, v2

    invoke-direct/range {v3 .. v10}, LX/DmQ;-><init>(Landroid/content/Context;LX/DkQ;LX/DnT;LX/DnP;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Uh;Landroid/content/Context;)V

    .line 2032813
    move-object v0, v3

    .line 2032814
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    .line 2032815
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a:LX/DkO;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->l:LX/DkQ;

    invoke-virtual {v0, v1}, LX/DkO;->a(LX/DkQ;)LX/DkN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->i:LX/DkN;

    .line 2032816
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 2032787
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2032788
    if-ne p1, v1, :cond_1

    .line 2032789
    if-ne p2, v1, :cond_0

    .line 2032790
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    .line 2032791
    :cond_0
    :goto_0
    return-void

    .line 2032792
    :cond_1
    if-ne p1, v0, :cond_2

    .line 2032793
    if-ne p2, v0, :cond_0

    .line 2032794
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    goto :goto_0

    .line 2032795
    :cond_2
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 2032796
    if-ne p2, v1, :cond_0

    .line 2032797
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x6fcd9b16

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2032780
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->f:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2032781
    const v2, 0x7f03010c

    invoke-virtual {v0, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2032782
    const v0, 0x7f0d05aa

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2032783
    new-instance v3, LX/1P1;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v5}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2032784
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2032785
    invoke-static {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->b(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    .line 2032786
    const/16 v0, 0x2b

    const v3, 0x1f5b9e31

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3cff43f4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2032776
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2032777
    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2032778
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2032779
    const/16 v1, 0x2b

    const v2, -0x6a882d18

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032772
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2032773
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2032774
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->k:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/Diy;

    invoke-direct {v1, p0}, LX/Diy;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2032775
    return-void
.end method
