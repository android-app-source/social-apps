.class public Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/DnK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/DkO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Dih;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

.field public n:Z

.field public o:Lcom/facebook/resources/ui/FbButton;

.field public p:LX/Dif;

.field public q:LX/DkN;

.field public r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

.field public final s:LX/Dix;

.field private t:LX/DkQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2033127
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2033128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->n:Z

    .line 2033129
    new-instance v0, LX/DjI;

    invoke-direct {v0, p0}, LX/DjI;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->s:LX/Dix;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;I)V
    .locals 1

    .prologue
    .line 2033130
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->p:LX/Dif;

    if-eqz v0, :cond_0

    .line 2033131
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->p:LX/Dif;

    invoke-virtual {v0, p1}, LX/Dif;->a(I)V

    .line 2033132
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033133
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033134
    const-string v1, "thread_booking_requests"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2033135
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->e:LX/Dih;

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->t:LX/DkQ;

    invoke-virtual {v0}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033136
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2033137
    iget-object v4, v1, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_error"

    const/4 v6, 0x0

    invoke-static {v5, v6}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "error_category"

    invoke-virtual {v5, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "query_config"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "viewer"

    invoke-virtual {v5, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "thread_booking_requests"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2033138
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->f:LX/03V;

    const-string v4, "%s,%s,viewer:%s,threadBookingRequests%s"

    if-nez p2, :cond_0

    const-string v0, ""

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->t:LX/DkQ;

    invoke-virtual {v0}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033139
    iget-object v6, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v6

    .line 2033140
    invoke-static {v4, v1, v5, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033141
    return-void

    .line 2033142
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2033143
    iget-boolean v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    .line 2033144
    iget-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2033145
    iget-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-object v1, v0

    .line 2033146
    move-object v0, v1

    .line 2033147
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne v0, v1, :cond_0

    .line 2033148
    sget-object v0, LX/DjN;->DISCARD_CHANGES:LX/DjN;

    .line 2033149
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2033150
    const-string v2, "arg_confirmation_type"

    invoke-virtual {v0}, LX/DjN;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033151
    new-instance v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;-><init>()V

    .line 2033152
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2033153
    move-object v0, v2

    .line 2033154
    new-instance v1, LX/DjM;

    invoke-direct {v1, p0}, LX/DjM;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;)V

    .line 2033155
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;->m:LX/DjM;

    .line 2033156
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "page_admin_appointment_note_confirmation_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2033157
    const/4 v0, 0x1

    .line 2033158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033159
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2033160
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const-class v3, LX/DnK;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DnK;

    const-class v4, LX/DkO;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DkO;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v7

    check-cast v7, LX/Dih;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const/16 v9, 0x19e

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a:LX/DnK;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->b:LX/DkO;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->c:Landroid/content/Context;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->d:LX/0kL;

    iput-object v7, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->e:LX/Dih;

    iput-object v8, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->f:LX/03V;

    iput-object v9, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->g:LX/0Or;

    iput-object v10, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->h:LX/0SG;

    iput-object v11, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->i:LX/0Uh;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->j:Lcom/facebook/content/SecureContextHelper;

    .line 2033161
    if-eqz p1, :cond_0

    .line 2033162
    const-string v0, "state_appointment_note_view_state_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2033163
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033164
    const-string v1, "arg_appointment_query_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/DkQ;->a(Landroid/os/Bundle;)LX/DkQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->t:LX/DkQ;

    .line 2033165
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033166
    const-string v1, "referrer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->l:Ljava/lang/String;

    .line 2033167
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->b:LX/DkO;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->t:LX/DkQ;

    invoke-virtual {v0, v1}, LX/DkO;->a(LX/DkQ;)LX/DkN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->q:LX/DkN;

    .line 2033168
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a:LX/DnK;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->l:Ljava/lang/String;

    .line 2033169
    new-instance v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/DnT;->b(LX/0QB;)LX/DnT;

    move-result-object v5

    check-cast v5, LX/DnT;

    const/16 v3, 0x19e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v3, LX/Dlo;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Dlo;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    .line 2033170
    new-instance v12, LX/Djv;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-direct {v12, v3, v10, v11}, LX/Djv;-><init>(LX/0tX;LX/1Ck;LX/0kL;)V

    .line 2033171
    move-object v10, v12

    .line 2033172
    check-cast v10, LX/Djv;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v11

    check-cast v11, LX/Dih;

    move-object v3, v1

    invoke-direct/range {v2 .. v11}, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;-><init>(Ljava/lang/String;Landroid/content/Context;LX/DnT;LX/0Or;LX/Dlo;LX/0wM;LX/0Uh;LX/Djv;LX/Dih;)V

    .line 2033173
    move-object v0, v2

    .line 2033174
    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    .line 2033175
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    new-instance v1, LX/DjJ;

    invoke-direct {v1, p0}, LX/DjJ;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;)V

    .line 2033176
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->l:LX/DjJ;

    .line 2033177
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2033178
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2033179
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 2033180
    if-ne p2, v1, :cond_0

    .line 2033181
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2033182
    iput-boolean v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->n:Z

    .line 2033183
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2033184
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x51a944e3

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2033185
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2033186
    const v2, 0x7f030e10

    invoke-virtual {v0, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2033187
    const v0, 0x7f0d0b9a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2033188
    new-instance v3, LX/1P1;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v5}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2033189
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2033190
    const v0, 0x7f0d09a9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2033191
    const v3, 0x7f082b8f

    invoke-static {p0, v3}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;I)V

    .line 2033192
    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->q:LX/DkN;

    new-instance v4, LX/DjL;

    invoke-direct {v4, p0, v0}, LX/DjL;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;Landroid/view/ViewStub;)V

    invoke-virtual {v3, v4}, LX/DkN;->a(LX/Dj9;)V

    .line 2033193
    const/16 v0, 0x2b

    const v3, 0x3a034db4

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x114cdb59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033194
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2033195
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->q:LX/DkN;

    invoke-virtual {v1}, LX/DkN;->a()V

    .line 2033196
    const/16 v1, 0x2b

    const v2, -0x3ae02594

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2033197
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2033198
    const-string v0, "state_appointment_note_view_state_data"

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2033199
    return-void
.end method
