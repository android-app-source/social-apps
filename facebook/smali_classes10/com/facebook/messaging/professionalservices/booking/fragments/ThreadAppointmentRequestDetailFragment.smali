.class public Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DmD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/DkO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Dih;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/01T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/DnV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:Landroid/content/Context;

.field public k:LX/Die;

.field public l:LX/DkN;

.field public m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

.field public n:Lcom/facebook/resources/ui/FbButton;

.field public o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:LX/Dix;

.field private q:LX/DkQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2033568
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2033569
    new-instance v0, LX/Dje;

    invoke-direct {v0, p0}, LX/Dje;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->p:LX/Dix;

    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 2033563
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 2033564
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2033565
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2033566
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 2033567
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;I)V
    .locals 1

    .prologue
    .line 2033511
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->k:LX/Die;

    if-eqz v0, :cond_0

    .line 2033512
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->k:LX/Die;

    invoke-virtual {v0, p1}, LX/Die;->a(I)V

    .line 2033513
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033557
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033558
    const-string v1, "thread_booking_requests"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2033559
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->d:LX/Dih;

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->q:LX/DkQ;

    invoke-virtual {v2}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v2

    .line 2033560
    iget-object v3, v1, LX/Dih;->a:LX/0Zb;

    const-string v4, "profservices_booking_error"

    const/4 v5, 0x0

    invoke-static {v4, v5}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "error_category"

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "query_config"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "thread_booking_requests"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2033561
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->e:LX/03V;

    const-string v2, "%s,%s,threadBookingRequests%s"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->q:LX/DkQ;

    invoke-virtual {v4}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033562
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033547
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2033548
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    const-class v3, LX/DmD;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DmD;

    const-class v4, LX/DkO;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DkO;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v6

    check-cast v6, LX/Dih;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v8

    check-cast v8, LX/01T;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/DnV;->b(LX/0QB;)LX/DnV;

    move-result-object p1

    check-cast p1, LX/DnV;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a:LX/DmD;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->b:LX/DkO;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->c:LX/0kL;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->d:LX/Dih;

    iput-object v7, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->e:LX/03V;

    iput-object v8, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->f:LX/01T;

    iput-object v9, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->g:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->h:LX/DnV;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->i:Lcom/facebook/content/SecureContextHelper;

    .line 2033549
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033550
    const-string v1, "arg_appointment_query_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/DkQ;->a(Landroid/os/Bundle;)LX/DkQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->q:LX/DkQ;

    .line 2033551
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010593

    const v2, 0x7f0e0a87

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->j:Landroid/content/Context;

    .line 2033552
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a:LX/DmD;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/DmD;->a(Landroid/content/Context;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    .line 2033553
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->b:LX/DkO;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->q:LX/DkQ;

    invoke-virtual {v0, v1}, LX/DkO;->a(LX/DkQ;)LX/DkN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->l:LX/DkN;

    .line 2033554
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    new-instance v1, LX/Djf;

    invoke-direct {v1, p0}, LX/Djf;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V

    .line 2033555
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    .line 2033556
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2033541
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2033542
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 2033543
    if-ne p2, v1, :cond_0

    .line 2033544
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2033545
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2033546
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x102c6bcf

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2033517
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->j:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2033518
    const v1, 0x7f030b8f

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2033519
    const v0, 0x7f0d0b9a

    invoke-static {v7, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2033520
    new-instance v1, LX/1P1;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2033521
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2033522
    const v0, 0x7f0d1cb0

    invoke-static {v7, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2033523
    new-instance v5, LX/Djg;

    invoke-direct {v5, p0}, LX/Djg;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V

    .line 2033524
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v4, v0

    .line 2033525
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->h:LX/DnV;

    invoke-virtual {v0}, LX/DnV;->a()LX/DnU;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f082ba1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    .line 2033526
    sget-object p1, LX/DnU;->FBUI:LX/DnU;

    if-ne v2, p1, :cond_1

    .line 2033527
    const p1, 0x7f030df3

    invoke-virtual {v1, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2033528
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbButton;

    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    .line 2033529
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033530
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-static {p1, v4, v4, v4, v4}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a(Landroid/view/View;IIII)V

    .line 2033531
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v5}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2033532
    :cond_0
    :goto_0
    const v0, 0x7f082b8f

    invoke-static {p0, v0}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;I)V

    .line 2033533
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->l:LX/DkN;

    new-instance v1, LX/Djh;

    invoke-direct {v1, p0}, LX/Djh;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V

    invoke-virtual {v0, v1}, LX/DkN;->a(LX/Dj9;)V

    .line 2033534
    const/16 v0, 0x2b

    const v1, 0x2c0f1b80

    invoke-static {v8, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v7

    .line 2033535
    :cond_1
    sget-object p1, LX/DnU;->FIG:LX/DnU;

    if-ne v2, p1, :cond_0

    .line 2033536
    const p1, 0x7f030df4

    invoke-virtual {v1, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2033537
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigButton;

    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    .line 2033538
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033539
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-static {p1, v4, v4, v4, v4}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a(Landroid/view/View;IIII)V

    .line 2033540
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v5}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6145be80

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033514
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2033515
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->l:LX/DkN;

    invoke-virtual {v1}, LX/DkN;->a()V

    .line 2033516
    const/16 v1, 0x2b

    const v2, 0x7f198d32

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
