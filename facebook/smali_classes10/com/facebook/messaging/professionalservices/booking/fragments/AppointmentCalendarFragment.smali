.class public Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dih;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

.field public d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field public e:LX/Din;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032586
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2032587
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032565
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2032566
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object p1

    check-cast p1, LX/Dih;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->a:LX/Dih;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->b:Ljava/lang/String;

    .line 2032567
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x29e14c7b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2032568
    const v1, 0x7f0300ff

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2032569
    new-instance v2, LX/Din;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/Din;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;LX/0gc;)V

    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->e:LX/Din;

    .line 2032570
    const v2, 0x7f0d058f

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2032571
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->e:LX/Din;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2032572
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    const/4 v3, 0x1

    .line 2032573
    iput-boolean v3, v2, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 2032574
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    new-instance v3, LX/Dim;

    invoke-direct {v3, p0}, LX/Dim;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2032575
    const v2, 0x7f0d058e

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    iput-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->c:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    .line 2032576
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->c:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->d:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->setViewPager(Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;)V

    .line 2032577
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->a:LX/Dih;

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->b:Ljava/lang/String;

    .line 2032578
    iget-object p0, v2, LX/Dih;->a:LX/0Zb;

    const-string p1, "appointment_calendar_view_impression"

    invoke-static {p1, v3}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032579
    const/16 v2, 0x2b

    const v3, -0x920435c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2ecab7b1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2032580
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2032581
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2032582
    if-eqz v1, :cond_0

    .line 2032583
    const v2, 0x7f082bbf

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2032584
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2032585
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x62d0edc5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
