.class public Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/DjM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2033227
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2033228
    return-void
.end method

.method private l()Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 4

    .prologue
    .line 2033205
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2033206
    const-string v1, "arg_confirmation_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2033207
    sget-object v1, LX/DjN;->DISCARD_CHANGES:LX/DjN;

    invoke-virtual {v1}, LX/DjN;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2033208
    new-instance v0, LX/6dy;

    const-string v1, ""

    const v2, 0x7f082be8

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f082bed

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2033209
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 2033210
    move-object v0, v0

    .line 2033211
    const v1, 0x7f082be9

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2033212
    iput-object v1, v0, LX/6dy;->e:Ljava/lang/String;

    .line 2033213
    move-object v0, v0

    .line 2033214
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    return-object v0

    .line 2033215
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2033216
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;->m:LX/DjM;

    if-eqz v0, :cond_0

    .line 2033217
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;->m:LX/DjM;

    .line 2033218
    iget-object v1, v0, LX/DjM;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2033219
    iget-object v1, v0, LX/DjM;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const/4 p0, 0x1

    .line 2033220
    iput-boolean p0, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->n:Z

    .line 2033221
    iget-object v1, v0, LX/DjM;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->onBackPressed()V

    .line 2033222
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x467367db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033223
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2033224
    invoke-direct {p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentNoteConfirmationDialogFragment;->l()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v1

    .line 2033225
    iput-object v1, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2033226
    const/16 v1, 0x2b

    const v2, 0x391ea668

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
