.class public Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Dka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DnP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Dih;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/DkO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DlS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/DkN;

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

.field public q:Landroid/support/v7/widget/RecyclerView;

.field public r:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2032955
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2032956
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2032957
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    new-instance v7, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DnT;->b(LX/0QB;)LX/DnT;

    move-result-object v4

    check-cast v4, LX/DnT;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {v7, v3, v4, v5, v6}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;-><init>(Landroid/content/Context;LX/DnT;LX/0Uh;LX/0SG;)V

    move-object v3, v7

    check-cast v3, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-static {v0}, LX/Dka;->b(LX/0QB;)LX/Dka;

    move-result-object v4

    check-cast v4, LX/Dka;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/DnP;->a(LX/0QB;)LX/DnP;

    move-result-object v8

    check-cast v8, LX/DnP;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v10

    check-cast v10, LX/Dih;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    const-class v12, LX/DkO;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/DkO;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v13

    check-cast v13, LX/0kL;

    invoke-static {v0}, LX/DlS;->b(LX/0QB;)LX/DlS;

    move-result-object p1

    check-cast p1, LX/DlS;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iput-object v4, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->b:LX/Dka;

    iput-object v5, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->c:Ljava/lang/String;

    iput-object v6, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->e:LX/0Uh;

    iput-object v8, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->f:LX/DnP;

    iput-object v9, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object v10, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->h:LX/Dih;

    iput-object v11, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->i:LX/1Ck;

    iput-object v12, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->j:LX/DkO;

    iput-object v13, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->k:LX/0kL;

    iput-object p1, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->l:LX/DlS;

    iput-object v0, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->m:LX/0SG;

    .line 2032958
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032959
    const-string v1, "arg_create_booking_appointment_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2032960
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2032961
    const-string v1, "referrer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    .line 2032962
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    new-instance v1, LX/Dj2;

    invoke-direct {v1, p0}, LX/Dj2;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032963
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->f:LX/Bm9;

    .line 2032964
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    new-instance v1, LX/Dj3;

    invoke-direct {v1, p0}, LX/Dj3;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032965
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->g:LX/BmO;

    .line 2032966
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    new-instance v1, LX/Dj4;

    invoke-direct {v1, p0}, LX/Dj4;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032967
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->h:LX/BmO;

    .line 2032968
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-eqz v0, :cond_0

    .line 2032969
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    new-instance v1, LX/Dj5;

    invoke-direct {v1, p0}, LX/Dj5;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032970
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->i:Landroid/view/View$OnClickListener;

    .line 2032971
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-nez v0, :cond_1

    .line 2032972
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    new-instance v1, LX/Dj6;

    invoke-direct {v1, p0}, LX/Dj6;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032973
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->j:Landroid/view/View$OnClickListener;

    .line 2032974
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-nez v0, :cond_2

    .line 2032975
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->j:LX/DkO;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    invoke-static {v1}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DkO;->a(LX/DkQ;)LX/DkN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    .line 2032976
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    new-instance v1, LX/DjA;

    invoke-direct {v1, p0}, LX/DjA;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    invoke-virtual {v0, v1}, LX/DkN;->a(LX/Dj9;)V

    .line 2032977
    :goto_0
    return-void

    .line 2032978
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->i:LX/1Ck;

    const-string v1, "fetch_page_services"

    new-instance v2, LX/Dj7;

    invoke-direct {v2, p0}, LX/Dj7;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    new-instance v3, LX/Dj8;

    invoke-direct {v3, p0}, LX/Dj8;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2032979
    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/16 v1, 0x3e8

    .line 2032980
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    if-ne p1, v1, :cond_1

    .line 2032981
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v0}, LX/Djn;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;

    move-result-object v0

    const/4 v1, 0x1

    .line 2032982
    iput-boolean v1, v0, LX/Djn;->n:Z

    .line 2032983
    move-object v0, v0

    .line 2032984
    invoke-virtual {v0}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2032985
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2032986
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2032987
    :cond_0
    :goto_0
    return-void

    .line 2032988
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2032989
    if-ne p1, v1, :cond_0

    .line 2032990
    const-string v0, "extra_selected_service_item"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    .line 2032991
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->l()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 p2, 0x0

    invoke-virtual {p1, v1, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2032992
    :goto_1
    iget-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {p1}, LX/Djn;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->k()Ljava/lang/String;

    move-result-object p2

    .line 2032993
    iput-object p2, p1, LX/Djn;->m:Ljava/lang/String;

    .line 2032994
    move-object p1, p1

    .line 2032995
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->m()Ljava/lang/String;

    move-result-object p2

    .line 2032996
    iput-object p2, p1, LX/Djn;->g:Ljava/lang/String;

    .line 2032997
    move-object p1, p1

    .line 2032998
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;->j()Ljava/lang/String;

    move-result-object p2

    .line 2032999
    iput-object p2, p1, LX/Djn;->k:Ljava/lang/String;

    .line 2033000
    move-object p1, p1

    .line 2033001
    iput-object v1, p1, LX/Djn;->l:Ljava/lang/String;

    .line 2033002
    move-object v1, p1

    .line 2033003
    invoke-virtual {v1}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2033004
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2033005
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2033006
    goto :goto_0

    .line 2033007
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5747e79a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033008
    const v1, 0x7f0303a9

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x76e52606

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2af2fc0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033009
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2033010
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    if-eqz v1, :cond_0

    .line 2033011
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    invoke-virtual {v1}, LX/DkN;->a()V

    .line 2033012
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->i:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2033013
    const/16 v1, 0x2b

    const v2, -0x49c2276f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xbdc9917

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033014
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2033015
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2033016
    if-eqz v1, :cond_0

    .line 2033017
    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v2, v2, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-eqz v2, :cond_1

    .line 2033018
    const v2, 0x7f082baa

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2033019
    :goto_0
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2033020
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x249b840c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2033021
    :cond_1
    const v2, 0x7f082b95

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033022
    const v0, 0x7f0d0b9a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->q:Landroid/support/v7/widget/RecyclerView;

    .line 2033023
    const v0, 0x7f0d0b9b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->r:Lcom/facebook/fig/button/FigButton;

    .line 2033024
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->r:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/DjE;

    invoke-direct {v1, p0}, LX/DjE;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2033025
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->q:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2033026
    return-void
.end method
