.class public Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DnV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Dka;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field public f:Lcom/facebook/resources/ui/FbButton;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2033342
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2033343
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-static {v4}, LX/DnV;->b(LX/0QB;)LX/DnV;

    move-result-object v1

    check-cast v1, LX/DnV;

    invoke-static {v4}, LX/Dka;->b(LX/0QB;)LX/Dka;

    move-result-object v2

    check-cast v2, LX/Dka;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const/16 p0, 0x19e

    invoke-static {v4, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    iput-object v1, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->a:LX/DnV;

    iput-object v2, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->b:LX/Dka;

    iput-object v3, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->c:LX/1Ck;

    iput-object v4, p1, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->d:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033344
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2033345
    const-class v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2033346
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010593

    const v2, 0x7f0e0a87

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->e:Landroid/content/Context;

    .line 2033347
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x78c47ba7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2033348
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->e:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2033349
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033350
    const-string v2, "arg_recipient"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2033351
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033352
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->g:Ljava/lang/String;

    .line 2033353
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033354
    const-string v2, "arg_request_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->h:Ljava/lang/String;

    .line 2033355
    const v1, 0x7f0311c3

    invoke-virtual {v0, v1, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 2033356
    const v0, 0x7f0d0ada

    invoke-static {v5, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2033357
    const v1, 0x7f0d0393

    invoke-static {v5, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2033358
    const v2, 0x7f0d1648

    invoke-static {v5, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 2033359
    iget-object v6, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->a:LX/DnV;

    invoke-virtual {v6}, LX/DnV;->a()LX/DnU;

    move-result-object v6

    .line 2033360
    sget-object p1, LX/DnU;->FBUI:LX/DnU;

    if-ne v6, p1, :cond_5

    .line 2033361
    const p1, 0x7f030667

    invoke-virtual {v1, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2033362
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbButton;

    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    .line 2033363
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033364
    const-string v6, "arg_rejection_type"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    sget v6, LX/Djc;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2033365
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033366
    iget-boolean v6, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v6

    .line 2033367
    if-eqz v1, :cond_2

    const v1, 0x7f082bd8

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p0, v1, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2033368
    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2033369
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 2033370
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    const v6, 0x7f082ba1

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033371
    const v1, 0x7f082bd3

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p0, v1, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2033372
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    const v1, 0x7f082ba0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->b(I)V

    .line 2033373
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/DjW;

    invoke-direct {v1, p0, v2}, LX/DjW;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2033374
    :cond_1
    :goto_2
    const v0, 0x7d27dd7c

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-object v5

    .line 2033375
    :cond_2
    const v1, 0x7f082bd6

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2033376
    :cond_3
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033377
    const-string v6, "arg_rejection_type"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    sget v6, LX/Djc;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2033378
    const v1, 0x7f082bd7

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2033379
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2033380
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 2033381
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    const v6, 0x7f082b98

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033382
    const v1, 0x7f082bd4

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p0, v1, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2033383
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    const v1, 0x7f082b97

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->b(I)V

    .line 2033384
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/DjY;

    invoke-direct {v1, p0, v2}, LX/DjY;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2033385
    :cond_4
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2033386
    const-string v6, "arg_rejection_type"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    sget v6, LX/Djc;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2033387
    const v1, 0x7f082bd9

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p0, v1, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2033388
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2033389
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 2033390
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    const v6, 0x7f082bcd

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033391
    const v1, 0x7f082bd4

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p0, v1, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2033392
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;

    const v1, 0x7f082b96

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->b(I)V

    .line 2033393
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Djb;

    invoke-direct {v1, p0, v2}, LX/Djb;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 2033394
    :cond_5
    sget-object p1, LX/DnU;->FIG:LX/DnU;

    if-ne v6, p1, :cond_0

    .line 2033395
    const p1, 0x7f030668

    invoke-virtual {v1, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2033396
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigButton;

    iput-object p1, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->f:Lcom/facebook/resources/ui/FbButton;

    goto/16 :goto_0
.end method
