.class public Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:I

.field private c:F

.field private d:Landroid/graphics/Typeface;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:F

.field private l:F

.field private m:F

.field private n:I

.field private o:Ljava/lang/CharSequence;

.field private p:Landroid/text/TextPaint;

.field private q:LX/Dpt;

.field private r:I

.field private s:LX/Dpt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047579
    const-class v0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    sput-object v0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2047580
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2047581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2047582
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2047583
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2047584
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2047585
    iput v5, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->r:I

    .line 2047586
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->MultilineEllipsizeTextView:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2047587
    const/16 v1, 0x0

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b:I

    .line 2047588
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    .line 2047589
    const/16 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->f:I

    .line 2047590
    const/16 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->g:I

    .line 2047591
    const/16 v1, 0x6

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->h:I

    .line 2047592
    const/16 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->i:I

    .line 2047593
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->j:Z

    .line 2047594
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->k:F

    .line 2047595
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->l:F

    .line 2047596
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->m:F

    .line 2047597
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->n:I

    .line 2047598
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 2047599
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 2047600
    invoke-direct {p0, v2, v1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(II)V

    .line 2047601
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2047602
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 2047603
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    .line 2047604
    :cond_0
    return-void
.end method

.method private static a(Landroid/text/Layout;)I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2047605
    move v0, v1

    move v2, v3

    .line 2047606
    :goto_0
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 2047607
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getParagraphDirection(I)I

    move-result v4

    .line 2047608
    if-nez v0, :cond_1

    move v2, v4

    .line 2047609
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2047610
    :cond_1
    if-eq v2, v4, :cond_0

    move v3, v1

    .line 2047611
    :cond_2
    if-eqz v3, :cond_3

    :goto_1
    return v2

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method private a(Landroid/text/Layout;I)I
    .locals 6

    .prologue
    .line 2047612
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 2047613
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 2047614
    const/4 v1, 0x0

    .line 2047615
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 2047616
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2047617
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2047618
    :cond_0
    float-to-double v0, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingLeft()I

    move-result v4

    int-to-double v4, v4

    add-double/2addr v0, v4

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingRight()I

    move-result v4

    int-to-double v4, v4

    add-double/2addr v0, v4

    double-to-int v0, v0

    .line 2047619
    sparse-switch v3, :sswitch_data_0

    move v0, v2

    .line 2047620
    :goto_1
    :sswitch_0
    return v0

    .line 2047621
    :sswitch_1
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(I)LX/Dpt;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2047622
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b()V

    .line 2047623
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    sub-int v0, p1, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2047624
    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->o:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget v4, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->g:I

    iget v5, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->f:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)LX/Dpu;

    move-result-object v0

    .line 2047625
    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    invoke-direct {p0, v0, v1, v3}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(LX/Dpu;Landroid/text/TextPaint;I)Landroid/text/StaticLayout;

    move-result-object v1

    .line 2047626
    new-instance v2, LX/Dpt;

    iget v0, v0, LX/Dpu;->b:I

    invoke-direct {v2, v1, v0}, LX/Dpt;-><init>(Landroid/text/Layout;I)V

    return-object v2
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)LX/Dpu;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x0

    .line 2047627
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-gez p3, :cond_1

    .line 2047628
    :cond_0
    new-instance v0, LX/Dpu;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/Dpu;-><init>(Ljava/util/List;I)V

    .line 2047629
    :goto_0
    return-object v0

    .line 2047630
    :cond_1
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->j:Z

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2047631
    invoke-static {v0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(Landroid/text/Layout;)I

    move-result v5

    .line 2047632
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    move v3, v8

    move v4, p4

    .line 2047633
    :goto_1
    if-lez v4, :cond_5

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 2047634
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v2

    .line 2047635
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 2047636
    add-int/lit8 v1, p4, -0x1

    if-lt v3, v1, :cond_2

    if-nez v5, :cond_4

    .line 2047637
    :cond_2
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v1

    .line 2047638
    add-int/lit8 v7, v1, -0x1

    invoke-interface {p1, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_3

    .line 2047639
    add-int/lit8 v1, v1, -0x1

    .line 2047640
    :cond_3
    invoke-interface {p1, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2047641
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2047642
    add-int/lit8 v1, v3, 0x1

    .line 2047643
    add-int/lit8 v2, v4, -0x1

    move v3, v1

    move v4, v2

    .line 2047644
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2047645
    :goto_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-ge v1, v7, :cond_3

    .line 2047646
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_3

    .line 2047647
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2047648
    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p5, :cond_6

    .line 2047649
    const-string v0, ""

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2047650
    :cond_6
    new-instance v0, LX/Dpu;

    invoke-direct {v0, v6, v5}, LX/Dpu;-><init>(Ljava/util/List;I)V

    goto :goto_0
.end method

.method private a(LX/Dpu;Landroid/text/TextPaint;I)Landroid/text/StaticLayout;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2047651
    if-nez p1, :cond_0

    .line 2047652
    const/4 v0, 0x0

    .line 2047653
    :goto_0
    return-object v0

    .line 2047654
    :cond_0
    const/4 v0, 0x1

    .line 2047655
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2047656
    iget-object v3, p1, LX/Dpu;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 2047657
    if-nez v3, :cond_1

    .line 2047658
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2047659
    :cond_1
    invoke-static {v0}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2047660
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move v3, v2

    .line 2047661
    goto :goto_1

    .line 2047662
    :cond_2
    iget v0, p1, LX/Dpu;->b:I

    if-nez v0, :cond_3

    move v5, p3

    .line 2047663
    :goto_2
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    iget-boolean v9, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->j:Z

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v4, p2

    move v11, p3

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    goto :goto_0

    .line 2047664
    :cond_3
    const/16 v5, 0x4000

    goto :goto_2
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2047665
    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->q:LX/Dpt;

    .line 2047666
    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->s:LX/Dpt;

    .line 2047667
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->r:I

    .line 2047668
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->requestLayout()V

    .line 2047669
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->invalidate()V

    .line 2047670
    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 2047671
    const/4 v0, 0x0

    .line 2047672
    packed-switch p1, :pswitch_data_0

    .line 2047673
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(Landroid/graphics/Typeface;I)V

    .line 2047674
    return-void

    .line 2047675
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 2047676
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 2047677
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 2047678
    invoke-static {p1, p2}, LX/A7i;->a(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->d:Landroid/graphics/Typeface;

    .line 2047679
    iput-object p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->d:Landroid/graphics/Typeface;

    .line 2047680
    iput p2, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->e:I

    .line 2047681
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    .line 2047682
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047683
    return-void
.end method

.method private b(Landroid/text/Layout;I)I
    .locals 4

    .prologue
    .line 2047684
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 2047685
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2047686
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    .line 2047687
    iget v3, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->i:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2047688
    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 2047689
    :goto_0
    :sswitch_0
    return v0

    .line 2047690
    :sswitch_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private b()V
    .locals 5

    .prologue
    .line 2047691
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 2047692
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    .line 2047693
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, v0, Landroid/text/TextPaint;->density:F

    .line 2047694
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2047695
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2047696
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->d:Landroid/graphics/Typeface;

    iget v2, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->e:I

    invoke-static {v0, v1, v2}, LX/A7i;->a(Landroid/text/TextPaint;Landroid/graphics/Typeface;I)V

    .line 2047697
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->d:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2047698
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->m:F

    iget v2, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->k:F

    iget v3, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->l:F

    iget v4, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->n:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 2047699
    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2047576
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->q:LX/Dpt;

    if-nez v0, :cond_0

    .line 2047577
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(I)LX/Dpt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->q:LX/Dpt;

    .line 2047578
    :cond_0
    return-void
.end method


# virtual methods
.method public getMaxLines()I
    .locals 1

    .prologue
    .line 2047700
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->g:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 2047517
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->h:I

    return v0
.end method

.method public getMinLines()I
    .locals 1

    .prologue
    .line 2047518
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->f:I

    return v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2047519
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 2047520
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b:I

    return v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 2047521
    iget v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    return v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 2047522
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->d:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2047523
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2047524
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b()V

    .line 2047525
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c()V

    .line 2047526
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2047527
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getHeight()I

    move-result v0

    .line 2047528
    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->q:LX/Dpt;

    iget-object v1, v1, LX/Dpt;->a:Landroid/text/Layout;

    .line 2047529
    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v2

    .line 2047530
    const/4 v3, 0x0

    sub-int/2addr v0, v2

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2047531
    div-int/lit8 v0, v0, 0x2

    .line 2047532
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2047533
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->q:LX/Dpt;

    iget v0, v0, LX/Dpt;->b:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 2047534
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 2047535
    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v2

    sub-int v0, v2, v0

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2047536
    :cond_0
    invoke-virtual {v1, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2047537
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2047538
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2047539
    const-string v0, "MultilineEllipsizeTextView.onMeasure"

    const v1, -0x9ee1a35

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2047540
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b()V

    .line 2047541
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getDefaultSize(II)I

    move-result v0

    .line 2047542
    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2047543
    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->r:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->s:LX/Dpt;

    if-nez v1, :cond_1

    .line 2047544
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(I)LX/Dpt;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->s:LX/Dpt;

    .line 2047545
    iput v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->r:I

    .line 2047546
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->s:LX/Dpt;

    iget-object v0, v0, LX/Dpt;->a:Landroid/text/Layout;

    invoke-direct {p0, v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a(Landroid/text/Layout;I)I

    move-result v0

    .line 2047547
    iget-object v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->s:LX/Dpt;

    iget-object v1, v1, LX/Dpt;->a:Landroid/text/Layout;

    invoke-direct {p0, v1, p2}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b(Landroid/text/Layout;I)I

    move-result v1

    .line 2047548
    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2047549
    const v0, 0xbad1f77

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2047550
    return-void

    .line 2047551
    :catchall_0
    move-exception v0

    const v1, -0x66601fc0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x397f570f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2047552
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2047553
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047554
    const/16 v1, 0x2d

    const v2, 0x4bf4eaff    # 3.2101886E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .prologue
    .line 2047555
    iput p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->g:I

    .line 2047556
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047557
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0

    .prologue
    .line 2047558
    iput p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->h:I

    .line 2047559
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047560
    return-void
.end method

.method public setMinLines(I)V
    .locals 0

    .prologue
    .line 2047561
    iput p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->f:I

    .line 2047562
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047563
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2047564
    iput-object p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->o:Ljava/lang/CharSequence;

    .line 2047565
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2047566
    invoke-direct {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->a()V

    .line 2047567
    return-void
.end method

.method public setTextColor(I)V
    .locals 2

    .prologue
    .line 2047568
    iput p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b:I

    .line 2047569
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    if-eqz v0, :cond_0

    .line 2047570
    iget-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    iget v1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->b:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2047571
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->invalidate()V

    .line 2047572
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 2047573
    iput p1, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->c:F

    .line 2047574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->p:Landroid/text/TextPaint;

    .line 2047575
    return-void
.end method
