.class public final Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3c3a41ab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2022297
    const-class v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2022298
    const-class v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2022295
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2022296
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2022292
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2022293
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2022294
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private m()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2022290
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    .line 2022291
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2022288
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->i:Ljava/util/List;

    .line 2022289
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2022286
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k:Ljava/util/List;

    .line 2022287
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2022299
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2022300
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2022301
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->m()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2022302
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2022303
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->n()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->c(Ljava/util/List;)I

    move-result v3

    .line 2022304
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2022305
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->o()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2022306
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2022307
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2022308
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2022309
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2022310
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2022311
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2022312
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2022313
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2022314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2022315
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2022263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2022264
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->m()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2022265
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->m()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    .line 2022266
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->m()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2022267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    .line 2022268
    iput-object v0, v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    .line 2022269
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2022270
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    .line 2022271
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2022272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    .line 2022273
    iput-object v0, v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->g:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    .line 2022274
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2022275
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    .line 2022276
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2022277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    .line 2022278
    iput-object v0, v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->j:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    .line 2022279
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2022280
    invoke-direct {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2022281
    if-eqz v2, :cond_3

    .line 2022282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    .line 2022283
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 2022284
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2022285
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2022261
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->g:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->g:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    .line 2022262
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->g:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$GamesModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2022258
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2022259
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->h:Z

    .line 2022260
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2022255
    new-instance v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;-><init>()V

    .line 2022256
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2022257
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2022254
    const v0, 0x6cb99380

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2022253
    const v0, 0x1eb73744

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2022249
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2022250
    iget-boolean v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->h:Z

    return v0
.end method

.method public final k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2022251
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->j:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->j:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    .line 2022252
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGamesListQueryModel$ItemsModel$NodesModel;->j:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$TitleModel;

    return-object v0
.end method
