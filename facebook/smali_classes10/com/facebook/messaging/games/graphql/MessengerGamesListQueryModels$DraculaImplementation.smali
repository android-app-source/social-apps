.class public final Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2021409
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2021410
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2021411
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2021412
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 2021413
    if-nez p1, :cond_0

    move v0, v6

    .line 2021414
    :goto_0
    return v0

    .line 2021415
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2021416
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2021417
    :sswitch_0
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2021418
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2021419
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2021420
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2021421
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2021422
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2021423
    invoke-virtual {p3, v11}, LX/186;->c(I)V

    .line 2021424
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2021425
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 2021426
    invoke-virtual {p3, v10, v3}, LX/186;->b(II)V

    .line 2021427
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2021428
    :sswitch_1
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 2021429
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2021430
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 2021431
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2021432
    invoke-virtual {p0, p1, v11, v6}, LX/15i;->a(III)I

    move-result v8

    .line 2021433
    const/4 v9, 0x4

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2021434
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    move-object v0, p3

    .line 2021435
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2021436
    invoke-virtual {p3, v10, v7}, LX/186;->b(II)V

    .line 2021437
    invoke-virtual {p3, v11, v8, v6}, LX/186;->a(III)V

    .line 2021438
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3a3b34d2 -> :sswitch_0
        0x5e09879e -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2021444
    new-instance v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2021439
    sparse-switch p0, :sswitch_data_0

    .line 2021440
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2021441
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3a3b34d2 -> :sswitch_0
        0x5e09879e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2021398
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2021442
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->b(I)V

    .line 2021443
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2021403
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2021404
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2021405
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2021406
    iput p2, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->b:I

    .line 2021407
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2021408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2021402
    new-instance v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2021399
    iget v0, p0, LX/1vt;->c:I

    .line 2021400
    move v0, v0

    .line 2021401
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2021395
    iget v0, p0, LX/1vt;->c:I

    .line 2021396
    move v0, v0

    .line 2021397
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2021392
    iget v0, p0, LX/1vt;->b:I

    .line 2021393
    move v0, v0

    .line 2021394
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2021389
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2021390
    move-object v0, v0

    .line 2021391
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2021380
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2021381
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2021382
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2021383
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2021384
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2021385
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2021386
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2021387
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2021388
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2021377
    iget v0, p0, LX/1vt;->c:I

    .line 2021378
    move v0, v0

    .line 2021379
    return v0
.end method
