.class public final Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x27a8fa08
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2021538
    const-class v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2021537
    const-class v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2021535
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2021536
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2021505
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2021506
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2021507
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2021508
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2021509
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2021510
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2021511
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2021512
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2021513
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2021514
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2021527
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2021528
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2021529
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    .line 2021530
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2021531
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    .line 2021532
    iput-object v0, v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    .line 2021533
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2021534
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2021526
    invoke-virtual {p0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2021523
    new-instance v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;-><init>()V

    .line 2021524
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2021525
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2021522
    const v0, -0x12b27897

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2021521
    const v0, -0x3ff252d0

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2021519
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->e:Ljava/lang/String;

    .line 2021520
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2021517
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    .line 2021518
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->f:Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel$InstantGameInfoModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2021515
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->g:Ljava/lang/String;

    .line 2021516
    iget-object v0, p0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GameListApplicationFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method
