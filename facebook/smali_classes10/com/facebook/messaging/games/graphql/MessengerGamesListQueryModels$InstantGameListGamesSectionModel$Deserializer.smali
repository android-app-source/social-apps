.class public final Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2021736
    const-class v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel;

    new-instance v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2021737
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2021738
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2021739
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2021740
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2021741
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 2021742
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2021743
    :goto_0
    move v1, v2

    .line 2021744
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2021745
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2021746
    new-instance v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel;

    invoke-direct {v1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListGamesSectionModel;-><init>()V

    .line 2021747
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2021748
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2021749
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2021750
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2021751
    :cond_0
    return-object v1

    .line 2021752
    :cond_1
    const-string p0, "is_badged"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2021753
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 2021754
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 2021755
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2021756
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2021757
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 2021758
    const-string p0, "games"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2021759
    invoke-static {p1, v0}, LX/DeW;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2021760
    :cond_3
    const-string p0, "title"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2021761
    invoke-static {p1, v0}, LX/DeX;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2021762
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2021763
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2021764
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2021765
    if-eqz v1, :cond_6

    .line 2021766
    invoke-virtual {v0, v3, v5}, LX/186;->a(IZ)V

    .line 2021767
    :cond_6
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2021768
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
