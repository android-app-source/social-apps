.class public final Lcom/facebook/messaging/audio/record/AudioRecorder$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Exception;

.field public final synthetic b:LX/2S5;


# direct methods
.method public constructor <init>(LX/2S5;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 2019089
    iput-object p1, p0, Lcom/facebook/messaging/audio/record/AudioRecorder$1;->b:LX/2S5;

    iput-object p2, p0, Lcom/facebook/messaging/audio/record/AudioRecorder$1;->a:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2019090
    iget-object v0, p0, Lcom/facebook/messaging/audio/record/AudioRecorder$1;->b:LX/2S5;

    iget-object v0, v0, LX/2S5;->e:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "audio_clips_recording_failed"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "error_message"

    iget-object v3, p0, Lcom/facebook/messaging/audio/record/AudioRecorder$1;->a:Ljava/lang/Exception;

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "audio_clips"

    .line 2019091
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2019092
    move-object v1, v1

    .line 2019093
    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2019094
    iget-object v0, p0, Lcom/facebook/messaging/audio/record/AudioRecorder$1;->b:LX/2S5;

    const/4 v1, 0x0

    .line 2019095
    iput-boolean v1, v0, LX/2S5;->k:Z

    .line 2019096
    return-void
.end method
