.class public Lcom/facebook/messaging/montage/model/Montage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/Montage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Lcom/facebook/user/model/UserKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageMessageInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:I

.field public final g:Z

.field private final h:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030845
    new-instance v0, LX/DhI;

    invoke-direct {v0}, LX/DhI;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/Montage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    .line 2030843
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    const-class v0, Lcom/facebook/messaging/montage/model/MontageMessageInfo;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/montage/model/Montage;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Ljava/lang/String;ILX/0Px;IZ)V

    .line 2030844
    return-void
.end method

.method private constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Ljava/lang/String;ILX/0Px;IZ)V
    .locals 5
    .param p2    # Lcom/facebook/user/model/UserKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageMessageInfo;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2030822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030823
    if-ltz p4, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2030824
    invoke-static {p5}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2030825
    if-ne p6, v3, :cond_0

    move p6, v2

    .line 2030826
    :cond_0
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v0

    invoke-static {p6, v0}, LX/0PB;->checkElementIndex(II)I

    .line 2030827
    :goto_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2030828
    iput-object p2, p0, Lcom/facebook/messaging/montage/model/Montage;->b:Lcom/facebook/user/model/UserKey;

    .line 2030829
    iput-object p3, p0, Lcom/facebook/messaging/montage/model/Montage;->c:Ljava/lang/String;

    .line 2030830
    iput p4, p0, Lcom/facebook/messaging/montage/model/Montage;->d:I

    .line 2030831
    iput-object p5, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    .line 2030832
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 2030833
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2030834
    iget-object v3, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/MontageMessageInfo;

    .line 2030835
    iget-object v0, v0, Lcom/facebook/messaging/montage/model/MontageMessageInfo;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2030836
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 2030837
    goto :goto_0

    .line 2030838
    :cond_2
    if-ne p6, v3, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_3

    .line 2030839
    :cond_4
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->h:LX/0Rf;

    .line 2030840
    iput p6, p0, Lcom/facebook/messaging/montage/model/Montage;->f:I

    .line 2030841
    iput-boolean p7, p0, Lcom/facebook/messaging/montage/model/Montage;->g:Z

    .line 2030842
    return-void
.end method

.method public static newBuilder()LX/DhJ;
    .locals 1

    .prologue
    .line 2030796
    new-instance v0, LX/DhJ;

    invoke-direct {v0}, LX/DhJ;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2030821
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/model/Montage;->hashCode()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030807
    if-ne p0, p1, :cond_1

    .line 2030808
    :cond_0
    :goto_0
    return v0

    .line 2030809
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2030810
    goto :goto_0

    .line 2030811
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/Montage;

    .line 2030812
    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/Montage;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->b:Lcom/facebook/user/model/UserKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/Montage;->b:Lcom/facebook/user/model/UserKey;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/Montage;->c:Ljava/lang/String;

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/facebook/messaging/montage/model/Montage;->d:I

    iget v3, p0, Lcom/facebook/messaging/montage/model/Montage;->d:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2030813
    if-ne v2, v3, :cond_6

    move v6, v7

    .line 2030814
    :cond_4
    :goto_1
    move v2, v6

    .line 2030815
    if-eqz v2, :cond_5

    iget v2, p0, Lcom/facebook/messaging/montage/model/Montage;->f:I

    iget v3, p1, Lcom/facebook/messaging/montage/model/Montage;->f:I

    if-ne v2, v3, :cond_5

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/Montage;->g:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/montage/model/Montage;->g:Z

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 2030816
    :cond_6
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_4

    move v5, v6

    .line 2030817
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_7

    .line 2030818
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/montage/model/MontageMessageInfo;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/facebook/messaging/montage/model/MontageMessageInfo;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2030819
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_7
    move v6, v7

    .line 2030820
    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030806
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->b:Lcom/facebook/user/model/UserKey;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/messaging/montage/model/Montage;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/messaging/montage/model/Montage;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/Montage;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2030797
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030798
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030799
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2030800
    iget v0, p0, Lcom/facebook/messaging/montage/model/Montage;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2030801
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/Montage;->e:LX/0Px;

    .line 2030802
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2030803
    iget v0, p0, Lcom/facebook/messaging/montage/model/Montage;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2030804
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/model/Montage;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2030805
    return-void
.end method
