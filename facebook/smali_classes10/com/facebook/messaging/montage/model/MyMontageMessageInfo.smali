.class public Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;
.super Lcom/facebook/messaging/montage/model/MontageMessageInfo;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030952
    new-instance v0, LX/DhR;

    invoke-direct {v0}, LX/DhR;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2030953
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/MontageMessageInfo;-><init>(Landroid/os/Parcel;)V

    .line 2030954
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    .line 2030955
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2030956
    invoke-super {p0, p1}, Lcom/facebook/messaging/montage/model/MontageMessageInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;

    iget-object v0, v0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eq v1, v0, :cond_1

    .line 2030957
    :cond_0
    :goto_0
    return v2

    .line 2030958
    :cond_1
    check-cast p1, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;

    move v1, v2

    .line 2030959
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2030960
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p1, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2030961
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2030962
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2030963
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v0

    invoke-super {p0}, Lcom/facebook/messaging/montage/model/MontageMessageInfo;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1l6;->a(I)LX/1l6;

    move-result-object v2

    .line 2030964
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2030965
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 2030966
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2030967
    :cond_0
    invoke-virtual {v2}, LX/1l6;->hashCode()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2030968
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/MontageMessageInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2030969
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MyMontageMessageInfo;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2030970
    return-void
.end method
