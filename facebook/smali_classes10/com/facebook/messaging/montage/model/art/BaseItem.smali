.class public abstract Lcom/facebook/messaging/montage/model/art/BaseItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2031112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2031106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    .line 2031108
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    .line 2031109
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    .line 2031110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    .line 2031111
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 2031100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031101
    iput-object p1, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    .line 2031102
    iput-object p2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    .line 2031103
    iput-wide p3, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    .line 2031104
    iput-object p5, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    .line 2031105
    return-void
.end method


# virtual methods
.method public abstract a()J
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2031094
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2031095
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031096
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2031097
    iget-wide v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2031098
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031099
    return-void
.end method
