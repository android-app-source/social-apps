.class public Lcom/facebook/messaging/montage/model/art/ImageAsset;
.super Lcom/facebook/messaging/montage/model/art/ArtAsset;
.source ""


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:F
    .annotation build Landroid/support/annotation/FloatRange;
    .end annotation
.end field

.field private h:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031231
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(Landroid/os/Parcel;)V

    .line 2031232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    .line 2031233
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    .line 2031234
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)V
    .locals 7

    .prologue
    .line 2031216
    invoke-virtual {p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->q()D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v2, v0

    invoke-virtual {p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v4, 0x0

    :goto_0
    invoke-static {p1}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    move-result-object v5

    invoke-static {p1}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(DLjava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;)V

    .line 2031217
    invoke-virtual {p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    .line 2031218
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2031219
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1H1;->h(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->c:Ljava/lang/String;

    .line 2031220
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->m()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    .line 2031221
    return-void

    .line 2031222
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;->j()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/DhV;
    .locals 1

    .prologue
    .line 2031230
    sget-object v0, LX/DhV;->IMAGE:LX/DhV;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031235
    if-ne p0, p1, :cond_1

    .line 2031236
    :cond_0
    :goto_0
    return v0

    .line 2031237
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031238
    goto :goto_0

    .line 2031239
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/ImageAsset;

    .line 2031240
    invoke-super {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031227
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->h:I

    if-nez v0, :cond_0

    .line 2031228
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->h:I

    .line 2031229
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->h:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031223
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2031224
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031225
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031226
    return-void
.end method
