.class public Lcom/facebook/messaging/montage/model/art/TextAsset;
.super Lcom/facebook/messaging/montage/model/art/ArtAsset;
.source ""


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final h:F
    .annotation build Landroid/support/annotation/FloatRange;
    .end annotation
.end field

.field public final i:LX/Dhh;

.field public final j:Lcom/facebook/messaging/font/FontAsset;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/Dhi;

.field private l:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031308
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(Landroid/os/Parcel;)V

    .line 2031309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    .line 2031310
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    .line 2031311
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    .line 2031312
    const-class v0, LX/Dhh;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhh;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    .line 2031313
    const-class v0, Lcom/facebook/messaging/font/FontAsset;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/font/FontAsset;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    .line 2031314
    const-class v0, LX/Dhi;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhi;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    .line 2031315
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;)V
    .locals 18

    .prologue
    .line 2031291
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->t()D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    mul-double v16, v2, v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->k()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->j()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->a()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->a()D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->j()D

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    move-result-object v12

    invoke-static {v12}, LX/DhY;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;)LX/DhY;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    move-result-object v13

    invoke-static {v13}, LX/DhZ;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;)LX/DhZ;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;-><init>(DDDDLX/DhY;LX/DhZ;)V

    new-instance v5, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->j()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->a()D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->a()D

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->j()D

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    move-result-object v4

    invoke-static {v4}, LX/DhY;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;)LX/DhY;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    move-result-object v4

    invoke-static {v4}, LX/DhZ;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;)LX/DhZ;

    move-result-object v15

    invoke-direct/range {v5 .. v15}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;-><init>(DDDDLX/DhY;LX/DhZ;)V

    move-object/from16 v7, p0

    move-wide/from16 v8, v16

    move-object v10, v2

    move-object v11, v3

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(DLjava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;)V

    .line 2031292
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    .line 2031293
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    .line 2031294
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->p()D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    .line 2031295
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Dhh;->from(Ljava/lang/String;)LX/Dhh;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    .line 2031296
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 2031297
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v4, v2, LX/1vs;->b:I

    .line 2031298
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->l()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v6, v2, LX/1vs;->b:I

    .line 2031299
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->l()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v8, v2, LX/1vs;->b:I

    .line 2031300
    new-instance v2, Lcom/facebook/messaging/font/FontAsset;

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v5, v6, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v7, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/messaging/font/FontAsset;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    .line 2031301
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtTextAssetModel;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Dhi;->from(Ljava/lang/String;)LX/Dhi;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    .line 2031302
    return-void

    .line 2031303
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/DhV;
    .locals 1

    .prologue
    .line 2031307
    sget-object v0, LX/DhV;->TEXT:LX/DhV;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031316
    if-ne p0, p1, :cond_1

    .line 2031317
    :cond_0
    :goto_0
    return v0

    .line 2031318
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031319
    goto :goto_0

    .line 2031320
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/TextAsset;

    .line 2031321
    invoke-super {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031304
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->l:I

    if-nez v0, :cond_0

    .line 2031305
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->l:I

    .line 2031306
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->l:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031283
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2031284
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031285
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2031286
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031287
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031288
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031289
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031290
    return-void
.end method
