.class public Lcom/facebook/messaging/montage/model/art/StickerAsset;
.super Lcom/facebook/messaging/montage/model/art/ArtAsset;
.source ""


# instance fields
.field public final f:Lcom/facebook/stickers/model/Sticker;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031241
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(Landroid/os/Parcel;)V

    .line 2031242
    const-class v0, Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    .line 2031243
    return-void
.end method

.method public constructor <init>(Lcom/facebook/stickers/model/Sticker;Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)V
    .locals 7

    .prologue
    .line 2031244
    invoke-virtual {p2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->q()D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v2, v0

    invoke-virtual {p2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-static {p2}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    move-result-object v5

    invoke-static {p2}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/montage/model/art/ArtAsset;-><init>(DLjava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;)V

    .line 2031245
    iput-object p1, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    .line 2031246
    return-void

    .line 2031247
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;->j()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/DhV;
    .locals 1

    .prologue
    .line 2031248
    sget-object v0, LX/DhV;->STICKER:LX/DhV;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031249
    if-ne p0, p1, :cond_1

    .line 2031250
    :cond_0
    :goto_0
    return v0

    .line 2031251
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031252
    goto :goto_0

    .line 2031253
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/StickerAsset;

    .line 2031254
    invoke-super {p0, p1}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031255
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->g:I

    if-nez v0, :cond_0

    .line 2031256
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->g:I

    .line 2031257
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->g:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031258
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2031259
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2031260
    return-void
.end method
