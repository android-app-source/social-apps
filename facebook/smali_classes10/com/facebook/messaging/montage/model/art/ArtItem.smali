.class public Lcom/facebook/messaging/montage/model/art/ArtItem;
.super Lcom/facebook/messaging/montage/model/art/BaseItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public e:Lcom/facebook/stickers/model/Sticker;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031113
    new-instance v0, LX/Dha;

    invoke-direct {v0}, LX/Dha;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/ArtItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031114
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/art/BaseItem;-><init>(Landroid/os/Parcel;)V

    .line 2031115
    const-class v0, Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    .line 2031116
    sget-object v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    .line 2031117
    sget-object v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    .line 2031118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;JLjava/lang/String;)V
    .locals 7
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;",
            "Landroid/net/Uri;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2031119
    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p5

    move-object v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/montage/model/art/BaseItem;-><init>(Ljava/lang/String;Landroid/net/Uri;JLjava/lang/String;)V

    .line 2031120
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    .line 2031121
    sget-object v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->a:Ljava/util/Comparator;

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1sm;->d(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    .line 2031122
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2031123
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    if-eqz v0, :cond_0

    .line 2031124
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2031125
    :goto_0
    return-wide v0

    .line 2031126
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    if-eqz v0, :cond_1

    .line 2031127
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 2031128
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031129
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031130
    if-ne p0, p1, :cond_1

    .line 2031131
    :cond_0
    :goto_0
    return v0

    .line 2031132
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031133
    goto :goto_0

    .line 2031134
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2031135
    iget-wide v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2031136
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->h:I

    if-nez v0, :cond_0

    .line 2031137
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->h:I

    .line 2031138
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->h:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031139
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/art/BaseItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2031140
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->e:Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2031141
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->f:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0Px;)V

    .line 2031142
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtItem;->g:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0Px;)V

    .line 2031143
    return-void
.end method
