.class public abstract Lcom/facebook/messaging/montage/model/art/ArtAsset;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:F

.field public c:Ljava/lang/String;

.field public final d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

.field public final e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031024
    new-instance v0, LX/DhS;

    invoke-direct {v0}, LX/DhS;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->a:Ljava/util/Comparator;

    .line 2031025
    new-instance v0, LX/DhT;

    invoke-direct {v0}, LX/DhT;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(DLjava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;)V
    .locals 1

    .prologue
    .line 2031018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031019
    double-to-float v0, p1

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    .line 2031020
    iput-object p3, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    .line 2031021
    iput-object p4, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    .line 2031022
    iput-object p5, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    .line 2031023
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2030996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030997
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    .line 2030998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    .line 2030999
    const-class v0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    .line 2031000
    const-class v0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    .line 2031001
    return-void
.end method


# virtual methods
.method public abstract a()LX/DhV;
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031017
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->a()LX/DhV;

    move-result-object v0

    invoke-virtual {v0}, LX/DhV;->ordinal()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031011
    if-ne p0, p1, :cond_1

    .line 2031012
    :cond_0
    :goto_0
    return v0

    .line 2031013
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031014
    goto :goto_0

    .line 2031015
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/ArtAsset;

    .line 2031016
    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2031008
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->f:I

    if-nez v0, :cond_0

    .line 2031009
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->f:I

    .line 2031010
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->f:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031002
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/model/art/ArtAsset;->a()LX/DhV;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031003
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031004
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031005
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031006
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031007
    return-void
.end method
