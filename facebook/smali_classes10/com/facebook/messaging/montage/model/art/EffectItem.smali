.class public Lcom/facebook/messaging/montage/model/art/EffectItem;
.super Lcom/facebook/messaging/montage/model/art/BaseItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/EffectItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/Dhg;

.field public final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/art/EffectAsset;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031177
    new-instance v0, LX/Dhe;

    invoke-direct {v0}, LX/Dhe;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/EffectItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    .line 2031178
    invoke-direct {p0, p1}, Lcom/facebook/messaging/montage/model/art/BaseItem;-><init>(Landroid/os/Parcel;)V

    .line 2031179
    const-class v0, LX/Dhg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhg;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->e:LX/Dhg;

    .line 2031180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    .line 2031181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->g:Ljava/lang/String;

    .line 2031182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    .line 2031183
    sget-object v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    .line 2031184
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->e:LX/Dhg;

    sget-object v1, LX/Dhg;->MASK:LX/Dhg;

    if-eq v0, v1, :cond_0

    .line 2031185
    const/4 v0, 0x0

    .line 2031186
    :goto_0
    move-object v0, v0

    .line 2031187
    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->j:LX/0Px;

    .line 2031188
    const-class v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    .line 2031189
    return-void

    .line 2031190
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2031191
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    if-eqz v0, :cond_1

    .line 2031192
    iget-object v3, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    const-string v1, ".msqrd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v4, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    :goto_2
    invoke-static {v3, v0, v4, v5, v1}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)LX/AMT;

    move-result-object v0

    move-object v0, v0

    .line 2031193
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2031194
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;

    .line 2031195
    iget-object v4, v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    invoke-static {v4, v5}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;)LX/AMT;

    move-result-object v4

    move-object v0, v4

    .line 2031196
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2031197
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2031198
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    const-string v1, ".msqrd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    goto :goto_2
.end method

.method public static newBuilder()LX/Dhf;
    .locals 1

    .prologue
    .line 2031199
    new-instance v0, LX/Dhf;

    invoke-direct {v0}, LX/Dhf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2031200
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2031201
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031202
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031203
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2031204
    :cond_1
    :goto_0
    return v0

    .line 2031205
    :cond_2
    if-eq p1, p0, :cond_1

    .line 2031206
    check-cast p1, Lcom/facebook/messaging/montage/model/art/EffectItem;

    .line 2031207
    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031208
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/BaseItem;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2031209
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/montage/model/art/BaseItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2031210
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031211
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031212
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031213
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->i:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0Px;)V

    .line 2031214
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->k:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031215
    return-void
.end method
