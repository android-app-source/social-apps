.class public Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:LX/DhY;

.field public final f:LX/DhZ;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031089
    new-instance v0, LX/DhW;

    invoke-direct {v0}, LX/DhW;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(DDDDLX/DhY;LX/DhZ;)V
    .locals 1

    .prologue
    .line 2031081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031082
    double-to-float v0, p1

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    .line 2031083
    double-to-float v0, p3

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    .line 2031084
    double-to-float v0, p5

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    .line 2031085
    double-to-float v0, p7

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    .line 2031086
    iput-object p9, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    .line 2031087
    iput-object p10, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    .line 2031088
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031074
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    .line 2031075
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    .line 2031076
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    .line 2031077
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    .line 2031078
    const-class v0, LX/DhY;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhY;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    .line 2031079
    const-class v0, LX/DhZ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhZ;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    .line 2031080
    return-void
.end method

.method public static a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;
    .locals 12

    .prologue
    .line 2031072
    new-instance v1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->j()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->a()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->a()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->j()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-static {v0}, LX/DhY;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;)LX/DhY;

    move-result-object v10

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    move-result-object v0

    invoke-static {v0}, LX/DhZ;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;)LX/DhZ;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;-><init>(DDDDLX/DhY;LX/DhZ;)V

    return-object v1
.end method

.method public static b(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;)Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;
    .locals 12

    .prologue
    .line 2031090
    new-instance v1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->j()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;->a()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->a()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;->j()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-static {v0}, LX/DhY;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;)LX/DhY;

    move-result-object v10

    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    move-result-object v0

    invoke-static {v0}, LX/DhZ;->from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;)LX/DhZ;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;-><init>(DDDDLX/DhY;LX/DhZ;)V

    return-object v1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031071
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031065
    if-ne p0, p1, :cond_1

    .line 2031066
    :cond_0
    :goto_0
    return v0

    .line 2031067
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2031068
    goto :goto_0

    .line 2031069
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    .line 2031070
    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    iget v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031062
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->g:I

    if-nez v0, :cond_0

    .line 2031063
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->g:I

    .line 2031064
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->g:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031055
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031056
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031057
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031058
    iget v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2031059
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031060
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2031061
    return-void
.end method
