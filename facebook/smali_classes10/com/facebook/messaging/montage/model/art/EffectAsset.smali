.class public Lcom/facebook/messaging/montage/model/art/EffectAsset;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/art/EffectAsset;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2031147
    new-instance v0, LX/Dhd;

    invoke-direct {v0}, LX/Dhd;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2031148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    .line 2031150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    .line 2031151
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2031152
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2031153
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/facebook/messaging/montage/model/art/EffectAsset;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2031154
    :cond_1
    :goto_0
    return v0

    .line 2031155
    :cond_2
    if-eq p1, p0, :cond_1

    .line 2031156
    check-cast p1, Lcom/facebook/messaging/montage/model/art/EffectAsset;

    .line 2031157
    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    .line 2031158
    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2031159
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    .line 2031160
    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2031161
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2031162
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2031163
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031164
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectAsset;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2031165
    return-void
.end method
