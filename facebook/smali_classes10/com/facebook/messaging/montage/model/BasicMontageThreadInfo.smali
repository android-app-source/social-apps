.class public Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final c:Lcom/facebook/messaging/model/messages/Message;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030788
    new-instance v0, LX/DhH;

    invoke-direct {v0}, LX/DhH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 2030768
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    const-class v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const-class v2, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;-><init>(Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Z)V

    .line 2030769
    return-void
.end method

.method private constructor <init>(Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Z)V
    .locals 1

    .prologue
    .line 2030782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030783
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->a:Lcom/facebook/user/model/UserKey;

    .line 2030784
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2030785
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->c:Lcom/facebook/messaging/model/messages/Message;

    .line 2030786
    iput-boolean p4, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->d:Z

    .line 2030787
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2030789
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->hashCode()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030776
    if-ne p0, p1, :cond_1

    .line 2030777
    :cond_0
    :goto_0
    return v0

    .line 2030778
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2030779
    goto :goto_0

    .line 2030780
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;

    .line 2030781
    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->d:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->d:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->a:Lcom/facebook/user/model/UserKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->a:Lcom/facebook/user/model/UserKey;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->c:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->c:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030775
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->a:Lcom/facebook/user/model/UserKey;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->c:Lcom/facebook/messaging/model/messages/Message;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2030770
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030771
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030772
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->c:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030773
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/model/BasicMontageThreadInfo;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2030774
    return-void
.end method
