.class public Lcom/facebook/messaging/montage/model/MontageNuxMessage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageNuxMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/messaging/montage/model/art/ArtItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030920
    new-instance v0, LX/DhO;

    invoke-direct {v0}, LX/DhO;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2030921
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v0, Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-direct {p0, v1, v0}, Lcom/facebook/messaging/montage/model/MontageNuxMessage;-><init>(Ljava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtItem;)V

    .line 2030922
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/montage/model/art/ArtItem;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2030923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030924
    iput-object p1, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->a:Ljava/lang/String;

    .line 2030925
    iput-object p2, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->b:Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2030926
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2030927
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030928
    if-ne p0, p1, :cond_1

    .line 2030929
    :cond_0
    :goto_0
    return v0

    .line 2030930
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2030931
    goto :goto_0

    .line 2030932
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/MontageNuxMessage;

    .line 2030933
    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->b:Lcom/facebook/messaging/montage/model/art/ArtItem;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->b:Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030934
    iget v0, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->c:I

    if-nez v0, :cond_0

    .line 2030935
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->b:Lcom/facebook/messaging/montage/model/art/ArtItem;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->c:I

    .line 2030936
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->c:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2030937
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2030938
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;->b:Lcom/facebook/messaging/montage/model/art/ArtItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030939
    return-void
.end method
