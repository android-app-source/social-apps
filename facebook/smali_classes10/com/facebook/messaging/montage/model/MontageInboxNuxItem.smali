.class public Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;
.super Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageNuxMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030874
    new-instance v0, LX/DhK;

    invoke-direct {v0}, LX/DhK;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2030881
    invoke-direct {p0, p1}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Landroid/os/Parcel;)V

    .line 2030882
    const-class v0, Lcom/facebook/messaging/montage/model/MontageNuxMessage;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    .line 2030883
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    .line 2030884
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/0Px;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/montage/model/MontageNuxMessage;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2030875
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)V

    .line 2030876
    if-nez p3, :cond_0

    .line 2030877
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p3, p1

    .line 2030878
    :cond_0
    iput-object p3, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    .line 2030879
    iput-boolean p4, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    .line 2030880
    return-void
.end method


# virtual methods
.method public final a()LX/DfY;
    .locals 1

    .prologue
    .line 2030868
    sget-object v0, LX/DfY;->V2_MONTAGE_NUX_ITEM:LX/DfY;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2030869
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->a(Landroid/os/Parcel;I)V

    .line 2030870
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    .line 2030871
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2030872
    iget-boolean v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2030873
    return-void
.end method

.method public final b()LX/Dfa;
    .locals 1

    .prologue
    .line 2030858
    sget-object v0, LX/Dfa;->V2_MONTAGE_NUX_ITEM:LX/Dfa;

    return-object v0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 2030854
    sget-object v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d:LX/51l;

    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v0

    .line 2030855
    iget-object v1, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, LX/51h;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;

    .line 2030856
    iget-boolean v1, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    invoke-interface {v0, v1}, LX/51h;->a(Z)LX/51h;

    .line 2030857
    invoke-interface {v0}, LX/51h;->a()LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030862
    if-ne p0, p1, :cond_1

    .line 2030863
    :cond_0
    :goto_0
    return v0

    .line 2030864
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2030865
    goto :goto_0

    .line 2030866
    :cond_3
    check-cast p1, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;

    .line 2030867
    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    iget-object v3, p1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    iget-object v3, p1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030859
    iget v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->c:I

    if-nez v0, :cond_0

    .line 2030860
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->i:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->c:I

    .line 2030861
    :cond_0
    iget v0, p0, Lcom/facebook/messaging/montage/model/MontageInboxNuxItem;->c:I

    return v0
.end method
