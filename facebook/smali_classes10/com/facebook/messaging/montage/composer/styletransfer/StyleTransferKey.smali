.class public Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030726
    new-instance v0, LX/DhE;

    invoke-direct {v0}, LX/DhE;-><init>()V

    sput-object v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2030721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030722
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    .line 2030723
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    .line 2030724
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    .line 2030725
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2030720
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030715
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2030716
    :cond_1
    :goto_0
    return v0

    .line 2030717
    :cond_2
    if-eq p1, p0, :cond_1

    .line 2030718
    check-cast p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    .line 2030719
    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030714
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2030710
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2030711
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030712
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030713
    return-void
.end method
