.class public Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/3AP;

.field public c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DhA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2030699
    const-class v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V
    .locals 10
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2030700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030701
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->d:LX/0Ot;

    .line 2030702
    new-instance v0, LX/3AP;

    const-string v3, "MontageStyleTransfer"

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->b:LX/3AP;

    .line 2030703
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2030704
    iget-object v0, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->c:LX/0TD;

    new-instance v1, LX/DhC;

    invoke-direct {v1, p0, p1, p2}, LX/DhC;-><init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
