.class public Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;
.super Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/photos/photogallery/LaunchableGalleryFragment",
        "<",
        "Lcom/facebook/photos/base/photos/LocalPhoto;",
        "LX/9iQ;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/view/View;

.field public c:LX/ESF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2118881
    invoke-direct {p0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;-><init>()V

    .line 2118882
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2118883
    iget-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->c:LX/ESF;

    invoke-virtual {p0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->b()I

    move-result v1

    invoke-virtual {v0, v1}, LX/ESF;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2118884
    iget-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2118885
    :goto_0
    return-void

    .line 2118886
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 0

    .prologue
    .line 2118887
    invoke-direct {p0}, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->e()V

    .line 2118888
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6d1e289

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2118889
    iget-boolean v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->f:Z

    move v0, v0

    .line 2118890
    if-nez v0, :cond_0

    .line 2118891
    const/4 v0, 0x0

    const/16 v1, 0x2b

    const v3, 0x7906359d

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2118892
    :goto_0
    return-object v0

    .line 2118893
    :cond_0
    const v0, 0x7f031574

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2118894
    const v0, 0x7f0d2e80

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/photogallery/PhotoGallery;

    .line 2118895
    iput-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    .line 2118896
    iget-object v3, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget v4, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->b:I

    iget-object v5, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->c:LX/Duv;

    iget-object p1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->d:LX/9iR;

    invoke-virtual {v3, v4, v5, p1}, Lcom/facebook/photos/photogallery/PhotoGallery;->a(ILX/Duv;LX/9iR;)V

    .line 2118897
    iget-object v3, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    new-instance v4, LX/9iI;

    invoke-direct {v4, p0}, LX/9iI;-><init>(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V

    .line 2118898
    iput-object v4, v3, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    .line 2118899
    const v0, 0x7f0d0a7e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    .line 2118900
    iget-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    new-instance v3, LX/EQf;

    invoke-direct {v3, p0}, LX/EQf;-><init>(Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2118901
    new-instance v0, LX/3Nk;

    iget-object v3, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    invoke-direct {v0, v3}, LX/3Nk;-><init>(Landroid/view/View;)V

    .line 2118902
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/3Nk;->setAlpha(F)V

    .line 2118903
    iget-object v0, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 2118904
    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2118905
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 2118906
    invoke-direct {p0}, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->e()V

    .line 2118907
    const v0, -0x4e10533f

    invoke-static {v0, v2}, LX/02F;->f(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x33046342

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2118908
    invoke-super {p0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->onDestroy()V

    .line 2118909
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->c:LX/ESF;

    .line 2118910
    const/16 v1, 0x2b

    const v2, -0x2e1ab460

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
