.class public Lcom/facebook/vault/model/FacebookVaultDevice;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/model/FacebookVaultDeviceDeserializer;
.end annotation


# instance fields
.field public final mEnabled:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "enabled"
    .end annotation
.end field

.field public final mFbid:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final mIdentifier:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "identifier_value"
    .end annotation
.end field

.field public final mLastSyncTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_sync_time"
    .end annotation
.end field

.field public final mOwnerId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "owner_id"
    .end annotation
.end field

.field public final mSyncMode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sync_mode"
    .end annotation
.end field

.field public final mSyncOlderPhotos:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sync_older_photos"
    .end annotation
.end field

.field public final mTimeCreated:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "date_created"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2118977
    const-class v0, Lcom/facebook/vault/model/FacebookVaultDeviceDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 2118966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118967
    iput-wide v2, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mFbid:J

    .line 2118968
    iput-wide v2, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mOwnerId:J

    .line 2118969
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mIdentifier:Ljava/lang/String;

    .line 2118970
    iput-wide v2, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mTimeCreated:J

    .line 2118971
    iput-wide v2, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mLastSyncTime:J

    .line 2118972
    iput-boolean v1, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mEnabled:Z

    .line 2118973
    const-string v0, "WIFI_ONLY"

    iput-object v0, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncMode:Ljava/lang/String;

    .line 2118974
    iput-boolean v1, p0, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncOlderPhotos:Z

    .line 2118975
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2118976
    const-string v0, "MOBILE_RADIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "WIFI_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
