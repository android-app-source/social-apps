.class public Lcom/facebook/vault/model/BlacklistedSyncPathsGroup;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/model/BlacklistedSyncPathsGroupDeserializer;
.end annotation


# instance fields
.field public paths:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/model/BlacklistedSyncPath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2118942
    const-class v0, Lcom/facebook/vault/model/BlacklistedSyncPathsGroupDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2118943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118944
    return-void
.end method
