.class public final Lcom/facebook/vault/provider/VaultImageProviderRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:J

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121254
    new-instance v0, LX/ERd;

    invoke-direct {v0}, LX/ERd;-><init>()V

    sput-object v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 2121255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121256
    sget-object v0, LX/DbD;->a:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    .line 2121257
    sget-object v0, LX/DbD;->b:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    .line 2121258
    const-wide/16 v0, 0x0

    sget-object v2, LX/DbD;->c:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    .line 2121259
    sget-object v0, LX/DbD;->d:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    .line 2121260
    sget-object v0, LX/DbD;->e:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    .line 2121261
    sget-object v0, LX/DbD;->f:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2121262
    sget-object v0, LX/DbD;->g:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->g:I

    .line 2121263
    sget-object v0, LX/DbD;->h:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2121264
    sget-object v0, LX/DbD;->i:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    .line 2121265
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2121191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    .line 2121193
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    .line 2121194
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    .line 2121195
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    .line 2121196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    .line 2121197
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2121198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->g:I

    .line 2121199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2121200
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    .line 2121201
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJJIIIIJ)V
    .locals 2

    .prologue
    .line 2121243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121244
    iput-object p1, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    .line 2121245
    iput-wide p2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    .line 2121246
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p4, p5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    .line 2121247
    iput-wide p6, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    .line 2121248
    iput p8, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    .line 2121249
    iput p9, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2121250
    iput p10, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->g:I

    .line 2121251
    iput p11, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2121252
    iput-wide p12, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    .line 2121253
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2121266
    const/4 v0, 0x1

    .line 2121267
    iget v1, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    move v0, v0

    .line 2121268
    if-nez v0, :cond_1

    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 2121214
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2121215
    sget-object v1, LX/DbD;->a:LX/0U1;

    .line 2121216
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121217
    iget-object v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121218
    sget-object v1, LX/DbD;->b:LX/0U1;

    .line 2121219
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121220
    iget-wide v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2121221
    sget-object v1, LX/DbD;->c:LX/0U1;

    .line 2121222
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121223
    iget-wide v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2121224
    sget-object v1, LX/DbD;->d:LX/0U1;

    .line 2121225
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121226
    iget-wide v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2121227
    sget-object v1, LX/DbD;->e:LX/0U1;

    .line 2121228
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121229
    iget v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2121230
    sget-object v1, LX/DbD;->f:LX/0U1;

    .line 2121231
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121232
    iget v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2121233
    sget-object v1, LX/DbD;->g:LX/0U1;

    .line 2121234
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121235
    iget v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2121236
    sget-object v1, LX/DbD;->h:LX/0U1;

    .line 2121237
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121238
    iget v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2121239
    sget-object v1, LX/DbD;->i:LX/0U1;

    .line 2121240
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2121241
    iget-wide v2, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2121242
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2121213
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2121212
    const-string v0, "image_hash: %s, fbid: %d, upload_date: %d, date_taken: %d, failure#: %d, upload_state: %d, queued: %d, last_attempt: %d"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget v3, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2121202
    iget-object v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2121203
    iget-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2121204
    iget-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2121205
    iget-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2121206
    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2121207
    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2121208
    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2121209
    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2121210
    iget-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2121211
    return-void
.end method
