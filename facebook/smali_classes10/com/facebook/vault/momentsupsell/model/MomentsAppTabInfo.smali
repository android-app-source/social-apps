.class public Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2120498
    new-instance v0, LX/ER6;

    invoke-direct {v0}, LX/ER6;-><init>()V

    sput-object v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/ER7;)V
    .locals 1

    .prologue
    .line 2120491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120492
    iget v0, p1, LX/ER7;->a:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    .line 2120493
    iget-object v0, p1, LX/ER7;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    .line 2120494
    iget-object v0, p1, LX/ER7;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    .line 2120495
    iget-object v0, p1, LX/ER7;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    .line 2120496
    return-void

    .line 2120497
    :cond_0
    iget-object v0, p1, LX/ER7;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2120485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120486
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    .line 2120487
    invoke-static {p1}, LX/ER8;->a(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    .line 2120488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    .line 2120489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    .line 2120490
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2120484
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2120478
    if-ne p0, p1, :cond_1

    .line 2120479
    :cond_0
    :goto_0
    return v0

    .line 2120480
    :cond_1
    instance-of v2, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 2120481
    goto :goto_0

    .line 2120482
    :cond_2
    check-cast p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    .line 2120483
    iget v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    iget v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2120470
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2120477
    const-class v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "badgeCount"

    iget v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "photoMemImages"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "actionText"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "actionUrl"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2120471
    iget v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120472
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    .line 2120473
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2120474
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120475
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120476
    return-void
.end method
