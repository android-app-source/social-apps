.class public Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2120410
    new-instance v0, LX/ER2;

    invoke-direct {v0}, LX/ER2;-><init>()V

    sput-object v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/ER3;)V
    .locals 1

    .prologue
    .line 2120401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120402
    iget-boolean v0, p1, LX/ER3;->a:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    .line 2120403
    iget-boolean v0, p1, LX/ER3;->b:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    .line 2120404
    iget-object v0, p1, LX/ER3;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120405
    iget-boolean v0, p1, LX/ER3;->d:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    .line 2120406
    iget-boolean v0, p1, LX/ER3;->e:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    .line 2120407
    iget-boolean v0, p1, LX/ER3;->f:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    .line 2120408
    iget-object v0, p1, LX/ER3;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    .line 2120409
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2120387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    .line 2120389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    .line 2120390
    const-class v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    .line 2120392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    .line 2120393
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    .line 2120394
    const-class v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    .line 2120395
    return-void

    :cond_0
    move v0, v2

    .line 2120396
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2120397
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2120398
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2120399
    goto :goto_3

    :cond_4
    move v1, v2

    .line 2120400
    goto :goto_4
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2120386
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2120380
    if-ne p0, p1, :cond_1

    .line 2120381
    :cond_0
    :goto_0
    return v0

    .line 2120382
    :cond_1
    instance-of v2, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 2120383
    goto :goto_0

    .line 2120384
    :cond_2
    check-cast p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120385
    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2120379
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2120365
    const-class v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "hasInstalledMoments"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "showInterstitial"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "interstitialInfo"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "showMomentsTab"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "shouldRemoveTab"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "viewerCanEnable"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "tabInfo"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2120366
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120367
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120368
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2120369
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120370
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120371
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120372
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2120373
    return-void

    :cond_0
    move v0, v2

    .line 2120374
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2120375
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2120376
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2120377
    goto :goto_3

    :cond_4
    move v1, v2

    .line 2120378
    goto :goto_4
.end method
