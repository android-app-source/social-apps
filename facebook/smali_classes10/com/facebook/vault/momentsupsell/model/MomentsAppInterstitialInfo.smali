.class public Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2120463
    new-instance v0, LX/ER4;

    invoke-direct {v0}, LX/ER4;-><init>()V

    sput-object v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/ER5;)V
    .locals 1

    .prologue
    .line 2120450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120451
    iget-object v0, p1, LX/ER5;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    .line 2120452
    iget-object v0, p1, LX/ER5;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    .line 2120453
    iget-object v0, p1, LX/ER5;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    .line 2120454
    iget-object v0, p1, LX/ER5;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    .line 2120455
    iget-object v0, p1, LX/ER5;->e:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    .line 2120456
    iget-object v0, p1, LX/ER5;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    .line 2120457
    iget-object v0, p1, LX/ER5;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    .line 2120458
    iget-object v0, p1, LX/ER5;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    .line 2120459
    iget-object v0, p1, LX/ER5;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    .line 2120460
    iget-boolean v0, p1, LX/ER5;->j:Z

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    .line 2120461
    return-void

    .line 2120462
    :cond_0
    iget-object v0, p1, LX/ER5;->e:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2120437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    .line 2120439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    .line 2120440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    .line 2120441
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    .line 2120442
    invoke-static {p1}, LX/ER8;->a(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    .line 2120443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    .line 2120444
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    .line 2120445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    .line 2120446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    .line 2120447
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    .line 2120448
    return-void

    .line 2120449
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2120464
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2120431
    if-ne p0, p1, :cond_1

    .line 2120432
    :cond_0
    :goto_0
    return v0

    .line 2120433
    :cond_1
    instance-of v2, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 2120434
    goto :goto_0

    .line 2120435
    :cond_2
    check-cast p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120436
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    iget-boolean v3, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2120430
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2120429
    const-class v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "description"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "imageUrl"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "socialContext"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "facepileImageUrls"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "actionText"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "actionUrl"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "helpText"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "helpUrl"

    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "xoutBlocksTab"

    iget-boolean v2, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2120416
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120417
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120418
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120419
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120420
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    .line 2120421
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2120422
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120423
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120424
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120425
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120426
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120427
    return-void

    .line 2120428
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
