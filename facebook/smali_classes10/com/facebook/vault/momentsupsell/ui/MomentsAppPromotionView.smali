.class public Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/EQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/fbui/facepile/FacepileView;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/resources/ui/FbButton;

.field private l:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2120563
    const-class v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2120564
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2120565
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a()V

    .line 2120566
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2120567
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2120568
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a()V

    .line 2120569
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2120570
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2120571
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a()V

    .line 2120572
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2120573
    const-class v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    invoke-static {v0, p0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2120574
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030b2f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2120575
    const v0, 0x7f0d1c2c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2120576
    const v0, 0x7f0d1c2d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2120577
    const v0, 0x7f0d1c2e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2120578
    const v0, 0x7f0d1c2f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2120579
    const v0, 0x7f0d1c30

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2120580
    const v0, 0x7f0d1c31

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->k:Lcom/facebook/resources/ui/FbButton;

    .line 2120581
    const v0, 0x7f0d1c32

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->l:Lcom/facebook/resources/ui/FbButton;

    .line 2120582
    return-void
.end method

.method private static a(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;Landroid/content/Context;LX/1Vg;LX/EQh;)V
    .locals 0

    .prologue
    .line 2120583
    iput-object p1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->b:LX/1Vg;

    iput-object p3, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->c:LX/EQh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v2, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v1, LX/1Vg;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1Vg;

    invoke-static {v2}, LX/EQh;->a(LX/0QB;)LX/EQh;

    move-result-object v2

    check-cast v2, LX/EQh;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;Landroid/content/Context;LX/1Vg;LX/EQh;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V
    .locals 7
    .param p1    # Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2120584
    if-eqz p1, :cond_0

    .line 2120585
    iget-object v0, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v0, v0

    .line 2120586
    if-nez v0, :cond_1

    .line 2120587
    :cond_0
    :goto_0
    return-void

    .line 2120588
    :cond_1
    iput-object p1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120589
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120590
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v0, v1

    .line 2120591
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2120592
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2120593
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2120594
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2120595
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2120596
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2120597
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2120598
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2120599
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2120600
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->e:LX/0Px;

    move-object v1, v1

    .line 2120601
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2120602
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 2120603
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2120604
    :goto_1
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2120605
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2120606
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2120607
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2120608
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2120609
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2120610
    :goto_2
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->b:LX/1Vg;

    new-instance v2, LX/1Wx;

    invoke-direct {v2}, LX/1Wx;-><init>()V

    invoke-virtual {v1, v2}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v1

    .line 2120611
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->k:Lcom/facebook/resources/ui/FbButton;

    .line 2120612
    iget-object v3, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2120613
    invoke-static {v3}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2120614
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->k:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/ERC;

    invoke-direct {v3, p0, v0, v1}, LX/ERC;-><init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;LX/1Vr;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2120615
    new-array v2, v6, [Ljava/lang/CharSequence;

    .line 2120616
    iget-object v3, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2120617
    aput-object v3, v2, v4

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    new-array v2, v6, [Ljava/lang/CharSequence;

    .line 2120618
    iget-object v3, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    move-object v3, v3

    .line 2120619
    aput-object v3, v2, v4

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2120620
    :cond_2
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2120621
    :cond_3
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->i:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto :goto_1

    .line 2120622
    :cond_4
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2

    .line 2120623
    :cond_5
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2120624
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->l:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120625
    iget-object v4, v3, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v3, v4

    .line 2120626
    iget-object v4, v3, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->h:Ljava/lang/String;

    move-object v3, v4

    .line 2120627
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2120628
    iget-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->l:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/ERD;

    invoke-direct {v3, p0, v1, v0}, LX/ERD;-><init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;LX/1Vr;Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
