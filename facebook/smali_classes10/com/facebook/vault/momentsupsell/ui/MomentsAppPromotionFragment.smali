.class public Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/EQj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/Dwz;

.field private d:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

.field private e:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2120513
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2120531
    iput-object p1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120532
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120533
    iget-object p1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v0, p1

    .line 2120534
    if-eqz v0, :cond_0

    .line 2120535
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2120536
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->d:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    invoke-virtual {v0, v1}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->setVisibility(I)V

    .line 2120537
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->d:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v0, v1}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V

    .line 2120538
    :goto_0
    return-void

    .line 2120539
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2120540
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->d:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    invoke-virtual {v0, v2}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2120541
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2120542
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;

    invoke-static {v0}, LX/EQj;->a(LX/0QB;)LX/EQj;

    move-result-object v0

    check-cast v0, LX/EQj;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->a:LX/EQj;

    .line 2120543
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xb534c3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2120527
    const v0, 0x7f030b2d

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2120528
    const v0, 0x7f0d1c2a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->d:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    .line 2120529
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->e:Landroid/widget/ProgressBar;

    .line 2120530
    const/16 v0, 0x2b

    const v3, -0x7ca87e36

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2120514
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2120515
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2120516
    if-eqz v0, :cond_0

    .line 2120517
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2120518
    const-string v1, "arg_moments_app_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-static {p0, v0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V

    .line 2120519
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-nez v0, :cond_1

    .line 2120520
    new-instance v0, LX/ERB;

    invoke-direct {v0, p0}, LX/ERB;-><init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;)V

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->c:LX/Dwz;

    .line 2120521
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->a:LX/EQj;

    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->c:LX/Dwz;

    invoke-virtual {v0, v1}, LX/EQj;->a(LX/Dwz;)V

    .line 2120522
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->a:LX/EQj;

    invoke-virtual {v0}, LX/EQj;->a()Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    move-result-object v0

    .line 2120523
    if-eqz v0, :cond_1

    .line 2120524
    invoke-static {p0, v0}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->b(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V

    .line 2120525
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->a:LX/EQj;

    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;->c:LX/Dwz;

    invoke-virtual {v0, v1}, LX/EQj;->b(LX/Dwz;)V

    .line 2120526
    :cond_1
    return-void
.end method
