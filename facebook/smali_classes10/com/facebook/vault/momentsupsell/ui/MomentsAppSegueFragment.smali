.class public Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/EQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/view/View;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2120635
    const-class v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2120636
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2120637
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2120638
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    const-class v2, Landroid/content/Context;

    const-class p1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v2, p1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class p1, LX/1Vg;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/1Vg;

    invoke-static {v0}, LX/EQh;->a(LX/0QB;)LX/EQh;

    move-result-object v0

    check-cast v0, LX/EQh;

    iput-object v2, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->a:Landroid/content/Context;

    iput-object p1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->b:LX/1Vg;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->c:LX/EQh;

    .line 2120639
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6b8431cb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2120640
    const v1, 0x7f030b30

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x14be6c95

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2120641
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2120642
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2120643
    if-eqz v0, :cond_0

    .line 2120644
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2120645
    const-string v1, "arg_moments_app_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120646
    :cond_0
    const v0, 0x7f0d1c33

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->f:Landroid/view/View;

    .line 2120647
    const v0, 0x7f0d1c34

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2120648
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120649
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    move-object v0, v1

    .line 2120650
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120651
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    move-object v0, v1

    .line 2120652
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    move-object v0, v1

    .line 2120653
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120654
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    move-object v0, v1

    .line 2120655
    iget-object v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    move-object v0, v1

    .line 2120656
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2120657
    iget-object v1, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->e:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2120658
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    move-object v0, v2

    .line 2120659
    iget-object v2, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;->b:LX/0Px;

    move-object v0, v2

    .line 2120660
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2120661
    :cond_1
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 2120662
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->f:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 2120663
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->f:Landroid/view/View;

    const v1, -0x3f99999a    # -3.6f

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 2120664
    const v0, 0x7f0d1c35

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2120665
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2120666
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/ERE;

    invoke-direct {v1, p0}, LX/ERE;-><init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2120667
    return-void
.end method
