.class public final Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2119744
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;

    new-instance v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2119745
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2119714
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2119716
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2119717
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2119718
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2119719
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2119720
    if-eqz v2, :cond_3

    .line 2119721
    const-string v3, "moments_app_synced_photos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2119722
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2119723
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2119724
    if-eqz v3, :cond_2

    .line 2119725
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2119726
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2119727
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 2119728
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2119729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2119730
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2119731
    if-eqz p0, :cond_0

    .line 2119732
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2119733
    invoke-static {v1, p0, p1, p2}, LX/EQr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2119734
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2119735
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2119736
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2119737
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2119738
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2119739
    if-eqz v2, :cond_4

    .line 2119740
    const-string v3, "vault"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2119741
    invoke-static {v1, v2, p1, p2}, LX/EQz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2119742
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2119743
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2119715
    check-cast p1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$Serializer;->a(Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
