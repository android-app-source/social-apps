.class public final Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2119395
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2119396
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2119393
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2119394
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2119326
    if-nez p1, :cond_0

    move v0, v1

    .line 2119327
    :goto_0
    return v0

    .line 2119328
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2119329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2119330
    :sswitch_0
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119331
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119332
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2119333
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2119334
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2119335
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2119336
    invoke-virtual {p3, v2, v1}, LX/186;->b(II)V

    .line 2119337
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2119338
    :sswitch_1
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119339
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119340
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2119341
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2119342
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2119343
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2119344
    invoke-virtual {p3, v2, v1}, LX/186;->b(II)V

    .line 2119345
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2119346
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119347
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119348
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119349
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2119350
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2119351
    :sswitch_3
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119352
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119353
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2119354
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2119355
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2119356
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 2119357
    invoke-virtual {p3, v2, v1}, LX/186;->b(II)V

    .line 2119358
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2119359
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119360
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119361
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119362
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2119363
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2119364
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2119365
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2119366
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119367
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2119368
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2119369
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2119370
    const v2, -0x6d1efd5d

    const/4 v5, 0x0

    .line 2119371
    if-nez v0, :cond_1

    move v4, v5

    .line 2119372
    :goto_1
    move v0, v4

    .line 2119373
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119374
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2119375
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2119376
    :sswitch_7
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    .line 2119377
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2119378
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119379
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2119380
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2119381
    :sswitch_8
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2119382
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2119383
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2119384
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2119385
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2119386
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2119387
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2119388
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2119389
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2119390
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2119391
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2119392
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e6d2f9f -> :sswitch_3
        -0x6d1efd5d -> :sswitch_7
        -0x565fb601 -> :sswitch_0
        -0x415e7df6 -> :sswitch_6
        -0x38e3bca1 -> :sswitch_5
        0xfbad9a1 -> :sswitch_1
        0x1c850c61 -> :sswitch_8
        0x7df427db -> :sswitch_4
        0x7e4dfa01 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2119325
    new-instance v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2119320
    if-eqz p0, :cond_0

    .line 2119321
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2119322
    if-eq v0, p0, :cond_0

    .line 2119323
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2119324
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2119307
    sparse-switch p2, :sswitch_data_0

    .line 2119308
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2119309
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2119310
    const v1, -0x6d1efd5d

    .line 2119311
    if-eqz v0, :cond_0

    .line 2119312
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2119313
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2119314
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2119315
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2119316
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2119317
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2119318
    :sswitch_2
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    .line 2119319
    invoke-static {v0, p3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e6d2f9f -> :sswitch_1
        -0x6d1efd5d -> :sswitch_2
        -0x565fb601 -> :sswitch_1
        -0x415e7df6 -> :sswitch_0
        -0x38e3bca1 -> :sswitch_1
        0xfbad9a1 -> :sswitch_1
        0x1c850c61 -> :sswitch_1
        0x7df427db -> :sswitch_1
        0x7e4dfa01 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2119301
    if-eqz p1, :cond_0

    .line 2119302
    invoke-static {p0, p1, p2}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2119303
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    .line 2119304
    if-eq v0, v1, :cond_0

    .line 2119305
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2119306
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2119300
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2119298
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2119299
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2119397
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2119398
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2119399
    :cond_0
    iput-object p1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2119400
    iput p2, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->b:I

    .line 2119401
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2119297
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2119296
    new-instance v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2119293
    iget v0, p0, LX/1vt;->c:I

    .line 2119294
    move v0, v0

    .line 2119295
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2119290
    iget v0, p0, LX/1vt;->c:I

    .line 2119291
    move v0, v0

    .line 2119292
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2119287
    iget v0, p0, LX/1vt;->b:I

    .line 2119288
    move v0, v0

    .line 2119289
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119284
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2119285
    move-object v0, v0

    .line 2119286
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2119275
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2119276
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2119277
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2119278
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2119279
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2119280
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2119281
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2119282
    invoke-static {v3, v9, v2}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2119283
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2119272
    iget v0, p0, LX/1vt;->c:I

    .line 2119273
    move v0, v0

    .line 2119274
    return v0
.end method
