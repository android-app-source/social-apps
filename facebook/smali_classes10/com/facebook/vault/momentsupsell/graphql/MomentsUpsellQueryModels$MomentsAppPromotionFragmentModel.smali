.class public final Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5fb8a597
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:I

.field private i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2119555
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2119556
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2119557
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2119558
    return-void
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119552
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119553
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119559
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119560
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2119561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2119562
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x565fb601

    invoke-static {v1, v0, v2}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2119563
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0xfbad9a1

    invoke-static {v2, v1, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2119564
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2119565
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 2119566
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x7e6d2f9f

    invoke-static {v5, v4, v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2119567
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x7df427db

    invoke-static {v6, v5, v7}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2119568
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2119569
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x38e3bca1    # -40003.37f

    invoke-static {v8, v7, v9}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2119570
    const/16 v8, 0xc

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2119571
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2119572
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2119573
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2119574
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->h:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 2119575
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2119576
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2119577
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2119578
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2119579
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2119580
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2119581
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2119582
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2119583
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2119584
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2119585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2119586
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2119587
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x565fb601

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2119588
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2119589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119590
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->e:I

    move-object v1, v0

    .line 2119591
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2119592
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xfbad9a1

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2119593
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2119594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119595
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->f:I

    move-object v1, v0

    .line 2119596
    :cond_1
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2119597
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119598
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2119599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119600
    iput-object v0, v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119601
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2119602
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2119603
    if-eqz v2, :cond_3

    .line 2119604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119605
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 2119606
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2119607
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7e6d2f9f

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2119608
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2119609
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119610
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k:I

    move-object v1, v0

    .line 2119611
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 2119612
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7df427db

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2119613
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2119614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119615
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l:I

    move-object v1, v0

    .line 2119616
    :cond_5
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2119617
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119618
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2119619
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119620
    iput-object v0, v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2119621
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2119622
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x38e3bca1    # -40003.37f

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2119623
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2119624
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119625
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p:I

    move-object v1, v0

    .line 2119626
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2119627
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 2119628
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 2119629
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 2119630
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 2119631
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 2119632
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_8
    move-object p0, v1

    .line 2119633
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2119634
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2119635
    const v0, -0x565fb601

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->e:I

    .line 2119636
    const/4 v0, 0x1

    const v1, 0xfbad9a1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->f:I

    .line 2119637
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->g:Z

    .line 2119638
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->h:I

    .line 2119639
    const/4 v0, 0x6

    const v1, -0x7e6d2f9f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k:I

    .line 2119640
    const/4 v0, 0x7

    const v1, 0x7df427db

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l:I

    .line 2119641
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m:Z

    .line 2119642
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n:Z

    .line 2119643
    const/16 v0, 0xb

    const v1, -0x38e3bca1    # -40003.37f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p:I

    .line 2119644
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2119645
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119646
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->g:Z

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2119647
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119648
    iget v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->h:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2119649
    new-instance v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;-><init>()V

    .line 2119650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2119651
    return-object v0
.end method

.method public final synthetic c()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119554
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2119550
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119551
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2119549
    const v0, 0x14783f6b

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2119547
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119548
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2119546
    const v0, 0x4e694e23    # 9.7855302E8f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLinkIfMomentsInstalled"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119533
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119534
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFacepileUsers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2119544
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j:Ljava/util/List;

    .line 2119545
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final kG_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2119542
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119543
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic kH_()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119541
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHelpLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119539
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119540
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119537
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119538
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119535
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119536
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->p:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
