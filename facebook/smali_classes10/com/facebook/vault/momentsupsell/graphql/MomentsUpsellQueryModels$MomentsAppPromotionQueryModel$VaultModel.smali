.class public final Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x22216154
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2119766
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2119796
    const-class v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2119797
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2119798
    return-void
.end method

.method private j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMomentsAppPromotion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119808
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->f:Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    iput-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->f:Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119809
    iget-object v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->f:Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2119799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2119800
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x1c850c61

    invoke-static {v1, v0, v2}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2119801
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2119802
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2119803
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2119804
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2119805
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2119806
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2119807
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2119779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2119780
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2119781
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1c850c61

    invoke-static {v2, v0, v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2119782
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2119783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    .line 2119784
    iput v3, v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->e:I

    move-object v1, v0

    .line 2119785
    :cond_0
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2119786
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119787
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2119788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    .line 2119789
    iput-object v0, v1, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->f:Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    .line 2119790
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2119791
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2119792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2119793
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2119794
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119795
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2119775
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2119776
    const/4 v0, 0x0

    const v1, 0x1c850c61

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->e:I

    .line 2119777
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->g:Z

    .line 2119778
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2119772
    new-instance v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    invoke-direct {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;-><init>()V

    .line 2119773
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2119774
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMomentsAppPromotion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119771
    invoke-direct {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->j()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2119769
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2119770
    iget-boolean v0, p0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2119768
    const v0, 0x47f397fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2119767
    const v0, 0x4e9c212

    return v0
.end method
