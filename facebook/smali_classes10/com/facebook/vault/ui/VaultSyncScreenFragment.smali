.class public Lcom/facebook/vault/ui/VaultSyncScreenFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

.field public B:LX/ESc;

.field public C:LX/ESY;

.field private final D:LX/ESd;

.field public final E:Landroid/view/GestureDetector;

.field public a:LX/ERL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2TM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/ERZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/ERY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/ES8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2TN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/ERr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/ESE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Landroid/content/BroadcastReceiver;

.field public n:Landroid/content/Context;

.field private o:Lcom/facebook/photos/base/photos/VaultPhoto;

.field public p:LX/0gc;

.field public q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

.field public r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public s:I

.field public t:I

.field public u:Z

.field private v:Landroid/view/View;

.field public w:LX/ES9;

.field public x:Landroid/widget/GridView;

.field public y:Landroid/widget/RelativeLayout;

.field public z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2123224
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2123225
    iput v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->s:I

    .line 2123226
    iput v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->t:I

    .line 2123227
    iput-boolean v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->u:Z

    .line 2123228
    new-instance v0, LX/ESd;

    invoke-direct {v0, p0}, LX/ESd;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->D:LX/ESd;

    .line 2123229
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/ESQ;

    invoke-direct {v1, p0}, LX/ESQ;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->E:Landroid/view/GestureDetector;

    .line 2123230
    return-void
.end method

.method public static synthetic a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 2123358
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->r:Ljava/util/Set;

    return-object p1
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2123349
    if-ltz p1, :cond_1

    .line 2123350
    sget-object v1, LX/ESc;->VISIBLE:LX/ESc;

    iput-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->B:LX/ESc;

    move p1, v0

    .line 2123351
    :cond_0
    :goto_0
    iget v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->t:I

    if-ne v1, p1, :cond_2

    .line 2123352
    :goto_1
    return-void

    .line 2123353
    :cond_1
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getHeight()I

    move-result v1

    if-gt p1, v1, :cond_0

    .line 2123354
    sget-object v1, LX/ESc;->HIDDEN:LX/ESc;

    iput-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->B:LX/ESc;

    .line 2123355
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getHeight()I

    move-result v1

    neg-int p1, v1

    goto :goto_0

    .line 2123356
    :cond_2
    iput p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->t:I

    .line 2123357
    new-instance v1, LX/ESb;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-direct {v1, p0, v2}, LX/ESb;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Landroid/view/View;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->t:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;I)V
    .locals 4

    .prologue
    .line 2123334
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-interface {v0, p1}, LX/ES9;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123335
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    if-nez v0, :cond_1

    .line 2123336
    :cond_0
    :goto_0
    return-void

    .line 2123337
    :cond_1
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    instance-of v0, v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;

    if-eqz v0, :cond_2

    .line 2123338
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->i:LX/ES8;

    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    check-cast v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;

    .line 2123339
    iget-object p1, v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;->f:Ljava/lang/String;

    move-object v0, p1

    .line 2123340
    invoke-virtual {v1, v0}, LX/ES8;->a(Ljava/lang/String;)Lcom/facebook/vault/provider/VaultImageProviderRow;

    move-result-object v0

    .line 2123341
    if-eqz v0, :cond_0

    .line 2123342
    invoke-virtual {v0}, Lcom/facebook/vault/provider/VaultImageProviderRow;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2123343
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123344
    iget-wide v2, v0, LX/74w;->a:J

    move-wide v0, v2

    .line 2123345
    invoke-static {p0, v0, v1}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;J)V

    goto :goto_0

    .line 2123346
    :cond_2
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->o:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123347
    iget-wide v2, v0, LX/74w;->a:J

    move-wide v0, v2

    .line 2123348
    invoke-static {p0, v0, v1}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;J)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;J)V
    .locals 5

    .prologue
    .line 2123325
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2123326
    const-string v0, "selectedPhotoIdParam"

    invoke-virtual {v2, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2123327
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2123328
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-interface {v0}, LX/ES9;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2123329
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-interface {v0, v1}, LX/ES9;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2123330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2123331
    :cond_0
    const-string v0, "photoObjectsParam"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2123332
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->h:Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0x141

    invoke-interface {v0, v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2123333
    return-void
.end method

.method public static c(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 2

    .prologue
    .line 2123322
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    if-eqz v0, :cond_0

    .line 2123323
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->p:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2123324
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2123320
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-interface {v0, v1, v1}, LX/ES9;->a(II)V

    .line 2123321
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2123317
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2123318
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-static {p1}, LX/ERL;->a(LX/0QB;)LX/ERL;

    move-result-object v3

    check-cast v3, LX/ERL;

    invoke-static {p1}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v4

    check-cast v4, LX/2TK;

    invoke-static {p1}, LX/2TM;->c(LX/0QB;)LX/2TM;

    move-result-object v5

    check-cast v5, LX/2TM;

    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v6

    check-cast v6, LX/11H;

    invoke-static {p1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    new-instance v8, LX/ERZ;

    invoke-direct {v8}, LX/ERZ;-><init>()V

    move-object v8, v8

    move-object v8, v8

    check-cast v8, LX/ERZ;

    new-instance v9, LX/ERY;

    invoke-direct {v9}, LX/ERY;-><init>()V

    move-object v9, v9

    move-object v9, v9

    check-cast v9, LX/ERY;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v11

    check-cast v11, LX/ES8;

    invoke-static {p1}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v12

    check-cast v12, LX/2TN;

    invoke-static {p1}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v13

    check-cast v13, LX/ERr;

    const-class v0, LX/ESE;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/ESE;

    iput-object v3, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a:LX/ERL;

    iput-object v4, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    iput-object v5, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->c:LX/2TM;

    iput-object v6, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->d:LX/11H;

    iput-object v7, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iput-object v8, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->f:LX/ERZ;

    iput-object v9, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->g:LX/ERY;

    iput-object v10, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object v11, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->i:LX/ES8;

    iput-object v12, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->j:LX/2TN;

    iput-object v13, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->k:LX/ERr;

    iput-object p1, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->l:LX/ESE;

    .line 2123319
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2123314
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(II)V

    .line 2123315
    return-void

    .line 2123316
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getHeight()I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x6cccc28d

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2123294
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2123295
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    .line 2123296
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    const v2, 0x7f0d304e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    .line 2123297
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->l:LX/ESE;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    .line 2123298
    new-instance p1, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p1, v4, v5, v2}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;-><init>(LX/11H;Landroid/content/Context;Landroid/widget/GridView;)V

    .line 2123299
    move-object v0, p1

    .line 2123300
    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    .line 2123301
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2123302
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    new-instance v2, LX/ESS;

    invoke-direct {v2, p0}, LX/ESS;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2123303
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    new-instance v2, LX/EST;

    invoke-direct {v2, p0}, LX/EST;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2123304
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    new-instance v2, LX/ESe;

    invoke-direct {v2, p0}, LX/ESe;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2123305
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 2123306
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->p:LX/0gc;

    .line 2123307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    .line 2123308
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->r:Ljava/util/Set;

    .line 2123309
    new-instance v0, LX/ESa;

    invoke-direct {v0, p0}, LX/ESa;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->m:Landroid/content/BroadcastReceiver;

    .line 2123310
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    .line 2123311
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    .line 2123312
    int-to-double v2, v2

    const-wide v4, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v2, v4

    int-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-int v0, v2

    iput v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->s:I

    .line 2123313
    const/16 v0, 0x2b

    const v2, 0x4dd0e909    # 4.3811664E8f

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2123288
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2123289
    const/16 v0, 0x141

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2123290
    :cond_0
    :goto_0
    return-void

    .line 2123291
    :cond_1
    const-string v0, "onResultPhotoObjectParam"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123292
    new-instance v1, LX/ESX;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->f:LX/ERZ;

    iget-object v3, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->g:LX/ERY;

    invoke-direct {v1, p0, v2, v3}, LX/ESX;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;LX/ERZ;LX/ERY;)V

    .line 2123293
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/photos/base/photos/VaultPhoto;

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x31d74c2e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2123280
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2123281
    const v0, 0x7f031578

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    .line 2123282
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    const v2, 0x7f0d304f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    .line 2123283
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    const v2, 0x7f0d3045

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    .line 2123284
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    const v2, 0x7f0d304a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    .line 2123285
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    new-instance v2, LX/ESR;

    invoke-direct {v2, p0}, LX/ESR;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2123286
    sget-object v0, LX/ESc;->VISIBLE:LX/ESc;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->B:LX/ESc;

    .line 2123287
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->v:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x5411d7c9

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x106e538e    # -9.01676E28f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123275
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2123276
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    invoke-static {v1}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 2123277
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    invoke-virtual {v1}, LX/2TK;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    instance-of v1, v1, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    if-eqz v1, :cond_0

    .line 2123278
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->D:LX/ESd;

    invoke-interface {v1, v2}, LX/ES9;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2123279
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x208cdfe9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x218fd191

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123257
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2123258
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a()V

    .line 2123259
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->c()V

    .line 2123260
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    invoke-virtual {v1}, LX/2TK;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2123261
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    instance-of v1, v1, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    if-eqz v1, :cond_0

    .line 2123262
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->D:LX/ESd;

    invoke-interface {v1, v2}, LX/ES9;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2123263
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, LX/2TK;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2123264
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->k:LX/ERr;

    invoke-virtual {v1}, LX/ERr;->e()Ljava/lang/String;

    move-result-object v1

    .line 2123265
    const-string v2, "end_vault_upload"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2123266
    new-instance v1, LX/ESZ;

    invoke-direct {v1, p0, v3}, LX/ESZ;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V

    .line 2123267
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2123268
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    invoke-static {v1}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->m:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "vault.intent.action.SyncStatus"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2123269
    const v1, 0x65a95fb

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2123270
    :cond_2
    invoke-static {p0}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    goto :goto_0

    .line 2123271
    :cond_3
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v1}, LX/ESY;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2123272
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v1, v3}, LX/ESY;->cancel(Z)Z

    .line 2123273
    :cond_4
    new-instance v1, LX/ESY;

    invoke-direct {v1, p0, v3}, LX/ESY;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V

    iput-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    .line 2123274
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    goto :goto_1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x17f4a4a7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123231
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2123232
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    invoke-virtual {v1}, LX/2TK;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2123233
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    if-nez v1, :cond_0

    .line 2123234
    new-instance v1, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-direct {v1}, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;-><init>()V

    const v2, 0x7f031573

    .line 2123235
    iput v2, v1, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->b:I

    .line 2123236
    move-object v1, v1

    .line 2123237
    iput-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    .line 2123238
    :cond_0
    const-string v1, "none"

    .line 2123239
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2123240
    if-eqz v2, :cond_1

    .line 2123241
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2123242
    const-string v3, "nux_ref"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2123243
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2123244
    const-string v2, "nux_ref"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2123245
    :cond_1
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->c:LX/2TM;

    .line 2123246
    sget-object v3, LX/2TM;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2123247
    const-string v4, "ref"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2123248
    iget-object v4, v2, LX/2TM;->y:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2123249
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    .line 2123250
    iput-object v1, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->a:Ljava/lang/String;

    .line 2123251
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    new-instance v2, LX/ESU;

    invoke-direct {v2, p0}, LX/ESU;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    .line 2123252
    iput-object v2, v1, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->k:LX/ESU;

    .line 2123253
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->p:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d304d

    iget-object v3, p0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->q:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2123254
    :goto_0
    const v1, -0x7321cf10

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2123255
    :cond_2
    invoke-static {p0}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->c(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    .line 2123256
    invoke-static {p0}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    goto :goto_0
.end method
