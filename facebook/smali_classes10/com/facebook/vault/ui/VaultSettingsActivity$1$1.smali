.class public final Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/ESI;


# direct methods
.method public constructor <init>(LX/ESI;Z)V
    .locals 0

    .prologue
    .line 2122797
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->b:LX/ESI;

    iput-boolean p2, p0, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->a:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2122787
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->b:LX/ESI;

    iget-object v0, v0, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->q:LX/2TL;

    invoke-virtual {v0}, LX/2TL;->a()J

    move-result-wide v0

    .line 2122788
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->b:LX/ESI;

    iget-object v2, v2, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSettingsActivity;->s:LX/ERh;

    iget-boolean v3, p0, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->a:Z

    .line 2122789
    new-instance v4, LX/ERV;

    invoke-direct {v4, v0, v1}, LX/ERV;-><init>(J)V

    .line 2122790
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, LX/ERV;->e:Ljava/lang/Boolean;

    .line 2122791
    :try_start_0
    iget-object v5, v2, LX/ERh;->b:LX/11H;

    iget-object p0, v2, LX/ERh;->e:LX/ERU;

    invoke-virtual {v5, p0, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2122792
    :cond_0
    :goto_0
    return-void

    .line 2122793
    :catch_0
    move-exception v4

    .line 2122794
    sget-object v5, LX/ERh;->a:Ljava/lang/String;

    const-string p0, "updateDeviceSyncOlderPhotosOnServer"

    invoke-static {v5, p0, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2122795
    instance-of v5, v4, LX/2Oo;

    if-eqz v5, :cond_0

    .line 2122796
    iget-object v5, v2, LX/ERh;->o:LX/03V;

    const-string p0, "vault_device_update_api exception"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, p0, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
