.class public Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;
.super Landroid/widget/RelativeLayout;
.source ""


# instance fields
.field public a:LX/ERr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2123386
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2123387
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->c:Landroid/content/Context;

    .line 2123388
    const-class v0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-static {v0, p0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2123389
    return-void
.end method

.method private static a(Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;LX/ERr;LX/2TK;)V
    .locals 0

    .prologue
    .line 2123390
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a:LX/ERr;

    iput-object p2, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->b:LX/2TK;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-static {v1}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v0

    check-cast v0, LX/ERr;

    invoke-static {v1}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v1

    check-cast v1, LX/2TK;

    invoke-static {p0, v0, v1}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a(Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;LX/ERr;LX/2TK;)V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2123391
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123392
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2123393
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123394
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2123381
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a:LX/ERr;

    invoke-virtual {v0}, LX/ERr;->e()Ljava/lang/String;

    move-result-object v0

    .line 2123382
    const-string v1, "end_vault_upload"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123383
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->c()V

    .line 2123384
    :goto_0
    return-void

    .line 2123385
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2123364
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->b:LX/2TK;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/2TK;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123365
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123366
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123367
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2123368
    :goto_0
    return-void

    .line 2123369
    :cond_0
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d()V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2123375
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->b:LX/2TK;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/2TK;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123376
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123377
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2123378
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2123379
    :goto_0
    return-void

    .line 2123380
    :cond_0
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d()V

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3b2678f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2123370
    const v0, 0x7f0d3048

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d:Landroid/widget/ImageView;

    .line 2123371
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->d:Landroid/widget/ImageView;

    new-instance v2, LX/ESf;

    invoke-direct {v2, p0}, LX/ESf;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2123372
    const v0, 0x7f0d3049

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->e:Landroid/widget/ImageView;

    .line 2123373
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->c:Landroid/content/Context;

    const v2, 0x7f0400fd

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->f:Landroid/view/animation/Animation;

    .line 2123374
    const/16 v0, 0x2d

    const v2, 0x2461eb84

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
