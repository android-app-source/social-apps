.class public Lcom/facebook/vault/ui/VaultDraweeGridAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/ES9;


# static fields
.field public static final b:Ljava/lang/String;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/widget/GridView;

.field public f:LX/ESB;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/photos/VaultPhoto;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "LX/ESH;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/11H;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2122684
    const-class v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->b:Ljava/lang/String;

    .line 2122685
    const-class v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    const-string v1, "photo_vault"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;Landroid/content/Context;Landroid/widget/GridView;)V
    .locals 1
    .param p3    # Landroid/widget/GridView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2122673
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2122674
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->a:Ljava/lang/String;

    .line 2122675
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    .line 2122676
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->j:LX/11H;

    .line 2122677
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    .line 2122678
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->i:Ljava/util/HashSet;

    .line 2122679
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->a:Ljava/lang/String;

    .line 2122680
    sget-object v0, LX/ESB;->IDLE:LX/ESB;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    .line 2122681
    iput-object p2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->d:Landroid/content/Context;

    .line 2122682
    iput-object p3, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->e:Landroid/widget/GridView;

    .line 2122683
    return-void
.end method

.method private c(Landroid/net/Uri;)LX/ESH;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2122659
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Photo URI cannot be null"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2122660
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2122661
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v2

    .line 2122662
    :goto_1
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2122663
    add-int v0, v2, v1

    .line 2122664
    iget-object v3, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122665
    invoke-virtual {v0}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2122666
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESH;

    .line 2122667
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->e:Landroid/widget/GridView;

    invoke-virtual {v2, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ESH;->a(Landroid/view/View;)V

    .line 2122668
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->e:Landroid/widget/GridView;

    invoke-virtual {v2, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2122669
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 2122670
    goto :goto_0

    .line 2122671
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2122672
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a(II)V
    .locals 5

    .prologue
    .line 2122603
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->getCount()I

    move-result v0

    add-int v1, p1, p2

    add-int/lit8 v1, v1, 0x28

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    sget-object v1, LX/ESB;->IDLE:LX/ESB;

    if-ne v0, v1, :cond_0

    .line 2122604
    sget-object v0, LX/ESB;->FETCHING:LX/ESB;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    .line 2122605
    new-instance v0, LX/ESC;

    invoke-direct {v0, p0}, LX/ESC;-><init>(Lcom/facebook/vault/ui/VaultDraweeGridAdapter;)V

    .line 2122606
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/ESC;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2122607
    :cond_0
    monitor-exit p0

    return-void

    .line 2122608
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2122650
    invoke-direct {p0, p1}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c(Landroid/net/Uri;)LX/ESH;

    move-result-object v0

    .line 2122651
    if-eqz v0, :cond_1

    .line 2122652
    const/16 p1, 0x8

    .line 2122653
    iget-object v1, v0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2122654
    iget-object v1, v0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2122655
    :cond_0
    iget-object v1, v0, LX/ESH;->c:Landroid/widget/ProgressBar;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122656
    iget-object v1, v0, LX/ESH;->c:Landroid/widget/ProgressBar;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2122657
    iget-object v1, v0, LX/ESH;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122658
    :cond_1
    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 2122641
    invoke-direct {p0, p1}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c(Landroid/net/Uri;)LX/ESH;

    move-result-object v0

    .line 2122642
    if-eqz v0, :cond_1

    .line 2122643
    const/16 p1, 0x8

    .line 2122644
    iget-object p0, v0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getVisibility()I

    move-result p0

    if-nez p0, :cond_0

    .line 2122645
    iget-object p0, v0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2122646
    :cond_0
    iget-object p0, v0, LX/ESH;->c:Landroid/widget/ProgressBar;

    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122647
    iget-object p0, v0, LX/ESH;->b:Landroid/widget/ProgressBar;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122648
    iget-object p0, v0, LX/ESH;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2122649
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/photos/base/photos/VaultPhoto;)V
    .locals 2

    .prologue
    .line 2122686
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2122687
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->i:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2122688
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2122689
    const v0, -0x29c23783

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2122690
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/photos/VaultPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2122634
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122635
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2122636
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2122637
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2122638
    :cond_1
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    new-instance v1, LX/ESA;

    invoke-direct {v1, p0}, LX/ESA;-><init>(Lcom/facebook/vault/ui/VaultDraweeGridAdapter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2122639
    const v0, -0x7c650ff7

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2122640
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2122630
    invoke-direct {p0, p1}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c(Landroid/net/Uri;)LX/ESH;

    move-result-object v0

    .line 2122631
    if-eqz v0, :cond_0

    .line 2122632
    invoke-virtual {v0}, LX/ESH;->a()V

    .line 2122633
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2122629
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2122628
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2122627
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2122609
    if-nez p2, :cond_0

    .line 2122610
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031571

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2122611
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122612
    invoke-virtual {v0}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v2

    .line 2122613
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2122614
    new-instance v1, LX/ESH;

    iget-object v3, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->d:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, LX/ESH;-><init>(Landroid/content/Context;LX/74w;)V

    .line 2122615
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 2122616
    :goto_0
    invoke-virtual {v0, p2}, LX/ESH;->a(Landroid/view/View;)V

    .line 2122617
    sget-object v1, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2122618
    iget-object v3, v0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2122619
    const/16 v1, 0x8

    .line 2122620
    iget-object v3, v0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0xff

    invoke-virtual {v3, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(I)V

    .line 2122621
    iget-object v3, v0, LX/ESH;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122622
    iget-object v3, v0, LX/ESH;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122623
    iget-object v3, v0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2122624
    invoke-virtual {v0}, LX/ESH;->a()V

    .line 2122625
    return-object p2

    .line 2122626
    :cond_1
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESH;

    goto :goto_0
.end method
