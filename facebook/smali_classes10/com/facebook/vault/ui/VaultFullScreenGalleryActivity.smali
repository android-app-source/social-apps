.class public Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/Duv;

.field private q:J

.field public r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/base/photos/VaultPhoto;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

.field private final t:LX/ESF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2122703
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2122704
    new-instance v0, LX/ESF;

    invoke-direct {v0, p0}, LX/ESF;-><init>(Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;)V

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->t:LX/ESF;

    return-void
.end method

.method private a()V
    .locals 13

    .prologue
    .line 2122710
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    .line 2122711
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    move v0, v1

    .line 2122712
    if-eqz v0, :cond_1

    .line 2122713
    :cond_0
    :goto_0
    return-void

    .line 2122714
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2122715
    new-instance v1, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    invoke-direct {v1}, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;-><init>()V

    iput-object v1, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    .line 2122716
    new-instance v1, LX/Duv;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->r:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, LX/Duv;-><init>(Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->p:LX/Duv;

    .line 2122717
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->p:LX/Duv;

    iget-wide v2, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->q:J

    .line 2122718
    const/4 v6, 0x0

    move v7, v6

    :goto_1
    iget-object v6, v1, LX/Duv;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_3

    .line 2122719
    iget-object v6, v1, LX/Duv;->a:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122720
    iget-wide v11, v6, LX/74w;->a:J

    move-wide v8, v11

    .line 2122721
    cmp-long v6, v8, v2

    if-nez v6, :cond_2

    .line 2122722
    :goto_2
    move v1, v7

    .line 2122723
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    iget-object v3, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->p:LX/Duv;

    new-instance v4, LX/9iR;

    invoke-direct {v4}, LX/9iR;-><init>()V

    sget-object v5, LX/74S;->TIMELINE_PHOTOS_SYNCED:LX/74S;

    .line 2122724
    iput v1, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->b:I

    .line 2122725
    iput-object v3, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->c:LX/Duv;

    .line 2122726
    iput-object v4, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->d:LX/9iR;

    .line 2122727
    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->f:Z

    .line 2122728
    iput-object v5, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->e:LX/74S;

    .line 2122729
    const-wide/16 v6, -0x1

    iput-wide v6, v2, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->k:J

    .line 2122730
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->t:LX/ESF;

    .line 2122731
    iput-object v2, v1, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;->c:LX/ESF;

    .line 2122732
    invoke-static {p0}, LX/18w;->b(Landroid/content/Context;)I

    move-result v1

    .line 2122733
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->s:Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    const-class v4, Lcom/facebook/vault/gallery/VaultPhotoGalleryFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2122734
    invoke-virtual {v0}, LX/0gc;->b()Z

    goto :goto_0

    .line 2122735
    :cond_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 2122736
    :cond_3
    const/4 v7, -0x1

    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2122705
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2122706
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selectedPhotoIdParam"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->q:J

    .line 2122707
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photoObjectsParam"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->r:Ljava/util/ArrayList;

    .line 2122708
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->a()V

    .line 2122709
    return-void
.end method
