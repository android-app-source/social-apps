.class public Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/0ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2TI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/TextView;

.field private h:LX/ESP;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2122996
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2122997
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    .line 2122998
    sget-object v0, LX/ESP;->INVISIBLE:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    .line 2122999
    const-class v0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    invoke-static {v0, p0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2123000
    return-void
.end method

.method private static a(Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;LX/0ka;LX/2TI;LX/2TK;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2122995
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a:LX/0ka;

    iput-object p2, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->b:LX/2TI;

    iput-object p3, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->c:LX/2TK;

    iput-object p4, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->d:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    invoke-static {v3}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v0

    check-cast v0, LX/0ka;

    invoke-static {v3}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v1

    check-cast v1, LX/2TI;

    invoke-static {v3}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v2

    check-cast v2, LX/2TK;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a(Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;LX/0ka;LX/2TI;LX/2TK;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2123001
    sget-object v0, LX/ESO;->a:[I

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    invoke-virtual {v1}, LX/ESP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2123002
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setVisibility(I)V

    .line 2123003
    :goto_0
    return-void

    .line 2123004
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->f:Landroid/widget/ImageView;

    const v1, 0x7f021a17

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2123005
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    const v2, 0x7f0811c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2123006
    invoke-virtual {p0, v3}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setVisibility(I)V

    goto :goto_0

    .line 2123007
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->f:Landroid/widget/ImageView;

    const v1, 0x7f021a1f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2123008
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    const v2, 0x7f0811c2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2123009
    invoke-virtual {p0, v3}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setVisibility(I)V

    goto :goto_0

    .line 2123010
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->f:Landroid/widget/ImageView;

    const v1, 0x7f020a7a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2123011
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    const v2, 0x7f0811c1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2123012
    invoke-virtual {p0, v3}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setVisibility(I)V

    goto :goto_0

    .line 2123013
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->f:Landroid/widget/ImageView;

    const v1, 0x7f021a11

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2123014
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    const v2, 0x7f0811c4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2123015
    invoke-virtual {p0, v3}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2122983
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->b:LX/2TI;

    invoke-virtual {v0}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v0

    .line 2122984
    const-string v1, "MOBILE_RADIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WIFI_ONLY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    invoke-static {v1}, LX/ERe;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2122985
    sget-object v0, LX/ESP;->NO_INTERNET:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    .line 2122986
    :goto_0
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->b()V

    .line 2122987
    return-void

    .line 2122988
    :cond_1
    const-string v1, "WIFI_ONLY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2122989
    sget-object v0, LX/ESP;->REQUIRES_WIFI:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    goto :goto_0

    .line 2122990
    :cond_2
    const-string v1, "OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2122991
    sget-object v0, LX/ESP;->SYNC_IS_OFF:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    goto :goto_0

    .line 2122992
    :cond_3
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->c:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2122993
    sget-object v0, LX/ESP;->LOW_BATTERY:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    goto :goto_0

    .line 2122994
    :cond_4
    sget-object v0, LX/ESP;->INVISIBLE:LX/ESP;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    goto :goto_0
.end method

.method public onClick()V
    .locals 3

    .prologue
    .line 2122979
    sget-object v0, LX/ESO;->a:[I

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->h:LX/ESP;

    invoke-virtual {v1}, LX/ESP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2122980
    :goto_0
    return-void

    .line 2122981
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2122982
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->e:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x25164abc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122976
    const v0, 0x7f0d304b

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->f:Landroid/widget/ImageView;

    .line 2122977
    const v0, 0x7f0d304c

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->g:Landroid/widget/TextView;

    .line 2122978
    const/16 v0, 0x2d

    const v2, -0x1b011100

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
