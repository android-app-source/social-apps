.class public Lcom/facebook/vault/ui/VaultSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# static fields
.field public static final x:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Landroid/widget/TextView;

.field public B:Landroid/widget/TextView;

.field public C:Landroid/widget/TextView;

.field public p:LX/ERK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2TL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2TI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/ERh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/2TH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/ERv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/CheckBox;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2122897
    const-class v0, Lcom/facebook/vault/ui/VaultSettingsActivity;

    sput-object v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->x:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2122907
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/vault/ui/VaultSettingsActivity;LX/ERK;LX/2TL;LX/2TI;LX/ERh;LX/2TK;LX/2TH;Ljava/util/concurrent/ScheduledExecutorService;LX/ERv;)V
    .locals 0

    .prologue
    .line 2122906
    iput-object p1, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->p:LX/ERK;

    iput-object p2, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->q:LX/2TL;

    iput-object p3, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->r:LX/2TI;

    iput-object p4, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->s:LX/ERh;

    iput-object p5, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->t:LX/2TK;

    iput-object p6, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->u:LX/2TH;

    iput-object p7, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->v:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p8, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->w:LX/ERv;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-static {v8}, LX/ERK;->a(LX/0QB;)LX/ERK;

    move-result-object v1

    check-cast v1, LX/ERK;

    invoke-static {v8}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v2

    check-cast v2, LX/2TL;

    invoke-static {v8}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v3

    check-cast v3, LX/2TI;

    invoke-static {v8}, LX/ERh;->b(LX/0QB;)LX/ERh;

    move-result-object v4

    check-cast v4, LX/ERh;

    invoke-static {v8}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v5

    check-cast v5, LX/2TK;

    invoke-static {v8}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object v6

    check-cast v6, LX/2TH;

    invoke-static {v8}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v8}, LX/ERv;->a(LX/0QB;)LX/ERv;

    move-result-object v8

    check-cast v8, LX/ERv;

    invoke-static/range {v0 .. v8}, Lcom/facebook/vault/ui/VaultSettingsActivity;->a(Lcom/facebook/vault/ui/VaultSettingsActivity;LX/ERK;LX/2TL;LX/2TI;LX/ERh;LX/2TK;LX/2TH;Ljava/util/concurrent/ScheduledExecutorService;LX/ERv;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2122898
    const-string v0, "MOBILE_RADIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2122899
    const v0, 0x7f0d3054

    .line 2122900
    :goto_0
    return v0

    .line 2122901
    :cond_0
    const-string v0, "WIFI_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2122902
    const v0, 0x7f0d3055

    goto :goto_0

    .line 2122903
    :cond_1
    const-string v0, "OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2122904
    const v0, 0x7f0d3056

    goto :goto_0

    .line 2122905
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown sync mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2122889
    const v0, 0x7f0d3054

    if-ne p0, v0, :cond_0

    .line 2122890
    const-string v0, "MOBILE_RADIO"

    .line 2122891
    :goto_0
    return-object v0

    .line 2122892
    :cond_0
    const v0, 0x7f0d3055

    if-ne p0, v0, :cond_1

    .line 2122893
    const-string v0, "WIFI_ONLY"

    goto :goto_0

    .line 2122894
    :cond_1
    const v0, 0x7f0d3056

    if-ne p0, v0, :cond_2

    .line 2122895
    const-string v0, "OFF"

    goto :goto_0

    .line 2122896
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(JLandroid/content/Context;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/high16 v10, 0x4090000000000000L    # 1024.0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2122882
    const/4 v0, 0x6

    new-array v0, v0, [I

    const v1, 0x7f0811cc

    aput v1, v0, v8

    const v1, 0x7f0811cd

    aput v1, v0, v9

    const/4 v1, 0x2

    const v2, 0x7f0811ce

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0811cf

    aput v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f0811d0

    aput v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x7f0811d1

    aput v2, v0, v1

    .line 2122883
    const-wide/16 v2, 0x400

    cmp-long v1, p0, v2

    if-gez v1, :cond_0

    .line 2122884
    const v0, 0x7f0811cb

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2122885
    :goto_0
    return-object v0

    .line 2122886
    :cond_0
    long-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    invoke-static {v10, v11}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-int v1, v2

    .line 2122887
    const-string v2, "%.1f"

    new-array v3, v9, [Ljava/lang/Object;

    long-to-double v4, p0

    int-to-double v6, v1

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2122888
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    new-array v1, v9, [Ljava/lang/Object;

    aput-object v2, v1, v8

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/vault/ui/VaultSettingsActivity;)V
    .locals 3

    .prologue
    .line 2122908
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->r:LX/2TI;

    invoke-virtual {v0}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v1

    .line 2122909
    invoke-static {v1}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(Ljava/lang/String;)I

    move-result v0

    .line 2122910
    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2122911
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2122912
    const-string v0, "OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2122913
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->y:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2122914
    :goto_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->p:LX/ERK;

    invoke-virtual {v0}, LX/ERK;->a()Z

    move-result v0

    .line 2122915
    invoke-static {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(Lcom/facebook/vault/ui/VaultSettingsActivity;Z)V

    .line 2122916
    return-void

    .line 2122917
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->y:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/vault/ui/VaultSettingsActivity;Z)V
    .locals 2

    .prologue
    .line 2122877
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2122878
    if-eqz p1, :cond_0

    .line 2122879
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->A:Landroid/widget/TextView;

    const v1, 0x7f0811b0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2122880
    :goto_0
    return-void

    .line 2122881
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->A:Landroid/widget/TextView;

    const v1, 0x7f0811af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/vault/ui/VaultSettingsActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2122874
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->r:LX/2TI;

    invoke-virtual {v0, p1}, LX/2TI;->a(Ljava/lang/String;)V

    .line 2122875
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->u:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->a()V

    .line 2122876
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    .line 2122828
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->w:LX/ERv;

    .line 2122829
    invoke-static {}, LX/6Wo;->a()LX/6We;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [LX/6Wc;

    const/4 v3, 0x0

    sget-object v4, LX/ERv;->a:LX/6Wc;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v1

    sget-object v2, LX/6Wn;->USER:LX/6Wn;

    invoke-virtual {v1, v2}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v1

    sget-object v2, LX/ERv;->b:LX/6Wc;

    .line 2122830
    new-instance v3, LX/6Wa;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = me()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6Wa;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 2122831
    invoke-virtual {v1, v2}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v1

    .line 2122832
    iget-object v2, v0, LX/ERv;->d:LX/0TD;

    new-instance v3, Lcom/facebook/vault/service/VaultQuotaClient$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/vault/service/VaultQuotaClient$1;-><init>(LX/ERv;LX/6Wd;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2122833
    new-instance v1, LX/ESJ;

    invoke-direct {v1, p0}, LX/ESJ;-><init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->v:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2122834
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    .line 2122861
    new-instance v0, LX/ESK;

    invoke-direct {v0, p0}, LX/ESK;-><init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    .line 2122862
    new-instance v1, LX/ESL;

    invoke-direct {v1, p0}, LX/ESL;-><init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    .line 2122863
    new-instance v2, LX/ESM;

    invoke-direct {v2, p0}, LX/ESM;-><init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    .line 2122864
    new-instance v3, LX/0ju;

    invoke-direct {v3, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2122865
    invoke-virtual {p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x108009b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0ju;->a(Landroid/graphics/drawable/Drawable;)LX/0ju;

    .line 2122866
    const v4, 0x7f0811b7

    invoke-virtual {p0, v4}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2122867
    const v4, 0x7f0811b8

    invoke-virtual {p0, v4}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2122868
    const v0, 0x7f0811b9

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2122869
    invoke-virtual {v3, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 2122870
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LX/0ju;->a(Z)LX/0ju;

    .line 2122871
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2122872
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2122873
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2122847
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2122848
    const v0, 0x7f031579

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->setContentView(I)V

    .line 2122849
    const v0, 0x7f0d3059

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->A:Landroid/widget/TextView;

    .line 2122850
    const v0, 0x7f0d3057

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->y:Landroid/widget/LinearLayout;

    .line 2122851
    const v0, 0x7f0d3058

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->z:Landroid/widget/CheckBox;

    .line 2122852
    const v0, 0x7f0d305a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->B:Landroid/widget/TextView;

    .line 2122853
    const v0, 0x7f0d305b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->C:Landroid/widget/TextView;

    .line 2122854
    invoke-static {p0, p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2122855
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->z:Landroid/widget/CheckBox;

    new-instance v1, LX/ESI;

    invoke-direct {v1, p0}, LX/ESI;-><init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2122856
    invoke-static {p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    .line 2122857
    const v0, 0x7f0d3053

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2122858
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2122859
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->l()V

    .line 2122860
    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    .prologue
    .line 2122841
    invoke-static {p2}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 2122842
    const-string v1, "OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2122843
    invoke-direct {p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->m()V

    .line 2122844
    :goto_0
    return-void

    .line 2122845
    :cond_0
    invoke-static {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->c(Lcom/facebook/vault/ui/VaultSettingsActivity;Ljava/lang/String;)V

    .line 2122846
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSettingsActivity;->y:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2f5e2b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122835
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2122836
    const v0, 0x7f0d3053

    invoke-virtual {p0, v0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2122837
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2122838
    invoke-static {p0}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(Lcom/facebook/vault/ui/VaultSettingsActivity;)V

    .line 2122839
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2122840
    const/16 v0, 0x23

    const v2, -0x2c472ab0

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
