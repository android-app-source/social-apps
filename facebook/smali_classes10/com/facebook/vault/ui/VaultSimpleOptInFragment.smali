.class public Lcom/facebook/vault/ui/VaultSimpleOptInFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:LX/2TI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2TL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ERr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2TM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2TH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/ESU;

.field private l:Landroid/widget/Button;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2122967
    const-class v0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2122964
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2122965
    const v0, 0x7f031573

    iput v0, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->b:I

    .line 2122966
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2122961
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2122962
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-static {v0}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v3

    check-cast v3, LX/2TI;

    invoke-static {v0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v4

    check-cast v4, LX/2TL;

    invoke-static {v0}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v5

    check-cast v5, LX/ERr;

    invoke-static {v0}, LX/2TM;->c(LX/0QB;)LX/2TM;

    move-result-object v6

    check-cast v6, LX/2TM;

    invoke-static {v0}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object v7

    check-cast v7, LX/2TH;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p1

    check-cast p1, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->c:LX/2TI;

    iput-object v4, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->d:LX/2TL;

    iput-object v5, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->e:LX/ERr;

    iput-object v6, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->f:LX/2TM;

    iput-object v7, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->g:LX/2TH;

    iput-object p1, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->h:LX/17Y;

    iput-object v0, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->i:Lcom/facebook/content/SecureContextHelper;

    .line 2122963
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f7e099

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122928
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2122929
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2122930
    const v2, 0x7f0d3042

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->l:Landroid/widget/Button;

    .line 2122931
    iget-object v0, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2122932
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2122933
    const v2, 0x7f0d3044

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/ESN;

    invoke-direct {v2, p0}, LX/ESN;-><init>(Lcom/facebook/vault/ui/VaultSimpleOptInFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2122934
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<b><u>"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0811bf

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</u></b>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2122935
    const v2, 0x7f0811be

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2122936
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2122937
    const v3, 0x7f0d3041

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2122938
    const/16 v0, 0x2b

    const v2, 0x1ad6774d

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x3dd7f53b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122940
    const-string v1, "MOBILE_RADIO"

    move-object v1, v1

    .line 2122941
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0d3042

    if-ne v2, v3, :cond_1

    .line 2122942
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->f:LX/2TM;

    .line 2122943
    const-string v3, "OFF"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2122944
    sget-object v3, LX/2TM;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2122945
    :goto_0
    const-string v4, "mode"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2122946
    iget-object v4, v2, LX/2TM;->y:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2122947
    iget-object v2, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->c:LX/2TI;

    invoke-virtual {v2, v1}, LX/2TI;->a(Ljava/lang/String;)V

    .line 2122948
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->g:LX/2TH;

    invoke-virtual {v1}, LX/2TH;->a()V

    .line 2122949
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->l:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0489

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 2122950
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->l:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2122951
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->d:LX/2TL;

    invoke-virtual {v1}, LX/2TL;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2122952
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->e:LX/ERr;

    .line 2122953
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "vault.table_refreshed_key"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 2122954
    invoke-static {v1, v2}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2122955
    :cond_0
    iget-object v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->k:LX/ESU;

    .line 2122956
    iget-object v2, v1, LX/ESU;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-static {v2}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->c(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    .line 2122957
    iget-object v2, v1, LX/ESU;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-static {v2}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    .line 2122958
    iget-object v2, v1, LX/ESU;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    invoke-virtual {v2}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a()V

    .line 2122959
    :cond_1
    const v1, 0x37ff43d1

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2122960
    :cond_2
    sget-object v3, LX/2TM;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x417e5ca5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122939
    iget v1, p0, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->b:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x20ae1541

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method
