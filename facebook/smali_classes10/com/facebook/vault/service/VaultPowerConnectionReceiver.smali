.class public Lcom/facebook/vault/service/VaultPowerConnectionReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121800
    const-class v0, Lcom/facebook/vault/service/VaultPowerConnectionReceiver;

    sput-object v0, Lcom/facebook/vault/service/VaultPowerConnectionReceiver;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2121801
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2121802
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x913082

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2121803
    new-instance v1, LX/ERs;

    invoke-direct {v1, p1}, LX/ERs;-><init>(Landroid/content/Context;)V

    .line 2121804
    invoke-static {v1}, LX/ERs;->a(LX/ERs;)LX/0Uq;

    move-result-object v2

    invoke-virtual {v2}, LX/0Uq;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2121805
    invoke-static {v1}, LX/ERs;->b(LX/ERs;)LX/2TK;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, LX/2TK;->c(I)V

    .line 2121806
    invoke-static {v1}, LX/ERs;->c(LX/ERs;)LX/ERr;

    move-result-object v1

    invoke-virtual {v1}, LX/ERr;->b()V

    .line 2121807
    :cond_0
    const/16 v1, 0x27

    const v2, -0x376a0a12

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
