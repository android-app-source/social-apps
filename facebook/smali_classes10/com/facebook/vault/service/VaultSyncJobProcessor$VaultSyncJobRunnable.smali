.class public final Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Lcom/facebook/vault/provider/VaultImageProviderRow;

.field public b:I

.field public final synthetic c:Lcom/facebook/vault/service/VaultSyncJobProcessor;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;Lcom/facebook/vault/provider/VaultImageProviderRow;I)V
    .locals 0

    .prologue
    .line 2121927
    iput-object p1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121928
    iput-object p2, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->a:Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2121929
    iput p3, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->b:I

    .line 2121930
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2121931
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->i:LX/ES8;

    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->a:Lcom/facebook/vault/provider/VaultImageProviderRow;

    iget-object v1, v1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/ES8;->a(Ljava/lang/String;)Lcom/facebook/vault/provider/VaultImageProviderRow;

    move-result-object v0

    .line 2121932
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 2121933
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERr;

    .line 2121934
    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->a:Lcom/facebook/vault/provider/VaultImageProviderRow;

    iget-object v1, v1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/ERr;->a(Ljava/lang/String;)V

    .line 2121935
    :cond_1
    :goto_0
    return-void

    .line 2121936
    :cond_2
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->j:LX/ERq;

    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->a:Lcom/facebook/vault/provider/VaultImageProviderRow;

    iget v2, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->b:I

    invoke-virtual {v0, v1, v2}, LX/ERq;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)Z

    move-result v0

    .line 2121937
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    sget-object v1, LX/ES2;->RESET_RETRY:LX/ES2;

    if-eq v0, v1, :cond_1

    .line 2121938
    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->c:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;->b:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    sget-object v0, LX/ES2;->MAINTAIN_RETRY:LX/ES2;

    .line 2121939
    :goto_1
    iput-object v0, v1, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    .line 2121940
    goto :goto_0

    :cond_3
    sget-object v0, LX/ES2;->RESET_RETRY:LX/ES2;

    goto :goto_1
.end method
