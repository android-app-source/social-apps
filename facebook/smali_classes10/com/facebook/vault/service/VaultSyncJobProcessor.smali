.class public Lcom/facebook/vault/service/VaultSyncJobProcessor;
.super LX/0te;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final d:Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zr;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/os/Looper;

.field private f:Landroid/os/Handler;

.field private g:Landroid/content/Context;

.field private h:LX/ERJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/ES8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/ERq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/2TK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ERr;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/ES2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2122001
    const-class v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;

    .line 2122002
    sput-object v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->c:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2121996
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2121997
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2121998
    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->l:LX/0Ot;

    .line 2121999
    sget-object v0, LX/ES2;->NO_RETRY:LX/ES2;

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    .line 2122000
    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 2121984
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->k:LX/2TK;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/2TK;->b(I)Landroid/content/Intent;

    move-result-object v4

    .line 2121985
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->h:LX/ERJ;

    .line 2121986
    iget-object v7, v0, LX/ERJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/2TR;->e:LX/0Tn;

    const-wide/32 v9, 0x1d4c0

    invoke-interface {v7, v8, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v7

    move-wide v0, v7

    .line 2121987
    if-eqz p2, :cond_0

    .line 2121988
    const-wide/32 v0, 0x1d4c0

    move-wide v2, v0

    .line 2121989
    :goto_0
    invoke-static {p1, v6, v4, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2121990
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12x;

    .line 2121991
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v2

    .line 2121992
    invoke-virtual {v0, v6, v4, v5, v1}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 2121993
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scheduling retry with interval: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2121994
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->h:LX/ERJ;

    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x2932e00

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/ERJ;->a(J)V

    .line 2121995
    return-void

    :cond_0
    move-wide v2, v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V
    .locals 3

    .prologue
    .line 2121982
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;-><init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    const v2, 0x716a60e5

    invoke-static {v0, v1, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2121983
    return-void
.end method

.method private static a(Lcom/facebook/vault/service/VaultSyncJobProcessor;LX/0Or;LX/0Or;LX/ERJ;LX/ES8;LX/ERq;LX/2TK;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/vault/service/VaultSyncJobProcessor;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Zr;",
            ">;",
            "LX/ERJ;",
            "LX/ES8;",
            "LX/ERq;",
            "LX/2TK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/ERr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121981
    iput-object p1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->h:LX/ERJ;

    iput-object p4, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->i:LX/ES8;

    iput-object p5, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->j:LX/ERq;

    iput-object p6, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->k:LX/2TK;

    iput-object p7, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->l:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->m:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;

    const/16 v1, 0x235

    invoke-static {v8, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x274

    invoke-static {v8, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v8}, LX/ERJ;->a(LX/0QB;)LX/ERJ;

    move-result-object v3

    check-cast v3, LX/ERJ;

    invoke-static {v8}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v4

    check-cast v4, LX/ES8;

    invoke-static {v8}, LX/ERq;->b(LX/0QB;)LX/ERq;

    move-result-object v5

    check-cast v5, LX/ERq;

    invoke-static {v8}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v6

    check-cast v6, LX/2TK;

    const/16 v7, 0x259

    invoke-static {v8, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v9, 0x37a9

    invoke-static {v8, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a(Lcom/facebook/vault/service/VaultSyncJobProcessor;LX/0Or;LX/0Or;LX/ERJ;LX/ES8;LX/ERq;LX/2TK;LX/0Ot;LX/0Or;)V

    return-void
.end method

.method private b(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V
    .locals 3

    .prologue
    .line 2121979
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/vault/service/VaultSyncJobProcessor$VaultSyncJobRunnable;-><init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    const v2, 0x6546acf0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2121980
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2121941
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x3990c083

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2121968
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2121969
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2121970
    iput-object p0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->g:Landroid/content/Context;

    .line 2121971
    invoke-static {p0, p0}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2121972
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zr;

    .line 2121973
    const-string v2, "vault_sync_job_processor"

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 2121974
    new-instance v2, LX/ES0;

    invoke-direct {v2, p0}, LX/ES0;-><init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;)V

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 2121975
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2121976
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->e:Landroid/os/Looper;

    .line 2121977
    new-instance v0, LX/ES1;

    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->e:Landroid/os/Looper;

    invoke-direct {v0, p0, v2}, LX/ES1;-><init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->f:Landroid/os/Handler;

    .line 2121978
    const/16 v0, 0x25

    const v2, -0x37e97cb

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x774d8b72

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2121960
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2121961
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->e:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 2121962
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->k:LX/2TK;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, LX/2TK;->c(I)V

    .line 2121963
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    sget-object v2, LX/ES2;->NO_RETRY:LX/ES2;

    if-eq v0, v2, :cond_0

    .line 2121964
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->g:Landroid/content/Context;

    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    sget-object v3, LX/ES2;->RESET_RETRY:LX/ES2;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a(Landroid/content/Context;Z)V

    .line 2121965
    sget-object v0, LX/ES2;->NO_RETRY:LX/ES2;

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->n:LX/ES2;

    .line 2121966
    :cond_0
    const v0, 0x6de58e9d

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 2121967
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/16 v0, 0x24

    const v1, -0x1f2d1b20

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2121942
    if-nez p1, :cond_0

    .line 2121943
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "VaultSyncJobProcessor onStartCommand"

    const-string v2, "started with null intent"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121944
    const/16 v0, 0x25

    const v1, -0x55788e34

    invoke-static {v8, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2121945
    :goto_0
    return v8

    .line 2121946
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting VaultSyncJobProcessor Service with startId "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2121947
    const-string v0, "sync_reason"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2121948
    const-string v0, "queuing_type"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 2121949
    const-string v0, "queuing_objects"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2121950
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2121951
    if-ne v5, v9, :cond_1

    .line 2121952
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2121953
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2121954
    if-ne v5, v8, :cond_2

    .line 2121955
    invoke-direct {p0, v0, v4}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->b(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    .line 2121956
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2121957
    :cond_2
    invoke-direct {p0, v0, v4}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    goto :goto_2

    .line 2121958
    :cond_3
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->f:Landroid/os/Handler;

    invoke-static {v1, v9, p3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2121959
    :cond_4
    const v0, 0x7a75d7c2

    invoke-static {v0, v3}, LX/02F;->d(II)V

    goto :goto_0
.end method
