.class public Lcom/facebook/vault/service/VaultSyncService;
.super LX/0te;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zr;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Looper;

.field private d:Landroid/os/Handler;

.field private e:LX/2TM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:J

.field private g:LX/ES8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:LX/ERr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/ERh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:LX/ERz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/2TN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private l:LX/ERJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/ERK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private n:LX/2TL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private o:LX/ERI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:LX/2TK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2122104
    const-class v0, Lcom/facebook/vault/service/VaultSyncService;

    sput-object v0, Lcom/facebook/vault/service/VaultSyncService;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2122105
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2122106
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2122107
    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->q:LX/0Ot;

    .line 2122108
    return-void
.end method

.method private static a(I)J
    .locals 2

    .prologue
    .line 2122132
    if-nez p0, :cond_0

    .line 2122133
    const-wide/16 v0, 0x7530

    .line 2122134
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 2122109
    if-lez p1, :cond_0

    .line 2122110
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->e:LX/2TM;

    .line 2122111
    sget-object v1, LX/2TM;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2122112
    packed-switch p2, :pswitch_data_0

    .line 2122113
    iget-object v2, v0, LX/2TM;->x:LX/03V;

    const-string v3, "vault_logger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "unknown sync reason: "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122114
    :goto_0
    const-string v2, "images_queued"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2122115
    iget-object v2, v0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2122116
    :cond_0
    return-void

    .line 2122117
    :pswitch_0
    const-string v2, "reason"

    sget-object v3, LX/2TM;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122118
    :pswitch_1
    const-string v2, "reason"

    sget-object v3, LX/2TM;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122119
    :pswitch_2
    const-string v2, "reason"

    sget-object v3, LX/2TM;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122120
    :pswitch_3
    const-string v2, "reason"

    sget-object v3, LX/2TM;->p:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122121
    :pswitch_4
    const-string v2, "reason"

    sget-object v3, LX/2TM;->q:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122122
    :pswitch_5
    const-string v2, "reason"

    sget-object v3, LX/2TM;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122123
    :pswitch_6
    const-string v2, "reason"

    sget-object v3, LX/2TM;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122124
    :pswitch_7
    const-string v2, "reason"

    sget-object v3, LX/2TM;->s:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122125
    :pswitch_8
    const-string v2, "reason"

    sget-object v3, LX/2TM;->t:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122126
    :pswitch_9
    const-string v2, "reason"

    const-string v3, "power_plugged_in"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122127
    :pswitch_a
    const-string v2, "reason"

    const-string v3, "sync_older_photos"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2122128
    const-string v2, "library_size"

    iget-object v3, v0, LX/2TM;->w:LX/2TN;

    invoke-virtual {v3}, LX/2TN;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122129
    :pswitch_b
    const-string v2, "reason"

    const-string v3, "retry_by_hashes"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122130
    :pswitch_c
    const-string v2, "reason"

    const-string v3, "pull_to_refresh"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2122131
    :pswitch_d
    const-string v2, "reason"

    const-string v3, "looking_at_sync_tab"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static a(Lcom/facebook/vault/service/VaultSyncService;LX/0Or;LX/2TM;LX/ES8;LX/ERr;LX/ERh;LX/ERz;LX/2TN;LX/ERJ;LX/ERK;LX/2TL;LX/ERI;LX/2TK;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/vault/service/VaultSyncService;",
            "LX/0Or",
            "<",
            "LX/0Zr;",
            ">;",
            "LX/2TM;",
            "LX/ES8;",
            "LX/ERr;",
            "LX/ERh;",
            "LX/ERz;",
            "LX/2TN;",
            "LX/ERJ;",
            "LX/ERK;",
            "LX/2TL;",
            "LX/ERI;",
            "LX/2TK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2122053
    iput-object p1, p0, Lcom/facebook/vault/service/VaultSyncService;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/vault/service/VaultSyncService;->e:LX/2TM;

    iput-object p3, p0, Lcom/facebook/vault/service/VaultSyncService;->g:LX/ES8;

    iput-object p4, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    iput-object p5, p0, Lcom/facebook/vault/service/VaultSyncService;->i:LX/ERh;

    iput-object p6, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    iput-object p7, p0, Lcom/facebook/vault/service/VaultSyncService;->k:LX/2TN;

    iput-object p8, p0, Lcom/facebook/vault/service/VaultSyncService;->l:LX/ERJ;

    iput-object p9, p0, Lcom/facebook/vault/service/VaultSyncService;->m:LX/ERK;

    iput-object p10, p0, Lcom/facebook/vault/service/VaultSyncService;->n:LX/2TL;

    iput-object p11, p0, Lcom/facebook/vault/service/VaultSyncService;->o:LX/ERI;

    iput-object p12, p0, Lcom/facebook/vault/service/VaultSyncService;->p:LX/2TK;

    iput-object p13, p0, Lcom/facebook/vault/service/VaultSyncService;->q:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/vault/service/VaultSyncService;Landroid/content/Intent;I)V
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 2122054
    :try_start_0
    const-string v1, "sync_reason"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2122055
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->k:LX/2TN;

    .line 2122056
    sget-object v11, LX/ERm;->MIN:LX/ERm;

    invoke-static {v2, v11}, LX/2TN;->a(LX/2TN;LX/ERm;)J

    move-result-wide v11

    move-wide v7, v11

    .line 2122057
    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-nez v7, :cond_8

    const/4 v7, 0x1

    :goto_0
    move v2, v7

    .line 2122058
    if-eqz v2, :cond_0

    .line 2122059
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    invoke-virtual {v0}, LX/ERr;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2122060
    invoke-virtual {p0, p2}, Lcom/facebook/vault/service/VaultSyncService;->stopSelfResult(I)Z

    .line 2122061
    :goto_1
    return-void

    .line 2122062
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->p:LX/2TK;

    invoke-virtual {v2, v1}, LX/2TK;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2122063
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    invoke-virtual {v0}, LX/ERr;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2122064
    invoke-virtual {p0, p2}, Lcom/facebook/vault/service/VaultSyncService;->stopSelfResult(I)Z

    goto :goto_1

    .line 2122065
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->g:LX/ES8;

    const-wide/32 v4, 0x1b7740

    invoke-virtual {v2, v4, v5}, LX/ES8;->a(J)I

    .line 2122066
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->g:LX/ES8;

    invoke-virtual {v2}, LX/ES8;->b()Ljava/util/Set;

    move-result-object v2

    .line 2122067
    iget-object v3, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    invoke-virtual {v3, v2}, LX/ERr;->a(Ljava/util/Set;)V

    .line 2122068
    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    .line 2122069
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 2122070
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    const/16 v2, 0x32

    .line 2122071
    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/ERz;->a(LX/ERz;IZ)I

    move-result v3

    move v0, v3

    .line 2122072
    if-nez v0, :cond_2

    .line 2122073
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->l:LX/ERJ;

    const-wide/32 v4, 0x1d4c0

    invoke-virtual {v2, v4, v5}, LX/ERJ;->a(J)V

    .line 2122074
    :cond_2
    :goto_2
    if-nez v0, :cond_7

    .line 2122075
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    invoke-virtual {v0}, LX/ERr;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2122076
    :goto_3
    invoke-virtual {p0, p2}, Lcom/facebook/vault/service/VaultSyncService;->stopSelfResult(I)Z

    goto :goto_1

    .line 2122077
    :cond_3
    const/16 v2, 0xb

    if-ne v1, v2, :cond_4

    .line 2122078
    :try_start_3
    const-string v2, "photo_hashes"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2122079
    if-eqz v2, :cond_2

    .line 2122080
    new-instance v0, Ljava/util/HashSet;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2122081
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    .line 2122082
    iget-object v3, v2, LX/ERz;->d:LX/2TK;

    invoke-virtual {v3}, LX/2TK;->c()Z

    .line 2122083
    iget-object v3, v2, LX/ERz;->c:LX/ES8;

    invoke-virtual {v3, v0}, LX/ES8;->a(Ljava/util/Set;)Ljava/util/List;

    move-result-object v3

    .line 2122084
    const/4 v4, 0x1

    invoke-static {v2, v3, v1, v4}, LX/ERz;->a(LX/ERz;Ljava/util/List;II)V

    .line 2122085
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v0, v3

    .line 2122086
    goto :goto_2

    .line 2122087
    :cond_4
    const/16 v0, 0xc

    if-ne v1, v0, :cond_5

    .line 2122088
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    const/16 v2, 0x32

    invoke-virtual {v0, v2, v1}, LX/ERz;->a(II)I

    move-result v0

    .line 2122089
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    const/4 v3, 0x0

    rsub-int/lit8 v4, v0, 0x32

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2122090
    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, LX/ERz;->a(LX/ERz;IZ)I

    move-result v4

    move v2, v4

    .line 2122091
    add-int/2addr v0, v2

    goto :goto_2

    .line 2122092
    :cond_5
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    const/16 v2, 0x32

    invoke-virtual {v0, v2, v1}, LX/ERz;->a(II)I

    move-result v0

    .line 2122093
    if-nez v0, :cond_2

    .line 2122094
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->m:LX/ERK;

    invoke-virtual {v2}, LX/ERK;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->g:LX/ES8;

    invoke-virtual {v2}, LX/ES8;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2122095
    :cond_6
    invoke-direct {p0}, Lcom/facebook/vault/service/VaultSyncService;->b()I

    move-result v2

    .line 2122096
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->j:LX/ERz;

    const/16 v3, 0x32

    const/16 v4, 0xa

    invoke-virtual {v0, v3, v4}, LX/ERz;->a(II)I

    move-result v0

    .line 2122097
    if-lez v2, :cond_2

    if-eqz v0, :cond_6

    goto :goto_2

    .line 2122098
    :cond_7
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->n:LX/2TL;

    invoke-virtual {v2}, LX/2TL;->a()J

    move-result-wide v2

    .line 2122099
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2122100
    iget-object v6, p0, Lcom/facebook/vault/service/VaultSyncService;->o:LX/ERI;

    invoke-virtual {v6, v4, v5}, LX/ERI;->a(J)V

    .line 2122101
    iget-object v6, p0, Lcom/facebook/vault/service/VaultSyncService;->i:LX/ERh;

    invoke-virtual {v6, v2, v3, v4, v5}, LX/ERh;->a(JJ)V

    .line 2122102
    invoke-direct {p0, v0, v1}, Lcom/facebook/vault/service/VaultSyncService;->a(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 2122103
    :catchall_0
    move-exception v0

    invoke-virtual {p0, p2}, Lcom/facebook/vault/service/VaultSyncService;->stopSelfResult(I)Z

    throw v0

    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/vault/service/VaultSyncService;

    const/16 v1, 0x274

    invoke-static {v13, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v13}, LX/2TM;->c(LX/0QB;)LX/2TM;

    move-result-object v2

    check-cast v2, LX/2TM;

    invoke-static {v13}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v3

    check-cast v3, LX/ES8;

    invoke-static {v13}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v4

    check-cast v4, LX/ERr;

    invoke-static {v13}, LX/ERh;->b(LX/0QB;)LX/ERh;

    move-result-object v5

    check-cast v5, LX/ERh;

    invoke-static {v13}, LX/ERz;->b(LX/0QB;)LX/ERz;

    move-result-object v6

    check-cast v6, LX/ERz;

    invoke-static {v13}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v7

    check-cast v7, LX/2TN;

    invoke-static {v13}, LX/ERJ;->a(LX/0QB;)LX/ERJ;

    move-result-object v8

    check-cast v8, LX/ERJ;

    invoke-static {v13}, LX/ERK;->a(LX/0QB;)LX/ERK;

    move-result-object v9

    check-cast v9, LX/ERK;

    invoke-static {v13}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v10

    check-cast v10, LX/2TL;

    invoke-static {v13}, LX/ERI;->a(LX/0QB;)LX/ERI;

    move-result-object v11

    check-cast v11, LX/ERI;

    invoke-static {v13}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v12

    check-cast v12, LX/2TK;

    const/16 v14, 0x259

    invoke-static {v13, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v0 .. v13}, Lcom/facebook/vault/service/VaultSyncService;->a(Lcom/facebook/vault/service/VaultSyncService;LX/0Or;LX/2TM;LX/ES8;LX/ERr;LX/ERh;LX/ERz;LX/2TN;LX/ERJ;LX/ERK;LX/2TL;LX/ERI;LX/2TK;LX/0Ot;)V

    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 2122015
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->g:LX/ES8;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, LX/ES8;->a(I)Ljava/util/Set;

    move-result-object v0

    .line 2122016
    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncService;->h:LX/ERr;

    invoke-virtual {v1, v0}, LX/ERr;->a(Ljava/util/Set;)V

    .line 2122017
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method private b(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2122018
    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    if-eq p1, v1, :cond_0

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    const/4 v2, 0x6

    if-eq p1, v2, :cond_0

    const/4 v2, 0x7

    if-eq p1, v2, :cond_0

    const/16 v2, 0x9

    if-eq p1, v2, :cond_0

    const/16 v2, 0xa

    if-eq p1, v2, :cond_0

    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    .line 2122019
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2122020
    iget-wide v4, p0, Lcom/facebook/vault/service/VaultSyncService;->f:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    move v0, v1

    .line 2122021
    :cond_1
    :goto_0
    return v0

    .line 2122022
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/vault/service/VaultSyncService;->f:J

    goto :goto_0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2122023
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x116be2e7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122024
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2122025
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2122026
    invoke-static {p0, p0}, Lcom/facebook/vault/service/VaultSyncService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2122027
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zr;

    .line 2122028
    const-string v2, "vault_thread"

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 2122029
    new-instance v2, LX/ES3;

    invoke-direct {v2, p0}, LX/ES3;-><init>(Lcom/facebook/vault/service/VaultSyncService;)V

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 2122030
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2122031
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->c:Landroid/os/Looper;

    .line 2122032
    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->c:Landroid/os/Looper;

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->d:Landroid/os/Handler;

    .line 2122033
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/vault/service/VaultSyncService;->f:J

    .line 2122034
    const/16 v0, 0x25

    const v2, 0x6d7c4d60

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x7dbc1592

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122035
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2122036
    iget-object v1, p0, Lcom/facebook/vault/service/VaultSyncService;->c:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 2122037
    const/16 v1, 0x25

    const v2, -0x322fd53

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x24

    const v1, 0x22c15591

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122038
    if-nez p1, :cond_0

    .line 2122039
    iget-object v0, p0, Lcom/facebook/vault/service/VaultSyncService;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "VaultSyncService onStartCommand"

    const-string v3, "started with null intent"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122040
    const/16 v0, 0x25

    const v2, -0x19fc2925

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2122041
    :goto_0
    return v6

    .line 2122042
    :cond_0
    const-string v0, "sync_reason"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2122043
    sget-object v0, Lcom/facebook/vault/service/VaultSyncService;->b:Ljava/lang/Class;

    const-string v2, "intent sent to VaultSyncService had no reason key, so no sync queued"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2122044
    const v0, 0x4d501c7c    # 2.1822048E8f

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 2122045
    :cond_1
    const-string v0, "sync_reason"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2122046
    invoke-direct {p0, v0}, Lcom/facebook/vault/service/VaultSyncService;->b(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2122047
    const v0, -0x28cbc8fb    # -1.981484E14f

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 2122048
    :cond_2
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->i:LX/ERh;

    invoke-virtual {v2}, LX/ERh;->a()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    .line 2122049
    sget-object v0, Lcom/facebook/vault/service/VaultSyncService;->b:Ljava/lang/Class;

    const-string v2, "Device has not been setup, and sync reason not SYNC_REASON_SETUP"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2122050
    const v0, -0x1501bd3d

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 2122051
    :cond_3
    iget-object v2, p0, Lcom/facebook/vault/service/VaultSyncService;->d:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/vault/service/VaultSyncService$VaultSyncServiceRunnable;

    invoke-direct {v3, p0, p0, p1, p3}, Lcom/facebook/vault/service/VaultSyncService$VaultSyncServiceRunnable;-><init>(Lcom/facebook/vault/service/VaultSyncService;Landroid/content/Context;Landroid/content/Intent;I)V

    invoke-static {v0}, Lcom/facebook/vault/service/VaultSyncService;->a(I)J

    move-result-wide v4

    const v0, -0x5b7f9751

    invoke-static {v2, v3, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2122052
    const v0, -0x20e87327

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0
.end method
