.class public Lcom/facebook/vault/service/VaultObserverService;
.super LX/0te;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/ERo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/2TK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121784
    const-class v0, Lcom/facebook/vault/service/VaultObserverService;

    sput-object v0, Lcom/facebook/vault/service/VaultObserverService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2121772
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/vault/service/VaultObserverService;LX/ERo;LX/2TK;)V
    .locals 0

    .prologue
    .line 2121783
    iput-object p1, p0, Lcom/facebook/vault/service/VaultObserverService;->b:LX/ERo;

    iput-object p2, p0, Lcom/facebook/vault/service/VaultObserverService;->c:LX/2TK;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/vault/service/VaultObserverService;

    new-instance p1, LX/ERo;

    invoke-static {v1}, LX/43r;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-direct {p1, v0}, LX/ERo;-><init>(Landroid/os/Handler;)V

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v0, v0

    move-object v0, v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v1}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v2

    check-cast v2, LX/2TK;

    iput-object v0, p1, LX/ERo;->b:Landroid/net/Uri;

    iput-object v2, p1, LX/ERo;->c:LX/2TK;

    move-object v0, p1

    check-cast v0, LX/ERo;

    invoke-static {v1}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v1

    check-cast v1, LX/2TK;

    invoke-static {p0, v0, v1}, Lcom/facebook/vault/service/VaultObserverService;->a(Lcom/facebook/vault/service/VaultObserverService;LX/ERo;LX/2TK;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2121785
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x24

    const v1, 0x494520c9

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2121776
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2121777
    invoke-static {p0, p0}, Lcom/facebook/vault/service/VaultObserverService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2121778
    invoke-virtual {p0}, Lcom/facebook/vault/service/VaultObserverService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/vault/service/VaultObserverService;->b:LX/ERo;

    .line 2121779
    iget-object v3, v2, LX/ERo;->b:Landroid/net/Uri;

    move-object v2, v3

    .line 2121780
    iget-object v3, p0, Lcom/facebook/vault/service/VaultObserverService;->b:LX/ERo;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2121781
    iget-object v1, p0, Lcom/facebook/vault/service/VaultObserverService;->c:LX/2TK;

    invoke-virtual {v1, v4}, LX/2TK;->c(I)V

    .line 2121782
    const/16 v1, 0x25

    const v2, -0x2e6a939e

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x702bc382

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2121774
    invoke-virtual {p0}, Lcom/facebook/vault/service/VaultObserverService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/vault/service/VaultObserverService;->b:LX/ERo;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2121775
    const/16 v1, 0x25

    const v2, 0x7244d9bc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x52c5afc7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2121773
    const/4 v1, 0x1

    const/16 v2, 0x25

    const v3, -0x1df7aa34

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
