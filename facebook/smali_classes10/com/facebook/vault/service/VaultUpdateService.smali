.class public Lcom/facebook/vault/service/VaultUpdateService;
.super LX/1ZN;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ERh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2122525
    const-string v0, "VaultUpdateService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2122526
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/vault/service/VaultUpdateService;

    const/16 v1, 0x37a4

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/service/VaultUpdateService;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x77295812

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2122527
    if-nez p1, :cond_0

    .line 2122528
    const/16 v0, 0x25

    const v2, -0x6f2e255b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2122529
    :goto_0
    return-void

    .line 2122530
    :cond_0
    iget-object v0, p0, Lcom/facebook/vault/service/VaultUpdateService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERh;

    .line 2122531
    const/4 v5, 0x0

    .line 2122532
    iget-object v4, v0, LX/ERh;->i:LX/2TL;

    invoke-virtual {v4}, LX/2TL;->a()J

    move-result-wide v6

    .line 2122533
    iget-object v4, v0, LX/ERh;->m:LX/2TI;

    invoke-virtual {v4}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v8

    .line 2122534
    const-string v4, "OFF"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    .line 2122535
    :goto_1
    new-instance v9, LX/ERV;

    invoke-direct {v9, v6, v7}, LX/ERV;-><init>(J)V

    .line 2122536
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v9, LX/ERV;->c:Ljava/lang/Boolean;

    .line 2122537
    if-eqz v4, :cond_1

    .line 2122538
    iput-object v8, v9, LX/ERV;->d:Ljava/lang/String;

    .line 2122539
    :cond_1
    :try_start_0
    iget-object v4, v0, LX/ERh;->b:LX/11H;

    iget-object v6, v0, LX/ERh;->e:LX/ERU;

    invoke-virtual {v4, v6, v9}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2122540
    :cond_2
    :goto_2
    const v0, 0x3f717d93

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    :cond_3
    move v4, v5

    .line 2122541
    goto :goto_1

    .line 2122542
    :catch_0
    move-exception v4

    .line 2122543
    sget-object v6, LX/ERh;->a:Ljava/lang/String;

    const-string v7, "updateDeviceEnabledOnServer"

    invoke-static {v6, v7, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2122544
    instance-of v6, v4, LX/2Oo;

    if-eqz v6, :cond_2

    .line 2122545
    iget-object v6, v0, LX/ERh;->o:LX/03V;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "vault_device_update_api exception: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x5eb3ef03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122521
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2122522
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2122523
    invoke-static {p0, p0}, Lcom/facebook/vault/service/VaultUpdateService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2122524
    const/16 v1, 0x25

    const v2, -0x4f4e3f56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
