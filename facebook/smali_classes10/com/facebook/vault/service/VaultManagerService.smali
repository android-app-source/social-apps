.class public Lcom/facebook/vault/service/VaultManagerService;
.super LX/1ZN;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/2TK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/ES8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/ERr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/ERh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:LX/2TH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121415
    const-class v0, LX/ERh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/vault/service/VaultManagerService;->b:Ljava/lang/String;

    .line 2121416
    const-string v0, "method_key"

    sput-object v0, Lcom/facebook/vault/service/VaultManagerService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2121417
    const-string v0, "VaultSyncManagerService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2121418
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2121419
    iput-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->h:LX/0Ot;

    .line 2121420
    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    .line 2121421
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->f:LX/ERh;

    invoke-virtual {v0}, LX/ERh;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2121422
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->f:LX/ERh;

    const-wide/16 v7, 0x3e8

    const/4 v2, 0x0

    .line 2121423
    iget-object v1, v0, LX/ERh;->f:LX/0dC;

    invoke-virtual {v1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v1

    .line 2121424
    new-instance v3, LX/ERR;

    invoke-direct {v3, v1}, LX/ERR;-><init>(Ljava/lang/String;)V

    .line 2121425
    :try_start_0
    iget-object v1, v0, LX/ERh;->b:LX/11H;

    iget-object v4, v0, LX/ERh;->d:LX/ERQ;

    invoke-virtual {v1, v4, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/vault/model/FacebookVaultDevice;

    .line 2121426
    if-eqz v1, :cond_5

    .line 2121427
    iget-wide v3, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mFbid:J

    iput-wide v3, v0, LX/ERh;->g:J

    .line 2121428
    iget-wide v3, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mLastSyncTime:J

    mul-long/2addr v3, v7

    iput-wide v3, v0, LX/ERh;->h:J

    .line 2121429
    iget-boolean v3, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mEnabled:Z

    if-nez v3, :cond_3

    .line 2121430
    iget-object v3, v0, LX/ERh;->m:LX/2TI;

    const-string v4, "OFF"

    invoke-virtual {v3, v4}, LX/2TI;->a(Ljava/lang/String;)V

    .line 2121431
    iget-object v3, v0, LX/ERh;->n:LX/2TH;

    invoke-virtual {v3}, LX/2TH;->a()V

    .line 2121432
    :goto_0
    invoke-static {v0}, LX/ERh;->e(LX/ERh;)V

    .line 2121433
    iget-object v3, v0, LX/ERh;->k:LX/ERH;

    iget-wide v5, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mTimeCreated:J

    mul-long/2addr v5, v7

    invoke-virtual {v3, v5, v6}, LX/ERH;->a(J)V

    .line 2121434
    iget-object v3, v0, LX/ERh;->l:LX/ERK;

    iget-boolean v1, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncOlderPhotos:Z

    invoke-virtual {v3, v1}, LX/ERK;->a(Z)V

    .line 2121435
    const/4 v1, 0x1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2121436
    :goto_1
    move v0, v1

    .line 2121437
    if-eqz v0, :cond_1

    .line 2121438
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121439
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->b()V

    .line 2121440
    :cond_0
    invoke-direct {p0}, Lcom/facebook/vault/service/VaultManagerService;->c()V

    .line 2121441
    :cond_1
    :goto_2
    return-void

    .line 2121442
    :cond_2
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2121443
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->b()V

    goto :goto_2

    .line 2121444
    :cond_3
    :try_start_1
    iget-object v3, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncMode:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/vault/model/FacebookVaultDevice;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2121445
    sget-object v3, LX/ERh;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "invalid sync mode for device: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncMode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121446
    iget-object v3, v0, LX/ERh;->o:LX/03V;

    const-string v4, "vault_device_setup bad sync mode"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid sync mode for device: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncMode:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2121447
    goto :goto_1

    .line 2121448
    :cond_4
    iget-object v3, v0, LX/ERh;->m:LX/2TI;

    iget-object v4, v1, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2TI;->a(Ljava/lang/String;)V

    .line 2121449
    iget-object v3, v0, LX/ERh;->n:LX/2TH;

    invoke-virtual {v3}, LX/2TH;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2121450
    :catch_0
    move-exception v1

    .line 2121451
    sget-object v3, LX/ERh;->a:Ljava/lang/String;

    const-string v4, "setupDeviceFromServer"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121452
    instance-of v3, v1, LX/2Oo;

    if-eqz v3, :cond_5

    .line 2121453
    iget-object v3, v0, LX/ERh;->o:LX/03V;

    sget-object v4, LX/ERh;->a:Ljava/lang/String;

    const-string v5, "exception when running mDevicePostMethod"

    invoke-virtual {v3, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move v1, v2

    .line 2121454
    goto :goto_1
.end method

.method private static a(Lcom/facebook/vault/service/VaultManagerService;LX/2TK;LX/ES8;LX/ERr;LX/ERh;LX/2TH;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/vault/service/VaultManagerService;",
            "LX/2TK;",
            "LX/ES8;",
            "LX/ERr;",
            "LX/ERh;",
            "LX/2TH;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121455
    iput-object p1, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    iput-object p2, p0, Lcom/facebook/vault/service/VaultManagerService;->d:LX/ES8;

    iput-object p3, p0, Lcom/facebook/vault/service/VaultManagerService;->e:LX/ERr;

    iput-object p4, p0, Lcom/facebook/vault/service/VaultManagerService;->f:LX/ERh;

    iput-object p5, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    iput-object p6, p0, Lcom/facebook/vault/service/VaultManagerService;->h:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/vault/service/VaultManagerService;

    invoke-static {v6}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v1

    check-cast v1, LX/2TK;

    invoke-static {v6}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v2

    check-cast v2, LX/ES8;

    invoke-static {v6}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v3

    check-cast v3, LX/ERr;

    invoke-static {v6}, LX/ERh;->b(LX/0QB;)LX/ERh;

    move-result-object v4

    check-cast v4, LX/ERh;

    invoke-static {v6}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object v5

    check-cast v5, LX/2TH;

    const/16 v7, 0x259

    invoke-static {v6, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/vault/service/VaultManagerService;->a(Lcom/facebook/vault/service/VaultManagerService;LX/2TK;LX/ES8;LX/ERr;LX/ERh;LX/2TH;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 12

    .prologue
    const/4 v1, 0x5

    .line 2121456
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->f:LX/ERh;

    invoke-virtual {v0}, LX/ERh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2121457
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->f:LX/ERh;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v10, 0x3e8

    .line 2121458
    iget-object v2, v0, LX/ERh;->f:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v5

    .line 2121459
    iget-object v2, v0, LX/ERh;->m:LX/2TI;

    invoke-virtual {v2}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v6

    .line 2121460
    iget-object v2, v0, LX/ERh;->l:LX/ERK;

    invoke-virtual {v2}, LX/ERK;->a()Z

    move-result v7

    .line 2121461
    invoke-static {v6}, Lcom/facebook/vault/model/FacebookVaultDevice;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2121462
    new-instance v2, LX/ERT;

    invoke-direct {v2, v5, v3, v6, v7}, LX/ERT;-><init>(Ljava/lang/String;ZLjava/lang/String;Z)V

    .line 2121463
    :goto_0
    :try_start_0
    iget-object v5, v0, LX/ERh;->b:LX/11H;

    iget-object v6, v0, LX/ERh;->c:LX/ERS;

    invoke-virtual {v5, v6, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/vault/model/FacebookVaultDevice;

    .line 2121464
    if-eqz v2, :cond_4

    .line 2121465
    iget-wide v6, v2, Lcom/facebook/vault/model/FacebookVaultDevice;->mFbid:J

    iput-wide v6, v0, LX/ERh;->g:J

    .line 2121466
    invoke-static {v0}, LX/ERh;->e(LX/ERh;)V

    .line 2121467
    iget-wide v6, v2, Lcom/facebook/vault/model/FacebookVaultDevice;->mLastSyncTime:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    .line 2121468
    iget-wide v6, v2, Lcom/facebook/vault/model/FacebookVaultDevice;->mLastSyncTime:J

    mul-long/2addr v6, v10

    iput-wide v6, v0, LX/ERh;->h:J

    .line 2121469
    iget-object v5, v0, LX/ERh;->j:LX/ERI;

    iget-wide v6, v0, LX/ERh;->h:J

    invoke-virtual {v5, v6, v7}, LX/ERI;->a(J)V

    .line 2121470
    iget-object v5, v0, LX/ERh;->k:LX/ERH;

    iget-wide v6, v2, Lcom/facebook/vault/model/FacebookVaultDevice;->mTimeCreated:J

    mul-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, LX/ERH;->a(J)V

    .line 2121471
    iget-object v5, v0, LX/ERh;->l:LX/ERK;

    iget-boolean v2, v2, Lcom/facebook/vault/model/FacebookVaultDevice;->mSyncOlderPhotos:Z

    invoke-virtual {v5, v2}, LX/ERK;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 2121472
    :goto_1
    move v0, v2

    .line 2121473
    if-eqz v0, :cond_0

    .line 2121474
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->b()V

    .line 2121475
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    invoke-virtual {v0, v1}, LX/2TK;->c(I)V

    .line 2121476
    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/vault/service/VaultUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2121477
    invoke-virtual {p0, v0}, Lcom/facebook/vault/service/VaultManagerService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2121478
    return-void

    .line 2121479
    :cond_0
    invoke-direct {p0}, Lcom/facebook/vault/service/VaultManagerService;->c()V

    .line 2121480
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->b()V

    .line 2121481
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/2TK;->c(I)V

    goto :goto_2

    .line 2121482
    :cond_1
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->g:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->b()V

    .line 2121483
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->c:LX/2TK;

    invoke-virtual {v0, v1}, LX/2TK;->c(I)V

    goto :goto_2

    .line 2121484
    :cond_2
    new-instance v2, LX/ERT;

    invoke-direct {v2, v5}, LX/ERT;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2121485
    :cond_3
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2121486
    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    .line 2121487
    iput-wide v2, v0, LX/ERh;->h:J

    .line 2121488
    iget-object v5, v0, LX/ERh;->j:LX/ERI;

    iget-wide v8, v0, LX/ERh;->h:J

    invoke-virtual {v5, v8, v9}, LX/ERI;->a(J)V

    .line 2121489
    iget-object v5, v0, LX/ERh;->k:LX/ERH;

    invoke-virtual {v5, v2, v3}, LX/ERH;->a(J)V

    .line 2121490
    iget-wide v2, v0, LX/ERh;->g:J

    invoke-virtual {v0, v2, v3, v6, v7}, LX/ERh;->a(JJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v2, v4

    .line 2121491
    goto :goto_1

    .line 2121492
    :catch_0
    move-exception v2

    .line 2121493
    sget-object v3, LX/ERh;->a:Ljava/lang/String;

    const-string v5, "setupAndUpdateDeviceFromServer"

    invoke-static {v3, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121494
    instance-of v3, v2, LX/2Oo;

    if-eqz v3, :cond_4

    .line 2121495
    iget-object v3, v0, LX/ERh;->o:LX/03V;

    sget-object v5, LX/ERh;->a:Ljava/lang/String;

    const-string v6, "exception when running mDevicePostMethod"

    invoke-virtual {v3, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    move v2, v4

    .line 2121496
    goto :goto_1
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2121497
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->d:LX/ES8;

    .line 2121498
    :try_start_0
    iget-object v1, v0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/DbE;->a:Landroid/net/Uri;

    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2121499
    :goto_0
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, LX/ES8;->a(I)Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    .line 2121500
    iget-object v1, p0, Lcom/facebook/vault/service/VaultManagerService;->e:LX/ERr;

    invoke-virtual {v1, v0}, LX/ERr;->a(Ljava/util/Set;)V

    .line 2121501
    return-void

    .line 2121502
    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IContentProvider for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x71315a4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2121503
    if-nez p1, :cond_0

    .line 2121504
    const/16 v0, 0x25

    const v2, -0x1256cefc

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2121505
    :goto_0
    return-void

    .line 2121506
    :cond_0
    sget-object v0, Lcom/facebook/vault/service/VaultManagerService;->a:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2121507
    packed-switch v0, :pswitch_data_0

    .line 2121508
    iget-object v0, p0, Lcom/facebook/vault/service/VaultManagerService;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/vault/service/VaultManagerService;->b:Ljava/lang/String;

    const-string v3, "Unsupported method"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121509
    :goto_1
    const v0, -0x2b935844

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 2121510
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/vault/service/VaultManagerService;->a()V

    goto :goto_1

    .line 2121511
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/vault/service/VaultManagerService;->b()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x4abb2247

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2121512
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2121513
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2121514
    invoke-static {p0, p0}, Lcom/facebook/vault/service/VaultManagerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2121515
    const/16 v1, 0x25

    const v2, 0x4b7db0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
