.class public Lcom/facebook/vault/protocol/VaultDeviceGetResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/protocol/VaultDeviceGetResultDeserializer;
.end annotation


# instance fields
.field public data:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/model/FacebookVaultDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2120877
    const-class v0, Lcom/facebook/vault/protocol/VaultDeviceGetResultDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2120878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120879
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/vault/protocol/VaultDeviceGetResult;->data:Ljava/util/List;

    .line 2120880
    return-void
.end method
