.class public Lcom/facebook/vault/protocol/VaultAllImagesGetResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/protocol/VaultAllImagesGetResultDeserializer;
.end annotation


# instance fields
.field public data:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/protocol/VaultImageResultObject;",
            ">;"
        }
    .end annotation
.end field

.field public paging:Lcom/facebook/vault/protocol/VaultPagingObject;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "paging"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2120831
    const-class v0, Lcom/facebook/vault/protocol/VaultAllImagesGetResultDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2120832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120833
    iput-object v0, p0, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->data:Ljava/util/List;

    .line 2120834
    iput-object v0, p0, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->paging:Lcom/facebook/vault/protocol/VaultPagingObject;

    .line 2120835
    return-void
.end method
