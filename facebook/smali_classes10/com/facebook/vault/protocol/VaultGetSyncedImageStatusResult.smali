.class public Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResultDeserializer;
.end annotation


# instance fields
.field public mResult:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2121038
    const-class v0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResultDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2121039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121040
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;->mResult:Ljava/util/Map;

    .line 2121041
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2121042
    iget-object v0, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;->mResult:Ljava/util/Map;

    return-object v0
.end method
