.class public Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusDeserializer;
.end annotation


# instance fields
.field public final mDeleted:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deleted"
    .end annotation
.end field

.field public final mFbid:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final mHeight:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field public final mWidth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2120978
    const-class v0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2120979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120980
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mFbid:J

    .line 2120981
    iput-boolean v2, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mDeleted:Z

    .line 2120982
    iput v2, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    .line 2120983
    iput v2, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    .line 2120984
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2120985
    check-cast p1, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;

    .line 2120986
    iget-wide v0, p1, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mFbid:J

    iget-wide v2, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mFbid:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mDeleted:Z

    iget-boolean v1, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mDeleted:Z

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    iget v1, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    iget v1, p0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
