.class public Lcom/facebook/runtimepermissions/RequestPermissionsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public p:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0i5;

.field public s:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1947583
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;[Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;)Landroid/content/Intent;
    .locals 2
    .param p1    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1947584
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1947585
    const-string v1, "extra_permissions"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1947586
    move-object v0, v0

    .line 1947587
    const-string v1, "extra_permissions_request_config"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1947588
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-static {v1}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    const-class v2, LX/0i4;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/0i4;

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->p:LX/1Ml;

    iput-object v1, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->q:LX/0i4;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1947578
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1947579
    const-string v1, "extra_permission_results"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1947580
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->setResult(ILandroid/content/Intent;)V

    .line 1947581
    invoke-virtual {p0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->finish()V

    .line 1947582
    return-void
.end method

.method public static b(Ljava/util/Map;[Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1947535
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1947536
    invoke-interface {p0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1947537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1947538
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1947542
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1947543
    invoke-static {p0, p0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1947544
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->q:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->r:LX/0i5;

    .line 1947545
    if-eqz p1, :cond_3

    .line 1947546
    const-string v0, "key_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    .line 1947547
    :goto_0
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 1947548
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    .line 1947549
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1947550
    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 1947551
    iget-object p1, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->p:LX/1Ml;

    invoke-virtual {p1, v4}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1947552
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1947553
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1947554
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v0, v1

    .line 1947555
    invoke-virtual {p0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1947556
    const-string v1, "extra_permissions_request_config"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    .line 1947557
    if-nez v1, :cond_2

    .line 1947558
    const-string v1, "extra_should_show_rationale"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1947559
    const-string v3, "extra_custom_title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1947560
    const-string v4, "extra_custom_subtitle"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1947561
    new-instance v4, LX/2rN;

    invoke-direct {v4}, LX/2rN;-><init>()V

    .line 1947562
    iput-object v3, v4, LX/2rN;->a:Ljava/lang/String;

    .line 1947563
    move-object v3, v4

    .line 1947564
    iput-object v2, v3, LX/2rN;->b:Ljava/lang/String;

    .line 1947565
    move-object v2, v3

    .line 1947566
    if-eqz v1, :cond_5

    sget-object v1, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    .line 1947567
    :goto_2
    iput-object v1, v2, LX/2rN;->c:LX/0jt;

    .line 1947568
    move-object v1, v2

    .line 1947569
    invoke-virtual {v1}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v1

    .line 1947570
    :cond_2
    iget-object v2, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->r:LX/0i5;

    .line 1947571
    new-instance v3, LX/Cux;

    invoke-direct {v3, p0}, LX/Cux;-><init>(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;)V

    move-object v3, v3

    .line 1947572
    invoke-virtual {v2, v0, v1, v3}, LX/0i5;->a([Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 1947573
    :goto_3
    return-void

    .line 1947574
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1947575
    const-string v1, "extra_permissions"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    goto :goto_0

    .line 1947576
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a$redex0(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;Ljava/util/HashMap;)V

    goto :goto_3

    .line 1947577
    :cond_5
    sget-object v1, LX/0jt;->NEVER_SHOW:LX/0jt;

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1947539
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1947540
    const-string v0, "key_permissions"

    iget-object v1, p0, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1947541
    return-void
.end method
