.class public Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/placetips/bootstrap/IsUserPlaceTipsDebugEmployee;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0bV;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2069736
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V
    .locals 3

    .prologue
    .line 2069737
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2069738
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->c:LX/0Sh;

    new-instance v2, LX/E1N;

    invoke-direct {v2, p0, v0}, LX/E1N;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;Ljava/util/Set;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2069739
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2069740
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2069741
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    const/16 v3, 0x2fd

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1556

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    new-instance v6, LX/Cdk;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-direct {v6, v7}, LX/Cdk;-><init>(LX/0QB;)V

    move-object v6, v6

    const/16 v7, 0xf59

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v0, 0x455

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->c:LX/0Sh;

    iput-object v6, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->e:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->f:LX/0Ot;

    .line 2069742
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2069743
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2069744
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2069745
    iget-object p1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->e:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/2ct;

    sget-object p2, LX/2cx;->INJECT:LX/2cx;

    invoke-static {p2}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(LX/2cx;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/CeG;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object p2

    .line 2069746
    iput-object p2, p1, LX/CeG;->c:Ljava/lang/String;

    .line 2069747
    move-object p1, p1

    .line 2069748
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object p2

    .line 2069749
    iput-object p2, p1, LX/CeG;->d:Ljava/lang/String;

    .line 2069750
    move-object p1, p1

    .line 2069751
    sget-object p2, LX/Cdj;->HIGH:LX/Cdj;

    .line 2069752
    iput-object p2, p1, LX/CeG;->p:LX/Cdj;

    .line 2069753
    move-object p1, p1

    .line 2069754
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object p2

    const-string p3, "You are pretending to be at %1$s"

    invoke-static {p2, p3}, LX/CeK;->a(Ljava/lang/String;Ljava/lang/String;)LX/175;

    move-result-object p2

    .line 2069755
    iput-object p2, p1, LX/CeG;->g:LX/175;

    .line 2069756
    move-object p1, p1

    .line 2069757
    invoke-virtual {p1}, LX/CeG;->a()LX/0bZ;

    .line 2069758
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x79cc8640

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2069759
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2069760
    :cond_0
    const/4 v0, 0x0

    const/16 v2, 0x2b

    const v3, -0x2e570f1a

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2069761
    :goto_0
    return-object v0

    :cond_1
    const v0, 0x7f030f86

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, -0x6ee9ee83

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2069762
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2069763
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2069764
    if-nez v0, :cond_0

    .line 2069765
    :goto_0
    return-void

    .line 2069766
    :cond_0
    const v0, 0x7f0d2574

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->g:Landroid/widget/TextView;

    .line 2069767
    const v0, 0x7f0d2572

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->h:Landroid/widget/Button;

    .line 2069768
    const v0, 0x7f0d2571

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/E1K;

    invoke-direct {v1, p0}, LX/E1K;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069769
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->h:Landroid/widget/Button;

    new-instance v1, LX/E1L;

    invoke-direct {v1, p0}, LX/E1L;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069770
    const v0, 0x7f0d2573

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/E1M;

    invoke-direct {v1, p0}, LX/E1M;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069771
    invoke-static {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->b$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V

    goto :goto_0
.end method
