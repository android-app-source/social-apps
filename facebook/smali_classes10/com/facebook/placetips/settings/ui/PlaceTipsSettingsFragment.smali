.class public Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/placetips/bootstrap/IsUserPlaceTipsDebugEmployee;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/CeT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Lcom/facebook/widget/BetterSwitch;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field public u:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2069894
    const-class v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2069830
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2069831
    new-instance v0, LX/E1O;

    invoke-direct {v0, p0}, LX/E1O;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->l:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;LX/E1W;)V
    .locals 2

    .prologue
    .line 2069890
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->m:Landroid/view/View;

    iget v1, p1, LX/E1W;->settingsVisibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2069891
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->n:Landroid/view/View;

    iget v1, p1, LX/E1W;->progressVisibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2069892
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->o:Landroid/view/View;

    iget v1, p1, LX/E1W;->errorVisibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2069893
    return-void
.end method

.method public static a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 2

    .prologue
    .line 2069886
    invoke-virtual {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Z)V

    .line 2069887
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->u:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2069888
    return-void

    .line 2069889
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2069883
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->b:LX/17Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_from_uri"

    sget-object v2, LX/0ax;->eh:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2069884
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2069885
    return-void
.end method

.method public static a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Z)V
    .locals 2

    .prologue
    .line 2069879
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->p:Lcom/facebook/widget/BetterSwitch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2069880
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->p:Lcom/facebook/widget/BetterSwitch;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 2069881
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->p:Lcom/facebook/widget/BetterSwitch;

    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->l:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2069882
    return-void
.end method

.method public static b(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Z)V
    .locals 13

    .prologue
    .line 2069895
    if-eqz p1, :cond_0

    const-string v0, "enabled"

    :goto_0
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 2069896
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 2069897
    :goto_1
    return-void

    .line 2069898
    :cond_0
    const-string v0, "disabled"

    goto :goto_0

    .line 2069899
    :cond_1
    const v2, 0x7f080024

    const/4 v3, 0x1

    invoke-static {v2, v3, v4, v4}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v3

    .line 2069900
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const-string v4, "save_setting_progress"

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2069901
    const-string v2, "enabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2cy;

    invoke-virtual {v2}, LX/2cy;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2069902
    iget-object v2, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->h:LX/CeT;

    sget-object v4, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 2069903
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2069904
    const-string v7, "BackgroundLocationUpdateSettingsParams"

    const/4 v8, 0x1

    .line 2069905
    new-instance v10, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v11}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v11

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;-><init>(LX/0am;LX/0am;)V

    move-object v8, v10

    .line 2069906
    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2069907
    iget-object v7, v2, LX/CeT;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    const-string v8, "background_location_update_settings"

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v12, -0x3387688c    # -6.51668E7f

    move-object v11, v4

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    move-object v2, v7

    .line 2069908
    new-instance v4, LX/E1U;

    invoke-direct {v4, p0, v0, v1}, LX/E1U;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v2, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2069909
    :goto_2
    iget-object v4, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->i:LX/1Ck;

    sget-object v5, LX/E1X;->TOUCH_SETTINGS:LX/E1X;

    new-instance v6, LX/E1V;

    invoke-direct {v6, p0, v3}, LX/E1V;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Landroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v4, v5, v2, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    .line 2069910
    :cond_2
    iget-object v2, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->h:LX/CeT;

    invoke-virtual {v2, v0, v1}, LX/CeT;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_2
.end method

.method public static c(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V
    .locals 6

    .prologue
    .line 2069871
    sget-object v0, LX/E1W;->PROGRESS:LX/E1W;

    invoke-static {p0, v0}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;LX/E1W;)V

    .line 2069872
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->i:LX/1Ck;

    sget-object v1, LX/E1X;->TOUCH_SETTINGS:LX/E1X;

    iget-object v2, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->h:LX/CeT;

    .line 2069873
    invoke-static {}, LX/3jx;->a()LX/3jy;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2069874
    iget-object v3, v2, LX/CeT;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance v4, LX/CeP;

    invoke-direct {v4, v2}, LX/CeP;-><init>(LX/CeT;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2069875
    new-instance v4, LX/CeQ;

    invoke-direct {v4, v2}, LX/CeQ;-><init>(LX/CeT;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2069876
    move-object v2, v3

    .line 2069877
    new-instance v3, LX/E1T;

    invoke-direct {v3, p0}, LX/E1T;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2069878
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2069868
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2069869
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0xf65

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    const/16 v8, 0x2fd

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x1556

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/CeT;->a(LX/0QB;)LX/CeT;

    move-result-object v10

    check-cast v10, LX/CeT;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->b:LX/17Y;

    iput-object v5, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->e:LX/0kL;

    iput-object v8, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->f:LX/0Or;

    iput-object v9, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->g:LX/0Or;

    iput-object v10, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->h:LX/CeT;

    iput-object p1, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->i:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->j:LX/0ad;

    .line 2069870
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1aaa0930

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2069867
    const v1, 0x7f030f89

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x63bb24a1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xea35323

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2069862
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2069863
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2069864
    if-eqz v0, :cond_0

    .line 2069865
    const v2, 0x7f083127

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2069866
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x5510d4fa

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2069832
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2069833
    const v0, 0x7f0d257a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->m:Landroid/view/View;

    .line 2069834
    const v0, 0x7f0d2583

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->n:Landroid/view/View;

    .line 2069835
    const v0, 0x7f0d2584

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->o:Landroid/view/View;

    .line 2069836
    const v0, 0x7f0d257c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/BetterSwitch;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->p:Lcom/facebook/widget/BetterSwitch;

    .line 2069837
    const v0, 0x7f0d257f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->q:Landroid/view/View;

    .line 2069838
    const v0, 0x7f0d2580

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->r:Landroid/view/View;

    .line 2069839
    const v0, 0x7f0d2581

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->s:Landroid/view/View;

    .line 2069840
    const v0, 0x7f0d257d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->t:Landroid/widget/TextView;

    .line 2069841
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->t:Landroid/widget/TextView;

    .line 2069842
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->j:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-char v3, LX/E1b;->a:C

    const p1, 0x7f08312a

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-interface {v1, v2, v3, p1, p2}, LX/0ad;->a(LX/0c0;CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2069843
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2069844
    const v0, 0x7f0d257e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->u:Landroid/widget/TextView;

    .line 2069845
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->u:Landroid/widget/TextView;

    .line 2069846
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->j:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-char v3, LX/E1b;->b:C

    const p1, 0x7f083137

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-interface {v1, v2, v3, p1, p2}, LX/0ad;->a(LX/0c0;CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2069847
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2069848
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->o:Landroid/view/View;

    new-instance v1, LX/E1P;

    invoke-direct {v1, p0}, LX/E1P;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069849
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->q:Landroid/view/View;

    new-instance v1, LX/E1Q;

    invoke-direct {v1, p0}, LX/E1Q;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069850
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->r:Landroid/view/View;

    new-instance v1, LX/E1R;

    invoke-direct {v1, p0}, LX/E1R;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069851
    const/4 v0, 0x1

    .line 2069852
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2069853
    if-nez v1, :cond_3

    .line 2069854
    :cond_0
    :goto_0
    move v0, v0

    .line 2069855
    if-eqz v0, :cond_2

    .line 2069856
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->s:Landroid/view/View;

    new-instance v1, LX/E1S;

    invoke-direct {v1, p0}, LX/E1S;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069857
    :goto_1
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2582

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2069858
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d2582

    new-instance v2, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    invoke-direct {v2}, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2069859
    :cond_1
    invoke-static {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->c(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V

    .line 2069860
    return-void

    .line 2069861
    :cond_2
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->s:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    sget-object v2, LX/0ax;->eg:Ljava/lang/String;

    const-string v3, "extra_from_uri"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
