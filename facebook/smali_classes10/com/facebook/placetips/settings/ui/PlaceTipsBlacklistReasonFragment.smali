.class public Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistReasonFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2069685
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GravityNegativeFeedbackOptions;
        .end annotation
    .end param

    .prologue
    .line 2069686
    new-instance v0, LX/E1G;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E1G;-><init>(Landroid/content/Context;)V

    .line 2069687
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v1

    .line 2069688
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2069689
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "gravity_location_data"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    .line 2069690
    :goto_0
    move-object v3, v1

    .line 2069691
    sget-object v1, LX/0cQ;->PLACE_TIPS_BLACKLIST_CONFIRMATION_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v4

    .line 2069692
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2069693
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "place_id"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2069694
    :goto_1
    move-object v5, v1

    .line 2069695
    move-object v1, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, LX/E1G;->a(Ljava/lang/String;LX/0gc;Lcom/facebook/placetips/settings/PlaceTipsLocationData;ILjava/lang/String;I)V

    .line 2069696
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2069697
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x65295f45

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2069698
    const v0, 0x7f030f82

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2069699
    const v1, 0x7f0d256e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2069700
    const v2, 0x7f0d256f

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 2069701
    const v4, 0x7f030f85

    invoke-virtual {v2, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2069702
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2069703
    const-string v2, "INCORRECT_LOCATION"

    const v4, 0x7f083131

    invoke-direct {p0, v2, v1, v4}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistReasonFragment;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 2069704
    const-string v2, "ANNOYING"

    const v4, 0x7f083132

    invoke-direct {p0, v2, v1, v4}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistReasonFragment;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 2069705
    const-string v2, "SINGLE_PAGE_OPT_OUT"

    const v4, 0x7f083133

    invoke-direct {p0, v2, v1, v4}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistReasonFragment;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 2069706
    const/16 v1, 0x2b

    const v2, 0x25ff8bd6

    invoke-static {v5, v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
