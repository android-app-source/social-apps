.class public Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/E1D;


# instance fields
.field public p:LX/0jo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CeT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2069620
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;LX/0jo;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;",
            "LX/0jo;",
            "LX/0Ot",
            "<",
            "LX/CeT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2069619
    iput-object p1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->p:LX/0jo;

    iput-object p2, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->q:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;

    invoke-static {v1}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v0

    check-cast v0, LX/0jo;

    const/16 v2, 0x2f7e

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->a(Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;LX/0jo;LX/0Ot;)V

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2069618
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "place_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2069606
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->r:LX/0h5;

    if-nez v0, :cond_0

    .line 2069607
    :goto_0
    return-void

    .line 2069608
    :cond_0
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->r:LX/0h5;

    const v1, 0x7f08312d

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2069609
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {p0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2069610
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2069611
    move-object v0, v0

    .line 2069612
    const/4 v1, 0x1

    .line 2069613
    iput-boolean v1, v0, LX/108;->d:Z

    .line 2069614
    move-object v0, v0

    .line 2069615
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2069616
    iget-object v1, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->r:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2069617
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->r:LX/0h5;

    new-instance v1, LX/E1C;

    invoke-direct {v1, p0}, LX/E1C;-><init>(Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2069594
    invoke-direct {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->b()Ljava/lang/String;

    move-result-object v1

    .line 2069595
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2069596
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CeT;

    .line 2069597
    new-instance v2, LX/4KR;

    invoke-direct {v2}, LX/4KR;-><init>()V

    .line 2069598
    const-string v3, "page_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2069599
    move-object v2, v2

    .line 2069600
    new-instance v3, LX/Ceb;

    invoke-direct {v3}, LX/Ceb;-><init>()V

    move-object v3, v3

    .line 2069601
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/Ceb;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2069602
    iget-object v2, v0, LX/CeT;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2069603
    :cond_0
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "gravity_undo_hide_place_tips"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->setResult(ILandroid/content/Intent;)V

    .line 2069604
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->finish()V

    .line 2069605
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2069583
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2069584
    invoke-static {p0, p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2069585
    const v0, 0x7f030f83

    invoke-virtual {p0, v0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->setContentView(I)V

    .line 2069586
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2069587
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->r:LX/0h5;

    .line 2069588
    invoke-direct {p0}, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->l()V

    .line 2069589
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2069590
    if-nez v0, :cond_0

    .line 2069591
    iget-object v0, p0, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;->p:LX/0jo;

    sget-object v1, LX/0cQ;->PLACE_TIPS_BLACKLIST_PROMPT_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-interface {v0, v1}, LX/0jo;->a(I)LX/0jq;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-interface {v0, v1}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2069592
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2069593
    :cond_0
    return-void
.end method
