.class public Lcom/facebook/appupdate/WaitForInitActivity;
.super Landroid/app/Activity;
.source ""


# instance fields
.field private final a:LX/EeL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2153459
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2153460
    new-instance v0, LX/Eeo;

    invoke-direct {v0, p0}, LX/Eeo;-><init>(Lcom/facebook/appupdate/WaitForInitActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/WaitForInitActivity;->a:LX/EeL;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1cb7e92b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153461
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2153462
    const v1, 0x7f031605

    invoke-virtual {p0, v1}, Lcom/facebook/appupdate/WaitForInitActivity;->setContentView(I)V

    .line 2153463
    iget-object v1, p0, Lcom/facebook/appupdate/WaitForInitActivity;->a:LX/EeL;

    invoke-static {v1}, LX/1wl;->a(LX/EeL;)V

    .line 2153464
    const/16 v1, 0x23

    const v2, -0x51ba270

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7eed0653

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153465
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 2153466
    iget-object v1, p0, Lcom/facebook/appupdate/WaitForInitActivity;->a:LX/EeL;

    invoke-static {v1}, LX/1wl;->b(LX/EeL;)V

    .line 2153467
    const/16 v1, 0x23

    const v2, -0x1724dc28

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
