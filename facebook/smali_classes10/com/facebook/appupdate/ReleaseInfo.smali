.class public Lcom/facebook/appupdate/ReleaseInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/appupdate/ReleaseInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final appName:Ljava/lang/String;

.field public final bsDiffDownloadSize:J

.field public final bsDiffDownloadUri:Ljava/lang/String;

.field public final downloadSize:J

.field public final downloadUri:Ljava/lang/String;

.field public final headerImageUri:Ljava/lang/String;

.field public final iconUri:Ljava/lang/String;

.field public final isHardNag:Z

.field private mHash:I

.field public final packageName:Ljava/lang/String;

.field public final releaseNotes:Ljava/lang/String;

.field public final source:Ljava/lang/String;

.field public final versionCode:I

.field public final versionName:Ljava/lang/String;

.field public final zipDiffDownloadSize:J

.field public final zipDiffDownloadUri:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2153189
    new-instance v0, LX/Eeg;

    invoke-direct {v0}, LX/Eeg;-><init>()V

    sput-object v0, Lcom/facebook/appupdate/ReleaseInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 2153165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153166
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2153167
    const-string v1, "package_name"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    .line 2153168
    const-string v1, "version_code"

    const/4 v2, -0x1

    .line 2153169
    invoke-static {v0, v1}, LX/Ef2;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    :cond_0
    move v1, v2

    .line 2153170
    iput v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    .line 2153171
    const-string v1, "download_uri"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    .line 2153172
    const-string v1, "bs_diff_download_uri"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    .line 2153173
    const-string v1, "zip_diff_download_uri"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    .line 2153174
    const-string v1, "is_hard_nag"

    .line 2153175
    const/4 v2, 0x0

    .line 2153176
    invoke-static {v0, v1}, LX/Ef2;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    :cond_1
    move v2, v2

    .line 2153177
    move v1, v2

    .line 2153178
    iput-boolean v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    .line 2153179
    const-string v1, "app_name"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    .line 2153180
    const-string v1, "icon_uri"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    .line 2153181
    const-string v1, "header_image_uri"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    .line 2153182
    const-string v1, "version_name"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    .line 2153183
    const-string v1, "release_notes"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    .line 2153184
    const-string v1, "download_size"

    invoke-static {v0, v1, v4, v5}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    .line 2153185
    const-string v1, "bs_diff_download_size"

    invoke-static {v0, v1, v4, v5}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    .line 2153186
    const-string v1, "zip_diff_download_size"

    invoke-static {v0, v1, v4, v5}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    .line 2153187
    const-string v1, "source"

    invoke-static {v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    .line 2153188
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
    .locals 2

    .prologue
    .line 2153148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153149
    iput-object p1, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    .line 2153150
    iput p2, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    .line 2153151
    iput-object p3, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    .line 2153152
    iput-object p4, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    .line 2153153
    iput-object p5, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    .line 2153154
    iput-boolean p6, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    .line 2153155
    iput-object p7, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    .line 2153156
    iput-object p8, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    .line 2153157
    iput-object p9, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    .line 2153158
    iput-object p10, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    .line 2153159
    iput-object p11, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    .line 2153160
    iput-wide p12, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    .line 2153161
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    .line 2153162
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    .line 2153163
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    .line 2153164
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 20

    .prologue
    .line 2153068
    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v14, -0x1

    const-wide/16 v16, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-wide/from16 v12, p10

    move-object/from16 v18, p12

    invoke-direct/range {v0 .. v18}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 2153069
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2153142
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 2153143
    const/4 v0, 0x1

    .line 2153144
    :goto_0
    return v0

    .line 2153145
    :cond_0
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 2153146
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2153147
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 2153125
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2153126
    const-string v1, "package_name"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153127
    const-string v1, "version_code"

    iget v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 2153128
    const-string v1, "download_uri"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153129
    const-string v1, "bs_diff_download_uri"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153130
    const-string v1, "zip_diff_download_uri"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153131
    const-string v1, "is_hard_nag"

    iget-boolean v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    invoke-static {v0, v1, v2}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)V

    .line 2153132
    const-string v1, "app_name"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153133
    const-string v1, "icon_uri"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153134
    const-string v1, "header_image_uri"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153135
    const-string v1, "version_name"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153136
    const-string v1, "release_notes"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153137
    const-string v1, "download_size"

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    invoke-static {v0, v1, v2, v3}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 2153138
    const-string v1, "bs_diff_download_size"

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    invoke-static {v0, v1, v2, v3}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 2153139
    const-string v1, "zip_diff_download_size"

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    invoke-static {v0, v1, v2, v3}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 2153140
    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153141
    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 2153124
    iget-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2153123
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2153117
    if-ne p0, p1, :cond_1

    .line 2153118
    :cond_0
    :goto_0
    return v0

    .line 2153119
    :cond_1
    instance-of v2, p1, Lcom/facebook/appupdate/ReleaseInfo;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/appupdate/ReleaseInfo;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2153120
    goto :goto_0

    .line 2153121
    :cond_3
    check-cast p1, Lcom/facebook/appupdate/ReleaseInfo;

    .line 2153122
    iget v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    iget v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    iget-boolean v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    iget-wide v4, p1, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    iget-wide v4, p1, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    iget-wide v4, p1, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/appupdate/ReleaseInfo;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2153087
    iget v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->mHash:I

    .line 2153088
    if-nez v0, :cond_0

    .line 2153089
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    .line 2153090
    :goto_0
    iget v3, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    add-int/2addr v3, v0

    .line 2153091
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v3, v0

    .line 2153092
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v3, v0

    .line 2153093
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 2153094
    iget-boolean v3, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    if-eqz v3, :cond_5

    :goto_4
    add-int/2addr v1, v0

    .line 2153095
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v2

    :goto_5
    add-int/2addr v1, v0

    .line 2153096
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v2

    :goto_6
    add-int/2addr v1, v0

    .line 2153097
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    .line 2153098
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v2

    :goto_8
    add-int/2addr v1, v0

    .line 2153099
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v2

    :goto_9
    add-int/2addr v0, v1

    .line 2153100
    int-to-long v0, v0

    iget-wide v4, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    add-long/2addr v0, v4

    long-to-int v0, v0

    .line 2153101
    int-to-long v0, v0

    iget-wide v4, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    add-long/2addr v0, v4

    long-to-int v0, v0

    .line 2153102
    int-to-long v0, v0

    iget-wide v4, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    add-long/2addr v0, v4

    long-to-int v0, v0

    .line 2153103
    iget-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    if-nez v1, :cond_b

    :goto_a
    add-int/2addr v0, v2

    .line 2153104
    iput v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->mHash:I

    .line 2153105
    :cond_0
    return v0

    .line 2153106
    :cond_1
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2153107
    :cond_2
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2153108
    :cond_3
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2153109
    :cond_4
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    move v1, v2

    .line 2153110
    goto :goto_4

    .line 2153111
    :cond_6
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 2153112
    :cond_7
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 2153113
    :cond_8
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 2153114
    :cond_9
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 2153115
    :cond_a
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 2153116
    :cond_b
    iget-object v1, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2153070
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153071
    iget v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2153072
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153073
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153074
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153075
    iget-boolean v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->isHardNag:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2153076
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153077
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->iconUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153078
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->headerImageUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153079
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153080
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153081
    iget-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2153082
    iget-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2153083
    iget-wide v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->zipDiffDownloadSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2153084
    iget-object v0, p0, Lcom/facebook/appupdate/ReleaseInfo;->source:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2153085
    return-void

    .line 2153086
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
