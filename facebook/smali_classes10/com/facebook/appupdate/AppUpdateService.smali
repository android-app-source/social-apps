.class public Lcom/facebook/appupdate/AppUpdateService;
.super LX/EeV;
.source ""


# instance fields
.field private a:LX/1wl;

.field private b:LX/1wo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2152776
    invoke-direct {p0}, LX/EeV;-><init>()V

    return-void
.end method

.method private a(J)Z
    .locals 7

    .prologue
    .line 2152797
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->b:LX/1wo;

    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 2152798
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v2

    .line 2152799
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-eqz v3, :cond_0

    iget-wide v2, v2, LX/EeX;->downloadId:J

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 2152800
    invoke-virtual {v0}, LX/EeS;->f()V

    goto :goto_0

    .line 2152801
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152793
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->b:LX/1wo;

    invoke-virtual {v0, p1}, LX/1wo;->a(Ljava/lang/String;)LX/EeS;

    move-result-object v0

    .line 2152794
    if-eqz v0, :cond_0

    .line 2152795
    invoke-virtual {v0}, LX/EeS;->b()Z

    .line 2152796
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152802
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->a:LX/1wl;

    invoke-virtual {v0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v0

    .line 2152803
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2152804
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->b:LX/1wo;

    invoke-virtual {v0, p1}, LX/1wo;->a(Ljava/lang/String;)LX/EeS;

    move-result-object v0

    .line 2152805
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    iget-object v1, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x6

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2152806
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    .line 2152807
    iget-object v1, v0, LX/EeX;->localFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2152808
    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateService;->a:LX/1wl;

    invoke-virtual {v2}, LX/1wl;->h()LX/1wh;

    move-result-object v2

    .line 2152809
    const-string v3, "appupdate_install_start"

    invoke-virtual {v0}, LX/EeX;->c()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2152810
    const-string v3, "appupdate_install_start"

    iget-object v4, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {v0}, LX/EeX;->d()LX/Eeh;

    move-result-object v0

    const-string v5, "task_start"

    invoke-virtual {v2, v3, v4, v0, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2152811
    invoke-static {p0, v1}, LX/1sY;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/AppUpdateService;->startActivity(Landroid/content/Intent;)V

    .line 2152812
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152789
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->b:LX/1wo;

    invoke-virtual {v0, p1}, LX/1wo;->a(Ljava/lang/String;)LX/EeS;

    move-result-object v0

    .line 2152790
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    iget-object v1, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2152791
    invoke-virtual {v0}, LX/EeS;->c()Z

    .line 2152792
    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final a(LX/1wl;)V
    .locals 1

    .prologue
    .line 2152786
    iput-object p1, p0, Lcom/facebook/appupdate/AppUpdateService;->a:LX/1wl;

    .line 2152787
    invoke-virtual {p1}, LX/1wl;->e()LX/1wo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appupdate/AppUpdateService;->b:LX/1wo;

    .line 2152788
    return-void
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2152777
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2152778
    :goto_1
    return v0

    .line 2152779
    :sswitch_0
    const-string v3, "download_complete"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v3, "start_download"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "start_install"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "restart_download"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 2152780
    :pswitch_0
    const-string v0, "download_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2152781
    invoke-direct {p0, v0, v1}, Lcom/facebook/appupdate/AppUpdateService;->a(J)Z

    move-result v0

    goto :goto_1

    .line 2152782
    :pswitch_1
    const-string v0, "operation_uuid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2152783
    invoke-direct {p0, v0}, Lcom/facebook/appupdate/AppUpdateService;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2152784
    :pswitch_2
    const-string v0, "operation_uuid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/appupdate/AppUpdateService;->b(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2152785
    :pswitch_3
    const-string v0, "operation_uuid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/appupdate/AppUpdateService;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4b8b4348 -> :sswitch_3
        -0x3210fc3b -> :sswitch_1
        -0x2ecb4cf0 -> :sswitch_0
        0x4f70ebe -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
