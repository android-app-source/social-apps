.class public final Lcom/facebook/appupdate/AppUpdateOperation$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EeO;

.field public final synthetic b:LX/EeS;


# direct methods
.method public constructor <init>(LX/EeS;LX/EeO;)V
    .locals 0

    .prologue
    .line 2152469
    iput-object p1, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    iput-object p2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->a:LX/EeO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2152470
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    .line 2152471
    :try_start_0
    iget-object v0, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0x8

    invoke-static {v0, v2}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->a:LX/EeO;

    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    iget-object v2, v2, LX/EeS;->w:LX/EeO;

    if-ne v0, v2, :cond_3

    .line 2152472
    :cond_0
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->a:LX/EeO;

    invoke-interface {v0, v1}, LX/EeO;->a(LX/EeX;)LX/EeY;

    move-result-object v0

    .line 2152473
    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    iget-object v3, v0, LX/EeY;->a:LX/EeX;

    invoke-static {v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeX;)V

    .line 2152474
    iget-object v2, v0, LX/EeY;->b:LX/EeO;

    if-eqz v2, :cond_3

    .line 2152475
    sget-boolean v2, LX/EeM;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/EeY;->b:LX/EeO;

    instance-of v2, v2, LX/Eel;

    if-nez v2, :cond_1

    iget-object v2, v0, LX/EeY;->b:LX/EeO;

    instance-of v2, v2, LX/Een;

    if-eqz v2, :cond_2

    .line 2152476
    :cond_1
    const-string v2, "Continuing next task %s for %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, LX/EeY;->b:LX/EeO;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152477
    :cond_2
    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    iget-object v3, v0, LX/EeY;->b:LX/EeO;

    iget-wide v4, v0, LX/EeY;->c:J

    invoke-static {v2, v3, v4, v5}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2152478
    :cond_3
    :goto_0
    return-void

    .line 2152479
    :catch_0
    move-exception v0

    .line 2152480
    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    invoke-static {v2, v0}, LX/EeS;->a$redex0(LX/EeS;Ljava/lang/Throwable;)V

    .line 2152481
    iget-object v2, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    iget-object v2, v2, LX/EeS;->b:LX/EeU;

    invoke-virtual {v2}, LX/EeU;->a()V

    .line 2152482
    new-instance v2, LX/EeW;

    invoke-direct {v2, v1}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2152483
    iput-object v1, v2, LX/EeW;->e:Ljava/lang/Integer;

    .line 2152484
    move-object v1, v2

    .line 2152485
    iput-object v0, v1, LX/EeW;->k:Ljava/lang/Throwable;

    .line 2152486
    move-object v1, v1

    .line 2152487
    instance-of v0, v0, LX/Eey;

    if-eqz v0, :cond_4

    .line 2152488
    iput-boolean v6, v1, LX/EeW;->c:Z

    .line 2152489
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    invoke-static {v0}, LX/EeS;->l(LX/EeS;)V

    .line 2152490
    :cond_4
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdateOperation$2;->b:LX/EeS;

    invoke-virtual {v1}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    invoke-static {v0, v1}, LX/EeS;->b$redex0(LX/EeS;LX/EeX;)Z

    goto :goto_0
.end method
