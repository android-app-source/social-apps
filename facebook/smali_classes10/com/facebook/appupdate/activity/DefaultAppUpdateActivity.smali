.class public Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source ""


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/Button;

.field private D:Landroid/view/ViewGroup;

.field private E:Landroid/widget/Button;

.field private F:Landroid/widget/Button;

.field private G:Landroid/view/ViewGroup;

.field private H:Landroid/widget/Button;

.field private I:Landroid/widget/Button;

.field private J:Landroid/view/ViewGroup;

.field private K:Landroid/widget/TextView;

.field private L:Landroid/widget/Button;

.field private final M:LX/1sX;

.field public final N:Ljava/lang/Runnable;

.field private final O:Landroid/view/View$OnClickListener;

.field private final P:Landroid/view/View$OnClickListener;

.field private final Q:Landroid/view/View$OnClickListener;

.field private final R:Landroid/view/View$OnClickListener;

.field private final S:Landroid/view/View$OnClickListener;

.field public q:LX/1wo;

.field public r:LX/1wh;

.field public s:Landroid/os/Handler;

.field public t:Lcom/facebook/appupdate/ReleaseInfo;

.field private u:Landroid/net/Uri;

.field public v:LX/EeS;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2153617
    const-class v0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;

    sput-object v0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2153609
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 2153610
    new-instance v0, LX/Eeq;

    invoke-direct {v0, p0}, LX/Eeq;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->M:LX/1sX;

    .line 2153611
    new-instance v0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity$2;

    invoke-direct {v0, p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity$2;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->N:Ljava/lang/Runnable;

    .line 2153612
    new-instance v0, LX/Eer;

    invoke-direct {v0, p0}, LX/Eer;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->O:Landroid/view/View$OnClickListener;

    .line 2153613
    new-instance v0, LX/Ees;

    invoke-direct {v0, p0}, LX/Ees;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->P:Landroid/view/View$OnClickListener;

    .line 2153614
    new-instance v0, LX/Eet;

    invoke-direct {v0, p0}, LX/Eet;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->Q:Landroid/view/View$OnClickListener;

    .line 2153615
    new-instance v0, LX/Eeu;

    invoke-direct {v0, p0}, LX/Eeu;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->R:Landroid/view/View$OnClickListener;

    .line 2153616
    new-instance v0, LX/Eev;

    invoke-direct {v0, p0}, LX/Eev;-><init>(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->S:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2153604
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->w:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->t:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v1, v1, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2153605
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageItemInfo;->icon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2153606
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->y:Landroid/widget/TextView;

    const-string v1, "New in version %1$s"

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->t:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v2, v2, Lcom/facebook/appupdate/ReleaseInfo;->versionName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2153607
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->t:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v1, v1, Lcom/facebook/appupdate/ReleaseInfo;->releaseNotes:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2153608
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2153599
    const-string v0, "AppUpdateLib"

    const-string v1, "Update Operation failed!"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2153600
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Update failed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2153601
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2153602
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2153603
    return-void
.end method

.method private b(Ljava/lang/String;)LX/EeS;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2153589
    :try_start_0
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->q:LX/1wo;

    invoke-virtual {v0, p1}, LX/1wo;->a(Ljava/lang/String;)LX/EeS;

    move-result-object v0

    .line 2153590
    if-nez v0, :cond_0

    .line 2153591
    const-string v1, "AppUpdateLib"

    const-string v2, "No such operation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2153592
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No such operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2153593
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2153594
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2153595
    :cond_0
    :goto_0
    return-object v0

    .line 2153596
    :catch_0
    move-exception v0

    .line 2153597
    invoke-direct {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->a(Ljava/lang/Throwable;)V

    .line 2153598
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V
    .locals 3

    .prologue
    .line 2153618
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    invoke-virtual {v0}, LX/EeS;->b()Z

    .line 2153619
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->r:LX/1wh;

    const-string v1, "appupdateactivity_download_and_install"

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->t:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {v2}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1wh;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2153620
    return-void
.end method

.method public static i(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2153565
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    .line 2153566
    iget-object v1, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2153567
    :goto_0
    :pswitch_0
    return-void

    .line 2153568
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153569
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2153570
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153571
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153572
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->B:Landroid/widget/TextView;

    const-string v1, "Starting..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2153573
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153574
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->A:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153575
    iget-wide v2, v0, LX/EeX;->downloadSize:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2153576
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->B:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Downloading... "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, LX/EeX;->downloadProgress:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, LX/EeX;->downloadSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2153577
    :cond_0
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->B:Landroid/widget/TextView;

    const-string v1, "Downloading... "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2153578
    :pswitch_4
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153579
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153580
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->B:Landroid/widget/TextView;

    const-string v1, "Verifying... "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2153581
    :pswitch_5
    iget-object v0, v0, LX/EeX;->localFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->u:Landroid/net/Uri;

    .line 2153582
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153583
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->G:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2153584
    :pswitch_6
    iget-object v0, v0, LX/EeX;->failureReason:Ljava/lang/Throwable;

    .line 2153585
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->j()V

    .line 2153586
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->J:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153587
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->K:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed with the following error:\n\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2153588
    :pswitch_7
    invoke-virtual {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private j()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2153512
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153513
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153514
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->G:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153515
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->J:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2153516
    return-void
.end method

.method public static k(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V
    .locals 1

    .prologue
    .line 2153562
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->u:Landroid/net/Uri;

    invoke-static {p0, v0}, LX/1sY;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2153563
    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->startActivity(Landroid/content/Intent;)V

    .line 2153564
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5742c454

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2153524
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2153525
    invoke-static {}, LX/1wl;->a()LX/1wl;

    move-result-object v0

    .line 2153526
    invoke-virtual {v0}, LX/1wl;->e()LX/1wo;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->q:LX/1wo;

    .line 2153527
    invoke-virtual {v0}, LX/1wl;->h()LX/1wh;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->r:LX/1wh;

    .line 2153528
    invoke-virtual {v0}, LX/1wl;->j()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->s:Landroid/os/Handler;

    .line 2153529
    const v0, 0x7f030952

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->setContentView(I)V

    .line 2153530
    const v0, 0x7f0d06d8

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->w:Landroid/widget/TextView;

    .line 2153531
    const v0, 0x7f0d17e0

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->x:Landroid/widget/ImageView;

    .line 2153532
    const v0, 0x7f0d17e2

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->y:Landroid/widget/TextView;

    .line 2153533
    const v0, 0x7f0d17e3

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->z:Landroid/widget/TextView;

    .line 2153534
    const v0, 0x7f0d17e4

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->A:Landroid/view/ViewGroup;

    .line 2153535
    const v0, 0x7f0d17e5

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->B:Landroid/widget/TextView;

    .line 2153536
    const v0, 0x7f0d17e6

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->C:Landroid/widget/Button;

    .line 2153537
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->C:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153538
    const v0, 0x7f0d17e7

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->D:Landroid/view/ViewGroup;

    .line 2153539
    const v0, 0x7f0d17e8

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->E:Landroid/widget/Button;

    .line 2153540
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->E:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153541
    const v0, 0x7f0d17e9

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->F:Landroid/widget/Button;

    .line 2153542
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->F:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153543
    const v0, 0x7f0d17ea

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->G:Landroid/view/ViewGroup;

    .line 2153544
    const v0, 0x7f0d17eb

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->H:Landroid/widget/Button;

    .line 2153545
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->H:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153546
    const v0, 0x7f0d17ec

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->I:Landroid/widget/Button;

    .line 2153547
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->I:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->S:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153548
    const v0, 0x7f0d17ed

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->J:Landroid/view/ViewGroup;

    .line 2153549
    const v0, 0x7f0d17ee

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->K:Landroid/widget/TextView;

    .line 2153550
    const v0, 0x7f0d17ef

    invoke-virtual {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->L:Landroid/widget/Button;

    .line 2153551
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->L:Landroid/widget/Button;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2153552
    invoke-virtual {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2153553
    const-string v2, "operation_uuid"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2153554
    invoke-direct {p0, v0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->b(Ljava/lang/String;)LX/EeS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    .line 2153555
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    if-nez v0, :cond_0

    .line 2153556
    invoke-virtual {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->finish()V

    .line 2153557
    const/16 v0, 0x23

    const v2, 0x35f2727

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2153558
    :goto_0
    return-void

    .line 2153559
    :cond_0
    iget-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    iget-object v0, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iput-object v0, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->t:Lcom/facebook/appupdate/ReleaseInfo;

    .line 2153560
    invoke-direct {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->a()V

    .line 2153561
    const v0, -0x5fe73ac0

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x61e43beb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153521
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->M:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->b(LX/1sX;)Z

    .line 2153522
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 2153523
    const/16 v1, 0x23

    const v2, 0x3fa8c5dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x56edd3d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2153517
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 2153518
    invoke-static {p0}, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->i(Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;)V

    .line 2153519
    iget-object v1, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->v:LX/EeS;

    iget-object v2, p0, Lcom/facebook/appupdate/activity/DefaultAppUpdateActivity;->M:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->a(LX/1sX;)Z

    .line 2153520
    const/16 v1, 0x23

    const v2, -0x65f7a54d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
