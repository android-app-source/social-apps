.class public Lcom/facebook/appupdate/DownloadNotificationClickReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2152934
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;J)V
    .locals 10

    .prologue
    const/high16 v9, 0x10000000

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 2152935
    invoke-static {}, LX/1wl;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2152936
    invoke-static {}, LX/1wl;->a()LX/1wl;

    move-result-object v1

    .line 2152937
    invoke-virtual {v1}, LX/1wl;->e()LX/1wo;

    move-result-object v0

    .line 2152938
    invoke-virtual {v0}, LX/1wo;->a()V

    .line 2152939
    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 2152940
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    .line 2152941
    iget-wide v4, v0, LX/EeX;->downloadId:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    cmp-long v3, p1, v6

    if-nez v3, :cond_1

    invoke-static {v0}, Lcom/facebook/appupdate/DownloadNotificationClickReceiver;->a(LX/EeX;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-wide v4, v0, LX/EeX;->downloadId:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 2152942
    :cond_2
    sget-boolean v2, LX/EeM;->a:Z

    if-eqz v2, :cond_3

    .line 2152943
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting AppUpdateActivity for download "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152944
    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {v1}, LX/1wl;->i()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2152945
    const-string v1, "operation_uuid"

    iget-object v0, v0, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2152946
    invoke-virtual {v2, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2152947
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2152948
    :cond_4
    :goto_0
    return-void

    .line 2152949
    :cond_5
    new-instance v0, LX/1sV;

    invoke-direct {v0, p0}, LX/1sV;-><init>(Landroid/content/Context;)V

    .line 2152950
    new-instance v1, LX/EeT;

    invoke-direct {v1, v0}, LX/EeT;-><init>(LX/1sV;)V

    .line 2152951
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2152952
    iget-object v0, v1, LX/EeT;->a:LX/1sV;

    invoke-virtual {v0}, LX/1sV;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2152953
    :try_start_0
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [B

    invoke-static {v0}, LX/EeT;->a([B)LX/EeX;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 2152954
    :catch_0
    goto :goto_1

    .line 2152955
    :cond_6
    move-object v0, v2

    .line 2152956
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeX;

    .line 2152957
    iget-wide v2, v0, LX/EeX;->downloadId:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    cmp-long v2, p1, v6

    if-nez v2, :cond_8

    invoke-static {v0}, Lcom/facebook/appupdate/DownloadNotificationClickReceiver;->a(LX/EeX;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_8
    iget-wide v2, v0, LX/EeX;->downloadId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_7

    .line 2152958
    :cond_9
    sget-boolean v1, LX/EeM;->a:Z

    if-eqz v1, :cond_a

    .line 2152959
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting WaitForInitActivity for download "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152960
    :cond_a
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/appupdate/WaitForInitActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2152961
    const-string v2, "operation_uuid"

    iget-object v0, v0, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2152962
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2152963
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2152964
    :catch_1
    goto :goto_1
.end method

.method private static a(LX/EeX;)Z
    .locals 2

    .prologue
    .line 2152965
    const/4 v0, 0x2

    iget-object v1, p0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/3CW;->b(II)I

    move-result v0

    if-gtz v0, :cond_0

    const/16 v0, 0x8

    iget-object v1, p0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/3CW;->b(II)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x26

    const v1, 0x73fb5712

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2152966
    const-wide/16 v0, 0x0

    .line 2152967
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    .line 2152968
    const-string v3, "extra_click_download_ids"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v3

    .line 2152969
    if-eqz v3, :cond_0

    array-length v4, v3

    if-lez v4, :cond_0

    .line 2152970
    const/4 v0, 0x0

    aget-wide v0, v3, v0

    .line 2152971
    :cond_0
    invoke-static {p1, v0, v1}, Lcom/facebook/appupdate/DownloadNotificationClickReceiver;->a(Landroid/content/Context;J)V

    .line 2152972
    const/16 v0, 0x27

    const v1, -0x200ca9db

    invoke-static {p2, v5, v0, v1, v2}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
