.class public Lcom/facebook/entitycards/collect/ParcelableOffsetArray;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/entitycards/collect/ParcelableOffsetArray;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2166627
    new-instance v0, LX/En5;

    invoke-direct {v0}, LX/En5;-><init>()V

    sput-object v0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/En3;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2166628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166629
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166630
    invoke-virtual {p1}, LX/En2;->c()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    move v1, v0

    move v2, v0

    .line 2166631
    :goto_0
    invoke-virtual {p1}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2166632
    invoke-virtual {p1, v1}, LX/En2;->a(I)I

    move-result v3

    .line 2166633
    invoke-virtual {p1, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2166634
    if-nez v1, :cond_0

    move v2, v3

    .line 2166635
    :cond_0
    iget-object v3, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    aput-object v0, v3, v1

    .line 2166636
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2166637
    :cond_1
    iput v2, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a:I

    .line 2166638
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2166639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a:I

    .line 2166641
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    .line 2166642
    return-void
.end method


# virtual methods
.method public final a()LX/En3;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2166643
    new-instance v1, LX/En3;

    iget v0, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a:I

    invoke-direct {v1, v0}, LX/En3;-><init>(I)V

    .line 2166644
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2166645
    iget v2, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2166646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2166647
    :cond_0
    invoke-virtual {v1}, LX/En3;->a()V

    .line 2166648
    return-object v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2166649
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2166650
    iget v0, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2166651
    iget-object v0, p0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 2166652
    return-void
.end method
