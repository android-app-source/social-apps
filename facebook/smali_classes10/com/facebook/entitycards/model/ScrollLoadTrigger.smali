.class public Lcom/facebook/entitycards/model/ScrollLoadTrigger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/entitycards/model/ScrollLoadTrigger;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/Enq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2167930
    new-instance v0, LX/Eo2;

    invoke-direct {v0}, LX/Eo2;-><init>()V

    sput-object v0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Enq;)V
    .locals 0

    .prologue
    .line 2167931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167932
    iput-object p1, p0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;->a:LX/Enq;

    .line 2167933
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2167934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167935
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Enq;

    iput-object v0, p0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;->a:LX/Enq;

    .line 2167936
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2167937
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2167938
    const-class v0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "direction"

    iget-object v2, p0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;->a:LX/Enq;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2167939
    iget-object v0, p0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;->a:LX/Enq;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2167940
    return-void
.end method
