.class public Lcom/facebook/entitycards/model/ScrollLoadError;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/entitycards/model/ScrollLoadError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2167923
    new-instance v0, LX/Eo1;

    invoke-direct {v0}, LX/Eo1;-><init>()V

    sput-object v0, Lcom/facebook/entitycards/model/ScrollLoadError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2167920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167921
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/model/ScrollLoadError;->a:Ljava/lang/String;

    .line 2167922
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2167924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167925
    iput-object p1, p0, Lcom/facebook/entitycards/model/ScrollLoadError;->a:Ljava/lang/String;

    .line 2167926
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2167919
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2167917
    iget-object v0, p0, Lcom/facebook/entitycards/model/ScrollLoadError;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167918
    return-void
.end method
