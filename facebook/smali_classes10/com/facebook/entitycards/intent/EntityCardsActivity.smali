.class public Lcom/facebook/entitycards/intent/EntityCardsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f1;


# instance fields
.field public p:LX/EnP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Emx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2166717
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/entitycards/intent/EntityCardsActivity;LX/EnP;LX/Emx;)V
    .locals 0

    .prologue
    .line 2166718
    iput-object p1, p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;->p:LX/EnP;

    iput-object p2, p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;->q:LX/Emx;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;

    invoke-static {v1}, LX/EnP;->a(LX/0QB;)LX/EnP;

    move-result-object v0

    check-cast v0, LX/EnP;

    invoke-static {v1}, LX/Emx;->a(LX/0QB;)LX/Emx;

    move-result-object v1

    check-cast v1, LX/Emx;

    invoke-static {p0, v0, v1}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->a(Lcom/facebook/entitycards/intent/EntityCardsActivity;LX/EnP;LX/Emx;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2166719
    const-string v0, "entity_cards"

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2166720
    invoke-virtual {p0}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/EnS;->c(Landroid/os/Bundle;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2166721
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2166722
    const-string v2, "profile_id"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166723
    return-object v1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2166724
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2166725
    invoke-static {p0, p0}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2166726
    invoke-virtual {p0}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2166727
    if-nez p1, :cond_0

    .line 2166728
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    .line 2166729
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;->q:LX/Emx;

    sget-object v2, LX/Emw;->ACTIVITY_CREATE:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->a(LX/Emw;)V

    .line 2166730
    invoke-static {p0, v0}, LX/EnP;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2166731
    :cond_0
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2166732
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2166733
    invoke-virtual {p0, v0, v0}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->overridePendingTransition(II)V

    .line 2166734
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2166735
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;->p:LX/EnP;

    if-eqz v0, :cond_0

    .line 2166736
    invoke-static {p0}, LX/EnP;->a(Landroid/content/Context;)Lcom/facebook/entitycards/intent/EntityCardsFragment;

    move-result-object v0

    .line 2166737
    if-eqz v0, :cond_0

    .line 2166738
    const/4 v1, 0x1

    sget-object v2, LX/Emm;->BACK_BUTTON_TAP:LX/Emm;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(ZLX/Emm;)V

    .line 2166739
    :goto_0
    return-void

    .line 2166740
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/entitycards/intent/EntityCardsActivity;->finish()V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x59eea1c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2166741
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2166742
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsActivity;->q:LX/Emx;

    sget-object v2, LX/Emw;->ACTIVITY_CREATE:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->b(LX/Emw;)V

    .line 2166743
    const/16 v1, 0x23

    const v2, -0x116b13d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
