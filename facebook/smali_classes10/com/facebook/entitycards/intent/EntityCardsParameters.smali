.class public Lcom/facebook/entitycards/intent/EntityCardsParameters;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/entitycards/intent/EntityCardsParameters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2167245
    new-instance v0, LX/EnV;

    invoke-direct {v0}, LX/EnV;-><init>()V

    sput-object v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2167236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a:Ljava/lang/String;

    .line 2167238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->b:Ljava/lang/String;

    .line 2167239
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    .line 2167240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    .line 2167241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->e:Ljava/lang/String;

    .line 2167242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->f:Ljava/lang/String;

    .line 2167243
    invoke-direct {p0}, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a()V

    .line 2167244
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167228
    iput-object p1, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a:Ljava/lang/String;

    .line 2167229
    iput-object p2, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->b:Ljava/lang/String;

    .line 2167230
    iput-object p3, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    .line 2167231
    iput-object p4, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    .line 2167232
    iput-object p5, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->e:Ljava/lang/String;

    .line 2167233
    iput-object p6, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->f:Ljava/lang/String;

    .line 2167234
    invoke-direct {p0}, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a()V

    .line 2167235
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2167221
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167222
    :goto_0
    return-void

    .line 2167223
    :cond_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 2167224
    invoke-virtual {v1}, LX/0Rf;->size()I

    move-result v0

    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v2, "Duplicate entity IDs are not supported"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2167225
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "initialEntityId must be in initialEntityIds"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    .line 2167226
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2167220
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2167213
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167214
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167215
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2167216
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167217
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167218
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2167219
    return-void
.end method
