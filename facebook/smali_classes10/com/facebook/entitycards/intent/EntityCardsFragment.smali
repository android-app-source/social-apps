.class public Lcom/facebook/entitycards/intent/EntityCardsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0f2;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/String;

.field private C:I

.field private D:I

.field private E:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end field

.field private F:Z

.field public G:Z

.field public H:LX/BYE;

.field private I:LX/0hc;

.field private J:LX/Emq;

.field private K:LX/Emy;

.field private L:LX/Ems;

.field public M:LX/195;

.field private N:LX/Emk;

.field public b:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/accessibility/AccessibilityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/En8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ena;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Emt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Emz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Emr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/En1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/EnS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/EnK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Emx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Eml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0ja;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Emd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final r:LX/EnL;

.field public s:Landroid/support/v4/view/ViewPager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Landroid/view/View;

.field public u:LX/Emc;

.field private v:LX/En7;

.field public w:LX/EnY;

.field public x:LX/EnI;

.field public y:LX/Enl;

.field private z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2167019
    const-class v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;

    const-string v1, "entity_cards"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2167015
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2167016
    new-instance v0, LX/EnL;

    invoke-direct {v0, p0}, LX/EnL;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->r:LX/EnL;

    .line 2167017
    iput-boolean v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->F:Z

    .line 2167018
    iput-boolean v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->G:Z

    return-void
.end method

.method private static a(Lcom/facebook/entitycards/intent/EntityCardsFragment;LX/1My;Ljava/util/concurrent/Executor;Landroid/view/accessibility/AccessibilityManager;LX/En8;LX/Ena;LX/Emt;LX/Emz;LX/Emr;LX/En1;LX/EnS;LX/EnK;LX/Emx;LX/Eml;LX/193;LX/0ja;LX/Emd;)V
    .locals 1

    .prologue
    .line 2167014
    iput-object p1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b:LX/1My;

    iput-object p2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->c:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->d:Landroid/view/accessibility/AccessibilityManager;

    iput-object p4, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->e:LX/En8;

    iput-object p5, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->f:LX/Ena;

    iput-object p6, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->g:LX/Emt;

    iput-object p7, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->h:LX/Emz;

    iput-object p8, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->i:LX/Emr;

    iput-object p9, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->j:LX/En1;

    iput-object p10, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->k:LX/EnS;

    iput-object p11, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->l:LX/EnK;

    iput-object p12, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    iput-object p13, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->n:LX/Eml;

    iput-object p14, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->o:LX/193;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->p:LX/0ja;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->q:LX/Emd;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 19

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-static/range {v17 .. v17}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-static/range {v17 .. v17}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static/range {v17 .. v17}, LX/0sY;->a(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    const-class v5, LX/En8;

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/En8;

    const-class v6, LX/Ena;

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Ena;

    const-class v7, LX/Emt;

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Emt;

    const-class v8, LX/Emz;

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Emz;

    const-class v9, LX/Emr;

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Emr;

    const-class v10, LX/En1;

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/En1;

    invoke-static/range {v17 .. v17}, LX/EnS;->a(LX/0QB;)LX/EnS;

    move-result-object v11

    check-cast v11, LX/EnS;

    const-class v12, LX/EnK;

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/EnK;

    invoke-static/range {v17 .. v17}, LX/Emx;->a(LX/0QB;)LX/Emx;

    move-result-object v13

    check-cast v13, LX/Emx;

    const-class v14, LX/Eml;

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/Eml;

    const-class v15, LX/193;

    move-object/from16 v0, v17

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/193;

    invoke-static/range {v17 .. v17}, LX/0ja;->a(LX/0QB;)LX/0ja;

    move-result-object v16

    check-cast v16, LX/0ja;

    const-class v18, LX/Emd;

    invoke-interface/range {v17 .. v18}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/Emd;

    invoke-static/range {v1 .. v17}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(Lcom/facebook/entitycards/intent/EntityCardsFragment;LX/1My;Ljava/util/concurrent/Executor;Landroid/view/accessibility/AccessibilityManager;LX/En8;LX/Ena;LX/Emt;LX/Emz;LX/Emr;LX/En1;LX/EnS;LX/EnK;LX/Emx;LX/Eml;LX/193;LX/0ja;LX/Emd;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/entitycards/intent/EntityCardsFragment;LX/Enl;)V
    .locals 3

    .prologue
    .line 2167009
    invoke-virtual {p1}, LX/Enl;->i()I

    move-result v0

    .line 2167010
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    invoke-virtual {v1, v0}, LX/Ems;->c(I)V

    .line 2167011
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2167012
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->N:LX/Emk;

    invoke-virtual {v1, v0}, LX/3sJ;->B_(I)V

    .line 2167013
    return-void
.end method

.method public static c(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V
    .locals 3

    .prologue
    .line 2167006
    iget-boolean v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->G:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->F:Z

    if-eqz v0, :cond_0

    .line 2167007
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    sget-object v1, LX/Emn;->ENTITY_CARDS_DISPLAYED:LX/Emn;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Emq;->a(LX/Emn;LX/0am;)V

    .line 2167008
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2167005
    const-string v0, "entity_cards"

    return-object v0
.end method

.method public final a(LX/Emm;)V
    .locals 4

    .prologue
    .line 2167020
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    .line 2167021
    sget-object v1, LX/Emm;->SWIPE_DOWN:LX/Emm;

    if-ne p1, v1, :cond_0

    .line 2167022
    iget-object v1, v0, LX/Emq;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    const-string v2, "swipe"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2167023
    :cond_0
    sget-object v1, LX/Emn;->ENTITY_CARDS_DISMISSED:LX/Emn;

    invoke-static {v0, v1}, LX/Emq;->a(LX/Emq;LX/Emn;)LX/0oG;

    move-result-object v1

    .line 2167024
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2167025
    const-string v2, "reason"

    iget-object v3, p1, LX/Emm;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2167026
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2167027
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2167028
    if-eqz v0, :cond_2

    .line 2167029
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2167030
    const-string v2, "entity_cards_visible_id"

    iget-object v3, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    invoke-virtual {v3}, LX/Emc;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2167031
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2167032
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2167033
    :cond_2
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 20
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2166966
    invoke-super/range {p0 .. p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2166967
    const-class v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;

    move-object/from16 v0, p0

    invoke-static {v1, v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2166968
    if-nez p1, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166969
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    sget-object v2, LX/Emw;->FRAGMENT_CREATE:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->a(LX/Emw;)V

    .line 2166970
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v1

    .line 2166971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->k:LX/EnS;

    iget-object v3, v1, Lcom/facebook/entitycards/intent/EntityCardsParameters;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/EnS;->a(Ljava/lang/String;)LX/EoA;

    move-result-object v19

    .line 2166972
    invoke-interface/range {v19 .. v19}, LX/EoA;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->z:Ljava/lang/String;

    .line 2166973
    iget-object v2, v1, Lcom/facebook/entitycards/intent/EntityCardsParameters;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->A:Ljava/lang/String;

    .line 2166974
    iget-object v1, v1, Lcom/facebook/entitycards/intent/EntityCardsParameters;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->B:Ljava/lang/String;

    .line 2166975
    invoke-interface/range {v19 .. v19}, LX/EoA;->c()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->C:I

    .line 2166976
    invoke-interface/range {v19 .. v19}, LX/EoA;->d()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->D:I

    .line 2166977
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->k:LX/EnS;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, LX/EnS;->a(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v8

    .line 2166978
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->h:LX/Emz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->B:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->z:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/Emz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Emy;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->K:LX/Emy;

    .line 2166979
    const/4 v2, 0x0

    .line 2166980
    if-eqz v8, :cond_2

    const-string v1, "friending_location"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2166981
    :goto_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2166982
    invoke-static {v1}, LX/2h7;->valueOf(Ljava/lang/String;)LX/2h7;

    move-result-object v1

    move-object v5, v1

    .line 2166983
    :goto_1
    move-object/from16 v0, v19

    invoke-interface {v0, v8}, LX/EoA;->b(Landroid/os/Bundle;)LX/Jxq;

    move-result-object v6

    .line 2166984
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->i:LX/Emr;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->B:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->z:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->A:Ljava/lang/String;

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-virtual/range {v1 .. v6}, LX/Emr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0am;LX/Jxq;)LX/Emq;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    .line 2166985
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, LX/EnS;->b(Landroid/os/Bundle;Landroid/os/Bundle;)LX/0Px;

    move-result-object v3

    .line 2166986
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    invoke-virtual {v1, v2}, LX/Emq;->a(LX/Emj;)V

    .line 2166987
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b:LX/1My;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, LX/EnS;->c(Landroid/os/Bundle;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->K:LX/Emy;

    sget-object v7, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v1, v19

    invoke-interface/range {v1 .. v8}, LX/EoA;->a(LX/1My;LX/0Px;Ljava/lang/String;LX/Emq;LX/Emy;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/Enl;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Enl;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    .line 2166988
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->g:LX/Emt;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v1, v2, v4}, LX/Emt;->a(LX/Emq;LX/Enl;)LX/Ems;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    .line 2166989
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    invoke-virtual {v1}, LX/Emq;->d()Ljava/lang/String;

    move-result-object v1

    .line 2166990
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->e:LX/En8;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->p:LX/0ja;

    invoke-interface/range {v19 .. v19}, LX/EoA;->b()LX/Enm;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/En8;->a(LX/0ja;LX/Enm;)LX/En7;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->v:LX/En7;

    .line 2166991
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->q:LX/Emd;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->v:LX/En7;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->r:LX/EnL;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->K:LX/Emy;

    invoke-static {v2}, LX/En1;->a(LX/Emy;)LX/En0;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->p:LX/0ja;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->C:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v18, v8

    invoke-virtual/range {v9 .. v18}, LX/Emd;->a(LX/Emj;LX/En7;LX/Enl;LX/EnL;LX/En0;Landroid/view/LayoutInflater;LX/0ja;Ljava/lang/Integer;Landroid/os/Bundle;)LX/Emc;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    .line 2166992
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->n:LX/Eml;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->B:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->z:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->A:Ljava/lang/String;

    move-object v14, v3

    invoke-virtual/range {v9 .. v14}, LX/Eml;->a(LX/Emc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/Emk;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->N:LX/Emk;

    .line 2166993
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->N:LX/Emk;

    invoke-virtual {v2, v3}, LX/Emq;->a(LX/Emj;)V

    .line 2166994
    move-object/from16 v0, v19

    invoke-interface {v0, v8}, LX/EoA;->a(Landroid/os/Bundle;)LX/Emj;

    move-result-object v2

    .line 2166995
    if-eqz v2, :cond_1

    .line 2166996
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    invoke-virtual {v3, v2}, LX/Emq;->a(LX/Emj;)V

    .line 2166997
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->o:LX/193;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_ec_scroll_animation"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->M:LX/195;

    .line 2166998
    new-instance v1, LX/EnM;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, LX/EnM;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->I:LX/0hc;

    .line 2166999
    new-instance v1, LX/BYE;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->I:LX/0hc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->N:LX/Emk;

    invoke-static {v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, LX/BYE;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->H:LX/BYE;

    .line 2167000
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    new-instance v2, LX/EnN;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/EnN;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    invoke-virtual {v1, v2}, LX/Enl;->a(LX/EnN;)V

    .line 2167001
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    sget-object v2, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->a(LX/Emw;)V

    .line 2167002
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v1}, LX/Enl;->e()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->E:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167003
    return-void

    .line 2167004
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    move-object v5, v2

    goto/16 :goto_1
.end method

.method public final a(ZLX/Emm;)V
    .locals 1

    .prologue
    .line 2166964
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    invoke-interface {v0, p1, p2}, LX/EnI;->a(ZLX/Emm;)V

    .line 2166965
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2166961
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 2166962
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2166963
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6e2d9ef2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2166920
    const v0, 0x7f030497

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2166921
    const v0, 0x7f0d0d94

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewStub;

    .line 2166922
    const v0, 0x7f0d0d91

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->t:Landroid/view/View;

    .line 2166923
    iget v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->D:I

    if-ne v0, v9, :cond_2

    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->f:LX/Ena;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    .line 2166924
    new-instance v6, LX/EnZ;

    invoke-direct {v6, v1, v2}, LX/EnZ;-><init>(Landroid/app/Activity;LX/Emj;)V

    .line 2166925
    const/16 v5, 0x1b

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    const/16 v5, 0x2eb

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    .line 2166926
    iput-object p1, v6, LX/EnZ;->a:LX/0Or;

    iput-object p2, v6, LX/EnZ;->b:LX/0Or;

    iput-object v5, v6, LX/EnZ;->c:LX/0Uh;

    .line 2166927
    move-object v0, v6

    .line 2166928
    :goto_0
    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    .line 2166929
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    .line 2166930
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2166931
    invoke-static {v1}, LX/EnS;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, LX/EnY;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2166932
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    .line 2166933
    iget-object v1, v0, LX/Emq;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2166934
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->o:LX/193;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_ec_intro_animation"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    .line 2166935
    iget v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->D:I

    if-ne v0, v9, :cond_3

    new-instance v0, LX/Enc;

    invoke-direct {v0, p0}, LX/Enc;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    :goto_1
    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    .line 2166936
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->E:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/EnO;

    invoke-direct {v1, p0}, LX/EnO;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2166937
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    invoke-interface {v0, v7}, LX/EnI;->a(Landroid/view/ViewStub;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    .line 2166938
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2166939
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->H:LX/BYE;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2166940
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/entitycards/intent/EntityCardsFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/entitycards/intent/EntityCardsFragment$5;-><init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->post(Ljava/lang/Runnable;)Z

    .line 2166941
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const-string v1, "Expected mViewPager\'s layout params to be a MarginLayoutParams, got %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-static {v0, v1, v2}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2166942
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2166943
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->d:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2166944
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    invoke-interface {v1}, LX/EnY;->a()I

    move-result v1

    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2166945
    :cond_0
    :goto_2
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2166946
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b220e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2166947
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2166948
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2166949
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2166950
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v0}, LX/Enl;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166951
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-static {p0, v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a$redex0(Lcom/facebook/entitycards/intent/EntityCardsFragment;LX/Enl;)V

    .line 2166952
    :cond_1
    iput-boolean v9, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->F:Z

    .line 2166953
    invoke-static {p0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->c(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    .line 2166954
    const v0, 0xf861b2b

    invoke-static {v0, v8}, LX/02F;->f(II)V

    return-object v3

    .line 2166955
    :cond_2
    new-instance v0, LX/Enb;

    invoke-direct {v0}, LX/Enb;-><init>()V

    goto/16 :goto_0

    .line 2166956
    :cond_3
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->l:LX/EnK;

    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    iget-object v4, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->t:Landroid/view/View;

    iget-object v5, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, LX/EnK;->a(LX/195;LX/Emq;Landroid/view/View;Landroid/view/View;LX/EnY;Lcom/facebook/entitycards/intent/EntityCardsFragment;)LX/EnJ;

    move-result-object v0

    goto/16 :goto_1

    .line 2166957
    :cond_4
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    invoke-interface {v1}, LX/EnY;->b()I

    move-result v1

    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2166958
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    invoke-interface {v1}, LX/EnI;->a()Landroid/view/View$OnTouchListener;

    move-result-object v1

    .line 2166959
    if-eqz v1, :cond_0

    .line 2166960
    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x11586fc9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2166911
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2166912
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2166913
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    if-eqz v1, :cond_0

    .line 2166914
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v1}, LX/Enl;->f()V

    .line 2166915
    :cond_0
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->J:LX/Emq;

    .line 2166916
    iget-object v2, v1, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2166917
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    const/4 v2, 0x0

    .line 2166918
    iput-object v2, v1, LX/Enl;->c:LX/EnN;

    .line 2166919
    const/16 v1, 0x2b

    const v2, -0x4adc5df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x370a33fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2166905
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2166906
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    .line 2166907
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2166908
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2166909
    iput-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    .line 2166910
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7d9c87e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5b1e6f4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2166896
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2166897
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b:LX/1My;

    invoke-virtual {v1}, LX/1My;->d()V

    .line 2166898
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    invoke-interface {v1}, LX/EnI;->d()V

    .line 2166899
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->M:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 2166900
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    if-eqz v1, :cond_0

    .line 2166901
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    .line 2166902
    invoke-static {v1}, LX/Ems;->c(LX/Ems;)V

    .line 2166903
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/Ems;->f:Z

    .line 2166904
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3bc81009

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6775bc03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2166876
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2166877
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 2166878
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    if-eqz v1, :cond_1

    .line 2166879
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->L:LX/Ems;

    .line 2166880
    iget-boolean v2, v1, LX/Ems;->f:Z

    if-eqz v2, :cond_0

    .line 2166881
    iget-object v2, v1, LX/Ems;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/Ems;->a(LX/Ems;Ljava/lang/String;)V

    .line 2166882
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/Ems;->f:Z

    .line 2166883
    :cond_1
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    if-eqz v1, :cond_2

    .line 2166884
    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    sget-object v2, LX/Emw;->FRAGMENT_CREATE:LX/Emw;

    invoke-virtual {v1, v2}, LX/Emx;->b(LX/Emw;)V

    .line 2166885
    :cond_2
    const/16 v1, 0x2b

    const v2, 0x7486f2ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2166886
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2166887
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    invoke-virtual {v0}, LX/Emc;->e()Ljava/lang/String;

    move-result-object v4

    .line 2166888
    if-nez v4, :cond_1

    .line 2166889
    :cond_0
    :goto_0
    return-void

    .line 2166890
    :cond_1
    const-string v7, "entity_cards_fragment_parameters"

    new-instance v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;

    iget-object v1, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v3}, LX/Enl;->g()LX/0Px;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->B:Ljava/lang/String;

    .line 2166891
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v6

    .line 2166892
    invoke-static {v6}, LX/EnS;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/entitycards/intent/EntityCardsParameters;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v7, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2166893
    iget-object v0, p0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-virtual {v0}, LX/Enl;->h()LX/0am;

    move-result-object v0

    .line 2166894
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166895
    const-string v1, "entity_cards_config_extras"

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method
