.class public Lcom/facebook/contactlogs/data/ContactLogMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contactlogs/data/ContactLogMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/DA1;

.field public b:LX/0m9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1970738
    const-class v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    sput-object v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->c:Ljava/lang/Class;

    .line 1970739
    new-instance v0, LX/DA0;

    invoke-direct {v0}, LX/DA0;-><init>()V

    sput-object v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0m9;LX/DA1;)V
    .locals 1

    .prologue
    .line 1970740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970741
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->b:LX/0m9;

    .line 1970742
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DA1;

    iput-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->a:LX/DA1;

    .line 1970743
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1970744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970745
    :try_start_0
    invoke-static {p1}, LX/46R;->l(Landroid/os/Parcel;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->b:LX/0m9;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1970746
    :goto_0
    const-class v0, LX/DA1;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DA1;

    iput-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->a:LX/DA1;

    .line 1970747
    return-void

    .line 1970748
    :catch_0
    move-exception v0

    .line 1970749
    sget-object v1, Lcom/facebook/contactlogs/data/ContactLogMetadata;->c:Ljava/lang/Class;

    const-string v2, "Could not read JSON from parcel"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1970750
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->b:LX/0m9;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1970751
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1970752
    iget-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->b:LX/0m9;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;LX/0lF;)V

    .line 1970753
    iget-object v0, p0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->a:LX/DA1;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1970754
    return-void
.end method
