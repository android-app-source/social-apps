.class public Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DAC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1970999
    new-instance v0, LX/DAG;

    invoke-direct {v0}, LX/DAG;-><init>()V

    sput-object v0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DAC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1970993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970994
    iput-object p1, p0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;->a:LX/0Px;

    .line 1970995
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1970996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970997
    const-class v0, LX/DAC;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;->a:LX/0Px;

    .line 1970998
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1970990
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1970991
    iget-object v0, p0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1970992
    return-void
.end method
