.class public Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:I

.field private final c:I

.field private d:Z

.field private e:Z

.field private f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2158558
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2158559
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2158560
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2158561
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2158562
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2158563
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2158564
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    .line 2158565
    iget-object v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    const v2, 0x7f0a00e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2158566
    iget-object v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    const v2, 0x7f0b0894

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2158567
    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2158568
    const v1, 0x7f0b0893

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->b:I

    .line 2158569
    const v1, 0x7f0b0892

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->c:I

    .line 2158570
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2158571
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2158572
    if-nez v3, :cond_0

    .line 2158573
    :goto_0
    return-void

    .line 2158574
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-int v0, v0

    .line 2158575
    :goto_1
    iget-boolean v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->d:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->b:I

    :goto_2
    add-int/2addr v0, v1

    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2158576
    invoke-virtual {p0, v3}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2158577
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->d:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->b:I

    .line 2158578
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0, v1, v0, v3, v2}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->setPadding(IIII)V

    goto :goto_0

    .line 2158579
    :cond_1
    iget v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->c:I

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2158580
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2158581
    goto :goto_3
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2158582
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2158583
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    iget v2, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->f:F

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v4, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->f:F

    iget-object v5, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2158584
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x5ce4f947

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2158585
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 2158586
    iget-object v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    iget-boolean v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->b:I

    :goto_0
    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->f:F

    .line 2158587
    const v0, -0x7655592f

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 2158588
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2158589
    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->e:Z

    .line 2158590
    invoke-direct {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a()V

    .line 2158591
    return-void

    .line 2158592
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setExtraPaddingEnabled(Z)V
    .locals 1

    .prologue
    .line 2158593
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->d:Z

    if-ne v0, p1, :cond_0

    .line 2158594
    :goto_0
    return-void

    .line 2158595
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->d:Z

    .line 2158596
    invoke-direct {p0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->a()V

    goto :goto_0
.end method
