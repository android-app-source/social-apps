.class public final Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/util/SmsReaderExperimental_SmsReaderPointerDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mmsId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mms_id"
    .end annotation
.end field

.field public smsId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sms_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160974
    const-class v0, Lcom/facebook/confirmation/util/SmsReaderExperimental_SmsReaderPointerDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2160973
    new-instance v0, LX/EjD;

    invoke-direct {v0}, LX/EjD;-><init>()V

    sput-object v0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 2160971
    invoke-direct {p0, v0, v1, v0, v1}, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;-><init>(JJ)V

    .line 2160972
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 2160967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160968
    iput-wide p1, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    .line 2160969
    iput-wide p3, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    .line 2160970
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2160963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160964
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    .line 2160965
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    .line 2160966
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2160959
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2160960
    iget-wide v0, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2160961
    iget-wide v0, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2160962
    return-void
.end method
