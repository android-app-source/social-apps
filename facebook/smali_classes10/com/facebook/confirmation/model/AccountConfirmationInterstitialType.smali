.class public final enum Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/model/AccountConfirmationInterstitialTypeDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

.field public static final enum HARD_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

.field public static final enum SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

.field public static final enum UNKNOWN:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160668
    const-class v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialTypeDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2160669
    new-instance v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    const-string v1, "HARD_CLIFF"

    invoke-direct {v0, v1, v2}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->HARD_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160670
    new-instance v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    const-string v1, "SOFT_CLIFF"

    invoke-direct {v0, v1, v3}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160671
    new-instance v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->UNKNOWN:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160672
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    sget-object v1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->HARD_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->UNKNOWN:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->$VALUES:[Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2160673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 2160674
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->HARD_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-virtual {v0}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->HARD_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160675
    :goto_0
    return-object v0

    .line 2160676
    :cond_0
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-virtual {v0}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    goto :goto_0

    .line 2160677
    :cond_1
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->UNKNOWN:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;
    .locals 1

    .prologue
    .line 2160678
    const-class v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;
    .locals 1

    .prologue
    .line 2160679
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->$VALUES:[Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    return-object v0
.end method
