.class public Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/model/AccountConfirmationInterstitialDataDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final contactpointType:Lcom/facebook/growth/model/ContactpointType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contactpoint_type"
    .end annotation
.end field

.field public final interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "interstitial_type"
    .end annotation
.end field

.field public final isoCountryCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "iso_country_code"
    .end annotation
.end field

.field public final minImpressionDelayMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "min_impression_delay_ms"
    .end annotation
.end field

.field public final normalizedContactpoint:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "normalized_contactpoint"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160607
    const-class v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialDataDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2160608
    new-instance v0, LX/Eix;

    invoke-direct {v0}, LX/Eix;-><init>()V

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2160609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160610
    iput-object v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    .line 2160611
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    .line 2160612
    iput-object v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    .line 2160613
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->UNKNOWN:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160614
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    .line 2160615
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2160616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160617
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    .line 2160618
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/growth/model/ContactpointType;->valueOf(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    .line 2160619
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    .line 2160620
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->valueOf(Ljava/lang/String;)Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160621
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    .line 2160622
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/growth/model/Contactpoint;
    .locals 2

    .prologue
    .line 2160623
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_0

    .line 2160624
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v0

    .line 2160625
    :goto_0
    return-object v0

    .line 2160626
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_1

    .line 2160627
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v0

    goto :goto_0

    .line 2160628
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2160629
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2160630
    if-ne p0, p1, :cond_1

    .line 2160631
    :cond_0
    :goto_0
    return v0

    .line 2160632
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 2160633
    goto :goto_0

    .line 2160634
    :cond_3
    check-cast p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;

    .line 2160635
    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    iget-object v3, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    iget-object v3, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2160636
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2160637
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->normalizedContactpoint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160638
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->contactpointType:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160639
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->isoCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160640
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    invoke-virtual {v0}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160641
    iget-wide v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2160642
    return-void
.end method
