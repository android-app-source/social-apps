.class public final Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x663442ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160444
    const-class v0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160445
    const-class v0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2160446
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2160447
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2160448
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2160449
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2160450
    return-void
.end method

.method private a()Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2160451
    iget-object v0, p0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    .line 2160452
    iget-object v0, p0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2160453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2160454
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2160455
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2160456
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2160457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2160458
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2160459
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2160460
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2160461
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    .line 2160462
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2160463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;

    .line 2160464
    iput-object v0, v1, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    .line 2160465
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2160466
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2160467
    new-instance v0, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;-><init>()V

    .line 2160468
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2160469
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2160470
    const v0, 0x8c9a8d8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2160471
    const v0, -0x889030b

    return v0
.end method
