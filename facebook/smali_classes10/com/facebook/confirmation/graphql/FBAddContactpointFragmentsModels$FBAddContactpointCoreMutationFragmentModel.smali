.class public final Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x79ee2f9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160218
    const-class v0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160245
    const-class v0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2160243
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2160244
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2160240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2160241
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2160242
    return-void
.end method

.method private a()Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2160238
    iget-object v0, p0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    .line 2160239
    iget-object v0, p0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2160232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2160233
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2160234
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2160235
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2160236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2160237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2160224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2160225
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2160226
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    .line 2160227
    invoke-direct {p0}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->a()Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2160228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;

    .line 2160229
    iput-object v0, v1, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;->e:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    .line 2160230
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2160231
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2160221
    new-instance v0, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;-><init>()V

    .line 2160222
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2160223
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2160220
    const v0, 0x6c3e4a8b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2160219
    const v0, -0x40fab2c4

    return v0
.end method
