.class public Lcom/facebook/confirmation/controller/ConfirmationFragmentController;
.super Lcom/facebook/base/fragment/AbstractNavigableFragmentController;
.source ""


# instance fields
.field public a:LX/EiH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/confirmation/model/AccountConfirmationData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2159341
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/confirmation/controller/ConfirmationFragmentController;LX/EiH;Lcom/facebook/confirmation/model/AccountConfirmationData;)V
    .locals 0

    .prologue
    .line 2159340
    iput-object p1, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a:LX/EiH;

    iput-object p2, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    new-instance p1, LX/EiH;

    invoke-static {v1}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(LX/0QB;)Lcom/facebook/confirmation/model/AccountConfirmationData;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-direct {p1, v0}, LX/EiH;-><init>(Lcom/facebook/confirmation/model/AccountConfirmationData;)V

    move-object v0, p1

    check-cast v0, LX/EiH;

    invoke-static {v1}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(LX/0QB;)Lcom/facebook/confirmation/model/AccountConfirmationData;

    move-result-object v1

    check-cast v1, Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-static {p0, v0, v1}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Lcom/facebook/confirmation/controller/ConfirmationFragmentController;LX/EiH;Lcom/facebook/confirmation/model/AccountConfirmationData;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2159335
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/os/Bundle;)V

    .line 2159336
    const-class v0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-static {v0, p0}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Ljava/lang/Class;LX/02k;)V

    .line 2159337
    new-instance v0, LX/EiJ;

    invoke-direct {v0, p0}, LX/EiJ;-><init>(Lcom/facebook/confirmation/controller/ConfirmationFragmentController;)V

    .line 2159338
    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    .line 2159339
    return-void
.end method

.method public final a(Lcom/facebook/growth/model/Contactpoint;)V
    .locals 1

    .prologue
    .line 2159333
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-virtual {v0, p1}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(Lcom/facebook/growth/model/Contactpoint;)V

    .line 2159334
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2159283
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159284
    iput-object p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    .line 2159285
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2159330
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159331
    iput-boolean p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    .line 2159332
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2159327
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159328
    iput-object p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    .line 2159329
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2159324
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159325
    iput-boolean p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    .line 2159326
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2159321
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159322
    iput-boolean p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->g:Z

    .line 2159323
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 2159292
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a:LX/EiH;

    .line 2159293
    const/4 v1, 0x0

    .line 2159294
    new-instance v2, LX/EiI;

    const-class v3, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-direct {v2, v3}, LX/EiI;-><init>(Ljava/lang/Class;)V

    .line 2159295
    iput-boolean v1, v2, LX/EiI;->b:Z

    .line 2159296
    move-object v2, v2

    .line 2159297
    iput-boolean v1, v2, LX/EiI;->c:Z

    .line 2159298
    move-object v2, v2

    .line 2159299
    iget-object v3, v0, LX/EiH;->a:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159300
    iget-object v4, v3, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v3, v4

    .line 2159301
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2159302
    :cond_0
    :goto_0
    move-object v1, v2

    .line 2159303
    move-object v1, v1

    .line 2159304
    invoke-virtual {v1}, LX/EiI;->c()Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 2159305
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    .line 2159306
    return-void

    .line 2159307
    :cond_1
    iget-object v2, v3, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159308
    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v2, v3, :cond_2

    .line 2159309
    new-instance v2, LX/EiI;

    const-class v3, Lcom/facebook/confirmation/fragment/ConfPhoneCodeInputFragment;

    invoke-direct {v2, v3}, LX/EiI;-><init>(Ljava/lang/Class;)V

    .line 2159310
    iput-boolean v1, v2, LX/EiI;->b:Z

    .line 2159311
    move-object v2, v2

    .line 2159312
    iput-boolean v1, v2, LX/EiI;->c:Z

    .line 2159313
    move-object v2, v2

    .line 2159314
    goto :goto_0

    .line 2159315
    :cond_2
    new-instance v2, LX/EiI;

    const-class v3, Lcom/facebook/confirmation/fragment/ConfEmailCodeInputFragment;

    invoke-direct {v2, v3}, LX/EiI;-><init>(Ljava/lang/Class;)V

    .line 2159316
    iput-boolean v1, v2, LX/EiI;->b:Z

    .line 2159317
    move-object v2, v2

    .line 2159318
    iput-boolean v1, v2, LX/EiI;->c:Z

    .line 2159319
    move-object v2, v2

    .line 2159320
    goto :goto_0
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 2159289
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159290
    iput-boolean p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->h:Z

    .line 2159291
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 2159286
    iget-object v0, p0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159287
    iput-boolean p1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->i:Z

    .line 2159288
    return-void
.end method
