.class public final Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/growth/model/Contactpoint;

.field public final b:Ljava/lang/String;

.field public final c:LX/Ej0;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2160687
    new-instance v0, LX/Eiy;

    invoke-direct {v0}, LX/Eiy;-><init>()V

    sput-object v0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2160688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160689
    const-class v0, Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 2160690
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->b:Ljava/lang/String;

    .line 2160691
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Ej0;->safeValueOf(Ljava/lang/String;)LX/Ej0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->c:LX/Ej0;

    .line 2160692
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->d:Ljava/lang/String;

    .line 2160693
    return-void
.end method

.method public constructor <init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/Ej0;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2160694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160695
    iput-object p1, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 2160696
    iput-object p2, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->b:Ljava/lang/String;

    .line 2160697
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->c:LX/Ej0;

    .line 2160698
    iput-object p4, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->d:Ljava/lang/String;

    .line 2160699
    return-void

    .line 2160700
    :cond_0
    sget-object p3, LX/Ej0;->UNKNOWN:LX/Ej0;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2160701
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2160702
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2160703
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160704
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->c:LX/Ej0;

    invoke-virtual {v0}, LX/Ej0;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160705
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160706
    return-void
.end method
