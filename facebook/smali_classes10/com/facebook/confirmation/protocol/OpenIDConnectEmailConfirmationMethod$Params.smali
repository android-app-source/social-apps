.class public final Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/growth/model/Contactpoint;

.field public final b:Ljava/lang/String;

.field public final c:LX/4gx;

.field public final d:LX/4gy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2160739
    new-instance v0, LX/Ej2;

    invoke-direct {v0}, LX/Ej2;-><init>()V

    sput-object v0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2160740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160741
    const-class v0, Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 2160742
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->b:Ljava/lang/String;

    .line 2160743
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4gx;->valueOf(Ljava/lang/String;)LX/4gx;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->c:LX/4gx;

    .line 2160744
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4gy;->valueOf(Ljava/lang/String;)LX/4gy;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->d:LX/4gy;

    .line 2160745
    return-void
.end method

.method public constructor <init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/4gx;LX/4gy;)V
    .locals 0

    .prologue
    .line 2160746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160747
    iput-object p1, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 2160748
    iput-object p2, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->b:Ljava/lang/String;

    .line 2160749
    iput-object p3, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->c:LX/4gx;

    .line 2160750
    iput-object p4, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->d:LX/4gy;

    .line 2160751
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2160752
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2160753
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2160754
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160755
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->c:LX/4gx;

    invoke-virtual {v0}, LX/4gx;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160756
    iget-object v0, p0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->d:LX/4gy;

    invoke-virtual {v0}, LX/4gy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2160757
    return-void
.end method
