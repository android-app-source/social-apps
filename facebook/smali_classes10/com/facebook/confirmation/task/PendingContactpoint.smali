.class public Lcom/facebook/confirmation/task/PendingContactpoint;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/confirmation/task/PendingContactpointDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final pendingContactpoint:Lcom/facebook/growth/model/Contactpoint;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pending_contactpoint"
    .end annotation
.end field

.field public final timestamp:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timestamp"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2160862
    const-class v0, Lcom/facebook/confirmation/task/PendingContactpointDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2160867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160868
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/confirmation/task/PendingContactpoint;->pendingContactpoint:Lcom/facebook/growth/model/Contactpoint;

    .line 2160869
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/confirmation/task/PendingContactpoint;->timestamp:J

    .line 2160870
    return-void
.end method

.method public constructor <init>(Lcom/facebook/growth/model/Contactpoint;J)V
    .locals 0

    .prologue
    .line 2160863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160864
    iput-object p1, p0, Lcom/facebook/confirmation/task/PendingContactpoint;->pendingContactpoint:Lcom/facebook/growth/model/Contactpoint;

    .line 2160865
    iput-wide p2, p0, Lcom/facebook/confirmation/task/PendingContactpoint;->timestamp:J

    .line 2160866
    return-void
.end method
