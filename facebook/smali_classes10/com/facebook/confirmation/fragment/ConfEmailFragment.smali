.class public Lcom/facebook/confirmation/fragment/ConfEmailFragment;
.super Lcom/facebook/confirmation/fragment/ConfContactpointFragment;
.source ""


# instance fields
.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/widget/AutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2159945
    invoke-direct {p0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2159946
    invoke-super {p0, p1}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->a(Landroid/os/Bundle;)V

    .line 2159947
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->j:LX/0Uh;

    .line 2159948
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2159936
    const v0, 0x7f0d0b0c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    .line 2159937
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->j:LX/0Uh;

    const/16 v1, 0x385

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2159938
    if-eqz v0, :cond_0

    .line 2159939
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->i:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v0

    .line 2159940
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p1, 0x109000a

    invoke-direct {v2, v3, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2159941
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2159942
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/widget/TextView;)V

    .line 2159943
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/EiY;

    invoke-direct {v1, p0}, LX/EiY;-><init>(Lcom/facebook/confirmation/fragment/ConfEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2159944
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2159935
    const v0, 0x7f030356

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2159934
    const v0, 0x7f0833ef

    return v0
.end method

.method public final m()Lcom/facebook/growth/model/Contactpoint;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2159949
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfEmailFragment;->k:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2159950
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2159951
    const/4 v0, 0x0

    .line 2159952
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Lcom/facebook/growth/model/ContactpointType;
    .locals 1

    .prologue
    .line 2159929
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    return-object v0
.end method

.method public final o()LX/EiG;
    .locals 1

    .prologue
    .line 2159930
    sget-object v0, LX/EiG;->EMAIL_ACQUIRED:LX/EiG;

    return-object v0
.end method

.method public final q()LX/EiG;
    .locals 1

    .prologue
    .line 2159931
    sget-object v0, LX/EiG;->EMAIL_SWITCH_TO_PHONE:LX/EiG;

    return-object v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 2159933
    const v0, 0x7f0833ee

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2159932
    const v0, 0x7f0833e2

    return v0
.end method
