.class public Lcom/facebook/confirmation/fragment/ConfPhoneFragment;
.super Lcom/facebook/confirmation/fragment/ConfContactpointFragment;
.source ""


# instance fields
.field private A:Ljava/util/Locale;

.field public B:Landroid/widget/AutoCompleteTextView;

.field public j:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/resources/ui/FbButton;

.field public x:LX/7Tj;

.field private y:Ljava/lang/String;

.field private z:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2160009
    invoke-direct {p0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;-><init>()V

    .line 2160010
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    return-void
.end method

.method private static a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2160063
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2160064
    instance-of v1, v0, Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_0

    .line 2160065
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2160066
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2160067
    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2160068
    :goto_0
    return-void

    .line 2160069
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;LX/7Tl;)V
    .locals 3

    .prologue
    .line 2160052
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->w:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p1, LX/7Tl;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2160053
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2160054
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->z:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2160055
    :cond_0
    new-instance v0, LX/A8g;

    iget-object v1, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->z:Landroid/text/TextWatcher;

    .line 2160056
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->z:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2160057
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2160058
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, "()-."

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2160059
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2160060
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    invoke-static {v1, v0}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2160061
    iget-object v0, p1, LX/7Tl;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    .line 2160062
    return-void
.end method

.method public static b(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2160050
    new-instance v0, LX/7Tl;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    invoke-virtual {v2, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Locale;

    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->A:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->A:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/7Tl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->a$redex0(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;LX/7Tl;)V

    .line 2160051
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2160047
    invoke-super {p0, p1}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->a(Landroid/os/Bundle;)V

    .line 2160048
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    const-class v4, LX/7Tk;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/7Tk;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 p1, 0x15ec

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v0

    check-cast v0, LX/3Lz;

    iput-object v3, v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->j:LX/0W9;

    iput-object v4, v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->k:LX/7Tk;

    iput-object v5, v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->l:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->u:LX/0Or;

    iput-object v0, v2, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    .line 2160049
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2160012
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->j:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->A:Ljava/util/Locale;

    .line 2160013
    const v0, 0x7f0d0b10

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->w:Lcom/facebook/resources/ui/FbButton;

    .line 2160014
    const v0, 0x7f0d0b11

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    .line 2160015
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->l:LX/0Uh;

    const/16 v1, 0x385

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2160016
    if-eqz v0, :cond_2

    .line 2160017
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->i:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v2

    .line 2160018
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2160019
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2160020
    :try_start_0
    iget-object v5, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    iget-object v6, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->i:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v6}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v5

    .line 2160021
    iget-object v6, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    sget-object v7, LX/4hG;->INTERNATIONAL:LX/4hG;

    invoke-virtual {v6, v5, v7}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v6

    .line 2160022
    new-instance v7, Ljava/lang/StringBuilder;

    const-string p1, "+"

    invoke-direct {v7, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2160023
    iget p1, v5, LX/4hT;->countryCode_:I

    move p1, p1

    .line 2160024
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2160025
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2160026
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 2160027
    sget-object v7, LX/1IA;->WHITESPACE:LX/1IA;

    const/16 p1, 0x2d

    invoke-static {p1}, LX/1IA;->is(C)LX/1IA;

    move-result-object p1

    invoke-virtual {v7, p1}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1IA;->trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 2160028
    :cond_0
    move-object v6, v6

    .line 2160029
    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2160030
    iget-object v6, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    sget-object v7, LX/4hG;->NATIONAL:LX/4hG;

    invoke-virtual {v6, v5, v7}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    .line 2160031
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2160032
    :catch_0
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2160033
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2160034
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x109000a

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2160035
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2160036
    :cond_2
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/Eic;

    invoke-direct {v1, p0}, LX/Eic;-><init>(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2160037
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/widget/TextView;)V

    .line 2160038
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2160039
    iget-object v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v0, v1

    .line 2160040
    iget-object v0, v0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2160041
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2160042
    iget-object v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v0, v1

    .line 2160043
    iget-object v0, v0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->b(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;Ljava/lang/String;)V

    .line 2160044
    :goto_2
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->w:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Eie;

    invoke-direct {v1, p0}, LX/Eie;-><init>(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2160045
    return-void

    .line 2160046
    :cond_3
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->b(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2160011
    const v0, 0x7f030358

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2160070
    const v0, 0x7f0833ee

    return v0
.end method

.method public final m()Lcom/facebook/growth/model/Contactpoint;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2159997
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2159998
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->B:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2159999
    :cond_0
    :goto_0
    return-object v0

    .line 2160000
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v1

    .line 2160001
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->v:LX/3Lz;

    sget-object v3, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v2, v1, v3}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v1

    .line 2160002
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->y:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2160003
    :catch_0
    goto :goto_0
.end method

.method public final n()Lcom/facebook/growth/model/ContactpointType;
    .locals 1

    .prologue
    .line 2160004
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    return-object v0
.end method

.method public final o()LX/EiG;
    .locals 1

    .prologue
    .line 2160005
    sget-object v0, LX/EiG;->PHONE_ACQUIRED:LX/EiG;

    return-object v0
.end method

.method public final q()LX/EiG;
    .locals 1

    .prologue
    .line 2160006
    sget-object v0, LX/EiG;->PHONE_SWITCH_TO_EMAIL:LX/EiG;

    return-object v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 2160007
    const v0, 0x7f0833ef

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2160008
    const v0, 0x7f0833e3

    return v0
.end method
