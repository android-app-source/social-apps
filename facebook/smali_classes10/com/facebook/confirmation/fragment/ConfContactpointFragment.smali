.class public abstract Lcom/facebook/confirmation/fragment/ConfContactpointFragment;
.super Lcom/facebook/confirmation/fragment/ConfInputFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EjB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2U9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/F9o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/growth/model/DeviceOwnerData;

.field public final j:Lcom/facebook/common/callercontext/CallerContext;

.field private k:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2159915
    invoke-direct {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;-><init>()V

    .line 2159916
    const-class v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public static a(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2159913
    :try_start_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->g:LX/3Lz;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->g:LX/3Lz;

    invoke-virtual {v1, p1, p2}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v1

    sget-object v2, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v0, v1, v2}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2159914
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2159906
    invoke-super {p0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/os/Bundle;)V

    .line 2159907
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const/16 v4, 0x3ea

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/EjB;->b(LX/0QB;)LX/EjB;

    move-result-object v5

    check-cast v5, LX/EjB;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v7

    check-cast v7, LX/2U9;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object p1

    check-cast p1, LX/3Lz;

    invoke-static {v0}, LX/F9o;->a(LX/0QB;)LX/F9o;

    move-result-object v0

    check-cast v0, LX/F9o;

    iput-object v3, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->b:LX/0aG;

    iput-object v4, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->c:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->d:LX/EjB;

    iput-object v6, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->e:LX/0tX;

    iput-object v7, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    iput-object p1, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->g:LX/3Lz;

    iput-object v0, v2, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->h:LX/F9o;

    .line 2159908
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->h:LX/F9o;

    invoke-virtual {v0}, LX/F9o;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2159909
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->h:LX/F9o;

    .line 2159910
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    move-object v0, v1

    .line 2159911
    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->i:Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2159912
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2159905
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2159819
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159820
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159821
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->n()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v2

    .line 2159822
    iget-object v3, v0, LX/2U9;->a:LX/0Zb;

    sget-object v4, LX/Eiw;->CHANGE_CONTACTPOINT_FLOW_ENTER:LX/Eiw;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 p2, 0x1

    invoke-interface {v3, v4, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2159823
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2159824
    const-string v4, "confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159825
    const-string v4, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v4, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159826
    const-string v4, "new_contactpoint_type"

    invoke-virtual {v2}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v4, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159827
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2159828
    :cond_0
    const v0, 0x7f0d0b0b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->k:Landroid/widget/TextView;

    .line 2159829
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159830
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, v1

    .line 2159831
    if-nez v0, :cond_1

    .line 2159832
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2159833
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->k:Landroid/widget/TextView;

    new-instance v1, LX/EiV;

    invoke-direct {v1, p0}, LX/EiV;-><init>(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2159834
    :goto_0
    invoke-virtual {p0, p1}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->a(Landroid/view/View;)V

    .line 2159835
    return-void

    .line 2159836
    :cond_1
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2159837
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159838
    iget-object v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v0, v1

    .line 2159839
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "initial number"

    iget-object v3, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-static {p0, v3, v0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->a(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159840
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    sget-object v2, LX/Eiw;->UPDATE_PHONE_NUMBER_IMPRESSION:LX/Eiw;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2159904
    const v0, 0x7f030355

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2159903
    const v0, 0x7f0833e9

    return v0
.end method

.method public final l()V
    .locals 13

    .prologue
    .line 2159841
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->m()Lcom/facebook/growth/model/Contactpoint;

    move-result-object v0

    .line 2159842
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2159843
    :cond_0
    :goto_0
    return-void

    .line 2159844
    :cond_1
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159845
    iget-boolean v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v1, v2

    .line 2159846
    if-eqz v1, :cond_3

    .line 2159847
    iget-object v2, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->a(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2159848
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v4, "phone number"

    invoke-virtual {v2, v4, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2159849
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    sget-object v5, LX/Eiw;->PHONE_NUMBER_ADD_ATTEMPT:LX/Eiw;

    const-string v6, "native flow"

    invoke-virtual {v4, v5, v6, v2}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159850
    new-instance v2, LX/4Cn;

    invoke-direct {v2}, LX/4Cn;-><init>()V

    iget-object v4, v0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    .line 2159851
    const-string v5, "country"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159852
    move-object v2, v2

    .line 2159853
    iget-object v4, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2159854
    const-string v5, "contact_point"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159855
    move-object v2, v2

    .line 2159856
    const-string v4, "PHONE_ACQUISITION_PROMO"

    .line 2159857
    const-string v5, "source"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159858
    move-object v2, v2

    .line 2159859
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159860
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    move-object v4, v5

    .line 2159861
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "null"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2159862
    :cond_2
    const-string v5, "ACQUISITION"

    .line 2159863
    :goto_1
    move-object v4, v5

    .line 2159864
    const-string v5, "promo_type"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159865
    move-object v2, v2

    .line 2159866
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159867
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2159868
    const-string v5, "qp_id"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159869
    move-object v2, v2

    .line 2159870
    new-instance v4, LX/Eih;

    invoke-direct {v4}, LX/Eih;-><init>()V

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 2159871
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 2159872
    iget-object v8, v4, LX/Eih;->a:Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel$ViewerModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2159873
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 2159874
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 2159875
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 2159876
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 2159877
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2159878
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2159879
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2159880
    new-instance v8, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;

    invoke-direct {v8, v7}, Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;-><init>(LX/15i;)V

    .line 2159881
    move-object v4, v8

    .line 2159882
    new-instance v5, LX/Eif;

    invoke-direct {v5}, LX/Eif;-><init>()V

    move-object v5, v5

    .line 2159883
    const-string v6, "input"

    invoke-virtual {v5, v6, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/Eif;

    .line 2159884
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    .line 2159885
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->e:LX/0tX;

    invoke-virtual {v4, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2159886
    new-instance v4, LX/EiW;

    invoke-direct {v4, p0, v3, v0}, LX/EiW;-><init>(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Ljava/lang/String;Lcom/facebook/growth/model/Contactpoint;)V

    .line 2159887
    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2159888
    goto/16 :goto_0

    .line 2159889
    :cond_3
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2159890
    const-string v2, "confirmationEditRegistrationContactpointParams"

    invoke-virtual {v4, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2159891
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159892
    iget-object v5, v3, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v3, v5

    .line 2159893
    iget-object v3, v3, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->n()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v5

    .line 2159894
    iget-object v6, v2, LX/2U9;->a:LX/0Zb;

    sget-object v7, LX/Eiw;->CHANGE_CONTACTPOINT_ATTEMPT:LX/Eiw;

    invoke-virtual {v7}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2159895
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2159896
    const-string v7, "confirmation"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159897
    const-string v7, "current_contactpoint_type"

    invoke-virtual {v3}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159898
    const-string v7, "new_contactpoint_type"

    invoke-virtual {v5}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159899
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2159900
    :cond_4
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->b:LX/0aG;

    const-string v3, "confirmation_edit_registration_contactpoint"

    sget-object v5, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    iget-object v6, p0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    const v7, -0x2a15acf0

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    new-instance v3, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080037

    invoke-direct {v3, v4, v5}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v2, v3}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 2159901
    new-instance v3, LX/EiX;

    invoke-direct {v3, p0, v0}, LX/EiX;-><init>(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Lcom/facebook/growth/model/Contactpoint;)V

    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2159902
    goto/16 :goto_0

    :cond_5
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public abstract m()Lcom/facebook/growth/model/Contactpoint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()Lcom/facebook/growth/model/ContactpointType;
.end method

.method public abstract o()LX/EiG;
.end method
