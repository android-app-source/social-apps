.class public abstract Lcom/facebook/confirmation/fragment/ConfInputFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""


# static fields
.field public static final b:Ljava/util/regex/Pattern;


# instance fields
.field public m:Lcom/facebook/confirmation/model/AccountConfirmationData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2U9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/resources/ui/FbButton;

.field public s:Landroid/widget/TextView;

.field public t:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2159607
    const-string v0, "(\\s*\\(\\d{1,10}\\))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2159606
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/EiG;)V
    .locals 2

    .prologue
    .line 2159603
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2159604
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, LX/EiG;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    .line 2159605
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2159600
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/os/Bundle;)V

    .line 2159601
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;

    invoke-static {v0}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(LX/0QB;)Lcom/facebook/confirmation/model/AccountConfirmationData;

    move-result-object v3

    check-cast v3, Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0x2ba

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object p1

    check-cast p1, LX/0kb;

    invoke-static {v0}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v0

    check-cast v0, LX/2U9;

    iput-object v3, v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    iput-object v4, v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    iput-object v5, v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;->o:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;->p:LX/0kb;

    iput-object v0, v2, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    .line 2159602
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2159542
    return-void
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 2159598
    new-instance v0, LX/Eia;

    invoke-direct {v0, p0}, LX/Eia;-><init>(Lcom/facebook/confirmation/fragment/ConfInputFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2159599
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2159595
    invoke-virtual {p0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->b(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;

    move-result-object v0

    .line 2159596
    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Ljava/lang/String;)V

    .line 2159597
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2159588
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2159589
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->p:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2159590
    const v0, 0x7f08003a

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2159591
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2159592
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2159593
    return-void

    .line 2159594
    :cond_1
    const v0, 0x7f080039

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2159552
    const/4 v2, 0x0

    .line 2159553
    if-nez p1, :cond_6

    move-object v0, v2

    .line 2159554
    :goto_0
    move-object v2, v0

    .line 2159555
    if-nez v2, :cond_0

    move-object v0, v1

    .line 2159556
    :goto_1
    return-object v0

    .line 2159557
    :cond_0
    new-instance v0, LX/Eib;

    invoke-direct {v0, p0}, LX/Eib;-><init>(Lcom/facebook/confirmation/fragment/ConfInputFragment;)V

    const/4 v4, 0x0

    .line 2159558
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object p1

    .line 2159559
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    move-object v3, v4

    .line 2159560
    :goto_2
    move-object v0, v3

    .line 2159561
    check-cast v0, Ljava/util/Map;

    .line 2159562
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2159563
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    .line 2159564
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2159565
    const/4 v0, 0x0

    .line 2159566
    :cond_2
    :goto_3
    move-object v0, v0

    .line 2159567
    goto :goto_1

    .line 2159568
    :cond_3
    const-string v2, "error_message"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2159569
    const-string v1, "error_message"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    .line 2159570
    :cond_4
    const-string v2, "error_title"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2159571
    const-string v1, "error_title"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 2159572
    goto :goto_1

    .line 2159573
    :cond_6
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2159574
    if-nez v0, :cond_7

    move-object v0, v2

    .line 2159575
    goto :goto_0

    .line 2159576
    :cond_7
    iget-object v3, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v0, v3

    .line 2159577
    if-nez v0, :cond_8

    move-object v0, v2

    .line 2159578
    goto :goto_0

    .line 2159579
    :cond_8
    const-string v3, "result"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2159580
    instance-of v3, v0, Lcom/facebook/http/protocol/ApiErrorResult;

    if-nez v3, :cond_9

    move-object v0, v2

    .line 2159581
    goto :goto_0

    .line 2159582
    :cond_9
    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    goto :goto_0

    .line 2159583
    :cond_a
    :try_start_0
    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->o:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-virtual {v3, p1, v0}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_2

    .line 2159584
    :catch_0
    move-object v3, v4

    goto :goto_2

    .line 2159585
    :cond_b
    sget-object v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2159586
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2159587
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public abstract c()I
.end method

.method public final c_()V
    .locals 2

    .prologue
    .line 2159543
    invoke-super {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c_()V

    .line 2159544
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2159545
    if-eqz v0, :cond_0

    .line 2159546
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->e()I

    move-result v1

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2159547
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159548
    iget-boolean p0, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v1, p0

    .line 2159549
    if-eqz v1, :cond_0

    .line 2159550
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2159551
    :cond_0
    return-void
.end method

.method public abstract d()I
.end method

.method public abstract e()I
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x651c7b16

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2159520
    iget-boolean v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    move v0, v0

    .line 2159521
    if-eqz v0, :cond_0

    .line 2159522
    const/4 v0, 0x0

    const/16 v1, 0x2b

    const v3, 0x20a598bf

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2159523
    :goto_0
    return-object v0

    .line 2159524
    :cond_0
    const v0, 0x7f030357

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2159525
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->c()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2159526
    const v0, 0x7f0d0b0d

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2159527
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->c()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2159528
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2159529
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->d()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2159530
    const v0, 0x7f0d0b0f

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2159531
    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->d()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2159532
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2159533
    :cond_2
    const v0, 0x7f0d0372

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    .line 2159534
    const v0, 0x7f0d0b0e

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    .line 2159535
    const v0, 0x7f0d090e

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t:Landroid/widget/TextView;

    .line 2159536
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->k()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2159537
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2159538
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2159539
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/EiZ;

    invoke-direct {v3, p0}, LX/EiZ;-><init>(Lcom/facebook/confirmation/fragment/ConfInputFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2159540
    invoke-virtual {p0, v1, p3}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2159541
    const v0, 0x6cd38e07

    invoke-static {v0, v2}, LX/02F;->f(II)V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method public abstract q()LX/EiG;
.end method

.method public abstract r()I
.end method

.method public abstract t()I
.end method
