.class public abstract Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;
.super Lcom/facebook/confirmation/fragment/ConfInputFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public A:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public B:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public C:Ljava/lang/String;

.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EjB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/2U9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final u:Lcom/facebook/common/callercontext/CallerContext;

.field public v:Landroid/widget/TextView;

.field public w:Landroid/widget/EditText;

.field private x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

.field public y:Landroid/widget/TextView;

.field public z:Lcom/facebook/fbui/widget/contentview/ContentView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2159758
    invoke-direct {p0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;-><init>()V

    .line 2159759
    const-class v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->u:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public static B(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)I
    .locals 4

    .prologue
    .line 2159757
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->k:LX/0W3;

    sget-wide v2, LX/0X5;->hU:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public static D(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V
    .locals 3

    .prologue
    .line 2159751
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->j:LX/0Uh;

    const/16 v1, 0x388

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2159752
    if-eqz v0, :cond_0

    .line 2159753
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    const/4 v1, 0x0

    .line 2159754
    iput-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    .line 2159755
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2159756
    :cond_0
    return-void
.end method

.method public static E(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2159744
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)I

    move-result v1

    .line 2159745
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159746
    iget-boolean v3, v2, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v2, v3

    .line 2159747
    if-nez v2, :cond_0

    if-lez v1, :cond_0

    .line 2159748
    const/4 v2, 0x6

    move v2, v2

    .line 2159749
    if-le v1, v2, :cond_1

    .line 2159750
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->j:LX/0Uh;

    const/16 v2, 0x387

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z
    .locals 3

    .prologue
    .line 2159743
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->j:LX/0Uh;

    const/16 v1, 0x386

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2159739
    invoke-super {p0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/os/Bundle;)V

    .line 2159740
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/EjB;->b(LX/0QB;)LX/EjB;

    move-result-object v6

    check-cast v6, LX/EjB;

    invoke-static {v0}, LX/3fx;->a(LX/0QB;)LX/3fx;

    move-result-object v7

    check-cast v7, LX/3fx;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v10

    check-cast v10, LX/8tu;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p1

    check-cast p1, LX/0W3;

    invoke-static {v0}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v0

    check-cast v0, LX/2U9;

    iput-object v4, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->b:LX/0aG;

    iput-object v5, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    iput-object v6, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->d:LX/EjB;

    iput-object v7, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->e:LX/3fx;

    iput-object v8, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->f:LX/0W9;

    iput-object v9, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->g:LX/0wM;

    iput-object v10, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->h:LX/8tu;

    iput-object v11, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->i:LX/0tX;

    iput-object v12, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->j:LX/0Uh;

    iput-object p1, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->k:LX/0W3;

    iput-object v0, v3, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    .line 2159741
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    sget-object v1, LX/Eiw;->CONFIRMATION_IMPRESSION:LX/Eiw;

    invoke-virtual {v0, v1, v2, v2}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159742
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2159661
    const v0, 0x7f0d0ada

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->v:Landroid/widget/TextView;

    .line 2159662
    const v0, 0x7f0d0b0a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    .line 2159663
    const v0, 0x7f0d0b09

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    .line 2159664
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->E(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2159665
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2159666
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2159667
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2159668
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b227f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2159669
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v2, v2, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2159670
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)I

    move-result v0

    .line 2159671
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    .line 2159672
    iget p2, v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->h:I

    move v1, p2

    .line 2159673
    if-eq v0, v1, :cond_0

    .line 2159674
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->setupView(I)V

    .line 2159675
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->setVisibility(I)V

    .line 2159676
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    new-instance v1, LX/EiL;

    invoke-direct {v1, p0}, LX/EiL;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159677
    iput-object v1, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->j:LX/EiK;

    .line 2159678
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    new-instance v1, LX/EiN;

    invoke-direct {v1, p0}, LX/EiN;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159679
    iput-object v1, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->k:LX/EiM;

    .line 2159680
    :goto_0
    const v0, 0x7f0d0b05

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->y:Landroid/widget/TextView;

    .line 2159681
    const v0, 0x7f0d0b06

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2159682
    const v0, 0x7f0d0b07

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->A:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2159683
    const v0, 0x7f0d0b08

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2159684
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159685
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    move v0, v1

    .line 2159686
    if-nez v0, :cond_1

    .line 2159687
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->D(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159688
    :cond_1
    const p1, -0x6e685d

    .line 2159689
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2159690
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->n()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2159691
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->g:LX/0wM;

    const v2, 0x7f0209bf

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2159692
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v1, LX/EiS;

    invoke-direct {v1, p0}, LX/EiS;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2159693
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->A:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2159694
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->A:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->g:LX/0wM;

    const v2, 0x7f020952

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2159695
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->A:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v1, LX/EiT;

    invoke-direct {v1, p0}, LX/EiT;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2159696
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159697
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, v1

    .line 2159698
    if-nez v0, :cond_2

    .line 2159699
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2159700
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->g:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->s()I

    move-result v2

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2159701
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v1, LX/EiU;

    invoke-direct {v1, p0}, LX/EiU;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2159702
    :cond_2
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159703
    iget-object v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v1

    .line 2159704
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2159705
    new-instance v3, LX/47x;

    invoke-direct {v3, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2159706
    iget-object v0, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v4, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v4, :cond_5

    .line 2159707
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->e:LX/3fx;

    iget-object v4, v1, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/3fx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2159708
    const v4, 0x7f083401

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 2159709
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->f:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    .line 2159710
    new-instance p1, Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p2

    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-direct {p1, p2, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2159711
    const v4, 0x7f0833ff

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "[[contactpoint]]"

    invoke-static {v2, v4, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2159712
    :goto_1
    const-string v1, "[[contactpoint]]"

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v3, v1, v0, v2, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2159713
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->v:Landroid/widget/TextView;

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2159714
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->v:Landroid/widget/TextView;

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2159715
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->v:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2159716
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->h:LX/8tu;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2159717
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->E(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2159718
    :cond_3
    :goto_2
    return-void

    .line 2159719
    :cond_4
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2159720
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2159721
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2159722
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    new-instance v1, LX/EiO;

    invoke-direct {v1, p0}, LX/EiO;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2159723
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Landroid/widget/TextView;)V

    goto/16 :goto_0

    .line 2159724
    :cond_5
    iget-object v0, v1, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2159725
    const v1, 0x7f083400

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 2159726
    const v1, 0x7f0833fe

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[[contactpoint]]"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    goto :goto_1

    .line 2159727
    :cond_6
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)I

    move-result v1

    .line 2159728
    if-lez v1, :cond_3

    .line 2159729
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2159730
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159731
    iget-object v3, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v0, v3

    .line 2159732
    iget-object v0, v0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v3, :cond_7

    .line 2159733
    const v0, 0x7f083402

    .line 2159734
    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2159735
    iget-object v3, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2159736
    const v0, 0x7f083404

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2159737
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2159738
    :cond_7
    const v0, 0x7f083403

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2159656
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->E(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2159657
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->x:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-virtual {v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->a()V

    .line 2159658
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Ljava/lang/String;)V

    .line 2159659
    return-void

    .line 2159660
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2159655
    const v0, 0x7f030354

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2159654
    const v0, 0x7f030353

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2159651
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159652
    iget-boolean p0, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, p0

    .line 2159653
    if-eqz v0, :cond_0

    const v0, 0x7f0833da

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0833d9

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2159608
    const v0, 0x7f080019

    return v0
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 2159609
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->E(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2159610
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    .line 2159611
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2159612
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833f1

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2159613
    :goto_0
    return-void

    .line 2159614
    :cond_1
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159615
    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    sget-object v2, LX/Eiw;->ENTER_PIN:LX/Eiw;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159616
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->z(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2159617
    invoke-static {p0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->B(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)I

    move-result v0

    .line 2159618
    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 2159619
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2159620
    :cond_2
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159621
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159622
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159623
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->CONFIRMATION_ATTEMPT:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159624
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2159625
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159626
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159627
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159628
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2159629
    new-instance v0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;

    iget-object v1, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159630
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159631
    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    sget-object v3, LX/Ej0;->ANDROID_DIALOG_API:LX/Ej0;

    .line 2159632
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159633
    iget-boolean v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->i:Z

    move v4, v5

    .line 2159634
    if-eqz v4, :cond_4

    .line 2159635
    const-string v4, "qp"

    .line 2159636
    :goto_1
    move-object v4, v4

    .line 2159637
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;-><init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/Ej0;Ljava/lang/String;)V

    .line 2159638
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2159639
    const-string v1, "confirmationConfirmContactpointParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2159640
    iget-object v0, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->b:LX/0aG;

    const-string v1, "confirmation_confirm_contactpoint"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->u:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x11a78377

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    new-instance v1, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0833ea

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v0, v1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2159641
    new-instance v1, LX/EiP;

    invoke-direct {v1, p0}, LX/EiP;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    iget-object v2, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 2159642
    :cond_4
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159643
    iget-boolean v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->g:Z

    move v4, v5

    .line 2159644
    if-eqz v4, :cond_5

    .line 2159645
    const-string v4, "hard_cliff"

    goto :goto_1

    .line 2159646
    :cond_5
    iget-object v4, p0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159647
    iget-boolean v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->h:Z

    move v4, v5

    .line 2159648
    if-eqz v4, :cond_6

    .line 2159649
    const-string v4, "dismissible_cliff"

    goto :goto_1

    .line 2159650
    :cond_6
    const-string v4, "unknown_source_cliff"

    goto :goto_1
.end method

.method public abstract m()I
.end method

.method public abstract n()I
.end method

.method public abstract o()LX/EiG;
.end method

.method public abstract p()I
.end method

.method public abstract q()LX/EiG;
.end method

.method public abstract r()I
.end method

.method public abstract s()I
.end method
