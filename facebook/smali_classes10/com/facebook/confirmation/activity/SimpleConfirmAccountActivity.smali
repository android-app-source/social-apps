.class public Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1ZF;


# instance fields
.field private A:Landroid/view/View;

.field public B:Lcom/facebook/growth/model/Contactpoint;

.field public C:Z

.field public D:Z

.field public E:Z

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field public p:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2U9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/EjB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final w:LX/0YZ;

.field private x:LX/0Yb;

.field private y:LX/0h5;

.field private z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2159137
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2159138
    new-instance v0, LX/Ei8;

    invoke-direct {v0, p0}, LX/Ei8;-><init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->w:LX/0YZ;

    .line 2159139
    iput-boolean v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    .line 2159140
    iput-boolean v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    .line 2159141
    iput-boolean v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->E:Z

    .line 2159142
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->F:Ljava/lang/String;

    .line 2159143
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->G:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2159144
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->p:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "action_background_contactpoint_confirmed"

    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->w:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->x:LX/0Yb;

    .line 2159145
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->x:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2159146
    return-void
.end method

.method private static a(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;LX/0Xl;LX/2U9;LX/GvB;LX/0kL;LX/0Uh;LX/EjB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 2159147
    iput-object p1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->p:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iput-object p3, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->r:LX/GvB;

    iput-object p4, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->s:LX/0kL;

    iput-object p5, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->t:LX/0Uh;

    iput-object p6, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->u:LX/EjB;

    iput-object p7, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-static {v7}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {v7}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v2

    check-cast v2, LX/2U9;

    invoke-static {v7}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v3

    check-cast v3, LX/GvB;

    invoke-static {v7}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v7}, LX/EjB;->b(LX/0QB;)LX/EjB;

    move-result-object v6

    check-cast v6, LX/EjB;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v7}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->a(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;LX/0Xl;LX/2U9;LX/GvB;LX/0kL;LX/0Uh;LX/EjB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2159166
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2159167
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    .line 2159168
    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    if-eqz v0, :cond_1

    .line 2159169
    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    if-eqz v0, :cond_0

    .line 2159170
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->d(Z)V

    .line 2159171
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2159172
    :goto_0
    new-instance v1, LX/EiA;

    invoke-direct {v1, p0}, LX/EiA;-><init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    .line 2159173
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 2159174
    iput-object v0, v2, LX/108;->g:Ljava/lang/String;

    .line 2159175
    move-object v2, v2

    .line 2159176
    iput-object v0, v2, LX/108;->j:Ljava/lang/String;

    .line 2159177
    move-object v0, v2

    .line 2159178
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2159179
    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2159180
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2159181
    return-void

    .line 2159182
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2159183
    :cond_1
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->c(Z)V

    .line 2159184
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0833db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V
    .locals 5

    .prologue
    .line 2159148
    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    if-eqz v0, :cond_2

    .line 2159149
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2159150
    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    if-eqz v0, :cond_0

    .line 2159151
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3df;->i:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2159152
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159153
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->CANCEL_CLICK:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159154
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2159155
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159156
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159157
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159158
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->finish()V

    .line 2159159
    :goto_0
    return-void

    .line 2159160
    :cond_2
    invoke-static {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->m(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V
    .locals 4

    .prologue
    .line 2159161
    new-instance v0, LX/EiB;

    invoke-direct {v0, p0}, LX/EiB;-><init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    .line 2159162
    new-instance v1, LX/EiC;

    invoke-direct {v1, p0}, LX/EiC;-><init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    .line 2159163
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080013

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080014

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080019

    invoke-virtual {v2, v3, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    invoke-virtual {v0, v2, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2159164
    return-void
.end method


# virtual methods
.method public final a(LX/63W;)V
    .locals 0

    .prologue
    .line 2159165
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2159134
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2159135
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2159136
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2159100
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2159101
    invoke-static {p0, p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2159102
    const v0, 0x7f030359

    invoke-virtual {p0, v0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->setContentView(I)V

    .line 2159103
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2159104
    const-string v0, "extra_contactpoint"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    .line 2159105
    const-string v0, "extra_cancel_allowed"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    .line 2159106
    const-string v0, "extra_is_cliff_interstitial"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    .line 2159107
    const-string v0, "extra_for_phone_number_confirmation"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->E:Z

    .line 2159108
    const-string v0, "extra_phone_number_acquisition_quick_promotion_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->F:Ljava/lang/String;

    .line 2159109
    const-string v0, "extra_phone_number_acquisition_quick_promotion_type"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->G:Ljava/lang/String;

    .line 2159110
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d0b12

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iput-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    .line 2159111
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Lcom/facebook/growth/model/Contactpoint;)V

    .line 2159112
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-boolean v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Z)V

    .line 2159113
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-boolean v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->E:Z

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b(Z)V

    .line 2159114
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a(Ljava/lang/String;)V

    .line 2159115
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->b(Ljava/lang/String;)V

    .line 2159116
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    const-string v2, "qp"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->e(Z)V

    .line 2159117
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->d()V

    .line 2159118
    invoke-direct {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->a()V

    .line 2159119
    invoke-direct {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->b()V

    .line 2159120
    const-string v0, "extra_ref"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2159121
    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iget-object v2, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    iget-boolean v3, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    const/4 v4, 0x1

    .line 2159122
    iget-object v5, v1, LX/2U9;->a:LX/0Zb;

    sget-object p0, LX/Eiw;->FLOW_ENTER:LX/Eiw;

    invoke-virtual {p0}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v5, p0, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 2159123
    if-nez v0, :cond_0

    .line 2159124
    const-string v0, "cliff_seen"

    .line 2159125
    :cond_0
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2159126
    const-string p0, "confirmation"

    invoke-virtual {v5, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159127
    const-string p0, "current_contactpoint"

    iget-object p1, v2, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159128
    const-string p0, "current_contactpoint_type"

    iget-object p1, v2, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v5, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2159129
    const-string p0, "is_blocked"

    if-nez v3, :cond_2

    :goto_0
    invoke-virtual {v5, p0, v4}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2159130
    const-string v4, "ref"

    invoke-virtual {v5, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159131
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 2159132
    :cond_1
    return-void

    .line 2159133
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2159099
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 2159098
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->A:Landroid/view/View;

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2159097
    return-void
.end method

.method public final lH_()V
    .locals 2

    .prologue
    .line 2159094
    new-instance v0, LX/Ei9;

    invoke-direct {v0, p0}, LX/Ei9;-><init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V

    .line 2159095
    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-interface {v1, v0}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2159096
    return-void
.end method

.method public final onBackPressed()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2159081
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2159082
    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->C:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    if-nez v0, :cond_1

    .line 2159083
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159084
    iget-object v3, v0, LX/2U9;->a:LX/0Zb;

    sget-object v4, LX/Eiw;->BACK_PRESS:LX/Eiw;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2159085
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2159086
    const-string v4, "confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159087
    const-string v4, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159088
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2159089
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    sget-object v1, LX/Eiw;->CLOSE_NATIVE_FLOW:LX/Eiw;

    invoke-virtual {v0, v1, v2, v2}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159090
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    invoke-virtual {v0}, LX/2U9;->a()V

    .line 2159091
    invoke-virtual {p0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->finish()V

    .line 2159092
    :cond_1
    :goto_0
    return-void

    .line 2159093
    :cond_2
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->z:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->c()Z

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x180a1545

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2159076
    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->x:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2159077
    iget-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->x:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2159078
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->x:LX/0Yb;

    .line 2159079
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2159080
    const/16 v1, 0x23

    const v2, -0x2cc48e2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2159073
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 2159074
    iput-object p1, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->A:Landroid/view/View;

    .line 2159075
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 2159071
    iget-object v0, p0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->y:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 2159072
    return-void
.end method
