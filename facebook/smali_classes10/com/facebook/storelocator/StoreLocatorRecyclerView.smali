.class public Lcom/facebook/storelocator/StoreLocatorRecyclerView;
.super Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
.source ""


# instance fields
.field public m:LX/D1S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1956837
    invoke-direct {p0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;)V

    .line 1956838
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->n:Z

    .line 1956839
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1956834
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1956835
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->n:Z

    .line 1956836
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1956831
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1956832
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->n:Z

    .line 1956833
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    invoke-static {v0}, LX/D1S;->c(LX/0QB;)LX/D1S;

    move-result-object v0

    check-cast v0, LX/D1S;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->m:LX/D1S;

    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1956827
    iget-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->n:Z

    if-nez v0, :cond_0

    .line 1956828
    const-class v0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1956829
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->n:Z

    .line 1956830
    :cond_0
    return-void
.end method


# virtual methods
.method public getLayoutManagerForInit()LX/25T;
    .locals 1

    .prologue
    .line 1956825
    invoke-direct {p0}, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->m()V

    .line 1956826
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->m:LX/D1S;

    return-object v0
.end method
