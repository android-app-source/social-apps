.class public Lcom/facebook/storelocator/StoreLocatorMapDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static H:LX/0Xm;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:LX/D1T;

.field public D:Landroid/view/View;

.field public E:LX/698;

.field private F:Z

.field private G:LX/697;

.field public a:Ljava/util/ArrayList;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/698;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Zb;

.field private final d:LX/Bi1;

.field public final e:LX/D0y;

.field public final f:Ljava/util/concurrent/Executor;

.field private final g:LX/0y3;

.field private final h:LX/17W;

.field private final i:LX/D1g;

.field public final j:LX/1HI;

.field public final k:LX/3hh;

.field public final l:LX/0hB;

.field private final m:Lcom/facebook/content/SecureContextHelper;

.field private n:Ljava/lang/String;

.field public o:LX/D14;

.field private p:I

.field private q:Landroid/content/Context;

.field public r:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public s:LX/68w;

.field public t:LX/680;

.field private u:I

.field public v:Z

.field public w:LX/4FK;

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/D17;

.field private z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1956754
    const-class v0, Lcom/facebook/storelocator/StoreLocatorActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/D0y;LX/Bi1;Ljava/util/concurrent/Executor;LX/0y3;LX/17W;LX/D1g;LX/1HI;LX/3hh;LX/0hB;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1956740
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->u:I

    .line 1956741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    .line 1956742
    iput-object p1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c:LX/0Zb;

    .line 1956743
    iput-object p2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->e:LX/D0y;

    .line 1956744
    iput-object p3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->d:LX/Bi1;

    .line 1956745
    iput-object p4, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f:Ljava/util/concurrent/Executor;

    .line 1956746
    iput-object p5, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->g:LX/0y3;

    .line 1956747
    iput-object p6, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->h:LX/17W;

    .line 1956748
    iput-object p7, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->i:LX/D1g;

    .line 1956749
    iput-object p8, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->j:LX/1HI;

    .line 1956750
    iput-object p9, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->k:LX/3hh;

    .line 1956751
    iput-object p10, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->l:LX/0hB;

    .line 1956752
    iput-object p11, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->m:Lcom/facebook/content/SecureContextHelper;

    .line 1956753
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/storelocator/StoreLocatorMapDelegate;
    .locals 15

    .prologue
    .line 1956728
    const-class v1, Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    monitor-enter v1

    .line 1956729
    :try_start_0
    sget-object v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->H:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1956730
    sput-object v2, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->H:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1956731
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956732
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1956733
    new-instance v3, Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/D0y;->b(LX/0QB;)LX/D0y;

    move-result-object v5

    check-cast v5, LX/D0y;

    invoke-static {v0}, LX/Bi1;->a(LX/0QB;)LX/Bi1;

    move-result-object v6

    check-cast v6, LX/Bi1;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v8

    check-cast v8, LX/0y3;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v0}, LX/D1g;->a(LX/0QB;)LX/D1g;

    move-result-object v10

    check-cast v10, LX/D1g;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v11

    check-cast v11, LX/1HI;

    invoke-static {v0}, LX/3hh;->b(LX/0QB;)LX/3hh;

    move-result-object v12

    check-cast v12, LX/3hh;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v13

    check-cast v13, LX/0hB;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v14

    check-cast v14, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;-><init>(LX/0Zb;LX/D0y;LX/Bi1;Ljava/util/concurrent/Executor;LX/0y3;LX/17W;LX/D1g;LX/1HI;LX/3hh;LX/0hB;Lcom/facebook/content/SecureContextHelper;)V

    .line 1956734
    move-object v0, v3

    .line 1956735
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1956736
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956737
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1956738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/util/Map;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 1956710
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1956711
    const-string v0, "ref"

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->C:LX/D1T;

    invoke-virtual {v2}, LX/D1T;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956712
    const-string v2, "ad_id"

    const-string v0, "NOT_SET"

    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956713
    const-string v2, "page_set_id"

    const-string v0, "NOT_SET"

    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->A:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956714
    const-string v2, "parent_page_id"

    const-string v0, "NOT_SET"

    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->B:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956715
    const-string v0, "geo_bounding_box"

    .line 1956716
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->w:LX/4FK;

    if-nez v2, :cond_3

    .line 1956717
    const-string v2, ""

    .line 1956718
    :goto_3
    move-object v2, v2

    .line 1956719
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956720
    const-string v0, "fetch_request_count"

    iget v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->u:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956721
    invoke-interface {v1, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1956722
    return-object v1

    .line 1956723
    :cond_0
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->n:Ljava/lang/String;

    goto :goto_0

    .line 1956724
    :cond_1
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->A:Ljava/lang/String;

    goto :goto_1

    .line 1956725
    :cond_2
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->B:Ljava/lang/String;

    goto :goto_2

    .line 1956726
    :cond_3
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->w:LX/4FK;

    invoke-virtual {v2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v2

    .line 1956727
    const-string v3, "{%s, %s, %s, %s}"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "north"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "west"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "south"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "east"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method private a(Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1956511
    invoke-virtual {p1}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v0

    move v4, v0

    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 1956512
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->d:LX/Bi1;

    sget-object v1, LX/Bi3;->STORE_LOCATOR:LX/Bi3;

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Bi1;->b(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1956513
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v4, v7

    goto :goto_0

    .line 1956514
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/31h;Landroid/graphics/Point;)V
    .locals 4

    .prologue
    .line 1956707
    iget v0, p2, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v1, v1, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->z:F

    float-to-int v2, v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 1956708
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-virtual {p1, p2}, LX/31h;->a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    invoke-static {v1}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;

    move-result-object v1

    const/16 v2, 0x1f4

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1956709
    return-void
.end method

.method public static a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/698;IZ)V
    .locals 3

    .prologue
    .line 1956681
    const/high16 v2, 0x3f000000    # 0.5f

    .line 1956682
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    if-eqz v0, :cond_0

    .line 1956683
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->s:LX/68w;

    invoke-virtual {v0, v1}, LX/698;->a(LX/68w;)V

    .line 1956684
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    invoke-virtual {v0}, LX/698;->p()V

    .line 1956685
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    invoke-virtual {v0, v2, v2}, LX/698;->f(FF)V

    .line 1956686
    :cond_0
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    invoke-virtual {v0, p2}, LX/D14;->a(I)Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->jo_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1956687
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1956688
    :goto_0
    iput-object p1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    .line 1956689
    if-eqz v0, :cond_1

    .line 1956690
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->j:LX/1HI;

    sget-object v2, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->r:LX/1ca;

    .line 1956691
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->r:LX/1ca;

    new-instance v1, LX/D1N;

    invoke-direct {v1, p0}, LX/D1N;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;)V

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1956692
    :cond_1
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    invoke-virtual {v0}, LX/698;->q()V

    .line 1956693
    if-eqz p3, :cond_2

    .line 1956694
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    .line 1956695
    iget-object v1, v0, LX/680;->k:LX/31h;

    move-object v0, v1

    .line 1956696
    invoke-virtual {p1}, LX/67m;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/31h;->a(Lcom/facebook/android/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v1

    .line 1956697
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v2, v2, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getHeight()I

    move-result v2

    .line 1956698
    if-lez v2, :cond_4

    .line 1956699
    invoke-static {p0, v0, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/31h;Landroid/graphics/Point;)V

    .line 1956700
    :cond_2
    :goto_1
    return-void

    .line 1956701
    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->k:LX/3hh;

    .line 1956702
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 1956703
    move-object v0, v0

    .line 1956704
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 1956705
    :cond_4
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v2, v2, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 1956706
    new-instance p2, LX/D1M;

    invoke-direct {p2, p0, v0, v1}, LX/D1M;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/31h;Landroid/graphics/Point;)V

    invoke-virtual {v2, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(ILcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;)V
    .locals 3

    .prologue
    .line 1956673
    new-instance v0, LX/D1O;

    invoke-direct {v0, p0, p1}, LX/D1O;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;I)V

    invoke-direct {p0, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1956674
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c:LX/0Zb;

    .line 1956675
    const-string v2, "store_locator_card_focused"

    invoke-static {v2, v0}, LX/D1A;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v0, v2

    .line 1956676
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1956677
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1956678
    invoke-direct {p0, p2}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;)V

    .line 1956679
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/698;

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/698;IZ)V

    .line 1956680
    :cond_0
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1956629
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-virtual {v0}, LX/680;->b()V

    .line 1956630
    new-instance v0, LX/D1K;

    invoke-direct {v0, p0, p1}, LX/D1K;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/0Px;)V

    invoke-direct {p0, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1956631
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c:LX/0Zb;

    .line 1956632
    const-string v3, "store_locator_fetched_locations"

    invoke-static {v3, v0}, LX/D1A;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object v0, v3

    .line 1956633
    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1956634
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v2, v0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 1956635
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/698;

    .line 1956636
    invoke-virtual {v0}, LX/67m;->l()V

    .line 1956637
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1956638
    goto :goto_0

    .line 1956639
    :cond_1
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1956640
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->l:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v2

    .line 1956641
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v6, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    .line 1956642
    :goto_2
    int-to-float v3, v2

    mul-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    iget v4, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->p:I

    .line 1956643
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->p:I

    .line 1956644
    iput v3, v0, LX/D14;->c:I

    .line 1956645
    iput v2, v0, LX/D14;->d:I

    .line 1956646
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    invoke-virtual {v0, p1}, LX/D14;->a(LX/0Px;)V

    .line 1956647
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_4

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1956648
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->k()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v0

    .line 1956649
    iget-object v4, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    const/high16 v8, 0x3f000000    # 0.5f

    .line 1956650
    iget-object v7, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->s:LX/68w;

    if-nez v7, :cond_2

    .line 1956651
    const v7, 0x7f020e8f

    invoke-static {v7}, LX/690;->a(I)LX/68w;

    move-result-object v7

    iput-object v7, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->s:LX/68w;

    .line 1956652
    :cond_2
    new-instance v7, LX/699;

    invoke-direct {v7}, LX/699;-><init>()V

    invoke-virtual {v7, v8, v8}, LX/699;->a(FF)LX/699;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->s:LX/68w;

    .line 1956653
    iput-object v8, v7, LX/699;->c:LX/68w;

    .line 1956654
    move-object v7, v7

    .line 1956655
    new-instance v8, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->a()D

    move-result-wide v9

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->b()D

    move-result-wide v11

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1956656
    iput-object v8, v7, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1956657
    move-object v7, v7

    .line 1956658
    move-object v0, v7

    .line 1956659
    invoke-virtual {v5, v0}, LX/680;->a(LX/699;)LX/698;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1956660
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1956661
    :cond_3
    const v0, 0x3f570a3d    # 0.84f

    goto :goto_2

    .line 1956662
    :cond_4
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1956663
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    invoke-direct {p0, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;)V

    .line 1956664
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v0, v0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setCurrentPosition(I)V

    .line 1956665
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/698;

    iget-boolean v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/698;IZ)V

    .line 1956666
    :goto_4
    iput-boolean v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    .line 1956667
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v0, v0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1956668
    iget-object v7, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->l:LX/0hB;

    invoke-virtual {v7}, LX/0hB;->c()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v0, v7}, Landroid/view/View;->setTranslationX(F)V

    .line 1956669
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v9, 0x40400000    # 3.0f

    invoke-direct {v8, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const-wide/16 v9, 0x2bc

    invoke-virtual {v7, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1956670
    iget v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->u:I

    .line 1956671
    return-void

    .line 1956672
    :cond_5
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    const v2, 0x7f08297b

    invoke-static {v0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4
.end method

.method public final a(LX/680;)V
    .locals 3

    .prologue
    .line 1956755
    iput-object p1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    .line 1956756
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-virtual {p0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/680;->a(Z)V

    .line 1956757
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    new-instance v1, LX/D1H;

    invoke-direct {v1, p0}, LX/D1H;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;)V

    .line 1956758
    iput-object v1, v0, LX/680;->s:LX/D1H;

    .line 1956759
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    new-instance v1, LX/D1I;

    invoke-direct {v1, p0}, LX/D1I;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;)V

    .line 1956760
    iput-object v1, v0, LX/680;->m:LX/67y;

    .line 1956761
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    new-instance v1, LX/D1J;

    invoke-direct {v1, p0}, LX/D1J;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;)V

    .line 1956762
    iput-object v1, v0, LX/680;->g:LX/67n;

    .line 1956763
    iget-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    if-eqz v0, :cond_0

    .line 1956764
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->G:LX/697;

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/67e;->a(LX/697;I)LX/67d;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/680;->b(LX/67d;)V

    .line 1956765
    :cond_0
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->y:LX/D17;

    if-eqz v0, :cond_1

    .line 1956766
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->y:LX/D17;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-interface {v0, v1}, LX/D17;->a(LX/680;)V

    .line 1956767
    :cond_1
    return-void
.end method

.method public final a(LX/D1R;LX/D14;)V
    .locals 2

    .prologue
    .line 1956608
    iget-object v0, p1, LX/D1R;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    .line 1956609
    iput-object p2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    .line 1956610
    const v0, 0x7f020e8f

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->s:LX/68w;

    .line 1956611
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a:Ljava/util/ArrayList;

    .line 1956612
    iget-object v0, p1, LX/D1R;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->n:Ljava/lang/String;

    .line 1956613
    iget-object v0, p1, LX/D1R;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->A:Ljava/lang/String;

    .line 1956614
    iget-object v0, p1, LX/D1R;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->B:Ljava/lang/String;

    .line 1956615
    iget-object v0, p1, LX/D1R;->e:LX/D1T;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->C:LX/D1T;

    .line 1956616
    iget-object v0, p1, LX/D1R;->f:LX/697;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->G:LX/697;

    .line 1956617
    iget-object v0, p1, LX/D1R;->g:Landroid/view/View;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->D:Landroid/view/View;

    .line 1956618
    iget-boolean v0, p1, LX/D1R;->i:Z

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->F:Z

    .line 1956619
    iget v0, p1, LX/D1R;->j:F

    iput v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->z:F

    .line 1956620
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    .line 1956621
    iput-object p0, v0, LX/D14;->e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1956622
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->z:F

    .line 1956623
    iput v1, v0, LX/D14;->b:F

    .line 1956624
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v1, p1, LX/D1R;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    invoke-virtual {v0, v1}, LX/D14;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 1956625
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    .line 1956626
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->e:LX/D0y;

    invoke-virtual {p0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f()Ljava/util/Map;

    move-result-object v1

    .line 1956627
    const-string p0, "store_locator_open"

    invoke-static {v0, p0, v1}, LX/D0y;->a(LX/D0y;Ljava/lang/String;Ljava/util/Map;)V

    .line 1956628
    return-void
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1956603
    new-instance v0, LX/D1P;

    invoke-direct {v0, p0, p1}, LX/D1P;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1956604
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c:LX/0Zb;

    .line 1956605
    const-string p0, "store_locator_card_click"

    invoke-static {p0, v0}, LX/D1A;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v0, p0

    .line 1956606
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1956607
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;)V
    .locals 3
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1956595
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    .line 1956596
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->h:LX/17W;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956597
    :goto_0
    return-void

    .line 1956598
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1956599
    const-string v2, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1956600
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1956601
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->m:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1956602
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1956586
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-virtual {v0}, LX/680;->b()V

    .line 1956587
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/4Ua;

    if-nez v0, :cond_1

    .line 1956588
    :cond_0
    :goto_0
    return-void

    .line 1956589
    :cond_1
    check-cast p1, LX/4Ua;

    .line 1956590
    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 1956591
    new-instance v1, LX/D1L;

    invoke-direct {v1, p0, v0}, LX/D1L;-><init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;Lcom/facebook/graphql/error/GraphQLError;)V

    invoke-direct {p0, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1956592
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c:LX/0Zb;

    .line 1956593
    const-string p0, "store_locator_error"

    invoke-static {p0, v0}, LX/D1A;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v0, p0

    .line 1956594
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1956581
    const v0, 0x7f0d01cf

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 1956582
    const v0, 0x7f0d01d1

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1956583
    const v0, 0x7f0d01d0

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Integer;

    .line 1956584
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->d:LX/Bi1;

    sget-object v1, LX/Bi3;->STORE_LOCATOR:LX/Bi3;

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Bi1;->a(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1956585
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1956580
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->g:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1956516
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    if-eqz v1, :cond_0

    .line 1956517
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    invoke-virtual {v1}, LX/67m;->l()V

    .line 1956518
    :cond_0
    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->E:LX/698;

    .line 1956519
    iget-boolean v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->F:Z

    if-eqz v1, :cond_1

    .line 1956520
    new-instance v0, LX/4BY;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1956521
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->q:Landroid/content/Context;

    const v2, 0x7f08297a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1956522
    invoke-virtual {v0}, LX/4BY;->show()V

    .line 1956523
    :cond_1
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->D:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1956524
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->D:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1956525
    :cond_2
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    .line 1956526
    iget-object v2, v1, LX/680;->k:LX/31h;

    move-object v1, v2

    .line 1956527
    invoke-virtual {v1}, LX/31h;->a()LX/69F;

    move-result-object v2

    .line 1956528
    iget-object v2, v2, LX/69F;->c:Lcom/facebook/android/maps/model/LatLng;

    .line 1956529
    new-instance v3, Landroid/graphics/Point;

    iget-object v4, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->l:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    iget-object v5, v5, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v5}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v1, v3}, LX/31h;->a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    .line 1956530
    new-instance v3, LX/4FK;

    invoke-direct {v3}, LX/4FK;-><init>()V

    iget-wide v4, v2, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 1956531
    const-string v5, "north"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1956532
    move-object v3, v3

    .line 1956533
    iget-wide v4, v2, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 1956534
    const-string v4, "west"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1956535
    move-object v2, v3

    .line 1956536
    iget-wide v4, v1, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 1956537
    const-string v4, "south"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1956538
    move-object v2, v2

    .line 1956539
    iget-wide v4, v1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 1956540
    const-string v3, "east"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1956541
    move-object v1, v2

    .line 1956542
    iput-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->w:LX/4FK;

    .line 1956543
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->i:LX/D1g;

    new-instance v2, LX/D1h;

    invoke-direct {v2}, LX/D1h;-><init>()V

    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->n:Ljava/lang/String;

    .line 1956544
    iput-object v3, v2, LX/D1h;->a:Ljava/lang/String;

    .line 1956545
    move-object v2, v2

    .line 1956546
    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->w:LX/4FK;

    .line 1956547
    iput-object v3, v2, LX/D1h;->b:LX/4FK;

    .line 1956548
    move-object v2, v2

    .line 1956549
    iput-object p0, v2, LX/D1h;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1956550
    move-object v2, v2

    .line 1956551
    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->A:Ljava/lang/String;

    .line 1956552
    iput-object v3, v2, LX/D1h;->d:Ljava/lang/String;

    .line 1956553
    move-object v2, v2

    .line 1956554
    iget-object v3, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->B:Ljava/lang/String;

    .line 1956555
    iput-object v3, v2, LX/D1h;->e:Ljava/lang/String;

    .line 1956556
    move-object v2, v2

    .line 1956557
    iput-object v0, v2, LX/D1h;->f:LX/4BY;

    .line 1956558
    move-object v0, v2

    .line 1956559
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->o:LX/D14;

    invoke-virtual {v2}, LX/D14;->b()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    move-result-object v2

    .line 1956560
    iput-object v2, v0, LX/D1h;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 1956561
    move-object v0, v0

    .line 1956562
    invoke-virtual {v0}, LX/D1h;->a()LX/D1i;

    move-result-object v0

    .line 1956563
    iget-object v2, v0, LX/D1i;->f:LX/4BY;

    iput-object v2, v1, LX/D1g;->a:LX/4BY;

    .line 1956564
    new-instance v2, LX/4JU;

    invoke-direct {v2}, LX/4JU;-><init>()V

    .line 1956565
    iget-object v3, v0, LX/D1i;->b:LX/4FK;

    .line 1956566
    const-string v4, "geo_bounding_box"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1956567
    iget-object v3, v0, LX/D1i;->a:Ljava/lang/String;

    const-string v4, "NOT_SET"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1956568
    iget-object v3, v0, LX/D1i;->a:Ljava/lang/String;

    .line 1956569
    const-string v4, "ad_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956570
    :cond_3
    iget-object v3, v0, LX/D1i;->e:Ljava/lang/String;

    const-string v4, "NOT_SET"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1956571
    iget-object v3, v0, LX/D1i;->e:Ljava/lang/String;

    .line 1956572
    const-string v4, "parent_page_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956573
    :cond_4
    iget-object v3, v0, LX/D1i;->d:Ljava/lang/String;

    const-string v4, "NOT_SET"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1956574
    iget-object v3, v0, LX/D1i;->d:Ljava/lang/String;

    .line 1956575
    const-string v4, "page_set_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956576
    :cond_5
    new-instance v3, LX/D1V;

    invoke-direct {v3}, LX/D1V;-><init>()V

    move-object v3, v3

    .line 1956577
    const-string v4, "2"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "3"

    iget-object v5, v0, LX/D1i;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/D1V;

    .line 1956578
    iget-object v3, v1, LX/D1g;->b:LX/1Ck;

    sget-object v4, LX/D1f;->FETCH_LOCATIONS_TASK:LX/D1f;

    iget-object v5, v1, LX/D1g;->c:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v5, LX/D1e;

    invoke-direct {v5, v1, v0}, LX/D1e;-><init>(LX/D1g;LX/D1i;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1956579
    return-void
.end method

.method public final f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 1956515
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
