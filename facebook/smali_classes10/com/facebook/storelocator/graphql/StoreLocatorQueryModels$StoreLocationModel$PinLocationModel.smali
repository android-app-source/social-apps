.class public final Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x36c111bd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1956987
    const-class v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1956986
    const-class v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1956984
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1956985
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1956982
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1956983
    iget-wide v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1956976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1956977
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1956978
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1956979
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1956980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1956981
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1956988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1956989
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1956990
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1956972
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1956973
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->e:D

    .line 1956974
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->f:D

    .line 1956975
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1956970
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1956971
    iget-wide v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1956967
    new-instance v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    invoke-direct {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;-><init>()V

    .line 1956968
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1956969
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1956966
    const v0, -0x35098306    # -8076925.0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1956965
    const v0, 0x752a03d5

    return v0
.end method
