.class public final Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x14405404
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1957018
    const-class v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1957019
    const-class v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1957020
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1957021
    return-void
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957022
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1957023
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957024
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    .line 1957025
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    return-object v0
.end method

.method private n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957026
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    .line 1957027
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1957028
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1957029
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1957030
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1957031
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->c()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1957032
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1957033
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1957034
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->jn_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1957035
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1957036
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1957037
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1957038
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1957039
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1957040
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1957041
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1957042
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1957043
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1957044
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1957045
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1957046
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1957047
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1957048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1957049
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1957050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1957051
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1957052
    invoke-virtual {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1957053
    if-eqz v1, :cond_4

    .line 1957054
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1957055
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1957056
    :goto_0
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1957057
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1957058
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1957059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1957060
    iput-object v0, v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1957061
    :cond_0
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1957062
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    .line 1957063
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1957064
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1957065
    iput-object v0, v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    .line 1957066
    :cond_1
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1957067
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    .line 1957068
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1957069
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1957070
    iput-object v0, v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m:Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    .line 1957071
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1957072
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957013
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e:Ljava/lang/String;

    .line 1957014
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1956998
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->f:Ljava/util/List;

    .line 1956999
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1957015
    new-instance v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    invoke-direct {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;-><init>()V

    .line 1957016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1957017
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957000
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 1957001
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957002
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->h:Ljava/lang/String;

    .line 1957003
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1957004
    const v0, 0x772c0fd7

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957005
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->i:Ljava/lang/String;

    .line 1957006
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1957007
    const v0, 0x1284df6a

    return v0
.end method

.method public final synthetic j()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957008
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->m()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final jn_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957009
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j:Ljava/lang/String;

    .line 1957010
    iget-object v0, p0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic jo_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957011
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957012
    invoke-direct {p0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->n()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PinLocationModel;

    move-result-object v0

    return-object v0
.end method
