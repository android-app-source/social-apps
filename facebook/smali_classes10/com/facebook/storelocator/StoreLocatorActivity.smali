.class public Lcom/facebook/storelocator/StoreLocatorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private A:F

.field private B:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/D1g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/D1G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Lcom/facebook/android/maps/SupportMapFragment;

.field private u:LX/697;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1956296
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1956297
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1956298
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1956299
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1956300
    const v1, 0x7f082978

    invoke-virtual {p0, v1}, Lcom/facebook/storelocator/StoreLocatorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1956301
    new-instance v1, LX/D19;

    invoke-direct {v1, p0}, LX/D19;-><init>(Lcom/facebook/storelocator/StoreLocatorActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1956302
    return-void
.end method

.method private static a(Lcom/facebook/storelocator/StoreLocatorActivity;LX/0Zb;LX/D1g;Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/D1G;)V
    .locals 0

    .prologue
    .line 1956303
    iput-object p1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->p:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->q:LX/D1g;

    iput-object p3, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iput-object p4, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->s:LX/D1G;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/storelocator/StoreLocatorActivity;

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v3}, LX/D1g;->a(LX/0QB;)LX/D1g;

    move-result-object v1

    check-cast v1, LX/D1g;

    invoke-static {v3}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/0QB;)Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    move-result-object v2

    check-cast v2, Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-static {v3}, LX/D1G;->a(LX/0QB;)LX/D1G;

    move-result-object v3

    check-cast v3, LX/D1G;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/storelocator/StoreLocatorActivity;->a(Lcom/facebook/storelocator/StoreLocatorActivity;LX/0Zb;LX/D1g;Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/D1G;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1956304
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1956305
    invoke-static {p0, p0}, Lcom/facebook/storelocator/StoreLocatorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1956306
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->p:LX/0Zb;

    .line 1956307
    const-string v1, "store_locator_open"

    invoke-static {v1}, LX/D1A;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v1, v1

    .line 1956308
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1956309
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1956310
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1956311
    const v0, 0x7f0313c9

    invoke-virtual {p0, v0}, Lcom/facebook/storelocator/StoreLocatorActivity;->setContentView(I)V

    .line 1956312
    new-instance v0, LX/681;

    invoke-direct {v0}, LX/681;-><init>()V

    const-string v1, "ad_area_picker"

    .line 1956313
    iput-object v1, v0, LX/681;->m:Ljava/lang/String;

    .line 1956314
    move-object v0, v0

    .line 1956315
    const/4 v1, 0x0

    .line 1956316
    iput-boolean v1, v0, LX/681;->d:Z

    .line 1956317
    move-object v0, v0

    .line 1956318
    invoke-static {v0}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/681;)Lcom/facebook/android/maps/SupportMapFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->t:Lcom/facebook/android/maps/SupportMapFragment;

    .line 1956319
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d043e

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->t:Lcom/facebook/android/maps/SupportMapFragment;

    const-string v3, "map_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1956320
    invoke-virtual {p0}, Lcom/facebook/storelocator/StoreLocatorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1956321
    const-string v1, "east"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1956322
    const-string v1, "north"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1956323
    const-string v1, "south"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1956324
    const-string v1, "west"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1956325
    const-string v1, "ad_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->v:Ljava/lang/String;

    .line 1956326
    const-string v1, "page_set_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->w:Ljava/lang/String;

    .line 1956327
    const-string v1, "parent_page_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->x:Ljava/lang/String;

    .line 1956328
    const-string v1, "FULL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->y:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 1956329
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v0

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v1, v4, v5, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v1, v6, v7, v2, v3}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->u:LX/697;

    .line 1956330
    const v0, 0x7f0d1830

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->z:Landroid/view/View;

    .line 1956331
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->z:Landroid/view/View;

    new-instance v1, LX/D16;

    invoke-direct {v1, p0}, LX/D16;-><init>(Lcom/facebook/storelocator/StoreLocatorActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956332
    invoke-virtual {p0}, Lcom/facebook/storelocator/StoreLocatorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1ca6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->A:F

    .line 1956333
    const v0, 0x7f0d2da0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    iput-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->B:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1956334
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance v1, LX/D18;

    invoke-direct {v1, p0}, LX/D18;-><init>(Lcom/facebook/storelocator/StoreLocatorActivity;)V

    .line 1956335
    iput-object v1, v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->y:LX/D17;

    .line 1956336
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance v1, LX/D1Q;

    invoke-direct {v1, p0}, LX/D1Q;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->v:Ljava/lang/String;

    .line 1956337
    iput-object v2, v1, LX/D1Q;->b:Ljava/lang/String;

    .line 1956338
    move-object v1, v1

    .line 1956339
    iget v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->A:F

    .line 1956340
    iput v2, v1, LX/D1Q;->j:F

    .line 1956341
    move-object v1, v1

    .line 1956342
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->w:Ljava/lang/String;

    .line 1956343
    iput-object v2, v1, LX/D1Q;->c:Ljava/lang/String;

    .line 1956344
    move-object v1, v1

    .line 1956345
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->x:Ljava/lang/String;

    .line 1956346
    iput-object v2, v1, LX/D1Q;->d:Ljava/lang/String;

    .line 1956347
    move-object v1, v1

    .line 1956348
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->B:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1956349
    iput-object v2, v1, LX/D1Q;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1956350
    move-object v1, v1

    .line 1956351
    sget-object v2, LX/D1T;->STORE_VISITS_AD:LX/D1T;

    .line 1956352
    iput-object v2, v1, LX/D1Q;->e:LX/D1T;

    .line 1956353
    move-object v1, v1

    .line 1956354
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->z:Landroid/view/View;

    .line 1956355
    iput-object v2, v1, LX/D1Q;->g:Landroid/view/View;

    .line 1956356
    move-object v1, v1

    .line 1956357
    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->u:LX/697;

    .line 1956358
    iput-object v2, v1, LX/D1Q;->f:LX/697;

    .line 1956359
    move-object v1, v1

    .line 1956360
    invoke-virtual {v1}, LX/D1Q;->a()LX/D1R;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->s:LX/D1G;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/D1R;LX/D14;)V

    .line 1956361
    iget-object v0, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->t:Lcom/facebook/android/maps/SupportMapFragment;

    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/SupportMapFragment;->a(LX/68J;)V

    .line 1956362
    invoke-direct {p0}, Lcom/facebook/storelocator/StoreLocatorActivity;->a()V

    .line 1956363
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3a31b492

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1956364
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1956365
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->q:LX/D1g;

    invoke-virtual {v1}, LX/D1g;->a()V

    .line 1956366
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->r:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1956367
    iget-object v2, v1, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    if-eqz v2, :cond_0

    .line 1956368
    iget-object v2, v1, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    const/4 p0, 0x0

    .line 1956369
    iput-object p0, v2, LX/680;->s:LX/D1H;

    .line 1956370
    :cond_0
    const/16 v1, 0x23

    const v2, 0x352576b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x50f85530

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1956371
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1956372
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->q:LX/D1g;

    invoke-virtual {v1}, LX/D1g;->a()V

    .line 1956373
    iget-object v1, p0, Lcom/facebook/storelocator/StoreLocatorActivity;->t:Lcom/facebook/android/maps/SupportMapFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 1956374
    const/16 v1, 0x23

    const v2, 0x449954f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
