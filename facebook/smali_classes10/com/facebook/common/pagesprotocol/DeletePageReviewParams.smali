.class public Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1970669
    new-instance v0, LX/D9v;

    invoke-direct {v0}, LX/D9v;-><init>()V

    sput-object v0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1970670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970671
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    .line 1970672
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->b:Ljava/lang/String;

    .line 1970673
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->c:Ljava/lang/String;

    .line 1970674
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 1970675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970676
    iput-object p1, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    .line 1970677
    iput-object p2, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->b:Ljava/lang/String;

    .line 1970678
    iput-object p3, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->c:Ljava/lang/String;

    .line 1970679
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1970680
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1970681
    iget-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1970682
    iget-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1970683
    iget-object v0, p0, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1970684
    return-void
.end method
