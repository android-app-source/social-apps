.class public Lcom/facebook/common/intent/AppChooserDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static m:Ljava/lang/String;


# instance fields
.field public n:Lcom/facebook/content/SecureContextHelper;

.field public o:LX/Ei1;

.field public p:Landroid/content/Context;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ei2;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/String;

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2158914
    const-string v0, "AppChooserFragmentDialog"

    sput-object v0, Lcom/facebook/common/intent/AppChooserDialogFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2158915
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/common/intent/AppChooserDialogFragment;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Ei1;->a(LX/0QB;)LX/Ei1;

    move-result-object p0

    check-cast p0, LX/Ei1;

    iput-object v2, p1, Lcom/facebook/common/intent/AppChooserDialogFragment;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/common/intent/AppChooserDialogFragment;->o:LX/Ei1;

    iput-object v1, p1, Lcom/facebook/common/intent/AppChooserDialogFragment;->p:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 2158916
    const-class v0, Lcom/facebook/common/intent/AppChooserDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/common/intent/AppChooserDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2158917
    new-instance v1, Landroid/app/Dialog;

    iget-object v0, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->p:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 2158918
    new-instance v2, LX/Ehz;

    iget-object v0, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->p:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->q:Ljava/util/List;

    invoke-direct {v2, v0, v3}, LX/Ehz;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2158919
    iget-object v0, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->o:LX/Ei1;

    iget-object v3, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->t:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->s:Ljava/util/Map;

    .line 2158920
    if-eqz v3, :cond_0

    .line 2158921
    iput-object v3, v0, LX/Ei1;->a:Ljava/lang/String;

    .line 2158922
    :cond_0
    iput-object v4, v0, LX/Ei1;->c:Ljava/util/Map;

    .line 2158923
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 2158924
    const v0, 0x7f0300e8

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 2158925
    const v0, 0x7f0d0548

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2158926
    iget-object v3, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2158927
    const v0, 0x7f0d054a

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 2158928
    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2158929
    new-instance v2, LX/Ehv;

    invoke-direct {v2, p0, v1}, LX/Ehv;-><init>(Lcom/facebook/common/intent/AppChooserDialogFragment;Landroid/app/Dialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2158930
    new-instance v0, LX/Ehw;

    invoke-direct {v0, p0}, LX/Ehw;-><init>(Lcom/facebook/common/intent/AppChooserDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2158931
    new-instance v0, LX/Ehx;

    invoke-direct {v0, p0}, LX/Ehx;-><init>(Lcom/facebook/common/intent/AppChooserDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2158932
    return-object v1
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2158933
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 2158934
    iget-object v0, p0, Lcom/facebook/common/intent/AppChooserDialogFragment;->o:LX/Ei1;

    invoke-virtual {v0}, LX/Ei1;->a()V

    .line 2158935
    return-void
.end method
