.class public Lcom/facebook/feed/banner/GenericNotificationBanner;
.super LX/4nk;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1973061
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/banner/GenericNotificationBanner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1973062
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1973051
    invoke-direct {p0, p1, p2}, LX/4nk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1973052
    invoke-direct {p0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->b()V

    .line 1973053
    return-void
.end method

.method private a(LX/DBa;LX/2qq;)V
    .locals 4

    .prologue
    .line 1973056
    sget-object v0, LX/DBa;->NO_CONNECTION:LX/DBa;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    if-ne p1, v0, :cond_1

    .line 1973057
    :cond_0
    :goto_0
    return-void

    .line 1973058
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/DBa;->getBannerString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p1, LX/DBa;->isTemporaryBanner:Z

    invoke-super {p0, v0, v1, p2}, LX/4nk;->a(Ljava/lang/String;ZLX/2qq;)V

    .line 1973059
    sget-object v0, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    if-ne p1, v0, :cond_0

    .line 1973060
    iget-object v0, p0, LX/4nk;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {p0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/DBa;->getBannerString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/NotificationTextSwitcher;->b(Ljava/lang/CharSequence;J)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1973054
    const-class v0, Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-static {v0, p0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1973055
    return-void
.end method


# virtual methods
.method public final a(LX/DBa;)V
    .locals 1

    .prologue
    .line 1973049
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;LX/2qq;)V

    .line 1973050
    return-void
.end method
