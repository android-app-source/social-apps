.class public final Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;
.super Ljava/util/TimerTask;
.source ""


# instance fields
.field public final synthetic a:LX/1KW;


# direct methods
.method public constructor <init>(LX/1KW;)V
    .locals 0

    .prologue
    .line 1973170
    iput-object p1, p0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 19

    .prologue
    .line 1973171
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->k:LX/1UQ;

    if-nez v1, :cond_0

    .line 1973172
    :goto_0
    return-void

    .line 1973173
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->k:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->j:LX/0g8;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v3, v3, LX/1KW;->k:LX/1UQ;

    invoke-virtual {v3}, LX/1UQ;->c()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1973174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v2, v2, LX/1KW;->k:LX/1UQ;

    invoke-interface {v2, v1}, LX/1Qr;->h_(I)I

    move-result v5

    .line 1973175
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->d:LX/0r7;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, LX/0r7;->a(I)V

    .line 1973176
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->e:LX/1KY;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, LX/1KY;->a(I)V

    .line 1973177
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v2, v2, LX/1KW;->c:LX/1KX;

    invoke-virtual {v2}, LX/1KX;->d()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v3, v3, LX/1KW;->c:LX/1KX;

    invoke-virtual {v3}, LX/1KX;->c()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v4, v4, LX/1KW;->c:LX/1KX;

    invoke-virtual {v4}, LX/1KX;->e()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v6, v6, LX/1KW;->k:LX/1UQ;

    invoke-interface {v6}, LX/1Qr;->d()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v7, v7, LX/1KW;->d:LX/0r7;

    invoke-virtual {v7}, LX/0r7;->b()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v8, v8, LX/1KW;->e:LX/1KY;

    invoke-virtual {v8}, LX/1KY;->c()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    invoke-static {v9}, LX/1KW;->e(LX/1KW;)Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v10, v10, LX/1KW;->c:LX/1KX;

    invoke-virtual {v10}, LX/1KX;->f()Ljava/util/List;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v11, v11, LX/1KW;->c:LX/1KX;

    invoke-virtual {v11}, LX/1KX;->g()Ljava/util/List;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v12, v12, LX/1KW;->c:LX/1KX;

    invoke-virtual {v12}, LX/1KX;->i()Ljava/util/List;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v13, v13, LX/1KW;->c:LX/1KX;

    invoke-virtual {v13}, LX/1KX;->j()Ljava/util/List;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v14, v14, LX/1KW;->c:LX/1KX;

    invoke-virtual {v14}, LX/1KX;->k()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v15, v15, LX/1KW;->c:LX/1KX;

    invoke-virtual {v15}, LX/1KX;->l()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, LX/1KW;->e(LX/1KW;)LX/1KX;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/1KX;->m()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, LX/1KW;->e(LX/1KW;)LX/1KX;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, LX/1KX;->p()Ljava/util/Set;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, LX/1KW;->e(LX/1KW;)LX/1KX;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/1KX;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v18

    invoke-static/range {v1 .. v18}, LX/1KW;->a(LX/1KW;IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1973178
    :catch_0
    move-exception v1

    .line 1973179
    sget-object v2, LX/1KW;->a:Ljava/lang/String;

    const-string v3, "Debug view refresh exception"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1973180
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;->a:LX/1KW;

    iget-object v1, v1, LX/1KW;->j:LX/0g8;

    invoke-interface {v1}, LX/0g8;->r()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto/16 :goto_1
.end method
