.class public final Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2173354
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iput-object p2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2173355
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-boolean v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2173356
    :cond_0
    monitor-exit p0

    return-void

    .line 2173357
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/Eru;->b(Ljava/lang/String;)I

    move-result v2

    .line 2173358
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->h()I

    move-result v0

    .line 2173359
    add-int/2addr v0, v2

    if-lez v0, :cond_0

    .line 2173360
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    iget-object v3, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-static {v3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)I

    move-result v3

    invoke-interface {v0, v3}, LX/DC2;->a(I)V

    .line 2173361
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-static {v0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2173362
    :goto_0
    if-ge v0, v2, :cond_0

    .line 2173363
    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;->b:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a$redex0(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173364
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2173365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
