.class public final Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Erq;


# direct methods
.method public constructor <init>(LX/Erq;)V
    .locals 0

    .prologue
    .line 2173490
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;->a:LX/Erq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2173491
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;->a:LX/Erq;

    iget-object v0, v0, LX/Erq;->a:LX/Err;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;->a:LX/Erq;

    iget-object v1, v1, LX/Erq;->c:Ljava/util/List;

    const/4 v6, 0x0

    .line 2173492
    iput v6, v0, LX/Err;->v:I

    .line 2173493
    iget-object v2, v0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2173494
    :cond_0
    :goto_0
    return-void

    .line 2173495
    :cond_1
    invoke-static {v0}, LX/Err;->e(LX/Err;)I

    move-result v2

    invoke-static {v0, v2}, LX/Err;->b$redex0(LX/Err;I)V

    .line 2173496
    invoke-static {v0, v1}, LX/Err;->c(LX/Err;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2173497
    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, v0, LX/Err;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0oy;

    invoke-virtual {v2}, LX/0oy;->i()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 2173498
    iget-object v2, v0, LX/Err;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pn;

    iget-object v3, v0, LX/Err;->m:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Ym;

    invoke-virtual {v3}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    invoke-virtual {v2, v3, v4, v5}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v2

    .line 2173499
    iget-object v3, v0, LX/Err;->p:Ljava/util/List;

    iget v4, v0, LX/Err;->q:I

    invoke-virtual {v2, v6, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2173500
    iget-object v2, v0, LX/Err;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v3, v0, LX/Err;->p:Ljava/util/List;

    .line 2173501
    new-instance v4, LX/Ero;

    invoke-direct {v4, v0}, LX/Ero;-><init>(LX/Err;)V

    move-object v4, v4

    .line 2173502
    invoke-virtual {v2, v3, v4}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(Ljava/util/List;LX/DC2;)V

    goto :goto_0
.end method
