.class public final Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Erl;


# direct methods
.method public constructor <init>(LX/Erl;)V
    .locals 0

    .prologue
    .line 2173379
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2173376
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    iget-object v0, v0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CH7;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    iget-object v1, v1, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v1, v1, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    iget-object v2, v2, LX/Erl;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/CH2;->OFFLINE_FEED:LX/CH2;

    sget-object v4, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/CH7;->a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;

    .line 2173377
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    iget-object v0, v0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eri;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;->a:LX/Erl;

    iget-object v1, v1, LX/Erl;->f:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0, v1}, LX/Eri;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)V

    .line 2173378
    return-void
.end method
