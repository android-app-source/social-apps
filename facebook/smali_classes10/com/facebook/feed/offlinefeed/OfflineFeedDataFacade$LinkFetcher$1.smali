.class public final Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:LX/Erm;


# direct methods
.method public constructor <init>(LX/Erm;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 2173392
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->d:LX/Erm;

    iput-object p2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->b:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iput-object p4, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2173393
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->d:LX/Erm;

    iget-object v0, v0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Be;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/1Be;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2173394
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->d:LX/Erm;

    iget-object v0, v0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Be;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->b:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iget-object v3, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Be;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Z)V

    .line 2173395
    :goto_0
    return-void

    .line 2173396
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->d:LX/Erm;

    iget-object v0, v0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;->a:Ljava/lang/String;

    .line 2173397
    invoke-static {v0, v1, v2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 2173398
    goto :goto_0
.end method
