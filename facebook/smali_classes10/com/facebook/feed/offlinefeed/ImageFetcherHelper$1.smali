.class public final Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

.field public final synthetic b:LX/1Pf;

.field public final synthetic c:LX/Eri;


# direct methods
.method public constructor <init>(LX/Eri;Lcom/facebook/feed/model/ClientFeedUnitEdge;LX/1Pf;)V
    .locals 0

    .prologue
    .line 2173328
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->c:LX/Eri;

    iput-object p2, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    iput-object p3, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->b:LX/1Pf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2173329
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->c:LX/Eri;

    iget-object v0, v0, LX/Eri;->c:LX/1R0;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->b:LX/1Pf;

    invoke-interface {v0, v1, v2}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v2

    .line 2173330
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/1RA;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2173331
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2173332
    iget-object v3, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v0, v3

    .line 2173333
    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2173334
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->c:LX/Eri;

    iget-object v0, v0, LX/Eri;->d:LX/FSR;

    iget-object v3, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2173335
    iget-object v4, v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v3, v4

    .line 2173336
    invoke-virtual {v2, v1}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v4

    .line 2173337
    iget-object v5, v0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v5, v3, v4}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(Ljava/lang/String;LX/1Rb;)V

    .line 2173338
    :cond_0
    :try_start_0
    invoke-virtual {v2, v1}, LX/1RA;->e(I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2173339
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2173340
    :catch_0
    move-exception v3

    .line 2173341
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;->c:LX/Eri;

    iget-object v0, v0, LX/Eri;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "ImageFetcherHelper_ensurePreparedException"

    invoke-virtual {v3}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x64

    invoke-virtual {v0, v4, v3, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 2173342
    :cond_1
    return-void
.end method
