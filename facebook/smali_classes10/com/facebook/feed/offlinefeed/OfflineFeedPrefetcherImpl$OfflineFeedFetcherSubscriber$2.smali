.class public final Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Erq;


# direct methods
.method public constructor <init>(LX/Erq;)V
    .locals 0

    .prologue
    .line 2173503
    iput-object p1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;->a:LX/Erq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2173504
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;->a:LX/Erq;

    iget-object v0, v0, LX/Erq;->a:LX/Err;

    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;->a:LX/Erq;

    iget-object v1, v1, LX/Erq;->c:Ljava/util/List;

    .line 2173505
    iget-object v2, v0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2173506
    :cond_0
    :goto_0
    return-void

    .line 2173507
    :cond_1
    iget v3, v0, LX/Err;->v:I

    iget-object v2, v0, LX/Err;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Fn;

    .line 2173508
    iget-object v4, v2, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0W3;

    sget-wide v6, LX/0X5;->dW:J

    const/4 v5, 0x3

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    move v2, v4

    .line 2173509
    if-lt v3, v2, :cond_2

    .line 2173510
    new-instance v2, LX/DC6;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v3}, LX/DC6;-><init>(Ljava/lang/Integer;)V

    move-object v2, v2

    .line 2173511
    invoke-static {v0, v2}, LX/Err;->a$redex0(LX/Err;LX/DC6;)V

    .line 2173512
    :cond_2
    iget v2, v0, LX/Err;->v:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/Err;->v:I

    .line 2173513
    invoke-static {v0, v1}, LX/Err;->c(LX/Err;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2173514
    iget-object v2, v0, LX/Err;->w:Ljava/lang/String;

    sget-object v3, LX/0rU;->TAIL:LX/0rU;

    invoke-static {v0, v2, v3}, LX/Err;->a(LX/Err;Ljava/lang/String;LX/0rU;)V

    goto :goto_0
.end method
