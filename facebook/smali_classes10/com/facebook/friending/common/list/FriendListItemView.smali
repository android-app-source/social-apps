.class public Lcom/facebook/friending/common/list/FriendListItemView;
.super Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;
.source ""

# interfaces
.implements LX/EyQ;


# instance fields
.field public j:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2186110
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;)V

    .line 2186111
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2186107
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2186108
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2186113
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186114
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186115
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 2186093
    const v0, 0x7f0306d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2186094
    const v0, 0x7f0d0062

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186095
    sget-object v0, LX/03r;->FriendListItemView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2186096
    const/16 v1, 0x5

    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186097
    const/16 v2, 0x0

    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v3}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2186098
    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v3}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2186099
    iget-object v1, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v1}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2186100
    const/16 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186101
    const/16 v2, 0x3

    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v3}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2186102
    const/16 v3, 0x2

    iget-object v4, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v4}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 2186103
    const/16 v4, 0x4

    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 2186104
    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5, v1, v3, v2, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->setPadding(IIII)V

    .line 2186105
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2186106
    return-void
.end method


# virtual methods
.method public final a(LX/EyR;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186090
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186091
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    iget v1, p1, LX/EyR;->backgroundRes:I

    iget v2, p1, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186092
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186088
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186089
    return-void
.end method
