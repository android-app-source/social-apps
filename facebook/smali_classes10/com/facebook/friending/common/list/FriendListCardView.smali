.class public Lcom/facebook/friending/common/list/FriendListCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/EyQ;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public c:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2186070
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2186071
    const p1, 0x7f0306da

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2186072
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/friending/common/list/FriendListCardView;->setOrientation(I)V

    .line 2186073
    const p1, 0x7f0d1276

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendListCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2186074
    const p1, 0x7f0d1277

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2186075
    const p1, 0x7f0d1278

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186076
    return-void
.end method


# virtual methods
.method public final a(LX/EyR;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186067
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186068
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    iget v1, p1, LX/EyR;->backgroundRes:I

    iget v2, p1, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186069
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186065
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186066
    return-void
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186064
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186063
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186061
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186062
    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186059
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186060
    return-void
.end method

.method public setShowActionButton(Z)V
    .locals 2

    .prologue
    .line 2186056
    iget-object v1, p0, Lcom/facebook/friending/common/list/FriendListCardView;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186057
    return-void

    .line 2186058
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSubtitleText(I)V
    .locals 1

    .prologue
    .line 2186047
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2186048
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186054
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2186055
    return-void
.end method

.method public setThumbnailResource(I)V
    .locals 0

    .prologue
    .line 2186053
    return-void
.end method

.method public setThumbnailUri(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2186051
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-class v2, Lcom/facebook/friending/common/list/FriendListCardView;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2186052
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186049
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListCardView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2186050
    return-void
.end method
