.class public Lcom/facebook/friending/common/list/FriendListSmallCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/EyQ;
.implements LX/EyT;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/friends/ui/SmartButtonLite;

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2186170
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2186171
    const/4 p1, 0x1

    .line 2186172
    const v0, 0x7f0306dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2186173
    invoke-virtual {p0, p1}, Lcom/facebook/friending/common/list/FriendListSmallCardView;->setOrientation(I)V

    .line 2186174
    const v0, 0x7f0d127a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2186175
    const v0, 0x7f0d127d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    .line 2186176
    const v0, 0x7f0d127e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->c:Landroid/widget/TextView;

    .line 2186177
    const v0, 0x7f0d127c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186178
    const v0, 0x7f0d127b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->e:Landroid/view/View;

    .line 2186179
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2186180
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2186181
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2186182
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/friending/common/list/FriendListSmallCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f0e0120

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2186183
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/friending/common/list/FriendListSmallCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f0e012d

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2186184
    return-void
.end method


# virtual methods
.method public final a(LX/EyR;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186185
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186186
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    iget v1, p1, LX/EyR;->backgroundRes:I

    iget v2, p1, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186187
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186188
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186189
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186190
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186191
    return-void
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186192
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186193
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186160
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186161
    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186194
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186195
    return-void
.end method

.method public setFriendRequestButtonsVisible(Z)V
    .locals 2

    .prologue
    .line 2186165
    iget-object v1, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186166
    return-void

    .line 2186167
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186168
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186169
    return-void
.end method

.method public setNegativeButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2186164
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186162
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186163
    return-void
.end method

.method public setShowActionButton(Z)V
    .locals 2

    .prologue
    .line 2186157
    iget-object v1, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186158
    return-void

    .line 2186159
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSubtitleText(I)V
    .locals 1

    .prologue
    .line 2186155
    invoke-virtual {p0}, Lcom/facebook/friending/common/list/FriendListSmallCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friending/common/list/FriendListSmallCardView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2186156
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186153
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186154
    return-void
.end method

.method public setThumbnailResource(I)V
    .locals 0

    .prologue
    .line 2186152
    return-void
.end method

.method public setThumbnailUri(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2186150
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-class v2, Lcom/facebook/friending/common/list/FriendListSmallCardView;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2186151
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186148
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendListSmallCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186149
    return-void
.end method
