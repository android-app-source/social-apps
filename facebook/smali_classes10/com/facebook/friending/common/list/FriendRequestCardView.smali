.class public Lcom/facebook/friending/common/list/FriendRequestCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/EyQ;
.implements LX/EyT;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/friends/ui/SmartButtonLite;

.field public e:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2186220
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2186221
    const p1, 0x7f0306e3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2186222
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/friending/common/list/FriendRequestCardView;->setOrientation(I)V

    .line 2186223
    const p1, 0x7f0d1287

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2186224
    const p1, 0x7f0d127d

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->b:Landroid/widget/TextView;

    .line 2186225
    const p1, 0x7f0d127e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->c:Landroid/widget/TextView;

    .line 2186226
    const p1, 0x7f0d1288

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186227
    const p1, 0x7f0d1289

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object p1, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186228
    return-void
.end method


# virtual methods
.method public final a(LX/EyR;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186234
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186235
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    iget v1, p1, LX/EyR;->backgroundRes:I

    iget v2, p1, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186236
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186232
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186233
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186230
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186231
    return-void
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186229
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186219
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186217
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186218
    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186215
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186216
    return-void
.end method

.method public setFriendRequestButtonsVisible(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2186237
    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186238
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186239
    return-void

    :cond_0
    move v0, v2

    .line 2186240
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2186241
    goto :goto_1
.end method

.method public setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186213
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186214
    return-void
.end method

.method public setNegativeButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186211
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 2186212
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186209
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186210
    return-void
.end method

.method public setShowActionButton(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 2186205
    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->d:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186206
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186207
    return-void

    :cond_0
    move v0, v1

    .line 2186208
    goto :goto_0
.end method

.method public setSubtitleText(I)V
    .locals 1

    .prologue
    .line 2186196
    invoke-virtual {p0}, Lcom/facebook/friending/common/list/FriendRequestCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friending/common/list/FriendRequestCardView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2186197
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186203
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186204
    return-void
.end method

.method public setThumbnailResource(I)V
    .locals 0

    .prologue
    .line 2186202
    return-void
.end method

.method public setThumbnailUri(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2186200
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-class v2, Lcom/facebook/friending/common/list/FriendRequestCardView;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2186201
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186198
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186199
    return-void
.end method
