.class public Lcom/facebook/friending/common/list/FriendRequestItemView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""

# interfaces
.implements LX/EyT;


# instance fields
.field private j:Lcom/facebook/friends/ui/SmartButtonLite;

.field private k:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2186285
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2186286
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2186282
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2186283
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friending/common/list/FriendRequestItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186284
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2186279
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186280
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/common/list/FriendRequestItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186281
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2186255
    const v0, 0x7f0306e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2186256
    const v0, 0x7f0d1284

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186257
    const v0, 0x7f0d1283

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186258
    sget-object v0, LX/03r;->FriendRequestItemView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2186259
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186260
    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setCompoundDrawablePadding(I)V

    .line 2186261
    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setCompoundDrawablePadding(I)V

    .line 2186262
    const/16 v1, 0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186263
    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2186264
    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2186265
    const/16 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186266
    const/16 v2, 0x4

    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v3}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2186267
    const/16 v3, 0x3

    iget-object v4, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v4}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 2186268
    const/16 v4, 0x5

    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 2186269
    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5, v1, v3, v2, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->setPadding(IIII)V

    .line 2186270
    const/16 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186271
    const/16 v2, 0x4

    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v3}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2186272
    const/16 v3, 0x3

    iget-object v4, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v4}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 2186273
    const/16 v4, 0x5

    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5}, Lcom/facebook/friends/ui/SmartButtonLite;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 2186274
    iget-object v5, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v5, v1, v3, v2, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->setPadding(IIII)V

    .line 2186275
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2186276
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    iget v1, v1, LX/EyR;->backgroundRes:I

    sget-object v2, LX/EyR;->SECONDARY:LX/EyR;

    iget v2, v2, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186277
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    iget v1, v1, LX/EyR;->backgroundRes:I

    sget-object v2, LX/EyR;->PRIMARY:LX/EyR;

    iget v2, v2, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186278
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186242
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186243
    return-void
.end method

.method public setFriendRequestButtonsVisible(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2186250
    iget-object v3, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186251
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186252
    return-void

    :cond_0
    move v0, v2

    .line 2186253
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2186254
    goto :goto_1
.end method

.method public setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186248
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186249
    return-void
.end method

.method public setNegativeButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186246
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->j:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 2186247
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186244
    iget-object v0, p0, Lcom/facebook/friending/common/list/FriendRequestItemView;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186245
    return-void
.end method
