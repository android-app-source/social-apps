.class public Lcom/facebook/friending/jewel/ContactsSectionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/facepile/FacepileView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbButton;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2186372
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2186373
    const/4 v0, 0x0

    const v1, 0x7f0101fb

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/friending/jewel/ContactsSectionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186369
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2186370
    const v0, 0x7f0101fb

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friending/jewel/ContactsSectionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186371
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186366
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186367
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/jewel/ContactsSectionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186368
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 2186351
    const v0, 0x7f030374

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2186352
    const v0, 0x7f0d0b34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2186353
    const v0, 0x7f0d0b35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2186354
    const v0, 0x7f0d0b36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2186355
    sget-object v0, LX/03r;->ContactsSectionView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2186356
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->d:Z

    .line 2186357
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186358
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/friending/jewel/ContactsSectionView;->setActionButtonText(I)V

    .line 2186359
    :cond_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2186360
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2186361
    const/16 v3, 0x1

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 2186362
    const/16 v4, 0x3

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 2186363
    invoke-virtual {p0, v1, v3, v2, v4}, Lcom/facebook/friending/jewel/ContactsSectionView;->setPadding(IIII)V

    .line 2186364
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2186365
    return-void
.end method


# virtual methods
.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186339
    iget-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186340
    return-void
.end method

.method public setActionButtonText(I)V
    .locals 1

    .prologue
    .line 2186349
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/ContactsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friending/jewel/ContactsSectionView;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 2186350
    return-void
.end method

.method public setActionButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2186345
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->d:Z

    if-eqz v0, :cond_0

    .line 2186346
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friending/jewel/ContactsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 2186347
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2186348
    return-void
.end method

.method public setFaceUrls(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2186343
    iget-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 2186344
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186341
    iget-object v0, p0, Lcom/facebook/friending/jewel/ContactsSectionView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186342
    return-void
.end method
