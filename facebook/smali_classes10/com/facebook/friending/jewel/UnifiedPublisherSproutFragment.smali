.class public Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Eyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Ljava/lang/String;

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Ez3;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/Ez3;

.field public s:LX/0wd;

.field public t:LX/Eyw;

.field public u:Landroid/widget/LinearLayout;

.field public v:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/Ez3;",
            "LX/Ez2;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z

.field private final x:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2186791
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2186792
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->w:Z

    .line 2186793
    new-instance v0, LX/Eyy;

    invoke-direct {v0, p0}, LX/Eyy;-><init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->x:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2186794
    return-void
.end method

.method private a(Landroid/widget/LinearLayout;LX/0P2;LX/Ez3;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "LX/0P2",
            "<",
            "LX/Ez3;",
            "LX/Ez2;",
            ">;",
            "LX/Ez3;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x2

    .line 2186856
    new-instance v1, LX/BS8;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/BS8;-><init>(Landroid/content/Context;)V

    .line 2186857
    iget-object v0, v1, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v2, v0

    .line 2186858
    if-eqz p4, :cond_1

    sget-object v0, LX/2iJ;->BIG:LX/2iJ;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/uicontrib/fab/FabView;->setSize(LX/2iJ;)V

    .line 2186859
    iget-object v0, v1, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v0, v0

    .line 2186860
    iget v2, p3, LX/Ez3;->a:I

    move v2, v2

    .line 2186861
    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setFillColor(I)V

    .line 2186862
    iget-object v0, v1, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v0, v0

    .line 2186863
    iget v2, p3, LX/Ez3;->b:I

    move v2, v2

    .line 2186864
    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableID(I)V

    .line 2186865
    iget-object v0, v1, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v0, v0

    .line 2186866
    iget-object v2, p3, LX/Ez3;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2186867
    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186868
    iget-object v0, v1, LX/BS8;->b:Landroid/widget/TextView;

    move-object v0, v0

    .line 2186869
    iget-object v2, p3, LX/Ez3;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2186870
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186871
    invoke-virtual {v1}, LX/BS8;->a()V

    .line 2186872
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2186873
    invoke-virtual {v1}, LX/BS8;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2186874
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2186875
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2186876
    if-eqz p4, :cond_0

    .line 2186877
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2286

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2186878
    :cond_0
    new-instance v0, LX/Ez2;

    invoke-direct {v0, v1}, LX/Ez2;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, p3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2186879
    return-void

    .line 2186880
    :cond_1
    sget-object v0, LX/2iJ;->SMALL:LX/2iJ;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    const/16 v3, 0x97

    invoke-static {v2, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class p0, LX/Eyx;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Eyx;

    iput-object v1, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->m:LX/0wW;

    iput-object v3, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->n:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->o:LX/Eyx;

    return-void
.end method

.method public static l(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V
    .locals 5

    .prologue
    .line 2186848
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->t:LX/Eyw;

    const/4 v1, 0x0

    .line 2186849
    const-string v2, "sprout_cancel"

    invoke-static {v0, v2}, LX/Eyw;->c(LX/Eyw;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "sprout_cancel_reason"

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2186850
    iget-object v3, v0, LX/Eyw;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2186851
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    const/4 v1, 0x1

    .line 2186852
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2186853
    move-object v0, v0

    .line 2186854
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2186855
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2186845
    new-instance v0, LX/Eyz;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/Eyz;-><init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;Landroid/content/Context;I)V

    .line 2186846
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186847
    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x67559761

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2186835
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2186836
    const-class v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-static {v0, p0}, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2186837
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->o:LX/Eyx;

    sget-object v2, LX/Eyv;->FRIENDS_TAB:LX/Eyv;

    iget-object v3, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->p:Ljava/lang/String;

    .line 2186838
    new-instance v7, LX/Eyw;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v6

    check-cast v6, LX/0oz;

    invoke-direct {v7, v5, v6, v2, v3}, LX/Eyw;-><init>(LX/0Zb;LX/0oz;LX/Eyv;Ljava/lang/String;)V

    .line 2186839
    move-object v0, v7

    .line 2186840
    iput-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->t:LX/Eyw;

    .line 2186841
    const v0, 0x7f0e0b52

    invoke-virtual {p0, v4, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2186842
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->w:Z

    .line 2186843
    const v0, 0x37a8c62b

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2186844
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x35d605f2    # -2784899.5f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2186817
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->w:Z

    if-eqz v0, :cond_0

    .line 2186818
    const/4 v0, 0x0

    const/16 v1, 0x2b

    const v2, 0x71d654c1

    invoke-static {v5, v1, v2, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2186819
    :goto_0
    return-object v0

    .line 2186820
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186821
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->q:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186822
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186823
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->t:LX/Eyw;

    .line 2186824
    const-string v1, "sprout_open"

    invoke-static {v0, v1}, LX/Eyw;->c(LX/Eyw;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "connection_class"

    iget-object v5, v0, LX/Eyw;->b:LX/0oz;

    invoke-virtual {v5}, LX/0oz;->c()LX/0p3;

    move-result-object v5

    invoke-virtual {v5}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2186825
    iget-object v2, v0, LX/Eyw;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2186826
    const v0, 0x7f0306c6

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2186827
    const v0, 0x7f0d1251

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    .line 2186828
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 2186829
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->q:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez3;

    .line 2186830
    iget-object v7, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    invoke-direct {p0, v7, v5, v0, v3}, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->a(Landroid/widget/LinearLayout;LX/0P2;LX/Ez3;Z)V

    .line 2186831
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2186832
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    const/4 v3, 0x1

    invoke-direct {p0, v0, v5, v2, v3}, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->a(Landroid/widget/LinearLayout;LX/0P2;LX/Ez3;Z)V

    .line 2186833
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    .line 2186834
    const v0, -0x721ea0fb

    invoke-static {v0, v4}, LX/02F;->f(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3877b910

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186813
    iget-object v1, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    if-eqz v1, :cond_0

    .line 2186814
    iget-object v1, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 2186815
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2186816
    const/16 v1, 0x2b

    const v2, 0x3dada038

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x79983051

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186810
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->w:Z

    .line 2186811
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 2186812
    const/16 v1, 0x2b

    const v2, -0xcd539d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x692cc5ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186806
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2186807
    iget-boolean v1, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->w:Z

    if-eqz v1, :cond_0

    .line 2186808
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2186809
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x40c1bbd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2186795
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2186796
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->x:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2186797
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2186798
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ez3;

    .line 2186799
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez2;

    .line 2186800
    iget-object v0, v0, LX/Ez2;->a:LX/BS8;

    new-instance p2, LX/Ez0;

    invoke-direct {p2, p0, v1}, LX/Ez0;-><init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;LX/Ez3;)V

    invoke-virtual {v0, p2}, LX/BS8;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2186801
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    new-instance v1, LX/Ez1;

    invoke-direct {v1, p0}, LX/Ez1;-><init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186802
    const-wide/16 v8, 0x0

    .line 2186803
    iget-object v2, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->m:LX/0wW;

    invoke-virtual {v2}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    const-wide/high16 v6, 0x401c000000000000L    # 7.0

    invoke-static {v4, v5, v6, v7}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, LX/0wd;->b(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    .line 2186804
    iget-object v2, p0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    new-instance v3, LX/Ez4;

    invoke-direct {v3, p0}, LX/Ez4;-><init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2186805
    return-void
.end method
