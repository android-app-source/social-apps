.class public Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;
.super LX/1ZN;
.source ""


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2186614
    const-class v0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;

    sput-object v0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2186615
    sget-object v0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2186616
    return-void
.end method

.method private static a(Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 0

    .prologue
    .line 2186617
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->a:LX/0tX;

    iput-object p2, p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->b:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->c:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v2}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->a(Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x1c9c8282

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186618
    new-instance v1, LX/4JM;

    invoke-direct {v1}, LX/4JM;-><init>()V

    .line 2186619
    const-string v2, "FRIENDS_OF_FRIENDS"

    .line 2186620
    const-string v3, "audience"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186621
    new-instance v2, LX/EzV;

    invoke-direct {v2}, LX/EzV;-><init>()V

    move-object v2, v2

    .line 2186622
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2186623
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2186624
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2186625
    new-instance v2, LX/Eys;

    invoke-direct {v2, p0}, LX/Eys;-><init>(Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;)V

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2186626
    const/16 v1, 0x25

    const v2, -0x667606ab

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x3f46ffce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186627
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2186628
    invoke-static {p0, p0}, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2186629
    const/16 v1, 0x25

    const v2, 0x2eb2f098

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
