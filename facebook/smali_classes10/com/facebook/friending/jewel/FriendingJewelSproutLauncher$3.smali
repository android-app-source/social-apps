.class public final Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2hl;


# direct methods
.method public constructor <init>(LX/2hl;)V
    .locals 0

    .prologue
    .line 2186642
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;->a:LX/2hl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2186643
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2186644
    const-string v2, "profile_name"

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;->a:LX/2hl;

    iget-object v0, v0, LX/2hl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2186645
    iget-object v3, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v3

    .line 2186646
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186647
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;->a:LX/2hl;

    iget-object v2, v0, LX/2hl;->b:LX/17W;

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;->a:LX/2hl;

    iget-object v3, v0, LX/2hl;->c:Landroid/content/Context;

    sget-object v4, LX/0ax;->bO:Ljava/lang/String;

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$3;->a:LX/2hl;

    iget-object v0, v0, LX/2hl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2186648
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2186649
    aput-object v0, v5, v6

    const/4 v0, 0x1

    sget-object v6, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v6}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    sget-object v6, LX/DHr;->FRIENDS_TAB_SPROUT_LAUNCHER:LX/DHr;

    invoke-virtual {v6}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2186650
    return-void
.end method
