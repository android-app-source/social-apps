.class public Lcom/facebook/friending/jewel/SectionEndView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Landroid/widget/ProgressBar;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2186651
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friending/jewel/SectionEndView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2186652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2186684
    const v0, 0x7f0101fb

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friending/jewel/SectionEndView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186685
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2186680
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186681
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->a:Landroid/graphics/Paint;

    .line 2186682
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friending/jewel/SectionEndView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2186683
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2186661
    const v0, 0x7f0312c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2186662
    const v0, 0x7f0d2bb4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2186663
    const v0, 0x7f0d2bb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->d:Landroid/widget/ProgressBar;

    .line 2186664
    invoke-static {p1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->f:Z

    .line 2186665
    sget-object v0, LX/03r;->SectionEndView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2186666
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    .line 2186667
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 2186668
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 2186669
    iget-object v2, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 2186670
    iget-boolean v3, p0, Lcom/facebook/friending/jewel/SectionEndView;->f:Z

    if-eqz v3, :cond_2

    .line 2186671
    iget-object v3, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    neg-int v1, v1

    invoke-virtual {v3, v1, v5, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2186672
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->a:Landroid/graphics/Paint;

    const/16 v2, 0x5

    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0114

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2186673
    const/16 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->e:I

    .line 2186674
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablePadding(I)V

    .line 2186675
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2186676
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x2

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2186677
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2186678
    return-void

    .line 2186679
    :cond_2
    iget-object v3, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 2186686
    iget-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2186687
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2186688
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 2186689
    iget-boolean v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->f:Z

    if-eqz v1, :cond_2

    .line 2186690
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawablePadding()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2186691
    :goto_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2186692
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2186693
    :cond_0
    iget v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->e:I

    if-lez v0, :cond_1

    .line 2186694
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getPaddingLeft()I

    move-result v6

    .line 2186695
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getPaddingRight()I

    move-result v1

    sub-int v7, v0, v1

    .line 2186696
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getMeasuredHeight()I

    move-result v8

    .line 2186697
    int-to-float v1, v6

    const/4 v2, 0x0

    int-to-float v3, v7

    iget v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->e:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/friending/jewel/SectionEndView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2186698
    int-to-float v1, v6

    iget v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->e:I

    sub-int v0, v8, v0

    int-to-float v2, v0

    int-to-float v3, v7

    int-to-float v4, v8

    iget-object v5, p0, Lcom/facebook/friending/jewel/SectionEndView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2186699
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2186700
    return-void

    .line 2186701
    :cond_2
    iget-object v1, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2186657
    iget-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2186658
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getMeasuredHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 2186659
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2186660
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 2186655
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/SectionEndView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friending/jewel/SectionEndView;->setText(Ljava/lang/CharSequence;)V

    .line 2186656
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186653
    iget-object v0, p0, Lcom/facebook/friending/jewel/SectionEndView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186654
    return-void
.end method
