.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x208bfb92
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2181371
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2181372
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2181373
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2181374
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2181375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2181376
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2181377
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2181378
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2181379
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2181380
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2181381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2181382
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2181383
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->e:Ljava/util/List;

    .line 2181384
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2181385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2181386
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2181387
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2181388
    if-eqz v1, :cond_2

    .line 2181389
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;

    .line 2181390
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2181391
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2181392
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2181393
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2181394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;

    .line 2181395
    iput-object v0, v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2181396
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2181397
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2181398
    new-instance v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;

    invoke-direct {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;-><init>()V

    .line 2181399
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2181400
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2181401
    const v0, 0x5b69e103

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2181402
    const v0, 0x21eaae33

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181403
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2181404
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method
