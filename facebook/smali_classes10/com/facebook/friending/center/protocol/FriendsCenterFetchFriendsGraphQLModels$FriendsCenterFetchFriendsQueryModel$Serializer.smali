.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2181408
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;

    new-instance v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2181409
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2181407
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2181411
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2181412
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2181413
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2181414
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2181415
    if-eqz v2, :cond_0

    .line 2181416
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181417
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2181418
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2181419
    if-eqz v2, :cond_1

    .line 2181420
    const-string p0, "friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181421
    invoke-static {v1, v2, p1, p2}, LX/EvR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2181422
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2181423
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2181410
    check-cast p1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$Serializer;->a(Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
