.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x28ea34df
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2181871
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2181870
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2181868
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2181869
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2181861
    iput-object p1, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2181862
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2181863
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2181864
    if-eqz v0, :cond_0

    .line 2181865
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2181866
    :cond_0
    return-void

    .line 2181867
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2181847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2181848
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2181849
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2181850
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2181851
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2181852
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x6e424971

    invoke-static {v5, v4, v6}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2181853
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2181854
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2181855
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2181856
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2181857
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2181858
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2181859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2181860
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2181832
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2181833
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2181834
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2181835
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2181836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;

    .line 2181837
    iput-object v0, v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2181838
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2181839
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6e424971

    invoke-static {v2, v0, v3}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2181840
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2181841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;

    .line 2181842
    iput v3, v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->i:I

    move-object v1, v0

    .line 2181843
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2181844
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2181845
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2181846
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2181831
    new-instance v0, LX/EvW;

    invoke-direct {v0, p1}, LX/EvW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181830
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2181827
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2181828
    const/4 v0, 0x4

    const v1, -0x6e424971

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->i:I

    .line 2181829
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2181803
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2181804
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2181805
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2181806
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2181807
    :goto_0
    return-void

    .line 2181808
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2181824
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2181825
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2181826
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2181821
    new-instance v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;

    invoke-direct {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;-><init>()V

    .line 2181822
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2181823
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2181820
    const v0, -0x77c34f3a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2181819
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181817
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2181818
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181815
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->f:Ljava/lang/String;

    .line 2181816
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181813
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->g:Ljava/lang/String;

    .line 2181814
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181811
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2181812
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSocialContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2181809
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2181810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
