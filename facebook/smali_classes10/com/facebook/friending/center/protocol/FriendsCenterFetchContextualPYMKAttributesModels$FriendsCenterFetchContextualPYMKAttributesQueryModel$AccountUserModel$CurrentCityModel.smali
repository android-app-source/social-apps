.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180406
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180405
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2180403
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2180404
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2180395
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180396
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2180397
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2180398
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2180399
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2180400
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2180401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180402
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2180392
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180394
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2180378
    new-instance v0, LX/Ev2;

    invoke-direct {v0, p1}, LX/Ev2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180391
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2180389
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2180390
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2180388
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2180385
    new-instance v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;

    invoke-direct {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;-><init>()V

    .line 2180386
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2180387
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2180384
    const v0, 0x5c98b325

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2180383
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180381
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->e:Ljava/lang/String;

    .line 2180382
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180379
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->f:Ljava/lang/String;

    .line 2180380
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->f:Ljava/lang/String;

    return-object v0
.end method
