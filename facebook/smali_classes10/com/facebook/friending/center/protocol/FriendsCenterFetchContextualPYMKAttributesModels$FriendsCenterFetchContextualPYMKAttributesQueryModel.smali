.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x130eaccf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180881
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180857
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2180858
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2180859
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2180860
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180861
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2180862
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2180863
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2180864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180865
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2180866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180867
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2180868
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    .line 2180869
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2180870
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;

    .line 2180871
    iput-object v0, v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    .line 2180872
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180873
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180874
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    .line 2180875
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2180876
    new-instance v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;

    invoke-direct {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;-><init>()V

    .line 2180877
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2180878
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2180879
    const v0, -0x3f356fa8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2180880
    const v0, -0x6747e1ce

    return v0
.end method
