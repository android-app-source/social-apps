.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x596f36d9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180528
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2180527
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2180525
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2180526
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2180519
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180520
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2180521
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2180522
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2180523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180524
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2180511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2180512
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2180513
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    .line 2180514
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2180515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;

    .line 2180516
    iput-object v0, v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    .line 2180517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2180518
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180509
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    iput-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    .line 2180510
    iget-object v0, p0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->e:Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2180504
    new-instance v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;-><init>()V

    .line 2180505
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2180506
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2180508
    const v0, 0x5dcbe343

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2180507
    const v0, 0x4b57f312    # 1.4152466E7f

    return v0
.end method
