.class public final Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2181234
    const-class v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2181235
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2181236
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2181237
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2181238
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2181239
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2181240
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2181241
    if-eqz v2, :cond_0

    .line 2181242
    const-string p0, "contextual_pymk"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181243
    invoke-static {v1, v2, p1, p2}, LX/EvL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2181244
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2181245
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2181246
    check-cast p1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$Serializer;->a(Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
