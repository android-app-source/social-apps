.class public Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public a:LX/Ey7;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/2Ip;

.field public final c:LX/2hO;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field private final f:LX/17W;

.field public final g:LX/2iT;

.field public final h:LX/2dj;

.field public final i:LX/2do;

.field public final j:LX/0ad;

.field public final k:Landroid/content/res/Resources;

.field private final l:LX/1DI;

.field private final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/2iT;LX/2dj;LX/2do;LX/Ey7;LX/0ad;Landroid/content/res/Resources;LX/1DI;)V
    .locals 1
    .param p9    # LX/1DI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185965
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2185966
    new-instance v0, LX/Ey8;

    invoke-direct {v0, p0}, LX/Ey8;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)V

    iput-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->b:LX/2Ip;

    .line 2185967
    new-instance v0, LX/Ey9;

    invoke-direct {v0, p0}, LX/Ey9;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)V

    iput-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->c:LX/2hO;

    .line 2185968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    .line 2185969
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->m:Ljava/util/Map;

    .line 2185970
    iput-object p1, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->e:Landroid/content/Context;

    .line 2185971
    iput-object p2, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->f:LX/17W;

    .line 2185972
    iput-object p3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->g:LX/2iT;

    .line 2185973
    iput-object p4, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->h:LX/2dj;

    .line 2185974
    iput-object p5, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    .line 2185975
    iput-object p6, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a:LX/Ey7;

    .line 2185976
    iput-object p7, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->j:LX/0ad;

    .line 2185977
    iput-object p8, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    .line 2185978
    iput-object p9, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->l:LX/1DI;

    .line 2185979
    return-void
.end method

.method private static a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Eus;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2185995
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2185996
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    .line 2185997
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2185998
    new-instance v6, LX/Euq;

    invoke-direct {v6}, LX/Euq;-><init>()V

    .line 2185999
    iput-wide v2, v6, LX/Euq;->a:J

    .line 2186000
    move-object v2, v6

    .line 2186001
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2186002
    :goto_0
    iput-object v0, v2, LX/Euq;->c:Ljava/lang/String;

    .line 2186003
    move-object v0, v2

    .line 2186004
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2186005
    iput-object v2, v0, LX/Euq;->d:Ljava/lang/String;

    .line 2186006
    move-object v2, v0

    .line 2186007
    if-eqz v4, :cond_1

    invoke-virtual {v5, v4, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2186008
    :goto_1
    iput v0, v2, LX/Euq;->e:I

    .line 2186009
    move-object v0, v2

    .line 2186010
    sget-object v1, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    .line 2186011
    iput-object v1, v0, LX/Euq;->f:LX/2h7;

    .line 2186012
    move-object v0, v0

    .line 2186013
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2186014
    iput-object v1, v0, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186015
    move-object v0, v0

    .line 2186016
    invoke-virtual {v0}, LX/Euq;->b()LX/Eus;

    move-result-object v0

    .line 2186017
    return-object v0

    .line 2186018
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2186019
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2185989
    sget-object v0, LX/EyI;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2185990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :goto_0
    return-object v0

    .line 2185991
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 2185992
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 2185993
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 2185994
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/fig/button/FigButton;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 0
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2185985
    invoke-virtual {p0, p3}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2185986
    invoke-virtual {p0, p4}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2185987
    invoke-virtual {p0, p1, p2}, Lcom/facebook/fig/button/FigButton;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185988
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;I)V
    .locals 7

    .prologue
    .line 2185980
    iget-object v1, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->g:LX/2iT;

    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v2

    sget-object v4, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    new-instance v6, LX/EyG;

    invoke-direct {v6, p0, p1, p2, p3}, LX/EyG;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;I)V

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 2185981
    invoke-virtual {p1, p2}, LX/Eus;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185982
    invoke-static {p2}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185983
    invoke-virtual {p0, p3}, LX/1OM;->i_(I)V

    .line 2185984
    return-void
.end method

.method public static h(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)Z
    .locals 2

    .prologue
    .line 2185960
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a:LX/Ey7;

    .line 2185961
    iget-object v1, v0, LX/Ey7;->a:LX/Ey6;

    move-object v0, v1

    .line 2185962
    sget-object v1, LX/Ey6;->LOADING_STATE_LOADING:LX/Ey6;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a:LX/Ey7;

    .line 2185963
    iget-object v1, v0, LX/Ey7;->a:LX/Ey6;

    move-object v0, v1

    .line 2185964
    sget-object v1, LX/Ey6;->LOADING_STATE_ERROR:LX/Ey6;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2186020
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2186021
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 2186022
    :goto_1
    return v0

    .line 2186023
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2186024
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4
    .param p2    # I
        .annotation build Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter$ViewType;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2185954
    packed-switch p2, :pswitch_data_0

    .line 2185955
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ViewType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2185956
    :pswitch_0
    new-instance v0, LX/EyL;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03070b

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EyL;-><init>(Landroid/view/View;)V

    .line 2185957
    :goto_0
    return-object v0

    .line 2185958
    :pswitch_1
    new-instance v0, LX/EyK;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030712

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EyK;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2185959
    :pswitch_2
    new-instance v0, LX/EyJ;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030706

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EyJ;-><init>(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2185911
    instance-of v0, p1, LX/EyK;

    if-eqz v0, :cond_4

    .line 2185912
    check-cast p1, LX/EyK;

    .line 2185913
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    .line 2185914
    invoke-virtual {v0}, LX/Eus;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2185915
    iget-object v1, p1, LX/EyK;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/Eus;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-class v3, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2185916
    :cond_0
    iget-object v1, p1, LX/EyK;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, LX/Eus;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2185917
    iget-object v1, p1, LX/EyK;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2185918
    invoke-virtual {v0}, LX/Eus;->e()I

    move-result v2

    .line 2185919
    sget-object v3, LX/EyI;->a:[I

    invoke-virtual {v0}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 2185920
    :cond_1
    if-lez v2, :cond_7

    .line 2185921
    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2185922
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f0f005c

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2185923
    :goto_0
    iget-object v1, p1, LX/EyK;->o:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    const/16 v8, 0x102

    const/4 v7, 0x0

    .line 2185924
    sget-object v3, LX/EyI;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_1

    .line 2185925
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2185926
    :goto_1
    iget-object v1, p1, LX/EyK;->o:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/EyC;

    invoke-direct {v2, p0, v0, p2}, LX/EyC;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;I)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185927
    iget-object v1, p1, LX/EyK;->p:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/EyD;

    invoke-direct {v2, p0, v0}, LX/EyD;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185928
    iget-object v1, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->j:LX/0ad;

    sget-short v2, LX/2hr;->y:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2185929
    iget-object v1, p1, LX/EyK;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/EyE;

    invoke-direct {v2, p0, v0}, LX/EyE;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;)V

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185930
    :cond_2
    :goto_2
    return-void

    .line 2185931
    :cond_3
    iget-object v1, p1, LX/EyK;->q:Landroid/widget/LinearLayout;

    new-instance v2, LX/EyF;

    invoke-direct {v2, p0, v0}, LX/EyF;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2185932
    :cond_4
    instance-of v0, p1, LX/EyJ;

    if-eqz v0, :cond_2

    .line 2185933
    check-cast p1, LX/EyJ;

    .line 2185934
    iget-object v0, p1, LX/EyJ;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2185935
    iget-object v0, p1, LX/EyJ;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->l:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_2

    .line 2185936
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->j:LX/0ad;

    sget-short v5, LX/2hr;->A:S

    invoke-interface {v3, v5, v8}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2185937
    if-lez v2, :cond_5

    .line 2185938
    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2185939
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f0f005c

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2185940
    :cond_5
    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2185941
    :cond_6
    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2185942
    const v2, 0x7f080f85

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_0

    .line 2185943
    :pswitch_1
    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2185944
    const v2, 0x7f080f8a

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_0

    .line 2185945
    :pswitch_2
    invoke-virtual {v0}, LX/Eus;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v5, :cond_1

    .line 2185946
    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2185947
    const v2, 0x7f080f86

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_0

    .line 2185948
    :cond_7
    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2185949
    :pswitch_3
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f080f7b

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v6, 0x7f080f7c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x12

    const v7, 0x7f020886

    invoke-static {v1, v3, v5, v6, v7}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/fig/button/FigButton;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    goto/16 :goto_1

    .line 2185950
    :pswitch_4
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->j:LX/0ad;

    sget-short v5, LX/2hr;->A:S

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2185951
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f080f85

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x44

    const v6, 0x7f0207fd

    invoke-static {v1, v3, v7, v5, v6}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/fig/button/FigButton;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    goto/16 :goto_1

    .line 2185952
    :cond_8
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f080f7e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f02089e

    invoke-static {v1, v3, v7, v8, v5}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/fig/button/FigButton;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    goto/16 :goto_1

    .line 2185953
    :pswitch_5
    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v5, 0x7f080f76

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f02088f

    invoke-static {v1, v3, v7, v8, v5}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/fig/button/FigButton;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/2kM;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2185866
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2185867
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2185868
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2185869
    invoke-interface {p1, v1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2185870
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2185871
    iget-object v4, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->m:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2185872
    invoke-static {v0}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Eus;

    move-result-object v0

    .line 2185873
    iget-object v4, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2185874
    iget-object v4, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->m:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185875
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2185876
    :cond_1
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2185877
    return-void
.end method

.method public final a(LX/Eus;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2185890
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2185891
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2185892
    const-string v2, "timeline_friend_request_ref"

    sget-object v3, LX/5P2;->FRIENDS_CENTER:LX/5P2;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2185893
    new-instance v2, LX/5vg;

    invoke-direct {v2}, LX/5vg;-><init>()V

    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2185894
    iput-object v3, v2, LX/5vg;->d:Ljava/lang/String;

    .line 2185895
    move-object v2, v2

    .line 2185896
    new-instance v3, LX/4aM;

    invoke-direct {v3}, LX/4aM;-><init>()V

    invoke-virtual {p1}, LX/Eus;->d()Ljava/lang/String;

    move-result-object v4

    .line 2185897
    iput-object v4, v3, LX/4aM;->b:Ljava/lang/String;

    .line 2185898
    move-object v3, v3

    .line 2185899
    invoke-virtual {v3}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    .line 2185900
    iput-object v3, v2, LX/5vg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2185901
    move-object v2, v2

    .line 2185902
    invoke-virtual {p1}, LX/Eus;->b()Ljava/lang/String;

    move-result-object v3

    .line 2185903
    iput-object v3, v2, LX/5vg;->e:Ljava/lang/String;

    .line 2185904
    move-object v2, v2

    .line 2185905
    invoke-virtual {p1}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 2185906
    iput-object v3, v2, LX/5vg;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2185907
    move-object v2, v2

    .line 2185908
    invoke-virtual {v2}, LX/5vg;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    move-result-object v2

    invoke-static {v1, v2}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 2185909
    iget-object v2, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->f:LX/17W;

    iget-object v3, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->e:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2185910
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/Eus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2185886
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2185887
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2185888
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2185889
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2
    .annotation build Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter$ViewType;
    .end annotation

    .prologue
    .line 2185879
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2185880
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a:LX/Ey7;

    .line 2185881
    iget-object v1, v0, LX/Ey7;->a:LX/Ey6;

    move-object v0, v1

    .line 2185882
    sget-object v1, LX/Ey6;->LOADING_STATE_LOADING:LX/Ey6;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 2185883
    :goto_0
    return v0

    .line 2185884
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 2185885
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2185878
    iget-object v0, p0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p0}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->h(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
