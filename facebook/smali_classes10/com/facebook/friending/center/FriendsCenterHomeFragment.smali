.class public Lcom/facebook/friending/center/FriendsCenterHomeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Etk;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Etl;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ml;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/30I;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1wU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:LX/Eta;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/Eum;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/Eti;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:LX/EuV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private o:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/3kp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public r:LX/0hs;

.field public s:LX/EtZ;

.field public t:LX/0gG;

.field public u:LX/5P0;

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5P0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2178507
    const-class v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2178502
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2178503
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178504
    iput-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->k:LX/0Ot;

    .line 2178505
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178506
    iput-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->l:LX/0Ot;

    return-void
.end method

.method private static a(Lcom/facebook/friending/center/FriendsCenterHomeFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/1wU;LX/Eta;LX/Eum;LX/Eti;LX/EuV;LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/res/Resources;LX/1Ck;LX/3kp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friending/center/FriendsCenterHomeFragment;",
            "LX/0Or",
            "<",
            "LX/Etk;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Etl;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ml;",
            ">;",
            "LX/0Or",
            "<",
            "LX/30I;",
            ">;",
            "LX/1wU;",
            "LX/Eta;",
            "LX/Eum;",
            "LX/Eti;",
            "LX/EuV;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            "LX/1Ck;",
            "LX/3kp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2178501
    iput-object p1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->f:LX/1wU;

    iput-object p6, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->g:LX/Eta;

    iput-object p7, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->h:LX/Eum;

    iput-object p8, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->i:LX/Eti;

    iput-object p9, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->j:LX/EuV;

    iput-object p10, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->k:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->l:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->m:LX/0ad;

    iput-object p13, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->n:Landroid/content/res/Resources;

    iput-object p14, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->o:LX/1Ck;

    iput-object p15, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->p:LX/3kp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    const/16 v1, 0x2222

    invoke-static {v15, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2223

    invoke-static {v15, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x1127

    invoke-static {v15, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x44f

    invoke-static {v15, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v15}, LX/1wU;->b(LX/0QB;)LX/1wU;

    move-result-object v5

    check-cast v5, LX/1wU;

    const-class v6, LX/Eta;

    invoke-interface {v15, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Eta;

    invoke-static {v15}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object v7

    check-cast v7, LX/Eum;

    const-class v8, LX/Eti;

    invoke-interface {v15, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Eti;

    invoke-static {v15}, LX/EuV;->b(LX/0QB;)LX/EuV;

    move-result-object v9

    check-cast v9, LX/EuV;

    const/16 v10, 0x97

    invoke-static {v15, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xf12

    invoke-static {v15, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v15}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v14

    check-cast v14, LX/1Ck;

    invoke-static {v15}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v15

    check-cast v15, LX/3kp;

    invoke-static/range {v0 .. v15}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->a(Lcom/facebook/friending/center/FriendsCenterHomeFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/1wU;LX/Eta;LX/Eum;LX/Eti;LX/EuV;LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/res/Resources;LX/1Ck;LX/3kp;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2178508
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2178425
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2178426
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2178427
    const/4 v7, 0x0

    .line 2178428
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2178429
    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2178430
    const-string v0, "default_id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2178431
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2178432
    const-string v1, "attachment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5Oy;->valueOf(Ljava/lang/String;)LX/5Oy;

    move-result-object v1

    .line 2178433
    sget-object v0, LX/5Oy;->NO_ATTACHMENT:LX/5Oy;

    if-ne v1, v0, :cond_0

    .line 2178434
    :goto_0
    return-void

    .line 2178435
    :cond_0
    new-instance v0, LX/Eup;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2178436
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2178437
    const-string v5, "name"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2178438
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2178439
    const-string v6, "profile_pic"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Eup;-><init>(LX/5Oy;JLjava/lang/String;Ljava/lang/String;)V

    move-object v7, v0

    .line 2178440
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30I;

    .line 2178441
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ml;

    .line 2178442
    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, LX/Etl;

    .line 2178443
    invoke-virtual {v0}, LX/30I;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "android.permission.READ_CONTACTS"

    invoke-virtual {v1, v0}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v6}, LX/Etl;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v5, 0x1

    .line 2178444
    :goto_1
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Etk;

    .line 2178445
    iget-object v1, v0, LX/Etk;->b:LX/0ad;

    sget-char v2, LX/2hr;->k:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2178446
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2178447
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2178448
    array-length v8, v4

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v8, :cond_4

    aget-object v9, v4, v2

    .line 2178449
    invoke-static {v9}, LX/5P0;->fromString(Ljava/lang/String;)LX/5P0;

    move-result-object v9

    .line 2178450
    if-eqz v9, :cond_3

    sget-object p1, LX/5P0;->INVITES:LX/5P0;

    if-ne v9, p1, :cond_2

    if-eqz v5, :cond_3

    .line 2178451
    :cond_2
    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2178452
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2178453
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2178454
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2178455
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2178456
    const/4 v2, 0x4

    new-array v2, v2, [LX/5P0;

    const/4 v3, 0x0

    sget-object v4, LX/5P0;->SUGGESTIONS:LX/5P0;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/5P0;->SEARCH:LX/5P0;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, LX/5P0;->REQUESTS:LX/5P0;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, LX/5P0;->CONTACTS:LX/5P0;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2178457
    if-eqz v5, :cond_5

    .line 2178458
    sget-object v2, LX/5P0;->INVITES:LX/5P0;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2178459
    :cond_5
    sget-object v2, LX/5P0;->FRIENDS:LX/5P0;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2178460
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2178461
    move-object v2, v1

    .line 2178462
    :goto_3
    iget-object v1, v0, LX/Etk;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hL;

    .line 2178463
    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v2}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    :cond_6
    move-object v1, v2

    .line 2178464
    iput-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    .line 2178465
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2178466
    const-string v2, "fc_tab"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    const/4 v8, 0x0

    .line 2178467
    invoke-static {v1}, LX/5P0;->fromString(Ljava/lang/String;)LX/5P0;

    move-result-object v3

    .line 2178468
    if-nez v3, :cond_c

    .line 2178469
    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5P0;

    .line 2178470
    :cond_7
    :goto_4
    move-object v0, v3

    .line 2178471
    iput-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    .line 2178472
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->g:LX/Eta;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    .line 2178473
    new-instance v4, LX/EtZ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v1, v2, v3}, LX/EtZ;-><init>(Landroid/content/Context;LX/0Px;LX/0ad;)V

    .line 2178474
    move-object v0, v4

    .line 2178475
    iput-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->s:LX/EtZ;

    .line 2178476
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->i:LX/Eti;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->s:LX/EtZ;

    iget-object v4, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    invoke-virtual {v6}, LX/Etl;->a()Z

    move-result v6

    invoke-virtual/range {v0 .. v7}, LX/Eti;->a(LX/0gc;Landroid/content/Context;LX/EtZ;LX/0Px;ZZLX/Eup;)LX/Eth;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->t:LX/0gG;

    .line 2178477
    invoke-virtual {p0}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d()V

    .line 2178478
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->f:LX/1wU;

    invoke-virtual {v0}, LX/1wU;->b()LX/03R;

    move-result-object v0

    .line 2178479
    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_8

    .line 2178480
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->s:LX/EtZ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EtZ;->d(I)V

    .line 2178481
    :cond_8
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->h:LX/Eum;

    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    invoke-virtual {v1}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v1

    .line 2178482
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2178483
    const-string v3, "source_ref"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2178484
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2178485
    const-string v2, "unknown"

    .line 2178486
    :cond_9
    iget-object v3, v0, LX/Eum;->a:LX/0Zb;

    sget-object v4, LX/Eul;->FRIENDS_CENTER_OPENED:LX/Eul;

    invoke-static {v0, v4}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "initial_tab"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "source_ref"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2178487
    goto/16 :goto_0

    .line 2178488
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_b
    move-object v2, v1

    goto/16 :goto_3

    .line 2178489
    :cond_c
    const/4 v4, 0x0

    .line 2178490
    sget-object v9, LX/Etj;->a:[I

    invoke-virtual {v3}, LX/5P0;->ordinal()I

    move-result v1

    aget v9, v9, v1

    packed-switch v9, :pswitch_data_0

    .line 2178491
    :goto_5
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 2178492
    :cond_d
    :goto_6
    move-object v3, v3

    .line 2178493
    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5P0;

    goto/16 :goto_4

    .line 2178494
    :pswitch_0
    iget-object v4, v0, LX/Etk;->b:LX/0ad;

    sget-char v9, LX/2hr;->i:C

    const-string v1, ""

    invoke-interface {v4, v9, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2178495
    :pswitch_1
    iget-object v4, v0, LX/Etk;->b:LX/0ad;

    sget-char v9, LX/2hr;->h:C

    const-string v1, ""

    invoke-interface {v4, v9, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2178496
    :pswitch_2
    iget-object v4, v0, LX/Etk;->b:LX/0ad;

    sget-char v9, LX/2hr;->g:C

    const-string v1, ""

    invoke-interface {v4, v9, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2178497
    :pswitch_3
    iget-object v4, v0, LX/Etk;->b:LX/0ad;

    sget-char v9, LX/2hr;->e:C

    const-string v1, ""

    invoke-interface {v4, v9, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2178498
    :pswitch_4
    iget-object v4, v0, LX/Etk;->b:LX/0ad;

    sget-char v9, LX/2hr;->f:C

    const-string v1, ""

    invoke-interface {v4, v9, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2178499
    :cond_e
    invoke-static {v4}, LX/5P0;->fromString(Ljava/lang/String;)LX/5P0;

    move-result-object v4

    .line 2178500
    if-eqz v4, :cond_d

    move-object v3, v4

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 2178415
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->o:LX/1Ck;

    sget-object v1, LX/Etf;->FETCH_REQUESTS_BADGE:LX/Etf;

    invoke-virtual {v1}, LX/Etf;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->j:LX/EuV;

    sget-object v3, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2178416
    const-string v4, "You must provide a caller context"

    invoke-static {v3, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2178417
    new-instance v4, LX/84o;

    invoke-direct {v4}, LX/84o;-><init>()V

    move-object v4, v4

    .line 2178418
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2178419
    iput-object v3, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2178420
    move-object v4, v4

    .line 2178421
    iget-object v5, v2, LX/EuV;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2178422
    new-instance v5, LX/EuU;

    invoke-direct {v5, v2}, LX/EuU;-><init>(LX/EuV;)V

    iget-object v6, v2, LX/EuV;->b:LX/0TD;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2178423
    new-instance v3, LX/Ete;

    invoke-direct {v3, p0}, LX/Ete;-><init>(Lcom/facebook/friending/center/FriendsCenterHomeFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2178424
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x28ad72cf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2178414
    const v1, 0x7f030708

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x604c5b73

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x8d3516a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2178410
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->o:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2178411
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2178412
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2178413
    const/16 v1, 0x2b

    const v2, 0x631b746

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2178392
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2178393
    const v0, 0x7f0d12ce

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2178394
    if-eqz p2, :cond_0

    move v3, v1

    .line 2178395
    :goto_0
    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnlyCreatePagesImmediatelyOffscreen(Z)V

    .line 2178396
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->t:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2178397
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->t:LX/0gG;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2178398
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2178399
    const v1, 0x7f0d12cd

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2178400
    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2178401
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    iget-object v2, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->B_(I)V

    .line 2178402
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Etc;

    invoke-direct {v1, p0}, LX/Etc;-><init>(Lcom/facebook/friending/center/FriendsCenterHomeFragment;)V

    .line 2178403
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 2178404
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Etd;

    invoke-direct {v1, p0}, LX/Etd;-><init>(Lcom/facebook/friending/center/FriendsCenterHomeFragment;)V

    .line 2178405
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2178406
    iget-object v0, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    iget-object v1, v1, LX/5P0;->analyticsTag:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->a(Ljava/lang/String;)V

    .line 2178407
    return-void

    :cond_0
    move v3, v2

    .line 2178408
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2178409
    goto :goto_1
.end method
