.class public Lcom/facebook/friending/center/components/FriendItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public b:LX/EuG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/EuD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eu4;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eto;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ets;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2178944
    const-class v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2178919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2178920
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178921
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->d:LX/0Ot;

    .line 2178922
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178923
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->e:LX/0Ot;

    .line 2178924
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178925
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->f:LX/0Ot;

    .line 2178926
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178927
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->g:LX/0Ot;

    .line 2178928
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178929
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->h:LX/0Ot;

    .line 2178930
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/center/components/FriendItemComponentSpec;
    .locals 10

    .prologue
    .line 2178931
    const-class v1, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    monitor-enter v1

    .line 2178932
    :try_start_0
    sget-object v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178933
    sput-object v2, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178934
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178935
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178936
    new-instance v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    invoke-direct {v3}, Lcom/facebook/friending/center/components/FriendItemComponentSpec;-><init>()V

    .line 2178937
    invoke-static {v0}, LX/EuG;->a(LX/0QB;)LX/EuG;

    move-result-object v4

    check-cast v4, LX/EuG;

    invoke-static {v0}, LX/EuD;->a(LX/0QB;)LX/EuD;

    move-result-object v5

    check-cast v5, LX/EuD;

    const/16 v6, 0x222a

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2224

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2226

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2eb

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0xa79

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2178938
    iput-object v4, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->b:LX/EuG;

    iput-object v5, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->c:LX/EuD;

    iput-object v6, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->d:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->e:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->f:LX/0Ot;

    iput-object v9, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->g:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->h:LX/0Ot;

    .line 2178939
    move-object v0, v3

    .line 2178940
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178941
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178942
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
