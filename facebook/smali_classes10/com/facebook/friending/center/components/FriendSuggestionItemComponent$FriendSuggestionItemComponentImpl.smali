.class public final Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EuJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/EuJ;


# direct methods
.method public constructor <init>(LX/EuJ;)V
    .locals 1

    .prologue
    .line 2179371
    iput-object p1, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->c:LX/EuJ;

    .line 2179372
    move-object v0, p1

    .line 2179373
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2179374
    sget-object v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179375
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179376
    const-string v0, "FriendSuggestionItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2179377
    if-ne p0, p1, :cond_1

    .line 2179378
    :cond_0
    :goto_0
    return v0

    .line 2179379
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2179380
    goto :goto_0

    .line 2179381
    :cond_3
    check-cast p1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;

    .line 2179382
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2179383
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2179384
    if-eq v2, v3, :cond_0

    .line 2179385
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2179386
    goto :goto_0

    .line 2179387
    :cond_5
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-nez v2, :cond_4

    .line 2179388
    :cond_6
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2179389
    goto :goto_0

    .line 2179390
    :cond_7
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
