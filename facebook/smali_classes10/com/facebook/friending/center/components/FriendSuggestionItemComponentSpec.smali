.class public Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public b:LX/EuG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/EuD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ets;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2179434
    const-class v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179436
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179437
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->d:LX/0Ot;

    .line 2179438
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179439
    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->e:LX/0Ot;

    .line 2179440
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;
    .locals 7

    .prologue
    .line 2179441
    const-class v1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;

    monitor-enter v1

    .line 2179442
    :try_start_0
    sget-object v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179443
    sput-object v2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179444
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179445
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179446
    new-instance v5, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;

    invoke-direct {v5}, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;-><init>()V

    .line 2179447
    invoke-static {v0}, LX/EuG;->a(LX/0QB;)LX/EuG;

    move-result-object v3

    check-cast v3, LX/EuG;

    invoke-static {v0}, LX/EuD;->a(LX/0QB;)LX/EuD;

    move-result-object v4

    check-cast v4, LX/EuD;

    const/16 v6, 0x2226

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2179448
    iput-object v3, v5, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->b:LX/EuG;

    iput-object v4, v5, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->c:LX/EuD;

    iput-object v6, v5, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->d:LX/0Ot;

    iput-object p0, v5, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->e:LX/0Ot;

    .line 2179449
    move-object v0, v5

    .line 2179450
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179451
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179452
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
