.class public final Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EuG;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/EuG;


# direct methods
.method public constructor <init>(LX/EuG;)V
    .locals 1

    .prologue
    .line 2179299
    iput-object p1, p0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->c:LX/EuG;

    .line 2179300
    move-object v0, p1

    .line 2179301
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2179302
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179284
    const-string v0, "FriendListProfilePicture"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2179285
    if-ne p0, p1, :cond_1

    .line 2179286
    :cond_0
    :goto_0
    return v0

    .line 2179287
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2179288
    goto :goto_0

    .line 2179289
    :cond_3
    check-cast p1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    .line 2179290
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2179291
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2179292
    if-eq v2, v3, :cond_0

    .line 2179293
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2179294
    goto :goto_0

    .line 2179295
    :cond_5
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-nez v2, :cond_4

    .line 2179296
    :cond_6
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2179297
    goto :goto_0

    .line 2179298
    :cond_7
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
