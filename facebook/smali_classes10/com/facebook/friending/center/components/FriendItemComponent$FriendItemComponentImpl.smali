.class public final Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Etz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/Etz;


# direct methods
.method public constructor <init>(LX/Etz;)V
    .locals 1

    .prologue
    .line 2178862
    iput-object p1, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->c:LX/Etz;

    .line 2178863
    move-object v0, p1

    .line 2178864
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2178865
    sget-object v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2178866
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2178847
    const-string v0, "FriendItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2178848
    if-ne p0, p1, :cond_1

    .line 2178849
    :cond_0
    :goto_0
    return v0

    .line 2178850
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2178851
    goto :goto_0

    .line 2178852
    :cond_3
    check-cast p1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    .line 2178853
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2178854
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2178855
    if-eq v2, v3, :cond_0

    .line 2178856
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2178857
    goto :goto_0

    .line 2178858
    :cond_5
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-nez v2, :cond_4

    .line 2178859
    :cond_6
    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2178860
    goto :goto_0

    .line 2178861
    :cond_7
    iget-object v2, p1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
