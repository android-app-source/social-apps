.class public Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Z

.field public c:LX/EyM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/EuP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/Eui;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/Ey5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/Ey7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

.field public l:LX/3Af;

.field public m:LX/34b;

.field public n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Euu;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/1vq;

.field public r:I
    .annotation build Lcom/facebook/friending/center/model/FriendsCenterContextMenuItemModel$ContextType;
    .end annotation
.end field

.field public s:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/3wu;

.field public u:Landroid/support/v7/widget/RecyclerView;

.field public v:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2184945
    const-class v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2184946
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2184947
    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    .line 2184948
    iput v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->r:I

    .line 2184949
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V
    .locals 5

    .prologue
    .line 2184950
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-eqz v0, :cond_1

    .line 2184951
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    .line 2184952
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184953
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2184954
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 2184955
    if-eqz v0, :cond_0

    .line 2184956
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2184957
    :cond_0
    :goto_0
    return-void

    .line 2184958
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2184959
    sget-object v0, LX/Ey6;->LOADING_STATE_FINISHED:LX/Ey6;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    .line 2184960
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0

    .line 2184961
    :cond_2
    sget-object v0, LX/Ey6;->LOADING_STATE_LOADING:LX/Ey6;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    .line 2184962
    iget v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->r:I

    if-nez v0, :cond_3

    .line 2184963
    invoke-static {p0, p1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V

    goto :goto_0

    .line 2184964
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->n:Landroid/util/SparseArray;

    iget v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->r:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Euu;

    .line 2184965
    iget-object v1, v0, LX/Euu;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2184966
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->j:LX/1Ck;

    const-string v2, "FETCH_CONTEXTUAL_PYMK_SUGGESTIONS"

    new-instance v3, LX/ExV;

    invoke-direct {v3, p0, p1, v0}, LX/ExV;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)LX/0Vd;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2184967
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V
    .locals 1

    .prologue
    .line 2184971
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->h:LX/Ey7;

    .line 2184972
    iput-object p1, v0, LX/Ey7;->a:LX/Ey6;

    .line 2184973
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2184974
    return-void
.end method

.method public static b$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V
    .locals 4

    .prologue
    .line 2184968
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->j:LX/1Ck;

    const-string v1, "FETCH_SUGGESTIONS"

    new-instance v2, LX/ExU;

    invoke-direct {v2, p0, p1}, LX/ExU;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V

    invoke-static {p0, p1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2184969
    return-void
.end method

.method public static c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zS;",
            ")",
            "LX/0Vd",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2184970
    new-instance v0, LX/ExX;

    invoke-direct {v0, p0, p1}, LX/ExX;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V

    return-object v0
.end method

.method public static d(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2184937
    new-instance v0, LX/ExW;

    invoke-direct {v0, p0}, LX/ExW;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    return-object v0
.end method

.method public static e$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V
    .locals 3

    .prologue
    .line 2184938
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-eqz v0, :cond_0

    .line 2184939
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->c(ILjava/lang/Object;)V

    .line 2184940
    :goto_0
    return-void

    .line 2184941
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2184942
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->j:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2184943
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->b()V

    .line 2184944
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 2184856
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2184857
    if-eqz v0, :cond_0

    .line 2184858
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2184859
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2184860
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->i:LX/0ad;

    sget-short v2, LX/2hr;->x:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2184861
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f020876

    .line 2184862
    iput v2, v1, LX/108;->i:I

    .line 2184863
    move-object v1, v1

    .line 2184864
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2184865
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2184866
    new-instance v1, LX/ExN;

    invoke-direct {v1, p0}, LX/ExN;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    .line 2184867
    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2184868
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->j:LX/1Ck;

    const-string v1, "FETCH_CONTEXT_ATTRIBUTES"

    new-instance v2, LX/ExS;

    invoke-direct {v2, p0}, LX/ExS;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    new-instance v3, LX/ExT;

    invoke-direct {v3, p0}, LX/ExT;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2184869
    :cond_0
    :goto_0
    return-void

    .line 2184870
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2184920
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2184921
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    const-class v4, LX/EyM;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/EyM;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/EuP;->a(LX/0QB;)LX/EuP;

    move-result-object v6

    check-cast v6, LX/EuP;

    invoke-static {v0}, LX/Eui;->b(LX/0QB;)LX/Eui;

    move-result-object v7

    check-cast v7, LX/Eui;

    new-instance p1, LX/Ey5;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v9

    check-cast v9, LX/0hL;

    invoke-direct {p1, v8, v9}, LX/Ey5;-><init>(Landroid/content/res/Resources;LX/0hL;)V

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v9

    check-cast v9, LX/0hL;

    iput-object v8, p1, LX/Ey5;->a:Landroid/content/res/Resources;

    iput-object v9, p1, LX/Ey5;->b:LX/0hL;

    move-object v8, p1

    check-cast v8, LX/Ey5;

    invoke-static {v0}, LX/Ey7;->a(LX/0QB;)LX/Ey7;

    move-result-object v9

    check-cast v9, LX/Ey7;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v4, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->c:LX/EyM;

    iput-object v5, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->d:Landroid/content/Context;

    iput-object v6, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->e:LX/EuP;

    iput-object v7, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    iput-object v8, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->g:LX/Ey5;

    iput-object v9, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->h:LX/Ey7;

    iput-object p1, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->i:LX/0ad;

    iput-object v0, v3, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->j:LX/1Ck;

    .line 2184922
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->c:LX/EyM;

    new-instance v1, LX/ExP;

    invoke-direct {v1, p0}, LX/ExP;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    .line 2184923
    new-instance v3, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/2iT;->b(LX/0QB;)LX/2iT;

    move-result-object v6

    check-cast v6, LX/2iT;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v7

    check-cast v7, LX/2dj;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v8

    check-cast v8, LX/2do;

    invoke-static {v0}, LX/Ey7;->a(LX/0QB;)LX/Ey7;

    move-result-object v9

    check-cast v9, LX/Ey7;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    move-object v12, v1

    invoke-direct/range {v3 .. v12}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;-><init>(Landroid/content/Context;LX/17W;LX/2iT;LX/2dj;LX/2do;LX/Ey7;LX/0ad;Landroid/content/res/Resources;LX/1DI;)V

    .line 2184924
    move-object v0, v3

    .line 2184925
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    .line 2184926
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->i:LX/0ad;

    sget-short v1, LX/2ez;->p:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    .line 2184927
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->e:LX/EuP;

    invoke-virtual {v0}, LX/EuP;->a()LX/95R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->o:LX/95R;

    .line 2184928
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->o:LX/95R;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->o:LX/95R;

    invoke-virtual {v0}, LX/95R;->a()LX/2kW;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    .line 2184929
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-eqz v0, :cond_0

    .line 2184930
    new-instance v0, LX/ExY;

    invoke-direct {v0, p0}, LX/ExY;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->q:LX/1vq;

    .line 2184931
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->q:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2184932
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    .line 2184933
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184934
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v1, v0}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(LX/2kM;)V

    .line 2184935
    :cond_0
    return-void

    .line 2184936
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c48c303

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184871
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    .line 2184872
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->n:Landroid/util/SparseArray;

    .line 2184873
    const v1, 0x7f030711

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x206dba93

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7123454d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184874
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    if-eqz v1, :cond_0

    .line 2184875
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->c()V

    .line 2184876
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-eqz v1, :cond_1

    .line 2184877
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->q:LX/1vq;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2184878
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->o:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2184879
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->o:LX/95R;

    .line 2184880
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2184881
    const/16 v1, 0x2b

    const v2, 0x7b46abc9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2e9fad28

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184882
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    .line 2184883
    iget-object v4, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->g:LX/2iT;

    const/4 v5, 0x1

    .line 2184884
    iput-boolean v5, v4, LX/2hY;->d:Z

    .line 2184885
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    .line 2184886
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->t:LX/3wu;

    .line 2184887
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2184888
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-nez v1, :cond_0

    .line 2184889
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    .line 2184890
    iget-object v2, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    iget-object v4, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->b:LX/2Ip;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2184891
    iget-object v2, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    iget-object v4, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->c:LX/2hO;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2184892
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2184893
    const/16 v1, 0x2b

    const v2, 0x4fd030df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c3f0e27

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184894
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2184895
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2184896
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k()V

    .line 2184897
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x5bc319

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2184898
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2184899
    const v0, 0x7f0d12d7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    .line 2184900
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->t:LX/3wu;

    .line 2184901
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->t:LX/3wu;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    .line 2184902
    new-instance v2, LX/EyH;

    invoke-direct {v2, v1}, LX/EyH;-><init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)V

    move-object v1, v2

    .line 2184903
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2184904
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->t:LX/3wu;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2184905
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2184906
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->g:LX/Ey5;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2184907
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/ExQ;

    invoke-direct {v1, p0}, LX/ExQ;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2184908
    const v0, 0x7f0d12d6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2184909
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/ExR;

    invoke-direct {v1, p0}, LX/ExR;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2184910
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->p:LX/2kW;

    if-nez v0, :cond_0

    .line 2184911
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    .line 2184912
    iget-object v1, v0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    iget-object v2, v0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->b:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2184913
    iget-object v1, v0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    iget-object v2, v0, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->c:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2184914
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V

    .line 2184915
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2184916
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2184917
    if-eqz p1, :cond_0

    .line 2184918
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k()V

    .line 2184919
    :cond_0
    return-void
.end method
