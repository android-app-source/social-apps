.class public Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private B:LX/Exv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private C:LX/EuP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private D:LX/193;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private E:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private F:LX/Eum;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/Exz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private H:LX/Euo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ExM;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/2hd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private K:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public L:LX/Eui;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private N:LX/1Kt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Exw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2Ip;

.field private final e:LX/2hO;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/Eun;

.field private j:Z

.field private k:Z

.field private l:Z

.field public m:Ljava/lang/String;

.field private n:Z

.field public o:Z

.field private p:LX/0g8;

.field public q:Landroid/view/View;

.field private r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/database/DataSetObserver;

.field private u:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/1vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vq",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/195;

.field public y:LX/Exj;

.field public z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2184437
    const-class v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    sput-object v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->b:Ljava/lang/Class;

    .line 2184438
    const-class v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2184516
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2184517
    new-instance v0, LX/Ex5;

    invoke-direct {v0, p0}, LX/Ex5;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->d:LX/2Ip;

    .line 2184518
    new-instance v0, LX/Ex7;

    invoke-direct {v0, p0}, LX/Ex7;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e:LX/2hO;

    .line 2184519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->f:Ljava/util/List;

    .line 2184520
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    .line 2184521
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    .line 2184522
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->j:Z

    .line 2184523
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->k:Z

    .line 2184524
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->l:Z

    .line 2184525
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->n:Z

    .line 2184526
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o:Z

    .line 2184527
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184528
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->I:LX/0Ot;

    .line 2184529
    return-void
.end method

.method private static a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0Or;LX/0SG;LX/Exv;LX/EuP;LX/193;LX/2do;LX/Eum;LX/Exz;LX/Euo;LX/0Ot;LX/2hd;LX/0ad;LX/Eui;LX/1Ck;LX/1Kt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;",
            "LX/0Or",
            "<",
            "LX/Exw;",
            ">;",
            "LX/0SG;",
            "LX/Exv;",
            "LX/EuP;",
            "LX/193;",
            "LX/2do;",
            "LX/Eum;",
            "LX/Exz;",
            "LX/Euo;",
            "LX/0Ot",
            "<",
            "LX/ExM;",
            ">;",
            "LX/2hd;",
            "LX/0ad;",
            "LX/Eui;",
            "LX/1Ck;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2184515
    iput-object p1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->A:LX/0SG;

    iput-object p3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->B:LX/Exv;

    iput-object p4, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->C:LX/EuP;

    iput-object p5, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->D:LX/193;

    iput-object p6, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iput-object p7, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->F:LX/Eum;

    iput-object p8, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    iput-object p9, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->H:LX/Euo;

    iput-object p10, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->I:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->J:LX/2hd;

    iput-object p12, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->K:LX/0ad;

    iput-object p13, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    iput-object p14, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->M:LX/1Ck;

    iput-object p15, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->N:LX/1Kt;

    return-void
.end method

.method public static a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0zS;)V
    .locals 4

    .prologue
    .line 2184512
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Exj;->a(Z)V

    .line 2184513
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->M:LX/1Ck;

    const-string v1, "FETCH_SUGGESTIONS"

    new-instance v2, LX/ExE;

    invoke-direct {v2, p0, p1}, LX/ExE;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0zS;)V

    new-instance v3, LX/ExF;

    invoke-direct {v3, p0}, LX/ExF;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2184514
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    const/16 v1, 0x2245

    invoke-static {v15, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v15}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    const-class v3, LX/Exv;

    invoke-interface {v15, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Exv;

    invoke-static {v15}, LX/EuP;->a(LX/0QB;)LX/EuP;

    move-result-object v4

    check-cast v4, LX/EuP;

    const-class v5, LX/193;

    invoke-interface {v15, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/193;

    invoke-static {v15}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v6

    check-cast v6, LX/2do;

    invoke-static {v15}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object v7

    check-cast v7, LX/Eum;

    invoke-static {v15}, LX/Exz;->a(LX/0QB;)LX/Exz;

    move-result-object v8

    check-cast v8, LX/Exz;

    const-class v9, LX/Euo;

    invoke-interface {v15, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Euo;

    const/16 v10, 0x2241

    invoke-static {v15, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v15}, LX/2hd;->a(LX/0QB;)LX/2hd;

    move-result-object v11

    check-cast v11, LX/2hd;

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v15}, LX/Eui;->b(LX/0QB;)LX/Eui;

    move-result-object v13

    check-cast v13, LX/Eui;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v14

    check-cast v14, LX/1Ck;

    invoke-static {v15}, LX/23N;->b(LX/0QB;)LX/23N;

    move-result-object v15

    check-cast v15, LX/1Kt;

    invoke-static/range {v0 .. v15}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0Or;LX/0SG;LX/Exv;LX/EuP;LX/193;LX/2do;LX/Eum;LX/Exz;LX/Euo;LX/0Ot;LX/2hd;LX/0ad;LX/Eui;LX/1Ck;LX/1Kt;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2184503
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_1

    .line 2184504
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184505
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184506
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 2184507
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->l()V

    .line 2184508
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->b:Ljava/lang/Class;

    const-string v1, "Failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "FETCH_SUGGESTIONS"

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2184509
    return-void

    .line 2184510
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2184511
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->l()V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Eus;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2184476
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    .line 2184477
    if-eqz v0, :cond_0

    .line 2184478
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2184479
    new-instance v4, LX/Euq;

    invoke-direct {v4}, LX/Euq;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2184480
    iput-wide v6, v4, LX/Euq;->a:J

    .line 2184481
    move-object v4, v4

    .line 2184482
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2184483
    :goto_1
    iput-object v0, v4, LX/Euq;->c:Ljava/lang/String;

    .line 2184484
    move-object v0, v4

    .line 2184485
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v4

    .line 2184486
    iput-object v4, v0, LX/Euq;->d:Ljava/lang/String;

    .line 2184487
    move-object v4, v0

    .line 2184488
    if-eqz v2, :cond_2

    invoke-virtual {v3, v2, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2184489
    :goto_2
    iput v0, v4, LX/Euq;->e:I

    .line 2184490
    move-object v0, v4

    .line 2184491
    sget-object v1, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    .line 2184492
    iput-object v1, v0, LX/Euq;->f:LX/2h7;

    .line 2184493
    move-object v0, v0

    .line 2184494
    iput-object p1, v0, LX/Euq;->g:Ljava/lang/String;

    .line 2184495
    move-object v0, v0

    .line 2184496
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2184497
    iput-object v1, v0, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2184498
    move-object v0, v0

    .line 2184499
    invoke-virtual {v0}, LX/Euq;->b()LX/Eus;

    move-result-object v0

    return-object v0

    .line 2184500
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    goto :goto_0

    .line 2184501
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2184502
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 3

    .prologue
    .line 2184464
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_1

    .line 2184465
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184466
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184467
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2184468
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 2184469
    if-eqz v0, :cond_2

    .line 2184470
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2184471
    :cond_0
    :goto_0
    return-void

    .line 2184472
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->M:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2184473
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2184474
    sget-object v0, LX/0zS;->a:LX/0zS;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0zS;)V

    goto :goto_0

    .line 2184475
    :cond_2
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    goto :goto_0
.end method

.method public static d$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 1

    .prologue
    .line 2184459
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->j:Z

    if-nez v0, :cond_0

    .line 2184460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->j:Z

    .line 2184461
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    if-eqz v0, :cond_0

    .line 2184462
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2184463
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2184454
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2184455
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2184456
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2184457
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2184458
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2184449
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2184450
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f080039

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ex3;

    invoke-direct {v2, p0}, LX/Ex3;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2184451
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2184452
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2184453
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2184443
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2184444
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2184445
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2184446
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    const v1, 0x7f0818ac

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2184447
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    new-instance v1, LX/Ex4;

    invoke-direct {v1, p0}, LX/Ex4;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2184448
    return-void
.end method

.method public static n$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 1

    .prologue
    .line 2184439
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->b()V

    .line 2184440
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e()V

    .line 2184441
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    .line 2184442
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2184530
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->n:Z

    if-nez v0, :cond_1

    .line 2184531
    :cond_0
    :goto_0
    return-void

    .line 2184532
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2184533
    if-eqz v0, :cond_0

    .line 2184534
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2184535
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2184536
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 2

    .prologue
    .line 2184271
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_1

    .line 2184272
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184273
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184274
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 2184275
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m()V

    .line 2184276
    :cond_0
    :goto_0
    return-void

    .line 2184277
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2184278
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2184279
    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 2184280
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2184281
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2184282
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->K:LX/0ad;

    sget-short v2, LX/2ez;->p:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o:Z

    .line 2184283
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    .line 2184284
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->C:LX/EuP;

    invoke-virtual {v0}, LX/EuP;->a()LX/95R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->u:LX/95R;

    .line 2184285
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->u:LX/95R;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->u:LX/95R;

    invoke-virtual {v0}, LX/95R;->a()LX/2kW;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184286
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_2

    .line 2184287
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->B:LX/Exv;

    sget-object v2, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    .line 2184288
    new-instance v5, LX/Exu;

    invoke-static {v0}, LX/Exs;->b(LX/0QB;)LX/Exs;

    move-result-object v3

    check-cast v3, LX/Exs;

    const/16 p1, 0x2246

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {v5, v2, v3, p1}, LX/Exu;-><init>(LX/2h7;LX/Exs;LX/0Ot;)V

    .line 2184289
    move-object v0, v5

    .line 2184290
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184291
    new-instance v0, LX/ExG;

    invoke-direct {v0, p0}, LX/ExG;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->w:LX/1vq;

    .line 2184292
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->w:LX/1vq;

    invoke-virtual {v0, v2}, LX/2kW;->a(LX/1vq;)V

    .line 2184293
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184294
    iget-object v2, v0, LX/2kW;->o:LX/2kM;

    move-object v2, v2

    .line 2184295
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    check-cast v0, LX/Exu;

    invoke-virtual {v0, v2, v1}, LX/Exu;->a(LX/2kM;LX/0Px;)V

    .line 2184296
    :goto_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->D:LX/193;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "fc_suggestions_scroll_perf"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->x:LX/195;

    .line 2184297
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->H:LX/Euo;

    invoke-virtual {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v1

    const v2, 0x2f000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FriendCenterSuggestionsTabTTI"

    invoke-virtual {v0, v1, v2, v3}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    .line 2184298
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->k:Z

    if-nez v0, :cond_0

    .line 2184299
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    invoke-virtual {v0, v4}, LX/Eun;->a(Z)V

    .line 2184300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->k:Z

    .line 2184301
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->N:LX/1Kt;

    new-instance v1, LX/Ex8;

    invoke-direct {v1, p0}, LX/Ex8;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2184302
    return-void

    :cond_1
    move-object v0, v1

    .line 2184303
    goto :goto_0

    .line 2184304
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Exw;

    .line 2184305
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    goto :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2184306
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->g(I)V

    .line 2184307
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2184308
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2184309
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x69e5b1dd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2184310
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    .line 2184311
    iget-boolean v2, v0, LX/Exz;->h:Z

    move v0, v2

    .line 2184312
    if-eqz v0, :cond_0

    const v0, 0x7f030710

    .line 2184313
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x79e93654

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2184314
    :cond_0
    const v0, 0x7f03070f

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x16ed2e3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184315
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->l:Z

    if-eqz v1, :cond_0

    .line 2184316
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->F:LX/Eum;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184317
    iget v4, v2, LX/Exj;->e:I

    move v2, v4

    .line 2184318
    invoke-virtual {v1, v2}, LX/Eum;->d(I)V

    .line 2184319
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v1, :cond_1

    .line 2184320
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->w:LX/1vq;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2184321
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->u:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2184322
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->u:LX/95R;

    .line 2184323
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184324
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2184325
    :goto_0
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184326
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2184327
    const v1, 0x5e261873

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2184328
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o:Z

    if-eqz v1, :cond_2

    .line 2184329
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->c()V

    goto :goto_0

    .line 2184330
    :cond_2
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->d:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2184331
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x338bbfac

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184332
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-nez v1, :cond_0

    .line 2184333
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->M:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2184334
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->t:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/Exj;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2184335
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184336
    iget-object v2, v1, LX/Exj;->c:LX/Exs;

    invoke-virtual {v2}, LX/Exs;->a()V

    .line 2184337
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->t:Landroid/database/DataSetObserver;

    .line 2184338
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/2ii;)V

    .line 2184339
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/0fx;)V

    .line 2184340
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->N:LX/1Kt;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2184341
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2184342
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    .line 2184343
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2184344
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    .line 2184345
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->q:Landroid/view/View;

    .line 2184346
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2184347
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2184348
    const/16 v1, 0x2b

    const v2, 0x60e31527

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x701d550e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184349
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->n:Z

    .line 2184350
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->x:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 2184351
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2184352
    const/16 v1, 0x2b

    const v2, 0x568379ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6a835f92

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184353
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2184354
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->n:Z

    .line 2184355
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2184356
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o()V

    .line 2184357
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v1, :cond_2

    .line 2184358
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2184359
    :cond_1
    :goto_0
    const v1, 0x35091d31

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2184360
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o:Z

    if-nez v1, :cond_1

    .line 2184361
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->d:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2184362
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->E:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v2, 0x102000a

    .line 2184363
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2184364
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    .line 2184365
    iget v1, v0, LX/Exz;->i:I

    if-lez v1, :cond_0

    .line 2184366
    const v1, 0x7f0a009e

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2184367
    const v1, 0x7f0d12d5

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    const p2, 0x7f0a009e

    invoke-virtual {v1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2184368
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->A:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    .line 2184369
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->q:Landroid/view/View;

    .line 2184370
    const v0, 0x7f0d12c9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2184371
    const v0, 0x7f0d12ca

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->s:Landroid/widget/TextView;

    .line 2184372
    new-instance v0, LX/Ex9;

    invoke-direct {v0, p0}, LX/Ex9;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->t:Landroid/database/DataSetObserver;

    .line 2184373
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Exj;->a(Ljava/lang/String;)V

    .line 2184374
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184375
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Exj;->b:Z

    .line 2184376
    iget-object v1, v0, LX/Exj;->c:LX/Exs;

    const-string p1, "suggestion"

    .line 2184377
    iput-object p1, v1, LX/Exs;->u:Ljava/lang/String;

    .line 2184378
    iget-object v1, v0, LX/Exj;->c:LX/Exs;

    new-instance p1, LX/Exg;

    invoke-direct {p1, v0}, LX/Exg;-><init>(LX/Exj;)V

    .line 2184379
    iput-object p1, v1, LX/Exs;->r:LX/Ew6;

    .line 2184380
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->t:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, LX/Exj;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2184381
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    .line 2184382
    iget-boolean v1, v0, LX/Exz;->h:Z

    move v0, v1

    .line 2184383
    if-eqz v0, :cond_3

    .line 2184384
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    .line 2184385
    new-instance v3, LX/62T;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x2

    invoke-direct {v3, p1, p2}, LX/62T;-><init>(Landroid/content/Context;I)V

    .line 2184386
    new-instance p1, LX/Exx;

    invoke-direct {p1, v1, v2}, LX/Exx;-><init>(LX/Exz;LX/Exj;)V

    move-object p1, p1

    .line 2184387
    iput-object p1, v3, LX/3wu;->h:LX/3wr;

    .line 2184388
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2184389
    new-instance v3, LX/Exy;

    invoke-direct {v3, v1, v2}, LX/Exy;-><init>(LX/Exz;LX/Exj;)V

    move-object v3, v3

    .line 2184390
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2184391
    new-instance v3, LX/0g7;

    invoke-direct {v3, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    move-object v0, v3

    .line 2184392
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    .line 2184393
    :goto_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2184394
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    new-instance v1, LX/ExA;

    invoke-direct {v1, p0}, LX/ExA;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 2184395
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->N:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2184396
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    .line 2184397
    iget-boolean v1, v0, LX/Exz;->h:Z

    move v0, v1

    .line 2184398
    if-nez v0, :cond_1

    .line 2184399
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    new-instance v1, LX/ExB;

    invoke-direct {v1, p0}, LX/ExB;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2ii;)V

    .line 2184400
    :cond_1
    const v0, 0x7f0d12d4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2184401
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    new-instance v1, LX/ExC;

    invoke-direct {v1, p0}, LX/ExC;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    invoke-virtual {v0, v1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2184402
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->e()V

    .line 2184403
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_5

    .line 2184404
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    .line 2184405
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2184406
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-nez v0, :cond_2

    .line 2184407
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2184408
    :cond_2
    :goto_1
    return-void

    .line 2184409
    :cond_3
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->G:LX/Exz;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2184410
    iget v2, v1, LX/Exz;->i:I

    if-lez v2, :cond_4

    .line 2184411
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2184412
    iget v2, v1, LX/Exz;->i:I

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setDividerHeight(I)V

    .line 2184413
    :cond_4
    new-instance v2, LX/2iI;

    invoke-direct {v2, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    move-object v0, v2

    .line 2184414
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p:LX/0g8;

    goto :goto_0

    .line 2184415
    :cond_5
    sget-object v0, LX/0zS;->a:LX/0zS;

    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0zS;)V

    goto :goto_1
.end method

.method public final setUserVisibleHint(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 2184416
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 2184417
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2184418
    if-eqz p1, :cond_0

    .line 2184419
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->o()V

    .line 2184420
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->l:Z

    .line 2184421
    :cond_0
    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    .line 2184422
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    if-eqz v0, :cond_1

    .line 2184423
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Eun;->a(Z)V

    .line 2184424
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->k:Z

    .line 2184425
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    .line 2184426
    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    .line 2184427
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2184428
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->J:LX/2hd;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    .line 2184429
    iget-object v3, v0, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2184430
    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2184431
    invoke-virtual {v0}, LX/Eus;->p()V

    .line 2184432
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2184433
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2184434
    :cond_4
    if-eqz p1, :cond_5

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->j:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    if-eqz v0, :cond_5

    .line 2184435
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->i:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2184436
    :cond_5
    return-void
.end method
