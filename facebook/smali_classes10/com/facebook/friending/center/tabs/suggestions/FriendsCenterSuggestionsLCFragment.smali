.class public Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fv;


# instance fields
.field public a:LX/EuP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Exc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Euo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Eum;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/Eun;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field public l:I

.field private final m:LX/2hO;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2184606
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    .line 2184607
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    .line 2184608
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->h:Z

    .line 2184609
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->i:Z

    .line 2184610
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->j:Z

    .line 2184611
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->k:Z

    .line 2184612
    new-instance v0, LX/ExI;

    invoke-direct {v0, p0}, LX/ExI;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->m:LX/2hO;

    .line 2184613
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;Z)V
    .locals 2

    .prologue
    .line 2184614
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->h:Z

    if-nez v0, :cond_0

    .line 2184615
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->h:Z

    .line 2184616
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    if-eqz v0, :cond_0

    .line 2184617
    if-eqz p1, :cond_1

    .line 2184618
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2184619
    :cond_0
    :goto_0
    return-void

    .line 2184620
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    .line 2184621
    iget-object v1, v0, LX/Eun;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget p0, v0, LX/Eun;->c:I

    iget-object p1, v0, LX/Eun;->d:Ljava/lang/String;

    invoke-interface {v1, p0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2184622
    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2184623
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->k:Z

    if-nez v0, :cond_1

    .line 2184624
    :cond_0
    :goto_0
    return-void

    .line 2184625
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2184626
    if-eqz v0, :cond_0

    .line 2184627
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2184628
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2184629
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/Runnable;)LX/1X1;
    .locals 2

    .prologue
    .line 2184651
    const/4 v0, 0x0

    .line 2184652
    new-instance v1, LX/EuL;

    invoke-direct {v1}, LX/EuL;-><init>()V

    .line 2184653
    sget-object p0, LX/EuM;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EuK;

    .line 2184654
    if-nez p0, :cond_0

    .line 2184655
    new-instance p0, LX/EuK;

    invoke-direct {p0}, LX/EuK;-><init>()V

    .line 2184656
    :cond_0
    invoke-static {p0, p1, v0, v0, v1}, LX/EuK;->a$redex0(LX/EuK;LX/1De;IILX/EuL;)V

    .line 2184657
    move-object v1, p0

    .line 2184658
    move-object v0, v1

    .line 2184659
    move-object v0, v0

    .line 2184660
    const v1, 0x7f0818ac

    .line 2184661
    iget-object p0, v0, LX/EuK;->a:LX/EuL;

    invoke-virtual {v0, v1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/EuL;->a:Ljava/lang/CharSequence;

    .line 2184662
    iget-object p0, v0, LX/EuK;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2184663
    move-object v0, v0

    .line 2184664
    iget-object v1, v0, LX/EuK;->a:LX/EuL;

    iput-object p2, v1, LX/EuL;->b:Ljava/lang/Runnable;

    .line 2184665
    iget-object v1, v0, LX/EuK;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2184666
    move-object v0, v0

    .line 2184667
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 3

    .prologue
    .line 2184630
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->b:LX/Exc;

    .line 2184631
    new-instance v1, LX/Exa;

    invoke-direct {v1, v0}, LX/Exa;-><init>(LX/Exc;)V

    .line 2184632
    iget-object v2, v0, LX/Exc;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ExZ;

    .line 2184633
    if-nez v2, :cond_0

    .line 2184634
    new-instance v2, LX/ExZ;

    invoke-direct {v2, v0}, LX/ExZ;-><init>(LX/Exc;)V

    .line 2184635
    :cond_0
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2184636
    iput-object v1, v2, LX/ExZ;->a:LX/Exa;

    .line 2184637
    iget-object v0, v2, LX/ExZ;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2184638
    move-object v1, v2

    .line 2184639
    move-object v0, v1

    .line 2184640
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->f:LX/95R;

    .line 2184641
    iget-object v2, v0, LX/ExZ;->a:LX/Exa;

    iput-object v1, v2, LX/Exa;->c:LX/95R;

    .line 2184642
    iget-object v2, v0, LX/ExZ;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2184643
    move-object v0, v0

    .line 2184644
    invoke-virtual {v0, p2}, LX/ExZ;->b(LX/BcQ;)LX/ExZ;

    move-result-object v0

    invoke-virtual {v0}, LX/ExZ;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Bdl;)LX/Bdl;
    .locals 3

    .prologue
    .line 2184645
    new-instance v0, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/62X;-><init>(II)V

    .line 2184646
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b08a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2184647
    iput v1, v0, LX/62X;->b:I

    .line 2184648
    iput v1, v0, LX/62X;->c:I

    .line 2184649
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/Bdl;)LX/Bdl;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bdl;->a(LX/3x6;)LX/Bdl;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2184650
    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2184596
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2184597
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;

    invoke-static {v0}, LX/EuP;->a(LX/0QB;)LX/EuP;

    move-result-object v3

    check-cast v3, LX/EuP;

    invoke-static {v0}, LX/Exc;->a(LX/0QB;)LX/Exc;

    move-result-object v4

    check-cast v4, LX/Exc;

    const-class v5, LX/Euo;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Euo;

    invoke-static {v0}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object p1

    check-cast p1, LX/Eum;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v0

    check-cast v0, LX/2do;

    iput-object v3, v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->a:LX/EuP;

    iput-object v4, v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->b:LX/Exc;

    iput-object v5, v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->c:LX/Euo;

    iput-object p1, v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->d:LX/Eum;

    iput-object v0, v2, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->e:LX/2do;

    .line 2184598
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->a:LX/EuP;

    invoke-virtual {v0}, LX/EuP;->a()LX/95R;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/95R;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->f:LX/95R;

    .line 2184599
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->c:LX/Euo;

    invoke-virtual {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->a()Ljava/lang/String;

    move-result-object v1

    const v2, 0x2f000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FriendCenterSuggestionsLCTabTTI"

    invoke-virtual {v0, v1, v2, v3}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    .line 2184600
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->i:Z

    if-nez v0, :cond_0

    .line 2184601
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Eun;->a(Z)V

    .line 2184602
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->i:Z

    .line 2184603
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->e:LX/2do;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->m:LX/2hO;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2184604
    return-void
.end method

.method public final c()LX/1OX;
    .locals 1

    .prologue
    .line 2184605
    new-instance v0, LX/ExK;

    invoke-direct {v0, p0}, LX/ExK;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;)V

    return-object v0
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2184562
    new-instance v0, LX/ExJ;

    invoke-direct {v0, p0}, LX/ExJ;-><init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;)V

    return-object v0
.end method

.method public final e()LX/BdI;
    .locals 2

    .prologue
    .line 2184563
    new-instance v0, LX/ExL;

    invoke-direct {v0}, LX/ExL;-><init>()V

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2184564
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    move-object v0, v0

    .line 2184565
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5K7;->a(Z)V

    .line 2184566
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2184567
    const/4 v0, 0x0

    return v0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2184568
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6ae7b022

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184569
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->j:Z

    if-eqz v1, :cond_0

    .line 2184570
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->d:LX/Eum;

    iget v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->l:I

    invoke-virtual {v1, v2}, LX/Eum;->d(I)V

    .line 2184571
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->e:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->m:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2184572
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->f:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2184573
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->f:LX/95R;

    .line 2184574
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onDestroy()V

    .line 2184575
    const/16 v1, 0x2b

    const v2, -0x4e9f2a75

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xc3a0cde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184576
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->k:Z

    .line 2184577
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onPause()V

    .line 2184578
    const/16 v1, 0x2b

    const v2, 0x6c705f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x691a6380

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184579
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onResume()V

    .line 2184580
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->k:Z

    .line 2184581
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2184582
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->l()V

    .line 2184583
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3554185d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2184584
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 2184585
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->setUserVisibleHint(Z)V

    .line 2184586
    if-eqz p1, :cond_0

    .line 2184587
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->j:Z

    .line 2184588
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->l()V

    .line 2184589
    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 2184590
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    if-eqz v0, :cond_1

    .line 2184591
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Eun;->a(Z)V

    .line 2184592
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->i:Z

    .line 2184593
    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    if-eqz v0, :cond_2

    .line 2184594
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;->g:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2184595
    :cond_2
    return-void
.end method
