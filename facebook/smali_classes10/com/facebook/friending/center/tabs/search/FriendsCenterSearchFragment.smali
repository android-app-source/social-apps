.class public Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/Eum;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final b:LX/2Ip;

.field public c:LX/Eun;

.field public d:Z

.field private e:Z

.field private f:Z

.field public g:Z

.field public h:Lcom/facebook/ui/search/SearchEditText;

.field public i:Lcom/facebook/widget/listview/BetterListView;

.field public j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/ImageButton;

.field public m:Landroid/widget/ImageView;

.field public n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/text/TextWatcher;

.field public p:Ljava/lang/String;

.field public q:J

.field public r:I

.field public s:LX/Exw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eub;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/Euo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public z:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2184058
    const-class v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2184059
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2184060
    new-instance v0, LX/Ewu;

    invoke-direct {v0, p0}, LX/Ewu;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->b:LX/2Ip;

    .line 2184061
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->e:Z

    .line 2184062
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->f:Z

    .line 2184063
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->g:Z

    .line 2184064
    iput v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->r:I

    .line 2184065
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184066
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    .line 2184067
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184068
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->w:LX/0Ot;

    .line 2184069
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184070
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->y:LX/0Ot;

    .line 2184071
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184072
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->A:LX/0Ot;

    return-void
.end method

.method public static c(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 2

    .prologue
    .line 2184073
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->t:LX/1Ck;

    const-string v1, "SEARCH"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2184074
    :cond_0
    :goto_0
    return-void

    .line 2184075
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    invoke-virtual {v0}, LX/Eub;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2184076
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d()V

    goto :goto_0

    .line 2184077
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2184078
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2184079
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2184080
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2184081
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    const v1, 0x7f0818aa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2184082
    goto :goto_0
.end method

.method public static d(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2184083
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->z:LX/0wM;

    const v1, -0x808081

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2184113
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Exj;->a(Z)V

    .line 2184114
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->t:LX/1Ck;

    const-string v1, "SEARCH"

    new-instance v2, LX/Eww;

    invoke-direct {v2, p0}, LX/Eww;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    new-instance v3, LX/Ewx;

    invoke-direct {v3, p0}, LX/Ewx;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2184115
    return-void
.end method

.method public static l(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 3

    .prologue
    .line 2184084
    iget v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->r:I

    .line 2184085
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2184086
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2184087
    :cond_0
    :goto_0
    return-void

    .line 2184088
    :cond_1
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2184089
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2184090
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2184091
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    .line 2184092
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    .line 2184093
    invoke-static {}, LX/Eub;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eub;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2184094
    iget v1, v0, LX/Eub;->e:I

    if-lez v1, :cond_2

    .line 2184095
    iget-object v1, v0, LX/Eub;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    invoke-virtual {v1}, LX/1My;->b()V

    .line 2184096
    const/4 v1, 0x0

    iput v1, v0, LX/Eub;->e:I

    .line 2184097
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->t:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2184098
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2184099
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    .line 2184100
    iget-object v1, v0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2184101
    const v1, 0x7b66933c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2184102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d:Z

    .line 2184103
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c:LX/Eun;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Eun;->a(Z)V

    .line 2184104
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d()V

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 2184105
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->f:Z

    if-nez v0, :cond_1

    .line 2184106
    :cond_0
    :goto_0
    return-void

    .line 2184107
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2184108
    if-eqz v0, :cond_0

    .line 2184109
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2184110
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2184111
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2184112
    sget-object v0, LX/5P0;->SEARCH:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2184049
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2184050
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-static {v0}, LX/Exw;->b(LX/0QB;)LX/Exw;

    move-result-object v3

    check-cast v3, LX/Exw;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    const/16 v5, 0x2238

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v6

    check-cast v6, LX/2do;

    const/16 v7, 0x2eb

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, LX/Euo;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Euo;

    const/16 v9, 0x2db

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    const/16 v11, 0x271

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object p1

    check-cast p1, LX/Eum;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    iput-object v4, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->t:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->v:LX/2do;

    iput-object v7, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->w:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->x:LX/Euo;

    iput-object v9, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->y:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->z:LX/0wM;

    iput-object v11, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->A:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->B:LX/Eum;

    iput-object v0, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->C:LX/0ad;

    .line 2184051
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->C:LX/0ad;

    sget-short v1, LX/2ez;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->g:Z

    .line 2184052
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2184053
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    .line 2184054
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->x:LX/Euo;

    invoke-virtual {p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->a()Ljava/lang/String;

    move-result-object v1

    const v2, 0x2f000a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FriendCenterSearchTabTTI"

    invoke-virtual {v0, v1, v2, v3}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c:LX/Eun;

    .line 2184055
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2184056
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 2184057
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2184048
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2184047
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2iI;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v0, v1}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5bf86554

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184046
    const v1, 0x7f03070d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4c0d4821

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x203d5347

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2184031
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->e:Z

    if-eqz v0, :cond_1

    .line 2184032
    iget v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->r:I

    if-lez v0, :cond_0

    .line 2184033
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->B:LX/Eum;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    .line 2184034
    iget v3, v2, LX/Exj;->e:I

    move v2, v3

    .line 2184035
    iget-object v3, v0, LX/Eum;->a:LX/0Zb;

    sget-object v4, LX/Eul;->FRIENDS_CENTER_SEARCH_IMPRESSION:LX/Eul;

    invoke-static {v0, v4}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "number_of_results_user_saw_per_search"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2184036
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    const/4 v2, 0x0

    .line 2184037
    iput v2, v0, LX/Exj;->e:I

    .line 2184038
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->B:LX/Eum;

    iget v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->r:I

    .line 2184039
    iget-object v3, v0, LX/Eum;->a:LX/0Zb;

    sget-object v4, LX/Eul;->FRIENDS_CENTER_TOTAL_SEARCHES:LX/Eul;

    invoke-static {v0, v4}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "number_of_searches"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2184040
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->g:Z

    if-eqz v0, :cond_2

    .line 2184041
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    .line 2184042
    iget-object v2, v0, LX/Eub;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2184043
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2184044
    const v0, -0x5e4adc10

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2184045
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->v:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->b:LX/2Ip;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x8a3976b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184018
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->t:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2184019
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2184020
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2184021
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    .line 2184022
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2184023
    iput-object v2, v1, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2184024
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2184025
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2184026
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    .line 2184027
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    .line 2184028
    iput-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->m:Landroid/widget/ImageView;

    .line 2184029
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2184030
    const/16 v1, 0x2b

    const v2, -0x2b9ad8e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x18a79cb7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184012
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->o:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2184013
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2184014
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2184015
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->f:Z

    .line 2184016
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2184017
    const/16 v1, 0x2b

    const v2, 0x1e5fdc81

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1bf5aa14

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2184002
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2184003
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->f:Z

    .line 2184004
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2184005
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s()V

    .line 2184006
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v1}, LX/Exw;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 2184007
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2184008
    :cond_1
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->o:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2184009
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->g:Z

    if-nez v1, :cond_2

    .line 2184010
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->v:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->b:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2184011
    :cond_2
    const/16 v1, 0x2b

    const v2, -0x6d327f46

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2183981
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2183982
    const v0, 0x7f0d12d1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2183983
    const v0, 0x7f0d12d3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    .line 2183984
    const v0, 0x7f0d12c9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2183985
    const v0, 0x7f0d12ca

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    .line 2183986
    const v0, 0x7f0d12d2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    .line 2183987
    const v0, 0x7f0d12d0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->m:Landroid/widget/ImageView;

    .line 2183988
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2183989
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2183990
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->q:J

    .line 2183991
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2183992
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2183993
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->m:Landroid/widget/ImageView;

    const v1, 0x7f02091b

    invoke-static {p0, v1}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2183994
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    const v1, 0x7f020818

    invoke-static {p0, v1}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2183995
    new-instance v0, LX/Ex1;

    invoke-direct {v0, p0}, LX/Ex1;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->o:Landroid/text/TextWatcher;

    .line 2183996
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    new-instance v1, LX/Ex2;

    invoke-direct {v1, p0}, LX/Ex2;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2183997
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/Ewy;

    invoke-direct {v1, p0}, LX/Ewy;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    .line 2183998
    iput-object v1, v0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 2183999
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/Ewz;

    invoke-direct {v1, p0}, LX/Ewz;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2184000
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/Ex0;

    invoke-direct {v1, p0}, LX/Ex0;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2184001
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 2183968
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2183969
    if-eqz p1, :cond_0

    .line 2183970
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s()V

    .line 2183971
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    if-nez v0, :cond_2

    .line 2183972
    :cond_1
    :goto_0
    return-void

    .line 2183973
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v0}, LX/Exw;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 2183974
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2183975
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->e:Z

    .line 2183976
    :goto_1
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2183977
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment$2;

    invoke-direct {v1, p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment$2;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2183978
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2183979
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2183980
    goto :goto_1
.end method
