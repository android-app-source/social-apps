.class public Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fv;


# instance fields
.field public a:Z

.field public b:LX/Evq;

.field private c:Z

.field private d:Z

.field private e:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EuP;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/Etb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/Evr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/Evv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/Evy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2182420
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    .line 2182421
    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->a:Z

    .line 2182422
    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->c:Z

    .line 2182423
    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->d:Z

    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2182492
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->d:Z

    if-nez v0, :cond_1

    .line 2182493
    :cond_0
    :goto_0
    return-void

    .line 2182494
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2182495
    if-eqz v0, :cond_0

    .line 2182496
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2182497
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2182498
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/Runnable;)LX/1X1;
    .locals 2

    .prologue
    .line 2182491
    invoke-static {p1}, LX/BdN;->c(LX/1De;)LX/BdL;

    move-result-object v0

    const v1, 0x7f0818ab

    invoke-virtual {v0, v1}, LX/BdL;->h(I)LX/BdL;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/BdL;->a(Ljava/lang/Runnable;)LX/BdL;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 3

    .prologue
    .line 2182476
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->i:LX/Evv;

    .line 2182477
    new-instance v1, LX/Evt;

    invoke-direct {v1, v0}, LX/Evt;-><init>(LX/Evv;)V

    .line 2182478
    iget-object v2, v0, LX/Evv;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Evs;

    .line 2182479
    if-nez v2, :cond_0

    .line 2182480
    new-instance v2, LX/Evs;

    invoke-direct {v2, v0}, LX/Evs;-><init>(LX/Evv;)V

    .line 2182481
    :cond_0
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2182482
    iput-object v1, v2, LX/Evs;->a:LX/Evt;

    .line 2182483
    iget-object v0, v2, LX/Evs;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2182484
    move-object v1, v2

    .line 2182485
    move-object v0, v1

    .line 2182486
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    .line 2182487
    iget-object v2, v0, LX/Evs;->a:LX/Evt;

    iput-object v1, v2, LX/Evt;->c:LX/95R;

    .line 2182488
    iget-object v2, v0, LX/Evs;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2182489
    move-object v0, v0

    .line 2182490
    invoke-virtual {v0, p2}, LX/Evs;->b(LX/BcQ;)LX/Evs;

    move-result-object v0

    invoke-virtual {v0}, LX/Evs;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Bdl;)LX/Bdl;
    .locals 3

    .prologue
    .line 2182471
    new-instance v0, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/62X;-><init>(II)V

    .line 2182472
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b08a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2182473
    iput v1, v0, LX/62X;->b:I

    .line 2182474
    iput v1, v0, LX/62X;->c:I

    .line 2182475
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/Bdl;)LX/Bdl;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bdl;->a(LX/3x6;)LX/Bdl;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2182470
    sget-object v0, LX/5P0;->FRIENDS:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2182455
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2182456
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;

    const/16 v3, 0x2232

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/Etb;->a(LX/0QB;)LX/Etb;

    move-result-object v4

    check-cast v4, LX/Etb;

    const-class v5, LX/Evr;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Evr;

    invoke-static {v0}, LX/Evv;->a(LX/0QB;)LX/Evv;

    move-result-object p1

    check-cast p1, LX/Evv;

    invoke-static {v0}, LX/Evy;->a(LX/0QB;)LX/Evy;

    move-result-object v0

    check-cast v0, LX/Evy;

    iput-object v3, v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->f:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->g:LX/Etb;

    iput-object v5, v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->h:LX/Evr;

    iput-object p1, v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->i:LX/Evv;

    iput-object v0, v2, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->j:LX/Evy;

    .line 2182457
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->h:LX/Evr;

    invoke-virtual {p0}, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->a()Ljava/lang/String;

    move-result-object v1

    .line 2182458
    new-instance v5, LX/Evq;

    const-class v2, LX/Euo;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Euo;

    const-class v3, LX/193;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/193;

    invoke-static {v0}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object v4

    check-cast v4, LX/Eum;

    invoke-direct {v5, v1, v2, v3, v4}, LX/Evq;-><init>(Ljava/lang/String;LX/Euo;LX/193;LX/Eum;)V

    .line 2182459
    move-object v0, v5

    .line 2182460
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->b:LX/Evq;

    .line 2182461
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->j:LX/Evy;

    .line 2182462
    iget-object v1, v0, LX/Evy;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2182463
    iget-object v1, v0, LX/Evy;->a:LX/0ad;

    sget-short v2, LX/2hr;->H:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/Evy;->b:Ljava/lang/Boolean;

    .line 2182464
    :cond_0
    iget-object v1, v0, LX/Evy;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 2182465
    if-nez v0, :cond_1

    .line 2182466
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->g:LX/Etb;

    sget-object v1, LX/Etb;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/95R;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    .line 2182467
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    if-nez v0, :cond_1

    .line 2182468
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuP;

    invoke-virtual {v0}, LX/EuP;->c()LX/95R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    .line 2182469
    :cond_1
    return-void
.end method

.method public final c()LX/1OX;
    .locals 1

    .prologue
    .line 2182454
    new-instance v0, LX/Evo;

    invoke-direct {v0, p0}, LX/Evo;-><init>(Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;)V

    return-object v0
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2182453
    new-instance v0, LX/Evn;

    invoke-direct {v0, p0}, LX/Evn;-><init>(Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;)V

    return-object v0
.end method

.method public final e()LX/BdI;
    .locals 1

    .prologue
    .line 2182452
    new-instance v0, LX/Evp;

    invoke-direct {v0, p0}, LX/Evp;-><init>(Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;)V

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2182449
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    move-object v0, v0

    .line 2182450
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5K7;->a(Z)V

    .line 2182451
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2182448
    const/4 v0, 0x0

    return v0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2182447
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2e2bc7b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182439
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    if-eqz v1, :cond_0

    .line 2182440
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->e:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2182441
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->c:Z

    if-eqz v1, :cond_1

    .line 2182442
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->b:LX/Evq;

    iget v2, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->k:I

    .line 2182443
    iget-object v4, v1, LX/Evq;->e:LX/Eum;

    .line 2182444
    iget-object v5, v4, LX/Eum;->a:LX/0Zb;

    sget-object v6, LX/Eul;->FRIENDS_CENTER_FRIENDS_TAB_IMPRESSION:LX/Eul;

    invoke-static {v4, v6}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v1, "number_of_friends_user_saw"

    invoke-virtual {v6, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2182445
    :cond_1
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onDestroy()V

    .line 2182446
    const/16 v1, 0x2b

    const v2, -0x35f8166e    # -2226788.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x76299df0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182434
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->d:Z

    .line 2182435
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->b:LX/Evq;

    .line 2182436
    iget-object v2, v1, LX/Evq;->a:LX/195;

    invoke-virtual {v2}, LX/195;->b()V

    .line 2182437
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onPause()V

    .line 2182438
    const/16 v1, 0x2b

    const v2, -0x1f811403

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42c7f8e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182429
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onResume()V

    .line 2182430
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->d:Z

    .line 2182431
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2182432
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->n()V

    .line 2182433
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x4d5d9c69

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2182424
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->setUserVisibleHint(Z)V

    .line 2182425
    if-eqz p1, :cond_0

    .line 2182426
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->n()V

    .line 2182427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;->c:Z

    .line 2182428
    :cond_0
    return-void
.end method
