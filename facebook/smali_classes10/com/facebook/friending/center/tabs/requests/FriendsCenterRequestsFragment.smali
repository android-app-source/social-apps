.class public Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field private static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/Eun;

.field public B:Z

.field private C:Z

.field private D:Z

.field public E:Ljava/lang/String;

.field private F:Z

.field public G:Z

.field public H:Z

.field private I:I

.field private J:Z

.field public K:Z

.field public L:Z

.field private M:Z

.field private N:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public O:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private P:LX/1vq;

.field public Q:Z

.field private R:Z

.field private S:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation
.end field

.field private T:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private U:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private V:LX/EuP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private W:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private X:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private Y:LX/2hS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private Z:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aa:LX/2dl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ab:LX/2hW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ac:LX/Eum;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ad:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field public ae:LX/0xB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private af:LX/Euo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ag:LX/2hd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ah:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ai:LX/EwG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aj:LX/EuY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ak:LX/Eui;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public al:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private am:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private an:LX/1Kt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final k:LX/2Ip;

.field private final l:LX/2hO;

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewo;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewi;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/2hs;

.field private q:LX/0xA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

.field private s:LX/0g8;

.field public t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public u:Landroid/widget/TextView;

.field public v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field public w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/83X;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewi;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewo;",
            ">;"
        }
    .end annotation
.end field

.field public z:LX/Ewf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2183595
    const-class v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    sput-object v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->i:Ljava/lang/Class;

    .line 2183596
    const-class v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2183535
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 2183536
    new-instance v0, LX/EwR;

    invoke-direct {v0, p0}, LX/EwR;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->k:LX/2Ip;

    .line 2183537
    new-instance v0, LX/EwT;

    invoke-direct {v0, p0}, LX/EwT;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->l:LX/2hO;

    .line 2183538
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    .line 2183539
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n:Ljava/util/List;

    .line 2183540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o:Ljava/util/List;

    .line 2183541
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    .line 2183542
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B:Z

    .line 2183543
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->C:Z

    .line 2183544
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->D:Z

    .line 2183545
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->F:Z

    .line 2183546
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->G:Z

    .line 2183547
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    .line 2183548
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    .line 2183549
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->J:Z

    .line 2183550
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->K:Z

    .line 2183551
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->L:Z

    .line 2183552
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->M:Z

    .line 2183553
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183554
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->T:LX/0Ot;

    .line 2183555
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183556
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->U:LX/0Ot;

    .line 2183557
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183558
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->W:LX/0Ot;

    .line 2183559
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183560
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->X:LX/0Ot;

    .line 2183561
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183562
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ad:LX/0Ot;

    .line 2183563
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2183564
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->am:LX/0Ot;

    .line 2183565
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 2183566
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->F:Z

    if-nez v0, :cond_1

    .line 2183567
    :cond_0
    :goto_0
    return-void

    .line 2183568
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2183569
    if-eqz v0, :cond_0

    .line 2183570
    const v1, 0x7f080faa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2183571
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2183572
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/0Ot;LX/0Ot;LX/EuP;LX/0Ot;LX/0Ot;LX/2hS;LX/2do;LX/2dl;LX/2hW;LX/Eum;LX/0Ot;LX/0xB;LX/Euo;LX/2hd;LX/0ad;LX/EwG;LX/EuY;LX/Eui;LX/1Ck;LX/0Ot;LX/1Kt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/EuP;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/2hS;",
            "LX/2do;",
            "LX/2dl;",
            "LX/2hW;",
            "LX/Eum;",
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;",
            "LX/0xB;",
            "LX/Euo;",
            "LX/2hd;",
            "LX/0ad;",
            "LX/EwG;",
            "LX/EuY;",
            "LX/Eui;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2183573
    iput-object p1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->T:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->U:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->V:LX/EuP;

    iput-object p4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->W:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->X:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Y:LX/2hS;

    iput-object p7, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iput-object p8, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aa:LX/2dl;

    iput-object p9, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ab:LX/2hW;

    iput-object p10, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ac:LX/Eum;

    iput-object p11, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ad:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ae:LX/0xB;

    iput-object p13, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->af:LX/Euo;

    iput-object p14, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ag:LX/2hd;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ah:LX/0ad;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->am:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->an:LX/1Kt;

    return-void
.end method

.method private static a(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/2kM;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2183574
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2183575
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2183576
    invoke-interface {p1, v1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183577
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2183578
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Ewo;

    move-result-object v0

    .line 2183579
    iget-object v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183580
    iget-object v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183581
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2183582
    :cond_0
    iput-object p1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->S:LX/2kM;

    .line 2183583
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 24

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const/16 v3, 0x245

    move-object/from16 v0, v23

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2e3

    move-object/from16 v0, v23

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {v23 .. v23}, LX/EuP;->a(LX/0QB;)LX/EuP;

    move-result-object v5

    check-cast v5, LX/EuP;

    const/16 v6, 0x2eb

    move-object/from16 v0, v23

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xa71

    move-object/from16 v0, v23

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, LX/2hS;

    move-object/from16 v0, v23

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2hS;

    invoke-static/range {v23 .. v23}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v9

    check-cast v9, LX/2do;

    invoke-static/range {v23 .. v23}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v10

    check-cast v10, LX/2dl;

    invoke-static/range {v23 .. v23}, LX/2hW;->a(LX/0QB;)LX/2hW;

    move-result-object v11

    check-cast v11, LX/2hW;

    invoke-static/range {v23 .. v23}, LX/Eum;->a(LX/0QB;)LX/Eum;

    move-result-object v12

    check-cast v12, LX/Eum;

    const/16 v13, 0xb0b

    move-object/from16 v0, v23

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v23 .. v23}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v14

    check-cast v14, LX/0xB;

    const-class v15, LX/Euo;

    move-object/from16 v0, v23

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/Euo;

    invoke-static/range {v23 .. v23}, LX/2hd;->a(LX/0QB;)LX/2hd;

    move-result-object v16

    check-cast v16, LX/2hd;

    invoke-static/range {v23 .. v23}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {v23 .. v23}, LX/EwG;->a(LX/0QB;)LX/EwG;

    move-result-object v18

    check-cast v18, LX/EwG;

    invoke-static/range {v23 .. v23}, LX/EuY;->a(LX/0QB;)LX/EuY;

    move-result-object v19

    check-cast v19, LX/EuY;

    invoke-static/range {v23 .. v23}, LX/Eui;->a(LX/0QB;)LX/Eui;

    move-result-object v20

    check-cast v20, LX/Eui;

    invoke-static/range {v23 .. v23}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v21

    check-cast v21, LX/1Ck;

    const/16 v22, 0x271

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v23 .. v23}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v23

    check-cast v23, LX/1Kt;

    invoke-static/range {v2 .. v23}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/0Ot;LX/0Ot;LX/EuP;LX/0Ot;LX/0Ot;LX/2hS;LX/2do;LX/2dl;LX/2hW;LX/Eum;LX/0Ot;LX/0xB;LX/Euo;LX/2hd;LX/0ad;LX/EwG;LX/EuY;LX/Eui;LX/1Ck;LX/0Ot;LX/1Kt;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2183584
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->g()V

    .line 2183585
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183586
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2183587
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f080039

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EwM;

    invoke-direct {v2, p0}, LX/EwM;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2183588
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2183589
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2183590
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    .line 2183591
    iget-object v1, v0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2183592
    const v1, 0x74f44927

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2183593
    :cond_0
    sget-object v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->i:Ljava/lang/Class;

    const-string v1, "Failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2183594
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/0Px;LX/2kM;LX/2kM;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 2183597
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->S:LX/2kM;

    if-ne v1, p2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->R:Z

    if-eqz v1, :cond_1

    .line 2183598
    :cond_0
    invoke-static {p0, p3}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/2kM;)V

    move v2, v5

    .line 2183599
    :goto_0
    return v2

    .line 2183600
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v6, v0

    move v2, v0

    :goto_1
    if-ge v6, v7, :cond_6

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 2183601
    sget-object v1, LX/EwQ;->a:[I

    iget-object v3, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v3}, LX/3Ca;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v4, v2

    .line 2183602
    :cond_2
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v2, v4

    goto :goto_1

    .line 2183603
    :pswitch_0
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    move v2, v1

    .line 2183604
    :goto_3
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    iget v3, v0, LX/3CY;->b:I

    add-int/2addr v1, v3

    if-ge v2, v1, :cond_3

    .line 2183605
    invoke-interface {p3, v2}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183606
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2183607
    invoke-static {v1, v10}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Ewo;

    move-result-object v1

    .line 2183608
    iget-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v3, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2183609
    iget-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183610
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_3
    move v4, v5

    .line 2183611
    goto :goto_2

    .line 2183612
    :pswitch_1
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v1

    iget v2, v0, LX/3CY;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    .line 2183613
    :goto_4
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v1

    if-lt v2, v1, :cond_4

    .line 2183614
    iget-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ewo;

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183615
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2183616
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_4

    :cond_4
    move v4, v5

    .line 2183617
    goto :goto_2

    .line 2183618
    :pswitch_2
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    move v3, v1

    move v4, v2

    .line 2183619
    :goto_5
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    iget v2, v0, LX/3CY;->b:I

    add-int/2addr v1, v2

    if-ge v3, v1, :cond_2

    .line 2183620
    invoke-interface {p3, v3}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183621
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2183622
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ewo;

    .line 2183623
    if-nez v2, :cond_5

    .line 2183624
    invoke-static {v1, v10}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Ewo;

    move-result-object v1

    .line 2183625
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v2, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2183626
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v5

    .line 2183627
    :goto_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto :goto_5

    .line 2183628
    :cond_5
    invoke-virtual {v2}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    if-eq v8, v9, :cond_7

    .line 2183629
    invoke-virtual {v2}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2183630
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2183631
    invoke-virtual {v2, v1}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2183632
    invoke-virtual {v2, v4}, LX/Eus;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move v2, v5

    .line 2183633
    goto :goto_6

    .line 2183634
    :cond_6
    iput-object p3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->S:LX/2kM;

    goto/16 :goto_0

    :cond_7
    move v2, v4

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Ewo;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2183699
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2183700
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    .line 2183701
    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2183702
    new-instance v0, LX/Ewn;

    invoke-direct {v0}, LX/Ewn;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 2183703
    iput-object v7, v0, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2183704
    move-object v0, v0

    .line 2183705
    check-cast v0, LX/Ewn;

    .line 2183706
    iput-wide v2, v0, LX/Euq;->a:J

    .line 2183707
    move-object v0, v0

    .line 2183708
    check-cast v0, LX/Ewn;

    if-eqz v6, :cond_0

    invoke-virtual {v5, v6, v1}, LX/15i;->j(II)I

    move-result v1

    .line 2183709
    :cond_0
    iput v1, v0, LX/Euq;->e:I

    .line 2183710
    move-object v0, v0

    .line 2183711
    check-cast v0, LX/Ewn;

    invoke-virtual {p0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v1

    .line 2183712
    iput-object v1, v0, LX/Euq;->d:Ljava/lang/String;

    .line 2183713
    move-object v0, v0

    .line 2183714
    check-cast v0, LX/Ewn;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2183715
    :goto_0
    iput-object v1, v0, LX/Euq;->c:Ljava/lang/String;

    .line 2183716
    move-object v0, v0

    .line 2183717
    check-cast v0, LX/Ewn;

    sget-object v1, LX/2h7;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

    .line 2183718
    iput-object v1, v0, LX/Euq;->f:LX/2h7;

    .line 2183719
    move-object v0, v0

    .line 2183720
    check-cast v0, LX/Ewn;

    .line 2183721
    iput-object p1, v0, LX/Euq;->g:Ljava/lang/String;

    .line 2183722
    move-object v0, v0

    .line 2183723
    check-cast v0, LX/Ewn;

    invoke-virtual {v0}, LX/Ewn;->a()LX/Ewo;

    move-result-object v0

    .line 2183724
    return-object v0

    .line 2183725
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2183726
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 5

    .prologue
    .line 2183635
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2183636
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    .line 2183637
    iget v2, v0, LX/EwG;->o:I

    move v2, v2

    .line 2183638
    sub-int v0, v2, v1

    add-int/lit8 v0, v0, -0x1

    .line 2183639
    iget-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ac:LX/Eum;

    if-le v1, v2, :cond_0

    add-int/lit8 v1, v2, -0x1

    :cond_0
    if-lez v0, :cond_1

    .line 2183640
    :goto_0
    iget-object v2, v3, LX/Eum;->a:LX/0Zb;

    sget-object v4, LX/Eul;->FRIENDS_CENTER_REQUESTS_TAB_IMPRESSION:LX/Eul;

    invoke-static {v3, v4}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p0, "friend_requests_seen"

    invoke-virtual {v4, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p0, "pymk_seen"

    invoke-virtual {v4, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2183641
    return-void

    .line 2183642
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 4

    .prologue
    .line 2183643
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2183644
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aa:LX/2dl;

    const-string v1, "FC_REQUESTS_QUERY"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 2183645
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183646
    :goto_0
    return-void

    .line 2183647
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    const-string v1, "CLEAR_FRIEND_REQUEST_CACHE_TASK"

    new-instance v2, LX/EwW;

    invoke-direct {v2, p0}, LX/EwW;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    new-instance v3, LX/EwX;

    invoke-direct {v3, p0}, LX/EwX;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static m$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 4

    .prologue
    .line 2183648
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2183649
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Q:Z

    .line 2183650
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aa:LX/2dl;

    const-string v1, "FC_REQUESTS_QUERY"

    const-string v2, "FC_SUGGESTIONS_QUERY"

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 2183651
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183652
    :goto_0
    return-void

    .line 2183653
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    const-string v1, "CLEAR_GRAPHQL_CACHE_TASK"

    new-instance v2, LX/EwY;

    invoke-direct {v2, p0}, LX/EwY;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    new-instance v3, LX/EwZ;

    invoke-direct {v3, p0}, LX/EwZ;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static n(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 3

    .prologue
    .line 2183654
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2183655
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    .line 2183656
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    .line 2183657
    iget-object v2, v0, LX/EwG;->e:LX/Exs;

    .line 2183658
    iput-object v1, v2, LX/Exs;->s:Ljava/lang/String;

    .line 2183659
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v0, :cond_0

    .line 2183660
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-virtual {v0}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d()V

    .line 2183661
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    invoke-virtual {v0}, LX/EuY;->b()V

    .line 2183662
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2183663
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2183664
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o()V

    .line 2183665
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q()V

    .line 2183666
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 2183667
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2183668
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->b()V

    .line 2183669
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2183670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->R:Z

    .line 2183671
    return-void
.end method

.method public static p$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x14

    const/4 v1, 0x0

    .line 2183672
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2183673
    :cond_0
    :goto_0
    return-void

    .line 2183674
    :cond_1
    iget v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_2

    const/4 v0, 0x1

    .line 2183675
    :goto_1
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    invoke-virtual {v2}, LX/EuY;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    .line 2183676
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2183677
    goto :goto_1

    .line 2183678
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v0, :cond_6

    .line 2183679
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    .line 2183680
    iget-object v2, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v2

    .line 2183681
    iget-boolean v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->R:Z

    if-eqz v2, :cond_4

    .line 2183682
    invoke-static {p0, v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/2kM;)V

    .line 2183683
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->R:Z

    .line 2183684
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183685
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Q:Z

    if-eqz v2, :cond_5

    .line 2183686
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Q:Z

    .line 2183687
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    invoke-virtual {v0, v3, v4}, LX/2kW;->c(ILjava/lang/Object;)V

    goto :goto_0

    .line 2183688
    :cond_5
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2183689
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 2183690
    if-eqz v0, :cond_0

    .line 2183691
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    invoke-virtual {v0, v3, v4}, LX/2kW;->b(ILjava/lang/Object;)V

    goto :goto_0

    .line 2183692
    :cond_6
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183693
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EwG;->a(Z)V

    .line 2183694
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    const-string v1, "FETCH_PYMK"

    new-instance v2, LX/EwJ;

    invoke-direct {v2, p0}, LX/EwJ;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    new-instance v3, LX/EwK;

    invoke-direct {v3, p0}, LX/EwK;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2183695
    goto :goto_0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 2183696
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EwG;->a(Z)V

    .line 2183697
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    const-string v1, "FETCH_REQUESTS"

    new-instance v2, LX/Ewb;

    invoke-direct {v2, p0}, LX/Ewb;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    new-instance v3, LX/EwH;

    invoke-direct {v3, p0}, LX/EwH;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2183698
    return-void
.end method

.method public static s(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 3

    .prologue
    .line 2183524
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->L:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewi;

    .line 2183525
    iget-boolean v1, v0, LX/Eut;->g:Z

    move v0, v1

    .line 2183526
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ae:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    if-nez v0, :cond_1

    .line 2183527
    :cond_0
    :goto_0
    return-void

    .line 2183528
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->L:Z

    .line 2183529
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->am:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->X:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dj;

    invoke-virtual {v1}, LX/2dj;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/EwL;

    invoke-direct {v2, p0}, LX/EwL;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 2183530
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2183531
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2183532
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2183533
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2183534
    return-void
.end method

.method public static v(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 2

    .prologue
    .line 2183325
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2183326
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2183327
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2183328
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    const v1, 0x7f080f8d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2183329
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    new-instance v1, LX/EwN;

    invoke-direct {v1, p0}, LX/EwN;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2183330
    return-void
.end method

.method public static w(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 1

    .prologue
    .line 2183331
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    invoke-virtual {v0}, LX/EuY;->b()V

    .line 2183332
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->b()V

    .line 2183333
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t()V

    .line 2183334
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->p$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183335
    return-void
.end method

.method public static y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 8

    .prologue
    .line 2183336
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->a()Z

    move-result v3

    .line 2183337
    :goto_0
    iget-object v7, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->z:LX/Ewf;

    iget-boolean v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->K:Z

    iget-boolean v5, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->J:Z

    iget v6, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    invoke-static/range {v0 .. v6}, LX/Ewd;->a(Ljava/util/List;Ljava/util/List;LX/Ewf;ZZZI)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/EwG;->a(Ljava/util/List;)V

    .line 2183338
    return-void

    .line 2183339
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    .line 2183340
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2183341
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2183342
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v3, v1

    .line 2183343
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2183344
    sget-object v0, LX/5P0;->REQUESTS:LX/5P0;

    iget-object v0, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 10

    .prologue
    .line 2183345
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    invoke-virtual {v0, p2}, LX/EwG;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    .line 2183346
    invoke-interface {v0}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v1

    sget-object v2, LX/Ewp;->SEE_ALL_FRIEND_REQUESTS:LX/Ewp;

    if-ne v1, v2, :cond_1

    .line 2183347
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    .line 2183348
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewo;

    .line 2183349
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2183350
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o()V

    .line 2183351
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183352
    :goto_1
    return-void

    .line 2183353
    :cond_1
    check-cast v0, LX/2lq;

    .line 2183354
    instance-of v1, v0, LX/Ewo;

    if-eqz v1, :cond_2

    .line 2183355
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ag:LX/2hd;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    move-object v3, v0

    check-cast v3, LX/Ewo;

    .line 2183356
    iget-object v4, v3, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v4

    .line 2183357
    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v4

    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->d(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2183358
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2183359
    new-instance v1, LX/5vg;

    invoke-direct {v1}, LX/5vg;-><init>()V

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2183360
    iput-object v3, v1, LX/5vg;->d:Ljava/lang/String;

    .line 2183361
    move-object v1, v1

    .line 2183362
    new-instance v3, LX/4aM;

    invoke-direct {v3}, LX/4aM;-><init>()V

    invoke-interface {v0}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v4

    .line 2183363
    iput-object v4, v3, LX/4aM;->b:Ljava/lang/String;

    .line 2183364
    move-object v3, v3

    .line 2183365
    invoke-virtual {v3}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    .line 2183366
    iput-object v3, v1, LX/5vg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2183367
    move-object v1, v1

    .line 2183368
    invoke-interface {v0}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v3

    .line 2183369
    iput-object v3, v1, LX/5vg;->e:Ljava/lang/String;

    .line 2183370
    move-object v1, v1

    .line 2183371
    invoke-interface {v0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 2183372
    iput-object v3, v1, LX/5vg;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2183373
    move-object v1, v1

    .line 2183374
    invoke-virtual {v1}, LX/5vg;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    move-result-object v1

    invoke-static {v2, v1}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 2183375
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->W:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/0ax;->bE:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2183376
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->g(I)V

    .line 2183377
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2183378
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2183379
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2a

    const v4, 0x23f11ac0

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2183380
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2183381
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2183382
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ah:LX/0ad;

    sget v4, LX/2hr;->j:I

    const/4 v5, -0x1

    invoke-interface {v0, v4, v5}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->I:I

    .line 2183383
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ah:LX/0ad;

    sget-short v4, LX/2ez;->p:S

    invoke-interface {v0, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    .line 2183384
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->V:LX/EuP;

    invoke-virtual {v0}, LX/EuP;->a()LX/95R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    .line 2183385
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    if-eqz v0, :cond_0

    .line 2183386
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    invoke-virtual {v0}, LX/95R;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    .line 2183387
    new-instance v0, LX/Ewc;

    invoke-direct {v0, p0}, LX/Ewc;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->P:LX/1vq;

    .line 2183388
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    iget-object v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->P:LX/1vq;

    invoke-virtual {v0, v4}, LX/2kW;->a(LX/1vq;)V

    .line 2183389
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2183390
    instance-of v4, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v4, :cond_1

    .line 2183391
    check-cast v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2183392
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    iget-object v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2183393
    iput-object v4, v0, LX/EwG;->m:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2183394
    iget-object v4, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v0, :cond_3

    move v0, v1

    .line 2183395
    :goto_0
    iget-object v5, v4, LX/EwG;->e:LX/Exs;

    .line 2183396
    iput-boolean v0, v5, LX/Exs;->t:Z

    .line 2183397
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->an:LX/1Kt;

    new-instance v4, LX/EwU;

    invoke-direct {v4, p0}, LX/EwU;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v4}, LX/1Kt;->a(LX/1Ce;)V

    .line 2183398
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->af:LX/Euo;

    invoke-virtual {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a()Ljava/lang/String;

    move-result-object v4

    const v5, 0x2f0009

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "FriendCenterRequestsTabTTI"

    invoke-virtual {v0, v4, v5, v6}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    .line 2183399
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->C:Z

    if-nez v0, :cond_2

    .line 2183400
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    invoke-virtual {v0, v2}, LX/Eun;->a(Z)V

    .line 2183401
    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->C:Z

    .line 2183402
    :cond_2
    const v0, 0x5a80d52e

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    :cond_3
    move v0, v2

    .line 2183403
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x533c029e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2183404
    if-eqz p3, :cond_0

    const-string v1, "ARG_CAN_SHOW_NOTICE_ITEM"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2183405
    const-string v1, "ARG_CAN_SHOW_NOTICE_ITEM"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->G:Z

    .line 2183406
    :cond_0
    const v1, 0x7f03070c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x41daa5ca

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x16c16c58

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2183507
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->D:Z

    if-eqz v1, :cond_0

    .line 2183508
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->i(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183509
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v1, :cond_1

    .line 2183510
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    .line 2183511
    iget-object v2, v1, LX/EuY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2183512
    :goto_0
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v1, :cond_2

    .line 2183513
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->P:LX/1vq;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2183514
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2183515
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->N:LX/95R;

    .line 2183516
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    .line 2183517
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->l:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2183518
    :goto_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 2183519
    const v1, -0x7cf98450

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2183520
    :cond_1
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->k:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0

    .line 2183521
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v1, :cond_3

    .line 2183522
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->c()V

    goto :goto_1

    .line 2183523
    :cond_3
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->l:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x6266ab2d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2183407
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2183408
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    .line 2183409
    iget-object v2, v1, LX/EwG;->e:LX/Exs;

    invoke-virtual {v2}, LX/Exs;->a()V

    .line 2183410
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/2ii;)V

    .line 2183411
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/0fx;)V

    .line 2183412
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->an:LX/1Kt;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2183413
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    .line 2183414
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2183415
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    .line 2183416
    iput-object v3, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2183417
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q:LX/0xA;

    if-eqz v1, :cond_0

    .line 2183418
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ae:LX/0xB;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q:LX/0xA;

    invoke-virtual {v1, v2}, LX/0xB;->b(LX/0xA;)V

    .line 2183419
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroyView()V

    .line 2183420
    const/16 v1, 0x2b

    const v2, 0x3cfd30d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x21feda1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2183421
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->F:Z

    .line 2183422
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onPause()V

    .line 2183423
    const/16 v1, 0x2b

    const v2, 0x4f2f3dae    # 2.94005504E9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xc84126c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2183424
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 2183425
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->F:Z

    .line 2183426
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2183427
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B()V

    .line 2183428
    :cond_0
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183429
    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-nez v1, :cond_1

    .line 2183430
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->k:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2183431
    :cond_1
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-nez v1, :cond_3

    .line 2183432
    :cond_2
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Z:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->l:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2183433
    :cond_3
    const/16 v1, 0x2b

    const v2, -0x5844be7d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2183434
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2183435
    const-string v0, "ARG_CAN_SHOW_NOTICE_ITEM"

    iget-boolean v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->G:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2183436
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2183437
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2183438
    new-instance v1, LX/2iI;

    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    .line 2183439
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2183440
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    .line 2183441
    new-instance v1, LX/EwP;

    invoke-direct {v1, p0}, LX/EwP;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    move-object v1, v1

    .line 2183442
    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 2183443
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->an:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2183444
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->Y:LX/2hS;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->al:LX/1Ck;

    invoke-virtual {v0, v1}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->p:LX/2hs;

    .line 2183445
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0g8;->c(Z)V

    .line 2183446
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s:LX/0g8;

    invoke-interface {v0}, LX/0g8;->k()V

    .line 2183447
    const v0, 0x7f0d12cc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2183448
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    new-instance v1, LX/EwV;

    invoke-direct {v1, p0}, LX/EwV;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    invoke-virtual {v0, v1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2183449
    const v0, 0x7f0d12c9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2183450
    const v0, 0x7f0d12ca

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->u:Landroid/widget/TextView;

    .line 2183451
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    .line 2183452
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    .line 2183453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    .line 2183454
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2183455
    if-eqz v0, :cond_0

    .line 2183456
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2183457
    const-string v1, "attachment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5Oy;->valueOf(Ljava/lang/String;)LX/5Oy;

    move-result-object v1

    .line 2183458
    sget-object v0, LX/5Oy;->REQUEST_ACCEPTED_NOTICE:LX/5Oy;

    if-ne v1, v0, :cond_2

    .line 2183459
    new-instance v0, LX/Ewf;

    .line 2183460
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2183461
    const-string v3, "user_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2183462
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2183463
    const-string v5, "name"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2183464
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2183465
    const-string v6, "profile_pic"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Ewf;-><init>(LX/5Oy;JLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->z:LX/Ewf;

    .line 2183466
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ah:LX/0ad;

    sget-short v1, LX/2hr;->B:S

    invoke-interface {v0, v1, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->J:Z

    .line 2183467
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    .line 2183468
    iput-object p0, v0, LX/EwG;->q:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    .line 2183469
    iput-boolean v7, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->K:Z

    .line 2183470
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ah:LX/0ad;

    sget-short v1, LX/2hr;->w:S

    invoke-interface {v0, v1, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->M:Z

    .line 2183471
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->M:Z

    if-eqz v0, :cond_1

    .line 2183472
    new-instance v0, LX/EwO;

    invoke-direct {v0, p0}, LX/EwO;-><init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    move-object v0, v0

    .line 2183473
    iput-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q:LX/0xA;

    .line 2183474
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ae:LX/0xB;

    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->q:LX/0xA;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/0xA;)V

    .line 2183475
    :cond_1
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->t()V

    .line 2183476
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ae:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    if-lez v0, :cond_3

    .line 2183477
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->l(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183478
    :goto_1
    return-void

    .line 2183479
    :cond_2
    goto :goto_0

    .line 2183480
    :cond_3
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    goto :goto_1
.end method

.method public final setUserVisibleHint(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 2183481
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 2183482
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->setUserVisibleHint(Z)V

    .line 2183483
    if-eqz p1, :cond_0

    .line 2183484
    invoke-direct {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B()V

    .line 2183485
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->D:Z

    .line 2183486
    :cond_0
    if-nez v0, :cond_5

    if-eqz p1, :cond_5

    .line 2183487
    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->C:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    if-eqz v0, :cond_1

    .line 2183488
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Eun;->a(Z)V

    .line 2183489
    iput-boolean v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->C:Z

    .line 2183490
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewo;

    .line 2183491
    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    .line 2183492
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2183493
    iget-object v1, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ag:LX/2hd;

    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    .line 2183494
    iget-object v3, v0, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2183495
    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2183496
    invoke-virtual {v0}, LX/Eus;->p()V

    .line 2183497
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2183498
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2183499
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewi;

    .line 2183500
    iget-object v2, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ab:LX/2hW;

    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    sget-object v3, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    iget-object v6, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3, v6}, LX/2hW;->a(JLX/2hA;Ljava/lang/String;)V

    .line 2183501
    invoke-virtual {v0}, LX/Eus;->p()V

    goto :goto_1

    .line 2183502
    :cond_4
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2183503
    invoke-static {p0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183504
    :cond_5
    if-eqz p1, :cond_6

    iget-boolean v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    if-eqz v0, :cond_6

    .line 2183505
    iget-object v0, p0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2183506
    :cond_6
    return-void
.end method
