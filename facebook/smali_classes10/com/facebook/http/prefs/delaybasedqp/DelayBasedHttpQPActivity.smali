.class public Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2016912
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V
    .locals 0

    .prologue
    .line 2016924
    iput-object p1, p0, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->q:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v0, v1}, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->a(Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2016913
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2016914
    invoke-static {p0, p0}, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2016915
    invoke-virtual {p0}, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "enable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2016916
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2016917
    const-string v1, "enable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2016918
    iget-object v1, p0, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dU;->m:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2016919
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v2, 0x5265c0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 2016920
    if-eqz v0, :cond_0

    const-string v0, "Enabled 2G Empathy for %d minutes"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2016921
    invoke-virtual {p0}, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;->finish()V

    .line 2016922
    return-void

    .line 2016923
    :cond_0
    const-string v0, "Disabled 2G Empathy"

    goto :goto_0
.end method
