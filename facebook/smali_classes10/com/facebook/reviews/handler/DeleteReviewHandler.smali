.class public Lcom/facebook/reviews/handler/DeleteReviewHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0aG;

.field public final b:LX/BNE;

.field public final c:LX/Ch5;

.field public final d:LX/79D;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0kL;


# direct methods
.method public constructor <init>(LX/0aG;LX/BNE;LX/Ch5;LX/79D;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085686
    iput-object p1, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->a:LX/0aG;

    .line 2085687
    iput-object p2, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->b:LX/BNE;

    .line 2085688
    iput-object p3, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->c:LX/Ch5;

    .line 2085689
    iput-object p4, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->d:LX/79D;

    .line 2085690
    iput-object p5, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->e:LX/1Ck;

    .line 2085691
    iput-object p6, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->f:LX/0kL;

    .line 2085692
    return-void
.end method

.method public static a$redex0(Lcom/facebook/reviews/handler/DeleteReviewHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2085693
    iget-object v0, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->d:LX/79D;

    .line 2085694
    const-string v1, "reviews_delete_review_success"

    invoke-static {v0, v1, p2, p1}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085695
    iget-object v0, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->c:LX/Ch5;

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/ChH;->a(Ljava/lang/String;LX/5tj;)LX/ChF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2085696
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2085674
    const v0, 0x7f0814e3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0814ea

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v2

    .line 2085675
    new-instance v0, LX/EAX;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/EAX;-><init>(Lcom/facebook/reviews/handler/DeleteReviewHandler;LX/4BY;Landroid/content/Context;Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;Ljava/lang/String;)V

    .line 2085676
    iget-object v1, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->e:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "delete_review_task_key"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2085677
    iget-object v3, p2, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2085678
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2085679
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2085680
    const-string v6, "deleteReviewParams"

    invoke-virtual {v8, v6, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2085681
    iget-object v6, p0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->a:LX/0aG;

    const-string v7, "delete_page_review"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, 0x94dd8b3

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 2085682
    move-object v3, v6

    .line 2085683
    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2085684
    return-void
.end method
