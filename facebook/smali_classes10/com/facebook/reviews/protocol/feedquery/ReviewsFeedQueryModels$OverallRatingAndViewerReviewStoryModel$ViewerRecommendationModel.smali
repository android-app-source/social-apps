.class public final Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x410551c5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086429
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086428
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2086426
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2086427
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2086420
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086421
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2086422
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2086423
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2086424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2086412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086413
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2086414
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2086415
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2086416
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    .line 2086417
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2086418
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086419
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086410
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2086411
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2086405
    new-instance v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;-><init>()V

    .line 2086406
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2086407
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2086409
    const v0, -0xbe4c9b6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2086408
    const v0, -0x7d2175f

    return v0
.end method
