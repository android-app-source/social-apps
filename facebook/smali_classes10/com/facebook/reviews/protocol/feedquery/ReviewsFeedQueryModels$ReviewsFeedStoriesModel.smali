.class public final Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1e4db7b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086795
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086794
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2086792
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2086793
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2086786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086787
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2086788
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2086789
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2086790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2086778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086779
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2086780
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    .line 2086781
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2086782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;

    .line 2086783
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->e:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    .line 2086784
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086785
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2086777
    new-instance v0, LX/EB8;

    invoke-direct {v0, p1}, LX/EB8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086796
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->e:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->e:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    .line 2086797
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->e:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2086775
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2086776
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2086774
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2086771
    new-instance v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;-><init>()V

    .line 2086772
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2086773
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2086770
    const v0, -0x1c5691f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2086769
    const v0, 0x25d6af

    return v0
.end method
