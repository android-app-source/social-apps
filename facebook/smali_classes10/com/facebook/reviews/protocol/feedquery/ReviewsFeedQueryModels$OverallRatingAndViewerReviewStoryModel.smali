.class public final Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2e38fe7c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086454
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086465
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2086463
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2086464
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2086455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086456
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2086457
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2086458
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2086459
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2086460
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2086461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086462
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2086441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086442
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2086443
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086444
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2086445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    .line 2086446
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086447
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2086448
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    .line 2086449
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2086450
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    .line 2086451
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->f:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    .line 2086452
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086453
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2086440
    new-instance v0, LX/EB6;

    invoke-direct {v0, p1}, LX/EB6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086466
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086467
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2086438
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2086439
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2086437
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2086430
    new-instance v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;-><init>()V

    .line 2086431
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2086432
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2086436
    const v0, 0x286841be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2086435
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086433
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->f:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->f:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    .line 2086434
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->f:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    return-object v0
.end method
