.class public final Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x11530459
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086627
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2086626
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2086624
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2086625
    return-void
.end method

.method private j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086622
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->f:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->f:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086623
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->f:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086620
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2086621
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086618
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->h:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->h:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    .line 2086619
    iget-object v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->h:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2086607
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086608
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2086609
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2086610
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2086611
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2086612
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 2086613
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2086614
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2086615
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2086616
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086617
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2086589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2086590
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2086591
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086592
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2086593
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    .line 2086594
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->f:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2086595
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2086596
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2086597
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2086598
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    .line 2086599
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2086600
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2086601
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    .line 2086602
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2086603
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    .line 2086604
    iput-object v0, v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->h:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    .line 2086605
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2086606
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2086572
    new-instance v0, LX/EB7;

    invoke-direct {v0, p1}, LX/EB7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2086586
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2086587
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->e:Z

    .line 2086588
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2086584
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2086585
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2086583
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2086581
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2086582
    iget-boolean v0, p0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2086578
    new-instance v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;-><init>()V

    .line 2086579
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2086580
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086577
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->j()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086576
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2086575
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->l()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2086574
    const v0, 0x7f0f1e52

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2086573
    const v0, 0x25d6af

    return v0
.end method
