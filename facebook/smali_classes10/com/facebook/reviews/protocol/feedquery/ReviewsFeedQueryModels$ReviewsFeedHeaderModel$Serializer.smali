.class public final Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2086529
    const-class v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    new-instance v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2086530
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2086506
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2086508
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2086509
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2086510
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2086511
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2086512
    if-eqz v2, :cond_0

    .line 2086513
    const-string p0, "can_viewer_rate"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2086514
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2086515
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2086516
    if-eqz v2, :cond_1

    .line 2086517
    const-string p0, "overall_star_rating"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2086518
    invoke-static {v1, v2, p1, p2}, LX/5tf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2086519
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2086520
    if-eqz v2, :cond_2

    .line 2086521
    const-string p0, "spotlight_snippets_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2086522
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2086523
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2086524
    if-eqz v2, :cond_3

    .line 2086525
    const-string p0, "viewer_recommendation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2086526
    invoke-static {v1, v2, p1}, LX/EBB;->a(LX/15i;ILX/0nX;)V

    .line 2086527
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2086528
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2086507
    check-cast p1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$Serializer;->a(Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;LX/0nX;LX/0my;)V

    return-void
.end method
