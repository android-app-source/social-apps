.class public Lcom/facebook/reviews/ui/PageReviewsFeedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EA9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EAB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/79D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/EA0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field private l:Z

.field public m:Landroid/view/ViewGroup;

.field private n:Landroid/widget/ProgressBar;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public q:Lcom/facebook/widget/listview/BetterListView;

.field public r:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2087126
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2087127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->k:Z

    .line 2087128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->l:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2087129
    const-string v0, "reviews_feed"

    return-object v0
.end method

.method public a(LX/62n;)V
    .locals 0

    .prologue
    .line 2087072
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2087130
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2087131
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-static {v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const/16 v7, 0x31c0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    new-instance v10, LX/EAB;

    const/16 v8, 0x12cb

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/EA9;->b(LX/0QB;)LX/EA9;

    move-result-object v8

    check-cast v8, LX/EA9;

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v9

    check-cast v9, LX/79D;

    invoke-direct {v10, v11, v8, v9}, LX/EAB;-><init>(LX/0Or;LX/EA9;LX/79D;)V

    move-object v8, v10

    check-cast v8, LX/EAB;

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v9

    check-cast v9, LX/79D;

    invoke-static {v0}, LX/EA0;->b(LX/0QB;)LX/EA0;

    move-result-object v10

    check-cast v10, LX/EA0;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    iput-object v5, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object v6, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->b:LX/03V;

    iput-object v7, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->c:LX/0Ot;

    iput-object v8, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->d:LX/EAB;

    iput-object v9, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->e:LX/79D;

    iput-object v10, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    iput-object v11, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->g:LX/0kL;

    iput-object v0, v4, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->h:LX/0hB;

    .line 2087132
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087133
    const-string v3, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2087134
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Invalid page id: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2087135
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->i:Ljava/lang/String;

    .line 2087136
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087137
    const-string v3, "profile_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->j:Ljava/lang/String;

    .line 2087138
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087139
    const-string v3, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->k:Z

    .line 2087140
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087141
    const-string v2, "show_reviews_composer"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->l:Z

    .line 2087142
    if-nez p1, :cond_0

    .line 2087143
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->e:LX/79D;

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->i:Ljava/lang/String;

    .line 2087144
    const-string v2, "reviews_feed_impression"

    const-string v3, "reviews_feed"

    invoke-static {v0, v2, v3, v1}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2087145
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2087146
    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;)V
    .locals 5

    .prologue
    .line 2087147
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2087148
    const v0, 0x7f0311db

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2087149
    iget-boolean v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->k:Z

    if-eqz v1, :cond_0

    .line 2087150
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 2087151
    :cond_0
    const v1, 0x7f0d29f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o:Landroid/view/View;

    .line 2087152
    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2087153
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    .line 2087154
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2087155
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2087156
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 2087157
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2087158
    :cond_1
    const/4 v3, 0x0

    .line 2087159
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2087160
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d3f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2087161
    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1, v3, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(II)V

    .line 2087162
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2087163
    return-void

    .line 2087164
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2087165
    const v0, 0x7f0311e0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    .line 2087166
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2087167
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 2087168
    const v0, 0x7f0311df

    return v0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 2087169
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2087170
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2087171
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2087172
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 2087124
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2087125
    return-void
.end method

.method public n()V
    .locals 3

    .prologue
    .line 2087173
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->b:LX/03V;

    const-class v1, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Review Feed failed to load reviews"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087174
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2087175
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->g:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814f8

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2087176
    return-void
.end method

.method public final o()Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;
    .locals 1

    .prologue
    .line 2087123
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o:Landroid/view/View;

    check-cast v0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    .line 2087112
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2087113
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2087114
    :goto_0
    return-void

    .line 2087115
    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2087116
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->g:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081412

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2087117
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2087118
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EA9;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->i:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a()Ljava/lang/String;

    move-result-object v5

    move v2, p2

    move-object v3, p3

    .line 2087119
    const/4 v6, -0x1

    if-eq v2, v6, :cond_1

    .line 2087120
    :goto_1
    goto :goto_0

    .line 2087121
    :cond_1
    new-instance v7, LX/EA7;

    invoke-direct {v7, v0, v4}, LX/EA7;-><init>(LX/EA9;Ljava/lang/String;)V

    .line 2087122
    iget-object v6, v0, LX/EA9;->d:LX/BNP;

    invoke-static {v7}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v10

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v11

    move v7, v2

    move-object v8, v3

    move-object v9, v5

    invoke-virtual/range {v6 .. v11}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x21a243fb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2087104
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->b()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m:Landroid/view/ViewGroup;

    .line 2087105
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 2087106
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m:Landroid/view/ViewGroup;

    const v2, 0x7f0d29f7

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    .line 2087107
    invoke-virtual {p0, p1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a(Landroid/view/LayoutInflater;)V

    .line 2087108
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    .line 2087109
    new-instance v2, LX/EBH;

    invoke-direct {v2, p0}, LX/EBH;-><init>(Lcom/facebook/reviews/ui/PageReviewsFeedFragment;)V

    move-object v2, v2

    .line 2087110
    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2087111
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, -0x1f1a7f49

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7a05b2ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087091
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2087092
    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    .line 2087093
    iget-object v2, v1, LX/EA0;->e:LX/EAV;

    if-eqz v2, :cond_1

    .line 2087094
    iget-object v2, v1, LX/EA0;->e:LX/EAV;

    .line 2087095
    iget-object v4, v2, LX/EAV;->c:LX/1My;

    invoke-virtual {v4}, LX/1My;->a()V

    .line 2087096
    iget-object v4, v2, LX/EAV;->g:LX/0am;

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2087097
    iget-object v4, v2, LX/EAV;->g:LX/0am;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1B1;

    iget-object p0, v2, LX/EAV;->b:LX/0bH;

    invoke-virtual {v4, p0}, LX/1B1;->b(LX/0b4;)V

    .line 2087098
    :cond_0
    iget-object v4, v2, LX/EAV;->f:LX/1B1;

    iget-object p0, v2, LX/EAV;->e:LX/Ch5;

    invoke-virtual {v4, p0}, LX/1B1;->b(LX/0b4;)V

    .line 2087099
    :cond_1
    iget-object v2, v1, LX/EA0;->m:LX/1Qq;

    if-eqz v2, :cond_2

    .line 2087100
    iget-object v2, v1, LX/EA0;->m:LX/1Qq;

    invoke-interface {v2}, LX/0Vf;->dispose()V

    .line 2087101
    :cond_2
    iget-object v2, v1, LX/EA0;->f:LX/EAH;

    if-eqz v2, :cond_3

    .line 2087102
    iget-object v2, v1, LX/EA0;->f:LX/EAH;

    invoke-virtual {v2}, LX/EAH;->a()V

    .line 2087103
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x2a4bd74c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b5ae263

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087086
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2087087
    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    .line 2087088
    iget-object v2, v1, LX/EA0;->e:LX/EAV;

    .line 2087089
    iget-object v1, v2, LX/EAV;->c:LX/1My;

    invoke-virtual {v1}, LX/1My;->d()V

    .line 2087090
    const/16 v1, 0x2b

    const v2, 0x1f56ce8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x40e019df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087081
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2087082
    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    .line 2087083
    iget-object v2, v1, LX/EA0;->e:LX/EAV;

    .line 2087084
    iget-object v1, v2, LX/EAV;->c:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 2087085
    const/16 v1, 0x2b

    const v2, -0x1b06cdda

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2087078
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2087079
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    iget-object v1, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->i:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->l:Z

    invoke-virtual {v0, v1, v2, v3, p0}, LX/EA0;->a(Landroid/widget/ListView;Ljava/lang/String;ZLcom/facebook/reviews/ui/PageReviewsFeedFragment;)V

    .line 2087080
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 2087075
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2087076
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2087077
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2087073
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2087074
    return-void
.end method
