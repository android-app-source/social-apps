.class public Lcom/facebook/reviews/ui/BarChart;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/EBF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2086904
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2086905
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    .line 2086906
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    .line 2086907
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    .line 2086908
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/reviews/ui/BarChart;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2086909
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2086984
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2086985
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    .line 2086986
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    .line 2086987
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    .line 2086988
    invoke-direct {p0, p1, p2}, Lcom/facebook/reviews/ui/BarChart;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2086989
    return-void
.end method

.method private a(Landroid/text/SpannableString;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2086979
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2086980
    iget v1, p0, Lcom/facebook/reviews/ui/BarChart;->f:I

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2086981
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2086982
    invoke-virtual {v0, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 2086983
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0d54

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2086963
    const-class v0, Lcom/facebook/reviews/ui/BarChart;

    invoke-static {v0, p0}, Lcom/facebook/reviews/ui/BarChart;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2086964
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/BarChart;->o:Ljava/util/ArrayList;

    .line 2086965
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/BarChart;->setOrientation(I)V

    .line 2086966
    sget-object v0, LX/03r;->BarChart:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2086967
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->e:I

    .line 2086968
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->f:I

    .line 2086969
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->g:I

    .line 2086970
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->h:I

    .line 2086971
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->i:I

    .line 2086972
    const/16 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->j:I

    .line 2086973
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->k:I

    .line 2086974
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->l:I

    .line 2086975
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/reviews/ui/BarChart;->m:Z

    .line 2086976
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2086977
    iput-boolean v2, p0, Lcom/facebook/reviews/ui/BarChart;->n:Z

    .line 2086978
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reviews/ui/BarChart;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/reviews/ui/BarChart;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    iput-object v0, p0, Lcom/facebook/reviews/ui/BarChart;->a:LX/154;

    return-void
.end method

.method private setBarCharItemViewStyle(LX/EBG;)V
    .locals 2

    .prologue
    .line 2086947
    iget-boolean v0, p0, Lcom/facebook/reviews/ui/BarChart;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, LX/EBG;->setValueVisibility(I)V

    .line 2086948
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->f:I

    invoke-virtual {p1, v0}, LX/EBG;->setLabelTextsize(I)V

    .line 2086949
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->g:I

    invoke-virtual {p1, v0}, LX/EBG;->setLabelTextColor(I)V

    .line 2086950
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->h:I

    invoke-virtual {p1, v0}, LX/EBG;->setLabelBarSpacing(I)V

    .line 2086951
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->j:I

    invoke-virtual {p1, v0}, LX/EBG;->setBarHeight(I)V

    .line 2086952
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->k:I

    .line 2086953
    iput v0, p1, LX/EBG;->h:I

    .line 2086954
    iget-boolean v0, p0, Lcom/facebook/reviews/ui/BarChart;->n:Z

    .line 2086955
    iput-boolean v0, p1, LX/EBG;->j:Z

    .line 2086956
    iget v0, p0, Lcom/facebook/reviews/ui/BarChart;->l:I

    .line 2086957
    iput v0, p1, LX/EBG;->k:I

    .line 2086958
    invoke-virtual {p1}, LX/EBG;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2086959
    iget v1, p0, Lcom/facebook/reviews/ui/BarChart;->e:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2086960
    invoke-virtual {p1, v0}, LX/EBG;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2086961
    return-void

    .line 2086962
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2086942
    iget-object v0, p0, Lcom/facebook/reviews/ui/BarChart;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2086943
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->removeAllViews()V

    .line 2086944
    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    .line 2086945
    iput v1, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    .line 2086946
    return-void
.end method

.method public final a(LX/EBF;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2086918
    iget-object v0, p0, Lcom/facebook/reviews/ui/BarChart;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2086919
    iget v0, p1, LX/EBF;->c:I

    move v0, v0

    .line 2086920
    iget v3, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    if-le v0, v3, :cond_4

    .line 2086921
    iget v0, p1, LX/EBF;->c:I

    move v0, v0

    .line 2086922
    iput v0, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    move v0, v1

    .line 2086923
    :goto_0
    iget-object v3, p1, LX/EBF;->b:Landroid/text/SpannableString;

    move-object v3, v3

    .line 2086924
    invoke-direct {p0, v3}, Lcom/facebook/reviews/ui/BarChart;->a(Landroid/text/SpannableString;)I

    move-result v3

    .line 2086925
    iget v4, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    if-le v3, v4, :cond_0

    .line 2086926
    iput v3, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    move v0, v1

    .line 2086927
    :cond_0
    new-instance v3, Landroid/text/SpannableString;

    iget-object v4, p0, Lcom/facebook/reviews/ui/BarChart;->a:LX/154;

    iget v5, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    invoke-virtual {v4, v5}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2086928
    invoke-direct {p0, v3}, Lcom/facebook/reviews/ui/BarChart;->a(Landroid/text/SpannableString;)I

    move-result v3

    .line 2086929
    iget v4, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    if-le v3, v4, :cond_3

    .line 2086930
    iput v3, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    .line 2086931
    :goto_1
    if-eqz v1, :cond_2

    move v6, v2

    .line 2086932
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 2086933
    invoke-virtual {p0, v6}, Lcom/facebook/reviews/ui/BarChart;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2086934
    instance-of v1, v0, LX/EBG;

    if-eqz v1, :cond_1

    .line 2086935
    check-cast v0, LX/EBG;

    iget-object v1, p0, Lcom/facebook/reviews/ui/BarChart;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EBF;

    iget v2, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    iget v3, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    iget v4, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    iget v5, p0, Lcom/facebook/reviews/ui/BarChart;->i:I

    invoke-virtual/range {v0 .. v5}, LX/EBG;->a(LX/EBF;IIII)V

    .line 2086936
    :cond_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 2086937
    :cond_2
    new-instance v0, LX/EBG;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EBG;-><init>(Landroid/content/Context;)V

    .line 2086938
    iget v2, p0, Lcom/facebook/reviews/ui/BarChart;->b:I

    iget v3, p0, Lcom/facebook/reviews/ui/BarChart;->c:I

    iget v4, p0, Lcom/facebook/reviews/ui/BarChart;->d:I

    iget v5, p0, Lcom/facebook/reviews/ui/BarChart;->i:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/EBG;->a(LX/EBF;IIII)V

    .line 2086939
    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/BarChart;->addView(Landroid/view/View;)V

    .line 2086940
    invoke-direct {p0, v0}, Lcom/facebook/reviews/ui/BarChart;->setBarCharItemViewStyle(LX/EBG;)V

    .line 2086941
    return-void

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public setBarAnimationEnabled(Z)V
    .locals 3

    .prologue
    .line 2086910
    iput-boolean p1, p0, Lcom/facebook/reviews/ui/BarChart;->n:Z

    .line 2086911
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/BarChart;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2086912
    invoke-virtual {p0, v1}, Lcom/facebook/reviews/ui/BarChart;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2086913
    instance-of v2, v0, LX/EBG;

    if-eqz v2, :cond_0

    .line 2086914
    check-cast v0, LX/EBG;

    iget-boolean v2, p0, Lcom/facebook/reviews/ui/BarChart;->n:Z

    .line 2086915
    iput-boolean v2, v0, LX/EBG;->j:Z

    .line 2086916
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2086917
    :cond_1
    return-void
.end method
