.class public Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;
.super Lcom/facebook/reviews/ui/PageReviewsFeedFragment;
.source ""


# instance fields
.field public i:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/view/ViewGroup;

.field private l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field private m:Landroid/view/View;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2087232
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;
    .locals 4

    .prologue
    .line 2087223
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2087224
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2087225
    const-string v1, "session_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087226
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087227
    const-string v1, "fragment_title"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087228
    const-string v1, "extra_is_inside_page_surface_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2087229
    new-instance v1, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;

    invoke-direct {v1}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;-><init>()V

    .line 2087230
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2087231
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->i:LX/03V;

    iput-object p0, p1, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->j:LX/0Uh;

    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 2087219
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2087220
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    if-eqz v0, :cond_0

    .line 2087221
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->setVisibility(I)V

    .line 2087222
    :cond_0
    return-void
.end method

.method private w()Z
    .locals 3

    .prologue
    .line 2087218
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->j:LX/0Uh;

    const/16 v1, 0x5e3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/62n;)V
    .locals 1

    .prologue
    .line 2087216
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0, p1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2087217
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2087177
    invoke-super {p0, p1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2087178
    const-class v0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;

    invoke-static {v0, p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2087179
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087180
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->n:Ljava/lang/String;

    .line 2087181
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2087214
    const v0, 0x7f0d29f8

    invoke-static {p3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2087215
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2087213
    const v0, 0x7f0311e1    # 1.742217E38f

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2087207
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    move-object v0, v0

    .line 2087208
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2087209
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    move-object v0, v0

    .line 2087210
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2087211
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->e()V

    .line 2087212
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 2087203
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2087204
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->u()V

    .line 2087205
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2087206
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 2087198
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2087199
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->u()V

    .line 2087200
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->i:LX/03V;

    const-class v1, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Review Feed failed to load reviews"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087201
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    const v1, 0x7f0814f8

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/62l;->a(Ljava/lang/String;)V

    .line 2087202
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x772eaf1d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2087192
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->k:Landroid/view/ViewGroup;

    .line 2087193
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2087194
    const v0, 0x7f0311e8

    iget-object v2, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->k:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2087195
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->k:Landroid/view/ViewGroup;

    const v2, 0x7f0d2a0b

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->m:Landroid/view/View;

    .line 2087196
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->l:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->setVisibility(I)V

    .line 2087197
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->k:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, -0x70f570cc

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x215e38da

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2087187
    invoke-super {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->onPause()V

    .line 2087188
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2087189
    if-eqz v0, :cond_0

    .line 2087190
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2087191
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x1853007

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e5ed0d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087182
    invoke-super {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->onResume()V

    .line 2087183
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2087184
    if-nez v1, :cond_0

    .line 2087185
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x32a2a9df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2087186
    :cond_0
    const v2, 0x7f0814f5

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0
.end method
