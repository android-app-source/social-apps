.class public Lcom/facebook/reviews/ui/UserPlacesToReviewView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Landroid/widget/Button;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2087495
    const-class v0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2087496
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2087497
    const p1, 0x7f03155e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2087498
    const p1, 0x7f0d3011

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2087499
    const p1, 0x7f0d3012

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2087500
    const p1, 0x7f0d3013

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2087501
    const p1, 0x7f0d3014

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2087502
    const p1, 0x7f0d3015

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->o:Landroid/widget/Button;

    .line 2087503
    return-void
.end method
