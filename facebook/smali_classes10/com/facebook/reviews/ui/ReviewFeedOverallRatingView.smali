.class public Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/79D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/BNM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BNF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/E9f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:Landroid/widget/TextView;

.field private k:Lcom/facebook/reviews/ui/BarChart;

.field private l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2087300
    const-class v0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2087297
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2087298
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a()V

    .line 2087299
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2087294
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2087295
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a()V

    .line 2087296
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2087287
    const-class v0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    invoke-static {v0, p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2087288
    const v0, 0x7f0311e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2087289
    const v0, 0x7f0d29f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->j:Landroid/widget/TextView;

    .line 2087290
    const v0, 0x7f0d29fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/ui/BarChart;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->k:Lcom/facebook/reviews/ui/BarChart;

    .line 2087291
    const v0, 0x7f0d29fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2087292
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->k:Lcom/facebook/reviews/ui/BarChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/BarChart;->setBarAnimationEnabled(Z)V

    .line 2087293
    return-void
.end method

.method private static a(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;LX/03V;LX/17W;LX/1nG;LX/154;LX/79D;LX/BNM;LX/BNF;LX/E9f;)V
    .locals 0

    .prologue
    .line 2087286
    iput-object p1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->b:LX/17W;

    iput-object p3, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->c:LX/1nG;

    iput-object p4, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->d:LX/154;

    iput-object p5, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->e:LX/79D;

    iput-object p6, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->f:LX/BNM;

    iput-object p7, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->g:LX/BNF;

    iput-object p8, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->h:LX/E9f;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    invoke-static {v8}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v8}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {v8}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v3

    check-cast v3, LX/1nG;

    invoke-static {v8}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v4

    check-cast v4, LX/154;

    invoke-static {v8}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v5

    check-cast v5, LX/79D;

    invoke-static {v8}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v6

    check-cast v6, LX/BNM;

    invoke-static {v8}, LX/BNF;->a(LX/0QB;)LX/BNF;

    move-result-object v7

    check-cast v7, LX/BNF;

    invoke-static {v8}, LX/E9f;->a(LX/0QB;)LX/E9f;

    move-result-object v8

    check-cast v8, LX/E9f;

    invoke-static/range {v0 .. v8}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;LX/03V;LX/17W;LX/1nG;LX/154;LX/79D;LX/BNM;LX/BNF;LX/E9f;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;LX/1W5;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2087275
    invoke-interface {p1}, LX/1W5;->a()LX/171;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2087276
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a:LX/03V;

    sget-object v1, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->i:Ljava/lang/String;

    const-string v2, "No entity in spotlight string"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087277
    :goto_0
    return-void

    .line 2087278
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->e:LX/79D;

    invoke-interface {p1}, LX/1W5;->a()LX/171;

    move-result-object v1

    invoke-interface {v1}, LX/171;->e()Ljava/lang/String;

    move-result-object v1

    .line 2087279
    const-string v2, "entity_in_spotlight_section_tap"

    invoke-static {v2, p2, p3}, LX/79D;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2087280
    const-string v3, "target"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2087281
    iget-object v3, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2087282
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->c:LX/1nG;

    invoke-interface {p1}, LX/1W5;->a()LX/171;

    move-result-object v1

    invoke-static {v1}, LX/2yc;->a(LX/171;)LX/1yA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v0

    .line 2087283
    if-nez v0, :cond_1

    .line 2087284
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a:LX/03V;

    sget-object v1, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->i:Ljava/lang/String;

    const-string v2, "Could not find entity url in spotlight string"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2087285
    :cond_1
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->b:LX/17W;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(DI)V
    .locals 5

    .prologue
    .line 2087271
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->g:LX/BNF;

    invoke-virtual {v0, p1, p2}, LX/BNF;->a(D)Ljava/lang/String;

    move-result-object v0

    .line 2087272
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00a3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->d:LX/154;

    invoke-virtual {v4, p3}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2087273
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2087274
    return-void
.end method

.method public final a(LX/175;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2087268
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2087269
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v1, LX/EBJ;

    invoke-direct {v1, p0, p2, p3}, LX/EBJ;-><init>(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/175;LX/8uH;)V

    .line 2087270
    return-void
.end method

.method public final a(Landroid/util/SparseIntArray;I)V
    .locals 9

    .prologue
    .line 2087252
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->h:LX/E9f;

    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->k:Lcom/facebook/reviews/ui/BarChart;

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 2087253
    invoke-virtual {v1}, Lcom/facebook/reviews/ui/BarChart;->a()V

    .line 2087254
    invoke-virtual {v1}, Lcom/facebook/reviews/ui/BarChart;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v6, 0x0

    .line 2087255
    iget-object v3, v0, LX/E9f;->b:LX/0wM;

    const v4, 0x7f021893

    const v5, -0x958e80

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2087256
    const v4, 0x7f0b0d4c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2087257
    invoke-virtual {v3, v6, v6, v4, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2087258
    move-object v3, v3

    .line 2087259
    :goto_0
    if-lez p2, :cond_1

    .line 2087260
    invoke-virtual {v1}, Lcom/facebook/reviews/ui/BarChart;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081505

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2087261
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2087262
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v5

    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    new-array v6, v8, [Ljava/lang/Object;

    new-instance v7, Landroid/text/style/ImageSpan;

    invoke-direct {v7, v3, v8}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    aput-object v7, v6, p0

    invoke-static {v5, v2, v4, v6}, LX/47t;->a(IILandroid/text/SpannableString;[Ljava/lang/Object;)V

    .line 2087263
    iget-object v5, v0, LX/E9f;->c:Landroid/content/res/Resources;

    sget-object v2, LX/E9f;->a:[I

    array-length v2, v2

    if-ge p2, v2, :cond_0

    sget-object v2, LX/E9f;->a:[I

    add-int/lit8 v6, p2, -0x1

    aget v2, v2, v6

    :goto_1
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2087264
    new-instance v5, LX/EBF;

    invoke-virtual {p1, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    invoke-direct {v5, v2, v4, v6}, LX/EBF;-><init>(ILandroid/text/SpannableString;I)V

    invoke-virtual {v1, v5}, Lcom/facebook/reviews/ui/BarChart;->a(LX/EBF;)V

    .line 2087265
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 2087266
    :cond_0
    sget-object v2, LX/E9f;->a:[I

    sget-object v6, LX/E9f;->a:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget v2, v2, v6

    goto :goto_1

    .line 2087267
    :cond_1
    return-void
.end method

.method public setTitle(D)V
    .locals 8

    .prologue
    .line 2087242
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2087243
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->f:LX/BNM;

    .line 2087244
    const v2, 0x7f0a00d2

    .line 2087245
    iget-object v3, v1, LX/BNM;->b:Landroid/content/res/Resources;

    const v4, 0x7f081504

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2087246
    invoke-virtual {v1, p1, p2}, LX/BNM;->a(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    .line 2087247
    invoke-static {v1, v3, v4, v0, v2}, LX/BNM;->a(LX/BNM;Ljava/lang/String;III)Landroid/text/SpannableString;

    move-result-object v3

    move-object v2, v3

    .line 2087248
    move-object v0, v2

    .line 2087249
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0814ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2087250
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087251
    return-void
.end method
