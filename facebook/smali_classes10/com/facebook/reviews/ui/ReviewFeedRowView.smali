.class public Lcom/facebook/reviews/ui/ReviewFeedRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/79D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Lcom/facebook/maps/FbStaticMapView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private t:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2087468
    const-class v0, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2087408
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2087409
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a()V

    .line 2087410
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087411
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2087412
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a()V

    .line 2087413
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087414
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2087415
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a()V

    .line 2087416
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 2087417
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2087418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2087419
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2087420
    return-object v0
.end method

.method private a(III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 3

    .prologue
    .line 2087421
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2087422
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(Ljava/lang/CharSequence;)V

    .line 2087423
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->b:LX/0wM;

    const v2, -0x6e685d

    invoke-virtual {v1, p3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2087424
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/215;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 2087425
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2087426
    const-class v0, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    invoke-static {v0, p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2087427
    const v0, 0x7f0311e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2087428
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setOrientation(I)V

    .line 2087429
    const v0, 0x7f0d29fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2087430
    const v0, 0x7f0d29fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->l:Landroid/widget/TextView;

    .line 2087431
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->l:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2087432
    const v0, 0x7f0d29ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->m:Landroid/widget/TextView;

    .line 2087433
    const v0, 0x7f0d2a00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->n:Landroid/widget/ImageView;

    .line 2087434
    const v0, 0x7f0d2a01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    .line 2087435
    const v0, 0x7f0d2a02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 2087436
    const v0, 0x7f0d2a04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    .line 2087437
    const v0, 0x7f0d2a05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->r:Landroid/view/View;

    .line 2087438
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->b()V

    .line 2087439
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2087440
    const v0, 0x7f0d29ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    .line 2087441
    const v0, 0x7f0d29ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    .line 2087442
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0d29ee

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2087443
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0d29ef

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2087444
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0d29f0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->k:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 2087445
    return-void
.end method

.method private static a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/03V;LX/0wM;LX/0Or;LX/79D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reviews/ui/ReviewFeedRowView;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0wM;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/79D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2087446
    iput-object p1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->b:LX/0wM;

    iput-object p3, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->d:LX/79D;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    const/16 v3, 0x13a4

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v2

    check-cast v2, LX/79D;

    invoke-static {p0, v0, v1, v3, v2}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/03V;LX/0wM;LX/0Or;LX/79D;)V

    return-void
.end method

.method private a(Ljava/util/List;Landroid/view/View;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/TouchDelegate;",
            ">;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2087447
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2087448
    invoke-static {p2, p0, p3}, LX/190;->a(Landroid/view/View;Landroid/view/ViewParent;I)Landroid/view/TouchDelegate;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2087449
    :cond_0
    return-void
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/Spannable;
    .locals 3

    .prologue
    .line 2087450
    invoke-static {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2087451
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 2087452
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2087453
    :cond_0
    invoke-static {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->d(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v1

    .line 2087454
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2087455
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2087456
    :cond_1
    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2087457
    const v0, 0x7f0d2a06

    const v1, 0x7f080fc8

    const v2, 0x7f0219c6

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2087458
    const v0, 0x7f0d2a07

    const v1, 0x7f080fcc

    const v2, 0x7f0219c3

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->t:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2087459
    return-void
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2087460
    new-instance v1, LX/3DI;

    invoke-direct {v1}, LX/3DI;-><init>()V

    .line 2087461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2087462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v0

    .line 2087463
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v3, :cond_0

    .line 2087464
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2087465
    invoke-virtual {v1, v0}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    goto :goto_1

    .line 2087466
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 2087467
    :cond_1
    return-object v1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2087375
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2087376
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0d40

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2087377
    iget-object v2, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->n:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Ljava/util/List;Landroid/view/View;I)V

    .line 2087378
    iget-object v2, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Ljava/util/List;Landroid/view/View;I)V

    .line 2087379
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2087380
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 2087381
    :goto_0
    return-void

    .line 2087382
    :cond_0
    new-instance v1, LX/46u;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Landroid/view/TouchDelegate;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/TouchDelegate;

    invoke-direct {v1, p0, v0}, LX/46u;-><init>(Landroid/view/View;[Landroid/view/TouchDelegate;)V

    invoke-virtual {p0, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2087469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->x()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    .line 2087470
    if-nez v0, :cond_0

    .line 2087471
    const/4 v0, 0x0

    .line 2087472
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setCoverPhotoOrMapView(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2087473
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2087474
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2087475
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2087476
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 2087477
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/reviews/ui/ReviewFeedRowView;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2087478
    :goto_0
    return-void

    .line 2087479
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2087480
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2087481
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    goto :goto_0

    .line 2087482
    :cond_1
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, v2}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2087483
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "place_review_ego_unit"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fn()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    .line 2087484
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v1, v0}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    goto :goto_0
.end method

.method private setMargin(Z)V
    .locals 4

    .prologue
    .line 2087485
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d49

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 2087486
    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d3f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 2087487
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2087488
    if-eqz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v3, v2, v3, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2087489
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2087490
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2087491
    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2087492
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2087493
    return-void

    :cond_0
    move v1, v3

    .line 2087494
    goto :goto_0
.end method

.method private setProfilePhotoLayout(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 3

    .prologue
    .line 2087383
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hm()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    .line 2087384
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2087385
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02147e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2087386
    :goto_0
    return-void

    .line 2087387
    :cond_1
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V
    .locals 4
    .param p1    # Landroid/text/SpannableStringBuilder;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/text/SpannableStringBuilder;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2087388
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 2087389
    :cond_1
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a:LX/03V;

    const-class v1, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tried to set a review without a rating"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087390
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087391
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 2087392
    :goto_0
    return-void

    .line 2087393
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 2087394
    :cond_3
    invoke-direct {p0, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setMargin(Z)V

    .line 2087395
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087396
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 2087397
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2087398
    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 2087399
    :cond_5
    invoke-direct {p0, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setMargin(Z)V

    .line 2087400
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087401
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 2087402
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2087403
    :cond_6
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setMargin(Z)V

    .line 2087404
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087405
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 2087406
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087407
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2087325
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 2087326
    invoke-direct {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->c()V

    .line 2087327
    return-void
.end method

.method public setAttachmentClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2087350
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087351
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0}, Lcom/facebook/maps/FbStaticMapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2087352
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, Lcom/facebook/maps/FbStaticMapView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087353
    :cond_0
    return-void
.end method

.method public setCommentButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2087348
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->t:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087349
    return-void
.end method

.method public setCommentButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2087346
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->t:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 2087347
    return-void
.end method

.method public setFeedbackDividerVisibility(I)V
    .locals 1

    .prologue
    .line 2087344
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->r:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2087345
    return-void
.end method

.method public setFeedbackSummary(Landroid/text/SpannableStringBuilder;)V
    .locals 2

    .prologue
    .line 2087338
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 2087339
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087340
    :goto_0
    return-void

    .line 2087341
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087342
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087343
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->q:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method public setLikeButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2087336
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087337
    return-void
.end method

.method public setLikeButtonLikeState(Z)V
    .locals 4

    .prologue
    .line 2087330
    if-eqz p1, :cond_0

    .line 2087331
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 2087332
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0219c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2087333
    :goto_0
    return-void

    .line 2087334
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 2087335
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->b:LX/0wM;

    const v2, 0x7f0219c6

    const v3, -0x6e685d

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setLikeButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2087328
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 2087329
    return-void
.end method

.method public setPageAttachmentView(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    .line 2087305
    if-nez p1, :cond_0

    .line 2087306
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2087307
    :goto_0
    return-void

    .line 2087308
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 2087309
    const v0, 0x7f0d2a03

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 2087310
    invoke-direct {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Landroid/view/View;)V

    .line 2087311
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2087312
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2087313
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2087314
    :cond_2
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2087315
    :cond_3
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2087316
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->k:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    const-string v2, "page_see_all_reviews"

    new-instance v3, LX/EBK;

    invoke-direct {v3, p0, v0}, LX/EBK;-><init>(Lcom/facebook/reviews/ui/ReviewFeedRowView;Lcom/facebook/graphql/model/GraphQLNode;)V

    invoke-virtual {v1, v0, v2, p1, v3}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V

    .line 2087317
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->j:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2087318
    invoke-direct {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setCoverPhotoOrMapView(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2087319
    invoke-direct {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setProfilePhotoLayout(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2087320
    invoke-static {v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/Spannable;

    move-result-object v1

    .line 2087321
    iget-object v2, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2087322
    iget-object v2, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2087323
    invoke-static {v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->b(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/text/Spannable;

    move-result-object v0

    .line 2087324
    iget-object v1, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->f:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setProfilePicOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2087354
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087355
    return-void
.end method

.method public setProfilePicture(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087356
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2087357
    return-void
.end method

.method public setReviewTextExpandedState(LX/4lJ;)V
    .locals 1

    .prologue
    .line 2087358
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setExpandState(LX/4lJ;)V

    .line 2087359
    return-void
.end method

.method public setReviewTextOnExpandStateChangeListener(LX/4lK;)V
    .locals 1

    .prologue
    .line 2087360
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setOnExpandedStateChangeListener(LX/4lK;)V

    .line 2087361
    return-void
.end method

.method public setSecondaryActionClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087362
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087363
    return-void
.end method

.method public setSecondaryActionVisibility(I)V
    .locals 1

    .prologue
    .line 2087364
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2087365
    return-void
.end method

.method public setSubtitle(Landroid/text/SpannableString;)V
    .locals 2

    .prologue
    .line 2087366
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->m:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2087367
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087368
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087369
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087370
    return-void
.end method

.method public setTitleOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2087371
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2087372
    return-void
.end method

.method public setTitleTextAppearance(I)V
    .locals 2

    .prologue
    .line 2087373
    iget-object v0, p0, Lcom/facebook/reviews/ui/ReviewFeedRowView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2087374
    return-void
.end method
