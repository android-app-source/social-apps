.class public Lcom/facebook/reviews/ui/UserReviewsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->USER_REVIEWS_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/79D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/E9U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EAh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/EA4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field public g:Lcom/facebook/widget/listview/BetterListView;

.field public h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2087578
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/reviews/ui/UserReviewsFragment;

    const/16 v1, 0x15e7

    invoke-static {v5, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v5}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v2

    check-cast v2, LX/79D;

    new-instance p1, LX/E9U;

    invoke-static {v5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v5}, LX/E9i;->a(LX/0QB;)LX/E9i;

    move-result-object v4

    check-cast v4, LX/E9i;

    invoke-static {v5}, LX/E9m;->a(LX/0QB;)LX/E9m;

    move-result-object v6

    check-cast v6, LX/E9m;

    new-instance p0, LX/E9o;

    invoke-direct {p0}, LX/E9o;-><init>()V

    move-object p0, p0

    move-object p0, p0

    check-cast p0, LX/E9o;

    invoke-direct {p1, v3, v4, v6, p0}, LX/E9U;-><init>(Landroid/content/res/Resources;LX/E9i;LX/E9m;LX/E9o;)V

    move-object v3, p1

    check-cast v3, LX/E9U;

    invoke-static {v5}, LX/EAh;->a(LX/0QB;)LX/EAh;

    move-result-object v4

    check-cast v4, LX/EAh;

    new-instance v6, LX/EA4;

    invoke-static {v5}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v7

    check-cast v7, LX/0bH;

    invoke-static {v5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v5}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v9

    check-cast v9, LX/Ch5;

    const/16 v10, 0x45a

    invoke-static {v5, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v5}, LX/EAs;->a(LX/0QB;)LX/EAs;

    move-result-object v11

    check-cast v11, LX/EAs;

    const/16 v12, 0x15e7

    invoke-static {v5, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v5}, LX/EB0;->a(LX/0QB;)LX/EB0;

    move-result-object v13

    check-cast v13, LX/EB0;

    invoke-direct/range {v6 .. v13}, LX/EA4;-><init>(LX/0bH;Landroid/content/res/Resources;LX/Ch5;LX/0Or;LX/EAs;LX/0Or;LX/EB0;)V

    move-object v5, v6

    check-cast v5, LX/EA4;

    iput-object v1, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->a:LX/0Or;

    iput-object v2, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->b:LX/79D;

    iput-object v3, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->c:LX/E9U;

    iput-object v4, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->d:LX/EAh;

    iput-object v5, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->e:LX/EA4;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2087577
    const-string v0, "user_reviews_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2087538
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2087539
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/reviews/ui/UserReviewsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2087540
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087541
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->i:Ljava/lang/String;

    .line 2087542
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2087543
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->i:Ljava/lang/String;

    .line 2087544
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087545
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->j:Ljava/lang/String;

    .line 2087546
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2087547
    const-string v1, "review_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->k:Ljava/lang/String;

    .line 2087548
    if-nez p1, :cond_1

    .line 2087549
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->b:LX/79D;

    iget-object v1, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->i:Ljava/lang/String;

    .line 2087550
    iget-object v2, v0, LX/79D;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "user_reviews_list_impression"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "user_reviews_list"

    .line 2087551
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2087552
    move-object p0, p0

    .line 2087553
    const-string p1, "review_creator_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2087554
    :cond_1
    return-void
.end method

.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 2087575
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2087576
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2087573
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2087574
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2087571
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2087572
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    .line 2087564
    const/16 v0, 0x6df

    if-ne p1, v0, :cond_0

    .line 2087565
    const-string v0, "publishReviewParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 2087566
    if-eqz v2, :cond_0

    .line 2087567
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->d:LX/EAh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, v2, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "user_reviews_list"

    move v2, p2

    move-object v3, p3

    .line 2087568
    new-instance v7, LX/EAe;

    invoke-direct {v7, v0, v4}, LX/EAe;-><init>(LX/EAh;Ljava/lang/String;)V

    .line 2087569
    iget-object v6, v0, LX/EAh;->b:LX/BNP;

    invoke-static {v7}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v10

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v11

    move v7, v2

    move-object v8, v3

    move-object v9, v5

    invoke-virtual/range {v6 .. v11}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2087570
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a9b909b    # 5097549.5f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2087555
    const v0, 0x7f03155f

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2087556
    const v0, 0x7f0d3016

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2087557
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    const v3, 0x7f0d3017

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 2087558
    const/4 p2, 0x0

    .line 2087559
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0311e7

    iget-object p1, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2087560
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 p1, 0x0

    invoke-virtual {v0, v3, p1, p2}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2087561
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->c:LX/E9U;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2087562
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2087563
    const/16 v0, 0x2b

    const v3, -0x4d6d777f

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x121b0839

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087528
    iget-object v1, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->e:LX/EA4;

    const/4 v5, 0x0

    .line 2087529
    iget-object v2, v1, LX/EA4;->g:LX/EB0;

    invoke-virtual {v2}, LX/EB0;->a()V

    .line 2087530
    iget-object v2, v1, LX/EA4;->o:LX/1B1;

    if-eqz v2, :cond_0

    .line 2087531
    iget-object v2, v1, LX/EA4;->o:LX/1B1;

    iget-object v4, v1, LX/EA4;->c:LX/Ch5;

    invoke-virtual {v2, v4}, LX/1B1;->b(LX/0b4;)V

    .line 2087532
    :cond_0
    iget-object v2, v1, LX/EA4;->p:LX/1B1;

    if-eqz v2, :cond_1

    .line 2087533
    iget-object v2, v1, LX/EA4;->p:LX/1B1;

    iget-object v4, v1, LX/EA4;->a:LX/0bH;

    invoke-virtual {v2, v4}, LX/1B1;->b(LX/0b4;)V

    .line 2087534
    :cond_1
    iput-object v5, v1, LX/EA4;->o:LX/1B1;

    .line 2087535
    iput-object v5, v1, LX/EA4;->p:LX/1B1;

    .line 2087536
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2087537
    const/16 v1, 0x2b

    const v2, -0xe9bc3dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x136afd4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087521
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2087522
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2087523
    if-eqz v1, :cond_0

    .line 2087524
    iget-object v2, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->j:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 2087525
    const v2, 0x7f0814ee

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->j:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2087526
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x7a610f17

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2087527
    :cond_1
    const v2, 0x7f0814f5

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2087508
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2087509
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->e:LX/EA4;

    iget-object v1, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->c:LX/E9U;

    iget-object v2, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->k:Ljava/lang/String;

    .line 2087510
    iget-object p1, v0, LX/EA4;->d:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1B1;

    iput-object p1, v0, LX/EA4;->o:LX/1B1;

    .line 2087511
    iget-object p1, v0, LX/EA4;->d:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1B1;

    iput-object p1, v0, LX/EA4;->p:LX/1B1;

    .line 2087512
    iput-object v2, v0, LX/EA4;->m:Ljava/lang/String;

    .line 2087513
    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, LX/EA4;->l:LX/0am;

    .line 2087514
    iput-object v1, v0, LX/EA4;->r:LX/E9U;

    .line 2087515
    iput-object p0, v0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    .line 2087516
    iget-object p1, v0, LX/EA4;->f:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object p2, v0, LX/EA4;->m:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, v0, LX/EA4;->j:Z

    .line 2087517
    invoke-static {v0}, LX/EA4;->k(LX/EA4;)V

    .line 2087518
    invoke-virtual {v0}, LX/EA4;->a()V

    .line 2087519
    iget-object v0, p0, Lcom/facebook/reviews/ui/UserReviewsFragment;->f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->e()V

    .line 2087520
    return-void
.end method
