.class public Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->GOODFRIENDS_NUX_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/9J2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/support/v4/view/ViewPager;

.field private c:LX/DIS;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1983458
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1983459
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 1983460
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->c:LX/DIS;

    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1983461
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1983462
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1983463
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1983464
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;

    invoke-static {v0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v0

    check-cast v0, LX/9J2;

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->a:LX/9J2;

    .line 1983465
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x2b23ae9b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1983466
    const v0, 0x7f0307c9

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1983467
    const v0, 0x7f0d14a0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->b:Landroid/support/v4/view/ViewPager;

    .line 1983468
    new-instance v0, LX/DIP;

    new-instance v3, Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-direct {v3}, Lcom/facebook/goodfriends/audience/AudienceFragment;-><init>()V

    new-instance v4, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;

    invoke-direct {v4}, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;-><init>()V

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v3, v4}, LX/DIP;-><init>(LX/0Px;Landroid/support/v4/view/ViewPager;)V

    .line 1983469
    new-instance v3, LX/DIS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    invoke-direct {v3, v4, v0}, LX/DIS;-><init>(LX/0gc;LX/DIP;)V

    iput-object v3, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->c:LX/DIS;

    .line 1983470
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->b:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->c:LX/DIS;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1983471
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->b:Landroid/support/v4/view/ViewPager;

    new-instance v3, LX/DIQ;

    invoke-direct {v3, p0}, LX/DIQ;-><init>(Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1983472
    const/16 v0, 0x2b

    const v3, 0x7f5df55c

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x75fcf4a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983473
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1983474
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsNuxFragment;->a:LX/9J2;

    const-string v2, "Onboarding Enter"

    invoke-virtual {v1, v2}, LX/9J2;->a(Ljava/lang/String;)V

    .line 1983475
    const/16 v1, 0x2b

    const v2, 0x73051c25

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
