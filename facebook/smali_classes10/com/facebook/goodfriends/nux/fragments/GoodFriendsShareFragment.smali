.class public Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;
.super Lcom/facebook/goodfriends/nux/NuxFragment;
.source ""


# instance fields
.field public a:LX/9J7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

.field private final c:LX/107;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1983500
    invoke-direct {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;-><init>()V

    .line 1983501
    new-instance v0, LX/DIT;

    invoke-direct {v0, p0}, LX/DIT;-><init>(Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->c:LX/107;

    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f400000    # 0.75f

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1983530
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b186c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1983531
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    invoke-virtual {v1, v4}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->setScaleX(F)V

    .line 1983532
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    invoke-virtual {v1, v4}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->setScaleY(F)V

    .line 1983533
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->setTranslationY(F)V

    .line 1983534
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    invoke-virtual {v0, v3}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->setAlpha(F)V

    .line 1983535
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/DIU;

    invoke-direct {v1, p0}, LX/DIU;-><init>(Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1983536
    return-void
.end method

.method public static l(Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;)V
    .locals 1

    .prologue
    .line 1983537
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->b()V

    .line 1983538
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1983521
    iget-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    move v0, v0

    .line 1983522
    if-eqz v0, :cond_0

    .line 1983523
    invoke-static {p0}, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->l(Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;)V

    .line 1983524
    invoke-virtual {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;->d()V

    .line 1983525
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1983526
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1983527
    invoke-super {p0, p1}, Lcom/facebook/goodfriends/nux/NuxFragment;->a(Landroid/os/Bundle;)V

    .line 1983528
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;

    invoke-static {v0}, LX/9J7;->b(LX/0QB;)LX/9J7;

    move-result-object v0

    check-cast v0, LX/9J7;

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->a:LX/9J7;

    .line 1983529
    return-void
.end method

.method public final d(Z)V
    .locals 4

    .prologue
    .line 1983516
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->a:LX/9J7;

    const v2, 0x7f0823c3

    .line 1983517
    iget-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->b:Z

    move v0, v0

    .line 1983518
    if-eqz v0, :cond_0

    const v0, 0x7f0823c6

    :goto_0
    iget-object v3, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->c:LX/107;

    invoke-virtual {v1, v2, v0, p1, v3}, LX/9J7;->a(IIZLX/107;)Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1983519
    return-void

    .line 1983520
    :cond_0
    const v0, 0x7f0823c5

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f9cc74e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1983513
    const v0, 0x7f0307cc

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1983514
    const v0, 0x7f0d14a2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->b:Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;

    .line 1983515
    const/16 v0, 0x2b

    const v3, 0x448e45ca

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1983508
    invoke-super {p0, p1, p2}, Lcom/facebook/goodfriends/nux/NuxFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1983509
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1983510
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/nux/NuxFragment;->d(Z)V

    .line 1983511
    invoke-direct {p0}, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->k()V

    .line 1983512
    :cond_0
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1983502
    invoke-super {p0, p1}, Lcom/facebook/goodfriends/nux/NuxFragment;->setUserVisibleHint(Z)V

    .line 1983503
    if-eqz p1, :cond_0

    .line 1983504
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 1983505
    if-eqz v0, :cond_0

    .line 1983506
    invoke-direct {p0}, Lcom/facebook/goodfriends/nux/fragments/GoodFriendsShareFragment;->k()V

    .line 1983507
    :cond_0
    return-void
.end method
