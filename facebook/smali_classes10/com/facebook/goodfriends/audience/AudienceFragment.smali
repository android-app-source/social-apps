.class public Lcom/facebook/goodfriends/audience/AudienceFragment;
.super Lcom/facebook/goodfriends/nux/NuxFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->GOODFRIENDS_AUDIENCE_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/DIO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9J2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9J7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/22r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Ljava/lang/Runnable;

.field public final g:LX/DIB;

.field public h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

.field private i:Landroid/support/v7/widget/RecyclerView;

.field public j:LX/3wu;

.field public k:Lcom/facebook/resources/ui/FbEditText;

.field public l:Lcom/facebook/fbui/glyph/GlyphView;

.field public m:Ljava/lang/String;

.field public n:Landroid/os/Handler;

.field public o:LX/4BY;

.field public p:Z

.field public q:Lcom/facebook/goodfriends/audience/AudienceFooterBar;

.field private final r:LX/DIC;

.field private final s:LX/107;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1983273
    const-class v0, Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1983267
    invoke-direct {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;-><init>()V

    .line 1983268
    new-instance v0, Lcom/facebook/goodfriends/audience/AudienceFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/goodfriends/audience/AudienceFragment$1;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->f:Ljava/lang/Runnable;

    .line 1983269
    new-instance v0, LX/DIB;

    invoke-direct {v0, p0}, LX/DIB;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->g:LX/DIB;

    .line 1983270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->p:Z

    .line 1983271
    new-instance v0, LX/DIC;

    invoke-direct {v0, p0}, LX/DIC;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->r:LX/DIC;

    .line 1983272
    new-instance v0, LX/DIE;

    invoke-direct {v0, p0}, LX/DIE;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->s:LX/107;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/goodfriends/audience/AudienceFragment;LX/9J1;I)V
    .locals 7

    .prologue
    .line 1983228
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983229
    iget v2, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    move v1, v2

    .line 1983230
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983231
    iget-object v3, v2, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    move v2, v3

    .line 1983232
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1983233
    const-string v4, "profiles_total_count"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1983234
    const-string v4, "profiles_selected_count"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1983235
    sget-object v4, LX/9J1;->AWESOMIZER_CLOSE:LX/9J1;

    invoke-static {v0, v4, v3}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1983236
    const-string v4, "Audience Picker Closed"

    .line 1983237
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "goodfriends_session"

    iget-object v1, v0, LX/9J2;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    .line 1983238
    if-eqz v3, :cond_0

    .line 1983239
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1983240
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v5, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    goto :goto_0

    .line 1983241
    :cond_0
    iget-object v5, v0, LX/9J2;->b:LX/0if;

    sget-object v1, LX/0ig;->aw:LX/0ih;

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v4, v2, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1983242
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    invoke-virtual {v0, p1}, LX/9J2;->a(LX/9J1;)V

    .line 1983243
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1983244
    if-nez v0, :cond_1

    .line 1983245
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1983246
    :cond_1
    if-nez p2, :cond_3

    .line 1983247
    invoke-virtual {v0, p2}, Landroid/app/Activity;->setResult(I)V

    .line 1983248
    iget-boolean v1, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    move v1, v1

    .line 1983249
    if-eqz v1, :cond_2

    .line 1983250
    invoke-virtual {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;->d()V

    .line 1983251
    :goto_1
    return-void

    .line 1983252
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 1983253
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1983254
    const-string v2, "has_good_friends"

    iget-object v3, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983255
    iget v4, v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    if-lez v4, :cond_6

    const/4 v4, 0x1

    :goto_2
    move v3, v4

    .line 1983256
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1983257
    const-string v2, "audience_changed"

    iget-object v3, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983258
    iget-object v4, v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_4
    const/4 v4, 0x1

    :goto_3
    move v3, v4

    .line 1983259
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1983260
    invoke-virtual {v0, p2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1983261
    iget-boolean v1, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    move v1, v1

    .line 1983262
    if-eqz v1, :cond_5

    .line 1983263
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->e:LX/22r;

    .line 1983264
    iget-object v1, v0, LX/22r;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/22r;->a:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1983265
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/nux/NuxFragment;->c(Z)V

    goto :goto_1

    .line 1983266
    :cond_5
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    :cond_7
    const/4 v4, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final S_()Z
    .locals 5

    .prologue
    .line 1983220
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1983221
    new-instance v1, LX/4me;

    invoke-direct {v1, v0}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 1983222
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0823bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4me;->setTitle(Ljava/lang/CharSequence;)V

    .line 1983223
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0823bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1983224
    const/4 v2, -0x1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0823bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/DIJ;

    invoke-direct {v4, p0}, LX/DIJ;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1983225
    const/4 v2, -0x2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0823be

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1983226
    invoke-virtual {v1}, LX/4me;->show()V

    .line 1983227
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1983274
    invoke-super {p0, p1}, Lcom/facebook/goodfriends/nux/NuxFragment;->a(Landroid/os/Bundle;)V

    .line 1983275
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodfriends/audience/AudienceFragment;

    const-class v2, LX/DIO;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/DIO;

    invoke-static {v0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v3

    check-cast v3, LX/9J2;

    invoke-static {v0}, LX/9J7;->b(LX/0QB;)LX/9J7;

    move-result-object p1

    check-cast p1, LX/9J7;

    invoke-static {v0}, LX/22r;->b(LX/0QB;)LX/22r;

    move-result-object v0

    check-cast v0, LX/22r;

    iput-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->b:LX/DIO;

    iput-object v3, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    iput-object p1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->d:LX/9J7;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->e:LX/22r;

    .line 1983276
    return-void
.end method

.method public final d(Z)V
    .locals 4

    .prologue
    .line 1983215
    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->d:LX/9J7;

    const v2, 0x7f0823a5

    .line 1983216
    iget-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->b:Z

    move v0, v0

    .line 1983217
    if-eqz v0, :cond_0

    const v0, 0x7f0823c1

    :goto_0
    iget-object v3, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->s:LX/107;

    invoke-virtual {v1, v2, v0, p1, v3}, LX/9J7;->a(IIZLX/107;)Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1983218
    return-void

    .line 1983219
    :cond_0
    const v0, 0x7f0823a6

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x785426c9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983212
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1983213
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/goodfriends/nux/NuxFragment;->d(Z)V

    .line 1983214
    :cond_0
    const v1, 0x7f030136

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1d38a13f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a52adb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983209
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->k:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1983210
    invoke-super {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;->onPause()V

    .line 1983211
    const/16 v1, 0x2b

    const v2, 0x9b238b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x8e2dff9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983204
    invoke-super {p0}, Lcom/facebook/goodfriends/nux/NuxFragment;->onResume()V

    .line 1983205
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->p:Z

    .line 1983206
    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    sget-object v2, LX/9J1;->PICKER_OPEN:LX/9J1;

    invoke-virtual {v1, v2}, LX/9J2;->a(LX/9J1;)V

    .line 1983207
    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    const-string v2, "Audience Picker Opened"

    invoke-virtual {v1, v2}, LX/9J2;->a(Ljava/lang/String;)V

    .line 1983208
    const/16 v1, 0x2b

    const v2, 0x36d114bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1983173
    invoke-super {p0, p1, p2}, Lcom/facebook/goodfriends/nux/NuxFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1983174
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->b:LX/DIO;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->r:LX/DIC;

    .line 1983175
    new-instance v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17Y;

    invoke-static {v0}, LX/87o;->b(LX/0QB;)LX/87o;

    move-result-object v7

    check-cast v7, LX/87o;

    .line 1983176
    new-instance v10, LX/87q;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    const/16 v11, 0x15e7

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct {v10, v8, v9, v11}, LX/87q;-><init>(LX/0tX;Ljava/util/concurrent/Executor;LX/0Or;)V

    .line 1983177
    move-object v8, v10

    .line 1983178
    check-cast v8, LX/87q;

    invoke-static {v0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v9

    check-cast v9, LX/9J2;

    invoke-static {v0}, LX/87w;->b(LX/0QB;)LX/87w;

    move-result-object v10

    check-cast v10, LX/87w;

    invoke-static {v0}, LX/1b1;->b(LX/0QB;)LX/1b1;

    move-result-object v11

    check-cast v11, LX/1b1;

    move-object v12, v1

    invoke-direct/range {v3 .. v12}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/87o;LX/87q;LX/9J2;LX/87w;LX/1b1;LX/DIC;)V

    .line 1983179
    move-object v0, v3

    .line 1983180
    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983181
    const v0, 0x7f0d05f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 1983182
    const v0, 0x7f0d034e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->k:Lcom/facebook/resources/ui/FbEditText;

    .line 1983183
    const v0, 0x7f0d05f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->l:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1983184
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->l:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/DIF;

    invoke-direct {v1, p0}, LX/DIF;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1983185
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->k:Lcom/facebook/resources/ui/FbEditText;

    const/4 v4, 0x0

    .line 1983186
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02091d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1983187
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1983188
    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1983189
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->j:LX/3wu;

    .line 1983190
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->n:Landroid/os/Handler;

    .line 1983191
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->j:LX/3wu;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1983192
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1983193
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->j:LX/3wu;

    new-instance v1, LX/DIG;

    invoke-direct {v1, p0}, LX/DIG;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    .line 1983194
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 1983195
    const v0, 0x7f0d05f8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->q:Lcom/facebook/goodfriends/audience/AudienceFooterBar;

    .line 1983196
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->q:Lcom/facebook/goodfriends/audience/AudienceFooterBar;

    new-instance v1, LX/DIH;

    invoke-direct {v1, p0}, LX/DIH;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->setOnDeselectListener(Landroid/view/View$OnClickListener;)V

    .line 1983197
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->k:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DII;

    invoke-direct {v1, p0}, LX/DII;-><init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1983198
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->i:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    .line 1983199
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1983200
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983201
    iget-object v1, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->e:LX/87o;

    const/4 v2, 0x1

    iget-object v3, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->g:LX/87n;

    invoke-virtual {v1, v2, v3}, LX/87o;->a(ZLX/87n;)V

    .line 1983202
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFragment;->o:LX/4BY;

    .line 1983203
    return-void
.end method
