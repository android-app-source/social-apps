.class public Lcom/facebook/goodfriends/audience/AudienceActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1983069
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/goodfriends/audience/AudienceFragment;
    .locals 6

    .prologue
    .line 1983070
    new-instance v0, Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-direct {v0}, Lcom/facebook/goodfriends/audience/AudienceFragment;-><init>()V

    .line 1983071
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1983072
    const-string v2, "triggered_by_nux"

    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/AudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "triggered_by_nux"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1983073
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1983074
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1983075
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1983076
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1983077
    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/audience/AudienceActivity;->setContentView(Landroid/view/View;)V

    .line 1983078
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x22

    const v1, 0x324730db

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983079
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1983080
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x1020002

    invoke-direct {p0}, Lcom/facebook/goodfriends/audience/AudienceActivity;->a()Lcom/facebook/goodfriends/audience/AudienceFragment;

    move-result-object v3

    sget-object v4, Lcom/facebook/goodfriends/audience/AudienceFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1983081
    const/16 v1, 0x23

    const v2, -0x9e6ee2f

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
