.class public Lcom/facebook/goodfriends/audience/FriendItemView;
.super Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;
.source ""


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1983428
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodfriends/audience/FriendItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1983429
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1983430
    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1983431
    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/FriendItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02024b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->d:Landroid/graphics/drawable/Drawable;

    .line 1983432
    const v0, 0x7f0d063c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1983433
    const v0, 0x7f0d063e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1983434
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/FriendItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1983435
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1983436
    invoke-super {p0, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1983437
    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/FriendItemView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1983438
    :goto_0
    return-void

    .line 1983439
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1983440
    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/FriendItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1844

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1983441
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->d:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v3

    add-int/2addr v3, v0

    sub-int/2addr v3, v1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v5

    add-int/2addr v0, v5

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1983442
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/FriendItemView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method
