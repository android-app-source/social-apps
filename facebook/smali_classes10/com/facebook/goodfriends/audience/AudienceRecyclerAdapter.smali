.class public Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "Lcom/facebook/goodfriends/data/FriendData;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final e:LX/87o;

.field private final f:LX/87w;

.field public final g:LX/87n;

.field private final h:LX/87q;

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/1b1;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:I

.field private p:LX/9J2;

.field private q:LX/DIC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1983393
    const-class v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/87o;LX/87q;LX/9J2;LX/87w;LX/1b1;LX/DIC;)V
    .locals 1
    .param p9    # LX/DIC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1983378
    invoke-direct {p0, p1, p2, p3}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1983379
    new-instance v0, LX/DIL;

    invoke-direct {v0, p0}, LX/DIL;-><init>(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->g:LX/87n;

    .line 1983380
    iput-object p4, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->e:LX/87o;

    .line 1983381
    iput-object p5, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h:LX/87q;

    .line 1983382
    iput-object p6, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->p:LX/9J2;

    .line 1983383
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1983384
    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    .line 1983385
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1983386
    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    .line 1983387
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    .line 1983388
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    .line 1983389
    iput-object p7, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->f:LX/87w;

    .line 1983390
    iput-object p9, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->q:LX/DIC;

    .line 1983391
    iput-object p8, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->k:LX/1b1;

    .line 1983392
    return-void
.end method

.method public static h(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;I)V
    .locals 3

    .prologue
    .line 1983371
    iput p1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    .line 1983372
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->q:LX/DIC;

    if-eqz v0, :cond_0

    .line 1983373
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->q:LX/DIC;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1983374
    iget-object v2, v0, LX/DIC;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v2, v2, Lcom/facebook/goodfriends/audience/AudienceFragment;->q:Lcom/facebook/goodfriends/audience/AudienceFooterBar;

    invoke-virtual {v2, p1, v1}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->a(II)V

    .line 1983375
    iget-object p0, v0, LX/DIC;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    if-lez p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {p0, v2}, Lcom/facebook/goodfriends/nux/NuxFragment;->d(Z)V

    .line 1983376
    :cond_0
    return-void

    .line 1983377
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private i(I)Lcom/facebook/goodfriends/data/FriendData;
    .locals 2

    .prologue
    .line 1983368
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1983369
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1983370
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1983364
    if-nez p2, :cond_0

    .line 1983365
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03013a

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1983366
    new-instance v1, LX/DIM;

    invoke-direct {v1, p0, v0}, LX/DIM;-><init>(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;Landroid/view/View;)V

    move-object v0, v1

    .line 1983367
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->c(Landroid/view/ViewGroup;)LX/Aii;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 1983335
    if-nez p2, :cond_1

    .line 1983336
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1983337
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1983338
    const v2, 0x7f0823a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1983339
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1983340
    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1983341
    const v4, 0x7f0823aa

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1983342
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1983343
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1983344
    new-instance p1, LX/DIN;

    invoke-direct {p1, p0, v1}, LX/DIN;-><init>(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;Landroid/content/res/Resources;)V

    .line 1983345
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 p2, 0x21

    invoke-virtual {v4, p1, v2, v3, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1983346
    move-object v1, v4

    .line 1983347
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1983348
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1983349
    :cond_0
    :goto_0
    return-void

    .line 1983350
    :cond_1
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1983351
    invoke-direct {p0, p2}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i(I)Lcom/facebook/goodfriends/data/FriendData;

    move-result-object v1

    .line 1983352
    check-cast p1, LX/Aii;

    .line 1983353
    iget-object v0, p1, LX/Aii;->l:Landroid/view/View;

    move-object v0, v0

    .line 1983354
    check-cast v0, Lcom/facebook/goodfriends/audience/FriendItemView;

    .line 1983355
    iget-object v2, v1, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1983356
    invoke-virtual {v0, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarNameViewText(Ljava/lang/String;)V

    .line 1983357
    iget-object v2, v1, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    move-object v2, v2

    .line 1983358
    sget-object v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1983359
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1983360
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/FriendItemView;->setSelected(Z)V

    goto :goto_0

    .line 1983361
    :cond_2
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1983362
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/FriendItemView;->setSelected(Z)V

    goto :goto_0

    .line 1983363
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/goodfriends/data/FriendData;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/FriendItemView;->setSelected(Z)V

    goto :goto_0
.end method

.method public final a(LX/DIB;)V
    .locals 7

    .prologue
    .line 1983394
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1983395
    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1983396
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1983397
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1983398
    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h:LX/87q;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1983399
    new-instance v3, LX/88X;

    invoke-direct {v3}, LX/88X;-><init>()V

    move-object v4, v3

    .line 1983400
    new-instance v5, LX/4Ev;

    invoke-direct {v5}, LX/4Ev;-><init>()V

    iget-object v3, v2, LX/87q;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1983401
    const-string v6, "actor_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1983402
    move-object v3, v5

    .line 1983403
    iget-object v5, v4, LX/0gW;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1983404
    const-string v6, "client_mutation_id"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1983405
    move-object v3, v3

    .line 1983406
    const-string v5, "GOOD_FRIENDS"

    .line 1983407
    const-string v6, "list_type"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1983408
    move-object v5, v3

    .line 1983409
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1983410
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/data/FriendData;

    .line 1983411
    iget-object v0, v3, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    move-object v3, v0

    .line 1983412
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1983413
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1983414
    const-string v6, "add_ids"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1983415
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1983416
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/data/FriendData;

    .line 1983417
    iget-object v0, v3, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    move-object v3, v0

    .line 1983418
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1983419
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1983420
    const-string v6, "remove_ids"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1983421
    const-string v3, "input"

    invoke-virtual {v4, v3, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1983422
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1983423
    iget-object v4, v2, LX/87q;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v3, v3

    .line 1983424
    new-instance v4, LX/87p;

    invoke-direct {v4, v2, p1}, LX/87p;-><init>(LX/87q;LX/DIB;)V

    iget-object v5, v2, LX/87q;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1983425
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 1983317
    if-nez p2, :cond_0

    .line 1983318
    :goto_0
    return-void

    .line 1983319
    :cond_0
    invoke-direct {p0, p2}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i(I)Lcom/facebook/goodfriends/data/FriendData;

    move-result-object v1

    .line 1983320
    check-cast p1, Lcom/facebook/goodfriends/audience/FriendItemView;

    .line 1983321
    invoke-virtual {p1}, Lcom/facebook/goodfriends/audience/FriendItemView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/facebook/goodfriends/audience/FriendItemView;->setSelected(Z)V

    .line 1983322
    invoke-virtual {p1}, Lcom/facebook/goodfriends/audience/FriendItemView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1983323
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1983324
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1983325
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1983326
    iget v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;I)V

    .line 1983327
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->p:LX/9J2;

    sget-object v2, LX/9J1;->PICKER_SELECT:LX/9J1;

    invoke-virtual {v0, v2}, LX/9J2;->a(LX/9J1;)V

    .line 1983328
    :goto_2
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    .line 1983329
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1983330
    :cond_3
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1983331
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1983332
    :cond_4
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1983333
    iget v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;I)V

    .line 1983334
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->p:LX/9J2;

    sget-object v2, LX/9J1;->PICKER_DESELECT:LX/9J1;

    invoke-virtual {v0, v2}, LX/9J2;->a(LX/9J1;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1983308
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1983309
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1983310
    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    .line 1983311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->n:Ljava/lang/String;

    .line 1983312
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1983313
    :goto_0
    return-void

    .line 1983314
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->n:Ljava/lang/String;

    .line 1983315
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->f:LX/87w;

    iget-object v1, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    iget-object v2, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/87w;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    .line 1983316
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1983307
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030139

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1983306
    const-string v0, ""

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1983303
    if-nez p1, :cond_0

    .line 1983304
    const/4 v0, 0x0

    .line 1983305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1983302
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
