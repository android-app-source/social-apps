.class public Lcom/facebook/goodfriends/audience/AudienceFooterBar;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1983082
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1983083
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1983101
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1983102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1983098
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1983099
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->a(Landroid/content/Context;)V

    .line 1983100
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1983092
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1983093
    const v1, 0x7f030135

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1983094
    const v0, 0x7f0d05f3

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1983095
    const v0, 0x7f0d05f4

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1983096
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->c:I

    .line 1983097
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    .prologue
    .line 1983088
    iget v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1983089
    :goto_0
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0823c2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1983090
    return-void

    .line 1983091
    :cond_0
    iget v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->c:I

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0
.end method

.method public setMaxAudienceCount(I)V
    .locals 0

    .prologue
    .line 1983086
    iput p1, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->c:I

    .line 1983087
    return-void
.end method

.method public setOnDeselectListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1983084
    iget-object v0, p0, Lcom/facebook/goodfriends/audience/AudienceFooterBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1983085
    return-void
.end method
