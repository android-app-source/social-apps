.class public Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2ea;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D3l;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/graphics/Paint;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Landroid/widget/ImageView;

.field private j:I

.field public k:I

.field public l:Z

.field public m:Z

.field private final n:Landroid/view/View$OnClickListener;

.field public o:LX/BiJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1961079
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1961080
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 1961081
    iput v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->j:I

    .line 1961082
    iput v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    .line 1961083
    new-instance v0, LX/D3n;

    invoke-direct {v0, p0}, LX/D3n;-><init>(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->n:Landroid/view/View$OnClickListener;

    .line 1961084
    invoke-direct {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b()V

    .line 1961085
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1961086
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1961087
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 1961088
    iput v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->j:I

    .line 1961089
    iput v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    .line 1961090
    new-instance v0, LX/D3n;

    invoke-direct {v0, p0}, LX/D3n;-><init>(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->n:Landroid/view/View$OnClickListener;

    .line 1961091
    sget-object v0, LX/03r;->PlutoniumContextualItemView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1961092
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->j:I

    .line 1961093
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1961094
    invoke-direct {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b()V

    .line 1961095
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;)LX/1Up;
    .locals 1

    .prologue
    .line 1961096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    if-ne p0, v0, :cond_0

    .line 1961097
    sget-object v0, LX/1Up;->f:LX/1Up;

    .line 1961098
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1Up;->g:LX/1Up;

    goto :goto_0
.end method

.method private a(IIIILandroid/net/Uri;Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1961099
    new-instance v0, LX/1au;

    invoke-direct {v0, p1, p1}, LX/1au;-><init>(II)V

    .line 1961100
    iput p2, v0, LX/1au;->d:I

    .line 1961101
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1au;->a:Z

    .line 1961102
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1961103
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-static {p6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;)LX/1Up;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 1961104
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const-class v1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    const-string v2, "about"

    invoke-static {v1, p7, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1961105
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1961106
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p3, p4, p3, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1961107
    return-void
.end method

.method private a(IIIILandroid/net/Uri;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1961108
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILandroid/net/Uri;Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;Ljava/lang/String;)V

    .line 1961109
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/Integer;I)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1961064
    if-eqz p1, :cond_0

    .line 1961065
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1961066
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1961067
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 1961068
    iput p2, v0, LX/1au;->d:I

    .line 1961069
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1961070
    :goto_0
    return-void

    .line 1961071
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a:LX/0Or;

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1961110
    const-class v0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    invoke-static {v0, p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1961111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setOrientation(I)V

    .line 1961112
    const v0, 0x7f030387

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1961113
    const v0, 0x7f0a0048

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setBackgroundResource(I)V

    .line 1961114
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1961115
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f0102c6

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1961116
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-nez v0, :cond_1

    const v0, 0x7f0b00b2

    .line 1961117
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1961118
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v3, 0x7f0102c5

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1961119
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-nez v0, :cond_2

    const v0, 0x7f0b00b1

    .line 1961120
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1961121
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0e87

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v3, v2, 0x2

    add-int/2addr v0, v3

    .line 1961122
    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 1961123
    invoke-virtual {p0, v1, v2, v1, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setPadding(IIII)V

    .line 1961124
    const v0, 0x7f0d0b5a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1961125
    const v0, 0x7f0d0b5b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1961126
    const v0, 0x7f0d0b5c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1961127
    const v0, 0x7f0d0b5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1961128
    const v0, 0x7f0d0b5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1961129
    const v0, 0x7f0d0b5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->i:Landroid/widget/ImageView;

    .line 1961130
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1961131
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1961132
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->g:Landroid/graphics/Paint;

    .line 1961133
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0543

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1961134
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->g:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1961135
    iget v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->j:I

    if-ne v0, v4, :cond_0

    .line 1961136
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1961137
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1961138
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setBackgroundResource(I)V

    .line 1961139
    :cond_0
    return-void

    .line 1961140
    :cond_1
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    goto/16 :goto_0

    .line 1961141
    :cond_2
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    goto/16 :goto_1
.end method

.method private getMoreChevronClickListenerWrapper()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1961142
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->p:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 1961143
    new-instance v0, LX/D3o;

    invoke-direct {v0, p0}, LX/D3o;-><init>(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->p:Landroid/view/View$OnClickListener;

    .line 1961144
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->p:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final a(IIIILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1961145
    new-instance v0, LX/1au;

    invoke-direct {v0, p1, p1}, LX/1au;-><init>(II)V

    .line 1961146
    const/16 v1, 0x10

    iput v1, v0, LX/1au;->d:I

    .line 1961147
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1au;->a:Z

    .line 1961148
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1961149
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const-class v1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    const-string v2, "about"

    invoke-static {v1, p5, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1961150
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1961151
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2, p3, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1961152
    return-void
.end method

.method public final a(IIILandroid/net/Uri;Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1961153
    const/16 v2, 0x10

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILandroid/net/Uri;Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;Ljava/lang/String;)V

    .line 1961154
    return-void
.end method

.method public final a(IIILandroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1961155
    const/16 v2, 0x10

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILandroid/net/Uri;Ljava/lang/String;)V

    .line 1961156
    return-void
.end method

.method public final a(ILjava/lang/CharSequence;II)V
    .locals 1

    .prologue
    .line 1961157
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1961158
    invoke-virtual {p0, p2, p3, p4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 1961159
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1961160
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 1961161
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 1961162
    const/16 v1, 0x10

    iput v1, v0, LX/1au;->d:I

    .line 1961163
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;II)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1961072
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    int-to-float v2, p3

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 1961073
    iget-object v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setSingleLine(Z)V

    .line 1961074
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1961075
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1961076
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1961077
    return-void

    .line 1961078
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1961013
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;I)V

    .line 1961014
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1961015
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1961016
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1961017
    :goto_0
    return-void

    .line 1961018
    :cond_1
    iget-object v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    int-to-float v3, p3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 1961019
    iget-object v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1961020
    iget-object v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    if-ne p2, v0, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setSingleLine(Z)V

    .line 1961021
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1961022
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1961023
    goto :goto_1
.end method

.method public final a(ZLX/BiJ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1961024
    if-eqz p1, :cond_0

    .line 1961025
    iput-object p2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->o:LX/BiJ;

    .line 1961026
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1961027
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->i:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getMoreChevronClickListenerWrapper()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1961028
    :goto_0
    return-void

    .line 1961029
    :cond_0
    iput-object v2, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->o:LX/BiJ;

    .line 1961030
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1961031
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(ZLjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1961032
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->n:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1961033
    invoke-virtual {p0, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(Ljava/lang/Object;)V

    .line 1961034
    iget v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->j:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 1961035
    if-eqz p1, :cond_1

    const v0, 0x7f0214bf

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setBackgroundResource(I)V

    .line 1961036
    :goto_2
    return-void

    .line 1961037
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1961038
    :cond_1
    const v0, 0x7f0a0048

    goto :goto_1

    .line 1961039
    :cond_2
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1961040
    iget-boolean v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->m:Z

    return v0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1961041
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0, v0, p1, v1, p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setPadding(IIII)V

    .line 1961042
    return-void
.end method

.method public getUnformattedSubtitleApplication()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1961043
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081773

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnformattedSubtitleDatetimeApplication()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1961044
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081772

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x24c2e94e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961045
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1961046
    const/4 v1, 0x1

    .line 1961047
    iput-boolean v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->m:Z

    .line 1961048
    const/16 v1, 0x2d

    const v2, -0x685df0c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x40ecce92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961049
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1961050
    const/4 v1, 0x0

    .line 1961051
    iput-boolean v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->m:Z

    .line 1961052
    const/16 v1, 0x2d

    const v2, -0x687e069a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1961053
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1961054
    iget-boolean v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    if-eqz v0, :cond_0

    .line 1961055
    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1961056
    iget v1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1961057
    :cond_0
    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 1961058
    iput-boolean p1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->m:Z

    .line 1961059
    return-void
.end method

.method public setSeparatorVisibility(Z)V
    .locals 0

    .prologue
    .line 1961060
    iput-boolean p1, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 1961061
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 1

    .prologue
    .line 1961062
    iget-object v0, p0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1961063
    return-void
.end method
