.class public Lcom/facebook/uicontrib/error/AlertView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/EllipsizingTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2118655
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2118656
    invoke-direct {p0}, Lcom/facebook/uicontrib/error/AlertView;->a()V

    .line 2118657
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2118616
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2118617
    invoke-direct {p0}, Lcom/facebook/uicontrib/error/AlertView;->a()V

    .line 2118618
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2118652
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118653
    invoke-direct {p0}, Lcom/facebook/uicontrib/error/AlertView;->a()V

    .line 2118654
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2118645
    const v0, 0x7f0300d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2118646
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0522

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/uicontrib/error/AlertView;->a:LX/0zw;

    .line 2118647
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0524

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/uicontrib/error/AlertView;->b:LX/0zw;

    .line 2118648
    const v0, 0x7f0d0523

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->d:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2118649
    const v0, 0x7f0d0525

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2118650
    invoke-virtual {p0}, Lcom/facebook/uicontrib/error/AlertView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->g:Landroid/content/res/Resources;

    .line 2118651
    return-void
.end method


# virtual methods
.method public setMessage(I)V
    .locals 1

    .prologue
    .line 2118643
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->setMessage(Ljava/lang/CharSequence;)V

    .line 2118644
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2118641
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->d:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2118642
    return-void
.end method

.method public setPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2118639
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2118640
    return-void
.end method

.method public setPrimaryButtonText(I)V
    .locals 1

    .prologue
    .line 2118658
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->setPrimaryButtonText(Ljava/lang/String;)V

    .line 2118659
    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2118637
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2118638
    return-void
.end method

.method public setSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2118633
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2118634
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_0

    .line 2118635
    :goto_0
    return-void

    .line 2118636
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setSecondaryButtonText(I)V
    .locals 1

    .prologue
    .line 2118631
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->setSecondaryButtonText(Ljava/lang/String;)V

    .line 2118632
    return-void
.end method

.method public setSecondaryButtonText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2118626
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2118627
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_0

    .line 2118628
    :goto_0
    return-void

    .line 2118629
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2118630
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2118624
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/error/AlertView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2118625
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2118619
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2118620
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->c:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_0

    .line 2118621
    :goto_0
    return-void

    .line 2118622
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2118623
    iget-object v0, p0, Lcom/facebook/uicontrib/error/AlertView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
