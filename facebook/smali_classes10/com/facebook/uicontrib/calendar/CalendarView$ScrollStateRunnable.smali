.class public final Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/uicontrib/calendar/CalendarView;

.field private b:Landroid/widget/AbsListView;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V
    .locals 0

    .prologue
    .line 1960191
    iput-object p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/uicontrib/calendar/CalendarView;B)V
    .locals 0

    .prologue
    .line 1960171
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/AbsListView;I)V
    .locals 4

    .prologue
    .line 1960186
    iput-object p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->b:Landroid/widget/AbsListView;

    .line 1960187
    iput p2, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->c:I

    .line 1960188
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0, p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1960189
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, p0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1960190
    return-void
.end method

.method public final run()V
    .locals 4

    .prologue
    const/16 v3, 0x1f4

    .line 1960172
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->c:I

    .line 1960173
    iput v1, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->M:I

    .line 1960174
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->L:I

    if-eqz v0, :cond_1

    .line 1960175
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->b:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1960176
    if-nez v0, :cond_0

    .line 1960177
    :goto_0
    return-void

    .line 1960178
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    sub-int/2addr v1, v2

    .line 1960179
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    if-le v1, v2, :cond_1

    .line 1960180
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-eqz v2, :cond_2

    .line 1960181
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {v2, v0, v3}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    .line 1960182
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->c:I

    .line 1960183
    iput v1, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->L:I

    .line 1960184
    goto :goto_0

    .line 1960185
    :cond_2
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    goto :goto_1
.end method
