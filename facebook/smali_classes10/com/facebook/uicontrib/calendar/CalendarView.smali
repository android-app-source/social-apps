.class public Lcom/facebook/uicontrib/calendar/CalendarView;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public A:LX/D3k;

.field public B:Landroid/widget/ListView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/view/ViewGroup;

.field private E:Landroid/widget/ImageView;

.field private F:[Ljava/lang/String;

.field private G:[Ljava/lang/String;

.field public H:I

.field private I:I

.field private J:J

.field public K:Z

.field public L:I

.field public M:I

.field public N:LX/BmE;

.field private O:Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;

.field public P:LX/D3g;

.field public Q:Ljava/util/Calendar;

.field private R:Ljava/util/Calendar;

.field public S:Ljava/util/Calendar;

.field public T:Ljava/util/Calendar;

.field private final U:Ljava/text/DateFormat;

.field private V:Ljava/util/Locale;

.field public W:Z

.field public aa:J

.field public ab:Ljava/util/Calendar;

.field public ac:I

.field private b:Ljava/text/DateFormatSymbols;

.field public final c:I

.field public final d:I

.field public e:I

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:Landroid/graphics/drawable/Drawable;

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:Z

.field public m:I

.field public n:I

.field private o:I

.field private p:I

.field private q:I

.field public r:I

.field public s:I

.field private t:I

.field private u:I

.field public v:I

.field public w:Z

.field public x:I

.field private y:F

.field private z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1960909
    const-class v0, Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/uicontrib/calendar/CalendarView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1960906
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1960907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1960904
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1960905
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1960833
    invoke-direct {p0, p1, p2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1960834
    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->r:I

    .line 1960835
    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    .line 1960836
    const/16 v0, 0xc

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->t:I

    .line 1960837
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->u:I

    .line 1960838
    const/4 v0, 0x7

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    .line 1960839
    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->y:F

    .line 1960840
    const v0, 0x3eaa7efa    # 0.333f

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->z:F

    .line 1960841
    iput v6, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    .line 1960842
    iput-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    .line 1960843
    iput v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->L:I

    .line 1960844
    iput v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->M:I

    .line 1960845
    new-instance v0, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->O:Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;

    .line 1960846
    sget-object v0, LX/D3g;->MINUTES:LX/D3g;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->P:LX/D3g;

    .line 1960847
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd/yyyy"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->U:Ljava/text/DateFormat;

    .line 1960848
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->aa:J

    .line 1960849
    iput v6, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    .line 1960850
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->setCurrentLocale(Ljava/util/Locale;)V

    .line 1960851
    sget-object v0, LX/03r;->CalendarView:[I

    const v2, 0x7f0102c7

    const v3, 0x7f0e0a5c

    invoke-virtual {p1, p2, v0, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1960852
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    .line 1960853
    const/16 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    .line 1960854
    const/16 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1960855
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-direct {p0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1960856
    :cond_0
    const-string v2, "01/01/1900"

    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-direct {p0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/lang/String;Ljava/util/Calendar;)Z

    .line 1960857
    :cond_1
    const/16 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1960858
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-direct {p0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1960859
    :cond_2
    const-string v2, "01/01/2100"

    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-direct {p0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/lang/String;Ljava/util/Calendar;)Z

    .line 1960860
    :cond_3
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1960861
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Max date cannot be before min date."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1960862
    :cond_4
    const/16 v2, 0x4

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    .line 1960863
    const/16 v2, 0x5

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->h:I

    .line 1960864
    const/16 v2, 0x6

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->i:I

    .line 1960865
    const/16 v2, 0x7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    .line 1960866
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->k:I

    .line 1960867
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->l:Z

    .line 1960868
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->m:I

    .line 1960869
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    .line 1960870
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1dd7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->d:I

    .line 1960871
    const/16 v2, 0xe

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->q:I

    .line 1960872
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->c()V

    .line 1960873
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    .line 1960874
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    .line 1960875
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1960876
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1960877
    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v5, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->t:I

    .line 1960878
    invoke-static {v5, v7, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->r:I

    .line 1960879
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v5, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    .line 1960880
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v5, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->u:I

    .line 1960881
    iget-boolean v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->l:Z

    if-eqz v2, :cond_5

    invoke-static {v5, v7, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_0
    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    .line 1960882
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0203a7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->f:Landroid/graphics/drawable/Drawable;

    .line 1960883
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0203a8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->g:Landroid/graphics/drawable/Drawable;

    .line 1960884
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1960885
    const v2, 0x7f03021e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1960886
    invoke-virtual {p0, v2}, Lcom/facebook/uicontrib/calendar/CalendarView;->addView(Landroid/view/View;)V

    .line 1960887
    const v0, 0x7f0d0846

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    .line 1960888
    const v0, 0x7f0d0844

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->D:Landroid/view/ViewGroup;

    .line 1960889
    const v0, 0x7f0d0845

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->E:Landroid/widget/ImageView;

    .line 1960890
    const v0, 0x7f0d0843

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->C:Landroid/widget/TextView;

    .line 1960891
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->f()V

    .line 1960892
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->g()V

    .line 1960893
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->e()V

    .line 1960894
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960895
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960896
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1960897
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-static {p0, v0, v1, v5, v5}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    .line 1960898
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->invalidate()V

    .line 1960899
    return-void

    :cond_5
    move v0, v1

    .line 1960900
    goto/16 :goto_0

    .line 1960901
    :cond_6
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1960902
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-static {p0, v0, v1, v5, v5}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    goto :goto_1

    .line 1960903
    :cond_7
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {p0, v0, v1, v5, v5}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    goto :goto_1
.end method

.method private static a(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 1960827
    if-nez p0, :cond_0

    .line 1960828
    invoke-static {p1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 1960829
    :goto_0
    return-object v0

    .line 1960830
    :cond_0
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1960831
    invoke-static {p1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 1960832
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method private a(JZZ)V
    .locals 3

    .prologue
    .line 1960822
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960823
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960824
    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v1, v1, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v1, v1, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960825
    :goto_0
    return-void

    .line 1960826
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-static {p0, v0, p3, v1, p4}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/uicontrib/calendar/CalendarView;Landroid/widget/AbsListView;)V
    .locals 10

    .prologue
    const/16 v9, 0xb

    const/4 v8, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1960793
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960794
    if-nez v0, :cond_1

    .line 1960795
    :cond_0
    :goto_0
    return-void

    .line 1960796
    :cond_1
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    invoke-virtual {v0}, LX/D3i;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    invoke-virtual {v0}, LX/D3i;->getBottom()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-long v4, v3

    .line 1960797
    iget-wide v6, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->J:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_7

    .line 1960798
    iput-boolean v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    .line 1960799
    :goto_1
    invoke-virtual {v0}, LX/D3i;->getBottom()I

    move-result v3

    iget v6, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->t:I

    if-ge v3, v6, :cond_2

    move v1, v2

    .line 1960800
    :cond_2
    iget-boolean v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-eqz v3, :cond_8

    .line 1960801
    add-int/lit8 v0, v1, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960802
    :cond_3
    :goto_2
    iget-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-eqz v1, :cond_9

    .line 1960803
    iget v1, v0, LX/D3i;->j:I

    move v1, v1

    .line 1960804
    :goto_3
    iget v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    if-ne v3, v9, :cond_a

    if-nez v1, :cond_a

    .line 1960805
    :goto_4
    iget-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-nez v1, :cond_4

    if-gtz v2, :cond_5

    :cond_4
    iget-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-eqz v1, :cond_6

    if-gez v2, :cond_6

    .line 1960806
    :cond_5
    iget-object v1, v0, LX/D3i;->i:Ljava/util/Calendar;

    move-object v0, v1

    .line 1960807
    iget-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    if-eqz v1, :cond_c

    .line 1960808
    const/4 v1, -0x7

    invoke-virtual {v0, v8, v1}, Ljava/util/Calendar;->add(II)V

    .line 1960809
    :goto_5
    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960810
    :cond_6
    iput-wide v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->J:J

    .line 1960811
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->M:I

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->L:I

    goto :goto_0

    .line 1960812
    :cond_7
    iget-wide v6, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->J:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 1960813
    iput-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->K:Z

    goto :goto_1

    .line 1960814
    :cond_8
    if-eqz v1, :cond_3

    .line 1960815
    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    goto :goto_2

    .line 1960816
    :cond_9
    iget v1, v0, LX/D3i;->k:I

    move v1, v1

    .line 1960817
    goto :goto_3

    .line 1960818
    :cond_a
    iget v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    if-nez v2, :cond_b

    if-ne v1, v9, :cond_b

    .line 1960819
    const/4 v2, -0x1

    goto :goto_4

    .line 1960820
    :cond_b
    iget v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    sub-int v2, v1, v2

    goto :goto_4

    .line 1960821
    :cond_c
    const/4 v1, 0x7

    invoke-virtual {v0, v8, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_5
.end method

.method public static a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1960731
    if-nez p1, :cond_0

    .line 1960732
    :goto_0
    return-void

    .line 1960733
    :cond_0
    sget-object v0, LX/D3f;->a:[I

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->P:LX/D3g;

    invoke-virtual {v1}, LX/D3g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1960734
    :pswitch_0
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 1960735
    :pswitch_1
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 1960736
    :pswitch_2
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/util/Calendar;)Z
    .locals 3

    .prologue
    .line 1960787
    :try_start_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->U:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1960788
    const/4 v0, 0x1

    .line 1960789
    :goto_0
    return v0

    .line 1960790
    :catch_0
    sget-object v0, Lcom/facebook/uicontrib/calendar/CalendarView;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Date: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not in format: MM/dd/yyyy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1960791
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v0, 0x1

    .line 1960786
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 1960784
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->O:Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/uicontrib/calendar/CalendarView$ScrollStateRunnable;->a(Landroid/widget/AbsListView;I)V

    .line 1960785
    return-void
.end method

.method public static a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1960756
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1960757
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Time not between "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1960758
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1960759
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1960760
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    if-gez v2, :cond_2

    .line 1960761
    add-int/lit8 v0, v0, 0x1

    .line 1960762
    :cond_2
    iget v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    .line 1960763
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->u:I

    if-le v3, v4, :cond_3

    .line 1960764
    add-int/lit8 v2, v2, -0x1

    .line 1960765
    :cond_3
    if-eqz p3, :cond_4

    .line 1960766
    iget-boolean v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v3, :cond_7

    .line 1960767
    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v3, p1, v1}, LX/D3k;->b(Ljava/util/Calendar;Z)V

    .line 1960768
    :cond_4
    :goto_0
    invoke-static {p0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v3

    .line 1960769
    if-lt v3, v0, :cond_5

    if-gt v3, v2, :cond_5

    if-eqz p4, :cond_a

    .line 1960770
    :cond_5
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960771
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1960772
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960773
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 1960774
    :goto_1
    const/4 v2, 0x2

    iput v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->L:I

    .line 1960775
    if-eqz p2, :cond_9

    .line 1960776
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    iget v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    const/16 v3, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(III)V

    .line 1960777
    :cond_6
    :goto_2
    return-void

    .line 1960778
    :cond_7
    iget-object v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v3, p1, v1}, LX/D3k;->a(Ljava/util/Calendar;Z)V

    goto :goto_0

    .line 1960779
    :cond_8
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    goto :goto_1

    .line 1960780
    :cond_9
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    iget v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->s:I

    invoke-virtual {v2, v0, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 1960781
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-static {p0, v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Landroid/widget/AbsListView;I)V

    goto :goto_2

    .line 1960782
    :cond_a
    if-eqz p3, :cond_6

    .line 1960783
    invoke-static {p0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    goto :goto_2
.end method

.method private b()I
    .locals 2

    .prologue
    .line 1960753
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->V:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960754
    const/4 v0, 0x2

    .line 1960755
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I
    .locals 8

    .prologue
    .line 1960747
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960748
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fromDate: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not precede toDate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1960749
    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 1960750
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 1960751
    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget v5, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    .line 1960752
    sub-long/2addr v0, v2

    add-long/2addr v0, v4

    const-wide/32 v2, 0x240c8400

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1960743
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->q:I

    sget-object v2, LX/03r;->TextAppearance:[I

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1960744
    const/16 v1, 0x0

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->e:I

    .line 1960745
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1960746
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1960737
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    .line 1960738
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1960739
    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1960740
    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 1960741
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1960742
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1960910
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    if-nez v0, :cond_0

    .line 1960911
    new-instance v0, LX/D3k;

    invoke-direct {v0, p0}, LX/D3k;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    .line 1960912
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    new-instance v1, LX/D3d;

    invoke-direct {v1, p0}, LX/D3d;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V

    invoke-virtual {v0, v1}, LX/D3k;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1960913
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1960914
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    const v1, 0x312e728a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1960915
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 1960955
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->F:[Ljava/lang/String;

    .line 1960956
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->G:[Ljava/lang/String;

    .line 1960957
    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    iget v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    add-int/2addr v3, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1960958
    const/4 v0, 0x7

    if-le v1, v0, :cond_0

    add-int/lit8 v0, v1, -0x7

    .line 1960959
    :goto_1
    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->F:[Ljava/lang/String;

    iget v5, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    sub-int v5, v1, v5

    const/16 v6, 0x32

    invoke-static {v0, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1960960
    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->G:[Ljava/lang/String;

    iget v5, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    sub-int v5, v1, v5

    const/16 v6, 0xa

    invoke-static {v0, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 1960961
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1960962
    goto :goto_1

    .line 1960963
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1960964
    iget-boolean v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v1, :cond_3

    .line 1960965
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1960966
    :goto_2
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v1, v2

    .line 1960967
    :goto_3
    add-int/lit8 v0, v3, -0x1

    if-ge v1, v0, :cond_5

    .line 1960968
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->D:Landroid/view/ViewGroup;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1960969
    iget v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    if-ltz v4, :cond_2

    .line 1960970
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1960971
    :cond_2
    iget v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    if-ge v1, v4, :cond_4

    .line 1960972
    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->F:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1960973
    iget-object v4, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->G:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1960974
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1960975
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1960976
    :cond_3
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 1960977
    :cond_4
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 1960978
    :cond_5
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 1960979
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1960948
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1960949
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 1960950
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 1960951
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    new-instance v1, LX/D3e;

    invoke-direct {v1, p0}, LX/D3e;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1960952
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->y:F

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFriction(F)V

    .line 1960953
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->z:F

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVelocityScale(F)V

    .line 1960954
    return-void
.end method

.method public static h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1960947
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 1960939
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->V:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960940
    :goto_0
    return-void

    .line 1960941
    :cond_0
    iput-object p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->V:Ljava/util/Locale;

    .line 1960942
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0, p1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->b:Ljava/text/DateFormatSymbols;

    .line 1960943
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {v0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    .line 1960944
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    invoke-static {v0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    .line 1960945
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-static {v0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    .line 1960946
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-static {v0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    goto :goto_0
.end method

.method public static setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V
    .locals 7

    .prologue
    .line 1960930
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    .line 1960931
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->I:I

    invoke-virtual {v0, v1}, LX/D3k;->a(I)V

    .line 1960932
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1960933
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v6, 0x34

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    .line 1960934
    iget v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    if-ltz v1, :cond_0

    .line 1960935
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->C:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1960936
    :cond_0
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1960937
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->C:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 1960938
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1960927
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    if-eqz v0, :cond_0

    .line 1960928
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v0}, LX/D3k;->b()V

    .line 1960929
    :cond_0
    return-void
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 1960922
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 1960923
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    .line 1960924
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 1960925
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 1960926
    return-void
.end method

.method public getDate()J
    .locals 2

    .prologue
    .line 1960921
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v0, v0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDateTextAppearance()I
    .locals 1

    .prologue
    .line 1960920
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->q:I

    return v0
.end method

.method public getFirstDayOfWeek()I
    .locals 1

    .prologue
    .line 1960730
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    return v0
.end method

.method public getFocusedMonthDateColor()I
    .locals 1

    .prologue
    .line 1960919
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    return v0
.end method

.method public getMaxDate()J
    .locals 2

    .prologue
    .line 1960918
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinDate()J
    .locals 2

    .prologue
    .line 1960917
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinSelectableDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 1960916
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    return-object v0
.end method

.method public getMonthTextAppearance()I
    .locals 1

    .prologue
    .line 1960792
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    return v0
.end method

.method public getSecondDate()J
    .locals 2

    .prologue
    .line 1960908
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v0, v0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedWeekBackgroundColor()I
    .locals 1

    .prologue
    .line 1960638
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->h:I

    return v0
.end method

.method public getShowWeekNumber()Z
    .locals 1

    .prologue
    .line 1960637
    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    return v0
.end method

.method public getShownWeekCount()I
    .locals 1

    .prologue
    .line 1960636
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    return v0
.end method

.method public getUnfocusedMonthDateColor()I
    .locals 1

    .prologue
    .line 1960635
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    return v0
.end method

.method public getWeekDayTextAppearance()I
    .locals 1

    .prologue
    .line 1960634
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    return v0
.end method

.method public getWeekNumberColor()I
    .locals 1

    .prologue
    .line 1960633
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    return v0
.end method

.method public getWeekSeparatorLineColor()I
    .locals 1

    .prologue
    .line 1960632
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->m:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1960631
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1960628
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1960629
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->setCurrentLocale(Ljava/util/Locale;)V

    .line 1960630
    return-void
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1960625
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1960626
    const-class v0, Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1960627
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1960622
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1960623
    const-class v0, Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1960624
    return-void
.end method

.method public setDate(J)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1960620
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(JZZ)V

    .line 1960621
    return-void
.end method

.method public setDateTextAppearance(I)V
    .locals 1

    .prologue
    .line 1960615
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->q:I

    if-eq v0, p1, :cond_0

    .line 1960616
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->q:I

    .line 1960617
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->c()V

    .line 1960618
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->d()V

    .line 1960619
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1960613
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1960614
    return-void
.end method

.method public setFirstDayOfWeek(I)V
    .locals 1

    .prologue
    .line 1960608
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    if-ne v0, p1, :cond_0

    .line 1960609
    :goto_0
    return-void

    .line 1960610
    :cond_0
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    .line 1960611
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-static {v0}, LX/D3k;->f(LX/D3k;)V

    .line 1960612
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->f()V

    goto :goto_0
.end method

.method public setFocusedMonthDateColor(I)V
    .locals 4

    .prologue
    .line 1960599
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    if-eq v0, p1, :cond_1

    .line 1960600
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    .line 1960601
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 1960602
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1960603
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960604
    iget-boolean v3, v0, LX/D3i;->g:Z

    if-eqz v3, :cond_0

    .line 1960605
    invoke-virtual {v0}, LX/D3i;->invalidate()V

    .line 1960606
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1960607
    :cond_1
    return-void
.end method

.method public setIsSelectingSecondDate(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1960639
    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eq v0, p1, :cond_2

    const/4 v0, 0x1

    .line 1960640
    :goto_0
    iput-boolean p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    .line 1960641
    if-eqz v0, :cond_4

    .line 1960642
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 1960643
    :goto_1
    if-ge v1, v2, :cond_3

    .line 1960644
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960645
    iget-boolean v3, v0, LX/D3i;->o:Z

    if-nez v3, :cond_0

    iget-boolean v3, v0, LX/D3i;->p:Z

    if-eqz v3, :cond_1

    .line 1960646
    :cond_0
    invoke-virtual {v0}, LX/D3i;->invalidate()V

    .line 1960647
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1960648
    goto :goto_0

    .line 1960649
    :cond_3
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v0}, LX/D3k;->c()V

    .line 1960650
    :cond_4
    return-void
.end method

.method public setMaxDate(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1960651
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960652
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960653
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960654
    :goto_0
    return-void

    .line 1960655
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960656
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-static {v0}, LX/D3k;->f(LX/D3k;)V

    .line 1960657
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v0, v0, LX/D3k;->b:Ljava/util/Calendar;

    .line 1960658
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1960659
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->setDate(J)V

    goto :goto_0

    .line 1960660
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v0, v2, v1, v2}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    goto :goto_0
.end method

.method public setMaxDateRangeDistanceInMilliseconds(J)V
    .locals 1

    .prologue
    .line 1960661
    iput-wide p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->aa:J

    .line 1960662
    return-void
.end method

.method public setMinDate(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1960663
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960664
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960665
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960666
    :goto_0
    return-void

    .line 1960667
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960668
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v0, v0, LX/D3k;->b:Ljava/util/Calendar;

    .line 1960669
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1960670
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    iget-object v2, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, LX/D3k;->a(Ljava/util/Calendar;Z)V

    .line 1960671
    :cond_1
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-static {v1}, LX/D3k;->f(LX/D3k;)V

    .line 1960672
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1960673
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->setDate(J)V

    goto :goto_0

    .line 1960674
    :cond_2
    const/4 v1, 0x1

    invoke-static {p0, v0, v3, v1, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    goto :goto_0
.end method

.method public setMonthTextAppearance(I)V
    .locals 1

    .prologue
    .line 1960675
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    if-eq v0, p1, :cond_0

    .line 1960676
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->o:I

    .line 1960677
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->R:Ljava/util/Calendar;

    invoke-static {p0, v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960678
    :cond_0
    return-void
.end method

.method public setOnDateChangeListener(LX/BmE;)V
    .locals 0

    .prologue
    .line 1960679
    iput-object p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->N:LX/BmE;

    .line 1960680
    return-void
.end method

.method public setSelectedWeekBackgroundColor(I)V
    .locals 4

    .prologue
    .line 1960681
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->h:I

    if-eq v0, p1, :cond_1

    .line 1960682
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->h:I

    .line 1960683
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 1960684
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1960685
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960686
    iget-boolean v3, v0, LX/D3i;->o:Z

    if-eqz v3, :cond_0

    .line 1960687
    invoke-virtual {v0}, LX/D3i;->invalidate()V

    .line 1960688
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1960689
    :cond_1
    return-void
.end method

.method public setShowDayNamesDivider(Z)V
    .locals 2

    .prologue
    .line 1960690
    iget-object v1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->E:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1960691
    return-void

    .line 1960692
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowWeekNumber(Z)V
    .locals 2

    .prologue
    .line 1960693
    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-ne v0, p1, :cond_0

    .line 1960694
    :goto_0
    return-void

    .line 1960695
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    .line 1960696
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    const v1, -0x296ff058

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1960697
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->f()V

    goto :goto_0
.end method

.method public setShownWeekCount(I)V
    .locals 1

    .prologue
    .line 1960698
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    if-eq v0, p1, :cond_0

    .line 1960699
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    .line 1960700
    invoke-virtual {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->invalidate()V

    .line 1960701
    :cond_0
    return-void
.end method

.method public setTimeSensitivity(LX/D3g;)V
    .locals 1

    .prologue
    .line 1960702
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->P:LX/D3g;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 1960703
    :goto_0
    iput-object p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->P:LX/D3g;

    .line 1960704
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    if-eqz v0, :cond_0

    .line 1960705
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->A:LX/D3k;

    invoke-virtual {v0}, LX/D3k;->a()V

    .line 1960706
    :cond_0
    return-void

    .line 1960707
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUnfocusedMonthDateColor(I)V
    .locals 4

    .prologue
    .line 1960708
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->k:I

    if-eq v0, p1, :cond_1

    .line 1960709
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->k:I

    .line 1960710
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 1960711
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1960712
    iget-object v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D3i;

    .line 1960713
    iget-boolean v3, v0, LX/D3i;->h:Z

    if-eqz v3, :cond_0

    .line 1960714
    invoke-virtual {v0}, LX/D3i;->invalidate()V

    .line 1960715
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1960716
    :cond_1
    return-void
.end method

.method public setWeekDayTextAppearance(I)V
    .locals 1

    .prologue
    .line 1960717
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    if-eq v0, p1, :cond_0

    .line 1960718
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->p:I

    .line 1960719
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->f()V

    .line 1960720
    :cond_0
    return-void
.end method

.method public setWeekNumberColor(I)V
    .locals 1

    .prologue
    .line 1960721
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    if-eq v0, p1, :cond_0

    .line 1960722
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    .line 1960723
    iget-boolean v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_0

    .line 1960724
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->d()V

    .line 1960725
    :cond_0
    return-void
.end method

.method public setWeekSeparatorLineColor(I)V
    .locals 1

    .prologue
    .line 1960726
    iget v0, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->m:I

    if-eq v0, p1, :cond_0

    .line 1960727
    iput p1, p0, Lcom/facebook/uicontrib/calendar/CalendarView;->m:I

    .line 1960728
    invoke-direct {p0}, Lcom/facebook/uicontrib/calendar/CalendarView;->d()V

    .line 1960729
    :cond_0
    return-void
.end method
