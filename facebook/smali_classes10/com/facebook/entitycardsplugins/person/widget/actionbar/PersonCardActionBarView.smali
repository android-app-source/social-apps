.class public Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;
.super LX/BS0;
.source ""


# instance fields
.field public a:LX/EpN;

.field private final b:LX/EpP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2170263
    invoke-direct {p0, p1}, LX/BS0;-><init>(Landroid/content/Context;)V

    .line 2170264
    new-instance v0, LX/EpP;

    invoke-direct {v0, p0}, LX/EpP;-><init>(Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->b:LX/EpP;

    .line 2170265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2170266
    invoke-direct {p0, p1, p2}, LX/BS0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2170267
    new-instance v0, LX/EpP;

    invoke-direct {v0, p0}, LX/EpP;-><init>(Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->b:LX/EpP;

    .line 2170268
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2170269
    invoke-direct {p0, p1, p2, p3}, LX/BS0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2170270
    new-instance v0, LX/EpP;

    invoke-direct {v0, p0}, LX/EpP;-><init>(Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->b:LX/EpP;

    .line 2170271
    return-void
.end method


# virtual methods
.method public setPresenter(LX/EpN;)V
    .locals 1

    .prologue
    .line 2170272
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->a:LX/EpN;

    .line 2170273
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->b:LX/EpP;

    .line 2170274
    iput-object v0, p0, LX/BS0;->b:LX/EpP;

    .line 2170275
    return-void
.end method
