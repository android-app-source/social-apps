.class public Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/util/IsWorkUserBadgeEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Ept;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/PointF;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2170798
    const-class v0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    const-string v1, "entity_cards"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2170799
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2170800
    new-instance v0, LX/Epv;

    invoke-direct {v0, p0}, LX/Epv;-><init>(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->k:Landroid/view/View$OnClickListener;

    .line 2170801
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b()V

    .line 2170802
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2170803
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2170804
    new-instance v0, LX/Epv;

    invoke-direct {v0, p0}, LX/Epv;-><init>(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->k:Landroid/view/View$OnClickListener;

    .line 2170805
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b()V

    .line 2170806
    return-void
.end method

.method private a(LX/1Fb;LX/1Fb;)Landroid/net/Uri;
    .locals 1
    .param p1    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2170807
    invoke-static {p2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(LX/1Fb;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b(LX/1Fb;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b(LX/1Fb;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;LX/1Ad;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2170809
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/16 v2, 0x36a

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;LX/1Ad;LX/0Or;)V

    return-void
.end method

.method private static a(LX/1Fb;)Z
    .locals 1
    .param p0    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170808
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/1Fb;)Landroid/net/Uri;
    .locals 1
    .param p0    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2170796
    invoke-static {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(LX/1Fb;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/1Fb;LX/1Fb;)Landroid/net/Uri;
    .locals 1
    .param p1    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2170797
    invoke-static {p2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(LX/1Fb;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b(LX/1Fb;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 2170785
    const-class v0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2170786
    const v0, 0x7f030f33

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2170787
    const v0, 0x7f0d24d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2170788
    const v0, 0x7f0d24d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2170789
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->g:Landroid/widget/TextView;

    .line 2170790
    const v0, 0x7f0d24d3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->h:Landroid/view/View;

    .line 2170791
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2170792
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170793
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021888

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->i:Landroid/graphics/drawable/Drawable;

    .line 2170794
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->j:Landroid/graphics/PointF;

    .line 2170795
    return-void
.end method

.method private e()LX/1aZ;
    .locals 2

    .prologue
    .line 2170784
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    sget-object v1, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2170781
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170782
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170783
    return-void
.end method

.method public final a(ILandroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;LX/1Ai;)V
    .locals 4
    .param p4    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1Ai;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170771
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2170772
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2170773
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2170774
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2170775
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    sget-object v1, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2170776
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170777
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2170778
    invoke-virtual {v0, p4}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2170779
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->h:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0815db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2170780
    return-void
.end method

.method public final a(LX/1Fb;LX/1Fb;LX/1Ai;)V
    .locals 4
    .param p1    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1Ai;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170766
    invoke-direct {p0, p1, p2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(LX/1Fb;LX/1Fb;)Landroid/net/Uri;

    move-result-object v1

    .line 2170767
    invoke-direct {p0, p1, p2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b(LX/1Fb;LX/1Fb;)Landroid/net/Uri;

    move-result-object v2

    .line 2170768
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    sget-object v3, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2170769
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170770
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PointF;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170761
    if-nez p1, :cond_0

    .line 2170762
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2170763
    :goto_0
    return-void

    .line 2170764
    :cond_0
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2170765
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, p2}, LX/1af;->b(Landroid/graphics/PointF;)V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->j:Landroid/graphics/PointF;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170753
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2170754
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2170755
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2170756
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->e()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2170757
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2170758
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->h:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0815db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2170759
    return-void

    .line 2170760
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170746
    const v1, 0x7f0b2220

    .line 2170747
    const v0, 0x7f0b2222

    .line 2170748
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 2170749
    const v1, 0x7f0b2221

    .line 2170750
    const v0, 0x7f0b2223

    move v10, v0

    move v11, v1

    .line 2170751
    :goto_0
    iget-object v12, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->g:Landroid/widget/TextView;

    const v1, 0x7f0e0696

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, p1, p2, v1, v2}, LX/EQR;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f021a24

    const v5, 0x7f021afe

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b221b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2224

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->b:LX/0Or;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v12, v0, v1, v2, v3}, LX/EQR;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;III)V

    .line 2170752
    return-void

    :cond_0
    move v10, v0

    move v11, v1

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x10946573

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2170741
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2170742
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->d:LX/Ept;

    if-eqz v1, :cond_0

    .line 2170743
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->d:LX/Ept;

    invoke-virtual {v1, p0}, LX/Ept;->b(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;)V

    .line 2170744
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->d:LX/Ept;

    .line 2170745
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x2620951

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setPresenter(LX/Ept;)V
    .locals 0

    .prologue
    .line 2170739
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->d:LX/Ept;

    .line 2170740
    return-void
.end method
