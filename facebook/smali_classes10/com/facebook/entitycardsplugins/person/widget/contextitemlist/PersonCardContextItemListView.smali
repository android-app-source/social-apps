.class public Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final b:Landroid/widget/LinearLayout;

.field private final c:Landroid/view/View;

.field private d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/Epk;

.field public f:LX/Eph;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2170559
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2170560
    new-instance v0, LX/Epj;

    invoke-direct {v0, p0}, LX/Epj;-><init>(Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->a:Landroid/view/View$OnClickListener;

    .line 2170561
    const v0, 0x7f030f2f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2170562
    const v0, 0x7f0d24cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    .line 2170563
    const v0, 0x7f0d24cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->c:Landroid/view/View;

    .line 2170564
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)I
    .locals 1

    .prologue
    .line 2170558
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;
    .locals 1

    .prologue
    .line 2170535
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2170554
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2170555
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2170556
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2170557
    :cond_0
    return-void
.end method

.method public getItemViewCount()I
    .locals 1

    .prologue
    .line 2170553
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getState()LX/Epk;
    .locals 1

    .prologue
    .line 2170552
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->e:LX/Epk;

    return-object v0
.end method

.method public setPresenter(LX/Eph;)V
    .locals 0

    .prologue
    .line 2170550
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->f:LX/Eph;

    .line 2170551
    return-void
.end method

.method public setState(LX/Epk;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2170536
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->e:LX/Epk;

    .line 2170537
    iget-object v3, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->b:Landroid/widget/LinearLayout;

    sget-object v0, LX/Epk;->READY:LX/Epk;

    if-ne p1, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2170538
    iget-object v3, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->c:Landroid/view/View;

    sget-object v0, LX/Epk;->LOADING_MOVING_SPINNER:LX/Epk;

    if-ne p1, v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2170539
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->d:Landroid/view/View;

    if-nez v0, :cond_0

    sget-object v0, LX/Epk;->ERROR:LX/Epk;

    if-ne p1, v0, :cond_0

    .line 2170540
    const v0, 0x7f0d24cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2170541
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->d:Landroid/view/View;

    .line 2170542
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->d:Landroid/view/View;

    const v3, 0x7f0d24cf

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2170543
    iget-object v3, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2170544
    :cond_0
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2170545
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->d:Landroid/view/View;

    sget-object v3, LX/Epk;->ERROR:LX/Epk;

    if-ne p1, v3, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2170546
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2170547
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2170548
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2170549
    goto :goto_2
.end method
