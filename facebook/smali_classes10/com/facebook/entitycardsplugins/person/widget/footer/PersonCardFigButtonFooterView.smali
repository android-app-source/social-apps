.class public Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/Epo;


# instance fields
.field private a:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2170663
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2170664
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2170665
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2170666
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2170667
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2170668
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2170669
    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3b75e6cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2170670
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 2170671
    const v0, 0x7f0d24d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;->a:Lcom/facebook/fig/button/FigButton;

    .line 2170672
    const/16 v0, 0x2d

    const v2, -0x5b94c80

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
