.class public Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/Epo;


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2170690
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2170691
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a()V

    .line 2170692
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2170687
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2170688
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a()V

    .line 2170689
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2170677
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a:Landroid/graphics/Paint;

    .line 2170678
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0543

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2170679
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2170680
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2170684
    invoke-virtual {p0, p1}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->setText(Ljava/lang/CharSequence;)V

    .line 2170685
    invoke-virtual {p0, p2}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2170686
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 2170681
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2170682
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFooterView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2170683
    return-void
.end method
