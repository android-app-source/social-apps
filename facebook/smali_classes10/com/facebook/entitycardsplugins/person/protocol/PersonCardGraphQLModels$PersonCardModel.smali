.class public final Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/Eop;
.implements LX/Eon;
.implements LX/Eoo;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5wQ;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ba25528
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Z

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Z

.field private y:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2169355
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2169356
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2169357
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2169358
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2169359
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2169360
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2169361
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2169362
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2169363
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169364
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169365
    if-eqz v0, :cond_0

    .line 2169366
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x9

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2169367
    :cond_0
    return-void

    .line 2169368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 2169369
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->y:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2169370
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169371
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169372
    if-eqz v0, :cond_0

    .line 2169373
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x14

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2169374
    :cond_0
    return-void

    .line 2169375
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 2169376
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->A:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2169377
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169378
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169379
    if-eqz v0, :cond_0

    .line 2169380
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x16

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2169381
    :cond_0
    return-void

    .line 2169382
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2169383
    iput-boolean p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->h:Z

    .line 2169384
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169385
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169386
    if-eqz v0, :cond_0

    .line 2169387
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2169388
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2169389
    iput-boolean p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->i:Z

    .line 2169390
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169391
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169392
    if-eqz v0, :cond_0

    .line 2169393
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2169394
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 2169395
    iput-boolean p1, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j:Z

    .line 2169396
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2169397
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2169398
    if-eqz v0, :cond_0

    .line 2169399
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2169400
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 2169401
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169402
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->s:Z

    return v0
.end method

.method public final synthetic B()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169403
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169404
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169528
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->l:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->l:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2169529
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->l:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    return-object v0
.end method

.method public final E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169405
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2169406
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    return-object v0
.end method

.method public final F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169526
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->u:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->u:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2169527
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->u:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169524
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2169525
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169522
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->w:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->w:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    .line 2169523
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->w:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    return-object v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 2169520
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169521
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->x:Z

    return v0
.end method

.method public final J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169302
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2169303
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method

.method public final K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineContextItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169518
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->B:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->B:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2169519
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->B:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 2169474
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2169475
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->x()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2169476
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2169477
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2169478
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2169479
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2169480
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->ce_()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2169481
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2169482
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2169483
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2169484
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2169485
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2169486
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 2169487
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2169488
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->b()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2169489
    const/16 v15, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 2169490
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 2169491
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169492
    const/4 v1, 0x2

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169493
    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169494
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169495
    const/4 v1, 0x5

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169496
    const/4 v1, 0x6

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 2169497
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2169498
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2169499
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2169500
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2169501
    const/16 v1, 0xb

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169502
    const/16 v1, 0xc

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169503
    const/16 v1, 0xd

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169504
    const/16 v1, 0xe

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169505
    const/16 v1, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2169506
    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2169507
    const/16 v1, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2169508
    const/16 v1, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2169509
    const/16 v1, 0x13

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->x:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169510
    const/16 v1, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2169511
    const/16 v1, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2169512
    const/16 v1, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2169513
    const/16 v1, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2169514
    const/16 v1, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 2169515
    const/16 v1, 0x19

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2169516
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2169517
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2169436
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2169437
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2169438
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2169439
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2169440
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169441
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->l:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2169442
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2169443
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2169444
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2169445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169446
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->m:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2169447
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2169448
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2169449
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2169450
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169451
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->u:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2169452
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2169453
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2169454
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2169455
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169456
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2169457
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2169458
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    .line 2169459
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2169460
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169461
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->w:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    .line 2169462
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2169463
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2169464
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2169465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169466
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2169467
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2169468
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2169469
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2169470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169471
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->B:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2169472
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2169473
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2169435
    new-instance v0, LX/Eow;

    invoke-direct {v0, p1}, LX/Eow;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169434
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2169420
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2169421
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->f:Z

    .line 2169422
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->g:Z

    .line 2169423
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->h:Z

    .line 2169424
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->i:Z

    .line 2169425
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j:Z

    .line 2169426
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k:Z

    .line 2169427
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->p:Z

    .line 2169428
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->q:Z

    .line 2169429
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->r:Z

    .line 2169430
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->s:Z

    .line 2169431
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->x:Z

    .line 2169432
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D:Z

    .line 2169433
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2169329
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2169330
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169331
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169332
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 2169333
    :goto_0
    return-void

    .line 2169334
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2169335
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169336
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169337
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2169338
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2169339
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->p()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169341
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2169342
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2169343
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169345
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2169346
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2169347
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169349
    const/16 v0, 0x14

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2169350
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2169351
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2169352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2169353
    const/16 v0, 0x16

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 2169354
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2169407
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2169408
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->a(Z)V

    .line 2169409
    :cond_0
    :goto_0
    return-void

    .line 2169410
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2169411
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->b(Z)V

    goto :goto_0

    .line 2169412
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2169413
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c(Z)V

    goto :goto_0

    .line 2169414
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2169415
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 2169416
    :cond_4
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2169417
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 2169418
    :cond_5
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2169419
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2169304
    new-instance v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-direct {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;-><init>()V

    .line 2169305
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2169306
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169300
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->C:Ljava/lang/String;

    .line 2169301
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169298
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->o:Ljava/lang/String;

    .line 2169299
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169296
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2169297
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->n:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final ce_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169294
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->t:Ljava/lang/String;

    .line 2169295
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2169292
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169293
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2169291
    const v0, -0xd468b3

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2169289
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169290
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2169288
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169286
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->y:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->y:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2169287
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->y:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169284
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->A:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->A:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2169285
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->A:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2169282
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169283
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2169307
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169308
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->g:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2169309
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169310
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k:Z

    return v0
.end method

.method public final synthetic o()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169311
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 2169312
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169313
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2169314
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169315
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->p:Z

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 2169316
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169317
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->r:Z

    return v0
.end method

.method public final synthetic s()LX/2rX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169318
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 2169319
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169320
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D:Z

    return v0
.end method

.method public final synthetic u()LX/Eoq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169321
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()LX/1k2;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169322
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineContextItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169323
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169324
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->e:Ljava/lang/String;

    .line 2169325
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic y()LX/Eos;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169326
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 2169327
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2169328
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->q:Z

    return v0
.end method
