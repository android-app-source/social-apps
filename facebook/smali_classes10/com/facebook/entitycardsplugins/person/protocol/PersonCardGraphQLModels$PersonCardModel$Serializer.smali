.class public final Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2169280
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    new-instance v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2169281
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2169169
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2169171
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2169172
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x16

    const/16 v5, 0x14

    const/16 v4, 0x9

    .line 2169173
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2169174
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2169175
    if-eqz v2, :cond_0

    .line 2169176
    const-string v3, "alternate_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169177
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169178
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169179
    if-eqz v2, :cond_1

    .line 2169180
    const-string v3, "can_viewer_act_as_memorial_contact"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169181
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169182
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169183
    if-eqz v2, :cond_2

    .line 2169184
    const-string v3, "can_viewer_block"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169185
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169186
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169187
    if-eqz v2, :cond_3

    .line 2169188
    const-string v3, "can_viewer_message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169189
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169190
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169191
    if-eqz v2, :cond_4

    .line 2169192
    const-string v3, "can_viewer_poke"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169193
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169194
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169195
    if-eqz v2, :cond_5

    .line 2169196
    const-string v3, "can_viewer_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169197
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169198
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169199
    if-eqz v2, :cond_6

    .line 2169200
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169201
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169202
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169203
    if-eqz v2, :cond_7

    .line 2169204
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169205
    invoke-static {v1, v2, p1, p2}, LX/Ep4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2169206
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169207
    if-eqz v2, :cond_8

    .line 2169208
    const-string v3, "friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169209
    invoke-static {v1, v2, p1}, LX/5zW;->a(LX/15i;ILX/0nX;)V

    .line 2169210
    :cond_8
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2169211
    if-eqz v2, :cond_9

    .line 2169212
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169213
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169214
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2169215
    if-eqz v2, :cond_a

    .line 2169216
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169217
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169218
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169219
    if-eqz v2, :cond_b

    .line 2169220
    const-string v3, "is_followed_by_everyone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169221
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169222
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169223
    if-eqz v2, :cond_c

    .line 2169224
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169225
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169226
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169227
    if-eqz v2, :cond_d

    .line 2169228
    const-string v3, "is_viewer_coworker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169229
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169230
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169231
    if-eqz v2, :cond_e

    .line 2169232
    const-string v3, "is_work_user"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169233
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169234
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2169235
    if-eqz v2, :cond_f

    .line 2169236
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169237
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169238
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169239
    if-eqz v2, :cond_10

    .line 2169240
    const-string v3, "posted_item_privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169241
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2169242
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169243
    if-eqz v2, :cond_11

    .line 2169244
    const-string v3, "preliminaryProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169245
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2169246
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169247
    if-eqz v2, :cond_12

    .line 2169248
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169249
    invoke-static {v1, v2, p1}, LX/Ep5;->a(LX/15i;ILX/0nX;)V

    .line 2169250
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169251
    if-eqz v2, :cond_13

    .line 2169252
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169253
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169254
    :cond_13
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2169255
    if-eqz v2, :cond_14

    .line 2169256
    const-string v2, "secondary_subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169257
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169258
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169259
    if-eqz v2, :cond_15

    .line 2169260
    const-string v3, "structured_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169261
    invoke-static {v1, v2, p1, p2}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2169262
    :cond_15
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2169263
    if-eqz v2, :cond_16

    .line 2169264
    const-string v2, "subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169265
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169266
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2169267
    if-eqz v2, :cond_17

    .line 2169268
    const-string v3, "timeline_context_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169269
    invoke-static {v1, v2, p1, p2}, LX/Ep2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2169270
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2169271
    if-eqz v2, :cond_18

    .line 2169272
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169273
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2169274
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2169275
    if-eqz v2, :cond_19

    .line 2169276
    const-string v3, "viewer_can_see_profile_insights"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2169277
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2169278
    :cond_19
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2169279
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2169170
    check-cast p1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$Serializer;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/0nX;LX/0my;)V

    return-void
.end method
