.class public final Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x76dedc0d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2168880
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2168879
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2168877
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2168878
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2168875
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2168876
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2168867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2168868
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2168869
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2168870
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2168871
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2168872
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2168873
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2168874
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2168847
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->e:Ljava/util/List;

    .line 2168848
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2168854
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2168855
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2168856
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2168857
    if-eqz v1, :cond_2

    .line 2168858
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2168859
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2168860
    :goto_0
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2168861
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2168862
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2168863
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2168864
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2168865
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2168866
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2168851
    new-instance v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    invoke-direct {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;-><init>()V

    .line 2168852
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2168853
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2168850
    const v0, 0x11444559

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2168849
    const v0, -0x7dff4aae

    return v0
.end method
