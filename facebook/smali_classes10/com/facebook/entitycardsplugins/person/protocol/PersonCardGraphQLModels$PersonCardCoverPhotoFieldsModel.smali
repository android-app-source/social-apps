.class public final Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/Eos;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x398ebdb3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2169021
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2169020
    const-class v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2169018
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2169019
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169016
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2169017
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2169014
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->f:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->f:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    .line 2169015
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->f:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2169006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2169007
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2169008
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2169009
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2169010
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2169011
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2169012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2169013
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2168993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2168994
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2168995
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2168996
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2168997
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2168998
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2168999
    :cond_0
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2169000
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    .line 2169001
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2169002
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2169003
    iput-object v0, v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->f:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    .line 2169004
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2169005
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/1f8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2168992
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/Eor;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2168986
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;->k()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel$PhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2168989
    new-instance v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;-><init>()V

    .line 2168990
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2168991
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2168988
    const v0, -0x1b74447b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2168987
    const v0, 0x1da3a91b

    return v0
.end method
