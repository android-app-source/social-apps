.class public Lcom/facebook/nearby/common/SearchSuggestion;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

.field public final d:Lcom/facebook/nearby/common/NearbyTopic;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047805
    new-instance v0, LX/Dpz;

    invoke-direct {v0}, LX/Dpz;-><init>()V

    sput-object v0, Lcom/facebook/nearby/common/SearchSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2047799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047800
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2047801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    .line 2047802
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    .line 2047803
    const-class v0, Lcom/facebook/nearby/common/NearbyTopic;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/common/NearbyTopic;

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2047804
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;)V
    .locals 2

    .prologue
    .line 2047792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047793
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    .line 2047794
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    .line 2047795
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2047796
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2047797
    return-void

    .line 2047798
    :cond_0
    new-instance v0, Lcom/facebook/nearby/common/NearbyTopic;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Lcom/facebook/graphql/model/GraphQLPage;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/common/NearbyTopic;)V
    .locals 0

    .prologue
    .line 2047806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047807
    iput-object p1, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    .line 2047808
    iput-object p2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    .line 2047809
    iput-object p3, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2047810
    iput-object p4, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2047811
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2047791
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2047781
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;

    if-nez v1, :cond_1

    .line 2047782
    :cond_0
    :goto_0
    return v0

    .line 2047783
    :cond_1
    check-cast p1, Lcom/facebook/nearby/common/SearchSuggestion;

    .line 2047784
    iget-object v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {v1, v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2047790
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2047785
    iget-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2047786
    iget-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2047787
    iget-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2047788
    iget-object v0, p0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2047789
    return-void
.end method
