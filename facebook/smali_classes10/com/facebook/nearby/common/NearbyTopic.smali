.class public Lcom/facebook/nearby/common/NearbyTopic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/common/NearbyTopic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047777
    new-instance v0, LX/Dpy;

    invoke-direct {v0}, LX/Dpy;-><init>()V

    sput-object v0, Lcom/facebook/nearby/common/NearbyTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2047773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047774
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    .line 2047775
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    .line 2047776
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPage;)V
    .locals 2

    .prologue
    .line 2047768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047769
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2047770
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    .line 2047771
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    .line 2047772
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2047763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047764
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2047765
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    .line 2047766
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    .line 2047767
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2047750
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2047755
    if-ne p0, p1, :cond_1

    .line 2047756
    :cond_0
    :goto_0
    return v0

    .line 2047757
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 2047758
    goto :goto_0

    .line 2047759
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2047760
    goto :goto_0

    .line 2047761
    :cond_3
    check-cast p1, Lcom/facebook/nearby/common/NearbyTopic;

    .line 2047762
    iget-object v2, p1, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    iget-object v3, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2047754
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2047751
    iget-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2047752
    iget-object v0, p0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2047753
    return-void
.end method
