.class public Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;
.super Lcom/facebook/fbui/glyph/GlyphView;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public b:LX/3OL;

.field public c:LX/3OL;

.field public d:LX/DHx;

.field private e:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982666
    invoke-direct {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1982667
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    iput-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    .line 1982668
    invoke-direct {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a()V

    .line 1982669
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1982624
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982625
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    iput-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    .line 1982626
    invoke-direct {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a()V

    .line 1982627
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1982662
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982663
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    iput-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    .line 1982664
    invoke-direct {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a()V

    .line 1982665
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1982658
    const v0, 0x7f020baa

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setImageResource(I)V

    .line 1982659
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1982660
    new-instance v0, LX/DHu;

    invoke-direct {v0, p0}, LX/DHu;-><init>(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1982661
    return-void
.end method

.method private a([I)V
    .locals 12

    .prologue
    .line 1982670
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1982671
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1982672
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1982673
    const v3, 0x7f020ba7

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1982674
    int-to-float v3, v0

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1982675
    int-to-float v3, v1

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1982676
    new-instance v3, Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1982677
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1982678
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1982679
    const/4 v0, 0x1

    aget v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1982680
    invoke-virtual {v3, v2, v4}, Lcom/facebook/resources/ui/FbFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1982681
    new-instance v0, Landroid/widget/PopupWindow;

    const/4 v1, -0x1

    const/4 v4, -0x1

    invoke-direct {v0, v3, v1, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 1982682
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1982683
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 1982684
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 1982685
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v3, 0x41e00000    # 28.0f

    invoke-static {v1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    .line 1982686
    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x1

    neg-float v6, v1

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1982687
    new-instance v4, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v4}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1982688
    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982689
    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v6, 0x1

    neg-float v7, v1

    aput v7, v5, v6

    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1982690
    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1982691
    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982692
    sget-object v5, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    invoke-static {v2, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1982693
    const-wide/16 v6, 0xc8

    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982694
    sget-object v6, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    fill-array-data v7, :array_1

    invoke-static {v2, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1982695
    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982696
    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1982697
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v3, 0x1

    aput-object v4, v8, v3

    const/4 v3, 0x2

    aput-object v5, v8, v3

    const/4 v3, 0x3

    aput-object v6, v8, v3

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1982698
    sget-object v3, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1982699
    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982700
    sget-object v4, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_3

    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1982701
    const-wide/16 v8, 0xc8

    invoke-virtual {v4, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982702
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1982703
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1982704
    sget-object v5, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    fill-array-data v6, :array_4

    invoke-static {v2, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1982705
    const-wide/16 v8, 0x64

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982706
    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    neg-float v10, v1

    aput v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-static {v2, v6, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1982707
    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v6, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1982708
    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982709
    sget-object v8, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    neg-float v1, v1

    aput v1, v9, v10

    const/4 v1, 0x1

    const/4 v10, 0x0

    aput v10, v9, v1

    invoke-static {v2, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1982710
    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1982711
    const-wide/16 v8, 0xc8

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982712
    sget-object v8, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v9, 0x2

    new-array v9, v9, [F

    fill-array-data v9, :array_5

    invoke-static {v2, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 1982713
    const-wide/16 v10, 0xc8

    invoke-virtual {v8, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982714
    sget-object v9, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_6

    invoke-static {v2, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1982715
    const-wide/16 v10, 0xc8

    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1982716
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1982717
    const/4 v10, 0x4

    new-array v10, v10, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v6, 0x1

    aput-object v1, v10, v6

    const/4 v1, 0x2

    aput-object v8, v10, v1

    const/4 v1, 0x3

    aput-object v2, v10, v1

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1982718
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    .line 1982719
    iget-object v1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v7, v2, v6

    const/4 v6, 0x1

    aput-object v3, v2, v6

    const/4 v3, 0x2

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v5, v2, v3

    const/4 v3, 0x4

    aput-object v9, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1982720
    iget-object v1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    new-instance v2, LX/DHv;

    invoke-direct {v2, p0, v0}, LX/DHv;-><init>(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;Landroid/widget/PopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1982721
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 1982722
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1982723
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 1982724
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 1982725
    :array_2
    .array-data 4
        0x0
        -0x3e600000    # -20.0f
    .end array-data

    .line 1982726
    :array_3
    .array-data 4
        -0x3e600000    # -20.0f
        0x41a00000    # 20.0f
    .end array-data

    .line 1982727
    :array_4
    .array-data 4
        -0x3e600000    # -20.0f
        0x0
    .end array-data

    .line 1982728
    :array_5
    .array-data 4
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 1982729
    :array_6
    .array-data 4
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;LX/3OL;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1982644
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    if-eq p1, v0, :cond_0

    .line 1982645
    sget-object v0, LX/DHw;->a:[I

    invoke-virtual {p1}, LX/3OL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1982646
    :goto_0
    iput-object p1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    .line 1982647
    :cond_0
    return-void

    .line 1982648
    :pswitch_0
    invoke-static {}, LX/10A;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setImageResource(I)V

    .line 1982649
    const v0, -0x6e685d

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1982650
    invoke-virtual {p0, v2}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    goto :goto_0

    .line 1982651
    :pswitch_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    goto :goto_0

    .line 1982652
    :pswitch_2
    const v0, 0x7f020baa

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setImageResource(I)V

    .line 1982653
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1982654
    invoke-virtual {p0, v2}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    goto :goto_0

    .line 1982655
    :pswitch_3
    const v0, 0x7f020ba8

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setImageResource(I)V

    .line 1982656
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1982657
    invoke-virtual {p0, v2}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V
    .locals 2

    .prologue
    .line 1982630
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    sget-object v1, LX/3OL;->NOT_SENT:LX/3OL;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    sget-object v1, LX/3OL;->RECEIVED:LX/3OL;

    if-ne v0, v1, :cond_2

    .line 1982631
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1982632
    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getLocationInWindow([I)V

    .line 1982633
    invoke-direct {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a([I)V

    .line 1982634
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    if-eqz v0, :cond_1

    .line 1982635
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    invoke-interface {v0, p0}, LX/DHx;->onClick(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    .line 1982636
    :cond_1
    :goto_0
    return-void

    .line 1982637
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    sget-object v1, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    if-ne v0, v1, :cond_3

    .line 1982638
    sget-object v0, LX/3OL;->NOT_SENT:LX/3OL;

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setWaveState(LX/3OL;)V

    .line 1982639
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    if-eqz v0, :cond_1

    .line 1982640
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    invoke-interface {v0, p0}, LX/DHx;->onClick(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    goto :goto_0

    .line 1982641
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    sget-object v1, LX/3OL;->INTERACTED:LX/3OL;

    if-ne v0, v1, :cond_1

    .line 1982642
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    if-eqz v0, :cond_1

    .line 1982643
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    invoke-interface {v0, p0}, LX/DHx;->onClick(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    goto :goto_0
.end method


# virtual methods
.method public setOnWaveViewClickListener(LX/DHx;)V
    .locals 0

    .prologue
    .line 1982628
    iput-object p1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    .line 1982629
    return-void
.end method

.method public setWaveState(LX/3OL;)V
    .locals 1

    .prologue
    .line 1982620
    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->c:LX/3OL;

    if-nez v0, :cond_0

    .line 1982621
    iput-object p1, p0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->c:LX/3OL;

    .line 1982622
    :goto_0
    return-void

    .line 1982623
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a$redex0(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;LX/3OL;)V

    goto :goto_0
.end method
