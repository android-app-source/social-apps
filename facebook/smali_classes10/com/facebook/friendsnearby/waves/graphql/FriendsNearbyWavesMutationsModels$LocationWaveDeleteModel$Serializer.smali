.class public final Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1982932
    const-class v0, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel;

    new-instance v1, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1982933
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1982934
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1982935
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1982936
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1982937
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1982938
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1982939
    if-eqz v2, :cond_0

    .line 1982940
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1982941
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1982942
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1982943
    if-eqz v2, :cond_1

    .line 1982944
    const-string p0, "user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1982945
    invoke-static {v1, v2, p1}, LX/DI9;->a(LX/15i;ILX/0nX;)V

    .line 1982946
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1982947
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1982948
    check-cast p1, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel$Serializer;->a(Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveDeleteModel;LX/0nX;LX/0my;)V

    return-void
.end method
