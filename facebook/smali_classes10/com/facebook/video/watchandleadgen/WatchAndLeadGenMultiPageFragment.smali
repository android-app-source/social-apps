.class public Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/B7i;


# instance fields
.field public a:LX/0gc;

.field public b:LX/B6H;

.field public c:Landroid/view/ViewGroup;

.field public d:Landroid/widget/LinearLayout;

.field public e:LX/D8V;

.field public f:I

.field public g:Landroid/widget/LinearLayout;

.field public h:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1967739
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static m(Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;)LX/B6H;
    .locals 2

    .prologue
    .line 1967740
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d3191

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/B6H;

    return-object v0
.end method


# virtual methods
.method public final a(LX/B6H;)V
    .locals 3

    .prologue
    .line 1967741
    iput-object p1, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    .line 1967742
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d3191

    check-cast p1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1967743
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1967744
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->c:Landroid/view/ViewGroup;

    const v1, 0x7f0d18c6

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    .line 1967745
    const/4 p1, 0x0

    .line 1967746
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 1967747
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->i:LX/B7W;

    new-instance v1, LX/B7b;

    sget-object v2, LX/B7a;->VideoAttachment:LX/B7a;

    invoke-direct {v1, v2}, LX/B7b;-><init>(LX/B7a;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1967748
    return-void

    .line 1967749
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->h:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1967750
    iget v1, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->f:I

    invoke-virtual {v0, p1, v1, p1, p1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1967751
    iget-object v1, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1967752
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1967753
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p1

    check-cast p1, LX/0hB;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object p1, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->h:LX/0hB;

    iput-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->i:LX/B7W;

    .line 1967754
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1967755
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->e:LX/D8V;

    invoke-virtual {v0}, LX/D8V;->a()V

    .line 1967756
    return-void
.end method

.method public final d()Landroid/widget/ScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1967757
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    invoke-interface {v0}, LX/B6H;->b()Landroid/widget/ScrollView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c4eb64b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1967758
    const v0, 0x7f03160c

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->c:Landroid/view/ViewGroup;

    .line 1967759
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->c:Landroid/view/ViewGroup;

    .line 1967760
    const v2, 0x7f0d3191

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    .line 1967761
    iget-object v2, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    new-instance v3, LX/D7z;

    invoke-direct {v3, p0}, LX/D7z;-><init>(Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1967762
    iget-object v2, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    new-instance v3, LX/D80;

    invoke-direct {v3, p0}, LX/D80;-><init>(Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1967763
    iget-object v0, p0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->c:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x3b791a4d

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
