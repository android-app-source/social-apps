.class public Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/2mk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1969158
    const-class v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1969159
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1969160
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1969161
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1969162
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1969163
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1969164
    const-class v0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1969165
    const v0, 0x7f031611

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1969166
    const v0, 0x7f0d319e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1969167
    iget-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->a:LX/2mk;

    .line 1969168
    iget-object v1, v0, LX/2mk;->a:LX/0ad;

    sget-short v2, LX/38t;->f:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1969169
    if-eqz v0, :cond_0

    .line 1969170
    invoke-virtual {p0}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1969171
    iget-object v1, p0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1969172
    const v2, 0x7f0b1c99

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1969173
    const v2, 0x7f0b1c98

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1969174
    :cond_0
    const v0, 0x7f0d319d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1969175
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-static {p0}, LX/2mk;->a(LX/0QB;)LX/2mk;

    move-result-object p0

    check-cast p0, LX/2mk;

    iput-object p0, p1, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->a:LX/2mk;

    return-void
.end method
