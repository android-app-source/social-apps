.class public Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Z

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1969302
    const-class v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1969303
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1969304
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1969305
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1969306
    const v0, 0x7f031614

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1969307
    const v0, 0x7f0d31a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->b:Landroid/widget/TextView;

    .line 1969308
    const v0, 0x7f0d31a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->c:Landroid/widget/TextView;

    .line 1969309
    const v0, 0x7f0d31a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1969310
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1969311
    iget-boolean v0, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x555a12dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1969312
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1969313
    const/4 v1, 0x1

    .line 1969314
    iput-boolean v1, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->a:Z

    .line 1969315
    const/16 v1, 0x2d

    const v2, -0x66f99d13

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x457058f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1969316
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1969317
    const/4 v1, 0x0

    .line 1969318
    iput-boolean v1, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->a:Z

    .line 1969319
    const/16 v1, 0x2d

    const v2, -0x708e418d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
