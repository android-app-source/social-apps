.class public Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/D8l;",
        "LX/D8k;",
        "TE;",
        "Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/1nA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1969301
    new-instance v0, LX/D8j;

    invoke-direct {v0}, LX/D8j;-><init>()V

    sput-object v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1nA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969298
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 1969299
    iput-object p1, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->b:LX/1nA;

    .line 1969300
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;
    .locals 4

    .prologue
    .line 1969287
    const-class v1, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

    monitor-enter v1

    .line 1969288
    :try_start_0
    sget-object v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1969289
    sput-object v2, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1969290
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969291
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1969292
    new-instance p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-direct {p0, v3}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;-><init>(LX/1nA;)V

    .line 1969293
    move-object v0, p0

    .line 1969294
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1969295
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1969296
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1969297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1969286
    sget-object v0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1969267
    check-cast p2, LX/D8l;

    .line 1969268
    iget-object v0, p2, LX/D8l;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1969269
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1969270
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1969271
    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1969272
    invoke-virtual {p2}, LX/D8l;->e()Ljava/lang/String;

    move-result-object v2

    .line 1969273
    const/4 v0, 0x0

    .line 1969274
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 1969275
    invoke-static {v1}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v1

    .line 1969276
    iget-object v0, p2, LX/D8l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1969277
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1969278
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j()Ljava/lang/String;

    move-result-object v0

    .line 1969279
    iput-object v0, v1, LX/4Ys;->bD:Ljava/lang/String;

    .line 1969280
    move-object v0, v1

    .line 1969281
    invoke-virtual {v0}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1969282
    iget-object v1, p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->b:LX/1nA;

    .line 1969283
    iget-object v2, p2, LX/D8l;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 1969284
    invoke-virtual {v1, v2, v0}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1969285
    :cond_0
    new-instance v1, LX/D8k;

    invoke-direct {v1, v0}, LX/D8k;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1dde40bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1969248
    check-cast p1, LX/D8l;

    check-cast p2, LX/D8k;

    check-cast p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;

    .line 1969249
    iget-object v1, p1, LX/D8l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1969250
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1969251
    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    .line 1969252
    const v2, 0x7f0d007d

    .line 1969253
    iget p0, p1, LX/D8l;->c:I

    move p0, p0

    .line 1969254
    add-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p4, v2, p0}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->setTag(ILjava/lang/Object;)V

    .line 1969255
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->l()Ljava/lang/String;

    move-result-object v2

    .line 1969256
    iget-object p0, p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1969257
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->k()Ljava/lang/String;

    move-result-object v2

    .line 1969258
    iget-object p0, p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1969259
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1969260
    iget-object v2, p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1969261
    iget-object v1, p2, LX/D8k;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969262
    iget v1, p1, LX/D8l;->d:I

    move v1, v1

    .line 1969263
    iget-object v2, p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1969264
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1969265
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1969266
    const/16 v1, 0x1f

    const v2, -0x5747e7b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1969244
    check-cast p4, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;

    const/4 v1, 0x0

    .line 1969245
    const v0, 0x7f0d007d

    invoke-virtual {p4, v0, v1}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->setTag(ILjava/lang/Object;)V

    .line 1969246
    invoke-virtual {p4, v1}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969247
    return-void
.end method
