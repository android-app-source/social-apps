.class public Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private q:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2123430
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;I)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/video/creatorchannel/CreatorChannel;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2123431
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2123432
    const-string v1, "target_creators_list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2123433
    const-string v1, "target_creators_list_index"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2123434
    return-object v0
.end method

.method private a()V
    .locals 0

    .prologue
    .line 2123435
    invoke-direct {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->b()V

    .line 2123436
    invoke-direct {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->l()V

    .line 2123437
    invoke-direct {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->m()V

    .line 2123438
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2123439
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123440
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2123441
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creator id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "target_creators_list"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2123442
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2123443
    new-instance v1, LX/2zD;

    invoke-direct {v1, p0}, LX/2zD;-><init>(Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2123444
    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2123445
    const v0, 0x7f0d0baa

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2123446
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2123447
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->B_(I)V

    .line 2123448
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2123449
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2123450
    const v0, 0x7f0d0bab

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    .line 2123451
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2123452
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    const-string v1, "creator1"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "creator2"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "creator3"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2123453
    new-instance v1, LX/ESi;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-direct {v1, v2, v3, v0}, LX/ESi;-><init>(LX/0gc;ILX/0Px;)V

    .line 2123454
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2123455
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2123456
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->B_(I)V

    .line 2123457
    iget-object v0, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2123458
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2123459
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2123460
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 2123461
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2123462
    invoke-virtual {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Extras of the intent was expected to hold a string value for creator id"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2123463
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2123464
    const v0, 0x7f0303b3

    invoke-virtual {p0, v0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->setContentView(I)V

    .line 2123465
    invoke-direct {p0}, Lcom/facebook/video/creatorchannel/CreatorChannelsActivity;->a()V

    .line 2123466
    return-void

    .line 2123467
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4354b864

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123468
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2123469
    const/16 v1, 0x23

    const v2, 0x1d567b31

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1fd9d775

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123470
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2123471
    const/16 v1, 0x23

    const v2, 0x584f1ed2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
