.class public Lcom/facebook/video/videohome/views/VideoHomeFollowButton;
.super Lcom/facebook/fbui/widget/text/GlyphWithTextView;
.source ""


# instance fields
.field public a:LX/8D8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field public final d:LX/8D5;

.field private e:Z

.field public f:LX/EUu;

.field private final g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128886
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128884
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 2128873
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128874
    new-instance v0, LX/EVi;

    invoke-direct {v0, p0}, LX/EVi;-><init>(Lcom/facebook/video/videohome/views/VideoHomeFollowButton;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->g:Landroid/view/View$OnClickListener;

    .line 2128875
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2128876
    invoke-static {p1}, LX/BUj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->c:Ljava/lang/String;

    .line 2128877
    invoke-static {p1}, LX/BUj;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->b:Ljava/lang/String;

    .line 2128878
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->c()V

    .line 2128879
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2128880
    new-instance v0, LX/8D5;

    const v1, 0x7f031597

    .line 2128881
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a33

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2128882
    iget-object v4, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->a:LX/8D8;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, LX/8D5;-><init>(ILjava/lang/String;Landroid/view/View;LX/8D6;ZI)V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->d:LX/8D5;

    .line 2128883
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;

    new-instance v1, LX/8D8;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct/range {v1 .. v6}, LX/8D8;-><init>(LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/0SG;)V

    move-object v0, v1

    check-cast v0, LX/8D8;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->a:LX/8D8;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/views/VideoHomeFollowButton;)V
    .locals 2

    .prologue
    .line 2128888
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setIsFollowing(Z)V

    .line 2128889
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->f:LX/EUu;

    if-eqz v0, :cond_0

    .line 2128890
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->f:LX/EUu;

    iget-boolean v1, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    invoke-interface {v0, v1}, LX/EUu;->a(Z)V

    .line 2128891
    :cond_0
    return-void

    .line 2128892
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2128868
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setText(Ljava/lang/CharSequence;)V

    .line 2128869
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setTextColor(I)V

    .line 2128870
    return-void

    .line 2128871
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->c:Ljava/lang/String;

    goto :goto_0

    .line 2128872
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public getNux()LX/8D5;
    .locals 1

    .prologue
    .line 2128856
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->d:LX/8D5;

    return-object v0
.end method

.method public setFollowStateChangedListener(LX/EUu;)V
    .locals 0

    .prologue
    .line 2128866
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->f:LX/EUu;

    .line 2128867
    return-void
.end method

.method public setIsFollowing(Z)V
    .locals 1

    .prologue
    .line 2128861
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    .line 2128862
    iput-boolean p1, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->e:Z

    .line 2128863
    if-eq v0, p1, :cond_0

    .line 2128864
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->c()V

    .line 2128865
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2128857
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->g:Landroid/view/View$OnClickListener;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2128858
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2128859
    return-void

    .line 2128860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
