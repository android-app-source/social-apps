.class public Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/glyph/GlyphButton;

.field public b:Lcom/facebook/widget/FacebookProgressCircleView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128833
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128834
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128835
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128836
    const v0, 0x7f0315c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128837
    const v0, 0x7f0d311a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2128838
    const v0, 0x7f0d311b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    .line 2128839
    return-void
.end method


# virtual methods
.method public getDownloadButton()Lcom/facebook/fbui/glyph/GlyphButton;
    .locals 1

    .prologue
    .line 2128840
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    return-object v0
.end method

.method public getProgressView()Lcom/facebook/widget/FacebookProgressCircleView;
    .locals 1

    .prologue
    .line 2128841
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    return-object v0
.end method
