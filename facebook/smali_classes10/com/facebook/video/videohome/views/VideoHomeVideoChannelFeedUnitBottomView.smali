.class public Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0xX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:LX/EVq;

.field public e:LX/EVH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2129127
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2129129
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129130
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2129131
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a:LX/0xX;

    sget-object v1, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0315c7

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2129132
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2129133
    const v0, 0x7f0d02a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2129134
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->c:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/EVu;

    invoke-direct {v1, p0}, LX/EVu;-><init>(Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2129135
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b()V

    .line 2129136
    return-void

    .line 2129137
    :cond_0
    const v0, 0x7f0315da

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v0

    check-cast v0, LX/0xX;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a:LX/0xX;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2129124
    new-instance v0, LX/EVq;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {v0, v1, v2}, LX/EVq;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->d:LX/EVq;

    .line 2129125
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->d:LX/EVq;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->a(LX/636;)V

    .line 2129126
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2129122
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2129123
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2129116
    if-eqz p1, :cond_0

    .line 2129117
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2129118
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setSingleLine(Z)V

    .line 2129119
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2129120
    :goto_0
    return-void

    .line 2129121
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setSingleLine(Z)V

    goto :goto_0
.end method

.method public setListener(LX/EVH;)V
    .locals 0

    .prologue
    .line 2129114
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->e:LX/EVH;

    .line 2129115
    return-void
.end method
