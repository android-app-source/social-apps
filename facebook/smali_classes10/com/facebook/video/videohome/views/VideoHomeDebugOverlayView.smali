.class public Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final h:J


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EVa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Jc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ESr;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Jb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2128790
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->h:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128791
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128792
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128793
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128794
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128795
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128796
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->i:Ljava/util/Map;

    .line 2128797
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->j:Ljava/util/Map;

    .line 2128798
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2128799
    new-instance v0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView$1;-><init>(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->k:Ljava/lang/Runnable;

    .line 2128800
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->f:LX/0Jb;

    new-instance v1, LX/EVh;

    invoke-direct {v1, p0}, LX/EVh;-><init>(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;)V

    .line 2128801
    iput-object v1, v0, LX/0Jb;->g:LX/0Ja;

    .line 2128802
    invoke-static {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;)V

    .line 2128803
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2128823
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2128824
    if-nez v0, :cond_0

    .line 2128825
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2128826
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2128827
    invoke-direct {p0, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Z)Landroid/widget/TextView;

    move-result-object v1

    .line 2128828
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2128829
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2128830
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->addView(Landroid/view/View;)V

    .line 2128831
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->i:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2128832
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 2128804
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2128805
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2128806
    if-nez v0, :cond_0

    .line 2128807
    invoke-direct {p0, p1}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v2

    .line 2128808
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Z)Landroid/widget/TextView;

    move-result-object v0

    .line 2128809
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2128810
    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->j:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2128811
    :cond_0
    return-object v0
.end method

.method private a(Z)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 2128812
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2128813
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2128814
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 2128815
    if-eqz p1, :cond_0

    .line 2128816
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2128817
    :cond_0
    return-object v0
.end method

.method private varargs a(ILjava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2128818
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 2128819
    aget-object v1, p3, v0

    .line 2128820
    invoke-direct {p0, p2, v1}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2128821
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2128822
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Jb;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;",
            "LX/0Ot",
            "<",
            "LX/EVa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1CC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Jc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ESr;",
            ">;",
            "LX/0Jb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2128789
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->f:LX/0Jb;

    iput-object p7, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->g:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    const/16 v1, 0x383c

    invoke-static {v7, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x136b

    invoke-static {v7, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-static {v7, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x37b4

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3806

    invoke-static {v7, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v7}, LX/0Jb;->a(LX/0QB;)LX/0Jb;

    move-result-object v6

    check-cast v6, LX/0Jb;

    const/16 v8, 0x271

    invoke-static {v7, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Jb;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;)V
    .locals 0

    .prologue
    .line 2128784
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->b()V

    .line 2128785
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->d()V

    .line 2128786
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c()V

    .line 2128787
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->f()V

    .line 2128788
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2128780
    if-nez p1, :cond_0

    .line 2128781
    const-string p1, "NULL"

    .line 2128782
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "%s=%s"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p3, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2128783
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2128778
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->getBadgedSessionsText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SESSION"

    const-string v2, "session_badge_count"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128779
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2128773
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->getCreatorChannelSubscriptionsText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Graphql Subs"

    const-string v2, "subscribed_creator_channel_units"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128774
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128775
    const-string v0, "Graphql Subs"

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "subscribed_creator_channel_units"

    aput-object v2, v1, v4

    invoke-direct {p0, v4, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(ILjava/lang/String;[Ljava/lang/String;)V

    .line 2128776
    :goto_0
    return-void

    .line 2128777
    :cond_0
    const/16 v0, 0x8

    const-string v1, "Graphql Subs"

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "subscribed_creator_channel_units"

    aput-object v3, v2, v4

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 13

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2128752
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128753
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    .line 2128754
    iget-object v1, v0, LX/0hn;->w:LX/095;

    move-object v0, v1

    .line 2128755
    const-string v1, "TTI"

    const-string v2, "badge_fetch_reason"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128756
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EVa;

    .line 2128757
    iget-object v1, v0, LX/EVa;->x:LX/0JU;

    move-object v0, v1

    .line 2128758
    const-string v1, "TTI"

    const-string v2, "badge_prefetch_reason"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128759
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2128760
    const-string v0, "TTI"

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "tti_type"

    aput-object v2, v1, v4

    const-string v2, "cache_status"

    aput-object v2, v1, v5

    const-string v2, "prefetch_hit_rate"

    aput-object v2, v1, v6

    invoke-direct {p0, v4, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(ILjava/lang/String;[Ljava/lang/String;)V

    .line 2128761
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Jc;

    .line 2128762
    iget-object v1, v0, LX/0Jc;->d:LX/0JP;

    move-object v0, v1

    .line 2128763
    const-string v1, "TTI"

    const-string v2, "tti_type"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128764
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Jc;

    .line 2128765
    iget-object v1, v0, LX/0Jc;->c:LX/0J9;

    move-object v0, v1

    .line 2128766
    const-string v1, "TTI"

    const-string v2, "cache_status"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128767
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Jc;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 2128768
    iget v9, v0, LX/0Jc;->b:I

    if-nez v9, :cond_2

    .line 2128769
    :goto_0
    move-wide v0, v7

    .line 2128770
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    const-string v1, "TTI"

    const-string v2, "prefetch_hit_rate"

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128771
    :cond_0
    :goto_1
    return-void

    .line 2128772
    :cond_1
    const/16 v0, 0x8

    const-string v1, "TTI"

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "tti_type"

    aput-object v3, v2, v4

    const-string v3, "cache_status"

    aput-object v3, v2, v5

    const-string v3, "prefetch_hit_rate"

    aput-object v3, v2, v6

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a(ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget v9, v0, LX/0Jc;->a:I

    int-to-double v9, v9

    iget v11, v0, LX/0Jc;->b:I

    int-to-double v11, v11

    mul-double/2addr v7, v11

    div-double v7, v9, v7

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 2128747
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    .line 2128748
    iget-object v1, v0, LX/0hn;->w:LX/095;

    move-object v0, v1

    .line 2128749
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EVa;

    .line 2128750
    iget-object v1, v0, LX/EVa;->x:LX/0JU;

    move-object v0, v1

    .line 2128751
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 2128745
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->k:Ljava/lang/Runnable;

    sget-wide v2, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->h:J

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    .line 2128746
    return-void
.end method

.method private getBadgedSessionsText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2128740
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128741
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    .line 2128742
    iget p0, v0, LX/1CC;->h:I

    move v0, p0

    .line 2128743
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2128744
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCreatorChannelSubscriptionsText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2128732
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESr;

    .line 2128733
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, v0, LX/ESr;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v1, v1

    .line 2128734
    const-string v0, "["

    .line 2128735
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2128736
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2128737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2128738
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2128739
    return-object v0
.end method
