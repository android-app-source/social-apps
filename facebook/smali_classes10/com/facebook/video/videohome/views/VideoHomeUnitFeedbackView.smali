.class public Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# static fields
.field private static final i:Ljava/util/Set;


# instance fields
.field public a:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1za;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/20n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/20i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/view/View;

.field public n:Lcom/facebook/graphql/model/GraphQLFeedback;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2129108
    sget-object v0, LX/20X;->LIKE:LX/20X;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    sget-object v2, LX/20X;->SHARE:LX/20X;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->i:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2129106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2129104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2129105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2129076
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2129077
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2129078
    invoke-virtual {p0, v3}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->setOrientation(I)V

    .line 2129079
    const v0, 0x7f03159c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2129080
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2129081
    invoke-virtual {p0, v3}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->setWillNotDraw(Z)V

    .line 2129082
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->f:LX/20J;

    sget-object v1, LX/1Wl;->HIDDEN:LX/1Wl;

    .line 2129083
    iput-object v1, v0, LX/20J;->e:LX/1Wl;

    .line 2129084
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->f:LX/20J;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    .line 2129085
    iput-object v1, v0, LX/20J;->d:LX/1Wl;

    .line 2129086
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->f:LX/20J;

    .line 2129087
    iput-boolean v3, v0, LX/20J;->f:Z

    .line 2129088
    const v0, 0x7f0d30b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 2129089
    const v0, 0x7f0d05c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    .line 2129090
    const v0, 0x7f0d05c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->l:Landroid/widget/TextView;

    .line 2129091
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->i:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtons(Ljava/util/Set;)V

    .line 2129092
    new-instance v2, LX/EVs;

    invoke-direct {v2}, LX/EVs;-><init>()V

    .line 2129093
    sget-object v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2129094
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2129095
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v1

    .line 2129096
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2129097
    check-cast v0, Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2129098
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 2129099
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 2129100
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 2129101
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setTopDividerStyle(LX/1Wl;)V

    .line 2129102
    const v0, 0x7f0d30b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->m:Landroid/view/View;

    .line 2129103
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;LX/20K;LX/1zf;LX/154;LX/1za;LX/20n;LX/20J;LX/0Ot;LX/20i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;",
            "LX/20K;",
            "LX/1zf;",
            "LX/154;",
            "LX/1za;",
            "LX/20n;",
            "LX/20J;",
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2129043
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a:LX/20K;

    iput-object p2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->b:LX/1zf;

    iput-object p3, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->c:LX/154;

    iput-object p4, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->d:LX/1za;

    iput-object p5, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->e:LX/20n;

    iput-object p6, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->f:LX/20J;

    iput-object p7, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->h:LX/20i;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    invoke-static {v8}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v1

    check-cast v1, LX/20K;

    invoke-static {v8}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v2

    check-cast v2, LX/1zf;

    invoke-static {v8}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v3

    check-cast v3, LX/154;

    invoke-static {v8}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v4

    check-cast v4, LX/1za;

    invoke-static {v8}, LX/20n;->a(LX/0QB;)LX/20n;

    move-result-object v5

    check-cast v5, LX/20n;

    invoke-static {v8}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object v6

    check-cast v6, LX/20J;

    const/16 v7, 0x475

    invoke-static {v8, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v8}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v8

    check-cast v8, LX/20i;

    invoke-static/range {v0 .. v8}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;LX/20K;LX/1zf;LX/154;LX/1za;LX/20n;LX/20J;LX/0Ot;LX/20i;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 2129060
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2129061
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2129062
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2129063
    :goto_0
    return-void

    .line 2129064
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 2129065
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 2129066
    if-lez v0, :cond_1

    .line 2129067
    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->c:LX/154;

    invoke-virtual {v3, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2129068
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2129069
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->d:LX/1za;

    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v3, v5}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;Z)V

    .line 2129070
    :goto_1
    if-lez v1, :cond_2

    .line 2129071
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->l:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->c:LX/154;

    invoke-virtual {v2, v1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2129072
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2129073
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2129074
    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->k:Landroid/widget/TextView;

    invoke-static {v2}, LX/1za;->a(Landroid/widget/TextView;)V

    goto :goto_1

    .line 2129075
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2129056
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a:LX/20K;

    invoke-static {v0, v1}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 2129057
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2129058
    iput-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2129059
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21I;LX/1Po;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/21I;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2129047
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2129048
    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2129049
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v2, p2, LX/21I;->g:Ljava/util/EnumMap;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    iget-object v4, p2, LX/21I;->c:LX/20Z;

    invoke-static {v0, v2, v3, v4}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 2129050
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->j:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    new-instance v2, LX/EVt;

    iget-object v3, p2, LX/21I;->d:LX/21M;

    invoke-direct {v2, p0, v3}, LX/EVt;-><init>(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;LX/21M;)V

    iget-object v3, p2, LX/21I;->e:LX/0wd;

    iget-object v4, p2, LX/21I;->f:LX/20z;

    iget-object v5, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->b:LX/1zf;

    sget-object v6, LX/20I;->DARK:LX/20I;

    iget-object v7, p2, LX/21I;->i:LX/0Px;

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 2129051
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2129052
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2129053
    invoke-static {p0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->b$redex0(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;)V

    .line 2129054
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->m:Landroid/view/View;

    new-instance v1, LX/EVr;

    invoke-direct {v1, p0, p1, p3}, LX/EVr;-><init>(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2129055
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2129044
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2129045
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->f:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 2129046
    return-void
.end method
