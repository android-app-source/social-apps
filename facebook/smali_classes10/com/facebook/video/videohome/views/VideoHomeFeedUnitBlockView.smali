.class public Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;
.super LX/62b;
.source ""


# instance fields
.field public a:LX/0xX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128850
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128851
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128848
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128849
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2128842
    invoke-direct {p0, p1, p2, p3}, LX/62b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128843
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2128844
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->a:LX/0xX;

    sget-object v1, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0315c8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128845
    return-void

    .line 2128846
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->getLayoutResIdWithoutDownloadButton()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v0

    check-cast v0, LX/0xX;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->a:LX/0xX;

    return-void
.end method

.method private getLayoutResIdWithoutDownloadButton()I
    .locals 1

    .prologue
    .line 2128847
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeFeedUnitBlockView;->a:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0315dc

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0315db

    goto :goto_0
.end method
