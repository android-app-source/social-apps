.class public Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/7zb;
.implements LX/7zg;
.implements LX/7zh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/View;",
        "LX/7zb;",
        "LX/7zg;",
        "LX/7zh",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ESm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/Runnable;

.field private final g:Landroid/graphics/Paint;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/Paint;

.field private final m:Landroid/graphics/Paint;

.field private final n:Landroid/graphics/Rect;

.field private final o:Landroid/graphics/Rect;

.field private p:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:J

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128657
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128658
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128615
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128616
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v4, -0x10000

    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x41f00000    # 30.0f

    .line 2128617
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128618
    const-string v0, "VideoHomeAutoplayDebugOverlayView"

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->d:Ljava/lang/String;

    .line 2128619
    new-instance v0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView$1;-><init>(Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->f:Ljava/lang/Runnable;

    .line 2128620
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    .line 2128621
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->o:Landroid/graphics/Rect;

    .line 2128622
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2128623
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->b:LX/ESm;

    .line 2128624
    iget-object v1, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, v1

    .line 2128625
    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    .line 2128626
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    .line 2128627
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128628
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2128629
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128630
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2128631
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    .line 2128632
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128633
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2128634
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128635
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2128636
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    .line 2128637
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128638
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2128639
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128640
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2128641
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    .line 2128642
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128643
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2128644
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->k:Landroid/graphics/Paint;

    .line 2128645
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128646
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128647
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->l:Landroid/graphics/Paint;

    .line 2128648
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->l:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128649
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128650
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->m:Landroid/graphics/Paint;

    .line 2128651
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->m:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2128652
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2128653
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2128654
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2128655
    const-string v1, "AUTOPLAY PAUSED"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->getBottom()I

    move-result v3

    const/16 v4, 0x190

    iget-object v5, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Ljava/lang/String;IIILandroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 2128656
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 7
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128543
    if-nez p2, :cond_1

    .line 2128544
    :cond_0
    :goto_0
    return-void

    .line 2128545
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    invoke-static {p2, v0}, LX/1AY;->a(Landroid/view/View;Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2128546
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-eqz v0, :cond_0

    .line 2128547
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->o:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2128548
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->o:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 2128549
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2128550
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/lit16 v2, v0, -0x104

    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->n:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    const/16 v4, 0x104

    iget-object v5, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->l:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Ljava/lang/String;IIILandroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;Ljava/lang/String;IIILandroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 7

    .prologue
    .line 2128659
    invoke-virtual {p5}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    float-to-int v0, v0

    sub-int v0, p3, v0

    add-int/lit8 v6, v0, -0x3c

    .line 2128660
    int-to-float v1, p2

    int-to-float v2, v6

    add-int v0, p2, p4

    int-to-float v3, v0

    int-to-float v4, p3

    move-object v0, p0

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2128661
    add-int/lit8 v0, p2, 0x1e

    int-to-float v0, v0

    add-int/lit8 v1, v6, 0x3c

    int-to-float v1, v1

    invoke-virtual {p0, p1, v0, v1, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2128662
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2128663
    const v0, 0x7f0d310f

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2128664
    if-nez v0, :cond_0

    .line 2128665
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0315be

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2128666
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;LX/0Sh;LX/ESm;LX/0SG;)V
    .locals 0

    .prologue
    .line 2128667
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->b:LX/ESm;

    iput-object p3, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->c:LX/0SG;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {v2}, LX/ESm;->a(LX/0QB;)LX/ESm;

    move-result-object v1

    check-cast v1, LX/ESm;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;LX/0Sh;LX/ESm;LX/0SG;)V

    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2128606
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2128607
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->s:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->getBottom()I

    move-result v0

    add-int/lit8 v3, v0, -0x3c

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->j:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Ljava/lang/String;IIILandroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 2128608
    :cond_0
    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2128609
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->b:LX/ESm;

    .line 2128610
    iget-object v1, v0, LX/ESm;->a:LX/7zZ;

    move-object v0, v1

    .line 2128611
    iget v1, v0, LX/7zZ;->e:I

    move v0, v1

    .line 2128612
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->o:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 2128613
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->m:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2128614
    return-void
.end method

.method public static c(Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;)V
    .locals 4

    .prologue
    .line 2128604
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 2128605
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2128602
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/0Sh;->c(Ljava/lang/Runnable;)V

    .line 2128603
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2128600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->s:Ljava/lang/String;

    .line 2128601
    return-void
.end method

.method public final a(LX/2oV;)V
    .locals 1

    .prologue
    .line 2128597
    instance-of v0, p1, LX/7zc;

    if-eqz v0, :cond_0

    .line 2128598
    check-cast p1, LX/7zc;

    invoke-virtual {p1, p0}, LX/7zc;->a(LX/7zb;)V

    .line 2128599
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128592
    iput-object p2, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->p:Landroid/view/View;

    .line 2128593
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->q:Landroid/view/View;

    .line 2128594
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->r:J

    .line 2128595
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->invalidate()V

    .line 2128596
    return-void
.end method

.method public final a(Ljava/util/Set;LX/2oO;)V
    .locals 3
    .param p2    # LX/2oO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "LX/2oO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2128579
    const-string v0, "UNKNOWN"

    .line 2128580
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2128581
    invoke-interface {p1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2128582
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Autoplay blocked: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->s:Ljava/lang/String;

    .line 2128583
    return-void

    .line 2128584
    :cond_1
    if-eqz p2, :cond_3

    .line 2128585
    iget-boolean v1, p2, LX/2oO;->u:Z

    move v1, v1

    .line 2128586
    if-eqz v1, :cond_2

    .line 2128587
    const-string v0, "isManuallyPaused"

    goto :goto_0

    .line 2128588
    :cond_2
    iget-boolean v1, p2, LX/2oO;->t:Z

    move v1, v1

    .line 2128589
    if-eqz v1, :cond_0

    .line 2128590
    const-string v0, "isInFullscreen"

    goto :goto_0

    .line 2128591
    :cond_3
    const-string v0, "autoplayStateManager is null"

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2128577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->s:Ljava/lang/String;

    .line 2128578
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2a63b1ce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128572
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2128573
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    invoke-virtual {v1, p0}, LX/1Aa;->a(LX/7zh;)V

    .line 2128574
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    invoke-virtual {v1, p0}, LX/1Aa;->a(LX/7zg;)V

    .line 2128575
    invoke-static {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->c(Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;)V

    .line 2128576
    const/16 v1, 0x2d

    const v2, -0x12e3d356

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x24f6f853

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128566
    invoke-direct {p0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->d()V

    .line 2128567
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    invoke-virtual {v1, p0}, LX/1Aa;->b(LX/7zh;)V

    .line 2128568
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    .line 2128569
    iget-object v2, v1, LX/1Aa;->p:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2128570
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2128571
    const/16 v1, 0x2d

    const v2, -0x4c8f0960

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2128551
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2128552
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    .line 2128553
    iget-boolean v1, v0, LX/1Aa;->i:Z

    move v0, v1

    .line 2128554
    if-nez v0, :cond_0

    .line 2128555
    invoke-direct {p0, p1}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;)V

    .line 2128556
    :goto_0
    return-void

    .line 2128557
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->r:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2128558
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->h:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Landroid/view/View;Landroid/graphics/Paint;)V

    .line 2128559
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->g:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Landroid/view/View;Landroid/graphics/Paint;)V

    .line 2128560
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->e:LX/1Aa;

    .line 2128561
    iget-object v1, v0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    .line 2128562
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2128563
    iget-object v2, p0, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->i:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v2}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/graphics/Canvas;Landroid/view/View;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 2128564
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->b(Landroid/graphics/Canvas;)V

    .line 2128565
    invoke-direct {p0, p1}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->c(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method
