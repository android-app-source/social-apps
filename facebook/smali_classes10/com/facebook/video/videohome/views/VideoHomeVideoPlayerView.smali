.class public Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3FR;
.implements LX/3FT;
.implements LX/1a7;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Z

.field private d:Lcom/facebook/video/player/RichVideoPlayer;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2129200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2129198
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2129199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2129183
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2129184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c:Z

    .line 2129185
    const-class v0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    invoke-static {v0, p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2129186
    const v0, 0x7f0315e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2129187
    const v0, 0x7f0d2a31

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    .line 2129188
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2129189
    new-instance v0, LX/7LN;

    invoke-direct {v0}, LX/7LN;-><init>()V

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    .line 2129190
    iput-object v1, v0, LX/7LN;->a:LX/04D;

    .line 2129191
    move-object v0, v0

    .line 2129192
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getPlayerType()LX/04G;

    move-result-object v1

    .line 2129193
    iput-object v1, v0, LX/7LN;->b:LX/04G;

    .line 2129194
    move-object v0, v0

    .line 2129195
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getAdditionalPlugins()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7LN;->a(Ljava/util/List;)LX/7LN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, LX/7LN;->a(Lcom/facebook/video/player/RichVideoPlayer;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2129196
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04H;->ELIGIBLE:LX/04H;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 2129197
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;Ljava/lang/Boolean;Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;)V
    .locals 0

    .prologue
    .line 2129182
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->a:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->b:Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    invoke-static {v1}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    new-instance v2, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v1}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static {v1}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v6

    check-cast v6, LX/0xX;

    invoke-static {v1}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v7

    check-cast v7, LX/0Wd;

    invoke-static {v1}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v8

    check-cast v8, LX/0iY;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;-><init>(Landroid/content/Context;LX/0Uh;Ljava/lang/Boolean;LX/0xX;LX/0Wd;LX/0iY;)V

    move-object v1, v2

    check-cast v1, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->a(Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;Ljava/lang/Boolean;Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;)V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;I)V
    .locals 1

    .prologue
    .line 2129177
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 2129178
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 2129179
    invoke-virtual {v0, p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 2129180
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2129181
    return-void
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 2129168
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    if-eq v0, p1, :cond_0

    .line 2129169
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 2129170
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c:Z

    if-nez v0, :cond_1

    .line 2129171
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2129172
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2129173
    :cond_1
    iput-object p1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2129174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c:Z

    .line 2129175
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->requestLayout()V

    .line 2129176
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 2129164
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 2129165
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2129166
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2129167
    :cond_0
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 3

    .prologue
    .line 2129153
    iget-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c:Z

    if-eqz v0, :cond_0

    .line 2129154
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2129155
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2129156
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2129157
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2129158
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2129159
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->e:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2129160
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->detachViewFromParent(Landroid/view/View;)V

    .line 2129161
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->requestLayout()V

    .line 2129162
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->c:Z

    .line 2129163
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2129152
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2129151
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getAdditionalPlugins()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2129138
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2129139
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2129140
    new-instance v1, LX/7MZ;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7MZ;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2129141
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAdditionalPlugins()Ljava/util/List;
    .locals 1

    .prologue
    .line 2129150
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getAdditionalPlugins()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 2129149
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    return-object v0
.end method

.method public getPluginSelector()Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;
    .locals 1

    .prologue
    .line 2129148
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->b:Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 2129147
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 2129146
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 2129144
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->x()V

    .line 2129145
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 2129142
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->y()V

    .line 2129143
    return-void
.end method
