.class public Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/graphql/model/GraphQLActor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2123588
    new-instance v0, LX/ESp;

    invoke-direct {v0}, LX/ESp;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2123585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123586
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;->a:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2123587
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 2123582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123583
    iput-object p1, p0, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;->a:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2123584
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2123579
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2123580
    iget-object v0, p0, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2123581
    return-void
.end method
