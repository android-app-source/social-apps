.class public Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile l:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;


# instance fields
.field private final b:LX/2xj;

.field public final c:LX/3DC;

.field private final d:LX/1ly;

.field public final e:LX/19j;

.field public final f:LX/0SG;

.field private final g:LX/0oB;

.field public final h:Landroid/os/Handler;

.field public i:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public k:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2125216
    const-class v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2xj;LX/3DC;LX/1JD;LX/19j;LX/0SG;Landroid/os/Handler;)V
    .locals 2
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125204
    iput-object p1, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->b:LX/2xj;

    .line 2125205
    iput-object p2, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->c:LX/3DC;

    .line 2125206
    sget-object v0, LX/1Li;->VIDEO_HOME:LX/1Li;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->d:LX/1ly;

    .line 2125207
    iput-object p4, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->e:LX/19j;

    .line 2125208
    iput-object p5, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->f:LX/0SG;

    .line 2125209
    iput-object p6, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->h:Landroid/os/Handler;

    .line 2125210
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 2125211
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->b:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->k:J

    .line 2125212
    new-instance v0, LX/ETV;

    invoke-direct {v0, p0}, LX/ETV;-><init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->g:LX/0oB;

    .line 2125213
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->b:LX/2xj;

    iget-object v1, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->g:LX/0oB;

    invoke-virtual {v0, v1}, LX/2xj;->a(LX/0oB;)V

    .line 2125214
    return-void

    .line 2125215
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;
    .locals 10

    .prologue
    .line 2125151
    sget-object v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->l:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    if-nez v0, :cond_1

    .line 2125152
    const-class v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    monitor-enter v1

    .line 2125153
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->l:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2125154
    if-eqz v2, :cond_0

    .line 2125155
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2125156
    new-instance v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v4

    check-cast v4, LX/2xj;

    invoke-static {v0}, LX/3DC;->b(LX/0QB;)LX/3DC;

    move-result-object v5

    check-cast v5, LX/3DC;

    const-class v6, LX/1JD;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1JD;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v7

    check-cast v7, LX/19j;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v9

    check-cast v9, Landroid/os/Handler;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;-><init>(LX/2xj;LX/3DC;LX/1JD;LX/19j;LX/0SG;Landroid/os/Handler;)V

    .line 2125157
    move-object v0, v3

    .line 2125158
    sput-object v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->l:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2125159
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2125160
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2125161
    :cond_1
    sget-object v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->l:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    return-object v0

    .line 2125162
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2125163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2125192
    iget-object v1, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->e:LX/19j;

    iget-boolean v1, v1, LX/19j;->aV:Z

    if-eqz v1, :cond_1

    .line 2125193
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->c:LX/3DC;

    invoke-virtual {v0, p1, p2}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z

    move-result v0

    .line 2125194
    :cond_0
    :goto_0
    return v0

    .line 2125195
    :cond_1
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2125196
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2125197
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2125198
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2125199
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v2, :cond_3

    .line 2125200
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->d:LX/1ly;

    invoke-virtual {v0, p1}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2125201
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2125202
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->c:LX/3DC;

    invoke-virtual {v0, v1, p2}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    goto :goto_1
.end method


# virtual methods
.method public final b(LX/0Px;ILX/379;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9qT;",
            ">;I",
            "LX/379;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2125173
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v6, v1

    move/from16 v2, p2

    :goto_0
    if-ge v6, v7, :cond_0

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9qT;

    .line 2125174
    invoke-interface {v1}, LX/9qT;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v9, :cond_4

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2125175
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2125176
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v10

    .line 2125177
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    .line 2125178
    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, v11, :cond_3

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2125179
    invoke-static {v1}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/0Px;

    move-result-object v12

    .line 2125180
    if-eqz v12, :cond_2

    .line 2125181
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    .line 2125182
    const/4 v1, 0x0

    move v3, v1

    :goto_3
    if-ge v3, v13, :cond_2

    invoke-virtual {v12, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9uc;

    .line 2125183
    if-gtz v2, :cond_1

    .line 2125184
    :cond_0
    return-void

    .line 2125185
    :cond_1
    invoke-interface {v1}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2125186
    move-object/from16 v0, p3

    invoke-direct {p0, v1, v0}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2125187
    add-int/lit8 v1, v2, -0x1

    .line 2125188
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_3

    .line 2125189
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2125190
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 2125191
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method public final d(LX/0Px;ILX/379;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;I",
            "LX/379;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2125164
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    move v1, p2

    :goto_0
    if-ge v3, v4, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125165
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v5

    invoke-virtual {v5}, LX/ETQ;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {p0, v5, v1, p3}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->d(LX/0Px;ILX/379;)I

    move-result v1

    .line 2125166
    if-gtz v1, :cond_0

    move v0, v2

    .line 2125167
    :goto_1
    return v0

    .line 2125168
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2125169
    invoke-direct {p0, v0, p3}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2125170
    add-int/lit8 v0, v1, -0x1

    .line 2125171
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2125172
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method
