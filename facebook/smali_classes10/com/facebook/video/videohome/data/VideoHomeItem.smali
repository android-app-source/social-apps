.class public Lcom/facebook/video/videohome/data/VideoHomeItem;
.super Lcom/facebook/reaction/common/ReactionUnitComponentNode;
.source ""

# interfaces
.implements LX/16g;


# instance fields
.field private a:LX/ETQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0us;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2124863
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2124864
    return-void
.end method


# virtual methods
.method public final a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 4

    .prologue
    .line 2124830
    new-instance v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124831
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2124832
    iget-object v2, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2124833
    invoke-direct {v1, p1, v0, v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2124834
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    move-object v0, v0

    .line 2124835
    iput-object v0, v1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    .line 2124836
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124837
    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    .line 2124838
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124839
    invoke-virtual {v2, v0}, LX/ETQ;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    goto :goto_0

    .line 2124840
    :cond_0
    return-object v1
.end method

.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2124857
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2124858
    if-nez v1, :cond_1

    .line 2124859
    :cond_0
    :goto_0
    return-object v0

    .line 2124860
    :cond_1
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2124861
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124862
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 2

    .prologue
    .line 2124851
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 2124852
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2124853
    :cond_0
    :goto_0
    return-object v0

    .line 2124854
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    .line 2124855
    if-eqz v1, :cond_0

    .line 2124856
    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2124848
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2124849
    invoke-super {p0}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->c:Ljava/lang/String;

    .line 2124850
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2124865
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2124866
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2124846
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2124847
    return-object v0
.end method

.method public final q()LX/ETQ;
    .locals 1

    .prologue
    .line 2124843
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    if-nez v0, :cond_0

    .line 2124844
    new-instance v0, LX/ETQ;

    invoke-direct {v0}, LX/ETQ;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    .line 2124845
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2124842
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2124841
    iget-object v1, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->a:LX/ETQ;

    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
