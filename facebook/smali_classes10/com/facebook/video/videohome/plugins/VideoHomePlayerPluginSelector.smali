.class public Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final n:Landroid/content/Context;

.field private final o:Lcom/facebook/video/player/plugins/VideoPlugin;

.field private final p:LX/0Uh;

.field public final q:LX/0xX;

.field public final r:LX/0iY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2128337
    const-class v0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uh;Ljava/lang/Boolean;LX/0xX;LX/0Wd;LX/0iY;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128328
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 2128329
    iput-object p1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    .line 2128330
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->k:Z

    .line 2128331
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 2128332
    iput-object p2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->p:LX/0Uh;

    .line 2128333
    iput-object p4, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->q:LX/0xX;

    .line 2128334
    iput-object p6, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->r:LX/0iY;

    .line 2128335
    new-instance v0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector$1;-><init>(Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;)V

    invoke-interface {p5, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2128336
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 2128325
    const-class v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2128326
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    .line 2128327
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2128321
    invoke-virtual {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v2

    .line 2128322
    sget-object v3, LX/EVS;->a:[I

    invoke-virtual {v2}, LX/3J8;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2128323
    if-ne p2, v2, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    .line 2128324
    :pswitch_0
    sget-object v2, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    if-ne p2, v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128316
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 2128317
    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->r:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->q:LX/0xX;

    sget-object v2, LX/1vy;->SOUND_ON:LX/1vy;

    invoke-virtual {v1, v2}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2128318
    if-eqz v1, :cond_0

    .line 2128319
    new-instance v1, LX/Bx2;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bx2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2128320
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128308
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2128309
    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2128310
    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2128311
    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->p:LX/0Uh;

    sget v2, LX/19n;->o:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2128312
    if-eqz v1, :cond_0

    .line 2128313
    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v3, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v4, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v3, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2128314
    :cond_0
    new-instance v1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2128315
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128307
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128306
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128301
    invoke-virtual {p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128305
    invoke-virtual {p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128304
    invoke-virtual {p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128303
    invoke-virtual {p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->f()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2128302
    invoke-virtual {p0}, Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;->f()LX/0Px;

    move-result-object v0

    return-object v0
.end method
