.class public Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private c:LX/2oO;

.field private d:LX/EVQ;

.field private e:Z

.field public f:Z

.field private n:LX/3HY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2128267
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128268
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128269
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128270
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2128271
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2128272
    iput-boolean v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->e:Z

    .line 2128273
    const v0, 0x7f03159a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2128274
    const v0, 0x7f0d2e38

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->a:Landroid/view/View;

    .line 2128275
    const v0, 0x7f0d1333

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->b:Landroid/view/View;

    .line 2128276
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/EVR;

    invoke-direct {v1, p0}, LX/EVR;-><init>(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2128277
    new-instance v0, LX/EVQ;

    invoke-direct {v0, p0}, LX/EVQ;-><init>(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->d:LX/EVQ;

    .line 2128278
    return-void
.end method

.method public static g(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2128279
    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    invoke-virtual {v0}, LX/2oO;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->n:LX/3HY;

    sget-object v2, LX/3HY;->REGULAR:LX/3HY;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2128280
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2128281
    return-void

    :cond_0
    move v0, v1

    .line 2128282
    goto :goto_0

    .line 2128283
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 2128284
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "AutoplayStateManager"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oO;

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    .line 2128285
    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    if-nez v0, :cond_0

    .line 2128286
    :goto_0
    return-void

    .line 2128287
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    iget-object v1, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->d:LX/EVQ;

    invoke-virtual {v0, v1}, LX/2oO;->a(LX/EVQ;)V

    .line 2128288
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2128289
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->e:Z

    .line 2128290
    invoke-virtual {p1}, LX/2pa;->f()LX/3HY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->n:LX/3HY;

    .line 2128291
    invoke-static {p0}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->g(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V

    goto :goto_0

    .line 2128292
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2128293
    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    if-eqz v0, :cond_0

    .line 2128294
    iget-object v0, p0, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->c:LX/2oO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2oO;->a(LX/EVQ;)V

    .line 2128295
    :cond_0
    return-void
.end method
