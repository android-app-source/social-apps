.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/videohome/environment/HasVideoHomePersistentState;",
        ":",
        "LX/7ze",
        "<",
        "Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;",
        ">;>",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2rI;",
        "LX/EUB;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/7zd;


# direct methods
.method public constructor <init>(LX/7zd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126412
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126413
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;->a:LX/7zd;

    .line 2126414
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;
    .locals 4

    .prologue
    .line 2126401
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    monitor-enter v1

    .line 2126402
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126403
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126404
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126405
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126406
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    const-class v3, LX/7zd;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/7zd;

    invoke-direct {p0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;-><init>(LX/7zd;)V

    .line 2126407
    move-object v0, p0

    .line 2126408
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126409
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126410
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126411
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2126385
    check-cast p2, LX/2rI;

    check-cast p3, LX/ETy;

    .line 2126386
    iget-object v0, p2, LX/2rI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p2, LX/2rI;->f:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, LX/ETy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/EUB;

    move-result-object v2

    .line 2126387
    iget-object v0, v2, LX/EUB;->c:LX/2oV;

    move-object v0, v0

    .line 2126388
    if-nez v0, :cond_0

    .line 2126389
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;->a:LX/7zd;

    iget-object v1, p2, LX/2rI;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p2, LX/2rI;->b:LX/093;

    iget-object v4, p2, LX/2rI;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, p2, LX/2rI;->d:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v6, p2, LX/2rI;->e:LX/04D;

    invoke-virtual/range {v0 .. v6}, LX/7zd;->a(Ljava/lang/String;LX/2oM;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;)LX/7zc;

    move-result-object v0

    .line 2126390
    iput-object v0, v2, LX/EUB;->c:LX/2oV;

    .line 2126391
    :cond_0
    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x26a60fe4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2126395
    check-cast p2, LX/EUB;

    check-cast p3, LX/ETy;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2126396
    move-object v1, p3

    check-cast v1, LX/7ze;

    invoke-interface {v1, p4}, LX/7ze;->a(Landroid/view/View;)V

    .line 2126397
    check-cast p3, LX/7ze;

    .line 2126398
    iget-object v1, p2, LX/EUB;->c:LX/2oV;

    move-object v1, v1

    .line 2126399
    invoke-interface {p3, p4, v1}, LX/7ze;->a(Landroid/view/View;LX/2oV;)V

    .line 2126400
    const/16 v1, 0x1f

    const v2, -0x7e9ec4d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2126392
    check-cast p3, LX/ETy;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2126393
    check-cast p3, LX/7ze;

    invoke-interface {p3, p4}, LX/7ze;->b(Landroid/view/View;)V

    .line 2126394
    return-void
.end method
