.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

.field private final f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126978
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2126979
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    .line 2126980
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    .line 2126981
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    .line 2126982
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    .line 2126983
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    .line 2126984
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    .line 2126985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;
    .locals 10

    .prologue
    .line 2126988
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;

    monitor-enter v1

    .line 2126989
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126990
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126991
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126992
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126993
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;)V

    .line 2126994
    move-object v0, v3

    .line 2126995
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126996
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126997
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2126999
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127000
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2127001
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2126986
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2126987
    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeItemSelectorPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
