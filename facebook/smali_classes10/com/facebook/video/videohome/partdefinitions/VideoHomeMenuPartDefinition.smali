.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ":",
        "LX/7Lk;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasVideoHomePersistentState;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127011
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2127012
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2127013
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;
    .locals 4

    .prologue
    .line 2127014
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    monitor-enter v1

    .line 2127015
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127016
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127017
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127018
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127019
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 2127020
    move-object v0, p0

    .line 2127021
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127022
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127023
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2127025
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127026
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127027
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2127028
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2127029
    const v1, 0x7f0d311f

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2127030
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2127031
    new-instance p2, LX/EUz;

    invoke-direct {p2, p0, p3, v0, v3}, LX/EUz;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    move-object v0, p2

    .line 2127032
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127033
    const/4 v0, 0x0

    return-object v0
.end method
