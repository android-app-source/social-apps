.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126767
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126768
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    .line 2126769
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;
    .locals 4

    .prologue
    .line 2126770
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    monitor-enter v1

    .line 2126771
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126772
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126775
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;)V

    .line 2126776
    move-object v0, p0

    .line 2126777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2126781
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2126782
    const v0, 0x7f0d0550

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    .line 2126783
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2126784
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2126785
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2126786
    const/4 v0, 0x0

    return-object v0
.end method
