.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETj",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;>",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2127034
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127035
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2127036
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    .line 2127037
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    .line 2127038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;
    .locals 5

    .prologue
    .line 2127039
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    monitor-enter v1

    .line 2127040
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127041
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127044
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;)V

    .line 2127045
    move-object v0, p0

    .line 2127046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 2

    .prologue
    .line 2127050
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127051
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2127052
    sget-object v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2127053
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    check-cast p3, LX/ETj;

    .line 2127054
    invoke-interface {p3, p2}, LX/ETj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127055
    if-eqz v0, :cond_0

    .line 2127056
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 2127057
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2127058
    if-eqz v0, :cond_1

    .line 2127059
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127060
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127061
    const/4 v0, 0x0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2127062
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollCreatorSpaceGroupPartDefinition;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method
