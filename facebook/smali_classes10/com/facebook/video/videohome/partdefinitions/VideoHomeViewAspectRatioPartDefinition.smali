.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeViewAspectRatioPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Float;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/view/Display;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128241
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2128242
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeViewAspectRatioPartDefinition;->a:Landroid/view/Display;

    .line 2128243
    return-void
.end method

.method private a(Ljava/lang/Float;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2128244
    const-string v0, "VideoHomeViewAspectRatioPartDefinition.bind"

    const v1, 0x553e1f18

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2128245
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2128246
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 2128247
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2128248
    iget-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeViewAspectRatioPartDefinition;->a:Landroid/view/Display;

    invoke-virtual {p1, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2128249
    move-object v1, v1

    .line 2128250
    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 2128251
    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-int v1, v1

    .line 2128252
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 2128253
    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2128254
    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2128255
    :cond_0
    const v0, -0x1839a3c3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2128256
    return-void

    .line 2128257
    :catchall_0
    move-exception v0

    const v1, 0x2326bdec

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7a4216b4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128258
    check-cast p1, Ljava/lang/Float;

    invoke-direct {p0, p1, p4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeViewAspectRatioPartDefinition;->a(Ljava/lang/Float;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, -0x58f701e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
