.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETj",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        ">;>",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

.field private final c:LX/ETB;

.field private final d:LX/EVb;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;LX/ETB;LX/EVb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127703
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2127704
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    .line 2127705
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    .line 2127706
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->c:LX/ETB;

    .line 2127707
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->d:LX/EVb;

    .line 2127708
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;
    .locals 7

    .prologue
    .line 2127723
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    monitor-enter v1

    .line 2127724
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127725
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127726
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127727
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127728
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    invoke-static {v0}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v5

    check-cast v5, LX/ETB;

    invoke-static {v0}, LX/EVb;->a(LX/0QB;)LX/EVb;

    move-result-object v6

    check-cast v6, LX/EVb;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;LX/ETB;LX/EVb;)V

    .line 2127729
    move-object v0, p0

    .line 2127730
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127731
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127732
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2127711
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/ETj;

    .line 2127712
    const/4 v1, 0x1

    .line 2127713
    invoke-interface {p3, p2}, LX/ETj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127714
    if-nez v0, :cond_1

    move v0, v1

    .line 2127715
    :goto_0
    move v0, v0

    .line 2127716
    if-eqz v0, :cond_0

    .line 2127717
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127718
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127719
    const/4 v0, 0x0

    return-object v0

    .line 2127720
    :cond_1
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v2

    .line 2127721
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2127722
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v2, v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 2127710
    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->c:LX/ETB;

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->d:LX/EVb;

    invoke-static {p1, v0, v1}, LX/EV8;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETB;LX/EVb;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2127709
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {p0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderGroupPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
