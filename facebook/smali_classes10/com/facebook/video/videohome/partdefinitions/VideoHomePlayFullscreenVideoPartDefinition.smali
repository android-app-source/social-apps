.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3FR;",
        ":",
        "LX/3FT;",
        "E::",
        "LX/ETa",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;:",
        "LX/ETb;",
        ":",
        "LX/ETh;",
        ":",
        "LX/ETi;",
        ":",
        "Ljava/lang/Object;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/ETj;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasRequestInformation;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasVideoHomePersistentState;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1qa;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2xj;

.field public final d:LX/1C2;

.field private final e:LX/0xX;

.field public final f:LX/3AW;


# direct methods
.method public constructor <init>(LX/1qa;LX/0Ot;LX/2xj;LX/1C2;LX/0xX;LX/3AW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qa;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/2xj;",
            "LX/1C2;",
            "LX/0xX;",
            "LX/3AW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127647
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2127648
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a:LX/1qa;

    .line 2127649
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->b:LX/0Ot;

    .line 2127650
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    .line 2127651
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->d:LX/1C2;

    .line 2127652
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->e:LX/0xX;

    .line 2127653
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->f:LX/3AW;

    .line 2127654
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;
    .locals 10

    .prologue
    .line 2127655
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    monitor-enter v1

    .line 2127656
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127657
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127658
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127659
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127660
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const/16 v5, 0x848

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v6

    check-cast v6, LX/2xj;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v7

    check-cast v7, LX/1C2;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v8

    check-cast v8, LX/0xX;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v9

    check-cast v9, LX/3AW;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;-><init>(LX/1qa;LX/0Ot;LX/2xj;LX/1C2;LX/0xX;LX/3AW;)V

    .line 2127661
    move-object v0, v3

    .line 2127662
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127663
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127664
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2127633
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/ETa;

    const/4 v2, -0x1

    const/4 v10, 0x0

    .line 2127634
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127635
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2127636
    if-nez v1, :cond_1

    .line 2127637
    :cond_0
    :goto_0
    return-object v10

    .line 2127638
    :cond_1
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v0

    .line 2127639
    move-object v0, p3

    .line 2127640
    check-cast v0, LX/ETi;

    invoke-interface {v0, v3}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v5

    .line 2127641
    if-eq v5, v2, :cond_0

    move-object v0, p3

    .line 2127642
    check-cast v0, LX/ETi;

    invoke-interface {v0, v3, v1}, LX/ETi;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v6

    .line 2127643
    if-eq v6, v2, :cond_0

    .line 2127644
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v0, p3

    .line 2127645
    check-cast v0, LX/ETy;

    invoke-virtual {v0, v2, v3}, LX/ETy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/EUB;

    move-result-object v4

    .line 2127646
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/1Nt;

    new-instance v9, LX/3FZ;

    new-instance v0, LX/EVB;

    move-object v1, p0

    move-object v3, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, LX/EVB;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUB;IILX/ETa;)V

    invoke-direct {v9, v0}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v8, v9}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
