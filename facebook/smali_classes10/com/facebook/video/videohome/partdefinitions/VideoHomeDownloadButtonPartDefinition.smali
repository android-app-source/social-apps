.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "LX/EUs;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/19w;

.field private final b:LX/16U;

.field public final c:Landroid/os/Handler;

.field private final d:LX/D7Y;


# direct methods
.method public constructor <init>(LX/16U;LX/19w;LX/D7Y;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2126802
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126803
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->b:LX/16U;

    .line 2126804
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->a:LX/19w;

    .line 2126805
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->c:Landroid/os/Handler;

    .line 2126806
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->d:LX/D7Y;

    .line 2126807
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;
    .locals 6

    .prologue
    .line 2126808
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;

    monitor-enter v1

    .line 2126809
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126810
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126811
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126812
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126813
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v3

    check-cast v3, LX/16U;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v4

    check-cast v4, LX/19w;

    const-class v5, LX/D7Y;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/D7Y;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;-><init>(LX/16U;LX/19w;LX/D7Y;)V

    .line 2126814
    move-object v0, p0

    .line 2126815
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126816
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126817
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;LX/2fs;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2126819
    iget-object v0, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    move-object v0, v0

    .line 2126820
    iget-object v1, p0, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    move-object v1, v1

    .line 2126821
    iget-object v2, p1, LX/2fs;->c:LX/1A0;

    .line 2126822
    sget-object v3, LX/EUr;->a:[I

    invoke-virtual {v2}, LX/1A0;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2126823
    :goto_0
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2126824
    invoke-virtual {p0, v4}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->setVisibility(I)V

    .line 2126825
    return-void

    .line 2126826
    :pswitch_0
    const v2, 0x7f0207d6

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2126827
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a008a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2126828
    invoke-virtual {v1, v5}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    goto :goto_0

    .line 2126829
    :pswitch_1
    const v2, 0x7f02081c

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2126830
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2126831
    invoke-static {p1}, LX/15V;->a(LX/2fs;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 2126832
    invoke-virtual {v1, v4}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    goto :goto_0

    .line 2126833
    :pswitch_2
    const v2, 0x7f020841

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2126834
    invoke-virtual {p0}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2126835
    invoke-virtual {v1, v5}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2126836
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2126837
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a()Ljava/lang/String;

    move-result-object v0

    .line 2126838
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2126839
    new-instance v2, LX/EUt;

    invoke-direct {v2, p0, v0}, LX/EUt;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;Ljava/lang/String;)V

    .line 2126840
    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->b:LX/16U;

    const-class v4, LX/1ub;

    invoke-virtual {v3, v4, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 2126841
    new-instance v3, LX/EUs;

    iget-object v4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->d:LX/D7Y;

    sget-object v5, LX/04D;->VIDEO_HOME:LX/04D;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v0, v1, v6}, LX/D7Y;->a(LX/04D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Z)LX/D7X;

    move-result-object v0

    invoke-direct {v3, v0, v2}, LX/EUs;-><init>(Landroid/view/View$OnClickListener;LX/16X;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1c312788

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2126842
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    check-cast p2, LX/EUs;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;

    .line 2126843
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2126844
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2126845
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->setVisibility(I)V

    .line 2126846
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x200aa52d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2126847
    :cond_1
    iget-object v1, p2, LX/EUs;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2126848
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->a:LX/19w;

    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 2126849
    invoke-static {p4, v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->a(Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;LX/2fs;)V

    .line 2126850
    iget-object v1, p2, LX/EUs;->a:LX/16X;

    check-cast v1, LX/EUt;

    .line 2126851
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/EUt;->d:Z

    .line 2126852
    iput-object p4, v1, LX/EUt;->c:Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;

    .line 2126853
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 3

    .prologue
    .line 2126854
    check-cast p2, LX/EUs;

    .line 2126855
    if-eqz p2, :cond_0

    iget-object v0, p2, LX/EUs;->a:LX/16X;

    if-eqz v0, :cond_0

    .line 2126856
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;->b:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p2, LX/EUs;->a:LX/16X;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 2126857
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2126858
    check-cast p2, LX/EUs;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;

    .line 2126859
    iget-object v0, p2, LX/EUs;->a:LX/16X;

    check-cast v0, LX/EUt;

    .line 2126860
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/EUt;->d:Z

    .line 2126861
    const/4 p0, 0x0

    iput-object p0, v0, LX/EUt;->c:Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;

    .line 2126862
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/video/videohome/views/VideoHomeDownloadButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2126863
    return-void
.end method
