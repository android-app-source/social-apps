.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EUh;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final b:Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126421
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126422
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 2126423
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->b:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2126424
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2126425
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;
    .locals 6

    .prologue
    .line 2126426
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;

    monitor-enter v1

    .line 2126427
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126428
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126429
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126430
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126431
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;-><init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2126432
    move-object v0, p0

    .line 2126433
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126434
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126435
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2126437
    check-cast p2, LX/EUh;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2126438
    iget-object v4, p2, LX/EUh;->a:LX/9uc;

    .line 2126439
    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v5

    .line 2126440
    invoke-interface {v4}, LX/9uc;->cg()LX/0Px;

    move-result-object v6

    .line 2126441
    const v7, 0x7f0d3110

    iget-object v8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->b:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz v5, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v7, v8, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2126442
    const v0, 0x7f0d3111

    iget-object v7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->b:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2126443
    if-eqz v5, :cond_0

    .line 2126444
    const v0, 0x7f0d0905

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2126445
    :cond_0
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2126446
    const v7, 0x7f0d3111

    iget-object v8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBadgeWithTextAndFacepilePartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    new-instance v0, LX/8Cj;

    .line 2126447
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2126448
    invoke-interface {v4}, LX/9uc;->cg()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v9, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;

    .line 2126449
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->d()LX/5sY;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2126450
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->d()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2126451
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 2126452
    :cond_2
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2126453
    const/4 v3, 0x3

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v4, p2, LX/EUh;->b:Ljava/lang/Float;

    if-eqz v4, :cond_6

    iget-object v4, p2, LX/EUh;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 2126454
    :goto_3
    new-instance v6, LX/4Ab;

    invoke-direct {v6}, LX/4Ab;-><init>()V

    const/4 v9, 0x1

    .line 2126455
    iput-boolean v9, v6, LX/4Ab;->b:Z

    .line 2126456
    move-object v6, v6

    .line 2126457
    invoke-virtual {v6, v4}, LX/4Ab;->c(F)LX/4Ab;

    move-result-object v6

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const p0, 0x7f0a06e3

    invoke-virtual {v9, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 2126458
    iput v9, v6, LX/4Ab;->f:I

    .line 2126459
    move-object v6, v6

    .line 2126460
    move-object v4, v6

    .line 2126461
    iget-object v5, p2, LX/EUh;->c:Ljava/lang/Integer;

    iget-object v6, p2, LX/EUh;->d:Ljava/lang/Integer;

    invoke-direct/range {v0 .. v6}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-interface {p1, v7, v8, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2126462
    :cond_3
    return-object v2

    :cond_4
    move v0, v3

    .line 2126463
    goto/16 :goto_0

    :cond_5
    move v1, v3

    .line 2126464
    goto/16 :goto_1

    .line 2126465
    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method
