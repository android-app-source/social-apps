.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETd",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;:",
        "LX/ETb;",
        ":",
        "LX/ETi;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EUn;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition",
            "<TE;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/D4u;

.field public final d:LX/3AW;

.field public final e:LX/2xj;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;LX/D4u;LX/3AW;LX/2xj;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126701
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126702
    const-class v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a:Ljava/lang/String;

    .line 2126703
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    .line 2126704
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->c:LX/D4u;

    .line 2126705
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->d:LX/3AW;

    .line 2126706
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->e:LX/2xj;

    .line 2126707
    return-void
.end method

.method private a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;Ljava/lang/String;)LX/3Qw;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2126606
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->c:LX/D4u;

    const/4 v4, 0x0

    .line 2126607
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2126608
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2126609
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 2126610
    :goto_2
    if-nez v2, :cond_3

    if-nez v5, :cond_3

    .line 2126611
    :goto_3
    move-object v2, v4

    .line 2126612
    const/4 p0, 0x0

    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 2126613
    if-nez v2, :cond_4

    .line 2126614
    :goto_4
    move-object v1, v5

    .line 2126615
    invoke-static {v0, v3, v1, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D4s;Ljava/lang/String;)LX/3Qw;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, v4

    .line 2126616
    goto :goto_0

    :cond_1
    move-object v5, v4

    .line 2126617
    goto :goto_1

    :cond_2
    move-object v6, v4

    .line 2126618
    goto :goto_2

    .line 2126619
    :cond_3
    new-instance v4, LX/A63;

    invoke-direct {v4}, LX/A63;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object p0

    .line 2126620
    iput-object p0, v4, LX/A63;->a:Ljava/lang/String;

    .line 2126621
    move-object v4, v4

    .line 2126622
    new-instance p0, LX/A67;

    invoke-direct {p0}, LX/A67;-><init>()V

    .line 2126623
    iput-object v5, p0, LX/A67;->a:Ljava/lang/String;

    .line 2126624
    move-object v5, p0

    .line 2126625
    invoke-virtual {v5}, LX/A67;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v5

    .line 2126626
    iput-object v5, v4, LX/A63;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 2126627
    move-object v4, v4

    .line 2126628
    new-instance v5, LX/A66;

    invoke-direct {v5}, LX/A66;-><init>()V

    .line 2126629
    iput-object v6, v5, LX/A66;->a:Ljava/lang/String;

    .line 2126630
    move-object v5, v5

    .line 2126631
    invoke-virtual {v5}, LX/A66;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v5

    .line 2126632
    iput-object v5, v4, LX/A63;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 2126633
    move-object v4, v4

    .line 2126634
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->gN_()Z

    move-result v5

    .line 2126635
    iput-boolean v5, v4, LX/A63;->e:Z

    .line 2126636
    move-object v4, v4

    .line 2126637
    new-instance v5, LX/A65;

    invoke-direct {v5}, LX/A65;-><init>()V

    .line 2126638
    iput-object v2, v5, LX/A65;->a:Ljava/lang/String;

    .line 2126639
    move-object v2, v5

    .line 2126640
    invoke-virtual {v2}, LX/A65;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v2

    .line 2126641
    iput-object v2, v4, LX/A63;->c:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 2126642
    move-object v2, v4

    .line 2126643
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->c()I

    move-result v4

    .line 2126644
    iput v4, v2, LX/A63;->b:I

    .line 2126645
    move-object v2, v2

    .line 2126646
    invoke-virtual {v2}, LX/A63;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    move-result-object v4

    goto :goto_3

    .line 2126647
    :cond_4
    new-instance v8, LX/D4t;

    invoke-direct {v8, v1, v2, v3}, LX/D4t;-><init>(LX/D4u;LX/9rj;Ljava/lang/String;)V

    .line 2126648
    invoke-interface {v2}, LX/9rj;->k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, LX/9rj;->k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2126649
    :goto_5
    invoke-interface {v2}, LX/9rj;->j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-interface {v2}, LX/9rj;->j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 2126650
    :goto_6
    invoke-interface {v2}, LX/9rj;->d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, LX/9rj;->d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2126651
    :cond_5
    invoke-interface {v2}, LX/9rj;->c()I

    move-result v9

    .line 2126652
    invoke-interface {v2}, LX/9rj;->gO_()I

    move-result v10

    .line 2126653
    const-string v7, ""

    .line 2126654
    if-lez v9, :cond_6

    .line 2126655
    if-le v9, v10, :cond_9

    iget-object v7, v1, LX/D4u;->a:Landroid/content/res/Resources;

    const v9, 0x7f082246

    new-array v11, p1, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v11, p0

    invoke-virtual {v7, v9, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2126656
    :cond_6
    :goto_7
    iget-object v9, v1, LX/D4u;->c:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f08224a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2126657
    iget-object v10, v1, LX/D4u;->c:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f082249

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2126658
    new-instance v11, LX/D4r;

    invoke-direct {v11}, LX/D4r;-><init>()V

    invoke-interface {v2}, LX/9rj;->b()Ljava/lang/String;

    move-result-object p0

    .line 2126659
    iput-object p0, v11, LX/D4r;->a:Ljava/lang/String;

    .line 2126660
    move-object v11, v11

    .line 2126661
    iput-object v4, v11, LX/D4r;->c:Ljava/lang/String;

    .line 2126662
    move-object v4, v11

    .line 2126663
    iput-object v6, v4, LX/D4r;->d:Ljava/lang/String;

    .line 2126664
    move-object v4, v4

    .line 2126665
    iput-object v7, v4, LX/D4r;->e:Ljava/lang/String;

    .line 2126666
    move-object v4, v4

    .line 2126667
    iput-object v5, v4, LX/D4r;->f:Ljava/lang/String;

    .line 2126668
    move-object v4, v4

    .line 2126669
    invoke-interface {v2}, LX/9rj;->gN_()Z

    move-result v5

    .line 2126670
    iput-boolean v5, v4, LX/D4r;->g:Z

    .line 2126671
    move-object v4, v4

    .line 2126672
    iput-boolean p1, v4, LX/D4r;->i:Z

    .line 2126673
    move-object v4, v4

    .line 2126674
    iput-object v9, v4, LX/D4r;->k:Ljava/lang/String;

    .line 2126675
    move-object v4, v4

    .line 2126676
    iput-object v10, v4, LX/D4r;->l:Ljava/lang/String;

    .line 2126677
    move-object v4, v4

    .line 2126678
    iput-object v8, v4, LX/D4r;->m:LX/BUv;

    .line 2126679
    move-object v4, v4

    .line 2126680
    invoke-virtual {v4}, LX/D4r;->a()LX/D4s;

    move-result-object v5

    goto/16 :goto_4

    :cond_7
    move-object v4, v5

    .line 2126681
    goto/16 :goto_5

    :cond_8
    move-object v6, v5

    .line 2126682
    goto :goto_6

    .line 2126683
    :cond_9
    iget-object v7, v1, LX/D4u;->a:Landroid/content/res/Resources;

    const v10, 0x7f082245

    new-array v11, p1, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v11, p0

    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_7
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D4s;Ljava/lang/String;)LX/3Qw;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D4s;",
            "Ljava/lang/String;",
            ")",
            "LX/3Qw;"
        }
    .end annotation

    .prologue
    .line 2126684
    new-instance v0, LX/3Qv;

    invoke-direct {v0}, LX/3Qv;-><init>()V

    .line 2126685
    iput-object p2, v0, LX/3Qv;->k:LX/D4s;

    .line 2126686
    move-object v0, v0

    .line 2126687
    iput-object p1, v0, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2126688
    move-object v0, v0

    .line 2126689
    invoke-virtual {v0, p0}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v0

    const-string v1, "VIDEO_HOME"

    .line 2126690
    iput-object v1, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 2126691
    move-object v0, v0

    .line 2126692
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 2126693
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 2126694
    move-object v0, v0

    .line 2126695
    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    .line 2126696
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 2126697
    move-object v0, v0

    .line 2126698
    iput-object p3, v0, LX/3Qv;->o:Ljava/lang/String;

    .line 2126699
    move-object v0, v0

    .line 2126700
    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Ljava/lang/String;)LX/3Qw;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2126605
    const/4 v0, 0x0

    invoke-static {p1, p3}, LX/D4u;->a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)LX/D4s;

    move-result-object v1

    invoke-static {p0, v0, v1, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D4s;Ljava/lang/String;)LX/3Qw;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/ETd;Ljava/lang/String;LX/EUn;)LX/D6L;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/String;",
            "LX/EUn;",
            ")",
            "LX/D6L;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2126598
    iget-object v0, p3, LX/EUn;->c:LX/0JD;

    if-nez v0, :cond_0

    .line 2126599
    const/4 v0, 0x0

    .line 2126600
    :goto_0
    return-object v0

    .line 2126601
    :cond_0
    check-cast p1, LX/ETi;

    iget-object v0, p3, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126602
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2126603
    invoke-interface {p1, v0}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v1

    .line 2126604
    new-instance v0, LX/EUi;

    invoke-direct {v0, p0, p3, p2, v1}, LX/EUi;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/EUn;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;
    .locals 7

    .prologue
    .line 2126587
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    monitor-enter v1

    .line 2126588
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126589
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126590
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126591
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126592
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    invoke-static {v0}, LX/D4u;->a(LX/0QB;)LX/D4u;

    move-result-object v4

    check-cast v4, LX/D4u;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v5

    check-cast v5, LX/3AW;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v6

    check-cast v6, LX/2xj;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;LX/D4u;LX/3AW;LX/2xj;)V

    .line 2126593
    move-object v0, p0

    .line 2126594
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126595
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126596
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2126537
    check-cast p2, LX/EUn;

    check-cast p3, LX/ETd;

    const/4 v1, 0x0

    .line 2126538
    iget-object v0, p2, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126539
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v2

    .line 2126540
    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    .line 2126541
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 2126542
    sget-object v4, LX/EUl;->a:[I

    iget-object v5, p2, LX/EUn;->b:LX/EUm;

    invoke-virtual {v5}, LX/EUm;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :cond_0
    move-object v0, v1

    move-object v2, v1

    .line 2126543
    :goto_0
    if-eqz v2, :cond_1

    .line 2126544
    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    new-instance v4, LX/D4q;

    .line 2126545
    new-instance v5, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance p2, LX/EUk;

    invoke-direct {p2, p0}, LX/EUk;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;)V

    invoke-direct {v5, p2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object v5, v5

    .line 2126546
    invoke-direct {v4, v2, v5, v0}, LX/D4q;-><init>(LX/3Qw;Ljava/util/concurrent/atomic/AtomicReference;LX/D6L;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2126547
    :cond_1
    return-object v1

    .line 2126548
    :pswitch_0
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2126549
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;Ljava/lang/String;)LX/3Qw;

    move-result-object v2

    .line 2126550
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/ETd;Ljava/lang/String;LX/EUn;)LX/D6L;

    move-result-object v0

    goto :goto_0

    .line 2126551
    :pswitch_1
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v3, v4, :cond_2

    .line 2126552
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2126553
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-static {v0}, LX/ESx;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2126554
    if-eqz v0, :cond_0

    .line 2126555
    invoke-static {v3, v0, v2, v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Ljava/lang/String;)LX/3Qw;

    move-result-object v2

    .line 2126556
    iget-object v6, p2, LX/EUn;->c:LX/0JD;

    if-nez v6, :cond_5

    .line 2126557
    const/4 v6, 0x0

    .line 2126558
    :goto_1
    move-object v0, v6

    .line 2126559
    goto :goto_0

    .line 2126560
    :cond_2
    iget-object v3, p2, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v4, 0x0

    .line 2126561
    if-nez v3, :cond_6

    .line 2126562
    :cond_3
    :goto_2
    move v3, v4

    .line 2126563
    if-eqz v3, :cond_0

    .line 2126564
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2126565
    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2126566
    invoke-interface {v0}, LX/9uc;->cl()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2126567
    invoke-interface {v0}, LX/9uc;->cl()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-static {v0}, LX/ESx;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    .line 2126568
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v4, v5, v2, v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Ljava/lang/String;)LX/3Qw;

    move-result-object v2

    .line 2126569
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/ETd;Ljava/lang/String;LX/EUn;)LX/D6L;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 2126570
    goto :goto_3

    .line 2126571
    :pswitch_2
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2126572
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2126573
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2126574
    const/4 v4, 0x0

    invoke-static {v3, v0, v4, v2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D4s;Ljava/lang/String;)LX/3Qw;

    move-result-object v4

    move-object v2, v4

    .line 2126575
    invoke-direct {p0, p3, v3, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/ETd;Ljava/lang/String;LX/EUn;)LX/D6L;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move-object v6, p3

    .line 2126576
    check-cast v6, LX/ETi;

    iget-object v7, p2, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126577
    iget-object v8, v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v7, v8

    .line 2126578
    invoke-interface {v6, v7}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v11

    .line 2126579
    iget-object v6, p2, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126580
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v7

    .line 2126581
    invoke-interface {v6}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v10

    .line 2126582
    new-instance v6, LX/EUj;

    move-object v7, p0

    move-object v8, p3

    move-object v9, p2

    invoke-direct/range {v6 .. v11}, LX/EUj;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/ETd;LX/EUn;Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;I)V

    goto/16 :goto_1

    .line 2126583
    :cond_6
    iget-object v5, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2126584
    invoke-interface {v5}, LX/9uc;->cl()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v5

    .line 2126585
    if-eqz v5, :cond_3

    .line 2126586
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_3

    const/4 v4, 0x1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
