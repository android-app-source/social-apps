.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETX;",
        ":",
        "LX/ETe;",
        ":",
        "LX/ETh;",
        ":",
        "Ljava/lang/Object;",
        ":",
        "LX/ETi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasRequestInformation;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasReactionSurfaceType;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasShownVideosContainer;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/EV3;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static p:LX/0Xm;


# instance fields
.field private final b:LX/14v;

.field private final c:LX/E3C;

.field private final d:LX/2dq;

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/1vo;

.field private final g:LX/Cfw;

.field private final h:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

.field public final i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

.field public final j:LX/03V;

.field private final k:LX/0xX;

.field public final l:LX/193;

.field private final m:LX/2xj;

.field public final n:LX/EVd;

.field private final o:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2127200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/14v;LX/E3C;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/193;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;LX/1vo;LX/Cfw;LX/0xX;LX/2xj;LX/EVd;Landroid/os/Handler;)V
    .locals 0
    .param p14    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127251
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2127252
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->j:LX/03V;

    .line 2127253
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->b:LX/14v;

    .line 2127254
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->c:LX/E3C;

    .line 2127255
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->h:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    .line 2127256
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->l:LX/193;

    .line 2127257
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->d:LX/2dq;

    .line 2127258
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2127259
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    .line 2127260
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->f:LX/1vo;

    .line 2127261
    iput-object p10, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->g:LX/Cfw;

    .line 2127262
    iput-object p11, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->k:LX/0xX;

    .line 2127263
    iput-object p12, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->m:LX/2xj;

    .line 2127264
    iput-object p13, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->n:LX/EVd;

    .line 2127265
    iput-object p14, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->o:Landroid/os/Handler;

    .line 2127266
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)I
    .locals 2

    .prologue
    .line 2127242
    instance-of v0, p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_1

    .line 2127243
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127244
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2127245
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 2127246
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2127247
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->c:LX/E3C;

    invoke-virtual {v1, p1, v0}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result v0

    .line 2127248
    :goto_1
    return v0

    .line 2127249
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 2127250
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->c:LX/E3C;

    invoke-virtual {v0, p1, p2}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)I

    move-result v0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2127212
    instance-of v0, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2127213
    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->a()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 2127214
    :goto_0
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    move-object v0, p2

    .line 2127215
    check-cast v0, LX/1Pr;

    new-instance v3, LX/EU1;

    .line 2127216
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v1

    .line 2127217
    move-object v1, p2

    check-cast v1, LX/ETy;

    .line 2127218
    iget-wide v9, v1, LX/ETy;->I:J

    move-wide v6, v9

    .line 2127219
    invoke-direct {v3, v5, v6, v7}, LX/EU1;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v3, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EU2;

    .line 2127220
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127221
    iget-object v6, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 2127222
    invoke-interface {v6}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v7

    .line 2127223
    iget-object v8, v0, LX/EU2;->a:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    move v7, v8

    .line 2127224
    if-nez v7, :cond_1

    .line 2127225
    invoke-interface {v6}, LX/9uc;->ck()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    if-eq v7, v8, :cond_0

    instance-of v7, p2, LX/ETg;

    if-nez v7, :cond_0

    invoke-interface {v6}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {p0, p2, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;LX/ETX;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2127226
    :cond_0
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2127227
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2127228
    :cond_2
    invoke-static {p1}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v2

    .line 2127229
    if-nez v2, :cond_4

    .line 2127230
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2127231
    :goto_2
    move-object v0, v0

    .line 2127232
    move-object v2, v0

    goto :goto_0

    .line 2127233
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2127234
    :cond_4
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2127235
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2127236
    new-instance v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127237
    iget-object v6, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2127238
    iget-object v7, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2127239
    invoke-direct {v5, v0, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2127240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2127241
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_2
.end method

.method private a(LX/EU1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;)LX/2eJ;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EU1;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2127201
    invoke-static {p0, p2, p3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;)LX/0Px;

    move-result-object v2

    move-object v0, p3

    .line 2127202
    check-cast v0, LX/1Pr;

    new-instance v3, LX/EU1;

    .line 2127203
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v1

    .line 2127204
    move-object v1, p3

    check-cast v1, LX/ETy;

    .line 2127205
    iget-wide v8, v1, LX/ETy;->I:J

    move-wide v6, v8

    .line 2127206
    invoke-direct {v3, v4, v6, v7}, LX/EU1;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EU2;

    .line 2127207
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    .line 2127208
    iput v0, v6, LX/EU2;->b:I

    .line 2127209
    iput-object v2, v6, LX/EU2;->e:LX/0Px;

    .line 2127210
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2127211
    new-instance v0, LX/EV1;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, LX/EV1;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;LX/0Px;Ljava/lang/ref/WeakReference;LX/EU1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EU2;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;
    .locals 3

    .prologue
    .line 2127117
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    monitor-enter v1

    .line 2127118
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127119
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127120
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127121
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->b(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127122
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127123
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 2127195
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127196
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2127197
    sget-object v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2127198
    const/4 v0, 0x0

    .line 2127199
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/EV8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;LX/ETX;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 5
    .param p1    # LX/ETX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2127267
    check-cast p1, LX/ETy;

    .line 2127268
    iget-object v2, p1, LX/ETy;->t:LX/ETk;

    move-object v2, v2

    .line 2127269
    if-eqz v2, :cond_0

    if-nez p2, :cond_1

    .line 2127270
    :cond_0
    :goto_0
    return v0

    .line 2127271
    :cond_1
    invoke-static {p2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2127272
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2127273
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    .line 2127274
    iget-object p1, v2, LX/ETk;->a:Ljava/util/Set;

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    move v2, p1

    .line 2127275
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->b:LX/14v;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2127276
    :cond_3
    const-string v2, "VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition"

    const-string v3, "Attachment or video is null for story %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;LX/EU2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "LX/EU2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2127178
    iget v0, p3, LX/EU2;->b:I

    move v0, v0

    .line 2127179
    iget v1, p3, LX/EU2;->c:I

    move v1, v1

    .line 2127180
    sub-int/2addr v0, v1

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 2127181
    :cond_0
    :goto_0
    return-void

    .line 2127182
    :cond_1
    instance-of v0, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2127183
    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127184
    iget-object v1, v0, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    move-object v0, v1

    .line 2127185
    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127186
    iget-boolean v0, p3, LX/EU2;->d:Z

    move v0, v0

    .line 2127187
    if-nez v0, :cond_0

    .line 2127188
    new-instance v0, LX/EV2;

    invoke-direct {v0, p0, p3}, LX/EV2;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;LX/EU2;)V

    .line 2127189
    invoke-interface {p2, p1, v0}, LX/ETX;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETO;)V

    .line 2127190
    const/4 v0, 0x1

    .line 2127191
    iput-boolean v0, p3, LX/EU2;->d:Z

    .line 2127192
    goto :goto_0

    .line 2127193
    :cond_2
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127194
    check-cast v0, LX/9uf;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v0

    goto :goto_1
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;
    .locals 15

    .prologue
    .line 2127176
    new-instance v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v2

    check-cast v2, LX/14v;

    invoke-static {p0}, LX/E3C;->a(LX/0QB;)LX/E3C;

    move-result-object v3

    check-cast v3, LX/E3C;

    invoke-static {p0}, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    const-class v5, LX/193;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/193;

    invoke-static {p0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v6

    check-cast v6, LX/2dq;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    invoke-static {p0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v9

    check-cast v9, LX/1vo;

    invoke-static {p0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v10

    check-cast v10, LX/Cfw;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v11

    check-cast v11, LX/0xX;

    invoke-static {p0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v12

    check-cast v12, LX/2xj;

    invoke-static {p0}, LX/EVd;->a(LX/0QB;)LX/EVd;

    move-result-object v13

    check-cast v13, LX/EVd;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v14

    check-cast v14, Landroid/os/Handler;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;-><init>(LX/03V;LX/14v;LX/E3C;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/193;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;LX/1vo;LX/Cfw;LX/0xX;LX/2xj;LX/EVd;Landroid/os/Handler;)V

    .line 2127177
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2127175
    sget-object v0, LX/2eA;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2127155
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/ETX;

    .line 2127156
    new-instance v3, LX/EU1;

    .line 2127157
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v0

    .line 2127158
    move-object v0, p3

    check-cast v0, LX/ETy;

    .line 2127159
    iget-wide v9, v0, LX/ETy;->I:J

    move-wide v4, v9

    .line 2127160
    invoke-direct {v3, v1, v4, v5}, LX/EU1;-><init>(Ljava/lang/String;J)V

    move-object v0, p3

    .line 2127161
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/EU2;

    move-object v0, p3

    .line 2127162
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(Landroid/content/Context;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)I

    move-result v1

    move-object v0, p3

    .line 2127163
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2127164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->n:LX/EVd;

    invoke-virtual {v2}, LX/EVd;->d()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2127165
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->h:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v5, LX/2eF;->a:LX/1Ua;

    invoke-direct {v2, v5}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2127166
    iget-object v7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->d:LX/2dq;

    int-to-float v1, v1

    const/high16 v5, 0x41000000    # 8.0f

    add-float/2addr v1, v5

    sget-object v5, LX/2eF;->a:LX/1Ua;

    const/4 v8, 0x1

    invoke-virtual {v2, v1, v5, v8}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    .line 2127167
    iget v2, v6, LX/EU2;->c:I

    move v2, v2

    .line 2127168
    invoke-direct {p0, v3, p2, p3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(LX/EU1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;)LX/2eJ;

    move-result-object v3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2127169
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->m:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->m:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2127170
    invoke-static {p0, p2, p3, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;LX/EU2;)V

    .line 2127171
    :cond_0
    new-instance v0, LX/EV3;

    .line 2127172
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->l:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "video_home_video_home_live_hscroll_perf"

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    .line 2127173
    new-instance v2, LX/5Mq;

    invoke-direct {v2, v1}, LX/5Mq;-><init>(LX/195;)V

    move-object v1, v2

    .line 2127174
    invoke-direct {v0, v1, v6}, LX/EV3;-><init>(LX/1OX;LX/EU2;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xf15538b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2127135
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/EV3;

    check-cast p3, LX/ETX;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;

    .line 2127136
    iget-object v1, p2, LX/EV3;->b:LX/EU2;

    .line 2127137
    iget-object v2, v1, LX/EU2;->e:LX/0Px;

    move-object v4, v2

    .line 2127138
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2127139
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0xfaebf1c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2127140
    :cond_1
    iget-object v5, p2, LX/EV3;->b:LX/EU2;

    .line 2127141
    iget v1, v5, LX/EU2;->c:I

    move v1, v1

    .line 2127142
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    move v2, v1

    :goto_1
    move-object v1, p3

    .line 2127143
    check-cast v1, LX/ETi;

    .line 2127144
    iget-object p0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object p0, p0

    .line 2127145
    invoke-interface {v1, p0}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result p0

    .line 2127146
    check-cast p3, LX/ETe;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127147
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v4

    .line 2127148
    invoke-interface {p3, v2, p0, v1, p1}, LX/ETe;->a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2127149
    iget-object v1, p2, LX/EV3;->a:LX/1OX;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2127150
    const/4 v1, 0x1

    .line 2127151
    iput-boolean v1, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 2127152
    iput-object p4, v5, LX/EU2;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2127153
    goto :goto_0

    .line 2127154
    :cond_2
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2127134
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2127125
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/EV3;

    check-cast p3, LX/ETX;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;

    const/4 v2, 0x0

    .line 2127126
    iget-object v0, p2, LX/EV3;->b:LX/EU2;

    .line 2127127
    check-cast p3, LX/ETe;

    invoke-interface {p3, p1}, LX/ETe;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2127128
    iget-object v1, p2, LX/EV3;->a:LX/1OX;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 2127129
    const/4 v1, 0x0

    .line 2127130
    iput-boolean v1, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 2127131
    iput-object v2, v0, LX/EU2;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2127132
    iput-object v2, v0, LX/EU2;->f:LX/0oB;

    .line 2127133
    return-void
.end method
