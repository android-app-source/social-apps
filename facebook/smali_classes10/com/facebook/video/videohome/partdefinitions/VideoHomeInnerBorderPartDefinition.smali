.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeInnerBorderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126976
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2126977
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2126975
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTopBorderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2126967
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126968
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2126969
    instance-of v0, v0, LX/9ud;

    if-nez v0, :cond_0

    .line 2126970
    const/4 v0, 0x0

    .line 2126971
    :goto_0
    return v0

    .line 2126972
    :cond_0
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2126973
    check-cast v0, LX/9ud;

    .line 2126974
    invoke-interface {v0}, LX/9ud;->aI()Z

    move-result v0

    goto :goto_0
.end method
