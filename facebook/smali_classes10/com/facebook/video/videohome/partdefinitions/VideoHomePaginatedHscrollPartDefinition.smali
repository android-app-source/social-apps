.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETX;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/3U7;",
        ":",
        "LX/ETe;",
        ":",
        "LX/ETi;",
        ":",
        "LX/ETj;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasReactionSurfaceType;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "LX/7Tw;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

.field public final c:LX/E3C;

.field private final d:LX/2xj;

.field private final e:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

.field public final f:LX/3AW;

.field public final g:LX/1V7;

.field public final h:Landroid/content/Context;

.field public final i:LX/1vo;

.field public final j:LX/2dq;

.field public final k:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2127397
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->l:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/1vo;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/E3C;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;LX/2xj;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/3AW;LX/1V7;LX/2dq;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127406
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2127407
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    .line 2127408
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2127409
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->c:LX/E3C;

    .line 2127410
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->d:LX/2xj;

    .line 2127411
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->e:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    .line 2127412
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->k:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;

    .line 2127413
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->f:LX/3AW;

    .line 2127414
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->g:LX/1V7;

    .line 2127415
    iput-object p11, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->h:Landroid/content/Context;

    .line 2127416
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->i:LX/1vo;

    .line 2127417
    iput-object p10, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->j:LX/2dq;

    .line 2127418
    return-void
.end method

.method private a(LX/E2b;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETX;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/2eJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E2b;",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            "TE;",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2127398
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    move-object v0, p3

    .line 2127399
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2b;

    .line 2127400
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2127401
    invoke-direct {v1, v3}, LX/E2b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/E2c;

    .line 2127402
    invoke-virtual {v2}, LX/ETQ;->size()I

    move-result v0

    .line 2127403
    iput v0, v7, LX/E2c;->c:I

    .line 2127404
    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2127405
    new-instance v0, LX/EV4;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p4

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, LX/EV4;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;Ljava/lang/ref/WeakReference;LX/E2b;LX/E2c;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;
    .locals 15

    .prologue
    .line 2127384
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    monitor-enter v1

    .line 2127385
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127386
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127389
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v4

    check-cast v4, LX/1vo;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/E3C;->a(LX/0QB;)LX/E3C;

    move-result-object v7

    check-cast v7, LX/E3C;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v9

    check-cast v9, LX/2xj;

    invoke-static {v0}, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v11

    check-cast v11, LX/3AW;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v12

    check-cast v12, LX/1V7;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v13

    check-cast v13, LX/2dq;

    const-class v14, Landroid/content/Context;

    invoke-interface {v0, v14}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/Context;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;-><init>(LX/1vo;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/E3C;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;LX/2xj;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/3AW;LX/1V7;LX/2dq;Landroid/content/Context;)V

    .line 2127390
    move-object v0, v3

    .line 2127391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETX;LX/E2c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            "TE;",
            "LX/E2c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2127372
    iget v0, p3, LX/E2c;->c:I

    move v0, v0

    .line 2127373
    iget v1, p3, LX/E2c;->d:I

    move v1, v1

    .line 2127374
    sub-int/2addr v0, v1

    const/16 v1, 0x8

    if-le v0, v1, :cond_1

    .line 2127375
    :cond_0
    :goto_0
    return-void

    .line 2127376
    :cond_1
    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127377
    iget-boolean v0, p3, LX/E2c;->e:Z

    move v0, v0

    .line 2127378
    if-nez v0, :cond_0

    .line 2127379
    new-instance v0, LX/EV5;

    invoke-direct {v0, p0, p3}, LX/EV5;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;LX/E2c;)V

    .line 2127380
    const/4 v1, 0x1

    .line 2127381
    iput-boolean v1, p3, LX/E2c;->e:Z

    .line 2127382
    invoke-interface {p2, p1, v0}, LX/ETX;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETO;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 1

    .prologue
    .line 2127395
    iget-object v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    move-object v0, v0

    .line 2127396
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2127383
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2127333
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    check-cast p3, LX/ETX;

    .line 2127334
    new-instance v3, LX/E2b;

    .line 2127335
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2127336
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2127337
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/E2c;

    .line 2127338
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2127339
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2127340
    :goto_0
    move-object v4, v0

    .line 2127341
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->e:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    .line 2127342
    sget-object v2, LX/EV7;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 2127343
    sget-object v2, LX/2eF;->a:LX/1Ua;

    :goto_1
    move-object v2, v2

    .line 2127344
    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2127345
    iget-object v8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v9, 0x1

    .line 2127346
    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->c:LX/E3C;

    invoke-virtual {v2, v1, v4}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result v2

    move v2, v2

    .line 2127347
    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v2

    .line 2127348
    sget-object v5, LX/EV7;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 2127349
    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->j:LX/2dq;

    int-to-float v2, v2

    const/high16 v6, 0x41000000    # 8.0f

    add-float/2addr v2, v6

    sget-object v6, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v5, v2, v6, v9}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 2127350
    iget v2, v7, LX/E2c;->d:I

    move v2, v2

    .line 2127351
    invoke-direct {p0, v3, p2, p3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a(LX/E2b;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETX;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x8

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;I)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2127352
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->d:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->d:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2127353
    invoke-static {p0, p2, p3, v7}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETX;LX/E2c;)V

    .line 2127354
    :cond_0
    check-cast p3, LX/ETi;

    .line 2127355
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2127356
    invoke-interface {p3, v0}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v0

    .line 2127357
    new-instance v1, LX/EV6;

    invoke-direct {v1, p0, p2, v0}, LX/EV6;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;Lcom/facebook/video/videohome/data/VideoHomeItem;I)V

    move-object v0, v1

    .line 2127358
    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2127359
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 2127360
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    goto/16 :goto_0

    .line 2127361
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b16a1

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2127362
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->g:LX/1V7;

    invoke-virtual {v6}, LX/1V7;->e()F

    move-result v6

    neg-float v6, v6

    .line 2127363
    iput v6, v5, LX/1UY;->c:F

    .line 2127364
    move-object v5, v5

    .line 2127365
    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->g:LX/1V7;

    invoke-virtual {v6}, LX/1V7;->d()F

    move-result v6

    neg-float v6, v6

    .line 2127366
    iput v6, v5, LX/1UY;->b:F

    .line 2127367
    move-object v5, v5

    .line 2127368
    iput v2, v5, LX/1UY;->d:F

    .line 2127369
    move-object v2, v5

    .line 2127370
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    goto/16 :goto_1

    .line 2127371
    :pswitch_1
    invoke-static {v2, v9, v9}, LX/2eF;->a(IZZ)LX/2eF;

    move-result-object v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5ed16b6e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2127328
    check-cast p2, LX/7Tw;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2127329
    const/4 v1, 0x1

    .line 2127330
    iput-boolean v1, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 2127331
    iput-object p2, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->o:LX/7Tw;

    .line 2127332
    const/16 v1, 0x1f

    const v2, -0x5f539446

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2127324
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127325
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127326
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2127327
    sget-object v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->l:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2127318
    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2127319
    const/4 v0, 0x0

    .line 2127320
    iput-boolean v0, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 2127321
    const/4 v0, 0x0

    .line 2127322
    iput-object v0, p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->o:LX/7Tw;

    .line 2127323
    return-void
.end method
