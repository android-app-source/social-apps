.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128006
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2128007
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 2128008
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2128009
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2128010
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;
    .locals 6

    .prologue
    .line 2128011
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    monitor-enter v1

    .line 2128012
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2128013
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2128014
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128015
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2128016
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;-><init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 2128017
    move-object v0, p0

    .line 2128018
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2128019
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2128020
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2128021
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2128022
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v7, 0x0

    .line 2128023
    const/4 v0, 0x0

    .line 2128024
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2128025
    if-nez v1, :cond_5

    .line 2128026
    :cond_0
    :goto_0
    move v1, v0

    .line 2128027
    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2128028
    if-eqz v1, :cond_3

    .line 2128029
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2128030
    invoke-interface {v0}, LX/9uc;->dp()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;

    move-result-object v0

    .line 2128031
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2128032
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;->a()LX/0Px;

    move-result-object v1

    .line 2128033
    if-eqz v1, :cond_2

    .line 2128034
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel$VideoNotificationContextProfilesModel;

    .line 2128035
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel$VideoNotificationContextProfilesModel;->b()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel$VideoNotificationContextProfilesModel;->b()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2128036
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel$VideoNotificationContextProfilesModel;->b()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2128037
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2128038
    const v2, 0x7f0d3129

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    new-instance v4, LX/8Cj;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v6

    invoke-direct {v4, v1, v7, v5, v6}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2128039
    const v1, 0x7f0d02a7

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2128040
    :cond_3
    return-object v7

    .line 2128041
    :cond_4
    const/4 v0, 0x4

    goto :goto_1

    .line 2128042
    :cond_5
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2128043
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2128044
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2128045
    invoke-interface {v2}, LX/9uc;->dp()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;

    move-result-object v2

    .line 2128046
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;->b()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method
