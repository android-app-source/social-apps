.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETj",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;>",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final e:LX/EVb;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;LX/0Ot;LX/EVb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;",
            ">;",
            "LX/EVb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127871
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2127872
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a:LX/0Ot;

    .line 2127873
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->b:LX/0Ot;

    .line 2127874
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->c:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    .line 2127875
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->d:LX/0Ot;

    .line 2127876
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->e:LX/EVb;

    .line 2127877
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;
    .locals 9

    .prologue
    .line 2127878
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    monitor-enter v1

    .line 2127879
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127880
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127881
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127882
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127883
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;

    const/16 v4, 0x3833

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x137b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    const/16 v7, 0x1070

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/EVb;->a(LX/0QB;)LX/EVb;

    move-result-object v8

    check-cast v8, LX/EVb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;LX/0Ot;LX/EVb;)V

    .line 2127884
    move-object v0, v3

    .line 2127885
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127886
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127887
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 3
    .param p0    # Lcom/facebook/video/videohome/data/VideoHomeItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2127889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x0

    .line 2127890
    if-nez p0, :cond_1

    .line 2127891
    :cond_0
    :goto_0
    move v0, v1

    .line 2127892
    return v0

    .line 2127893
    :cond_1
    iget-object v2, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2127894
    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    .line 2127895
    if-ne v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2127896
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    check-cast p3, LX/ETj;

    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2127897
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2127898
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127899
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2127900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v0, :cond_0

    .line 2127901
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127902
    invoke-interface {v0}, LX/9uc;->aK()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2127903
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2127904
    invoke-interface {p3, p2}, LX/ETj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127905
    :goto_0
    if-eqz v0, :cond_4

    .line 2127906
    iget-object v5, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2127907
    invoke-interface {v5}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v5, v6, :cond_4

    .line 2127908
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2127909
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2127910
    :goto_1
    move-object v1, v0

    .line 2127911
    new-instance v0, LX/EVN;

    invoke-direct {v0, p2, v1}, LX/EVN;-><init>(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V

    .line 2127912
    :goto_2
    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->c:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    invoke-static {p1, v5, v2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_1

    move v2, v3

    :goto_3
    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a:LX/0Ot;

    invoke-virtual {v5, v2, v6, v0}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v5, :cond_2

    :goto_4
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->b:LX/0Ot;

    invoke-virtual {v2, v3, v1, v0}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2127913
    invoke-interface {p3, p2}, LX/ETj;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127914
    invoke-static {p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2127915
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->d:LX/0Ot;

    invoke-virtual {p1, v0, v1, v7}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 2127916
    return-object v7

    .line 2127917
    :cond_0
    new-instance v0, LX/EVN;

    invoke-direct {v0, p2}, LX/EVN;-><init>(Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_2

    :cond_1
    move v2, v4

    .line 2127918
    goto :goto_3

    :cond_2
    move v3, v4

    goto :goto_4

    .line 2127919
    :cond_3
    invoke-interface {p3, v0}, LX/ETj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2127920
    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 1

    .prologue
    .line 2127921
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->e:LX/EVb;

    invoke-static {p1, v0}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVb;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2127922
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitGroupPartDefinition;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method
