.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ":",
        "LX/7Lk;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasVideoHomePersistentState;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/String;",
        "TE;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field public final b:LX/1za;


# direct methods
.method public constructor <init>(LX/14w;LX/1za;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126484
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126485
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->a:LX/14w;

    .line 2126486
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->b:LX/1za;

    .line 2126487
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;
    .locals 5

    .prologue
    .line 2126473
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    monitor-enter v1

    .line 2126474
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126475
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126476
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126477
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126478
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v4

    check-cast v4, LX/1za;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;-><init>(LX/14w;LX/1za;)V

    .line 2126479
    move-object v0, p0

    .line 2126480
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126481
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126482
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2126488
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2126489
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->a:LX/14w;

    invoke-virtual {v0, p2}, LX/14w;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0am;

    move-result-object v0

    .line 2126490
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2126491
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2126492
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1731b593

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2126469
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    check-cast p2, Ljava/lang/String;

    check-cast p4, Landroid/widget/TextView;

    .line 2126470
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2126471
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->b:LX/1za;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p4, v2}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;Z)V

    .line 2126472
    const/16 v1, 0x1f

    const v2, 0x24cc74db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2126466
    check-cast p4, Landroid/widget/TextView;

    .line 2126467
    const/4 v0, 0x0

    invoke-static {p4, v0}, LX/1za;->a(Landroid/widget/TextView;Z)V

    .line 2126468
    return-void
.end method
