.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127815
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2127816
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 2127817
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2127818
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    .line 2127819
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2127820
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;
    .locals 7

    .prologue
    .line 2127754
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    monitor-enter v1

    .line 2127755
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127756
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127757
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127758
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127759
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;-><init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 2127760
    move-object v0, p0

    .line 2127761
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127762
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127763
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 2127765
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, p2, v1, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127766
    return-void

    .line 2127767
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a(LX/174;)Z
    .locals 1
    .param p0    # LX/174;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2127768
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2127769
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2127770
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127771
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2127772
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2127773
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 2127774
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v1

    .line 2127775
    :goto_0
    move-object v6, v1

    .line 2127776
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2127777
    invoke-interface {v1}, LX/9uc;->ct()LX/174;

    move-result-object v7

    .line 2127778
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2127779
    invoke-interface {v1}, LX/9uc;->cm()LX/5sd;

    move-result-object v8

    .line 2127780
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/174;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_1
    move v9, v1

    .line 2127781
    if-nez v9, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 2127782
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v4, 0x1

    :goto_2
    move v1, v4

    .line 2127783
    if-eqz v1, :cond_3

    move v5, v2

    .line 2127784
    :goto_3
    if-nez v9, :cond_4

    if-nez v5, :cond_4

    invoke-static {v7}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/174;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v4, v2

    .line 2127785
    :goto_4
    if-nez v9, :cond_5

    if-nez v5, :cond_5

    if-nez v4, :cond_5

    invoke-static {v8}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/174;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    .line 2127786
    :goto_5
    const v10, 0x7f0d056b

    if-nez v9, :cond_0

    if-nez v5, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    invoke-direct {p0, p1, v10, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/1aD;IZ)V

    .line 2127787
    const v2, 0x7f0d311c

    invoke-direct {p0, p1, v2, v9}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/1aD;IZ)V

    .line 2127788
    const v2, 0x7f0d311e

    invoke-direct {p0, p1, v2, v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/1aD;IZ)V

    .line 2127789
    if-eqz v9, :cond_6

    .line 2127790
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2127791
    const v1, 0x7f0d056b

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127792
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v0

    if-nez v0, :cond_c

    .line 2127793
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2127794
    :goto_6
    move-object v0, v0

    .line 2127795
    const v1, 0x7f0d311c

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, LX/EV8;->a(Landroid/content/Context;LX/0Px;)LX/8Cj;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127796
    :cond_2
    :goto_7
    const/4 v0, 0x0

    return-object v0

    :cond_3
    move v5, v3

    .line 2127797
    goto :goto_3

    :cond_4
    move v4, v3

    .line 2127798
    goto :goto_4

    :cond_5
    move v1, v3

    .line 2127799
    goto :goto_5

    .line 2127800
    :cond_6
    if-eqz v5, :cond_7

    .line 2127801
    const v1, 0x7f0d056b

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeBlingBarPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_7

    .line 2127802
    :cond_7
    if-eqz v4, :cond_8

    .line 2127803
    const v0, 0x7f0d056b

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_7

    .line 2127804
    :cond_8
    if-eqz v1, :cond_2

    .line 2127805
    const v0, 0x7f0d311e

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_7

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2127806
    :cond_c
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2127807
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2127808
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v4, :cond_e

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;

    .line 2127809
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 2127810
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 2127811
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 2127812
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2127813
    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 2127814
    :cond_e
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_6
.end method
