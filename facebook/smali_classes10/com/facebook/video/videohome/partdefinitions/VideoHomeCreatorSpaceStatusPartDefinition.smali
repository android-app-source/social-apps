.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/ETd;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EUq;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/EVf;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126735
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126736
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;
    .locals 3

    .prologue
    .line 2126756
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    monitor-enter v1

    .line 2126757
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126758
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126759
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126760
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2126761
    new-instance v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    invoke-direct {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;-><init>()V

    .line 2126762
    move-object v0, v0

    .line 2126763
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126764
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126765
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x30c2fbae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2126737
    check-cast p1, LX/EUq;

    check-cast p4, LX/EVf;

    .line 2126738
    iget-object v1, p1, LX/EUq;->a:LX/ESz;

    const/4 p3, 0x0

    const/4 p2, 0x4

    .line 2126739
    sget-object v2, LX/EVe;->a:[I

    invoke-virtual {v1}, LX/ESz;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 2126740
    iget-object v2, p4, LX/EVf;->f:Lcom/facebook/widget/text/BetterTextView;

    iget p0, p4, LX/EVf;->j:I

    invoke-virtual {v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2126741
    iget-object v2, p4, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2126742
    iget-object v2, p4, LX/EVf;->d:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    .line 2126743
    :goto_0
    iget-object v1, p1, LX/EUq;->b:Ljava/lang/String;

    .line 2126744
    iget-object v2, p4, LX/EVf;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2126745
    const/16 v1, 0x1f

    const v2, -0x6b52b5aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2126746
    :pswitch_0
    iget-object v2, p4, LX/EVf;->f:Lcom/facebook/widget/text/BetterTextView;

    iget p0, p4, LX/EVf;->k:I

    invoke-virtual {v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2126747
    iget-boolean v2, p4, LX/EVf;->h:Z

    if-eqz v2, :cond_0

    .line 2126748
    iget-object v2, p4, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2126749
    iget-object v2, p4, LX/EVf;->d:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2126750
    :cond_0
    iget-object v2, p4, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2126751
    iget-object v2, p4, LX/EVf;->d:Landroid/view/View;

    invoke-virtual {v2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 2126752
    iget-object v2, p4, LX/EVf;->d:Landroid/view/View;

    iget-object p0, p4, LX/EVf;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2126753
    :pswitch_1
    iget-object v2, p4, LX/EVf;->f:Lcom/facebook/widget/text/BetterTextView;

    iget p0, p4, LX/EVf;->k:I

    invoke-virtual {v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2126754
    iget-object v2, p4, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2126755
    iget-object v2, p4, LX/EVf;->d:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
