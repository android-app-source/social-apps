.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETi;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EUx;",
        "LX/EUy;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeFollowButton;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/D7a;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Gp;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D7a;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D7a;",
            "LX/0Ot",
            "<",
            "LX/3Gp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126916
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2126917
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->a:LX/D7a;

    .line 2126918
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->b:LX/0Ot;

    .line 2126919
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->c:LX/0Ot;

    .line 2126920
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->d:LX/0Ot;

    .line 2126921
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;
    .locals 7

    .prologue
    .line 2126922
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    monitor-enter v1

    .line 2126923
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2126924
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2126925
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126926
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2126927
    new-instance v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    invoke-static {v0}, LX/D7a;->a(LX/0QB;)LX/D7a;

    move-result-object v3

    check-cast v3, LX/D7a;

    const/16 v5, 0x130d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1387

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x1369

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;-><init>(LX/D7a;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2126928
    move-object v0, v4

    .line 2126929
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2126930
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2126931
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2126932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/D6V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pr;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/D6V;"
        }
    .end annotation

    .prologue
    .line 2126933
    new-instance v0, LX/D6T;

    invoke-direct {v0, p2}, LX/D6T;-><init>(Ljava/lang/String;)V

    .line 2126934
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6V;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2126935
    check-cast p2, LX/EUx;

    check-cast p3, LX/ETi;

    .line 2126936
    iget-object v0, p2, LX/EUx;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2126937
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2126938
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v2

    move-object v1, p3

    .line 2126939
    check-cast v1, LX/1Pr;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2126940
    iget-boolean p1, p2, LX/EUx;->e:Z

    if-nez p1, :cond_1

    .line 2126941
    :goto_0
    move v1, v3

    .line 2126942
    if-eqz v1, :cond_0

    .line 2126943
    new-instance v3, LX/EUv;

    invoke-direct {v3, p0, v0, p2, p3}, LX/EUv;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;LX/EUx;LX/ETi;)V

    move-object v0, v3

    .line 2126944
    :goto_1
    new-instance v3, LX/EUy;

    invoke-direct {v3, v0, v1, v2}, LX/EUy;-><init>(LX/EUu;ZZ)V

    return-object v3

    .line 2126945
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2126946
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result p1

    if-nez p1, :cond_2

    move v3, v4

    .line 2126947
    :cond_2
    if-eqz v3, :cond_3

    move v3, v4

    .line 2126948
    goto :goto_0

    .line 2126949
    :cond_3
    iget-object v3, p2, LX/EUx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->b(LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/D6V;

    move-result-object v3

    .line 2126950
    iget-boolean v4, v3, LX/D6V;->a:Z

    move v3, v4

    .line 2126951
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p4    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x654c057f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2126952
    check-cast p2, LX/EUy;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;

    .line 2126953
    if-nez p4, :cond_0

    .line 2126954
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x111820de    # 1.2000814E-28f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2126955
    :cond_0
    iget-object v1, p2, LX/EUy;->c:LX/EUu;

    .line 2126956
    iput-object v1, p4, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->f:LX/EUu;

    .line 2126957
    iget-boolean v1, p2, LX/EUy;->a:Z

    if-eqz v1, :cond_1

    .line 2126958
    iget-boolean v1, p2, LX/EUy;->b:Z

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setIsFollowing(Z)V

    .line 2126959
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setVisibility(I)V

    .line 2126960
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->a:LX/D7a;

    .line 2126961
    iget-object v2, p4, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->d:LX/8D5;

    move-object v2, v2

    .line 2126962
    iget-object p0, v1, LX/D7a;->b:LX/1PJ;

    invoke-virtual {p0, v2}, LX/1PJ;->a(LX/8D5;)V

    .line 2126963
    goto :goto_0

    .line 2126964
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->a:LX/D7a;

    .line 2126965
    iget-object v2, v1, LX/D7a;->b:LX/1PJ;

    invoke-virtual {v2, p4}, LX/1PJ;->a(Landroid/view/View;)V

    .line 2126966
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeFollowButton;->setVisibility(I)V

    goto :goto_0
.end method
