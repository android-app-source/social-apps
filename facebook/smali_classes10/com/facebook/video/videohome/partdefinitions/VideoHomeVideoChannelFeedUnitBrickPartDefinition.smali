.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/EVN;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127848
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2127849
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->a:LX/0Ot;

    .line 2127850
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->b:LX/0Ot;

    .line 2127851
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->c:LX/0Ot;

    .line 2127852
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;
    .locals 6

    .prologue
    .line 2127860
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;

    monitor-enter v1

    .line 2127861
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127862
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127865
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;

    const/16 v4, 0x1380

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x137c

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1370

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2127866
    move-object v0, v3

    .line 2127867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2127854
    check-cast p2, LX/EVN;

    .line 2127855
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127856
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127857
    iget-object v0, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2127858
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBrickPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2127859
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2127853
    const/4 v0, 0x1

    return v0
.end method
