.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EVN;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/154;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2127970
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2127971
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2127972
    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->a:LX/0Ot;

    .line 2127973
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2127974
    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->b:LX/0Ot;

    .line 2127975
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2127976
    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->c:LX/0Ot;

    .line 2127977
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;
    .locals 6

    .prologue
    .line 2127978
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;

    monitor-enter v1

    .line 2127979
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127980
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127983
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;

    invoke-direct {v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;-><init>()V

    .line 2127984
    const/16 v4, 0x2d0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xe17

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xe1a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2127985
    iput-object v4, v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->a:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->b:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->c:LX/0Ot;

    .line 2127986
    move-object v0, v3

    .line 2127987
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127988
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127989
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2127991
    check-cast p2, LX/EVN;

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2127992
    iget-object v0, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127993
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v2

    .line 2127994
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2127995
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 2127996
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v2, :cond_1

    move v2, v3

    .line 2127997
    :goto_0
    if-eqz v2, :cond_0

    .line 2127998
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/154;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v4

    invoke-virtual {v0, v4, v3}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v3

    .line 2127999
    const v4, 0x7f0d19be

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v4, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2128000
    :cond_0
    if-eqz v2, :cond_2

    .line 2128001
    :goto_1
    const v2, 0x7f0d19bc

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v2, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2128002
    const v2, 0x7f0d19be

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2128003
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move v2, v1

    .line 2128004
    goto :goto_0

    .line 2128005
    :cond_2
    const/4 v0, 0x4

    move v1, v0

    goto :goto_1
.end method
