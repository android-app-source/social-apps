.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2127964
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2127965
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 2127966
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2127967
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 2127968
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;
    .locals 6

    .prologue
    .line 2127953
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    monitor-enter v1

    .line 2127954
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2127955
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2127956
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127957
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2127958
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;-><init>(Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 2127959
    move-object v0, p0

    .line 2127960
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2127961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2127963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2127923
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2127924
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2127925
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2127926
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 2127927
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127928
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2127929
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2127930
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2127931
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v1

    .line 2127932
    :goto_0
    move-object v0, v1

    .line 2127933
    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;)Z

    move-result v1

    .line 2127934
    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->c:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v3, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2127935
    if-eqz v1, :cond_0

    .line 2127936
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2127937
    const v2, 0x7f0d056b

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127938
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2127939
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2127940
    :goto_2
    move-object v0, v1

    .line 2127941
    const v1, 0x7f0d311c

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, LX/EV8;->a(Landroid/content/Context;LX/0Px;)LX/8Cj;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2127942
    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2127943
    :cond_2
    const/16 v2, 0x8

    goto :goto_1

    .line 2127944
    :cond_3
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2127945
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2127946
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;

    .line 2127947
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 2127948
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {v1}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    .line 2127949
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 2127950
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2127951
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2127952
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_2
.end method
