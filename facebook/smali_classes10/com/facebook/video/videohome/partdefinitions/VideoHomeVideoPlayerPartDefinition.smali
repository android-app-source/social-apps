.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/videohome/environment/HasVideoHomePersistentState;",
        ":",
        "LX/1Po;",
        ":",
        "LX/BwV;",
        ":",
        "Lcom/facebook/video/videohome/environment/HasPlayerEnvironment;",
        ":",
        "LX/ETi;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EVN;",
        "LX/EVO;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/2mZ;

.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

.field private final c:LX/23s;

.field private final d:LX/EVP;

.field private final e:LX/D49;

.field private final f:LX/0xX;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2mn;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2mZ;Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;LX/23s;LX/EVP;LX/D49;LX/0xX;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2mZ;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;",
            "LX/23s;",
            "LX/EVP;",
            "LX/D49;",
            "LX/0xX;",
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2mn;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128177
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2128178
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a:LX/2mZ;

    .line 2128179
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    .line 2128180
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->c:LX/23s;

    .line 2128181
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->d:LX/EVP;

    .line 2128182
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->e:LX/D49;

    .line 2128183
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->f:LX/0xX;

    .line 2128184
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->g:LX/0Ot;

    .line 2128185
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->h:LX/0Ot;

    .line 2128186
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->i:LX/0Ot;

    .line 2128187
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/graphql/model/GraphQLMedia;LX/3HY;)LX/1bf;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2128199
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->f:LX/0xX;

    .line 2128200
    iget-object v1, v0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->b:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 2128201
    if-nez v0, :cond_0

    .line 2128202
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2128203
    const/4 v0, 0x0

    .line 2128204
    :goto_0
    move-object v0, v0

    .line 2128205
    :goto_1
    return-object v0

    .line 2128206
    :cond_0
    sget-object v0, LX/EVM;->a:[I

    invoke-virtual {p2}, LX/3HY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2128207
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mn;

    invoke-virtual {v0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    move v1, v0

    .line 2128208
    :goto_2
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qa;

    sget-object v2, LX/26P;->Video:LX/26P;

    invoke-virtual {v0, p1, v1, v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    goto :goto_1

    .line 2128209
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mn;

    invoke-virtual {v0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    move v1, v0

    .line 2128210
    goto :goto_2

    .line 2128211
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f0b1628

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 2128212
    goto :goto_2

    .line 2128213
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f0b16b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 2128214
    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;LX/EUB;)LX/3It;
    .locals 1

    .prologue
    .line 2128075
    new-instance v0, LX/EVL;

    invoke-direct {v0, p0, p1}, LX/EVL;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;LX/EUB;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;
    .locals 13

    .prologue
    .line 2128188
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    monitor-enter v1

    .line 2128189
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2128190
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2128191
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128192
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2128193
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v6

    check-cast v6, LX/23s;

    invoke-static {v0}, LX/EVP;->b(LX/0QB;)LX/EVP;

    move-result-object v7

    check-cast v7, LX/EVP;

    invoke-static {v0}, LX/D49;->b(LX/0QB;)LX/D49;

    move-result-object v8

    check-cast v8, LX/D49;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v9

    check-cast v9, LX/0xX;

    const/16 v10, 0x649

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x849

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1b

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;-><init>(LX/2mZ;Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;LX/23s;LX/EVP;LX/D49;LX/0xX;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2128194
    move-object v0, v3

    .line 2128195
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2128196
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2128197
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2128198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/EVN;LX/EVO;LX/ETy;Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EVN;",
            "LX/EVO;",
            "TE;",
            "Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2128138
    const-string v0, "VideoHomeVideoPlayerPartDefinition.bind"

    const v1, -0x62b3b599

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2128139
    :try_start_0
    iget-object v2, p2, LX/EVO;->c:LX/EUB;

    .line 2128140
    iput-object p4, v2, LX/EUB;->d:LX/3FR;

    .line 2128141
    invoke-static {p4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2128142
    :cond_0
    :goto_0
    const v0, 0x565d5bed

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2128143
    return-void

    .line 2128144
    :catchall_0
    move-exception v0

    const v1, 0x5ab9868f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2128145
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2128146
    const-class v3, LX/0f8;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0f8;

    .line 2128147
    if-eqz v3, :cond_2

    .line 2128148
    invoke-interface {v3}, LX/0f8;->g()Z

    move-result v3

    .line 2128149
    :goto_1
    move v2, v3

    .line 2128150
    if-nez v2, :cond_0

    .line 2128151
    invoke-virtual {p4}, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v3

    .line 2128152
    iget-object v4, p2, LX/EVO;->b:Ljava/lang/String;

    iget-object v5, p2, LX/EVO;->a:LX/3It;

    .line 2128153
    iget-object v2, p4, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;->b:Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;

    move-object v7, v2

    .line 2128154
    iget-object v8, p2, LX/EVO;->d:LX/2pa;

    invoke-virtual {p1}, LX/EVN;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v9

    move-object v6, p3

    invoke-static/range {v3 .. v9}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(Lcom/facebook/video/player/RichVideoPlayer;Ljava/lang/String;LX/3It;LX/ETy;Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;LX/2pa;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/video/player/RichVideoPlayer;Ljava/lang/String;LX/3It;LX/ETy;Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;LX/2pa;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            "Ljava/lang/String;",
            "LX/3It;",
            "TE;",
            "Lcom/facebook/video/videohome/plugins/VideoHomePlayerPluginSelector;",
            "LX/2pa;",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2128155
    move-object v0, p3

    check-cast v0, LX/ETy;

    .line 2128156
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p6, v1, :cond_0

    .line 2128157
    iget-object v0, v0, LX/ETy;->r:LX/EUf;

    .line 2128158
    :cond_0
    move-object v0, v0

    .line 2128159
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 2128160
    invoke-static {v1}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 2128161
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    .line 2128162
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2128163
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 2128164
    if-eqz v1, :cond_4

    iget-object v2, p5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget-object p1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object p1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2, p1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2128165
    if-eqz v1, :cond_2

    .line 2128166
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 2128167
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p6, v1, :cond_3

    .line 2128168
    invoke-static {p0}, LX/2mq;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 2128169
    :cond_3
    const-string v1, "VideoHomeVideoPlayerPartDefinition.configurePlayerLazily"

    const v2, 0x5254c463

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2128170
    :try_start_0
    invoke-virtual {p4, p0, p5, v0}, LX/3Ge;->b(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2128171
    const v0, 0x18ea0d7b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2128172
    iput-object p2, p0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 2128173
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 2128174
    invoke-virtual {p0, p5}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2128175
    return-void

    .line 2128176
    :catchall_0
    move-exception v0

    const v1, 0x1922c3fc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 2128131
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const v1, 0x7f0d00ff

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128132
    const/4 v0, 0x1

    .line 2128133
    :goto_0
    return v0

    .line 2128134
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2128135
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_1

    instance-of v1, v0, Landroid/widget/AdapterView;

    if-eqz v1, :cond_2

    .line 2128136
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2128137
    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(LX/1aD;LX/EVN;LX/ETy;)LX/EVO;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/EVN;",
            "TE;)",
            "LX/EVO;"
        }
    .end annotation

    .prologue
    .line 2128088
    move-object/from16 v0, p2

    iget-object v10, v0, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2128089
    invoke-virtual {v10}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v11

    .line 2128090
    invoke-interface {v11}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2128091
    invoke-static {v9}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v12

    .line 2128092
    invoke-static {v9}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 2128093
    invoke-virtual {v3, v12}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v13

    .line 2128094
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v14

    .line 2128095
    invoke-virtual {v10}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v8

    .line 2128096
    invoke-virtual/range {p2 .. p2}, LX/EVN;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v15

    .line 2128097
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v8}, LX/ETy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/EUB;

    move-result-object v16

    .line 2128098
    invoke-virtual/range {v16 .. v16}, LX/EUB;->b()LX/2oO;

    move-result-object v2

    invoke-virtual {v2, v14}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 2128099
    invoke-static {v3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    .line 2128100
    invoke-virtual/range {v16 .. v16}, LX/EUB;->b()LX/2oO;

    move-result-object v2

    invoke-virtual {v2}, LX/2oO;->l()Z

    move-result v2

    .line 2128101
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a:LX/2mZ;

    invoke-virtual {v5, v13, v14}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v2}, LX/3Im;->a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 2128102
    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 2128103
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->c:LX/23s;

    move-object/from16 v2, p3

    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v7, v13, v2}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v2

    .line 2128104
    new-instance v7, LX/0AW;

    invoke-direct {v7, v6}, LX/0AW;-><init>(LX/162;)V

    invoke-virtual {v7, v4}, LX/0AW;->a(Z)LX/0AW;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0AW;->a(LX/04H;)LX/0AW;

    move-result-object v2

    invoke-virtual {v2}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v6

    .line 2128105
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v15, v2, :cond_3

    const/4 v2, 0x1

    .line 2128106
    :goto_0
    if-eqz v2, :cond_0

    .line 2128107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeAutoplayPartDefinition;

    move-object/from16 v17, v0

    new-instance v2, LX/2rI;

    new-instance v4, LX/093;

    invoke-direct {v4}, LX/093;-><init>()V

    move-object/from16 v7, p3

    check-cast v7, LX/1Po;

    invoke-interface {v7}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    invoke-static {v7}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v7

    invoke-direct/range {v2 .. v8}, LX/2rI;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2128108
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->d:LX/EVP;

    move-object/from16 v2, p3

    check-cast v2, LX/ETi;

    invoke-virtual {v4, v10, v9, v2}, LX/EVP;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;LX/ETi;)V

    .line 2128109
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v15, v2, :cond_4

    .line 2128110
    sget-object v2, LX/3HY;->SMALL:LX/3HY;

    move-object v4, v2

    .line 2128111
    :goto_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v15, v2, :cond_6

    const/4 v2, 0x1

    move v6, v2

    :goto_2
    move-object/from16 v2, p3

    .line 2128112
    check-cast v2, LX/ETi;

    invoke-interface {v2, v8}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v2

    .line 2128113
    check-cast p3, LX/ETi;

    move-object/from16 v0, p3

    invoke-interface {v0, v8, v9}, LX/ETi;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v7

    .line 2128114
    invoke-interface {v11}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v8

    .line 2128115
    invoke-static {v13}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v9

    .line 2128116
    new-instance v11, LX/0P2;

    invoke-direct {v11}, LX/0P2;-><init>()V

    const-string v13, "GraphQLStoryProps"

    invoke-virtual {v11, v13, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "SubtitlesLocalesKey"

    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "ShowDeleteOptionKey"

    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "ShowReportOptionKey"

    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLVideo;->t()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "AutoplayStateManager"

    invoke-virtual/range {v16 .. v16}, LX/EUB;->b()LX/2oO;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "HideOnReportKey"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "VideoPlayerViewSizeKey"

    invoke-virtual {v3, v11, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v11, "UnitComponentTokenKey"

    invoke-virtual {v10}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v11, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v10, "UnitComponentTrackingDataKey"

    invoke-virtual {v3, v10, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v8, "UnitPositionKey"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v8, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "PositionInUnitKey"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "IsNotifVideoKey"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    .line 2128117
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/graphql/model/GraphQLMedia;LX/3HY;)LX/1bf;

    move-result-object v3

    .line 2128118
    if-eqz v3, :cond_1

    .line 2128119
    const-string v4, "CoverImageParamsKey"

    invoke-virtual {v2, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2128120
    :cond_1
    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2128121
    const-string v3, "BlurredCoverImageParamsKey"

    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2128122
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 2128123
    new-instance v3, LX/2pZ;

    invoke-direct {v3}, LX/2pZ;-><init>()V

    invoke-virtual {v3, v5}, LX/2pZ;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pZ;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->e:LX/D49;

    invoke-virtual {v4, v9}, LX/D49;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v2

    invoke-virtual {v2}, LX/2pZ;->b()LX/2pa;

    move-result-object v6

    .line 2128124
    new-instance v7, LX/7K4;

    invoke-virtual/range {v16 .. v16}, LX/EUB;->a()I

    move-result v2

    invoke-virtual/range {v16 .. v16}, LX/EUB;->a()I

    move-result v3

    invoke-direct {v7, v2, v3}, LX/7K4;-><init>(II)V

    .line 2128125
    new-instance v2, LX/EVO;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;LX/EUB;)LX/3It;

    move-result-object v3

    iget-object v4, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v5, v16

    invoke-direct/range {v2 .. v7}, LX/EVO;-><init>(LX/3It;Ljava/lang/String;LX/EUB;LX/2pa;LX/7K4;)V

    return-object v2

    .line 2128126
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2128127
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v15, v2, :cond_5

    .line 2128128
    sget-object v2, LX/3HY;->EXTRA_SMALL:LX/3HY;

    move-object v4, v2

    goto/16 :goto_1

    .line 2128129
    :cond_5
    sget-object v2, LX/3HY;->REGULAR:LX/3HY;

    move-object v4, v2

    goto/16 :goto_1

    .line 2128130
    :cond_6
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2128084
    check-cast p2, LX/EVN;

    check-cast p3, LX/ETy;

    .line 2128085
    const-string v0, "VideoHomeVideoPlayerPartDefinition.prepare"

    const v1, 0x257a2ee1    # 2.1699927E-16f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2128086
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->b(LX/1aD;LX/EVN;LX/ETy;)LX/EVO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2128087
    const v1, -0x2ed12254

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x380a12a2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xc13a542

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128083
    check-cast p1, LX/EVN;

    check-cast p2, LX/EVO;

    check-cast p3, LX/ETy;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(LX/EVN;LX/EVO;LX/ETy;Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;)V

    const/16 v1, 0x1f

    const v2, 0x8fc39e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2128076
    check-cast p1, LX/EVN;

    check-cast p2, LX/EVO;

    check-cast p3, LX/ETy;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2128077
    const-string v0, "unbind"

    const v1, -0x45f972f4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2128078
    :try_start_0
    iget-object v0, p2, LX/EVO;->c:LX/EUB;

    const/4 v1, 0x0

    .line 2128079
    iput-object v1, v0, LX/EUB;->d:LX/3FR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2128080
    const v0, -0x72993f9f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2128081
    return-void

    .line 2128082
    :catchall_0
    move-exception v0

    const v1, -0x33cc2d6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
