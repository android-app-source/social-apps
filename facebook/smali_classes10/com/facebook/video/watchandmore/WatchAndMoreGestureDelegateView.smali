.class public Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/D8f;

.field private b:LX/D8S;

.field private c:Z

.field private d:Z

.field private e:Landroid/view/GestureDetector;

.field public f:Z

.field public g:Z

.field public h:F

.field public i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1968491
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1968492
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1968454
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968455
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1968481
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968482
    iput-boolean v3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->c:Z

    .line 1968483
    iput-boolean v3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->d:Z

    .line 1968484
    iput-boolean v3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->f:Z

    .line 1968485
    iput-boolean v3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->g:Z

    .line 1968486
    iput v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    .line 1968487
    iput v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    .line 1968488
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/D8L;

    invoke-direct {v2, p0}, LX/D8L;-><init>(Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->e:Landroid/view/GestureDetector;

    .line 1968489
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->e:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1968490
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1968473
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1968474
    :goto_0
    :pswitch_0
    return-void

    .line 1968475
    :pswitch_1
    iput-boolean v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->d:Z

    .line 1968476
    iput-boolean v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->c:Z

    .line 1968477
    iput-boolean v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->g:Z

    .line 1968478
    iput-boolean v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->f:Z

    .line 1968479
    iput v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    .line 1968480
    iput v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1968470
    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    .line 1968471
    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    .line 1968472
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1968493
    if-nez p1, :cond_0

    .line 1968494
    :goto_0
    return v0

    .line 1968495
    :cond_0
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->e:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->c:Z

    .line 1968496
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v2, p1}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1968497
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v2, p1}, LX/D8f;->b(Landroid/view/MotionEvent;)Z

    .line 1968498
    :cond_1
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    invoke-virtual {v2, p1}, LX/D8S;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1968499
    iput-boolean v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->d:Z

    .line 1968500
    :cond_2
    iget-boolean v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->d:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->c:Z

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    .line 1968501
    :cond_4
    invoke-direct {p0, p1}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, 0x592ac6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1968459
    if-nez p1, :cond_0

    .line 1968460
    const/4 v0, 0x0

    const v2, 0x4c4da79a    # 5.3911144E7f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1968461
    :goto_0
    return v0

    .line 1968462
    :cond_0
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->e:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1968463
    iget-boolean v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->d:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    if-eqz v2, :cond_1

    .line 1968464
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    .line 1968465
    invoke-virtual {v2, p1}, LX/D8S;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1968466
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->c:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    if-eqz v2, :cond_2

    .line 1968467
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v2, p1}, LX/D8f;->b(Landroid/view/MotionEvent;)Z

    .line 1968468
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a(Landroid/view/MotionEvent;)V

    .line 1968469
    const v2, 0x5521fed0

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    :cond_3
    iget-object v3, v2, LX/D8S;->v:Landroid/view/ViewGroup;

    invoke-virtual {v3, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 1968458
    return-void
.end method

.method public setContentGestureListener(LX/D8f;)V
    .locals 0

    .prologue
    .line 1968456
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    .line 1968457
    return-void
.end method

.method public setPlayerGestureListener(LX/D8S;)V
    .locals 0

    .prologue
    .line 1968452
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->b:LX/D8S;

    .line 1968453
    return-void
.end method
