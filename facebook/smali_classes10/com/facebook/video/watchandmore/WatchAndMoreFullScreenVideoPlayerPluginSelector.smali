.class public Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1968345
    const-class v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/19m;LX/0sV;Ljava/lang/Boolean;)V
    .locals 6
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968346
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1968347
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->k:Z

    .line 1968348
    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1968349
    new-instance v2, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, p1, v0}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1968350
    new-instance v3, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1968351
    iget-boolean v0, p3, LX/0sV;->l:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPluginWithSocialContext;

    invoke-direct {v0, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPluginWithSocialContext;-><init>(Landroid/content/Context;)V

    .line 1968352
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->a:LX/0Px;

    .line 1968353
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->b:LX/0Px;

    .line 1968354
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    iget-object v5, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->c:LX/0Px;

    .line 1968355
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/7Ng;

    invoke-direct {v1, p1}, LX/7Ng;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 1968356
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->d:LX/0Px;

    .line 1968357
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->j:LX/0Px;

    .line 1968358
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->i:LX/0Px;

    .line 1968359
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->e:LX/0Px;

    .line 1968360
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->f:LX/0Px;

    .line 1968361
    return-void

    .line 1968362
    :cond_0
    new-instance v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;
    .locals 5

    .prologue
    .line 1968363
    new-instance v4, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v1

    check-cast v1, LX/19m;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v2

    check-cast v2, LX/0sV;

    invoke-static {p0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;-><init>(Landroid/content/Context;LX/19m;LX/0sV;Ljava/lang/Boolean;)V

    .line 1968364
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1968365
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1968366
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    .line 1968367
    :goto_0
    return-object v0

    .line 1968368
    :cond_0
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1968369
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1968370
    :cond_1
    const-class v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1968371
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1968372
    :cond_2
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
