.class public Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3FT;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Lcom/facebook/video/player/RichVideoPlayer;

.field private final e:LX/D8X;

.field public f:Lcom/facebook/video/player/RichVideoPlayer;

.field public g:LX/2pa;

.field public h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public i:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/2pa;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/3FO;

.field public k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1968973
    const-class v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1968990
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1968991
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1968988
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968989
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1968974
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968975
    new-instance v0, LX/D8X;

    invoke-direct {v0, p0}, LX/D8X;-><init>(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->e:LX/D8X;

    .line 1968976
    const-class v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1968977
    const v0, 0x7f031610

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1968978
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968979
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1968980
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1968981
    invoke-static {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1968982
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968983
    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getAdditionalPlugins()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1968984
    iget-object v4, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968985
    invoke-static {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1968986
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1968987
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-static {p0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v1

    check-cast v1, LX/2mn;

    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->b(LX/0QB;)Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    move-result-object p0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    iput-object v1, p1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->a:LX/2mn;

    iput-object p0, p1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    return-void
.end method

.method public static f(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V
    .locals 4

    .prologue
    .line 1968967
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    iget v0, v0, LX/3FO;->b:I

    if-nez v0, :cond_1

    .line 1968968
    :cond_0
    :goto_0
    return-void

    .line 1968969
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    iget v0, v0, LX/3FO;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    iget v2, v2, LX/3FO;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1968970
    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    iget-wide v2, v2, LX/2pa;->d:D

    .line 1968971
    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    .line 1968972
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    goto :goto_0
.end method

.method public static setupPlayerLayout(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 4

    .prologue
    .line 1968963
    if-nez p1, :cond_0

    .line 1968964
    :goto_0
    return-void

    .line 1968965
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->a:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;F)LX/3FO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    .line 1968966
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    iget v2, v2, LX/3FO;->a:I

    iget-object v3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    iget v3, v3, LX/3FO;->b:I

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2

    .prologue
    .line 1968955
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1968956
    iput-object p2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1968957
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    .line 1968958
    invoke-static {p0, p2}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupPlayerLayout(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1968959
    invoke-static {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V

    .line 1968960
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1968961
    return-void

    .line 1968962
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 1968951
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->d:Lcom/facebook/video/player/RichVideoPlayer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1968952
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968953
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1968954
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1968992
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    .line 1968993
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    if-eqz v0, :cond_0

    .line 1968994
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {v0}, LX/7N3;->g()V

    .line 1968995
    :cond_0
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1968948
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1968949
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1968950
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1968947
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->d:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getAdditionalPlugins()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1968945
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 1968946
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAdditionalPlugins()Ljava/util/List;
    .locals 1

    .prologue
    .line 1968944
    invoke-virtual {p0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getAdditionalPlugins()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1968930
    sget-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1968943
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getVideoSize()LX/3FO;
    .locals 1

    .prologue
    .line 1968942
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    return-object v0
.end method

.method public setupDismissPlayerButton(LX/D8U;)V
    .locals 2

    .prologue
    .line 1968937
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    .line 1968938
    if-nez v0, :cond_0

    .line 1968939
    :goto_0
    return-void

    .line 1968940
    :cond_0
    iput-object p1, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->t:LX/D8U;

    .line 1968941
    goto :goto_0
.end method

.method public setupFullscreenButtonClickHandler(LX/0QK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "LX/2pa;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1968931
    if-nez p1, :cond_1

    .line 1968932
    :cond_0
    :goto_0
    return-void

    .line 1968933
    :cond_1
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->i:LX/0QK;

    .line 1968934
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    .line 1968935
    if-eqz v0, :cond_0

    .line 1968936
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->e:LX/D8X;

    invoke-virtual {v0, v1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    goto :goto_0
.end method
