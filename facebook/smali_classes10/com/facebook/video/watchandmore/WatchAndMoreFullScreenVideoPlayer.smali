.class public Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/video/player/RichVideoPlayer;

.field private d:LX/2pa;

.field private e:LX/04D;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1968341
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1968342
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1968343
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1968303
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968304
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->e:LX/04D;

    .line 1968305
    const-class v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1968306
    const v0, 0x7f03073b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1968307
    new-instance v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968308
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1968309
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->addView(Landroid/view/View;)V

    .line 1968310
    return-void
.end method

.method private static a(Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;LX/1C2;Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;)V
    .locals 0

    .prologue
    .line 1968340
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a:LX/1C2;

    iput-object p2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->b:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    invoke-static {v1}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v0

    check-cast v0, LX/1C2;

    invoke-static {v1}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;->b(LX/0QB;)Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a(Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;LX/1C2;Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1968332
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_1

    .line 1968333
    :cond_0
    :goto_0
    return-void

    .line 1968334
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v7

    .line 1968335
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1968336
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1968337
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    sget-object v3, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v4, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->e:LX/04D;

    sget-object v6, LX/04g;->BY_USER:LX/04g;

    iget-object v6, v6, LX/04g;->value:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v8}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v8

    iget-object v9, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1968338
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1968339
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/2pa;IILX/2oi;)V
    .locals 14

    .prologue
    .line 1968321
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->d:LX/2pa;

    .line 1968322
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->e:LX/04D;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1968323
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->b:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayerPluginSelector;

    iget-object v2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968324
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1968325
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a:LX/1C2;

    iget-object v2, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    sget-object v4, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    iget-object v5, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->e:LX/04D;

    sget-object v7, LX/04g;->BY_USER:LX/04g;

    iget-object v7, v7, LX/04g;->value:Ljava/lang/String;

    iget-object v10, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-virtual/range {v1 .. v13}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1968326
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x0

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1968327
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1968328
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move/from16 v0, p2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1968329
    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1968330
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->setVisibility(I)V

    .line 1968331
    return-void
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 1968320
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public getVideoResolution()LX/2oi;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1968313
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968314
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1968315
    if-nez v0, :cond_0

    .line 1968316
    const/4 v0, 0x0

    .line 1968317
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968318
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1968319
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    goto :goto_0
.end method

.method public setPlayerOrigin(LX/04D;)V
    .locals 0

    .prologue
    .line 1968311
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->e:LX/04D;

    .line 1968312
    return-void
.end method
