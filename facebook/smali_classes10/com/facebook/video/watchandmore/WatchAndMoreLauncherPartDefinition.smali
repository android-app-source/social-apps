.class public Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3FU;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/D8O;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2mz;

.field private final c:LX/D8N;


# direct methods
.method public constructor <init>(LX/0Ot;LX/D8N;LX/2mz;)V
    .locals 0
    .param p3    # LX/2mz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/D8N;",
            "LX/2mz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968550
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1968551
    iput-object p1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->a:LX/0Ot;

    .line 1968552
    iput-object p3, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->b:LX/2mz;

    .line 1968553
    iput-object p2, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->c:LX/D8N;

    .line 1968554
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1968542
    check-cast p2, LX/D8O;

    check-cast p3, LX/1Po;

    .line 1968543
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->c:LX/D8N;

    iget-object v1, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->b:LX/2mz;

    .line 1968544
    new-instance v5, LX/D8M;

    const-class v2, LX/1VK;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1VK;

    const-class v3, LX/D8G;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/D8G;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v2, v3, v4, v1}, LX/D8M;-><init>(LX/1VK;LX/D8G;Landroid/content/Context;LX/2mz;)V

    .line 1968545
    move-object v0, v5

    .line 1968546
    invoke-virtual {v0, p2, p3}, LX/D8M;->a(LX/D8O;LX/1Po;)LX/D8F;

    move-result-object v1

    .line 1968547
    if-eqz v1, :cond_0

    .line 1968548
    iget-object v0, p0, Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3FZ;

    invoke-direct {v2, v1}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1968549
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
