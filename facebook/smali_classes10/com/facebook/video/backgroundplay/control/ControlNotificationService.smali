.class public Lcom/facebook/video/backgroundplay/control/ControlNotificationService;
.super LX/0te;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:LX/0Yd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:LX/2pb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/D44;

.field private final f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private g:LX/2om;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/19P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/19d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1FZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/15d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D47;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Landroid/app/NotificationManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Landroid/media/AudioManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Hc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/D3y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/BSB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Landroid/os/Looper;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/os/Handler;

.field private v:LX/D45;

.field private w:Lcom/facebook/video/engine/VideoPlayerParams;

.field private x:Lcom/facebook/graphql/model/GraphQLStory;

.field private y:I

.field public z:LX/2qD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1961782
    const-class v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->c:Ljava/lang/String;

    .line 1961783
    const-class v0, LX/D46;

    sget-object v1, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1961784
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".InitData"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1961778
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 1961779
    new-instance v0, LX/D44;

    invoke-direct {v0, p0}, LX/D44;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->e:LX/D44;

    .line 1961780
    new-instance v0, LX/D40;

    invoke-direct {v0, p0}, LX/D40;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1961781
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1961738
    sget-object v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;

    .line 1961739
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    iget-object v3, v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v1, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;

    move-result-object v1

    const/4 v3, 0x0

    .line 1961740
    iput-boolean v3, v1, LX/2oH;->g:Z

    .line 1961741
    move-object v1, v1

    .line 1961742
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1961743
    iget-object v1, v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->x:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1961744
    iget v1, v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->c:I

    iput v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->y:I

    .line 1961745
    const-string v1, "initialize"

    invoke-static {p0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961746
    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->x:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 1961747
    iget-object v4, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 1961748
    :goto_0
    iput-object v1, v4, LX/D3y;->d:Ljava/lang/CharSequence;

    .line 1961749
    move-object v1, v4

    .line 1961750
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->d()Z

    move-result v4

    .line 1961751
    iput-boolean v4, v1, LX/D3y;->i:Z

    .line 1961752
    move-object v1, v1

    .line 1961753
    const/4 v4, 0x1

    .line 1961754
    iput-boolean v4, v1, LX/D3y;->j:Z

    .line 1961755
    move-object v1, v1

    .line 1961756
    iget-object v4, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->x:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v4

    .line 1961757
    iput-boolean v4, v1, LX/D3y;->h:Z

    .line 1961758
    move-object v1, v1

    .line 1961759
    const v4, 0x7f020a20

    .line 1961760
    iput v4, v1, LX/D3y;->f:I

    .line 1961761
    const/4 p1, 0x0

    invoke-virtual {v1, p1}, LX/D3y;->a(LX/1FJ;)LX/D3y;

    .line 1961762
    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->m:LX/15d;

    .line 1961763
    iget-object v4, v1, LX/15d;->g:Ljava/lang/Boolean;

    if-nez v4, :cond_0

    .line 1961764
    iget-object v4, v1, LX/15d;->c:LX/0ad;

    sget-short v5, LX/BSC;->a:S

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v1, LX/15d;->g:Ljava/lang/Boolean;

    .line 1961765
    :cond_0
    iget-object v4, v1, LX/15d;->g:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move v1, v4

    .line 1961766
    if-eqz v1, :cond_1

    .line 1961767
    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->d:Landroid/app/PendingIntent;

    .line 1961768
    iget-object v4, v1, LX/D3y;->b:LX/2HB;

    .line 1961769
    iput-object v0, v4, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1961770
    :cond_1
    if-eqz v3, :cond_2

    invoke-static {v3}, LX/1xl;->b(Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    .line 1961771
    :cond_2
    if-eqz v2, :cond_3

    .line 1961772
    invoke-direct {p0, v2}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Ljava/lang/String;)V

    .line 1961773
    :cond_3
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->m()V

    .line 1961774
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b()I

    .line 1961775
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->g()V

    .line 1961776
    return-void

    :cond_4
    move-object v1, v2

    .line 1961777
    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;LX/2om;LX/19P;LX/1C2;LX/19d;LX/1HI;LX/1FZ;LX/15d;LX/0Ot;Landroid/app/NotificationManager;Landroid/media/AudioManager;LX/0Ot;LX/D3y;LX/BSB;Landroid/os/Looper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/backgroundplay/control/ControlNotificationService;",
            "LX/2om;",
            "LX/19P;",
            "LX/1C2;",
            "LX/19d;",
            "LX/1HI;",
            "LX/1FZ;",
            "LX/15d;",
            "LX/0Ot",
            "<",
            "LX/D47;",
            ">;",
            "Landroid/app/NotificationManager;",
            "Landroid/media/AudioManager;",
            "LX/0Ot",
            "<",
            "LX/3Hc;",
            ">;",
            "LX/D3y;",
            "LX/BSB;",
            "Landroid/os/Looper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1961737
    iput-object p1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->g:LX/2om;

    iput-object p2, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->h:LX/19P;

    iput-object p3, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->i:LX/1C2;

    iput-object p4, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->j:LX/19d;

    iput-object p5, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->k:LX/1HI;

    iput-object p6, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->l:LX/1FZ;

    iput-object p7, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->m:LX/15d;

    iput-object p8, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->n:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->o:Landroid/app/NotificationManager;

    iput-object p10, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->p:Landroid/media/AudioManager;

    iput-object p11, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->q:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    iput-object p13, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->s:LX/BSB;

    iput-object p14, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->t:Landroid/os/Looper;

    iput-object p15, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const-class v1, LX/2om;

    invoke-interface {v15, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2om;

    invoke-static {v15}, LX/19P;->a(LX/0QB;)LX/19P;

    move-result-object v2

    check-cast v2, LX/19P;

    invoke-static {v15}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v3

    check-cast v3, LX/1C2;

    invoke-static {v15}, LX/19d;->b(LX/0QB;)LX/19d;

    move-result-object v4

    check-cast v4, LX/19d;

    invoke-static {v15}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v5

    check-cast v5, LX/1HI;

    invoke-static {v15}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v6

    check-cast v6, LX/1FZ;

    invoke-static {v15}, LX/15d;->b(LX/0QB;)LX/15d;

    move-result-object v7

    check-cast v7, LX/15d;

    const/16 v8, 0x37b9

    invoke-static {v15, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v15}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    invoke-static {v15}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v10

    check-cast v10, Landroid/media/AudioManager;

    const/16 v11, 0x537

    invoke-static {v15, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v15}, LX/D3y;->b(LX/0QB;)LX/D3y;

    move-result-object v12

    check-cast v12, LX/D3y;

    invoke-static {v15}, LX/BSB;->b(LX/0QB;)LX/BSB;

    move-result-object v13

    check-cast v13, LX/BSB;

    invoke-static {v15}, LX/0Zp;->b(LX/0QB;)Landroid/os/Looper;

    move-result-object v14

    check-cast v14, Landroid/os/Looper;

    invoke-static {v15}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v15}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;LX/2om;LX/19P;LX/1C2;LX/19d;LX/1HI;LX/1FZ;LX/15d;LX/0Ot;Landroid/app/NotificationManager;Landroid/media/AudioManager;LX/0Ot;LX/D3y;LX/BSB;Landroid/os/Looper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1961731
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 1961732
    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->k:LX/1HI;

    sget-object v2, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1961733
    new-instance v1, LX/D41;

    invoke-direct {v1, p0}, LX/D41;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    .line 1961734
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1961735
    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1961736
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 1961726
    if-eqz p1, :cond_0

    const-string v0, "like_media"

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961727
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    .line 1961728
    iput-boolean p1, v0, LX/D3y;->h:Z

    .line 1961729
    return-void

    .line 1961730
    :cond_0
    const-string v0, "unlike_media"

    goto :goto_0
.end method

.method private b()I
    .locals 4

    .prologue
    .line 1961725
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->p:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1961717
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    .line 1961718
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->s:LX/BSB;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 1961719
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "facecast_broadcaster_update"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "facecast"

    .line 1961720
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1961721
    move-object v2, v2

    .line 1961722
    const-string v3, "facecast_event_name"

    const-string p0, "background_play_action"

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "facecast_event_extra"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "video_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1961723
    iget-object v3, v0, LX/BSB;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1961724
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1961715
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->p:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1961716
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 1961714
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1961785
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 10

    .prologue
    .line 1961572
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    if-nez v0, :cond_0

    .line 1961573
    :goto_0
    return-void

    .line 1961574
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1961575
    sget-object v1, LX/BSN;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 1961576
    if-nez v0, :cond_2

    .line 1961577
    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->o:Landroid/app/NotificationManager;

    const/16 v2, 0x4e27

    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D47;

    .line 1961578
    const/4 v6, 0x1

    .line 1961579
    new-instance v3, LX/2HB;

    iget-object v4, v0, LX/D47;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 1961580
    const v4, 0x7f020343

    invoke-virtual {v3, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v4

    .line 1961581
    iput v6, v4, LX/2HB;->z:I

    .line 1961582
    move-object v4, v4

    .line 1961583
    iget-object v5, v0, LX/D47;->a:Landroid/content/Context;

    invoke-static {v5}, LX/D3x;->b(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v5

    .line 1961584
    iput-object v5, v4, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1961585
    move-object v4, v4

    .line 1961586
    invoke-virtual {v4, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v4

    .line 1961587
    iput v6, v4, LX/2HB;->j:I

    .line 1961588
    invoke-virtual {v3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v3

    move-object v3, v3

    .line 1961589
    iget-object v4, v0, LX/D47;->b:Landroid/widget/RemoteViews;

    iput-object v4, v3, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1961590
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_1

    .line 1961591
    iget-object v4, v0, LX/D47;->c:Landroid/widget/RemoteViews;

    iput-object v4, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 1961592
    :cond_1
    move-object v0, v3

    .line 1961593
    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1961594
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1961595
    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/BSN;->f:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1961596
    :cond_2
    const/16 v0, 0x4e26

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    .line 1961597
    new-instance v4, LX/D46;

    iget-object v3, v1, LX/D3y;->a:Landroid/content/Context;

    invoke-direct {v4, v3}, LX/D46;-><init>(Landroid/content/Context;)V

    .line 1961598
    iget-object v3, v1, LX/D3y;->d:Ljava/lang/CharSequence;

    .line 1961599
    const v5, 0x7f0d0b6b

    invoke-static {v4, v5, v3}, LX/D46;->a(LX/D46;ILjava/lang/CharSequence;)V

    .line 1961600
    move-object v3, v4

    .line 1961601
    iget-object v5, v1, LX/D3y;->e:Ljava/lang/CharSequence;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1961602
    if-eqz v5, :cond_6

    move v6, v7

    .line 1961603
    :goto_1
    const v9, 0x7f0d0b72

    invoke-static {v3, v9, v6}, LX/D46;->a(LX/D46;IZ)V

    .line 1961604
    const v9, 0x7f0d0b71

    if-nez v6, :cond_7

    :goto_2
    invoke-static {v3, v9, v7}, LX/D46;->a(LX/D46;IZ)V

    .line 1961605
    const v6, 0x7f0d0b72

    invoke-static {v3, v6, v5}, LX/D46;->a(LX/D46;ILjava/lang/CharSequence;)V

    .line 1961606
    move-object v3, v3

    .line 1961607
    iget-boolean v5, v1, LX/D3y;->i:Z

    .line 1961608
    const v7, 0x7f0d0b71

    if-eqz v5, :cond_8

    const v6, 0x7f021b23

    :goto_3
    const/4 v8, 0x0

    invoke-static {v3, v7, v6, v8}, LX/D46;->a(LX/D46;IILandroid/app/PendingIntent;)V

    .line 1961609
    move-object v3, v3

    .line 1961610
    move-object v3, v3

    .line 1961611
    iget-boolean v5, v1, LX/D3y;->j:Z

    .line 1961612
    const v8, 0x7f0d0b69

    if-eqz v5, :cond_9

    const v6, 0x7f021b27

    move v7, v6

    :goto_4
    if-eqz v5, :cond_a

    const-string v6, "video.playback.control.action.pause"

    :goto_5
    invoke-static {v3, v6}, LX/D46;->a(LX/D46;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-static {v3, v8, v7, v6}, LX/D46;->a(LX/D46;IILandroid/app/PendingIntent;)V

    .line 1961613
    iget-object v3, v1, LX/D3y;->g:LX/1FJ;

    if-nez v3, :cond_4

    .line 1961614
    iget v3, v1, LX/D3y;->f:I

    .line 1961615
    const v5, 0x7f0d0b6e

    const/4 v6, 0x0

    invoke-static {v4, v5, v3, v6}, LX/D46;->a(LX/D46;IILandroid/app/PendingIntent;)V

    .line 1961616
    :goto_6
    move-object v3, v4

    .line 1961617
    iget-object v4, v1, LX/D3y;->b:LX/2HB;

    iget-object v5, v1, LX/D3y;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, LX/2HB;->a(J)LX/2HB;

    .line 1961618
    iget-object v4, v1, LX/D3y;->b:LX/2HB;

    invoke-virtual {v4}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v4

    .line 1961619
    iget-object v5, v3, LX/D46;->b:Landroid/widget/RemoteViews;

    iput-object v5, v4, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1961620
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v5, v6, :cond_3

    .line 1961621
    iget-object v5, v3, LX/D46;->c:Landroid/widget/RemoteViews;

    iput-object v5, v4, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 1961622
    :cond_3
    move-object v1, v4

    .line 1961623
    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->startForeground(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 1961624
    :cond_4
    iget-object v3, v1, LX/D3y;->g:LX/1FJ;

    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 1961625
    const v5, 0x7f0d0b6e

    const/4 v6, 0x0

    .line 1961626
    iget-object v7, v4, LX/D46;->b:Landroid/widget/RemoteViews;

    invoke-static {v7, v5, v3, v6}, LX/D46;->a(Landroid/widget/RemoteViews;ILandroid/graphics/Bitmap;Landroid/app/PendingIntent;)V

    .line 1961627
    iget-object v7, v4, LX/D46;->c:Landroid/widget/RemoteViews;

    if-eqz v7, :cond_5

    .line 1961628
    iget-object v7, v4, LX/D46;->c:Landroid/widget/RemoteViews;

    invoke-static {v7, v5, v3, v6}, LX/D46;->a(Landroid/widget/RemoteViews;ILandroid/graphics/Bitmap;Landroid/app/PendingIntent;)V

    .line 1961629
    :cond_5
    goto :goto_6

    :cond_6
    move v6, v8

    .line 1961630
    goto/16 :goto_1

    :cond_7
    move v7, v8

    .line 1961631
    goto/16 :goto_2

    .line 1961632
    :cond_8
    const v6, 0x7f021b21

    goto :goto_3

    .line 1961633
    :cond_9
    const v6, 0x7f021b26

    move v7, v6

    goto :goto_4

    :cond_a
    const-string v6, "video.playback.control.action.play"

    goto :goto_5
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1961634
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    if-nez v0, :cond_0

    .line 1961635
    :goto_0
    return-void

    .line 1961636
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    const/4 v1, 0x1

    .line 1961637
    iput-boolean v1, v0, LX/D3y;->j:Z

    .line 1961638
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    const/4 v1, 0x0

    .line 1961639
    iput-object v1, v0, LX/D3y;->e:Ljava/lang/CharSequence;

    .line 1961640
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    iget v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->y:I

    sget-object v2, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, LX/2pb;->a(ILX/04g;)V

    .line 1961641
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/2pb;->a(F)V

    .line 1961642
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    .line 1961643
    iget-object v1, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v1}, LX/2q7;->j()V

    .line 1961644
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/04g;)V

    .line 1961645
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->k()V

    .line 1961646
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->m:LX/15d;

    invoke-virtual {v0}, LX/15d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1961647
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3Hc;->a(Z)V

    .line 1961648
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hc;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->a(Ljava/lang/String;)V

    .line 1961649
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    goto :goto_0
.end method

.method public static h(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 2

    .prologue
    .line 1961650
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    if-nez v0, :cond_0

    .line 1961651
    :goto_0
    return-void

    .line 1961652
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->l()V

    .line 1961653
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    .line 1961654
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    const/4 v1, 0x0

    .line 1961655
    iput-boolean v1, v0, LX/D3y;->j:Z

    .line 1961656
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->b(LX/04g;)V

    .line 1961657
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->y:I

    .line 1961658
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->j()V

    goto :goto_0
.end method

.method public static i(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 2

    .prologue
    .line 1961659
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    if-eqz v0, :cond_0

    .line 1961660
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->b(LX/04g;)V

    .line 1961661
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    .line 1961662
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->stopForeground(Z)V

    .line 1961663
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->l()V

    .line 1961664
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->c()V

    .line 1961665
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->j()V

    .line 1961666
    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 1961667
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->m:LX/15d;

    invoke-virtual {v0}, LX/15d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1961668
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1961669
    :cond_0
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 1961670
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->A:LX/0Yd;

    if-nez v0, :cond_0

    .line 1961671
    const/4 v0, 0x1

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 1961672
    const-string v1, "android.intent.action.HEADSET_PLUG"

    new-instance v2, LX/D42;

    invoke-direct {v2, p0}, LX/D42;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1961673
    new-instance v1, LX/0Yd;

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->A:LX/0Yd;

    .line 1961674
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1961675
    invoke-virtual {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->A:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1961676
    :cond_1
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1961677
    invoke-virtual {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->A:LX/0Yd;

    if-eqz v0, :cond_0

    .line 1961678
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->A:LX/0Yd;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1961679
    :cond_0
    :goto_0
    return-void

    .line 1961680
    :catch_0
    move-exception v0

    .line 1961681
    sget-object v1, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->c:Ljava/lang/String;

    const-string v2, "Failed to unregister plug receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private m()V
    .locals 5

    .prologue
    .line 1961682
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    if-nez v0, :cond_0

    .line 1961683
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->g:LX/2om;

    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sget-object v3, LX/04D;->BACKGROUND_PLAY:LX/04D;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2om;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;LX/04D;Ljava/lang/Boolean;)LX/2pb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    .line 1961684
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->c(LX/04g;)V

    .line 1961685
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04G;->BACKGROUND_PLAY:LX/04G;

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/04G;)V

    .line 1961686
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->e:LX/D44;

    .line 1961687
    iget-object v2, v0, LX/2pb;->h:LX/2pd;

    .line 1961688
    iget-object v0, v2, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, v1, :cond_0

    .line 1961689
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, v2, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    .line 1961690
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    sget-object v1, LX/04D;->BACKGROUND_PLAY:LX/04D;

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/04D;)V

    .line 1961691
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->j:LX/19d;

    sget-object v2, LX/2p7;->NORMAL:LX/2p7;

    invoke-virtual {v1, v2}, LX/19d;->a(LX/2p7;)LX/2p8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/2p8;)V

    .line 1961692
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->B:LX/2pb;

    iget-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1}, LX/2pb;->a(Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1961693
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1961694
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This service can\'t be bind"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onFbCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x2084f257

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961695
    invoke-static {p0, p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1961696
    new-instance v1, LX/D45;

    invoke-direct {v1, p0}, LX/D45;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    iput-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->v:LX/D45;

    .line 1961697
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->t:Landroid/os/Looper;

    iget-object v3, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->v:LX/D45;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    .line 1961698
    const/16 v1, 0x25

    const v2, -0x40ce99b6

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x4dd63500    # 4.49224704E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961699
    invoke-static {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->i(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    .line 1961700
    const/16 v1, 0x25

    const v2, 0x379421d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x24

    const v4, -0x65dec373

    invoke-static {v3, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1961701
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 1961702
    const/4 v0, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1961703
    :goto_1
    const-string v0, "video.playback.control.action.close"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "video.playback.control.action.delete"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1961704
    invoke-static {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    .line 1961705
    :cond_1
    const-string v0, "extra_analytics"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961706
    const v0, -0xbb03bd

    invoke-static {v0, v4}, LX/02F;->d(II)V

    return v3

    .line 1961707
    :sswitch_0
    const-string v6, "video.playback.control.action.initialize"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v6, "video.playback.control.action.like"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v6, "video.playback.control.action.unlike"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string v6, "video.playback.control.action.play"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v6, "video.playback.control.action.pause"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v6, "video.playback.control.action.close"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v6, "video.playback.control.action.delete"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    .line 1961708
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 1961709
    :pswitch_1
    invoke-direct {p0, v2}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Z)V

    goto :goto_1

    .line 1961710
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a(Z)V

    goto :goto_1

    .line 1961711
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->g()V

    goto :goto_1

    .line 1961712
    :pswitch_4
    invoke-static {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->h(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_1

    .line 1961713
    :pswitch_5
    invoke-static {p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->i(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7e74bb0f -> :sswitch_5
        -0x7dc27311 -> :sswitch_4
        -0x4ed2022e -> :sswitch_6
        -0x3150be29 -> :sswitch_2
        0x165cd837 -> :sswitch_0
        0x1cf8057e -> :sswitch_1
        0x1cf9e11b -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
