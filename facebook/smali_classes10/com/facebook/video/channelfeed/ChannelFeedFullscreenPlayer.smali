.class public Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/D5t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/D4G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BwE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/AjP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:LX/1SX;

.field private final k:LX/AjN;

.field public l:Lcom/facebook/video/player/RichVideoPlayer;

.field public m:LX/2pa;

.field public n:LX/04D;

.field public o:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private s:I

.field public t:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1962824
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1962825
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1962822
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1962823
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1962826
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1962827
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1962828
    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->i:LX/0Ot;

    .line 1962829
    new-instance v0, LX/D4n;

    invoke-direct {v0, p0}, LX/D4n;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->k:LX/AjN;

    .line 1962830
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    .line 1962831
    iput-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    .line 1962832
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    .line 1962833
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1962834
    const v0, 0x7f03073b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1962835
    new-instance v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962836
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1962837
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04H;->ELIGIBLE:LX/04H;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1962838
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962839
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, LX/D4o;

    invoke-direct {v2, p0, v0}, LX/D4o;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1962840
    iput-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1962841
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->addView(Landroid/view/View;)V

    .line 1962842
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->g:LX/BwE;

    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    const-string v2, "video_channel_feed"

    invoke-virtual {v0, v3, v1, v2}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->j:LX/1SX;

    .line 1962843
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->h:LX/AjP;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->k:LX/AjN;

    .line 1962844
    iput-object v1, v0, LX/AjP;->c:LX/AjN;

    .line 1962845
    return-void
.end method

.method private static a(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;LX/D5t;LX/1C2;LX/D4G;LX/0sV;Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;LX/Ac6;LX/BwE;LX/AjP;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;",
            "LX/D5t;",
            "LX/1C2;",
            "LX/D4G;",
            "LX/0sV;",
            "Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;",
            "LX/Ac6;",
            "LX/BwE;",
            "LX/AjP;",
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1962846
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a:LX/D5t;

    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b:LX/1C2;

    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->c:LX/D4G;

    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->d:LX/0sV;

    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->e:Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->f:LX/Ac6;

    iput-object p7, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->g:LX/BwE;

    iput-object p8, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->h:LX/AjP;

    iput-object p9, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->i:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-static {v9}, LX/D5t;->a(LX/0QB;)LX/D5t;

    move-result-object v1

    check-cast v1, LX/D5t;

    invoke-static {v9}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static {v9}, LX/D4G;->a(LX/0QB;)LX/D4G;

    move-result-object v3

    check-cast v3, LX/D4G;

    invoke-static {v9}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v4

    check-cast v4, LX/0sV;

    invoke-static {v9}, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    invoke-static {v9}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v6

    check-cast v6, LX/Ac6;

    const-class v7, LX/BwE;

    invoke-interface {v9, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BwE;

    invoke-static {v9}, LX/AjP;->b(LX/0QB;)LX/AjP;

    move-result-object v8

    check-cast v8, LX/AjP;

    const/16 v10, 0x3851

    invoke-static {v9, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;LX/D5t;LX/1C2;LX/D4G;LX/0sV;Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;LX/Ac6;LX/BwE;LX/AjP;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;LX/2pa;)V
    .locals 4

    .prologue
    .line 1962847
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->e:Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, LX/D4p;

    invoke-direct {v2, p0}, LX/D4p;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    invoke-virtual {v0, v1, p1, v2}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962848
    invoke-static {p1}, LX/393;->m(LX/2pa;)Z

    move-result v0

    .line 1962849
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 1962850
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 1962851
    :goto_0
    return-void

    .line 1962852
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 1962853
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1962854
    :cond_0
    :goto_0
    return-void

    .line 1962855
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->h:LX/AjP;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V
    .locals 1

    .prologue
    .line 1962819
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->h:LX/AjP;

    invoke-virtual {v0}, LX/AjP;->b()V

    .line 1962820
    return-void
.end method

.method public static synthetic i(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)I
    .locals 2

    .prologue
    .line 1962821
    iget v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    return v0
.end method


# virtual methods
.method public final a(LX/04G;)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 1962768
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_1

    .line 1962769
    :cond_0
    :goto_0
    return-void

    .line 1962770
    :cond_1
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    .line 1962771
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v7

    .line 1962772
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1962773
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1962774
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1962775
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    const-string v1, "swipe_count"

    iget v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962776
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    iget-object v6, v2, LX/04g;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v8

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v9, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v10, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    move-object v2, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1962777
    iput-object v11, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    .line 1962778
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    .line 1962779
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1962780
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/2pa;IIZLX/2oi;)V
    .locals 14

    .prologue
    .line 1962783
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1962784
    invoke-static {p0, p1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;LX/2pa;)V

    .line 1962785
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1962786
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    .line 1962787
    invoke-static {p1}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962788
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_0

    .line 1962789
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1962790
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b:LX/1C2;

    iget-object v2, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    sget-object v4, LX/04G;->CHANNEL_PLAYER:LX/04G;

    iget-object v5, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    sget-object v7, LX/04g;->BY_USER:LX/04g;

    iget-object v7, v7, LX/04g;->value:Ljava/lang/String;

    iget-object v10, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-virtual/range {v1 .. v13}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1962791
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->d:LX/0sV;

    iget-boolean v1, v1, LX/0sV;->i:Z

    if-eqz v1, :cond_1

    .line 1962792
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    .line 1962793
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->s:I

    .line 1962794
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x0

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1962795
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move-object/from16 v0, p5

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1962796
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move/from16 v0, p2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1962797
    if-eqz p4, :cond_2

    .line 1962798
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1962799
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->setVisibility(I)V

    .line 1962800
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1962801
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v0

    return v0
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 1962802
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public getCurrentVideoId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1962803
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    if-nez v0, :cond_0

    .line 1962804
    const/4 v0, 0x0

    .line 1962805
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoResolution()LX/2oi;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1962806
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962807
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1962808
    if-nez v0, :cond_0

    .line 1962809
    const/4 v0, 0x0

    .line 1962810
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962811
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1962812
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    goto :goto_0
.end method

.method public setAutoplaySettingFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1962813
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->q:LX/0QK;

    .line 1962814
    return-void
.end method

.method public setNextStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1962815
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    .line 1962816
    return-void
.end method

.method public setPlayerOrigin(LX/04D;)V
    .locals 0

    .prologue
    .line 1962781
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    .line 1962782
    return-void
.end method

.method public setPreviousStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1962817
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->p:LX/0QK;

    .line 1962818
    return-void
.end method
