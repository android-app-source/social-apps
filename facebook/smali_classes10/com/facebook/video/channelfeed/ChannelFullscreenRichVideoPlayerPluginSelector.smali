.class public Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1965383
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/19m;LX/0sV;LX/Ac6;LX/1b4;Ljava/lang/Boolean;LX/0Uh;)V
    .locals 11
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965341
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1965342
    new-instance v3, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1965343
    new-instance v4, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-direct {v4, p1}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    .line 1965344
    new-instance v5, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {}, LX/3Hk;->a()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v5, v1}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;-><init>(Landroid/content/Context;)V

    .line 1965345
    new-instance v6, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    invoke-direct {v6, p1}, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;-><init>(Landroid/content/Context;)V

    .line 1965346
    new-instance v7, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1965347
    new-instance v8, LX/Bvl;

    invoke-direct {v8, p1}, LX/Bvl;-><init>(Landroid/content/Context;)V

    .line 1965348
    invoke-virtual/range {p5 .. p5}, LX/1b4;->ab()Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, LX/BvW;

    invoke-direct {v1, p1}, LX/BvW;-><init>(Landroid/content/Context;)V

    .line 1965349
    :goto_0
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->k:Z

    .line 1965350
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    new-instance v9, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v10, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v9, p1, v10}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v2, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->b:LX/0Px;

    .line 1965351
    iget-boolean v2, p3, LX/0sV;->l:Z

    if-eqz v2, :cond_9

    new-instance v2, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPluginWithSocialContext;

    invoke-direct {v2, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPluginWithSocialContext;-><init>(Landroid/content/Context;)V

    .line 1965352
    :goto_1
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    iget-object v10, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v9, v10}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v4, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-direct {v4, p1}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    .line 1965353
    iget-boolean v4, p3, LX/0sV;->i:Z

    if-eqz v4, :cond_0

    .line 1965354
    new-instance v4, Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;

    invoke-direct {v4, p1}, Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965355
    :cond_0
    iget-boolean v4, p3, LX/0sV;->h:Z

    if-eqz v4, :cond_1

    .line 1965356
    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965357
    :cond_1
    const/16 v4, 0x466

    const/4 v9, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v4, v9}, LX/0Uh;->a(IZ)Z

    move-result v4

    .line 1965358
    const/16 v9, 0x427

    const/4 v10, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v9

    .line 1965359
    if-eqz v4, :cond_2

    .line 1965360
    new-instance v10, LX/3Hz;

    invoke-direct {v10, p1}, LX/3Hz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965361
    :cond_2
    if-eqz v9, :cond_3

    .line 1965362
    new-instance v10, LX/31H;

    invoke-direct {v10, p1}, LX/31H;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965363
    :cond_3
    if-nez v4, :cond_4

    if-eqz v9, :cond_5

    .line 1965364
    :cond_4
    new-instance v4, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v9, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v4, p1, v9}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965365
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->c:LX/0Px;

    .line 1965366
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    .line 1965367
    new-instance v4, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;

    invoke-direct {v4, p1}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v10, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v9, p1, v10}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, LX/BvH;

    invoke-direct {v9, p1}, LX/BvH;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, LX/Bv4;

    invoke-direct {v9, p1}, LX/Bv4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, LX/BvE;

    invoke-direct {v9, p1}, LX/BvE;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    new-instance v9, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965368
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->e:LX/0Px;

    .line 1965369
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    new-instance v4, LX/7N2;

    invoke-direct {v4, p1}, LX/7N2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v4, LX/7Ng;

    invoke-direct {v4, p1}, LX/7Ng;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v4, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {v4, p1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v4, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v4, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    .line 1965370
    iget-boolean v4, p3, LX/0sV;->h:Z

    if-eqz v4, :cond_6

    .line 1965371
    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965372
    :cond_6
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->d:LX/0Px;

    .line 1965373
    iget-object v2, p0, LX/3Ge;->d:LX/0Px;

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->j:LX/0Px;

    .line 1965374
    iget-object v2, p0, LX/3Ge;->d:LX/0Px;

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->i:LX/0Px;

    .line 1965375
    iget-object v2, p0, LX/3Ge;->c:LX/0Px;

    iput-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->h:LX/0Px;

    .line 1965376
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    .line 1965377
    iget-boolean v2, p3, LX/0sV;->h:Z

    if-eqz v2, :cond_7

    .line 1965378
    invoke-virtual {v1, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965379
    :cond_7
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;->f:LX/0Px;

    .line 1965380
    return-void

    .line 1965381
    :cond_8
    new-instance v1, LX/BvQ;

    invoke-direct {v1, p1}, LX/BvQ;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1965382
    :cond_9
    new-instance v2, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;
    .locals 8

    .prologue
    .line 1965339
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v2

    check-cast v2, LX/19m;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-static {p0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v4

    check-cast v4, LX/Ac6;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v5

    check-cast v5, LX/1b4;

    invoke-static {p0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/video/channelfeed/ChannelFullscreenRichVideoPlayerPluginSelector;-><init>(Landroid/content/Context;LX/19m;LX/0sV;LX/Ac6;LX/1b4;Ljava/lang/Boolean;LX/0Uh;)V

    .line 1965340
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1965329
    const-class v0, LX/BvQ;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-nez v0, :cond_0

    const-class v0, LX/BvW;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1965330
    :cond_0
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    .line 1965331
    :goto_0
    return-object v0

    .line 1965332
    :cond_1
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1965333
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965334
    :cond_2
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1965335
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965336
    :cond_3
    const-class v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1965337
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965338
    :cond_4
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
