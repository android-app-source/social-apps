.class public Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D4d;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/BtI;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1964772
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1964773
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    .line 1964774
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->b:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    .line 1964775
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;
    .locals 5

    .prologue
    .line 1964782
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    monitor-enter v1

    .line 1964783
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1964784
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1964785
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1964786
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1964787
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V

    .line 1964788
    move-object v0, p0

    .line 1964789
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1964790
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1964791
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1964792
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1964793
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1964778
    check-cast p2, LX/D4d;

    .line 1964779
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1964780
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->b:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    new-instance v1, LX/82O;

    iget-object v2, p2, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/82O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1964781
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1964776
    check-cast p1, LX/D4d;

    .line 1964777
    invoke-static {p1}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/D4d;)Z

    move-result v0

    return v0
.end method
