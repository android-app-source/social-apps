.class public Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Li;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/video/player/CountdownRingContainer;

.field public e:LX/2oa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2oa",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/2oa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2oa",
            "<",
            "LX/2os;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1966090
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966091
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 1966057
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966058
    const v0, 0x7f030271

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1966059
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->a:Landroid/view/View;

    .line 1966060
    const v0, 0x7f0d090f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->b:Landroid/view/View;

    .line 1966061
    const v0, 0x7f0d06a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->c:Landroid/view/View;

    .line 1966062
    const v0, 0x7f0d0910

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/CountdownRingContainer;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    .line 1966063
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->b:Landroid/view/View;

    new-instance p1, LX/D6c;

    invoke-direct {p1, p0}, LX/D6c;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1966064
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->c:Landroid/view/View;

    new-instance p1, LX/D6d;

    invoke-direct {p1, p0}, LX/D6d;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1966065
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    new-instance p1, LX/D6e;

    invoke-direct {p1, p0}, LX/D6e;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/CountdownRingContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1966066
    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    const-wide/16 v3, 0x1388

    .line 1966067
    iput-wide v3, v1, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    .line 1966068
    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    new-instance v2, LX/D6f;

    invoke-direct {v2, p0}, LX/D6f;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    .line 1966069
    iput-object v2, v1, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    .line 1966070
    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->a:Landroid/view/View;

    new-instance v2, LX/D6g;

    invoke-direct {v2, p0}, LX/D6g;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1966071
    new-instance v0, LX/D6h;

    invoke-direct {v0, p0}, LX/D6h;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->e:LX/2oa;

    .line 1966072
    new-instance v0, LX/D6i;

    invoke-direct {v0, p0}, LX/D6i;-><init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->f:LX/2oa;

    .line 1966073
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;LX/04g;)V
    .locals 1

    .prologue
    .line 1966085
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0}, LX/7Lh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1966086
    invoke-static {p0}, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->y(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    .line 1966087
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0, p1}, LX/7Lh;->b(LX/04g;)V

    .line 1966088
    invoke-static {p0}, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->v(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    .line 1966089
    :cond_0
    return-void
.end method

.method public static v(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V
    .locals 3

    .prologue
    .line 1966092
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1966093
    :goto_0
    return-void

    .line 1966094
    :cond_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7ME;

    sget-object v2, LX/7MD;->DEFAULT:LX/7MD;

    invoke-direct {v1, v2}, LX/7ME;-><init>(LX/7MD;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public static y(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V
    .locals 2

    .prologue
    .line 1966083
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1966084
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1966078
    if-eqz p2, :cond_0

    .line 1966079
    invoke-static {p0}, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->y(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V

    .line 1966080
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->e:LX/2oa;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1966081
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->f:LX/2oa;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1966082
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1966074
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->e:LX/2oa;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1966075
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->f:LX/2oa;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1966076
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    invoke-virtual {v0}, Lcom/facebook/video/player/CountdownRingContainer;->b()V

    .line 1966077
    return-void
.end method
