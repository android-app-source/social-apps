.class public Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;
.super LX/7Mz;
.source ""


# instance fields
.field public final o:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1965976
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1965977
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1965968
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965969
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1965972
    invoke-direct {p0, p1, p2, p3}, LX/7Mz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965973
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/D6a;

    invoke-direct {v1, p0}, LX/D6a;-><init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1965974
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;->o:Landroid/view/View;

    .line 1965975
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1965978
    invoke-super {p0, p1, p2}, LX/7Mz;->a(LX/2pa;Z)V

    .line 1965979
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "SeekPositionMsKey"

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965980
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "SeekPositionMsKey"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1965981
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_3

    .line 1965982
    const/4 v0, 0x1

    .line 1965983
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;->o:Landroid/view/View;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1965984
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    if-eqz v0, :cond_2

    .line 1965985
    invoke-virtual {p0, v1}, LX/7Mz;->setQualitySelectorVisibility(I)V

    .line 1965986
    :cond_0
    :goto_2
    return-void

    .line 1965987
    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 1965988
    :cond_2
    const/16 v0, 0x8

    .line 1965989
    iget-object v1, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1965990
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1965971
    const/4 v0, 0x0

    return v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1965970
    const v0, 0x7f03026b

    return v0
.end method
