.class public Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;
.super LX/D6Y;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public o:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/D6W;

.field public s:LX/3IC;

.field public t:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1965915
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1965916
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1965920
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965921
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1965922
    invoke-direct {p0, p1, p2, p3}, LX/D6Y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965923
    const-class v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1965924
    new-instance v0, LX/D6W;

    invoke-direct {v0, p0}, LX/D6W;-><init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->r:LX/D6W;

    .line 1965925
    new-instance v0, LX/3IC;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->r:LX/D6W;

    iget-object p2, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->r:LX/D6W;

    invoke-direct {v0, p1, v1, p2}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->s:LX/3IC;

    .line 1965926
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance p2, LX/D6X;

    invoke-direct {p2, p0}, LX/D6X;-><init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;)V

    invoke-direct {v0, v1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->t:Landroid/view/GestureDetector;

    .line 1965927
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object p0

    check-cast p0, LX/19m;

    iput-object p0, p1, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->o:LX/19m;

    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x333d5bc3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1965917
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1965918
    iget-object v0, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->s:LX/3IC;

    invoke-virtual {v0, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x1985e295

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1965919
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/D6Y;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x500b187b

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
