.class public Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lj;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1965934
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1965935
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1965936
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965937
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1965938
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965939
    const v0, 0x7f030264

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1965940
    const v0, 0x7f0d0903

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1965941
    new-instance v1, LX/D6Z;

    invoke-direct {v1, p0}, LX/D6Z;-><init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1965942
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1965943
    iput-object p1, p0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;->a:LX/2pa;

    .line 1965944
    return-void
.end method
