.class public Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextIconPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965599
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1965600
    iput-object p1, p0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    .line 1965601
    iput-object p2, p0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1965602
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;
    .locals 5

    .prologue
    .line 1965603
    const-class v1, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    monitor-enter v1

    .line 1965604
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1965605
    sput-object v2, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1965606
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965607
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1965608
    new-instance p0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextIconPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;-><init>(Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 1965609
    move-object v0, p0

    .line 1965610
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1965611
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1965612
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1965613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLActor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965614
    const/4 v0, 0x0

    .line 1965615
    if-eqz p0, :cond_0

    .line 1965616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1965617
    :cond_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1965618
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1965619
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1965620
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1965621
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965622
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1965623
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1965624
    if-nez v1, :cond_2

    move-object v1, v2

    .line 1965625
    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1965626
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1965627
    const v4, 0x7f0d02c4

    iget-object v5, p0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v4, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1965628
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1965629
    :goto_1
    const v1, 0x7f0d02c4

    iget-object v4, p0, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    new-instance v5, LX/8Ct;

    if-eqz v0, :cond_0

    const v3, 0x7f021a25

    :cond_0
    const v0, 0x7f0b16b6

    sget-object v6, LX/8Cs;->END:LX/8Cs;

    invoke-direct {v5, v3, v0, v6}, LX/8Ct;-><init>(IILX/8Cs;)V

    invoke-interface {p1, v1, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1965630
    :cond_1
    return-object v2

    .line 1965631
    :cond_2
    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v0, v3

    .line 1965632
    goto :goto_1
.end method
