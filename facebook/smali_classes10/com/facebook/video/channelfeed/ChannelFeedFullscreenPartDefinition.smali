.class public Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "LX/2oV;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/D4l;


# direct methods
.method public constructor <init>(LX/7zZ;LX/1AZ;LX/D4l;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962669
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1962670
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->b:LX/D4l;

    .line 1962671
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->a:LX/1Aa;

    .line 1962672
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;
    .locals 6

    .prologue
    .line 1962673
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    monitor-enter v1

    .line 1962674
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962675
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962676
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962677
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962678
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v3

    check-cast v3, LX/7zZ;

    const-class v4, LX/1AZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1AZ;

    const-class v5, LX/D4l;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/D4l;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;-><init>(LX/7zZ;LX/1AZ;LX/D4l;)V

    .line 1962679
    move-object v0, p0

    .line 1962680
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962681
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962682
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962683
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1962684
    check-cast p2, Ljava/lang/String;

    .line 1962685
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->b:LX/D4l;

    .line 1962686
    new-instance p1, LX/D4k;

    invoke-static {v0}, LX/D4m;->a(LX/0QB;)LX/D4m;

    move-result-object p0

    check-cast p0, LX/D4m;

    invoke-direct {p1, p2, p0}, LX/D4k;-><init>(Ljava/lang/String;LX/D4m;)V

    .line 1962687
    move-object v0, p1

    .line 1962688
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x240ea62a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1962689
    check-cast p2, LX/2oV;

    .line 1962690
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->a:LX/1Aa;

    invoke-virtual {v1, p4, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1962691
    const/16 v1, 0x1f

    const v2, -0x5f376697

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
