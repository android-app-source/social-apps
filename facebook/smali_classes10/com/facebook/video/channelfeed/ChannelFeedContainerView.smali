.class public Lcom/facebook/video/channelfeed/ChannelFeedContainerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/D4i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D4G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1962091
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1962092
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1962104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1962105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1962101
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1962102
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;

    invoke-static {v0, p0}, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1962103
    return-void
.end method

.method private static a(Lcom/facebook/video/channelfeed/ChannelFeedContainerView;LX/D4i;LX/D4G;)V
    .locals 0

    .prologue
    .line 1962100
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->a:LX/D4i;

    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->b:LX/D4G;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;

    invoke-static {v1}, LX/D4i;->a(LX/0QB;)LX/D4i;

    move-result-object v0

    check-cast v0, LX/D4i;

    invoke-static {v1}, LX/D4G;->a(LX/0QB;)LX/D4G;

    move-result-object v1

    check-cast v1, LX/D4G;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->a(Lcom/facebook/video/channelfeed/ChannelFeedContainerView;LX/D4i;LX/D4G;)V

    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1962093
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->a:LX/D4i;

    .line 1962094
    iget-object v1, v0, LX/D4i;->a:LX/D4g;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/D4g;->removeMessages(I)V

    .line 1962095
    invoke-static {v0}, LX/D4i;->e(LX/D4i;)V

    .line 1962096
    invoke-static {v0}, LX/D4i;->c$redex0(LX/D4i;)V

    .line 1962097
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedContainerView;->b:LX/D4G;

    .line 1962098
    iget-object v1, v0, LX/D4G;->a:LX/D4D;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/D4D;->removeMessages(I)V

    .line 1962099
    const/4 v0, 0x0

    return v0
.end method
