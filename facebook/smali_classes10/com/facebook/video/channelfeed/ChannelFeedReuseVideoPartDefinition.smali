.class public Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;
.super Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/channelfeed/HasChannelFeedParams;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "Lcom/facebook/video/channelfeed/CanReusePlayer;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pe;",
        ":",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        ">",
        "Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;LX/1AM;LX/0bH;LX/04J;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;",
            "Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;",
            "LX/2mZ;",
            "Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;",
            ">;",
            "LX/1AM;",
            "LX/0bH;",
            "LX/04J;",
            "LX/0Ot",
            "<",
            "LX/3Gg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963563
    invoke-direct/range {p0 .. p9}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;LX/1AM;LX/0bH;LX/04J;LX/0Ot;)V

    .line 1963564
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;
    .locals 13

    .prologue
    .line 1963523
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    monitor-enter v1

    .line 1963524
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963525
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963526
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963527
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963528
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    const-class v6, LX/2mZ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    const/16 v8, 0x846

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v9

    check-cast v9, LX/1AM;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static {v0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v11

    check-cast v11, LX/04J;

    const/16 v12, 0x12e8

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;LX/1AM;LX/0bH;LX/04J;LX/0Ot;)V

    .line 1963529
    move-object v0, v3

    .line 1963530
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963531
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963532
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/D65;Lcom/facebook/video/engine/VideoPlayerParams;LX/D4U;LX/D5z;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D65;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "TE;",
            "LX/D5z;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1963534
    move-object v0, p3

    check-cast v0, LX/7ze;

    iget-object v1, p1, LX/D65;->b:LX/D5r;

    .line 1963535
    iget-object v2, v1, LX/D5r;->b:LX/D60;

    move-object v1, v2

    .line 1963536
    invoke-interface {v0, p4, v1}, LX/7ze;->a(Landroid/view/View;LX/2oV;)V

    move-object v0, p3

    .line 1963537
    check-cast v0, LX/D4U;

    .line 1963538
    iget-object v1, v0, LX/D4U;->t:LX/D6I;

    move-object v0, v1

    .line 1963539
    if-eqz v0, :cond_0

    .line 1963540
    iget-object v1, v0, LX/D6I;->f:LX/3FT;

    if-nez v1, :cond_1

    .line 1963541
    const/4 v1, 0x0

    .line 1963542
    :goto_0
    move-object v0, v1

    .line 1963543
    move-object v1, v0

    :goto_1
    move-object v0, p3

    .line 1963544
    check-cast v0, LX/D4U;

    .line 1963545
    iget-object v2, v0, LX/D4U;->q:LX/04D;

    move-object v0, v2

    .line 1963546
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1963547
    iget-object v0, p1, LX/D65;->b:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->a()I

    move-result v2

    iget-object v3, p1, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p1, LX/D65;->b:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/D4U;

    move-object v0, p4

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, LX/D5z;->a(Lcom/facebook/video/engine/VideoPlayerParams;ILcom/facebook/feed/rows/core/props/FeedProps;LX/2oO;LX/D4U;)V

    .line 1963548
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1963549
    return-void

    .line 1963550
    :cond_0
    invoke-virtual {p4}, LX/D5z;->a()V

    .line 1963551
    invoke-virtual {p4}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1963552
    :cond_1
    iget-object v1, v0, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-interface {p4, v1}, LX/3FT;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1963553
    iget-boolean v1, v0, LX/D6I;->l:Z

    if-nez v1, :cond_2

    .line 1963554
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/D6I;->l:Z

    .line 1963555
    invoke-interface {p4}, LX/3FT;->getAdditionalPlugins()Ljava/util/List;

    move-result-object v1

    .line 1963556
    if-eqz v1, :cond_2

    .line 1963557
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2oy;

    .line 1963558
    iget-object v3, v0, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1963559
    invoke-static {v3, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1963560
    goto :goto_2

    .line 1963561
    :cond_2
    iget-object v1, v0, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-interface {p4}, LX/3FT;->getPlayerType()LX/04G;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1963562
    iget-object v1, v0, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1993e4d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1963522
    check-cast p1, LX/D65;

    check-cast p2, Lcom/facebook/video/engine/VideoPlayerParams;

    check-cast p3, LX/D4U;

    check-cast p4, LX/D5z;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a(LX/D65;Lcom/facebook/video/engine/VideoPlayerParams;LX/D4U;LX/D5z;)V

    const/16 v1, 0x1f

    const v2, 0x14e5c5d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/D65;)Z
    .locals 1

    .prologue
    .line 1963519
    iget-object v0, p1, LX/D65;->b:LX/D5r;

    .line 1963520
    iget-boolean p0, v0, LX/D5r;->j:Z

    move v0, p0

    .line 1963521
    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1963518
    check-cast p1, LX/D65;

    invoke-virtual {p0, p1}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a(LX/D65;)Z

    move-result v0

    return v0
.end method
