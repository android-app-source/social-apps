.class public Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/D52;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1xc;

.field private final b:LX/BsK;

.field public final c:LX/D4z;

.field public final d:LX/1DR;

.field private final e:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

.field public final f:LX/1xv;


# direct methods
.method public constructor <init>(LX/1xc;LX/BsK;LX/D4z;LX/1DR;Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1xv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963229
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1963230
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->a:LX/1xc;

    .line 1963231
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->b:LX/BsK;

    .line 1963232
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->c:LX/D4z;

    .line 1963233
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->d:LX/1DR;

    .line 1963234
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->e:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    .line 1963235
    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->f:LX/1xv;

    .line 1963236
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;
    .locals 10

    .prologue
    .line 1963237
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    monitor-enter v1

    .line 1963238
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963239
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963242
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    invoke-static {v0}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v4

    check-cast v4, LX/1xc;

    invoke-static {v0}, LX/BsK;->a(LX/0QB;)LX/BsK;

    move-result-object v5

    check-cast v5, LX/BsK;

    invoke-static {v0}, LX/D4z;->a(LX/0QB;)LX/D4z;

    move-result-object v6

    check-cast v6, LX/D4z;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v9

    check-cast v9, LX/1xv;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;-><init>(LX/1xc;LX/BsK;LX/D4z;LX/1DR;Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1xv;)V

    .line 1963243
    move-object v0, v3

    .line 1963244
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963245
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963246
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1963224
    check-cast p2, LX/D52;

    check-cast p3, LX/1Pk;

    .line 1963225
    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->e:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    new-instance v0, LX/D50;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->b:LX/BsK;

    .line 1963226
    iget-object v3, v1, LX/BsK;->a:LX/1nq;

    move-object v3, v3

    .line 1963227
    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v5

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/D50;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;Landroid/content/Context;LX/1nq;LX/D52;LX/1SX;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1963228
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x10c30d60

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1963221
    check-cast p1, LX/D52;

    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 1963222
    const v1, 0x7f0d0081

    iget-object v2, p1, LX/D52;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTag(ILjava/lang/Object;)V

    .line 1963223
    const/16 v1, 0x1f

    const v2, 0x365be485

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1963218
    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 1963219
    const v0, 0x7f0d0081

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTag(ILjava/lang/Object;)V

    .line 1963220
    return-void
.end method
