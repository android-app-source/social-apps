.class public Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/D4q;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/23r;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/23r;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/23r;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962861
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1962862
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->a:LX/23r;

    .line 1962863
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->b:LX/0Ot;

    .line 1962864
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;
    .locals 5

    .prologue
    .line 1962865
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    monitor-enter v1

    .line 1962866
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962867
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962870
    new-instance v4, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    const-class v3, LX/23r;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/23r;

    const/16 p0, 0x848

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;-><init>(LX/23r;LX/0Ot;)V

    .line 1962871
    move-object v0, v4

    .line 1962872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1962876
    check-cast p2, LX/D4q;

    const/4 v2, 0x0

    .line 1962877
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1Nt;

    new-instance v7, LX/3FZ;

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->a:LX/23r;

    iget-object v1, p2, LX/D4q;->a:LX/3Qw;

    iget-object v3, p2, LX/D4q;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, p2, LX/D4q;->c:LX/D6L;

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    invoke-direct {v7, v0}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v6, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1962878
    return-object v2
.end method
