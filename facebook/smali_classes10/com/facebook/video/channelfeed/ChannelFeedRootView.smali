.class public Lcom/facebook/video/channelfeed/ChannelFeedRootView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0hE;


# static fields
.field private static final H:Ljava/lang/String;

.field public static final I:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/3DC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0hI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final J:LX/BV5;

.field private final K:LX/D5n;

.field private final L:LX/D5m;

.field public final M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

.field public final N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

.field private final O:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final P:LX/D5J;

.field public final Q:LX/7zh;

.field public final R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

.field public final S:Landroid/view/View;

.field public final T:LX/0g8;

.field public final U:LX/D4U;

.field private final V:LX/D4Z;

.field public final W:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/D6B;",
            ">;"
        }
    .end annotation
.end field

.field public a:LX/BV6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public aB:LX/D5l;

.field private final aa:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ac:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final ad:LX/D59;

.field private final ae:LX/BV4;

.field public final af:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final ag:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/D6B;",
            ">;"
        }
    .end annotation
.end field

.field private final ah:LX/3It;

.field public final ai:Landroid/view/OrientationEventListener;

.field public aj:LX/3Qw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:LX/1Qq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Landroid/content/Context;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Landroid/view/Window;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:LX/D5D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:LX/394;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:LX/1ly;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:LX/D5r;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private au:Z

.field public av:Z

.field public aw:Z

.field private ax:I

.field private ay:I

.field public az:I

.field public b:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/D6S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/D4V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/D5q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6Vd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/D4a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/99w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AjP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/D5E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/7za;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/D4i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/D4G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/D4m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1JD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/14w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/D5t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/D6C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/1Kt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/1LV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/04B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1964273
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->H:Ljava/lang/String;

    .line 1964274
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/04D;->VIDEO_SETS:LX/04D;

    iget-object v3, v3, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/04D;->SINGLE_CREATOR_SET_VIDEO:LX/04D;

    iget-object v3, v3, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/04D;->SINGLE_CREATOR_SET_FOOTER:LX/04D;

    iget-object v3, v3, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->I:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1964275
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1964276
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1964277
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1964278
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1964279
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1964280
    new-instance v0, LX/D5n;

    invoke-direct {v0, p0}, LX/D5n;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->K:LX/D5n;

    .line 1964281
    new-instance v0, LX/D5m;

    invoke-direct {v0, p0}, LX/D5m;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->L:LX/D5m;

    .line 1964282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    .line 1964283
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aa:Ljava/util/Map;

    .line 1964284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    .line 1964285
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    .line 1964286
    new-instance v0, LX/D5k;

    invoke-direct {v0, p0}, LX/D5k;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ae:LX/BV4;

    .line 1964287
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->af:Ljava/util/List;

    .line 1964288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    .line 1964289
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ax:I

    .line 1964290
    iput v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->az:I

    .line 1964291
    sget-object v0, LX/D5l;->RELATED_VIDEOS:LX/D5l;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    .line 1964292
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1964293
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContextThemeWrapper()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1964294
    const v1, 0x7f030272

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1964295
    const v0, 0x7f0d0912

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1964296
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1964297
    new-instance v1, LX/0g6;

    invoke-direct {v1, v0}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    .line 1964298
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->e:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 1964299
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    new-instance v1, LX/D5T;

    invoke-direct {v1, p0}, LX/D5T;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 1964300
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 1964301
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->y:LX/1LV;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 1964302
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->f:LX/D4V;

    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContextThemeWrapper()Landroid/content/Context;

    move-result-object v1

    .line 1964303
    sget-object v2, LX/D58;->a:LX/D58;

    move-object v2, v2

    .line 1964304
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedRootView$2;

    invoke-direct {v3, p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView$2;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964305
    new-instance v4, LX/D4U;

    invoke-static {v0}, LX/7za;->a(LX/0QB;)LX/7za;

    move-result-object v8

    check-cast v8, LX/7za;

    const-class v5, LX/BwE;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BwE;

    move-object v5, v1

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, LX/D4U;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/7za;LX/BwE;)V

    .line 1964306
    move-object v0, v4

    .line 1964307
    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964308
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    .line 1964309
    iput-object v1, v0, LX/D4U;->s:LX/D4m;

    .line 1964310
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    new-instance v1, LX/D5c;

    invoke-direct {v1, p0}, LX/D5c;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964311
    iput-object v1, v0, LX/D4U;->r:LX/0QK;

    .line 1964312
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    new-instance v1, LX/D5d;

    invoke-direct {v1, p0}, LX/D5d;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964313
    iput-object v1, v0, LX/D4U;->v:LX/0QK;

    .line 1964314
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    .line 1964315
    const v0, 0x7f0d0916

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    .line 1964316
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v0, v1}, LX/D4m;->a(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    .line 1964317
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    .line 1964318
    iput-object v1, v0, LX/D4m;->b:Landroid/view/Window;

    .line 1964319
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    new-instance v1, LX/D5e;

    invoke-direct {v1, p0}, LX/D5e;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964320
    iput-object v1, v0, LX/D4m;->e:LX/D5e;

    .line 1964321
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    new-instance v1, LX/D5g;

    invoke-direct {v1, p0}, LX/D5g;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964322
    iput-object v1, v0, LX/D6S;->d:LX/D5f;

    .line 1964323
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    new-instance v1, LX/D5h;

    invoke-direct {v1, p0}, LX/D5h;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964324
    iput-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    .line 1964325
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    new-instance v1, LX/D5i;

    invoke-direct {v1, p0}, LX/D5i;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964326
    iput-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->p:LX/0QK;

    .line 1964327
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    new-instance v1, LX/D5j;

    invoke-direct {v1, p0}, LX/D5j;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964328
    iput-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->q:LX/0QK;

    .line 1964329
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0915

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->O:LX/0zw;

    .line 1964330
    new-instance v0, LX/D5J;

    invoke-direct {v0, p0}, LX/D5J;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->P:LX/D5J;

    .line 1964331
    const v0, 0x7f0d0914

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    .line 1964332
    const v0, 0x7f0d0913

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    .line 1964333
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-virtual {v0, v1}, LX/D4i;->a(Landroid/view/View;)V

    .line 1964334
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContextThemeWrapper()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    .line 1964335
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v0, v1}, LX/D4i;->a(Landroid/view/View;)V

    .line 1964336
    new-instance v0, LX/D5K;

    invoke-direct {v0, p0}, LX/D5K;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->Q:LX/7zh;

    .line 1964337
    new-instance v0, LX/D5L;

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/D5L;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ai:Landroid/view/OrientationEventListener;

    .line 1964338
    new-instance v0, LX/D59;

    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContextThemeWrapper()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/D59;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    .line 1964339
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    invoke-virtual {v0, v1}, LX/D4i;->a(Landroid/view/View;)V

    .line 1964340
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->i:LX/D4a;

    new-instance v1, LX/D5M;

    invoke-direct {v1, p0}, LX/D5M;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964341
    new-instance v4, LX/D4Z;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v6

    check-cast v6, LX/189;

    const/16 v5, 0x123

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v9

    check-cast v9, LX/20h;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v10

    check-cast v10, LX/1zf;

    move-object v5, v1

    invoke-direct/range {v4 .. v10}, LX/D4Z;-><init>(LX/0QK;LX/189;LX/0Or;LX/0SG;LX/20h;LX/1zf;)V

    .line 1964342
    move-object v0, v4

    .line 1964343
    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->V:LX/D4Z;

    .line 1964344
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->V:LX/D4Z;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    .line 1964345
    iput-object v1, v0, LX/D4Z;->g:LX/D6S;

    .line 1964346
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->j:LX/99w;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->V:LX/D4Z;

    const/4 v5, 0x0

    .line 1964347
    const/4 v2, 0x3

    new-array v2, v2, [LX/0b2;

    new-instance v3, LX/D4X;

    invoke-direct {v3, v1}, LX/D4X;-><init>(LX/D4Z;)V

    aput-object v3, v2, v5

    const/4 v3, 0x1

    new-instance v4, LX/D4Y;

    invoke-direct {v4, v1}, LX/D4Y;-><init>(LX/D4Z;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, LX/D4W;

    invoke-direct {v4, v1}, LX/D4W;-><init>(LX/D4Z;)V

    aput-object v4, v2, v3

    .line 1964348
    move-object v1, v2

    .line 1964349
    invoke-virtual {v0, v1}, LX/99w;->a([LX/0b2;)V

    .line 1964350
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    new-instance v1, LX/D5N;

    invoke-direct {v1, p0}, LX/D5N;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964351
    iput-object v1, v0, LX/AjP;->c:LX/AjN;

    .line 1964352
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a:LX/BV6;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/BV6;->a(Z)LX/BV5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->J:LX/BV5;

    .line 1964353
    new-instance v0, LX/D5O;

    invoke-direct {v0, p0}, LX/D5O;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ah:LX/3It;

    .line 1964354
    return-void

    .line 1964355
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private A()V
    .locals 3

    .prologue
    .line 1964356
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 1964357
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->j:LX/99w;

    .line 1964358
    invoke-static {v0}, LX/99w;->d(LX/99w;)V

    .line 1964359
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p:LX/1AM;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->K:LX/D5n;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1964360
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p:LX/1AM;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->L:LX/D5m;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1964361
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->J:LX/BV5;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ae:LX/BV4;

    .line 1964362
    iput-object v1, v0, LX/BV5;->e:LX/BV4;

    .line 1964363
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1964364
    return-void
.end method

.method private B()V
    .locals 3

    .prologue
    .line 1964365
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 1964366
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 1964367
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->j:LX/99w;

    .line 1964368
    iget-object v1, v0, LX/99w;->a:LX/1B1;

    iget-object v2, v0, LX/99w;->b:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 1964369
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p:LX/1AM;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->K:LX/D5n;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1964370
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p:LX/1AM;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->L:LX/D5m;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1964371
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->J:LX/BV5;

    const/4 v1, 0x0

    .line 1964372
    iput-object v1, v0, LX/BV5;->e:LX/BV4;

    .line 1964373
    return-void
.end method

.method public static a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1964374
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1964375
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    .line 1964376
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 1964377
    goto :goto_0

    .line 1964378
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v3}, LX/D6S;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    .line 1964379
    :goto_1
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v0}, LX/D6S;->size()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1964380
    goto :goto_1

    .line 1964381
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private a(ILcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1964382
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1964383
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->A:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1964384
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v3, v0

    move v1, p1

    :goto_0
    if-ge v3, v8, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;

    .line 1964385
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1964386
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_3

    .line 1964387
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1964388
    invoke-static {v0, v6, v7}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 1964389
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    invoke-virtual {v2, v0}, LX/D5t;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v9

    .line 1964390
    if-eqz v9, :cond_3

    .line 1964391
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    .line 1964392
    iget-object v10, v2, LX/0sV;->x:LX/0ad;

    sget-short v11, LX/0sW;->c:S

    iget-object v12, v2, LX/0sV;->w:LX/0Uh;

    const/16 p1, 0x28

    const/4 p2, 0x0

    invoke-virtual {v12, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result v12

    invoke-interface {v10, v11, v12}, LX/0ad;->a(SZ)Z

    move-result v10

    move v2, v10

    .line 1964393
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    .line 1964394
    iget-object v10, v2, LX/D6S;->b:Ljava/util/Set;

    invoke-interface {v10, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    move v2, v10

    .line 1964395
    if-nez v2, :cond_3

    .line 1964396
    :cond_0
    iget-object v10, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v11

    invoke-virtual {v10, v1, v11}, LX/D6S;->a(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1964397
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    const/4 v10, 0x1

    invoke-virtual {v1, v0, v10}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 1964398
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 1964399
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1964400
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1964401
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z:LX/04B;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/04B;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1964402
    :cond_2
    return-object v4

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(LX/04G;LX/04G;II)V
    .locals 17

    .prologue
    .line 1964403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37Y;

    invoke-virtual {v1}, LX/37Y;->g()LX/38p;

    move-result-object v1

    invoke-virtual {v1}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1964404
    :goto_0
    return-void

    .line 1964405
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v1, LX/3Qw;->q:LX/19o;

    .line 1964406
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v6, v1, LX/3Qw;->r:LX/03z;

    .line 1964407
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->m:LX/1C2;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getTrackingCodes()LX/0lF;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->g:LX/04D;

    move-object v9, v1

    :goto_3
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    iget-object v0, v1, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v16, v0

    new-instance v1, LX/0J6;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->av:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aw:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aA:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-boolean v8, v8, LX/3Qw;->s:Z

    invoke-direct/range {v1 .. v8}, LX/0J6;-><init>(ZZZLX/19o;LX/03z;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Z)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v13, v2, LX/3Qw;->l:LX/0JG;

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v14, v2, LX/3Qw;->m:Ljava/lang/String;

    :goto_6
    move-object v2, v10

    move-object v3, v11

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object v6, v15

    move-object v7, v9

    move-object/from16 v8, v16

    move/from16 v9, p3

    move/from16 v10, p4

    move-object v11, v1

    invoke-virtual/range {v2 .. v14}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    goto/16 :goto_0

    .line 1964408
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 1964409
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 1964410
    :cond_3
    sget-object v1, LX/04D;->UNKNOWN:LX/04D;

    move-object v9, v1

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    const/4 v13, 0x0

    goto :goto_5

    :cond_6
    const/4 v14, 0x0

    goto :goto_6
.end method

.method public static a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/0QK;LX/0QK;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "LX/D5z;",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0QK",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1964411
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1964412
    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/D5W;

    invoke-direct {v2, p0, v0, p1, p2}, LX/D5W;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/0QK;LX/0QK;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1964413
    return-void
.end method

.method private static a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/BV6;LX/1DS;LX/0Ot;LX/D6S;LX/1Db;LX/D4V;LX/D5q;LX/6Vd;LX/D4a;LX/99w;LX/AjP;LX/D5E;LX/1C2;LX/7za;LX/D4i;LX/1AM;LX/D4G;LX/D4m;LX/1JD;LX/0Ot;LX/14w;LX/D5t;LX/D6C;LX/1Kt;LX/1LV;LX/04B;LX/0SG;Landroid/os/Handler;LX/3DC;LX/0sV;LX/0hB;LX/0hI;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedRootView;",
            "LX/BV6;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;",
            ">;",
            "LX/D6S;",
            "LX/1Db;",
            "LX/D4V;",
            "LX/D5q;",
            "LX/6Vd;",
            "LX/D4a;",
            "LX/99w;",
            "LX/AjP;",
            "LX/D5E;",
            "LX/1C2;",
            "LX/7za;",
            "LX/D4i;",
            "LX/1AM;",
            "LX/D4G;",
            "LX/D4m;",
            "LX/1JD;",
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;",
            "LX/14w;",
            "LX/D5t;",
            "LX/D6C;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/1LV;",
            "LX/04B;",
            "LX/0SG;",
            "Landroid/os/Handler;",
            "LX/3DC;",
            "LX/0sV;",
            "LX/0hB;",
            "LX/0hI;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1964640
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a:LX/BV6;

    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->b:LX/1DS;

    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->e:LX/1Db;

    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->f:LX/D4V;

    iput-object p7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->g:LX/D5q;

    iput-object p8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->h:LX/6Vd;

    iput-object p9, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->i:LX/D4a;

    iput-object p10, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->j:LX/99w;

    iput-object p11, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    iput-object p12, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->l:LX/D5E;

    iput-object p13, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->m:LX/1C2;

    iput-object p14, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p:LX/1AM;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->s:LX/1JD;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->t:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->u:LX/14w;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->w:LX/D6C;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->y:LX/1LV;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z:LX/04B;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->A:LX/0SG;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->B:Landroid/os/Handler;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->C:LX/3DC;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->E:LX/0hB;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->F:LX/0hI;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->G:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 36

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v35

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    const-class v3, LX/BV6;

    move-object/from16 v0, v35

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/BV6;

    invoke-static/range {v35 .. v35}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v4

    check-cast v4, LX/1DS;

    const/16 v5, 0x37d2

    move-object/from16 v0, v35

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v35 .. v35}, LX/D6S;->a(LX/0QB;)LX/D6S;

    move-result-object v6

    check-cast v6, LX/D6S;

    invoke-static/range {v35 .. v35}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v7

    check-cast v7, LX/1Db;

    const-class v8, LX/D4V;

    move-object/from16 v0, v35

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/D4V;

    const-class v9, LX/D5q;

    move-object/from16 v0, v35

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/D5q;

    invoke-static/range {v35 .. v35}, LX/6Vd;->a(LX/0QB;)LX/6Vd;

    move-result-object v10

    check-cast v10, LX/6Vd;

    const-class v11, LX/D4a;

    move-object/from16 v0, v35

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/D4a;

    invoke-static/range {v35 .. v35}, LX/99w;->a(LX/0QB;)LX/99w;

    move-result-object v12

    check-cast v12, LX/99w;

    invoke-static/range {v35 .. v35}, LX/AjP;->a(LX/0QB;)LX/AjP;

    move-result-object v13

    check-cast v13, LX/AjP;

    const-class v14, LX/D5E;

    move-object/from16 v0, v35

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/D5E;

    invoke-static/range {v35 .. v35}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v15

    check-cast v15, LX/1C2;

    invoke-static/range {v35 .. v35}, LX/7za;->a(LX/0QB;)LX/7za;

    move-result-object v16

    check-cast v16, LX/7za;

    invoke-static/range {v35 .. v35}, LX/D4i;->a(LX/0QB;)LX/D4i;

    move-result-object v17

    check-cast v17, LX/D4i;

    invoke-static/range {v35 .. v35}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v18

    check-cast v18, LX/1AM;

    invoke-static/range {v35 .. v35}, LX/D4G;->a(LX/0QB;)LX/D4G;

    move-result-object v19

    check-cast v19, LX/D4G;

    invoke-static/range {v35 .. v35}, LX/D4m;->a(LX/0QB;)LX/D4m;

    move-result-object v20

    check-cast v20, LX/D4m;

    const-class v21, LX/1JD;

    move-object/from16 v0, v35

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/1JD;

    const/16 v22, 0x1317

    move-object/from16 v0, v35

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v35 .. v35}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v23

    check-cast v23, LX/14w;

    invoke-static/range {v35 .. v35}, LX/D5t;->a(LX/0QB;)LX/D5t;

    move-result-object v24

    check-cast v24, LX/D5t;

    const-class v25, LX/D6C;

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/D6C;

    invoke-static/range {v35 .. v35}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v26

    check-cast v26, LX/1Kt;

    invoke-static/range {v35 .. v35}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v27

    check-cast v27, LX/1LV;

    invoke-static/range {v35 .. v35}, LX/04B;->a(LX/0QB;)LX/04B;

    move-result-object v28

    check-cast v28, LX/04B;

    invoke-static/range {v35 .. v35}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v29

    check-cast v29, LX/0SG;

    invoke-static/range {v35 .. v35}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v30

    check-cast v30, Landroid/os/Handler;

    invoke-static/range {v35 .. v35}, LX/3DC;->a(LX/0QB;)LX/3DC;

    move-result-object v31

    check-cast v31, LX/3DC;

    invoke-static/range {v35 .. v35}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v32

    check-cast v32, LX/0sV;

    invoke-static/range {v35 .. v35}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v33

    check-cast v33, LX/0hB;

    invoke-static/range {v35 .. v35}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v34

    check-cast v34, LX/0hI;

    invoke-static/range {v35 .. v35}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v35

    check-cast v35, LX/03V;

    invoke-static/range {v2 .. v35}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/BV6;LX/1DS;LX/0Ot;LX/D6S;LX/1Db;LX/D4V;LX/D5q;LX/6Vd;LX/D4a;LX/99w;LX/AjP;LX/D5E;LX/1C2;LX/7za;LX/D4i;LX/1AM;LX/D4G;LX/D4m;LX/1JD;LX/0Ot;LX/14w;LX/D5t;LX/D6C;LX/1Kt;LX/1LV;LX/04B;LX/0SG;Landroid/os/Handler;LX/3DC;LX/0sV;LX/0hB;LX/0hI;LX/03V;)V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 1964414
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1964415
    if-gez v2, :cond_1

    .line 1964416
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->G:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".updateInsertionMap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "insertedIndex should be non negative"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964417
    :cond_0
    return-void

    .line 1964418
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1964419
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1964420
    if-gez v1, :cond_3

    .line 1964421
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->G:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->H:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ".updateInsertionMap"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "index should be non negative"

    invoke-virtual {v0, v1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1964422
    :cond_3
    if-lt v1, v2, :cond_2

    .line 1964423
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1964424
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    add-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/04G;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1964425
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1964426
    iget v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ax:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1964427
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k()V

    .line 1964428
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-virtual {v1, v3}, LX/1Kt;->c(LX/0g8;)V

    .line 1964429
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-virtual {v1, v2, v3}, LX/1Kt;->a(ZLX/0g8;)V

    .line 1964430
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    .line 1964431
    iget-object v3, v1, LX/7za;->a:LX/1Aa;

    move-object v1, v3

    .line 1964432
    invoke-virtual {v1}, LX/1Aa;->c()V

    .line 1964433
    sget-object v4, LX/04G;->CHANNEL_PLAYER:LX/04G;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    invoke-virtual {v1}, LX/D5r;->a()I

    move-result v1

    :goto_0
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    invoke-virtual {v3}, LX/D5r;->a()I

    move-result v3

    :goto_1
    invoke-direct {p0, p1, v4, v1, v3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/04G;LX/04G;II)V

    .line 1964434
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->i:LX/D6I;

    if-eqz v1, :cond_1

    .line 1964435
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->i:LX/D6I;

    const/4 p1, 0x0

    .line 1964436
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 1964437
    instance-of v4, v3, LX/3FT;

    if-eqz v4, :cond_0

    .line 1964438
    check-cast v3, LX/3FT;

    invoke-interface {v3}, LX/3FT;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964439
    :cond_0
    iget-object v3, v1, LX/D6I;->f:LX/3FT;

    invoke-interface {v3}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v3

    iget-object v4, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    if-eq v3, v4, :cond_b

    .line 1964440
    :cond_1
    :goto_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v1, v3, :cond_2

    .line 1964441
    iget v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ay:I

    if-eqz v1, :cond_a

    .line 1964442
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ay:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1964443
    :cond_2
    :goto_3
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1964444
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->a()I

    move-result v0

    .line 1964445
    :goto_4
    new-instance v5, LX/7Ju;

    invoke-direct {v5}, LX/7Ju;-><init>()V

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    .line 1964446
    iget-boolean p1, v3, LX/D5r;->f:Z

    move v3, p1

    .line 1964447
    if-eqz v3, :cond_e

    move v3, v4

    .line 1964448
    :goto_5
    iput-boolean v3, v5, LX/7Ju;->a:Z

    .line 1964449
    move-object v3, v5

    .line 1964450
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    .line 1964451
    iget-boolean p1, v5, LX/D5r;->g:Z

    move v5, p1

    .line 1964452
    if-eqz v5, :cond_f

    .line 1964453
    :goto_6
    iput-boolean v4, v3, LX/7Ju;->b:Z

    .line 1964454
    move-object v3, v3

    .line 1964455
    iput v0, v3, LX/7Ju;->c:I

    .line 1964456
    move-object v3, v3

    .line 1964457
    iput v0, v3, LX/7Ju;->d:I

    .line 1964458
    move-object v0, v3

    .line 1964459
    iput-boolean v1, v0, LX/7Ju;->e:Z

    .line 1964460
    move-object v0, v0

    .line 1964461
    sget-object v1, LX/04g;->BY_INLINE_CHANNEL_FEED_TRANSITION:LX/04g;

    .line 1964462
    iput-object v1, v0, LX/7Ju;->h:LX/04g;

    .line 1964463
    move-object v0, v0

    .line 1964464
    invoke-virtual {v0}, LX/7Ju;->a()LX/7Jv;

    move-result-object v0

    move-object v0, v0

    .line 1964465
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aq:LX/394;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 1964466
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aq:LX/394;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v1, v3, v0}, LX/394;->a(LX/04g;LX/7Jv;)V

    .line 1964467
    :cond_3
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->B()V

    .line 1964468
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1964469
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    .line 1964470
    iput-boolean v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->av:Z

    .line 1964471
    iput-boolean v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aw:Z

    .line 1964472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aA:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1964473
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6B;

    .line 1964474
    const/4 p1, 0x0

    .line 1964475
    iput-object p1, v0, LX/D6B;->c:LX/D5X;

    .line 1964476
    iget-object v3, v0, LX/D6B;->i:LX/0g8;

    iget-object v4, v0, LX/D6B;->u:LX/D6A;

    invoke-interface {v3, v4}, LX/0g8;->c(LX/0fx;)V

    .line 1964477
    invoke-static {v0}, LX/D6B;->f(LX/D6B;)V

    .line 1964478
    iget-object v3, v0, LX/D6B;->v:LX/1Zp;

    if-eqz v3, :cond_4

    .line 1964479
    iget-object v3, v0, LX/D6B;->v:LX/1Zp;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1Zp;->cancel(Z)Z

    .line 1964480
    iput-object p1, v0, LX/D6B;->v:LX/1Zp;

    .line 1964481
    :cond_4
    goto :goto_7

    .line 1964482
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1964483
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1964484
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1964485
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1964486
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1964487
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b()V

    .line 1964488
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setTitle(Ljava/lang/String;)V

    .line 1964489
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/view/View;)V

    .line 1964490
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    invoke-interface {v0, v1}, LX/0g8;->b(Landroid/view/View;)V

    .line 1964491
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v1}, LX/0g8;->h()I

    move-result v1

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v3}, LX/0g8;->g()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v4}, LX/0g8;->i()I

    move-result v4

    invoke-interface {v0, v1, v3, v4, v6}, LX/0g8;->a(IIII)V

    .line 1964492
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964493
    iput-object v5, v0, LX/D4U;->q:LX/04D;

    .line 1964494
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964495
    iput-object v5, v0, LX/D4U;->t:LX/D6I;

    .line 1964496
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964497
    iput-boolean v6, v0, LX/D4U;->u:Z

    .line 1964498
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964499
    iput-object v5, v0, LX/D4U;->w:LX/3Qw;

    .line 1964500
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964501
    iput-object v5, v0, LX/D4U;->x:LX/3It;

    .line 1964502
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964503
    iget-object v1, v0, LX/1Qj;->r:LX/1QR;

    .line 1964504
    iget-object v0, v1, LX/1QR;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1964505
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ai:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1964506
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    .line 1964507
    iget-object v1, v0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1964508
    iget-object v1, v0, LX/D6S;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1964509
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v0, v5}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1964510
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v0, :cond_6

    .line 1964511
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 1964512
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    .line 1964513
    :cond_6
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    .line 1964514
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aq:LX/394;

    .line 1964515
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    .line 1964516
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ap:LX/D5D;

    const/4 v1, 0x0

    .line 1964517
    invoke-static {v0}, LX/D5D;->h(LX/D5D;)V

    .line 1964518
    iput-object v1, v0, LX/D5D;->c:LX/0zw;

    .line 1964519
    iput-object v1, v0, LX/D5D;->d:LX/D5J;

    .line 1964520
    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ap:LX/D5D;

    .line 1964521
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    .line 1964522
    iget-object v3, v1, LX/7za;->a:LX/1Aa;

    move-object v1, v3

    .line 1964523
    const/4 v3, 0x0

    .line 1964524
    if-eqz v1, :cond_7

    .line 1964525
    invoke-static {v0, v1}, LX/D4G;->c(LX/D4G;LX/1Aa;)V

    .line 1964526
    :cond_7
    iput-object v3, v0, LX/D4G;->j:LX/D5P;

    .line 1964527
    iput-object v3, v0, LX/D4G;->l:LX/0QK;

    .line 1964528
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    .line 1964529
    iget-object v1, v0, LX/7za;->a:LX/1Aa;

    move-object v0, v1

    .line 1964530
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->Q:LX/7zh;

    invoke-virtual {v0, v1}, LX/1Aa;->b(LX/7zh;)V

    .line 1964531
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    invoke-virtual {v0}, LX/AjP;->b()V

    .line 1964532
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1964533
    iput-object v1, v0, LX/D4m;->b:Landroid/view/Window;

    .line 1964534
    iput-object v1, v0, LX/D4m;->c:Ljava/lang/ref/WeakReference;

    .line 1964535
    iput-object v1, v0, LX/D4m;->d:Ljava/lang/ref/WeakReference;

    .line 1964536
    const/4 v1, 0x1

    iput v1, v0, LX/D4m;->f:I

    .line 1964537
    iput-boolean v3, v0, LX/D4m;->g:Z

    .line 1964538
    iput-boolean v3, v0, LX/D4m;->h:Z

    .line 1964539
    iput-boolean v3, v0, LX/D4m;->i:Z

    .line 1964540
    iput-boolean v3, v0, LX/D4m;->j:Z

    .line 1964541
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    .line 1964542
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    invoke-virtual {v1}, LX/1My;->b()V

    .line 1964543
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v0, v1}, LX/D4m;->a(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    .line 1964544
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    .line 1964545
    iput-object v1, v0, LX/D4m;->b:Landroid/view/Window;

    .line 1964546
    iput v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->az:I

    .line 1964547
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getVideoPrefetchVisitor(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)LX/1ly;

    move-result-object v0

    invoke-virtual {v0}, LX/1ly;->d()V

    .line 1964548
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getVideoPrefetchVisitor(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)LX/1ly;

    move-result-object v0

    invoke-virtual {v0}, LX/1ly;->b()V

    .line 1964549
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z()V

    .line 1964550
    iput-boolean v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->au:Z

    .line 1964551
    return-void

    :cond_8
    move v1, v2

    .line 1964552
    goto/16 :goto_0

    :cond_9
    move v3, v2

    goto/16 :goto_1

    .line 1964553
    :cond_a
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v3, 0x7f010297

    invoke-static {v0, v3, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    goto/16 :goto_3

    .line 1964554
    :cond_b
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->o()Ljava/util/List;

    .line 1964555
    iget-object v3, v1, LX/D6I;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2oy;

    .line 1964556
    iget-object v5, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964557
    invoke-static {v5, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1964558
    goto :goto_8

    .line 1964559
    :cond_c
    iget-object v3, v1, LX/D6I;->f:LX/3FT;

    iget-object v4, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-interface {v3, v4}, LX/3FT;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1964560
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, v1, LX/D6I;->j:LX/2pa;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1964561
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1964562
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, v1, LX/D6I;->f:LX/3FT;

    invoke-interface {v4}, LX/3FT;->getPlayerType()LX/04G;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1964563
    iget-object v3, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, v1, LX/D6I;->i:LX/3It;

    .line 1964564
    iput-object v4, v3, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1964565
    const/4 v3, 0x0

    iput-boolean v3, v1, LX/D6I;->l:Z

    .line 1964566
    iput-object p1, v1, LX/D6I;->f:LX/3FT;

    .line 1964567
    iput-object p1, v1, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964568
    iput-object p1, v1, LX/D6I;->j:LX/2pa;

    .line 1964569
    iput-object p1, v1, LX/D6I;->h:Ljava/util/List;

    .line 1964570
    iput-object p1, v1, LX/D6I;->i:LX/3It;

    .line 1964571
    iput-object p1, v1, LX/D6I;->k:Ljava/lang/String;

    goto/16 :goto_2

    :cond_d
    move v0, v1

    .line 1964572
    goto/16 :goto_4

    :cond_e
    move v3, v1

    .line 1964573
    goto/16 :goto_5

    :cond_f
    move v4, v1

    goto/16 :goto_6
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1964574
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1964575
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v3

    .line 1964576
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1964577
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1964578
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 1964579
    const-string v0, ""

    .line 1964580
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1964581
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1964582
    :cond_2
    move-object v6, v0

    .line 1964583
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1964584
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1964585
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1964586
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1964587
    :goto_1
    if-lez v0, :cond_8

    if-eqz p3, :cond_8

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    move v4, v1

    .line 1964588
    :goto_2
    if-eqz v4, :cond_4

    .line 1964589
    new-instance v7, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;

    invoke-direct {v7, v6}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;-><init>(Ljava/lang/String;)V

    .line 1964590
    iget-object v8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-static {v7}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    invoke-virtual {v8, v0, v7}, LX/D6S;->a(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1964591
    add-int/lit8 v0, v0, 0x1

    .line 1964592
    :cond_4
    invoke-direct {p0, v0, v3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(ILcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)Ljava/util/List;

    move-result-object v7

    .line 1964593
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v8, v0

    .line 1964594
    invoke-virtual {v3}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v3}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    move v3, v1

    .line 1964595
    :goto_3
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    iget-object v9, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1964596
    if-eqz v4, :cond_a

    if-eqz v3, :cond_a

    if-nez v0, :cond_a

    move v3, v1

    .line 1964597
    :goto_4
    if-eqz v3, :cond_5

    .line 1964598
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    invoke-direct {v0, v6, v5}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964599
    iget-object v9, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v9, v8, v0}, LX/D6S;->a(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1964600
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    if-eqz v4, :cond_b

    move v0, v1

    :goto_5
    add-int v4, v6, v0

    if-eqz v3, :cond_c

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    invoke-direct {p0, v5, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Ljava/lang/String;I)V

    .line 1964601
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v0, :cond_6

    .line 1964602
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1964603
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1964604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1964605
    :cond_7
    :try_start_2
    invoke-static {p0, v5}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;)I

    move-result v0

    .line 1964606
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_8
    move v4, v2

    .line 1964607
    goto/16 :goto_2

    :cond_9
    move v3, v2

    .line 1964608
    goto :goto_3

    :cond_a
    move v3, v2

    .line 1964609
    goto :goto_4

    :cond_b
    move v0, v2

    .line 1964610
    goto :goto_5

    :cond_c
    move v0, v2

    goto :goto_6
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V
    .locals 2

    .prologue
    .line 1964611
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1964612
    :cond_0
    :goto_0
    return-void

    .line 1964613
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(ILcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)Ljava/util/List;

    move-result-object v0

    .line 1964614
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Ljava/lang/String;I)V

    .line 1964615
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v1, :cond_2

    .line 1964616
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 1964617
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    goto :goto_0
.end method

.method private getContextThemeWrapper()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1964238
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->am:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1964239
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0748

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->am:Landroid/content/Context;

    .line 1964240
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->am:Landroid/content/Context;

    return-object v0
.end method

.method private getTrackingCodes()LX/0lF;
    .locals 2

    .prologue
    .line 1964618
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v0, v0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 1964619
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1964620
    :goto_0
    return-object v0

    .line 1964621
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v0, v0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1964622
    if-eqz v0, :cond_1

    .line 1964623
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 1964624
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v0, v0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public static getVideoPrefetchVisitor(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)LX/1ly;
    .locals 3

    .prologue
    .line 1964625
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ar:LX/1ly;

    if-nez v0, :cond_0

    .line 1964626
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->s:LX/1JD;

    sget-object v1, LX/1Li;->CHANNEL:LX/1Li;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ar:LX/1ly;

    .line 1964627
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ar:LX/1ly;

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1964628
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 1964629
    :goto_0
    return-void

    .line 1964630
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->az:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 14

    .prologue
    .line 1964241
    sget-object v0, LX/D5b;->a:[I

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    invoke-virtual {v1}, LX/D5l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1964242
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964243
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v3, v2

    .line 1964244
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->w:LX/D6C;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-boolean v6, v6, LX/3Qw;->j:Z

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    new-instance v9, LX/D5Z;

    invoke-direct {v9, p0}, LX/D5Z;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iget-object v10, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    invoke-virtual/range {v2 .. v10}, LX/D6C;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0g8;LX/1Qq;LX/D5X;LX/D59;)LX/D6B;

    move-result-object v2

    .line 1964245
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, LX/D6B;->a(Z)V

    .line 1964246
    invoke-virtual {v2}, LX/D6B;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1964247
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1964248
    :cond_0
    if-eqz v3, :cond_5

    .line 1964249
    :goto_1
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964250
    :goto_2
    return-void

    .line 1964251
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964252
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->c:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v3, v2

    .line 1964253
    :goto_3
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->w:LX/D6C;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-boolean v6, v6, LX/3Qw;->j:Z

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    new-instance v9, LX/D5a;

    invoke-direct {v9, p0}, LX/D5a;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iget-object v10, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    invoke-virtual/range {v2 .. v10}, LX/D6C;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0g8;LX/1Qq;LX/D5X;LX/D59;)LX/D6B;

    move-result-object v2

    .line 1964254
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964255
    goto :goto_2

    .line 1964256
    :pswitch_1
    const/4 v13, 0x1

    .line 1964257
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964258
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1964259
    iput-boolean v13, v2, LX/D4U;->u:Z

    .line 1964260
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Ljava/lang/String;

    .line 1964261
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1964262
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-static {p0, v3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964263
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->w:LX/D6C;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-boolean v6, v6, LX/3Qw;->j:Z

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    new-instance v9, LX/D5Y;

    invoke-direct {v9, p0}, LX/D5Y;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    iget-object v10, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ad:LX/D59;

    invoke-virtual/range {v2 .. v10}, LX/D6C;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0g8;LX/1Qq;LX/D5X;LX/D59;)LX/D6B;

    move-result-object v2

    .line 1964264
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1964265
    invoke-virtual {v2, v13}, LX/D6B;->a(Z)V

    .line 1964266
    :cond_1
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v4, v4, LX/3Qw;->b:Ljava/util/List;

    if-nez v4, :cond_2

    invoke-virtual {v2}, LX/D6B;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1964267
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1964268
    :cond_2
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->W:Ljava/util/Map;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1964269
    :cond_3
    goto/16 :goto_2

    .line 1964270
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1964271
    :cond_5
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    goto/16 :goto_1

    .line 1964272
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static t(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1964631
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v0}, LX/D6S;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1964632
    :goto_0
    if-eqz v0, :cond_2

    .line 1964633
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1964634
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1964635
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v2}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    :cond_0
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1964636
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1964637
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1964638
    goto :goto_0

    .line 1964639
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->S:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1963901
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1963902
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aa:Ljava/util/Map;

    invoke-static {p0, v0, v1}, LX/7QU;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V

    .line 1963903
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1963904
    :cond_0
    return-void
.end method

.method private z()V
    .locals 2

    .prologue
    .line 1963905
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_0

    .line 1963906
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aa:Ljava/util/Map;

    invoke-static {p0, v0, v1}, LX/7QU;->b(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V

    .line 1963907
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1963908
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/394;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1963909
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aq:LX/394;

    .line 1963910
    return-object p0
.end method

.method public final a(LX/395;)V
    .locals 0

    .prologue
    .line 1963911
    return-void
.end method

.method public final a(LX/3Qw;)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    .line 1963912
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->au:Z

    if-eqz v0, :cond_0

    .line 1963913
    :goto_0
    return-void

    .line 1963914
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    if-nez v0, :cond_1b

    .line 1963915
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    if-eqz v0, :cond_1

    .line 1963916
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    invoke-static {v0}, LX/470;->b(Landroid/view/Window;)V

    .line 1963917
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1963918
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ax:I

    .line 1963919
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1963920
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 1963921
    iput v3, v1, LX/D4m;->f:I

    .line 1963922
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963923
    iget-object v1, p1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v1, :cond_2

    iget-object v1, p1, LX/3Qw;->c:Ljava/util/List;

    if-eqz v1, :cond_4

    :cond_2
    move v1, v2

    :goto_2
    const-string v3, "Either story or videoChannelIds must be provided"

    invoke-static {v1, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1963924
    iget-object v1, p1, LX/3Qw;->h:LX/04g;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963925
    iget-object v1, p1, LX/3Qw;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963926
    iget-object v1, p1, LX/3Qw;->g:LX/04D;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963927
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    .line 1963928
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963929
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-static {v1}, LX/3ha;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1963930
    sget-object v1, LX/D5l;->MULTISHARE:LX/D5l;

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    .line 1963931
    :goto_3
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    iget-boolean v1, v1, LX/0sV;->u:Z

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    sget-object v3, LX/D5l;->RELATED_VIDEOS:LX/D5l;

    if-ne v1, v3, :cond_1e

    const/4 v1, 0x1

    .line 1963932
    :goto_4
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    .line 1963933
    iput-boolean v1, v3, LX/D4i;->i:Z

    .line 1963934
    iget-object v1, p1, LX/3Qw;->c:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 1963935
    iget-object v1, p1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1963936
    if-eqz v1, :cond_3

    .line 1963937
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ab:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1963938
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 1963939
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->g:LX/04D;

    .line 1963940
    iput-object v3, v1, LX/D4U;->q:LX/04D;

    .line 1963941
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->i:LX/D6I;

    .line 1963942
    iput-object v3, v1, LX/D4U;->t:LX/D6I;

    .line 1963943
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    .line 1963944
    iput-object v3, v1, LX/D4U;->w:LX/3Qw;

    .line 1963945
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ah:LX/3It;

    .line 1963946
    iput-object v3, v1, LX/D4U;->x:LX/3It;

    .line 1963947
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->b:LX/1DS;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->c:LX/0Ot;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v1, v3, v4}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    .line 1963948
    iput-object v3, v1, LX/1Ql;->f:LX/1PW;

    .line 1963949
    move-object v1, v1

    .line 1963950
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->h:LX/6Vd;

    .line 1963951
    iput-object v3, v1, LX/1Ql;->e:LX/1DZ;

    .line 1963952
    move-object v1, v1

    .line 1963953
    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    .line 1963954
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v1, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1963955
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_6

    .line 1963956
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1963957
    iget-object v1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1963958
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963959
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 1963960
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1963961
    if-eqz v4, :cond_6

    .line 1963962
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->g:LX/D5q;

    invoke-virtual {v6, v3, v4}, LX/D5q;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/D5p;

    move-result-object v3

    invoke-virtual {v5, v3, v1}, LX/1Qj;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D5r;

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    .line 1963963
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget v3, v3, LX/3Qw;->e:I

    invoke-virtual {v1, v3}, LX/D5r;->a(I)V

    .line 1963964
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    invoke-virtual {v1}, LX/D5r;->b()LX/2oO;

    move-result-object v1

    .line 1963965
    iput-boolean v2, v1, LX/2oO;->y:Z

    .line 1963966
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->at:LX/D5r;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->h:LX/04g;

    invoke-virtual {v1, v3}, LX/D5r;->a(LX/04g;)V

    .line 1963967
    :cond_6
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_9

    .line 1963968
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963969
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1963970
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963971
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    invoke-virtual {v3, v1}, LX/D5t;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    .line 1963972
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->b:Ljava/util/List;

    if-nez v3, :cond_9

    .line 1963973
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    const/4 v4, 0x0

    .line 1963974
    invoke-static {v3, v1}, LX/D5t;->e(LX/D5t;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1963975
    if-nez v5, :cond_1f

    .line 1963976
    :cond_7
    :goto_6
    move v3, v4

    .line 1963977
    iput-boolean v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->av:Z

    .line 1963978
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    const/4 v4, 0x0

    .line 1963979
    invoke-static {v3, v1}, LX/D5t;->e(LX/D5t;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1963980
    if-nez v5, :cond_20

    .line 1963981
    :cond_8
    :goto_7
    move v3, v4

    .line 1963982
    iput-boolean v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aw:Z

    .line 1963983
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    .line 1963984
    invoke-static {v3, v1}, LX/D5t;->e(LX/D5t;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1963985
    if-nez v4, :cond_21

    .line 1963986
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1963987
    :goto_8
    move-object v1, v4

    .line 1963988
    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aA:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1963989
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963990
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    .line 1963991
    iget-object v5, v3, LX/D6S;->d:LX/D5f;

    .line 1963992
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1963993
    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v5, v4}, LX/D5f;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1963994
    iget-object v4, v3, LX/D6S;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1963995
    invoke-static {v3, v1}, LX/D6S;->c(LX/D6S;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1963996
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v3}, LX/1Cw;->notifyDataSetChanged()V

    .line 1963997
    :cond_9
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    if-eqz v1, :cond_1a

    .line 1963998
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1963999
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1964000
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    .line 1964001
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-static {v1}, LX/3ha;->a(Ljava/util/List;)Z

    move-result v10

    .line 1964002
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    sget-object v3, LX/D5l;->SINGLE_PUBLISHER:LX/D5l;

    if-ne v1, v3, :cond_c

    move v3, v4

    :goto_9
    move v7, v5

    .line 1964003
    :goto_a
    if-ge v7, v9, :cond_d

    .line 1964004
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964005
    iget-object v11, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v11

    .line 1964006
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1964007
    iget-object v11, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    invoke-virtual {v11, v1}, LX/D5t;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v11

    .line 1964008
    if-eqz v11, :cond_b

    .line 1964009
    if-eqz v10, :cond_a

    .line 1964010
    if-nez v7, :cond_27

    .line 1964011
    sget-object v12, LX/D6N;->FIRST:LX/D6N;

    .line 1964012
    :goto_b
    new-instance p1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    invoke-direct {p1, v1, v12}, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/D6N;)V

    move-object v1, p1

    .line 1964013
    :cond_a
    iget-object v12, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v12, v1}, LX/D6S;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1964014
    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1964015
    :cond_b
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_a

    :cond_c
    move v3, v5

    .line 1964016
    goto :goto_9

    .line 1964017
    :cond_d
    if-eqz v10, :cond_24

    .line 1964018
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    const/4 v7, 0x0

    .line 1964019
    if-eqz v1, :cond_e

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_29

    :cond_e
    move-object v3, v7

    .line 1964020
    :goto_c
    move-object v3, v3

    .line 1964021
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_f

    .line 1964022
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    .line 1964023
    :cond_f
    if-eqz v3, :cond_10

    if-eqz v6, :cond_10

    .line 1964024
    new-instance v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    invoke-direct {v1, v3, v6, v4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1964025
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/D6S;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1964026
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964027
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964028
    :cond_10
    :goto_d
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1964029
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-nez v1, :cond_2c

    .line 1964030
    :cond_11
    :goto_e
    move v1, v4

    .line 1964031
    if-eqz v1, :cond_25

    .line 1964032
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z:LX/04B;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v1

    .line 1964033
    if-eqz v8, :cond_12

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 1964034
    :cond_12
    :goto_f
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v1, :cond_13

    .line 1964035
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 1964036
    :cond_13
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget v1, v1, LX/3Qw;->n:I

    .line 1964037
    if-ltz v1, :cond_14

    .line 1964038
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-nez v3, :cond_32

    .line 1964039
    :cond_14
    :goto_10
    const/4 v7, 0x0

    .line 1964040
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->k:LX/D4s;

    .line 1964041
    if-eqz v1, :cond_35

    .line 1964042
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v3, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a(LX/D4s;)V

    .line 1964043
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    .line 1964044
    iget-object v4, v1, LX/D4s;->b:Ljava/lang/String;

    move-object v1, v4

    .line 1964045
    iput-object v1, v3, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->h:Ljava/lang/String;

    .line 1964046
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-interface {v1, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 1964047
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v3}, LX/0g8;->h()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v4}, LX/0g8;->i()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v5}, LX/0g8;->e()I

    move-result v5

    invoke-interface {v1, v3, v7, v4, v5}, LX/0g8;->a(IIII)V

    .line 1964048
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setVisibility(I)V

    .line 1964049
    :goto_11
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->N:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->g:LX/04D;

    .line 1964050
    iput-object v3, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    .line 1964051
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->l:LX/D5E;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->O:LX/0zw;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->P:LX/D5J;

    .line 1964052
    new-instance v7, LX/D5D;

    invoke-static {v1}, LX/7za;->a(LX/0QB;)LX/7za;

    move-result-object v10

    check-cast v10, LX/7za;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v11

    check-cast v11, LX/0wW;

    invoke-static {v1}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v12

    check-cast v12, LX/4mV;

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v7 .. v12}, LX/D5D;-><init>(LX/0zw;LX/D5J;LX/7za;LX/0wW;LX/4mV;)V

    .line 1964053
    move-object v1, v7

    .line 1964054
    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ap:LX/D5D;

    .line 1964055
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    .line 1964056
    iget-object v3, v1, LX/7za;->a:LX/1Aa;

    move-object v1, v3

    .line 1964057
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->Q:LX/7zh;

    invoke-virtual {v1, v3}, LX/1Aa;->a(LX/7zh;)V

    .line 1964058
    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget v4, v4, LX/3Qw;->e:I

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget v5, v5, LX/3Qw;->f:I

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/04G;LX/04G;II)V

    .line 1964059
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v1, v3, :cond_15

    .line 1964060
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getStatusBarColor()I

    move-result v1

    iput v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ay:I

    .line 1964061
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v3, 0x7f0a00ca

    invoke-static {v0, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1964062
    :cond_15
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->y()V

    .line 1964063
    iput-boolean v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->au:Z

    .line 1964064
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->n:LX/7za;

    .line 1964065
    iget-object v2, v1, LX/7za;->a:LX/1Aa;

    move-object v1, v2

    .line 1964066
    iget-object v2, v0, LX/D4G;->f:LX/7zh;

    invoke-virtual {v1, v2}, LX/1Aa;->a(LX/7zh;)V

    .line 1964067
    iget-object v2, v0, LX/D4G;->g:LX/D4B;

    .line 1964068
    iget-object v0, v1, LX/1Aa;->q:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1964069
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    new-instance v1, LX/D5P;

    invoke-direct {v1, p0}, LX/D5P;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964070
    iput-object v1, v0, LX/D4G;->j:LX/D5P;

    .line 1964071
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    new-instance v1, LX/D5Q;

    invoke-direct {v1, p0}, LX/D5Q;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964072
    iput-object v1, v0, LX/D4G;->l:LX/0QK;

    .line 1964073
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->A()V

    .line 1964074
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getVideoPrefetchVisitor(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)LX/1ly;

    move-result-object v0

    invoke-virtual {v0}, LX/1ly;->a()V

    .line 1964075
    const/4 v10, 0x0

    .line 1964076
    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964077
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v8

    .line 1964078
    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->b:Ljava/util/List;

    if-nez v7, :cond_37

    .line 1964079
    :cond_16
    :goto_12
    move v0, v10

    .line 1964080
    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->v:Z

    if-eqz v0, :cond_17

    .line 1964081
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-nez v0, :cond_39

    .line 1964082
    :cond_17
    :goto_13
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    const-string v1, "Fetching RichVideoPlayerParams without ChannelFeedParams"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964083
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v0, v0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964084
    if-nez v0, :cond_18

    .line 1964085
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3a

    .line 1964086
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v0, v0, LX/3Qw;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964087
    :cond_18
    const-string v1, "Transitioning to channel feed without a story"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964088
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->v:LX/D5t;

    invoke-virtual {v1, v0}, LX/D5t;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;

    move-result-object v0

    .line 1964089
    :goto_14
    move-object v1, v0

    .line 1964090
    if-eqz v1, :cond_19

    .line 1964091
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37Y;

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v2, v2, LX/3Qw;->g:LX/04D;

    invoke-static {v1, v2}, LX/7JO;->a(LX/2pa;LX/04D;)LX/7JN;

    move-result-object v2

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v2, v1}, LX/37Y;->a(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)Z

    .line 1964092
    :cond_19
    goto/16 :goto_0

    .line 1964093
    :cond_1a
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    goto/16 :goto_10

    .line 1964094
    :cond_1b
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->az:I

    goto/16 :goto_1

    .line 1964095
    :cond_1c
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->k:LX/D4s;

    if-eqz v1, :cond_1d

    .line 1964096
    sget-object v1, LX/D5l;->SINGLE_PUBLISHER:LX/D5l;

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    goto/16 :goto_3

    .line 1964097
    :cond_1d
    sget-object v1, LX/D5l;->RELATED_VIDEOS:LX/D5l;

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aB:LX/D5l;

    goto/16 :goto_3

    .line 1964098
    :cond_1e
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 1964099
    :cond_1f
    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1964100
    if-eqz v5, :cond_7

    .line 1964101
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-static {v5}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v5, v6, :cond_7

    const/4 v4, 0x1

    goto/16 :goto_6

    .line 1964102
    :cond_20
    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1964103
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 1964104
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v4

    goto/16 :goto_7

    .line 1964105
    :cond_21
    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 1964106
    if-eqz v4, :cond_22

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-nez v5, :cond_23

    .line 1964107
    :cond_22
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto/16 :goto_8

    .line 1964108
    :cond_23
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v4

    goto/16 :goto_8

    .line 1964109
    :cond_24
    if-eqz v3, :cond_10

    .line 1964110
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_26

    .line 1964111
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1964112
    :goto_15
    if-eqz v1, :cond_10

    .line 1964113
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ac:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964114
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->p(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964115
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    .line 1964116
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    goto/16 :goto_d

    .line 1964117
    :cond_25
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1964118
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z:LX/04B;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->as:Ljava/lang/String;

    invoke-virtual {v1, v3, v8}, LX/04B;->a(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_f

    :cond_26
    move-object v1, v6

    goto :goto_15

    .line 1964119
    :cond_27
    add-int/lit8 v12, v9, -0x1

    if-ne v7, v12, :cond_28

    .line 1964120
    sget-object v12, LX/D6N;->LAST:LX/D6N;

    goto/16 :goto_b

    .line 1964121
    :cond_28
    sget-object v12, LX/D6N;->NORMAL:LX/D6N;

    goto/16 :goto_b

    .line 1964122
    :cond_29
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2a
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964123
    iget-object v10, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v10

    .line 1964124
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 1964125
    if-eqz v3, :cond_2a

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2a

    .line 1964126
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_c

    :cond_2b
    move-object v3, v7

    .line 1964127
    goto/16 :goto_c

    .line 1964128
    :cond_2c
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->g:LX/04D;

    if-eqz v1, :cond_2d

    sget-object v1, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->I:Ljava/util/Set;

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->g:LX/04D;

    iget-object v5, v5, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    move v1, v3

    .line 1964129
    :goto_16
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v5, :cond_2e

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    if-eqz v5, :cond_2e

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v5, v5, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    instance-of v5, v5, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v5, :cond_2e

    move v5, v3

    .line 1964130
    :goto_17
    if-eqz v1, :cond_11

    if-eqz v5, :cond_11

    move v4, v3

    goto/16 :goto_e

    :cond_2d
    move v1, v4

    .line 1964131
    goto :goto_16

    :cond_2e
    move v5, v4

    .line 1964132
    goto :goto_17

    .line 1964133
    :cond_2f
    iget-object v4, v3, LX/04B;->j:Ljava/lang/String;

    if-eqz v4, :cond_30

    iget-object v4, v3, LX/04B;->j:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_31

    .line 1964134
    :cond_30
    invoke-static {v3, v1}, LX/04B;->b(LX/04B;Ljava/lang/String;)V

    .line 1964135
    :cond_31
    iget-object v4, v3, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 1964136
    const/4 v4, 0x0

    :goto_18
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_12

    .line 1964137
    iget-object v5, v3, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964138
    add-int/lit8 v4, v4, 0x1

    goto :goto_18

    .line 1964139
    :cond_32
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v4, v1}, LX/1Qr;->m_(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0g8;->d(II)V

    .line 1964140
    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v3, v1}, LX/D6S;->b(I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1964141
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 1964142
    check-cast v3, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1964143
    const/4 v4, 0x0

    .line 1964144
    iget-object v5, v3, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1964145
    if-eqz v5, :cond_33

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-nez v1, :cond_34

    .line 1964146
    :cond_33
    :goto_19
    move-object v3, v4

    .line 1964147
    new-instance v4, LX/D5U;

    invoke-direct {v4, p0, v3}, LX/D5U;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;)V

    .line 1964148
    new-instance v3, LX/D5V;

    invoke-direct {v3, p0}, LX/D5V;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964149
    invoke-static {p0, v4, v3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/0QK;LX/0QK;)V

    goto/16 :goto_10

    .line 1964150
    :cond_34
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-static {v5}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v5

    .line 1964151
    if-eqz v5, :cond_33

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    goto :goto_19

    .line 1964152
    :cond_35
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v3}, LX/0g8;->h()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b10fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v5}, LX/0g8;->i()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v6}, LX/0g8;->e()I

    move-result v6

    invoke-interface {v1, v3, v4, v5, v6}, LX/0g8;->a(IIII)V

    .line 1964153
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_36

    .line 1964154
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v3, v3, LX/3Qw;->p:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setTitle(Ljava/lang/String;)V

    .line 1964155
    :cond_36
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-virtual {v1, v7}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setVisibility(I)V

    goto/16 :goto_11

    :cond_37
    move v9, v10

    .line 1964156
    :goto_1a
    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v7}, LX/D6S;->size()I

    move-result v7

    if-ge v9, v7, :cond_38

    .line 1964157
    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v7, v9}, LX/D6S;->b(I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 1964158
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v8

    .line 1964159
    check-cast v7, Lcom/facebook/graphql/model/FeedUnit;

    .line 1964160
    iget-object v8, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v8, v8, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964161
    iget-object v11, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v11

    .line 1964162
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v8

    .line 1964163
    if-eqz v8, :cond_38

    invoke-interface {v7}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_38

    .line 1964164
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_1a

    .line 1964165
    :cond_38
    if-eqz v9, :cond_16

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v7}, LX/D6S;->size()I

    move-result v7

    if-eq v9, v7, :cond_16

    .line 1964166
    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->B:Landroid/os/Handler;

    new-instance v8, Lcom/facebook/video/channelfeed/ChannelFeedRootView$23;

    invoke-direct {v8, p0, v9}, Lcom/facebook/video/channelfeed/ChannelFeedRootView$23;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;I)V

    const-wide/16 v9, 0x64

    const v11, 0x6c82e4f1

    invoke-static {v7, v8, v9, v10, v11}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1964167
    const/4 v10, 0x1

    goto/16 :goto_12

    .line 1964168
    :cond_39
    new-instance v0, LX/D5R;

    invoke-direct {v0, p0}, LX/D5R;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964169
    new-instance v1, LX/D5S;

    invoke-direct {v1, p0}, LX/D5S;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1964170
    invoke-static {p0, v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/0QK;LX/0QK;)V

    goto/16 :goto_13

    .line 1964171
    :cond_3a
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    .line 1964172
    const/4 v0, 0x0

    goto/16 :goto_14
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1964173
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->au:Z

    return v0
.end method

.method public final b()Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1964174
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1964175
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->G:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".handleBackPressed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "the rootview is not visible"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964176
    const/4 v0, 0x0

    .line 1964177
    :goto_0
    return v0

    .line 1964178
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    const/4 v3, 0x0

    .line 1964179
    iget-object v4, v0, LX/D4m;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    iput-wide v5, v0, LX/D4m;->k:J

    .line 1964180
    iget-boolean v4, v0, LX/D4m;->g:Z

    if-eqz v4, :cond_1

    .line 1964181
    invoke-static {v0}, LX/D4m;->k(LX/D4m;)V

    .line 1964182
    iput-boolean v3, v0, LX/D4m;->h:Z

    .line 1964183
    const/4 v3, 0x1

    .line 1964184
    :cond_1
    move v0, v3

    .line 1964185
    if-eqz v0, :cond_2

    .line 1964186
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1964187
    iget v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ax:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1964188
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k()V

    move v0, v1

    .line 1964189
    goto :goto_0

    .line 1964190
    :cond_2
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-static {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/04G;)V

    move v0, v1

    .line 1964191
    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1964192
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->y()V

    .line 1964193
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1964194
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->A()V

    .line 1964195
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->al:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 1964196
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->al:Landroid/os/Parcelable;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/os/Parcelable;)V

    .line 1964197
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1964198
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->B()V

    .line 1964199
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    .line 1964200
    iget-object v1, v0, LX/D4G;->a:LX/D4D;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/D4D;->removeMessages(I)V

    .line 1964201
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v0}, LX/0g8;->A()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->al:Landroid/os/Parcelable;

    .line 1964202
    return-void
.end method

.method public getFullScreenListener()LX/394;
    .locals 1

    .prologue
    .line 1964203
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aq:LX/394;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1964204
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->z()V

    .line 1964205
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1963900
    const/4 v0, 0x0

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1964206
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v0, :cond_0

    .line 1964207
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 1964208
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v0, v1, v1}, LX/0g8;->a(II)V

    .line 1964209
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 1964210
    iget v2, v0, LX/D4m;->f:I

    if-ne v2, v1, :cond_2

    .line 1964211
    :cond_1
    :goto_0
    return-void

    .line 1964212
    :cond_2
    iput v1, v0, LX/D4m;->f:I

    .line 1964213
    iget-boolean v2, v0, LX/D4m;->g:Z

    if-eqz v2, :cond_3

    iget-boolean v2, v0, LX/D4m;->h:Z

    if-nez v2, :cond_1

    .line 1964214
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 1964215
    iget-wide v3, v0, LX/D4m;->k:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    iget-object v3, v0, LX/D4m;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, v0, LX/D4m;->k:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x3e8

    cmp-long v3, v3, v5

    if-lez v3, :cond_7

    :cond_4
    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 1964216
    if-eqz v2, :cond_1

    .line 1964217
    invoke-static {v0}, LX/D4m;->j(LX/D4m;)V

    goto :goto_0

    .line 1964218
    :cond_5
    iget-boolean v2, v0, LX/D4m;->g:Z

    if-eqz v2, :cond_1

    .line 1964219
    invoke-static {v0}, LX/D4m;->m(LX/D4m;)LX/D5z;

    move-result-object v2

    .line 1964220
    if-eqz v2, :cond_6

    .line 1964221
    iget-object v3, v2, LX/D5z;->q:LX/2pa;

    move-object v3, v3

    .line 1964222
    if-nez v3, :cond_9

    .line 1964223
    :cond_6
    const/4 v2, 0x0

    .line 1964224
    :goto_2
    move-object v2, v2

    .line 1964225
    if-nez v2, :cond_8

    .line 1964226
    const/4 v2, 0x0

    .line 1964227
    :goto_3
    move v2, v2

    .line 1964228
    if-nez v2, :cond_1

    .line 1964229
    invoke-static {v0}, LX/D4m;->k(LX/D4m;)V

    goto :goto_0

    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    iget-boolean v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    goto :goto_3

    .line 1964230
    :cond_9
    iget-object v3, v2, LX/D5z;->q:LX/2pa;

    move-object v2, v3

    .line 1964231
    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    goto :goto_2
.end method

.method public setAllowLooping(Z)V
    .locals 2

    .prologue
    .line 1964232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Looping is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLogEnteringStartEvent(Z)V
    .locals 0

    .prologue
    .line 1964233
    return-void
.end method

.method public setLogExitingPauseEvent(Z)V
    .locals 0

    .prologue
    .line 1964234
    return-void
.end method

.method public setNextStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1964235
    return-void
.end method

.method public setPreviousStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1964236
    return-void
.end method

.method public final t_(I)V
    .locals 0

    .prologue
    .line 1964237
    return-void
.end method
