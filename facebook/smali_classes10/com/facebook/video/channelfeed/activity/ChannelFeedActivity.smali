.class public Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0iL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1965858
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1965859
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1965860
    iput-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->r:LX/0Ot;

    .line 1965861
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1965862
    iput-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->s:LX/0Ot;

    return-void
.end method

.method private a()LX/D4s;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965845
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "headerProfilePicUri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1965846
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "headerTitle"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1965847
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "headerSubtitle"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1965848
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1965849
    :cond_0
    const/4 v0, 0x0

    .line 1965850
    :goto_0
    return-object v0

    :cond_1
    new-instance v3, LX/D4r;

    invoke-direct {v3}, LX/D4r;-><init>()V

    .line 1965851
    iput-object v1, v3, LX/D4r;->c:Ljava/lang/String;

    .line 1965852
    move-object v1, v3

    .line 1965853
    iput-object v2, v1, LX/D4r;->d:Ljava/lang/String;

    .line 1965854
    move-object v1, v1

    .line 1965855
    iput-object v0, v1, LX/D4r;->f:Ljava/lang/String;

    .line 1965856
    move-object v0, v1

    .line 1965857
    invoke-virtual {v0}, LX/D4r;->a()LX/D4s;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/04D;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I",
            "LX/04D;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1965838
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1965839
    const-string v1, "storyProps"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1965840
    const-string v1, "disableCache"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1965841
    const-string v1, "seekTime"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1965842
    const-string v1, "playerOrigin"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1965843
    const-string v1, "fromWatchAndGo"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1965844
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1965837
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1965836
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1965829
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1965830
    const-string v1, "videoChannelId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1965831
    const-string v1, "disableCache"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1965832
    const-string v1, "headerTitle"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1965833
    const-string v1, "headerSubtitle"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1965834
    const-string v1, "headerProfilePicUri"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1965835
    return-object v0
.end method

.method private static a(Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;LX/0iL;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;",
            "LX/0iL;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1965828
    iput-object p1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->p:LX/0iL;

    iput-object p2, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->r:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->s:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;

    invoke-static {v1}, LX/0iL;->a(LX/0QB;)LX/0iL;

    move-result-object v0

    check-cast v0, LX/0iL;

    const/16 v2, 0xc49

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {v1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;LX/0iL;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1965796
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1965797
    invoke-static {p0, p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1965798
    iget-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->p:LX/0iL;

    invoke-virtual {v0, p0}, LX/0iL;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1965799
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "playerOrigin"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 1965800
    instance-of v1, v0, LX/04D;

    if-eqz v1, :cond_1

    check-cast v0, LX/04D;

    move-object v1, v0

    .line 1965801
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "storyProps"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965802
    new-instance v2, LX/3Qv;

    invoke-direct {v2}, LX/3Qv;-><init>()V

    invoke-direct {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a()LX/D4s;

    move-result-object v3

    .line 1965803
    iput-object v3, v2, LX/3Qv;->k:LX/D4s;

    .line 1965804
    move-object v2, v2

    .line 1965805
    iput-object v0, v2, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965806
    move-object v0, v2

    .line 1965807
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "videoChannelId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v0

    const-string v2, "UNKNOWN"

    .line 1965808
    iput-object v2, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 1965809
    move-object v0, v0

    .line 1965810
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 1965811
    move-object v0, v0

    .line 1965812
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 1965813
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 1965814
    move-object v0, v0

    .line 1965815
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "disableCache"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1965816
    iput-boolean v1, v0, LX/3Qv;->j:Z

    .line 1965817
    move-object v0, v0

    .line 1965818
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "seekTime"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1965819
    iput v1, v0, LX/3Qv;->e:I

    .line 1965820
    move-object v0, v0

    .line 1965821
    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v0

    .line 1965822
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/3Qw;)V

    .line 1965823
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fromWatchAndGo"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965824
    iget-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->je:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1965825
    iget-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 1965826
    :cond_0
    return-void

    .line 1965827
    :cond_1
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1965791
    iget-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->b()Z

    .line 1965792
    iget-object v0, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965793
    :goto_0
    return-void

    .line 1965794
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1965795
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->finish()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x72d58fdf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1965785
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1965786
    iput-object v2, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1965787
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->p:LX/0iL;

    if-eqz v1, :cond_0

    .line 1965788
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->p:LX/0iL;

    invoke-virtual {v1}, LX/0iL;->a()V

    .line 1965789
    iput-object v2, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->p:LX/0iL;

    .line 1965790
    :cond_0
    const/16 v1, 0x23

    const v2, 0x3d20f6fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x25906a95

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1965782
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1965783
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->g()V

    .line 1965784
    const/16 v1, 0x23

    const v2, -0x3d15b52d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2c4ed088

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1965779
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1965780
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->f()V

    .line 1965781
    const/16 v1, 0x23

    const v2, 0x204c4cdb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3da29f51

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1965776
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1965777
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->e()V

    .line 1965778
    const/16 v1, 0x23

    const v2, 0x76066276

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x6c5fa5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1965773
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1965774
    iget-object v1, p0, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->q:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->h()V

    .line 1965775
    const/16 v1, 0x23

    const v2, -0x30463530

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
