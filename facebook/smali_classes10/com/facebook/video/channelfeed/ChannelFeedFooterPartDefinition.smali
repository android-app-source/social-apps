.class public Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1We;

.field private final f:LX/1We;

.field private final g:LX/14w;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/1Wg;LX/1Wg;Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;LX/14w;)V
    .locals 1
    .param p4    # LX/1Wg;
        .annotation runtime Lcom/facebook/video/channelfeed/ForChannelFeed;
        .end annotation
    .end param
    .param p5    # LX/1Wg;
        .annotation runtime Lcom/facebook/video/channelfeed/ForMultiShareChannelFeed;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962576
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1962577
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->a:Landroid/content/res/Resources;

    .line 1962578
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->b:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    .line 1962579
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 1962580
    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    .line 1962581
    new-instance v0, LX/1We;

    invoke-direct {v0, p4}, LX/1We;-><init>(LX/1Wg;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->e:LX/1We;

    .line 1962582
    new-instance v0, LX/1We;

    invoke-direct {v0, p5}, LX/1We;-><init>(LX/1Wg;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->f:LX/1We;

    .line 1962583
    iput-object p7, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->g:LX/14w;

    .line 1962584
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;
    .locals 11

    .prologue
    .line 1962545
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    monitor-enter v1

    .line 1962546
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962547
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962548
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962549
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962550
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 1962551
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v8

    check-cast v8, LX/1V7;

    invoke-static {v7, v8}, LX/D5A;->a(Landroid/content/res/Resources;LX/1V7;)LX/1Wg;

    move-result-object v7

    move-object v7, v7

    .line 1962552
    check-cast v7, LX/1Wg;

    .line 1962553
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v9

    check-cast v9, LX/1V7;

    invoke-static {v8, v9}, LX/D5A;->b(Landroid/content/res/Resources;LX/1V7;)LX/1Wg;

    move-result-object v8

    move-object v8, v8

    .line 1962554
    check-cast v8, LX/1Wg;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v10

    check-cast v10, LX/14w;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/1Wg;LX/1Wg;Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;LX/14w;)V

    .line 1962555
    move-object v0, v3

    .line 1962556
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962557
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962558
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1962575
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1962563
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962564
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962565
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962566
    invoke-static {v0}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->f:LX/1We;

    sget-object v2, LX/1Wi;->TOP:LX/1Wi;

    invoke-virtual {v1, v2}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v1

    .line 1962567
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->b:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-interface {p1, v2, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1962568
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    new-instance v3, LX/21R;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v5

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-direct {v3, v4, v5, v0, v1}, LX/21R;-><init>(ZZZLX/1Wj;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1962569
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    .line 1962570
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->a:Landroid/content/res/Resources;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v2, v3}, LX/1Wj;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1962571
    new-instance v3, LX/21Q;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v2, v4}, LX/21Q;-><init>(LX/1Wj;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v1, v3

    .line 1962572
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1962573
    const/4 v0, 0x0

    return-object v0

    .line 1962574
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->e:LX/1We;

    sget-object v2, LX/1Wi;->TOP:LX/1Wi;

    invoke-virtual {v1, v2}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1962560
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962561
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962562
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->f(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
