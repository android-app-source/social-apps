.class public Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1964641
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1964642
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;
    .locals 3

    .prologue
    .line 1964643
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    monitor-enter v1

    .line 1964644
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1964645
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1964646
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1964647
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1964648
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;-><init>()V

    .line 1964649
    move-object v0, v0

    .line 1964650
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1964651
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1964652
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1964653
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2680b591

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1964654
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p4, Landroid/widget/TextView;

    .line 1964655
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964656
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964657
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1964658
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1964659
    const/16 v1, 0x1f

    const v2, -0x452bf8a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1964660
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
