.class public Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D4d;",
        "LX/D4e;",
        "TE;",
        "LX/BtI;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/1DR;

.field public final c:LX/Bt4;

.field private final d:LX/D4f;


# direct methods
.method public constructor <init>(LX/Bt4;LX/1DR;Landroid/content/res/Resources;LX/D4f;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962393
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1962394
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->b:LX/1DR;

    .line 1962395
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->c:LX/Bt4;

    .line 1962396
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->d:LX/D4f;

    .line 1962397
    const v0, 0x7f081032

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a:Ljava/lang/String;

    .line 1962398
    return-void
.end method

.method private static a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;)LX/Bt7;
    .locals 1

    .prologue
    .line 1962392
    new-instance v0, LX/Bt6;

    invoke-direct {v0, p1}, LX/Bt6;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p0, v0, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bt7;

    return-object v0
.end method

.method public static a(LX/Bt3;LX/D4d;Z)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 1962337
    iget-object v0, p1, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962338
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1962339
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1962340
    iget-object v1, p1, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1962341
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1962342
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962343
    invoke-static {v1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, LX/Bt3;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nA;

    invoke-virtual {v2, v0, p2}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1962344
    :goto_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-static {v2}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1962345
    iget-object v2, p0, LX/Bt3;->e:LX/1zC;

    iget-object v4, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {v4}, LX/1nq;->b()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 1962346
    iget-object v2, p0, LX/Bt3;->c:LX/1xc;

    invoke-virtual {v2, v1, v3}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/Spannable;)V

    .line 1962347
    move-object v0, v3

    .line 1962348
    iget v1, p1, LX/D4d;->c:I

    const/4 v3, 0x0

    .line 1962349
    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v4, LX/1yS;

    invoke-interface {v0, v3, v2, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LX/1yS;

    .line 1962350
    array-length v4, v2

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object p0, v2, v3

    .line 1962351
    iput v1, p0, LX/1yS;->f:I

    .line 1962352
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1962353
    :cond_0
    return-object v0

    .line 1962354
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;
    .locals 7

    .prologue
    .line 1962381
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    monitor-enter v1

    .line 1962382
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962383
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962384
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962385
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962386
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;

    const-class v3, LX/Bt4;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Bt4;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/D4f;->a(LX/0QB;)LX/D4f;

    move-result-object v6

    check-cast v6, LX/D4f;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;-><init>(LX/Bt4;LX/1DR;Landroid/content/res/Resources;LX/D4f;)V

    .line 1962387
    move-object v0, p0

    .line 1962388
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962389
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962390
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/text/SpannableStringBuilder;LX/1Ps;Lcom/facebook/graphql/model/GraphQLStory;LX/D4d;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "TE;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/D4d;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v8, 0x50

    .line 1962364
    move-object v0, p2

    check-cast v0, LX/1Pr;

    invoke-static {v0, p3}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;)LX/Bt7;

    move-result-object v2

    .line 1962365
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a:Ljava/lang/String;

    invoke-static {p1, v8, v0, v1}, LX/D4f;->a(Landroid/text/SpannableStringBuilder;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p4, LX/D4d;->b:Z

    if-eqz v0, :cond_1

    .line 1962366
    :cond_0
    new-instance v6, LX/1zQ;

    move-object v0, p2

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    new-instance v0, LX/D4b;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/D4b;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;LX/Bt7;LX/1Ps;LX/D4d;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-direct {v6, v7, v0}, LX/1zQ;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V

    .line 1962367
    iget v0, p4, LX/D4d;->c:I

    .line 1962368
    iput v0, v6, LX/1zQ;->b:I

    .line 1962369
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->d:LX/D4f;

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a:Ljava/lang/String;

    const/16 v5, 0x21

    .line 1962370
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962371
    if-lez v8, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1962372
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962373
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962374
    invoke-static {p1, v8, v1}, LX/D4f;->c(Landroid/text/SpannableStringBuilder;ILjava/lang/String;)I

    move-result v2

    .line 1962375
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v8}, LX/D4f;->a(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 1962376
    invoke-static {v1}, LX/D4f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1962377
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {p1, v6, v3, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1962378
    iget-object v3, v0, LX/D4f;->a:LX/1Uj;

    invoke-virtual {v3}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {p1, v3, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1962379
    :cond_1
    return-void

    .line 1962380
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(LX/D4d;)Z
    .locals 1

    .prologue
    .line 1962399
    iget-object v0, p0, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962400
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1962401
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962402
    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1962403
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1962363
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1962355
    check-cast p2, LX/D4d;

    check-cast p3, LX/1Ps;

    .line 1962356
    iget-object v0, p2, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962357
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1962358
    move-object v5, v0

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962359
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->c:LX/Bt4;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Bt4;->a(Landroid/content/Context;)LX/Bt3;

    move-result-object v4

    .line 1962360
    const/4 v0, 0x1

    invoke-static {v4, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/Bt3;LX/D4d;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1962361
    invoke-direct {p0, v3, p3, v5, p2}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(Landroid/text/SpannableStringBuilder;LX/1Ps;Lcom/facebook/graphql/model/GraphQLStory;LX/D4d;)V

    .line 1962362
    new-instance v0, LX/D4e;

    iget-object v1, p2, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v4, v1, v3}, LX/Bt3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;)Landroid/text/Layout;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->b:LX/1DR;

    invoke-virtual {v2}, LX/1DR;->a()I

    move-result v2

    check-cast p3, LX/1Pr;

    invoke-static {p3, v5}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;)LX/Bt7;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/D4e;-><init>(Landroid/text/Layout;ILandroid/text/SpannableStringBuilder;LX/Bt3;LX/Bt7;B)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x240ab74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1962324
    check-cast p1, LX/D4d;

    check-cast p2, LX/D4e;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/BtI;

    const/4 v4, 0x1

    .line 1962325
    iget-object v1, p2, LX/D4e;->f:LX/Bt7;

    .line 1962326
    iget-boolean v2, v1, LX/Bt7;->a:Z

    move v7, v2

    .line 1962327
    if-eqz v7, :cond_0

    iget-boolean v1, p2, LX/D4e;->d:Z

    if-nez v1, :cond_0

    .line 1962328
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->c:LX/Bt4;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Bt4;->a(Landroid/content/Context;)LX/Bt3;

    move-result-object v1

    .line 1962329
    invoke-static {v1, p1, v4}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/Bt3;LX/D4d;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iput-object v1, p2, LX/D4e;->c:Landroid/text/SpannableStringBuilder;

    .line 1962330
    iput-boolean v4, p2, LX/D4e;->d:Z

    .line 1962331
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->b:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v6

    .line 1962332
    iget-object v1, p1, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/D4e;->a:Landroid/text/Layout;

    iget v4, p2, LX/D4e;->b:I

    iget-object v5, p2, LX/D4e;->c:Landroid/text/SpannableStringBuilder;

    iget-object v8, p2, LX/D4e;->e:LX/Bt3;

    .line 1962333
    if-ne v4, v6, :cond_1

    if-nez v7, :cond_1

    :goto_0
    move-object v1, v2

    .line 1962334
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTextLayout(Landroid/text/Layout;)V

    .line 1962335
    const v1, 0x7f0d0081

    iget-object v2, p1, LX/D4d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, LX/BtI;->setTag(ILjava/lang/Object;)V

    .line 1962336
    const/16 v1, 0x1f

    const v2, 0x64be3aaa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    invoke-virtual {v8, v1, v5}, LX/Bt3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;)Landroid/text/Layout;

    move-result-object v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1962323
    check-cast p1, LX/D4d;

    invoke-static {p1}, Lcom/facebook/video/channelfeed/ChannelFeedExpandingContentTextPartDefinition;->a(LX/D4d;)Z

    move-result v0

    return v0
.end method
