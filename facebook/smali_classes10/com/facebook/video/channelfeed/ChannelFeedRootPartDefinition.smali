.class public Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/channelfeed/HasChannelFeedParams;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pg;",
        ":",
        "Lcom/facebook/video/channelfeed/CanReusePlayer;",
        ":",
        "LX/1Pe;",
        ":",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        ":",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

.field private final c:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

.field private final d:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

.field private final e:Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963565
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1963566
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    .line 1963567
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->b:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    .line 1963568
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    .line 1963569
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    .line 1963570
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->e:Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    .line 1963571
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;
    .locals 9

    .prologue
    .line 1963572
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;

    monitor-enter v1

    .line 1963573
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963574
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963577
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;)V

    .line 1963578
    move-object v0, v3

    .line 1963579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1963583
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 1963584
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1963585
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963586
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    instance-of v1, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v1, :cond_1

    invoke-static {p2}, Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1963587
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1963588
    :cond_0
    :goto_0
    return-object v3

    .line 1963589
    :cond_1
    instance-of v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v1, :cond_2

    .line 1963590
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->e:Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1963591
    :cond_2
    instance-of v1, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v1, :cond_3

    .line 1963592
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->b:Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1963593
    :cond_3
    instance-of v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 1963594
    check-cast v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;

    .line 1963595
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1963596
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1963597
    :cond_4
    instance-of v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    if-eqz v1, :cond_0

    .line 1963598
    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    .line 1963599
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1963600
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedRootPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1963601
    const/4 v0, 0x1

    return v0
.end method
