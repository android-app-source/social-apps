.class public Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/D53;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1963328
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    const-string v1, "video_channel_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1963326
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1963327
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1963324
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963325
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1963314
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963315
    const-class v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1963316
    const v0, 0x7f030269

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1963317
    const v0, 0x7f0d0575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1963318
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->d:Landroid/widget/TextView;

    .line 1963319
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->e:Landroid/widget/TextView;

    .line 1963320
    const v0, 0x7f0d0905

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->f:Landroid/widget/TextView;

    .line 1963321
    const v0, 0x7f0d0906

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    .line 1963322
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->setClipChildren(Z)V

    .line 1963323
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1963310
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1963311
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1963312
    return-void

    .line 1963313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    const/16 p0, 0xafc

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;Z)V
    .locals 3

    .prologue
    .line 1963260
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const-string v1, "VIDEO_CHANNEL_HEADER"

    const-string v2, "VIDEO_CHANNEL_HEADER"

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1963261
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;ZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 1963308
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const-string v3, "VIDEO_CHANNEL_HEADER"

    const-string v4, "VIDEO_CHANNEL_HEADER"

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->h:Ljava/lang/String;

    move v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1963309
    return-void
.end method


# virtual methods
.method public final a(LX/D4s;)V
    .locals 14

    .prologue
    .line 1963264
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963265
    iget-object v0, p1, LX/D4s;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1963266
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1963267
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1963268
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->d:Landroid/widget/TextView;

    .line 1963269
    iget-object v1, p1, LX/D4s;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1963270
    invoke-static {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1963271
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->e:Landroid/widget/TextView;

    .line 1963272
    iget-object v1, p1, LX/D4s;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1963273
    invoke-static {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1963274
    iget-object v0, p1, LX/D4s;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1963275
    invoke-virtual {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->setBadgeText(Ljava/lang/String;)V

    .line 1963276
    iget-boolean v0, p1, LX/D4s;->i:Z

    move v0, v0

    .line 1963277
    if-nez v0, :cond_0

    .line 1963278
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    .line 1963279
    :goto_1
    return-void

    .line 1963280
    :cond_0
    iget-object v0, p1, LX/D4s;->m:LX/BUv;

    move-object v0, v0

    .line 1963281
    if-eqz v0, :cond_1

    .line 1963282
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    .line 1963283
    iget-boolean v1, p1, LX/D4s;->g:Z

    move v1, v1

    .line 1963284
    iget-object v2, p1, LX/D4s;->k:Ljava/lang/String;

    move-object v2, v2

    .line 1963285
    iget-object v3, p1, LX/D4s;->l:Ljava/lang/String;

    move-object v3, v3

    .line 1963286
    iget-object v4, p1, LX/D4s;->m:LX/BUv;

    move-object v4, v4

    .line 1963287
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;LX/BUv;)V

    .line 1963288
    :goto_2
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->g:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    goto :goto_1

    .line 1963289
    :cond_1
    iget-boolean v0, p1, LX/D4s;->g:Z

    move v0, v0

    .line 1963290
    iget-object v1, p1, LX/D4s;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1963291
    invoke-static {p0, v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;ZLjava/lang/String;)V

    .line 1963292
    iget-boolean v0, p1, LX/D4s;->j:Z

    move v0, v0

    .line 1963293
    if-eqz v0, :cond_2

    .line 1963294
    iget-boolean v0, p1, LX/D4s;->h:Z

    move v0, v0

    .line 1963295
    invoke-static {p0, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;Z)V

    .line 1963296
    :cond_2
    iget-object v0, p1, LX/D4s;->n:LX/0jT;

    move-object v0, v0

    .line 1963297
    instance-of v5, v0, Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v5, :cond_5

    .line 1963298
    :cond_3
    :goto_3
    goto :goto_2

    .line 1963299
    :cond_4
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1963300
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move-object v5, v0

    .line 1963301
    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v12

    .line 1963302
    if-eqz v12, :cond_3

    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    invoke-virtual {v5, v12}, LX/1My;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1963303
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->i:LX/D53;

    if-nez v5, :cond_6

    .line 1963304
    new-instance v5, LX/D53;

    invoke-direct {v5, p0}, LX/D53;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;)V

    iput-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->i:LX/D53;

    .line 1963305
    :cond_6
    invoke-static {v12}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v11

    .line 1963306
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    invoke-virtual {v5, v11}, LX/1My;->a(Ljava/util/Set;)V

    .line 1963307
    iget-object v5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    iget-object v13, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->i:LX/D53;

    new-instance v6, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v8, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v9, 0x0

    move-object v7, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    invoke-virtual {v5, v13, v12, v6}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_3
.end method

.method public setBadgeText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1963262
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1963263
    return-void
.end method
