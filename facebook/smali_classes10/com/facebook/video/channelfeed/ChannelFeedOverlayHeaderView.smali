.class public Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:Landroid/animation/ValueAnimator;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1963496
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1963497
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1963494
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1963484
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963485
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    .line 1963486
    const v0, 0x7f030270

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1963487
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1963488
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1963489
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1963490
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/D5F;

    invoke-direct {v1, p0}, LX/D5F;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1963491
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1963492
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1963493
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1963473
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 1963474
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-eqz v3, :cond_1

    .line 1963475
    :goto_1
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1963476
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v0, 0x1

    aput v2, v3, v0

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1963477
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1963478
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/D5G;

    invoke-direct {v1, p0}, LX/D5G;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1963479
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1963480
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 1963481
    goto :goto_0

    :cond_1
    move v2, v1

    .line 1963482
    goto :goto_1

    .line 1963483
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1963469
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-eqz v0, :cond_0

    .line 1963470
    :goto_0
    return-void

    .line 1963471
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    .line 1963472
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1963465
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-nez v0, :cond_0

    .line 1963466
    :goto_0
    return-void

    .line 1963467
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    .line 1963468
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c()V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1963463
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1963464
    return-void
.end method
