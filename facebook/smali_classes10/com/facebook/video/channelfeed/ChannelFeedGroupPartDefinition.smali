.class public Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "Lcom/facebook/video/channelfeed/HasChannelFeedParams;",
        ":",
        "LX/1Po;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pg;",
        ":",
        "Lcom/facebook/video/channelfeed/CanReusePlayer;",
        ":",
        "LX/1Pe;",
        ":",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        ":",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/D4Q;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

.field public final b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

.field public final e:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

.field private final f:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final j:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/D5q;

.field private final l:LX/0sV;

.field public final m:Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;LX/1AZ;LX/7zZ;Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;LX/D5q;LX/0sV;Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962965
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    .line 1962966
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    .line 1962967
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    .line 1962968
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    .line 1962969
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->e:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    .line 1962970
    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    .line 1962971
    iput-object p7, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->g:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    .line 1962972
    iput-object p8, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->h:Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    .line 1962973
    iput-object p11, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    .line 1962974
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p9, p10, v1, v2}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->j:LX/1Aa;

    .line 1962975
    iput-object p12, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->k:LX/D5q;

    .line 1962976
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->l:LX/0sV;

    .line 1962977
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->m:Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    .line 1962978
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1962962
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1962963
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;
    .locals 3

    .prologue
    .line 1962954
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    monitor-enter v1

    .line 1962955
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962956
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962957
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962958
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962959
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962960
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1962893
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 1962894
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    new-instance v1, LX/D4d;

    invoke-direct {v1, p3}, LX/D4d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1962895
    return-void

    :cond_0
    move-object p3, p2

    .line 1962896
    goto :goto_0
.end method

.method public static a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z
    .locals 1

    .prologue
    .line 1962953
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->l:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->m:Z

    return v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;
    .locals 15

    .prologue
    .line 1962951
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    const-class v9, LX/1AZ;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1AZ;

    invoke-static {p0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v10

    check-cast v10, LX/7zZ;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    const-class v12, LX/D5q;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/D5q;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v13

    check-cast v13, LX/0sV;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;LX/1AZ;LX/7zZ;Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;LX/D5q;LX/0sV;Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;)V

    .line 1962952
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1962897
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 1962898
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962899
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962900
    invoke-static {p2}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 1962901
    iget-object v1, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1962902
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962903
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1962904
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1962905
    :goto_0
    move-object v6, p2

    .line 1962906
    if-eqz v6, :cond_2

    .line 1962907
    iget-object v2, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1962908
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v2

    .line 1962909
    :goto_1
    invoke-static {v1}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1962910
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p3

    .line 1962911
    check-cast v2, LX/1Pr;

    iget-object v7, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->k:LX/D5q;

    invoke-virtual {v7, v5, v4}, LX/D5q;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/D5p;

    move-result-object v7

    invoke-interface {v2, v7, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D5r;

    .line 1962912
    check-cast p3, LX/D4U;

    .line 1962913
    iget-object v7, p3, LX/D4U;->t:LX/D6I;

    move-object v7, v7

    .line 1962914
    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    .line 1962915
    iget-object v8, v7, LX/D6I;->k:Ljava/lang/String;

    move-object v7, v8

    .line 1962916
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    .line 1962917
    :goto_2
    iput-boolean v4, v2, LX/D5r;->j:Z

    .line 1962918
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    invoke-virtual {p1, v4, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1962919
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eq v3, v1, :cond_0

    if-nez v3, :cond_7

    :cond_0
    const/4 v0, 0x1

    .line 1962920
    :goto_3
    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z

    move-result v4

    if-eqz v4, :cond_8

    if-eqz v3, :cond_8

    .line 1962921
    :goto_4
    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    new-instance v7, LX/D4x;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    invoke-direct {v7, v8, v0}, LX/D4x;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-virtual {p1, v4, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1962922
    iget-object v0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962923
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962924
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1962925
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->m:Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedReuseVideoPartDefinition;

    new-instance v4, LX/D65;

    invoke-direct {v4, v0, v2}, LX/D65;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/D5r;)V

    invoke-virtual {v1, v3, v4}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    new-instance v4, LX/D65;

    invoke-direct {v4, v0, v2}, LX/D65;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/D5r;)V

    invoke-virtual {v1, v3, v4}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1962926
    iget-object v0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962927
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1962928
    if-eqz v0, :cond_1

    .line 1962929
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1962930
    if-nez v1, :cond_9

    .line 1962931
    :cond_1
    :goto_5
    invoke-direct {p0, p1, v5, v6}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1962932
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->h:Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    new-instance v3, LX/D4H;

    .line 1962933
    iget-object v0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962934
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z

    move-result v4

    invoke-direct {v3, v0, v6, v4}, LX/D4H;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-static {p1, v1, v3}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->g:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    invoke-virtual {v0, v1, v5}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1962935
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_a

    if-eqz v6, :cond_a

    :goto_6
    invoke-virtual {p1, v0, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1962936
    iget-object v0, v2, LX/D5r;->c:LX/D4Q;

    move-object v0, v0

    .line 1962937
    return-object v0

    .line 1962938
    :cond_2
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_1

    .line 1962939
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1962940
    :cond_4
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1962941
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962942
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1962943
    :goto_7
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    if-nez v4, :cond_5

    .line 1962944
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1962945
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    goto :goto_7

    .line 1962946
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->reverse()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p2

    goto/16 :goto_0

    :cond_6
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 1962947
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_8
    move-object v3, v1

    .line 1962948
    goto/16 :goto_4

    .line 1962949
    :cond_9
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->e:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_5

    :cond_a
    move-object v6, v5

    .line 1962950
    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 1962887
    check-cast p2, LX/D4Q;

    .line 1962888
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, LX/D5z;

    if-eqz v0, :cond_0

    .line 1962889
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->j:LX/1Aa;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v1, v0, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1962890
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1962891
    :cond_0
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->a(Landroid/view/View;)V

    .line 1962892
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1962883
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962884
    invoke-static {p1}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1962885
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1962886
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 1962879
    check-cast p2, LX/D4Q;

    .line 1962880
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->b(Landroid/view/View;)V

    .line 1962881
    invoke-virtual {p2}, LX/D4Q;->b()V

    .line 1962882
    return-void
.end method
