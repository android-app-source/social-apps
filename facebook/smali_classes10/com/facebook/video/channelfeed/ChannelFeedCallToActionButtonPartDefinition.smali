.class public Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/D4M;


# direct methods
.method public constructor <init>(LX/D4M;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962071
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1962072
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;->a:LX/D4M;

    .line 1962073
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;
    .locals 4

    .prologue
    .line 1962074
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    monitor-enter v1

    .line 1962075
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962076
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962077
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962078
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962079
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    invoke-static {v0}, LX/D4M;->a(LX/0QB;)LX/D4M;

    move-result-object v3

    check-cast v3, LX/D4M;

    invoke-direct {p0, v3}, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;-><init>(LX/D4M;)V

    .line 1962080
    move-object v0, p0

    .line 1962081
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962082
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962083
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6e26720a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1962085
    move-object v1, p4

    check-cast v1, LX/35p;

    invoke-interface {v1}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1962086
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v2

    .line 1962087
    const v2, 0x7f020226

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 1962088
    invoke-virtual {p4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1962089
    const p0, 0x7f0a05de

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 1962090
    const/16 v1, 0x1f

    const v2, -0x7f3239df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
