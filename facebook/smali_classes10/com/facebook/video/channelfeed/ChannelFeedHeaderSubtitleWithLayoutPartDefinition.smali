.class public Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/D4y;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/D4v;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/D4v;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963125
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1963126
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    .line 1963127
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->b:LX/D4v;

    .line 1963128
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;
    .locals 5

    .prologue
    .line 1963129
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    monitor-enter v1

    .line 1963130
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963131
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963132
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963133
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963134
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, LX/D4v;->a(LX/0QB;)LX/D4v;

    move-result-object v4

    check-cast v4, LX/D4v;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/D4v;)V

    .line 1963135
    move-object v0, p0

    .line 1963136
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963137
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963138
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1963140
    check-cast p2, LX/D4y;

    .line 1963141
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    new-instance v1, LX/Brw;

    iget-object v2, p2, LX/D4y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->b:LX/D4v;

    iget v4, p2, LX/D4y;->b:I

    invoke-direct {v1, v2, v3, v4}, LX/Brw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1xb;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1963142
    const/4 v0, 0x0

    return-object v0
.end method
