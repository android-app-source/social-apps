.class public Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pg;",
        ":",
        "Lcom/facebook/video/channelfeed/CanReusePlayer;",
        ":",
        "LX/1Pe;",
        ":",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;",
        ">;",
        "LX/D4Q;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

.field private final b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

.field private final d:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;

.field private final g:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/D5q;

.field private final i:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;LX/1AZ;LX/7zZ;Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;LX/D5q;Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965527
    iput-object p1, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    .line 1965528
    iput-object p2, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    .line 1965529
    iput-object p3, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    .line 1965530
    iput-object p4, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    .line 1965531
    iput-object p7, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->e:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    .line 1965532
    iput-object p10, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    .line 1965533
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p5, p6, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->g:LX/1Aa;

    .line 1965534
    iput-object p8, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->h:LX/D5q;

    .line 1965535
    iput-object p9, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->f:Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;

    .line 1965536
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965467
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1965468
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;
    .locals 14

    .prologue
    .line 1965515
    const-class v1, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    monitor-enter v1

    .line 1965516
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1965517
    sput-object v2, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1965518
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965519
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1965520
    new-instance v3, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    const-class v8, LX/1AZ;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1AZ;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v9

    check-cast v9, LX/7zZ;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    const-class v11, LX/D5q;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/D5q;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;LX/1AZ;LX/7zZ;Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;LX/D5q;Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;)V

    .line 1965521
    move-object v0, v3

    .line 1965522
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1965523
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1965524
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1965525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1965485
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    const/4 v3, 0x1

    .line 1965486
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965487
    check-cast v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965488
    iget-object v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v4, v1

    .line 1965489
    invoke-static {v4}, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 1965490
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1965491
    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    move-object v1, p3

    .line 1965492
    check-cast v1, LX/1Pr;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->h:LX/D5q;

    invoke-virtual {v6, v5, v2}, LX/D5q;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/D5p;

    move-result-object v6

    invoke-interface {v1, v6, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D5r;

    .line 1965493
    check-cast p3, LX/D4U;

    .line 1965494
    iget-object v6, p3, LX/D4U;->t:LX/D6I;

    move-object v6, v6

    .line 1965495
    if-eqz v6, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    .line 1965496
    iget-object v7, v6, LX/D6I;->k:Ljava/lang/String;

    move-object v6, v7

    .line 1965497
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    .line 1965498
    :goto_0
    iput-boolean v2, v1, LX/D5r;->j:Z

    .line 1965499
    iget-object v2, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->b:LX/D6N;

    sget-object v6, LX/D6N;->FIRST:LX/D6N;

    if-ne v2, v6, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1965500
    if-eqz v2, :cond_0

    .line 1965501
    iget-object v2, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    .line 1965502
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1965503
    invoke-virtual {p1, v2, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1965504
    iget-object v2, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    new-instance v6, LX/D4x;

    invoke-direct {v6, v5, v3}, LX/D4x;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-virtual {p1, v2, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1965505
    :cond_0
    iget-object v2, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    new-instance v3, LX/D65;

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-direct {v3, v4, v1}, LX/D65;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/D5r;)V

    invoke-static {p1, v2, v3}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1965506
    iget-object v2, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedTextPartDefinition;

    new-instance v3, LX/D4d;

    invoke-direct {v3, v5}, LX/D4d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1965507
    iget-object v2, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->b:LX/D6N;

    sget-object v3, LX/D6N;->LAST:LX/D6N;

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 1965508
    if-eqz v0, :cond_2

    .line 1965509
    iget-object v0, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    invoke-virtual {p1, v0, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1965510
    iget-object v0, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->e:Lcom/facebook/video/channelfeed/ChannelFeedFooterPartDefinition;

    invoke-virtual {p1, v0, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1965511
    :goto_3
    iget-object v0, v1, LX/D5r;->c:LX/D4Q;

    move-object v0, v0

    .line 1965512
    return-object v0

    .line 1965513
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1965514
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->f:Lcom/facebook/video/channelfeed/MultiShareChannelFeedDividerPartDefinition;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 1965479
    check-cast p2, LX/D4Q;

    .line 1965480
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, LX/D5z;

    if-eqz v0, :cond_0

    .line 1965481
    iget-object v1, p0, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->g:LX/1Aa;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v1, v0, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1965482
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1965483
    :cond_0
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->a(Landroid/view/View;)V

    .line 1965484
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1965473
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965474
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965475
    check-cast v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965476
    iget-object v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1965477
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1965478
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/facebook/video/channelfeed/MultiShareChannelFeedPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 1965469
    check-cast p2, LX/D4Q;

    .line 1965470
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/D4Q;->b(Landroid/view/View;)V

    .line 1965471
    invoke-virtual {p2}, LX/D4Q;->b()V

    .line 1965472
    return-void
.end method
