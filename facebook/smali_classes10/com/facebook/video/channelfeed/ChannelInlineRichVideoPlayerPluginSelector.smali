.class public Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1965384
    const-class v0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;LX/0Uh;)V
    .locals 11
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 1965385
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1965386
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1965387
    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v2, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, p1, v2}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1965388
    new-instance v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1965389
    new-instance v3, LX/2pM;

    invoke-direct {v3, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    .line 1965390
    new-instance v4, LX/D6Y;

    invoke-direct {v4, p1}, LX/D6Y;-><init>(Landroid/content/Context;)V

    .line 1965391
    new-instance v5, Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;

    invoke-direct {v5, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeedClickToFullscreenPlugin;-><init>(Landroid/content/Context;)V

    .line 1965392
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->b:LX/0Px;

    .line 1965393
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/3Gh;

    invoke-direct {v7, p1}, LX/3Gh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    .line 1965394
    const/16 v7, 0x466

    invoke-virtual {p3, v7, v10}, LX/0Uh;->a(IZ)Z

    move-result v7

    .line 1965395
    const/16 v8, 0x427

    invoke-virtual {p3, v8, v10}, LX/0Uh;->a(IZ)Z

    move-result v8

    .line 1965396
    if-eqz v7, :cond_0

    .line 1965397
    new-instance v9, LX/3Hz;

    invoke-direct {v9, p1}, LX/3Hz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965398
    :cond_0
    if-eqz v8, :cond_1

    .line 1965399
    new-instance v9, LX/31H;

    invoke-direct {v9, p1}, LX/31H;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965400
    :cond_1
    if-nez v7, :cond_2

    if-eqz v8, :cond_3

    .line 1965401
    :cond_2
    new-instance v7, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v8, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v7, p1, v8}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v7

    new-instance v8, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {v8, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965402
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->c:LX/0Px;

    .line 1965403
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->e:LX/0Px;

    .line 1965404
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/3Ie;

    invoke-direct {v7, p1}, LX/3Ie;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->d:LX/0Px;

    .line 1965405
    iget-object v6, p0, LX/3Ge;->d:LX/0Px;

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->j:LX/0Px;

    .line 1965406
    iget-object v6, p0, LX/3Ge;->d:LX/0Px;

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->i:LX/0Px;

    .line 1965407
    iget-object v6, p0, LX/3Ge;->c:LX/0Px;

    iput-object v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->h:LX/0Px;

    .line 1965408
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iput-boolean v6, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->k:Z

    .line 1965409
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1965410
    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v7

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965411
    const/16 v0, 0x2a6

    invoke-virtual {p3, v0, v10}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1965412
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1965413
    :goto_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->f:LX/0Px;

    .line 1965414
    return-void

    .line 1965415
    :cond_4
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;
    .locals 4

    .prologue
    .line 1965416
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;-><init>(Landroid/content/Context;Ljava/lang/Boolean;LX/0Uh;)V

    .line 1965417
    move-object v0, v3

    .line 1965418
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1965419
    const-class v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1965420
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    .line 1965421
    :goto_0
    return-object v0

    .line 1965422
    :cond_0
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1965423
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965424
    :cond_1
    const-class v0, LX/2pM;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1965425
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965426
    :cond_2
    const-class v0, LX/D6Y;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1965427
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1965428
    :cond_3
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
