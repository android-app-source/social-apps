.class public Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D6P;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1965556
    const v0, 0x7f0310a4

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965557
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1965558
    iput-object p2, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->b:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    .line 1965559
    iput-object p1, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    .line 1965560
    iput-object p3, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->d:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    .line 1965561
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;
    .locals 6

    .prologue
    .line 1965562
    const-class v1, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    monitor-enter v1

    .line 1965563
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1965564
    sput-object v2, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1965565
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965566
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1965567
    new-instance p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;-><init>(Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;)V

    .line 1965568
    move-object v0, p0

    .line 1965569
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1965570
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1965571
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1965572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1965573
    sget-object v0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1965574
    check-cast p2, LX/D6P;

    .line 1965575
    const v0, 0x7f0d02c4

    iget-object v1, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->b:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    iget-object v2, p2, LX/D6P;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1965576
    iget-object v0, p2, LX/D6P;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1965577
    iget-object v0, p2, LX/D6P;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_2

    iget-object v0, p2, LX/D6P;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1965578
    :goto_0
    const v2, 0x7f0d02c3

    iget-object v3, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->d:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    new-instance v4, LX/8Cv;

    const v5, 0x7f0b1108

    const/4 v6, 0x0

    invoke-direct {v4, v0, v5, v6}, LX/8Cv;-><init>(Lcom/facebook/graphql/model/GraphQLTextWithEntities;II)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1965579
    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1965580
    invoke-static {v1}, LX/1xl;->f(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 1965581
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1965582
    iget-object v2, p2, LX/D6P;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 1965583
    if-eqz v2, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1965584
    :cond_0
    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 1965585
    :cond_1
    const v1, 0x7f0d0952

    iget-object v2, p0, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    new-instance v3, LX/8Cq;

    invoke-direct {v3}, LX/8Cq;-><init>()V

    invoke-virtual {v3, v0}, LX/8Cq;->a(Ljava/lang/String;)LX/8Cq;

    move-result-object v0

    const v3, 0x7f02111f

    .line 1965586
    iput v3, v0, LX/8Cq;->c:I

    .line 1965587
    move-object v3, v0

    .line 1965588
    iget-boolean v0, p2, LX/D6P;->b:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0b1105

    .line 1965589
    :goto_1
    iput v0, v3, LX/8Cq;->d:I

    .line 1965590
    move-object v3, v3

    .line 1965591
    iget-boolean v0, p2, LX/D6P;->b:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0b1107

    .line 1965592
    :goto_2
    iput v0, v3, LX/8Cq;->e:I

    .line 1965593
    move-object v0, v3

    .line 1965594
    invoke-virtual {v0}, LX/8Cq;->a()LX/8Cr;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1965595
    const/4 v0, 0x0

    return-object v0

    .line 1965596
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1965597
    :cond_3
    const v0, 0x7f0b1104

    goto :goto_1

    :cond_4
    const v0, 0x7f0b1106

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1965598
    const/4 v0, 0x1

    return v0
.end method
