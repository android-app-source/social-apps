.class public Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/D57;",
        "LX/2oV;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/D56;


# direct methods
.method public constructor <init>(LX/7zZ;LX/1AZ;LX/D56;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963369
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1963370
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->b:LX/D56;

    .line 1963371
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->a:LX/1Aa;

    .line 1963372
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;
    .locals 6

    .prologue
    .line 1963358
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    monitor-enter v1

    .line 1963359
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963360
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963363
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v3

    check-cast v3, LX/7zZ;

    const-class v4, LX/1AZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1AZ;

    const-class v5, LX/D56;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/D56;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;-><init>(LX/7zZ;LX/1AZ;LX/D56;)V

    .line 1963364
    move-object v0, p0

    .line 1963365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1963350
    check-cast p2, LX/D57;

    .line 1963351
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->b:LX/D56;

    iget-object v1, p2, LX/D57;->a:Ljava/lang/String;

    iget-object v2, p2, LX/D57;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963352
    new-instance p1, LX/D55;

    invoke-static {v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(LX/0QB;)Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    move-result-object p0

    check-cast p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-direct {p1, v1, v2, p0}, LX/D55;-><init>(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;)V

    .line 1963353
    move-object v0, p1

    .line 1963354
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2240bf17

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1963355
    check-cast p2, LX/2oV;

    .line 1963356
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->a:LX/1Aa;

    invoke-virtual {v1, p4, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1963357
    const/16 v1, 0x1f

    const v2, -0x3823078a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
