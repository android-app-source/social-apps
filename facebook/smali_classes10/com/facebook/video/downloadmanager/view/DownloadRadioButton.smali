.class public Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;
.implements LX/BcB;


# instance fields
.field public a:Z

.field private b:Landroid/widget/ImageButton;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:LX/Bc8;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1967305
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1967306
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->a()V

    .line 1967307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1967349
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1967350
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->a()V

    .line 1967351
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1967342
    const v0, 0x7f03043c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1967343
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setClickable(Z)V

    .line 1967344
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->b:Landroid/widget/ImageButton;

    .line 1967345
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 1967346
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1967347
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1967348
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1967341
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    return v0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1967337
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1967338
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1967339
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 1967340
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1967332
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1967333
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1967334
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 1967335
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 1967336
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1967331
    const/4 v0, 0x1

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1967352
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->toggle()V

    .line 1967353
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1967321
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    if-eq v0, p1, :cond_0

    .line 1967322
    iput-boolean p1, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    .line 1967323
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1967324
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->refreshDrawableState()V

    .line 1967325
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->a:Z

    if-eqz v0, :cond_1

    .line 1967326
    :goto_0
    return-void

    .line 1967327
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->a:Z

    .line 1967328
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->e:LX/Bc8;

    if-eqz v0, :cond_2

    .line 1967329
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->e:LX/Bc8;

    invoke-virtual {v0, p0}, LX/Bc8;->a(Landroid/view/View;)V

    .line 1967330
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->a:Z

    goto :goto_0
.end method

.method public setDescription(I)V
    .locals 1

    .prologue
    .line 1967319
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1967320
    return-void
.end method

.method public setDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1967317
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1967318
    return-void
.end method

.method public setOnCheckedChangeWidgetListener(LX/Bc8;)V
    .locals 0

    .prologue
    .line 1967315
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->e:LX/Bc8;

    .line 1967316
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1967313
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1967314
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1967311
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1967312
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1967308
    iget-boolean v0, p0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->f:Z

    if-nez v0, :cond_0

    .line 1967309
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setChecked(Z)V

    .line 1967310
    :cond_0
    return-void
.end method
