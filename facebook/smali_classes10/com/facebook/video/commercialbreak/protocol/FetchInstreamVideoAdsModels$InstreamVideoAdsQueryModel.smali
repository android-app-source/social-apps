.class public final Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x226b3ed2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1967053
    const-class v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1967029
    const-class v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1967030
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1967031
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1967032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1967033
    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1967034
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1967035
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1967036
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1967037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1967038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1967039
    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1967040
    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    .line 1967041
    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1967042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;

    .line 1967043
    iput-object v0, v1, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->e:Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    .line 1967044
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1967045
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1967046
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->e:Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->e:Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    .line 1967047
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->e:Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1967048
    new-instance v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;-><init>()V

    .line 1967049
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1967050
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1967051
    const v0, -0x46067001

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1967052
    const v0, -0x6747e1ce

    return v0
.end method
