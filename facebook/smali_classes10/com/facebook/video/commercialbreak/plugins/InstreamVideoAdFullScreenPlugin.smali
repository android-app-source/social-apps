.class public Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;
.super LX/3Ga;
.source ""

# interfaces
.implements LX/3Gr;
.implements LX/2pG;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final q:Ljava/lang/String;


# instance fields
.field public b:LX/2mZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3H4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/3HS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Landroid/view/View;

.field public u:Lcom/facebook/video/player/RichVideoPlayer;

.field private v:LX/D6x;

.field public w:LX/D6v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1966665
    const-class v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1966672
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1966673
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1966670
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966671
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 1966666
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966667
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    const-class v3, LX/2mZ;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mZ;

    invoke-static {v0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v4

    check-cast v4, LX/2ml;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object p1

    check-cast p1, LX/1C2;

    invoke-static {v0}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object p2

    check-cast p2, LX/3H4;

    invoke-static {v0}, LX/3HS;->a(LX/0QB;)LX/3HS;

    move-result-object p3

    check-cast p3, LX/3HS;

    invoke-static {v0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v3, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->b:LX/2mZ;

    iput-object v4, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->c:LX/2ml;

    iput-object v5, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object p1, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->e:LX/1C2;

    iput-object p2, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->f:LX/3H4;

    iput-object p3, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->o:LX/3HS;

    iput-object v0, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->p:Ljava/lang/Boolean;

    .line 1966668
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/D6y;

    invoke-direct {v1, p0}, LX/D6y;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1966669
    return-void
.end method

.method public static b(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    .line 1966629
    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1966630
    if-nez v1, :cond_0

    .line 1966631
    :goto_0
    return-void

    .line 1966632
    :cond_0
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1966633
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 1966634
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->b:LX/2mZ;

    invoke-virtual {v0, v1, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v0

    .line 1966635
    invoke-virtual {v0, v4, v4}, LX/3Im;->a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v7

    .line 1966636
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    .line 1966637
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v3

    .line 1966638
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v2

    .line 1966639
    if-lez v2, :cond_1

    if-lez v3, :cond_1

    .line 1966640
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v10, v3

    mul-double/2addr v0, v10

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1966641
    :cond_1
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    const-string v3, "GraphQLStoryProps"

    iget-object v5, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3, v5}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v2

    .line 1966642
    iput-object v7, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1966643
    move-object v2, v2

    .line 1966644
    iput-wide v0, v2, LX/2pZ;->e:D

    .line 1966645
    move-object v0, v2

    .line 1966646
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1966647
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1966648
    iget-object v1, p0, LX/3Gb;->n:LX/7Lf;

    if-eqz v1, :cond_2

    .line 1966649
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, LX/3Gb;->n:LX/7Lf;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPluginEnvironment(LX/7Lf;)V

    .line 1966650
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1966651
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1966652
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v2, v1

    .line 1966653
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1966654
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966655
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v1

    .line 1966656
    :goto_2
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->e:LX/1C2;

    iget-object v1, v7, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04g;->BY_AUTOPLAY:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v5, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1966657
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 1966658
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setIsInstreamVideoAdPlayer(Z)V

    .line 1966659
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-eqz v0, :cond_6

    .line 1966660
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    iget-wide v0, v0, LX/D6v;->c:J

    .line 1966661
    :goto_3
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    cmp-long v3, v0, v8

    if-lez v3, :cond_3

    long-to-int v4, v0

    :cond_3
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v2, v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1966662
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto/16 :goto_0

    .line 1966663
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 1966664
    :cond_5
    sget-object v6, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_2

    :cond_6
    move-wide v0, v8

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1966626
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-nez v0, :cond_1

    .line 1966627
    :cond_0
    :goto_0
    return-void

    .line 1966628
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_0
.end method

.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1966617
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->c:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_1

    .line 1966618
    :cond_0
    :goto_0
    return-void

    .line 1966619
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1966620
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966621
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1966622
    sget-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->q:Ljava/lang/String;

    invoke-static {p1, v0}, LX/3JF;->a(LX/2pa;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1966623
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    .line 1966624
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    .line 1966625
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->o:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->a(LX/3Gr;)V

    goto :goto_0
.end method

.method public final a(LX/D6q;)V
    .locals 2

    .prologue
    .line 1966674
    sget-object v0, LX/D6w;->a:[I

    invoke-virtual {p1}, LX/D6q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1966675
    :cond_0
    :goto_0
    return-void

    .line 1966676
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-eqz v0, :cond_0

    .line 1966677
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    sget-object v1, LX/BSR;->HIDE_AD:LX/BSR;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSR;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1966614
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-eqz v1, :cond_0

    .line 1966615
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    iget-object v1, v1, LX/D6v;->a:LX/2oN;

    sget-object v2, LX/2oN;->VIDEO_AD:LX/2oN;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1966616
    :cond_0
    return v0
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 1966610
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-nez v0, :cond_1

    .line 1966611
    :cond_0
    :goto_0
    return-void

    .line 1966612
    :cond_1
    sget-object v0, LX/04g;->BY_REPORTING_FLOW:LX/04g;

    if-eq p1, v0, :cond_0

    .line 1966613
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 1966609
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1966601
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1966602
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1966603
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->o:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->b(LX/3Gr;)V

    .line 1966604
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 1966605
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1966606
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1966607
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    .line 1966608
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1966577
    const v0, 0x7f030981

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 1966600
    const v0, 0x7f030982

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 1966599
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1966578
    const v0, 0x7f0d0553

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->t:Landroid/view/View;

    .line 1966579
    const v0, 0x7f0d1850

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966580
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966581
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 1966582
    :goto_0
    new-instance v1, LX/7LN;

    invoke-direct {v1}, LX/7LN;-><init>()V

    .line 1966583
    iput-object v0, v1, LX/7LN;->a:LX/04D;

    .line 1966584
    move-object v0, v1

    .line 1966585
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1966586
    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v3, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1966587
    new-instance v2, LX/D78;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/D78;-><init>(Landroid/content/Context;)V

    .line 1966588
    sget-object v3, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    .line 1966589
    iput-object v3, v2, LX/D78;->b:LX/3H0;

    .line 1966590
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v3, LX/D7D;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, LX/D7D;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1966591
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1966592
    new-instance v2, LX/7MZ;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7MZ;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1966593
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1966594
    invoke-virtual {v0, v1}, LX/7LN;->a(Ljava/util/List;)LX/7LN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, LX/7LN;->a(Lcom/facebook/video/player/RichVideoPlayer;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966595
    new-instance v0, LX/D6x;

    invoke-direct {v0, p0}, LX/D6x;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->v:LX/D6x;

    .line 1966596
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->v:LX/D6x;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1966597
    return-void

    .line 1966598
    :cond_1
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_0
.end method
