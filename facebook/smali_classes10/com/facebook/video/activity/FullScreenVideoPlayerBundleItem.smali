.class public Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:Lcom/facebook/graphql/model/GraphQLImage;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1961363
    new-instance v0, LX/D3s;

    invoke-direct {v0}, LX/D3s;-><init>()V

    sput-object v0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;ILcom/facebook/graphql/model/GraphQLImage;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1961364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961365
    iput-object p1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->a:Landroid/net/Uri;

    .line 1961366
    iput p2, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->b:I

    .line 1961367
    iput-object p3, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1961368
    iput-object p4, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->d:Ljava/lang/String;

    .line 1961369
    iput-object p5, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1961370
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1961371
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1961372
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1961373
    iget v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1961374
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1961375
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1961376
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1961377
    return-void
.end method
