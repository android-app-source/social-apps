.class public Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field private D:LX/0kL;

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C1d;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/AVV;

.field public G:LX/Abd;

.field public H:Z

.field public I:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final p:LX/394;

.field private final q:LX/D3q;

.field private r:LX/0hE;

.field private s:LX/395;

.field private t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/7KR;

.field private w:LX/0iI;

.field private x:LX/3Q3;

.field private y:Z

.field private z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1961281
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1961282
    new-instance v0, LX/D3p;

    invoke-direct {v0, p0}, LX/D3p;-><init>(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    iput-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->p:LX/394;

    .line 1961283
    new-instance v0, LX/D3q;

    invoke-direct {v0, p0}, LX/D3q;-><init>(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    iput-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->q:LX/D3q;

    .line 1961284
    iput-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    .line 1961285
    iput-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->H:Z

    return-void
.end method

.method public static a(Landroid/content/Context;LX/04D;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1961286
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1961287
    const-string v1, "video_player_origin"

    invoke-virtual {p1}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1961288
    return-object v0
.end method

.method private static a(Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 2

    .prologue
    .line 1961289
    new-instance v0, LX/2oI;

    invoke-direct {v0}, LX/2oI;-><init>()V

    .line 1961290
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 1961291
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1961292
    iput-object v1, v0, LX/2oI;->aT:Ljava/lang/String;

    .line 1961293
    move-object v0, v0

    .line 1961294
    iget v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->b:I

    move v1, v1

    .line 1961295
    iput v1, v0, LX/2oI;->aS:I

    .line 1961296
    move-object v0, v0

    .line 1961297
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->c:Lcom/facebook/graphql/model/GraphQLImage;

    move-object v1, v1

    .line 1961298
    iput-object v1, v0, LX/2oI;->bO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1961299
    move-object v0, v0

    .line 1961300
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1961301
    iput-object v1, v0, LX/2oI;->N:Ljava/lang/String;

    .line 1961302
    move-object v0, v0

    .line 1961303
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;->e:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1961304
    iput-object v1, v0, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1961305
    move-object v0, v0

    .line 1961306
    invoke-virtual {v0}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLImage;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1961307
    invoke-direct {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->n()LX/04D;

    move-result-object v6

    .line 1961308
    if-eqz p2, :cond_0

    .line 1961309
    invoke-static {p2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    .line 1961310
    :goto_0
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1961311
    new-instance v0, LX/395;

    new-instance v1, LX/0AV;

    invoke-direct {v1, p3}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v1

    new-instance v4, LX/0AW;

    invoke-direct {v4, v2}, LX/0AW;-><init>(LX/162;)V

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    .line 1961312
    iput-object v2, v4, LX/0AW;->b:LX/04g;

    .line 1961313
    move-object v2, v4

    .line 1961314
    const/4 v4, 0x0

    .line 1961315
    iput-boolean v4, v2, LX/0AW;->d:Z

    .line 1961316
    move-object v2, v2

    .line 1961317
    invoke-virtual {v2}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->s:LX/395;

    .line 1961318
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->s:LX/395;

    invoke-virtual {v0, v6}, LX/395;->a(LX/04D;)LX/395;

    .line 1961319
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->p:LX/394;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 1961320
    return-void

    .line 1961321
    :cond_0
    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    goto :goto_0
.end method

.method private a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Or;LX/7KR;LX/0iI;LX/3Q3;LX/0Ot;LX/0kL;LX/0Ot;LX/0Ot;LX/0Ot;LX/AVV;LX/Abd;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/7KR;",
            "LX/0iI;",
            "LX/3Q3;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0kL;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/C1d;",
            ">;",
            "LX/AVV;",
            "LX/Abd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961325
    iput-object p1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1961326
    iput-object p2, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->u:LX/0Or;

    .line 1961327
    iput-object p3, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->v:LX/7KR;

    .line 1961328
    iput-object p4, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->w:LX/0iI;

    .line 1961329
    iput-object p5, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->x:LX/3Q3;

    .line 1961330
    iput-object p6, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->z:LX/0Ot;

    .line 1961331
    iput-object p7, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->D:LX/0kL;

    .line 1961332
    iput-object p8, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->A:LX/0Ot;

    .line 1961333
    iput-object p9, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->B:LX/0Ot;

    .line 1961334
    iput-object p10, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->E:LX/0Ot;

    .line 1961335
    iput-object p11, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->F:LX/AVV;

    .line 1961336
    iput-object p12, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->G:LX/Abd;

    .line 1961337
    return-void
.end method

.method public static a(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V
    .locals 3

    .prologue
    .line 1961322
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->D:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080d5f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1961323
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->onBackPressed()V

    .line 1961324
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1961227
    instance-of v0, p1, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;

    if-eqz v0, :cond_2

    .line 1961228
    check-cast p1, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;

    .line 1961229
    invoke-static {p1}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1961230
    :goto_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1961231
    if-nez v2, :cond_0

    .line 1961232
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1961233
    :cond_0
    const/4 v5, 0x0

    .line 1961234
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1961235
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1961236
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 1961237
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLImage;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1961238
    return-void

    .line 1961239
    :cond_2
    instance-of v0, p1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    if-eqz v0, :cond_3

    .line 1961240
    check-cast p1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-static {p1}, LX/5hK;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    goto :goto_0

    .line 1961241
    :cond_3
    check-cast p1, Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v4, p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    invoke-static {v12}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const/16 v2, 0xc

    invoke-static {v12, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v12}, LX/7KR;->a(LX/0QB;)LX/7KR;

    move-result-object v3

    check-cast v3, LX/7KR;

    invoke-static {v12}, LX/0iI;->a(LX/0QB;)LX/0iI;

    move-result-object v4

    check-cast v4, LX/0iI;

    invoke-static {v12}, LX/3Q3;->b(LX/0QB;)LX/3Q3;

    move-result-object v5

    check-cast v5, LX/3Q3;

    const/16 v6, 0x455

    invoke-static {v12, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v12}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    const/16 v8, 0xc49

    invoke-static {v12, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1364

    invoke-static {v12, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1e91

    invoke-static {v12, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v12}, LX/AVV;->a(LX/0QB;)LX/AVV;

    move-result-object v11

    check-cast v11, LX/AVV;

    invoke-static {v12}, LX/Abd;->a(LX/0QB;)LX/Abd;

    move-result-object v12

    check-cast v12, LX/Abd;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Or;LX/7KR;LX/0iI;LX/3Q3;LX/0Ot;LX/0kL;LX/0Ot;LX/0Ot;LX/0Ot;LX/AVV;LX/Abd;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6

    .prologue
    .line 1961339
    if-nez p1, :cond_1

    .line 1961340
    invoke-static {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    .line 1961341
    :cond_0
    :goto_0
    return-void

    .line 1961342
    :cond_1
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1961343
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1961344
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    goto :goto_0

    .line 1961345
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1961346
    if-nez v4, :cond_4

    .line 1961347
    invoke-static {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    goto :goto_0

    .line 1961348
    :cond_4
    invoke-static {v4}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLVideo;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->H:Z

    .line 1961349
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1961350
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLImage;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1961351
    iget-boolean v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    if-nez v0, :cond_0

    .line 1961352
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->s:LX/395;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/395;)V

    .line 1961353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 1961338
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    return v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 1961270
    invoke-direct {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->m()Z

    move-result v1

    .line 1961271
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v2, LX/0ax;->hJ:Ljava/lang/String;

    invoke-interface {v0, p0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1961272
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1961273
    const-string v0, "is_from_push_notification"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1961274
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1961275
    return-void
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1961276
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1961277
    if-nez v1, :cond_1

    .line 1961278
    :cond_0
    :goto_0
    return v0

    .line 1961279
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notification_launch_source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1961280
    const-string v2, "source_system_tray"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "source_lockscreen"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private n()LX/04D;
    .locals 2

    .prologue
    .line 1961186
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "video_player_origin"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/04D;->asPlayerOrigin(Ljava/lang/String;)LX/04D;

    move-result-object v0

    .line 1961187
    sget-object v1, LX/04D;->PERMALINK:LX/04D;

    if-ne v0, v1, :cond_0

    .line 1961188
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notification_launch_source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3In;->a(Ljava/lang/String;)LX/04D;

    move-result-object v0

    .line 1961189
    :cond_0
    return-object v0
.end method

.method private o()V
    .locals 7

    .prologue
    .line 1961190
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "video_notification_story_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1961191
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "video_notification_story_cache_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1961192
    iget-object v2, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->x:LX/3Q3;

    new-instance v3, LX/D3r;

    invoke-direct {v3, p0}, LX/D3r;-><init>(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V

    .line 1961193
    const/4 v4, 0x0

    .line 1961194
    iget-object v5, v2, LX/3Q3;->f:LX/0xX;

    sget-object v6, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v5, v6}, LX/0xX;->a(LX/1vy;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v2, LX/3Q3;->f:LX/0xX;

    sget-object v6, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    invoke-virtual {v5, v6}, LX/0xX;->a(LX/1vy;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1961195
    iget-object v4, v2, LX/3Q3;->g:LX/3Q4;

    invoke-virtual {v4, v0}, LX/3Q4;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1961196
    :cond_0
    if-nez v4, :cond_1

    .line 1961197
    iget-object v4, v2, LX/3Q3;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v4, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1961198
    :cond_1
    invoke-static {v4}, LX/3Q3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1961199
    if-eqz v4, :cond_2

    .line 1961200
    invoke-interface {v3, v4}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1961201
    :goto_0
    return-void

    .line 1961202
    :cond_2
    iget-object v4, v2, LX/3Q3;->e:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetch_single_notification_from_db "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, LX/3Q3;->d:LX/1rn;

    invoke-virtual {v6, v0, v1}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p0, LX/D3t;

    invoke-direct {p0, v2, v3, v0}, LX/D3t;-><init>(LX/3Q3;LX/0Ve;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1961203
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1961204
    invoke-static {p0, p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1961205
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1961206
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "video_graphql_object"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "video_notification_story_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    const-string v3, "Extras of the intent was expected to hold the video object or the id of the notification that contains a video story"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1961207
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1961208
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->v:LX/7KR;

    if-eqz v0, :cond_1

    .line 1961209
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->v:LX/7KR;

    const-class v3, LX/7KS;

    iget-object v4, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->q:LX/D3q;

    invoke-virtual {v0, v3, v4}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 1961210
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->w:LX/0iI;

    invoke-virtual {v0, p0}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    .line 1961211
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "video_player_allow_looping"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1961212
    iget-object v3, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v3, v0}, LX/0hE;->setAllowLooping(Z)V

    .line 1961213
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "video_graphql_object"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->C:Z

    .line 1961214
    iget-boolean v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->C:Z

    if-eqz v0, :cond_4

    .line 1961215
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "video_graphql_object"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1961216
    invoke-direct {p0, v0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Ljava/lang/Object;)V

    .line 1961217
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 1961218
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1961219
    goto :goto_1

    .line 1961220
    :cond_4
    invoke-direct {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->o()V

    goto :goto_2
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1961221
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1961222
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1961223
    :cond_0
    :goto_0
    return-void

    .line 1961224
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1961225
    :pswitch_0
    const-string v0, "is_uploading_media"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1961226
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->t:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 1961242
    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1961243
    iget-object v0, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    .line 1961244
    sget-object v0, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "target_tab_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1961245
    invoke-direct {p0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->l()V

    .line 1961246
    :goto_1
    return-void

    .line 1961247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1961248
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x157c6bdc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961249
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1961250
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->v:LX/7KR;

    if-eqz v1, :cond_0

    .line 1961251
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->v:LX/7KR;

    const-class v2, LX/7KS;

    iget-object v3, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->q:LX/D3q;

    invoke-virtual {v1, v2, v3}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 1961252
    :cond_0
    const/16 v1, 0x23

    const v2, -0x30cf2fad

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x60c0ed5b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961253
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1961254
    iget-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    if-eqz v1, :cond_0

    .line 1961255
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v1}, LX/0hE;->g()V

    .line 1961256
    :cond_0
    const/16 v1, 0x23

    const v2, 0x31848f6d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x765b7f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961257
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1961258
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v1}, LX/0hE;->f()V

    .line 1961259
    iget-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->C:Z

    if-eqz v1, :cond_0

    .line 1961260
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    iget-object v2, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->s:LX/395;

    invoke-interface {v1, v2}, LX/0hE;->a(LX/395;)V

    .line 1961261
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    .line 1961262
    :cond_0
    const/16 v1, 0x23

    const v2, -0x2f584b56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3b78363e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961263
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1961264
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v1}, LX/0hE;->e()V

    .line 1961265
    const/16 v1, 0x23

    const v2, -0x122f4e8e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x4ecc93fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1961266
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1961267
    iget-boolean v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->y:Z

    if-eqz v1, :cond_0

    .line 1961268
    iget-object v1, p0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->r:LX/0hE;

    invoke-interface {v1}, LX/0hE;->h()V

    .line 1961269
    :cond_0
    const/16 v1, 0x23

    const v2, 0x952e5d2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
