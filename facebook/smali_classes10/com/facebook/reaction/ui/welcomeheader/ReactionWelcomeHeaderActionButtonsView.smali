.class public Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1vi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/E9A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/E1i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/961;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field private g:LX/0b2;

.field private h:LX/0wM;

.field public i:LX/2ja;

.field private j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

.field private l:Landroid/support/v4/app/Fragment;

.field private m:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private n:LX/0b2;

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2084125
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2084126
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b()V

    .line 2084127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2084122
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2084123
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b()V

    .line 2084124
    return-void
.end method

.method private static a(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;LX/1Kf;LX/1vi;LX/E9A;LX/E1i;LX/961;)V
    .locals 0

    .prologue
    .line 2084121
    iput-object p1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a:LX/1Kf;

    iput-object p2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    iput-object p3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    iput-object p4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->d:LX/E1i;

    iput-object p5, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->e:LX/961;

    return-void
.end method

.method public static a(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const v3, -0xa76f01

    .line 2084113
    if-eqz p1, :cond_0

    .line 2084114
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v1, 0x7f081781

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 2084115
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTextColor(I)V

    .line 2084116
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->h:LX/0wM;

    const v2, 0x7f0208fa

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2084117
    :goto_0
    return-void

    .line 2084118
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v1, 0x7f08103f

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 2084119
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTextColor(I)V

    .line 2084120
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->h:LX/0wM;

    const v2, 0x7f0208fa

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-static {v5}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {v5}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v2

    check-cast v2, LX/1vi;

    new-instance p0, LX/E9A;

    invoke-static {v5}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v5}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-direct {p0, v3, v4}, LX/E9A;-><init>(LX/0Sh;LX/2dj;)V

    move-object v3, p0

    check-cast v3, LX/E9A;

    invoke-static {v5}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v4

    check-cast v4, LX/E1i;

    invoke-static {v5}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v5

    check-cast v5, LX/961;

    invoke-static/range {v0 .. v5}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;LX/1Kf;LX/1vi;LX/E9A;LX/E1i;LX/961;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2084052
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->h:LX/0wM;

    .line 2084053
    const v0, 0x7f031182

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2084054
    const-class v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-static {v0, p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2084055
    new-instance v0, LX/E9D;

    invoke-direct {v0, p0}, LX/E9D;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->n:LX/0b2;

    .line 2084056
    new-instance v0, LX/E9E;

    invoke-direct {v0, p0}, LX/E9E;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->g:LX/0b2;

    .line 2084057
    return-void
.end method

.method public static c$redex0(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2084108
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/Cfl;

    move-result-object v0

    .line 2084109
    iget-object v1, v0, LX/Cfl;->d:Landroid/content/Intent;

    if-nez v1, :cond_0

    .line 2084110
    :goto_0
    return-void

    .line 2084111
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a:LX/1Kf;

    iget-object v0, v0, LX/Cfl;->d:Landroid/content/Intent;

    const-string v2, "composer_configuration"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    const/16 v2, 0x6dc

    iget-object v3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->l:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v4, v0, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2084112
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Cfd;->HEADER_CHECK_IN:LX/Cfd;

    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;LX/Cfd;)V

    goto :goto_0
.end method

.method public static d$redex0(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V
    .locals 4

    .prologue
    .line 2084100
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    if-eqz v0, :cond_0

    .line 2084101
    new-instance v0, LX/5OM;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2084102
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2084103
    const v2, 0x7f081782

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v3, LX/E9H;

    invoke-direct {v3, p0}, LX/E9H;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2084104
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->getFollowStateTitle()I

    move-result v2

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    new-instance v2, LX/E9I;

    invoke-direct {v2, p0}, LX/E9I;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2084105
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2084106
    :goto_0
    return-void

    .line 2084107
    :cond_0
    invoke-static {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->e(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2084092
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    new-instance v2, LX/CgR;

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    iget-object v4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    invoke-virtual {v4}, LX/2ja;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, LX/CgR;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2084093
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Cfd;->HEADER_LIKE_TAP:LX/Cfd;

    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;LX/Cfd;)V

    .line 2084094
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2084095
    :goto_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    new-instance v2, LX/Cg6;

    iget-object v4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    invoke-virtual {v4}, LX/2ja;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, LX/Cg6;-><init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2084096
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->e:LX/961;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    const-string v4, "reaction_dialog"

    new-instance v9, LX/E9J;

    invoke-direct {v9, p0}, LX/E9J;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2084097
    return-void

    .line 2084098
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2084099
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_1
.end method

.method private getFollowStateTitle()I
    .locals 2

    .prologue
    .line 2084089
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    .line 2084090
    iget-object p0, v1, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v1, p0

    .line 2084091
    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f081793

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f081792

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;Landroid/support/v4/app/Fragment;Ljava/lang/String;LX/2ja;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x8

    .line 2084069
    iput-object p4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    .line 2084070
    iput-object p1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 2084071
    iput-object p2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->l:Landroid/support/v4/app/Fragment;

    .line 2084072
    iput-object p3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->m:Ljava/lang/String;

    .line 2084073
    iput-boolean p5, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    .line 2084074
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    .line 2084075
    iput-object p6, v0, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2084076
    const v0, 0x7f0d2930

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2084077
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2084078
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    invoke-static {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;Z)V

    .line 2084079
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v1, LX/E9F;

    invoke-direct {v1, p0}, LX/E9F;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084080
    :goto_0
    const v0, 0x7f0d2931

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2084081
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->m:Ljava/lang/String;

    invoke-static {v0}, LX/2s8;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2084082
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2084083
    :goto_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 2084084
    invoke-virtual {p0, v4}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->setVisibility(I)V

    .line 2084085
    :cond_1
    return-void

    .line 2084086
    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    goto :goto_0

    .line 2084087
    :cond_3
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->h:LX/0wM;

    const v2, 0x7f020964

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2084088
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->f:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v1, LX/E9G;

    invoke-direct {v1, p0}, LX/E9G;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public getFollowSubscribeStatus()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 2084066
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    .line 2084067
    iget-object p0, v0, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v0, p0

    .line 2084068
    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5b41a32d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2084062
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onAttachedToWindow()V

    .line 2084063
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->g:LX/0b2;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2084064
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->n:LX/0b2;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2084065
    const/16 v1, 0x2d

    const v2, -0x43cb917a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5306eb20

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2084058
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 2084059
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->g:LX/0b2;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2084060
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->n:LX/0b2;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2084061
    const/16 v1, 0x2d

    const v2, 0x26e815ef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
