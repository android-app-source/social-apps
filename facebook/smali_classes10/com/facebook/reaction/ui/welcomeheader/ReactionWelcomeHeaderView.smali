.class public Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;
.super LX/E8t;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private c:LX/2ja;

.field private d:Z

.field public e:Z

.field private f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

.field public g:Z

.field private h:F

.field private i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field public q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

.field private r:Landroid/widget/LinearLayout;

.field private s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private t:Lcom/facebook/resources/ui/FbTextView;

.field private u:Lcom/facebook/resources/ui/FbTextView;

.field private v:Lcom/facebook/resources/ui/FbTextView;

.field private w:Landroid/support/v4/app/Fragment;

.field private x:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2084299
    const-class v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2084295
    invoke-direct {p0, p1}, LX/E8t;-><init>(Landroid/content/Context;)V

    .line 2084296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    .line 2084297
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e()V

    .line 2084298
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2084291
    invoke-direct {p0, p1, p2}, LX/E8t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2084292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    .line 2084293
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e()V

    .line 2084294
    return-void
.end method

.method private static a(FF)F
    .locals 2

    .prologue
    .line 2084289
    neg-float v0, p1

    div-float/2addr v0, p0

    .line 2084290
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private static a(FFLandroid/view/View;)V
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084285
    if-nez p2, :cond_0

    .line 2084286
    :goto_0
    return-void

    .line 2084287
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FF)F

    move-result v0

    .line 2084288
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2084282
    const v0, 0x7f0d2937

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    .line 2084283
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    iget-object v4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->aq_()Z

    move-result v5

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;Landroid/support/v4/app/Fragment;Ljava/lang/String;LX/2ja;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2084284
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084281
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(F)V
    .locals 1

    .prologue
    .line 2084277
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 2084278
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 2084279
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 2084280
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2084275
    const v0, 0x7f031183

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2084276
    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 2084145
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    if-nez v0, :cond_2

    .line 2084146
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2084147
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2084148
    :cond_1
    :goto_0
    return-void

    .line 2084149
    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_3

    .line 2084150
    const v0, 0x7f0d2936

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2084151
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2084152
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->g:Z

    .line 2084153
    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->w:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->x:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    .line 2084154
    iput-object v1, v0, LX/E8q;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 2084155
    iput-object v2, v0, LX/E8q;->h:Landroid/support/v4/app/Fragment;

    .line 2084156
    iput-object v3, v0, LX/E8q;->i:Ljava/lang/String;

    .line 2084157
    iput-object v4, v0, LX/E8q;->j:LX/2ja;

    .line 2084158
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/E9K;

    invoke-direct {v1, p0}, LX/E9K;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2084270
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getHeaderCoverPhotoUri()Ljava/lang/String;

    move-result-object v1

    .line 2084271
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084272
    :goto_0
    return-void

    .line 2084273
    :cond_0
    const v0, 0x7f0d2932

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2084274
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method private getHeaderCoverPhotoUri()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2084264
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->e()LX/1U8;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->e()LX/1U8;

    move-result-object v0

    invoke-interface {v0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->e()LX/1U8;

    move-result-object v0

    invoke-interface {v0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2084265
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->e()LX/1U8;

    move-result-object v0

    invoke-interface {v0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 2084266
    :goto_0
    return-object v0

    .line 2084267
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2084268
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2084269
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2084248
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 2084249
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->a()LX/174;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->a()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->b()LX/174;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->b()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2084250
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    .line 2084251
    :cond_0
    :goto_1
    return v0

    .line 2084252
    :cond_1
    new-instance v1, LX/E9C;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/E9C;-><init>(Landroid/content/Context;)V

    .line 2084253
    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 2084254
    iget-object v3, v1, LX/E9C;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->a()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084255
    iget-object v3, v1, LX/E9C;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->b()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084256
    iget-object v3, v1, LX/E9C;->b:LX/CeT;

    invoke-virtual {v3}, LX/CeT;->a()Ljava/lang/String;

    move-result-object v3

    .line 2084257
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2084258
    iget-object v3, v1, LX/E9C;->e:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2084259
    :goto_2
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2084260
    iget-object v3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2084261
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2084262
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2084263
    :cond_3
    iget-object v4, v1, LX/E9C;->e:Landroid/widget/TextView;

    new-instance v2, LX/E9B;

    invoke-direct {v2, v1, v3}, LX/E9B;-><init>(LX/E9C;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2084245
    const v0, 0x7f0d2934

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 2084246
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->v:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084247
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2084239
    const v0, 0x7f0d2935

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 2084240
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->gR_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->gR_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2084241
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->u:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->gR_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084242
    :cond_0
    :goto_0
    return-void

    .line 2084243
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->d()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->d()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2084244
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->u:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->d()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static k(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;)V
    .locals 9

    .prologue
    .line 2084217
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Cfd;->HEADER_SEE_LESS_TAP:LX/Cfd;

    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;LX/Cfd;)V

    .line 2084218
    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    if-nez v0, :cond_0

    .line 2084219
    :goto_0
    return-void

    .line 2084220
    :cond_0
    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    new-instance v1, LX/E9L;

    invoke-direct {v1, p0}, LX/E9L;-><init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;)V

    .line 2084221
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, v0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    .line 2084222
    iget-object v3, v0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v3}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildCount()I

    move-result v4

    .line 2084223
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_1

    .line 2084224
    iget-object v5, v0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v5, v3}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2084225
    invoke-virtual {v0}, LX/E8q;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f050002

    invoke-static {v6, v7}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v6

    .line 2084226
    new-instance v7, LX/E8n;

    invoke-direct {v7, v0, v5}, LX/E8n;-><init>(LX/E8q;Landroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2084227
    invoke-virtual {v6, v5}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 2084228
    add-int/lit8 v5, v4, -0x1

    sub-int/2addr v5, v3

    .line 2084229
    mul-int/lit8 v5, v5, 0x1e

    int-to-long v7, v5

    invoke-virtual {v6, v7, v8}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 2084230
    iget-object v5, v0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2084231
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2084232
    :cond_1
    iget-object v3, v0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2084233
    iget-object v3, v0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 2084234
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08222d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2084235
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->b(F)V

    .line 2084236
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->o:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    .line 2084237
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->o:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    .line 2084238
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->p:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    goto :goto_0
.end method

.method public static l(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;)V
    .locals 3

    .prologue
    .line 2084205
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Cfd;->HEADER_SEE_MORE_TAP:LX/Cfd;

    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;LX/Cfd;)V

    .line 2084206
    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    if-nez v0, :cond_0

    .line 2084207
    :goto_0
    return-void

    .line 2084208
    :cond_0
    iget-object v0, p0, LX/E8t;->a:LX/E8q;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    .line 2084209
    iget-boolean v2, v1, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    move v1, v2

    .line 2084210
    iget-object v2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-virtual {v2}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->getFollowSubscribeStatus()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/E8q;->a(ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2084211
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08222e

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2084212
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->setVisibility(I)V

    .line 2084213
    const/high16 v0, -0x40800000    # -1.0f

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->h:F

    mul-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->b(F)V

    .line 2084214
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->o:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    .line 2084215
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->o:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    .line 2084216
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->p:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 2084195
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->k:F

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-static {v0, p1, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FFLandroid/view/View;)V

    .line 2084196
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, p1, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FFLandroid/view/View;)V

    .line 2084197
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, p1, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FFLandroid/view/View;)V

    .line 2084198
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, p1, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FFLandroid/view/View;)V

    .line 2084199
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->d:Z

    if-eqz v0, :cond_0

    .line 2084200
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, p1, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FFLandroid/view/View;)V

    .line 2084201
    :cond_0
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    invoke-static {v0, p1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(FF)F

    move-result v0

    .line 2084202
    iget v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->n:F

    mul-float/2addr v0, v1

    .line 2084203
    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setScrollY(I)V

    .line 2084204
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/2ja;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084160
    invoke-static {p4}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2084161
    const v0, 0x7f0d2933

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    .line 2084162
    iput-object p3, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->x:Ljava/lang/String;

    .line 2084163
    iput-object p1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->w:Landroid/support/v4/app/Fragment;

    .line 2084164
    iput-object p4, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 2084165
    iput-object p2, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    .line 2084166
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 2084167
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    .line 2084168
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->d()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->d()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->d:Z

    .line 2084169
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->i()V

    .line 2084170
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j()V

    .line 2084171
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->w:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 2084172
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->g()V

    .line 2084173
    invoke-direct {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f()V

    .line 2084174
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->c:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    .line 2084175
    iget-object v2, v0, LX/2ja;->g:LX/2j3;

    iget-object p1, v0, LX/2ja;->l:LX/2jY;

    .line 2084176
    iget-object p2, p1, LX/2jY;->a:Ljava/lang/String;

    move-object p1, p2

    .line 2084177
    iget-object p2, v0, LX/2ja;->l:LX/2jY;

    .line 2084178
    iget-object v0, p2, LX/2jY;->b:Ljava/lang/String;

    move-object p2, v0

    .line 2084179
    iget-object p3, v2, LX/2j3;->a:LX/0Zb;

    sget-object p4, LX/Cff;->REACTION_HEADER_DISPLAYED:LX/Cff;

    const-string v0, "reaction_overlay"

    invoke-static {p4, p1, v0, p2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p4

    const-string v0, "place_id"

    invoke-virtual {p4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p4

    invoke-interface {p3, p4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2084180
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1666

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->k:F

    .line 2084181
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b1668

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    .line 2084182
    iget v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->l:F

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->m:F

    .line 2084183
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->d:Z

    if-eqz v0, :cond_3

    .line 2084184
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0b1665

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    .line 2084185
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    if-eqz v0, :cond_5

    const v0, 0x7f0b166a

    :goto_4
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->n:F

    .line 2084186
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b166d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->h:F

    .line 2084187
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b166b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->o:F

    .line 2084188
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b166c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->p:F

    .line 2084189
    return-void

    .line 2084190
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2084191
    :cond_1
    const v0, 0x7f0b1667

    goto :goto_1

    .line 2084192
    :cond_2
    const v0, 0x7f0b1664

    goto :goto_2

    .line 2084193
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0b1663

    :goto_5
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->j:F

    goto :goto_3

    :cond_4
    const v0, 0x7f0b1662

    goto :goto_5

    .line 2084194
    :cond_5
    const v0, 0x7f0b1669

    goto :goto_4
.end method

.method public getContentViewHeight()I
    .locals 1

    .prologue
    .line 2084159
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method
