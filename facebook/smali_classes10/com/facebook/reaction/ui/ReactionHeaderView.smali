.class public Lcom/facebook/reaction/ui/ReactionHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/widget/ImageButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/0wM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:LX/E6T;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2080130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2080222
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080223
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    .line 2080224
    const v0, 0x7f031167

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2080225
    const-class v0, Lcom/facebook/reaction/ui/ReactionHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2080226
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/resources/ui/FbTextView;
    .locals 3

    .prologue
    .line 2080219
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031168

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2080220
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080221
    return-object v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2080213
    invoke-static {p2}, LX/2s8;->s(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2080214
    new-instance v0, LX/E6T;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E6T;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->g:LX/E6T;

    .line 2080215
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->g:LX/E6T;

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Landroid/view/View;)V

    .line 2080216
    :cond_0
    :goto_0
    return-void

    .line 2080217
    :cond_1
    if-eqz p1, :cond_0

    const-string v0, "place_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2080218
    const-string v0, "place_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->setPlaceNameTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x2

    .line 2080206
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2080207
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2080208
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1622

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 2080209
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2080210
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080211
    invoke-virtual {p0, p1}, Lcom/facebook/reaction/ui/ReactionHeaderView;->addView(Landroid/view/View;)V

    .line 2080212
    return-void
.end method

.method public static a(Lcom/facebook/reaction/ui/ReactionHeaderView;)V
    .locals 4

    .prologue
    .line 2080201
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 2080202
    :goto_0
    return-void

    .line 2080203
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    if-eqz v0, :cond_1

    const v0, -0xa76f01

    .line 2080204
    :goto_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getGlyphColorizer()LX/0wM;

    move-result-object v2

    const v3, 0x7f0209e1

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2080205
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/reaction/ui/ReactionHeaderView;LX/0Ot;LX/0qn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/ui/ReactionHeaderView;",
            "LX/0Ot",
            "<",
            "LX/1vi;",
            ">;",
            "LX/0qn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2080200
    iput-object p1, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->b:LX/0qn;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/reaction/ui/ReactionHeaderView;

    const/16 v1, 0x106f

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-static {p0, v1, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Lcom/facebook/reaction/ui/ReactionHeaderView;LX/0Ot;LX/0qn;)V

    return-void
.end method

.method private static b()Landroid/widget/RelativeLayout$LayoutParams;
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 2080227
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2080228
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2080229
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 2080230
    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2080231
    :cond_0
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2080232
    return-object v0
.end method

.method private b(LX/2jY;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2080195
    new-instance v0, LX/E6L;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E6L;-><init>(Landroid/content/Context;)V

    .line 2080196
    invoke-virtual {v0, p1, p2}, LX/E6L;->a(LX/2jY;Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2080197
    :goto_0
    return-void

    .line 2080198
    :cond_0
    invoke-static {}, Lcom/facebook/reaction/ui/ReactionHeaderView;->b()Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E6L;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080199
    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private c(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2080193
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2080194
    return-void
.end method

.method private getGlyphColorizer()LX/0wM;
    .locals 2

    .prologue
    .line 2080190
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->d:LX/0wM;

    if-nez v0, :cond_0

    .line 2080191
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->d:LX/0wM;

    .line 2080192
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->d:LX/0wM;

    return-object v0
.end method

.method private setPlaceNameTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2080186
    invoke-direct {p0, p1}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Ljava/lang/String;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2080187
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->setPlaceNameTitleAlpha(F)V

    .line 2080188
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Landroid/view/View;)V

    .line 2080189
    return-void
.end method

.method private setupPlaceTipsExplorerMenuButton(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080177
    if-eqz p1, :cond_0

    const-string v0, "place_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "favorite_page"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2080178
    :cond_0
    :goto_0
    return-void

    .line 2080179
    :cond_1
    const-string v0, "favorite_page"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    .line 2080180
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    .line 2080181
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 2080182
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/facebook/reaction/ui/ReactionHeaderView;->b()Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080183
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    new-instance v1, LX/E6R;

    invoke-direct {v1, p0, p1}, LX/E6R;-><init>(Lcom/facebook/reaction/ui/ReactionHeaderView;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080184
    invoke-static {p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Lcom/facebook/reaction/ui/ReactionHeaderView;)V

    .line 2080185
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->c:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2jY;Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 2080169
    iget-object v0, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2080170
    iget-object v1, p1, LX/2jY;->x:Landroid/os/Bundle;

    move-object v1, v1

    .line 2080171
    invoke-direct {p0, v1, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2080172
    invoke-static {v0}, LX/2s8;->g(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2080173
    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/ui/ReactionHeaderView;->b(LX/2jY;Landroid/support/v4/app/Fragment;)V

    .line 2080174
    :cond_0
    :goto_0
    return-void

    .line 2080175
    :cond_1
    const-string v2, "ANDROID_PLACE_TIPS_EXPLORER"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2080176
    invoke-direct {p0, v1}, Lcom/facebook/reaction/ui/ReactionHeaderView;->setupPlaceTipsExplorerMenuButton(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 2
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080138
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->g:LX/E6T;

    if-eqz v0, :cond_1

    .line 2080139
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->g:LX/E6T;

    .line 2080140
    if-nez p2, :cond_6

    .line 2080141
    invoke-static {v0}, LX/E6T;->b(LX/E6T;)V

    .line 2080142
    :cond_0
    :goto_0
    return-void

    .line 2080143
    :cond_1
    if-nez p2, :cond_2

    .line 2080144
    const v0, 0x7f082226

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->c(I)V

    goto :goto_0

    .line 2080145
    :cond_2
    const-string v0, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2080146
    const v0, 0x7f082225

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->c(I)V

    goto :goto_0

    .line 2080147
    :cond_3
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->b:LX/0qn;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_4

    .line 2080148
    const v0, 0x7f082226

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->c(I)V

    goto :goto_0

    .line 2080149
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2080150
    const v0, 0x7f082225

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->c(I)V

    goto :goto_0

    .line 2080151
    :cond_5
    const v0, 0x7f082223

    invoke-direct {p0, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->c(I)V

    goto :goto_0

    .line 2080152
    :cond_6
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2080153
    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2080154
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v0, v1}, LX/E6T;->a(LX/E6T;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 2080155
    :cond_7
    iget-object v1, v0, LX/E6T;->a:LX/0qn;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, p0, :cond_8

    .line 2080156
    invoke-static {v0}, LX/E6T;->b(LX/E6T;)V

    goto :goto_0

    .line 2080157
    :cond_8
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2080158
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v0, v1}, LX/E6T;->a(LX/E6T;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 2080159
    :cond_9
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2080160
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    const/4 p2, 0x0

    .line 2080161
    if-eqz v1, :cond_b

    iget-object p0, v0, LX/E6T;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz p0, :cond_b

    .line 2080162
    const p0, 0x7f0206f3

    const p1, 0x7f082224

    invoke-static {v0, p0, p1, p2}, LX/E6T;->a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V

    .line 2080163
    iget-object p0, v0, LX/E6T;->c:Landroid/view/View$OnClickListener;

    if-nez p0, :cond_a

    .line 2080164
    new-instance p0, LX/E6S;

    invoke-direct {p0, v0}, LX/E6S;-><init>(LX/E6T;)V

    iput-object p0, v0, LX/E6T;->c:Landroid/view/View$OnClickListener;

    .line 2080165
    :cond_a
    iget-object p0, v0, LX/E6T;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p0}, LX/E6T;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080166
    :goto_1
    goto/16 :goto_0

    .line 2080167
    :cond_b
    const p0, 0x7f0215c6

    const p1, 0x7f082223

    invoke-static {v0, p0, p1, p2}, LX/E6T;->a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V

    .line 2080168
    invoke-static {v0}, LX/E6T;->c$redex0(LX/E6T;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080135
    invoke-static {p1}, LX/2s8;->s(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2080136
    :cond_0
    :goto_0
    return-void

    .line 2080137
    :cond_1
    invoke-direct {p0, p2}, Lcom/facebook/reaction/ui/ReactionHeaderView;->setPlaceNameTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setPlaceNameTitleAlpha(F)V
    .locals 1

    .prologue
    .line 2080132
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 2080133
    iget-object v0, p0, Lcom/facebook/reaction/ui/ReactionHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 2080134
    :cond_0
    return-void
.end method
