.class public Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/CfG;
.implements LX/0o8;


# static fields
.field private static final B:Ljava/lang/String;


# instance fields
.field public A:LX/11i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field public H:J

.field public I:J

.field private J:J

.field private K:J

.field private L:J

.field public M:J

.field private N:J

.field public O:J

.field public P:LX/2jY;

.field private Q:Ljava/lang/String;

.field private R:Landroid/content/Context;

.field private S:LX/195;

.field public T:LX/2ja;

.field public U:LX/0Yb;

.field public V:LX/0Yb;

.field private W:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/Cfg;",
            ">;"
        }
    .end annotation
.end field

.field public X:LX/E8m;

.field public Y:LX/1P0;

.field public Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

.field public ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

.field private ac:Landroid/view/View;

.field private ad:Landroid/view/ViewStub;

.field private ae:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Amz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cga;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/2j3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/E94;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2083062
    const-class v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->B:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2083064
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2083065
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->C:Z

    .line 2083066
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->F:Z

    .line 2083067
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->G:Z

    .line 2083068
    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ae:Landroid/view/View;

    .line 2083069
    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->af:Landroid/view/ViewGroup;

    .line 2083070
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->E:Z

    .line 2083071
    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->N:J

    .line 2083072
    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->O:J

    .line 2083073
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 2083074
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 2083075
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->F:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2083076
    :cond_0
    return-void

    .line 2083077
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083078
    if-eqz p0, :cond_0

    .line 2083079
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2083080
    :cond_0
    return-void

    .line 2083081
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;IZF)V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2083082
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v1, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 2083083
    if-eqz p2, :cond_0

    if-eqz v1, :cond_0

    .line 2083084
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    .line 2083085
    sub-float v1, p3, v1

    div-float/2addr v1, p3

    .line 2083086
    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2083087
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->F:Z

    if-nez v1, :cond_1

    .line 2083088
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2083089
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

    invoke-virtual {v1, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->setPlaceNameTitleAlpha(F)V

    .line 2083090
    return-void
.end method

.method private static a(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/0Xl;LX/0So;LX/Amz;LX/03V;LX/193;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2j3;LX/Cfr;LX/E94;LX/2iz;LX/3Fx;LX/11i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;",
            "LX/0Xl;",
            "LX/0So;",
            "LX/Amz;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/193;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cga;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/2j3;",
            "LX/Cfr;",
            "LX/E94;",
            "LX/2iz;",
            "LX/3Fx;",
            "LX/11i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2083091
    iput-object p1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->m:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    iput-object p3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->o:LX/Amz;

    iput-object p4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->p:LX/03V;

    iput-object p5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->q:LX/193;

    iput-object p6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->r:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->s:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->t:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->u:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p10, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->v:LX/2j3;

    iput-object p11, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->w:LX/Cfr;

    iput-object p12, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->x:LX/E94;

    iput-object p13, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    iput-object p14, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->z:LX/3Fx;

    iput-object p15, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A:LX/11i;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-static {v15}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {v15}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {v15}, LX/Amz;->a(LX/0QB;)LX/Amz;

    move-result-object v3

    check-cast v3, LX/Amz;

    invoke-static {v15}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const-class v5, LX/193;

    invoke-interface {v15, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/193;

    const/16 v6, 0x19c6

    invoke-static {v15, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xf59

    invoke-static {v15, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x314c

    invoke-static {v15, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v15}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v15}, LX/2j3;->a(LX/0QB;)LX/2j3;

    move-result-object v10

    check-cast v10, LX/2j3;

    const-class v11, LX/Cfr;

    invoke-interface {v15, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Cfr;

    const-class v12, LX/E94;

    invoke-interface {v15, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/E94;

    invoke-static {v15}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v13

    check-cast v13, LX/2iz;

    invoke-static {v15}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v14

    check-cast v14, LX/3Fx;

    invoke-static {v15}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v15

    check-cast v15, LX/11i;

    invoke-static/range {v0 .. v15}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/0Xl;LX/0So;LX/Amz;LX/03V;LX/193;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2j3;LX/Cfr;LX/E94;LX/2iz;LX/3Fx;LX/11i;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;J)J
    .locals 3

    .prologue
    .line 2083092
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->N:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->N:J

    return-wide v0
.end method

.method public static b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;F)V
    .locals 1

    .prologue
    .line 2083093
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    if-eqz v0, :cond_0

    .line 2083094
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->setAlpha(F)V

    .line 2083095
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/9qT;)V
    .locals 7
    .param p0    # Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083096
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083097
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 2083098
    :goto_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083099
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2083100
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2083101
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->v:LX/2j3;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083102
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2083103
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083104
    iget-object p0, v3, LX/2jY;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2083105
    iget-object v4, v1, LX/2j3;->a:LX/0Zb;

    sget-object v5, LX/Cff;->REACTION_OVERLAY_ERROR:LX/Cff;

    const-string v6, "reaction_overlay"

    invoke-static {v5, v3, v6, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "error"

    const-string p0, "SESSION_ID_MISMATCH"

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "error_message"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Expected: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, " Actual: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2083106
    :cond_0
    return-void

    .line 2083107
    :cond_1
    invoke-interface {p1}, LX/9qT;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p0    # Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083108
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->w:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v0, v1, v2}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    .line 2083109
    const-string v0, "SUCCESS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2083110
    iput-object p1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Q:Ljava/lang/String;

    .line 2083111
    :cond_0
    return-void
.end method

.method public static c$redex0(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V
    .locals 1

    .prologue
    .line 2083249
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ae:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Landroid/view/View;Z)V

    .line 2083250
    return-void
.end method

.method public static d(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V
    .locals 1

    .prologue
    .line 2083112
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->af:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Landroid/view/View;Z)V

    .line 2083113
    return-void
.end method

.method private k()V
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    .line 2083114
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-nez v0, :cond_3

    .line 2083115
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Q:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v2, :cond_1

    move-wide v2, v6

    :goto_0
    iget-wide v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->K:J

    iget-object v8, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v8, :cond_2

    :goto_1
    invoke-virtual/range {v0 .. v7}, LX/2ja;->a(Ljava/lang/String;JJJ)V

    .line 2083116
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2083117
    :goto_2
    return-void

    .line 2083118
    :cond_1
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083119
    iget-wide v9, v2, LX/2jY;->g:J

    move-wide v2, v9

    .line 2083120
    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v6}, LX/2jY;->r()J

    move-result-wide v6

    goto :goto_1

    .line 2083121
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->E:Z

    if-nez v0, :cond_6

    .line 2083122
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083123
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2083124
    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2083125
    invoke-static {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->u(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2083126
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->E:Z

    .line 2083127
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->I:J

    iput-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->J:J

    .line 2083128
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->C:Z

    if-eqz v0, :cond_5

    .line 2083129
    invoke-static {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->z(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2083130
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->m()V

    goto :goto_2

    .line 2083131
    :cond_6
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2ja;->e(J)V

    goto :goto_3
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2083132
    iput-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    .line 2083133
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v0, :cond_1

    .line 2083134
    const-string v0, "NO_SESSION"

    invoke-static {p0, v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2083135
    :cond_0
    :goto_0
    return-void

    .line 2083136
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083137
    iget-object v2, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v2

    .line 2083138
    invoke-static {v0}, LX/2s8;->s(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ANDROID_APP_INSTALL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 2083139
    if-eqz v0, :cond_a

    .line 2083140
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    const/4 v2, 0x0

    .line 2083141
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/2jY;->y()Z

    move-result v3

    if-nez v3, :cond_d

    .line 2083142
    :cond_3
    const-string v3, "ERROR_INVALID_RESPONSE"

    invoke-static {p0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2083143
    :goto_2
    move v0, v2

    .line 2083144
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v0, v2}, LX/E8m;->a(LX/2jY;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    .line 2083145
    :goto_4
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-eqz v0, :cond_b

    .line 2083146
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083147
    iget-object v2, v0, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object v0, v2

    .line 2083148
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083149
    iget-object v2, v0, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object v0, v2

    .line 2083150
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2083151
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083152
    iget-object v2, v0, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object v0, v2

    .line 2083153
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    .line 2083154
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083155
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2083156
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2083157
    :cond_4
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083158
    iget-object v2, v0, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object v0, v2

    .line 2083159
    invoke-static {v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2083160
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v2, LX/Cfg;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083161
    iget-object v4, v3, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2083162
    const v4, 0x19548bb8

    invoke-static {v0, v2, v3, v5, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2083163
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    if-nez v0, :cond_5

    .line 2083164
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ad:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    .line 2083165
    :cond_5
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/E8m;->a(LX/E8t;Landroid/content/Context;)V

    .line 2083166
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2083167
    iget-object v3, v0, LX/E8m;->i:LX/E8t;

    invoke-virtual {v0, v2}, LX/E8m;->a(Landroid/content/Context;)LX/E8q;

    move-result-object v4

    .line 2083168
    iput-object v4, v3, LX/E8t;->a:LX/E8q;

    .line 2083169
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-virtual {v0}, LX/E8t;->a()V

    .line 2083170
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083171
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2083172
    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083173
    iget-object v6, v4, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object v4, v6

    .line 2083174
    invoke-virtual {v0, p0, v2, v3, v4}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(Landroid/support/v4/app/Fragment;LX/2ja;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V

    .line 2083175
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setPadding(IIII)V

    .line 2083176
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v2, LX/Cfg;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083177
    iget-object v4, v3, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2083178
    const v4, 0x171ecae0

    invoke-static {v0, v2, v3, v5, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2083179
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2083180
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1620

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2083181
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2083182
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    const v2, 0x7f0a06dd

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2083183
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    .line 2083184
    iget-boolean v2, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->e:Z

    move v0, v2

    .line 2083185
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->F:Z

    .line 2083186
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A()V

    .line 2083187
    :cond_6
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2083188
    invoke-static {p0, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->d(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V

    .line 2083189
    invoke-static {p0, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c$redex0(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V

    .line 2083190
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083191
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2083192
    invoke-static {v0}, LX/2s8;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2083193
    if-eqz v0, :cond_7

    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_10

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_10

    :cond_7
    const/4 v1, 0x1

    :goto_5
    move v0, v1

    .line 2083194
    if-eqz v0, :cond_f

    .line 2083195
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->C:Z

    .line 2083196
    :goto_6
    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 2083197
    goto/16 :goto_3

    .line 2083198
    :cond_a
    const-string v0, "UNSUPPORTED_SURFACE"

    invoke-static {p0, v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2083199
    goto/16 :goto_4

    .line 2083200
    :cond_b
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Q:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2083201
    const-string v0, "NO_VALID_ADAPTERS"

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Q:Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2083202
    :cond_d
    invoke-virtual {v0}, LX/2jY;->h()LX/9qT;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/9qT;)V

    .line 2083203
    invoke-virtual {v0}, LX/2jY;->z()Z

    move-result v3

    if-nez v3, :cond_e

    .line 2083204
    const-string v3, "NO_UNITS_RETURNED"

    invoke-static {p0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2083205
    :cond_e
    const-string v2, "SUCCESS"

    invoke-static {p0, v2}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2083206
    const/4 v2, 0x1

    goto/16 :goto_2

    .line 2083207
    :cond_f
    new-instance v0, Landroid/view/animation/LayoutAnimationController;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400ab

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;)V

    .line 2083208
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 2083209
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/E8X;

    invoke-direct {v1, p0}, LX/E8X;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLayoutAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_6

    :cond_10
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private t()J
    .locals 4

    .prologue
    .line 2083210
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083211
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2083212
    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2083213
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->I:J

    .line 2083214
    :goto_0
    return-wide v0

    .line 2083215
    :cond_0
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2083216
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->H:J

    goto :goto_0

    .line 2083217
    :cond_1
    invoke-static {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->u(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2083218
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V
    .locals 4

    .prologue
    .line 2083219
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083220
    iget-object v1, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2083221
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2083222
    :cond_0
    :goto_0
    return-void

    .line 2083223
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->u:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1e0002

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083224
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2083225
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0
.end method

.method private y()V
    .locals 4

    .prologue
    .line 2083226
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1659

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2083227
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A()V

    .line 2083228
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;F)V

    .line 2083229
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/E8a;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->S:LX/195;

    invoke-direct {v2, p0, v3, v0}, LX/E8a;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/195;F)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2083230
    return-void
.end method

.method public static z(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V
    .locals 18

    .prologue
    .line 2083231
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->j()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->K:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->r()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->q()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->t()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->w()J

    move-result-wide v16

    sub-long v14, v14, v16

    invoke-virtual/range {v3 .. v15}, LX/2ja;->a(JJJJJJ)V

    .line 2083232
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2083233
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    .line 2083234
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2083235
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0f8;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2083236
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2083237
    new-instance v1, LX/E8W;

    invoke-direct {v1, p0}, LX/E8W;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2083238
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2083063
    const-string v0, "reaction_dialog"

    return-object v0
.end method

.method public final a(LX/9qT;)V
    .locals 2

    .prologue
    .line 2083239
    invoke-static {p0, p1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/9qT;)V

    .line 2083240
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2083241
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2083242
    if-nez v0, :cond_1

    .line 2083243
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->q()V

    .line 2083244
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2083245
    if-eqz v0, :cond_0

    .line 2083246
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->k()V

    .line 2083247
    :cond_0
    :goto_0
    return-void

    .line 2083248
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->a(LX/9qT;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 1
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082859
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 2082860
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2082861
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0, p1, p2}, LX/E8m;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2082862
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082863
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2082864
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2082865
    const-string v1, "close_activity_after_finish"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2082866
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2082867
    :goto_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->c()V

    .line 2082868
    if-eqz v0, :cond_0

    .line 2082869
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2082870
    :cond_0
    return-void

    .line 2082871
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082872
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2082873
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->G:Z

    return v0
.end method

.method public final kK_()V
    .locals 1

    .prologue
    .line 2082874
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->d(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V

    .line 2082875
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c$redex0(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V

    .line 2082876
    return-void
.end method

.method public final kL_()V
    .locals 1

    .prologue
    .line 2082877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->G:Z

    .line 2082878
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->q()V

    .line 2082879
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2082880
    if-eqz v0, :cond_0

    .line 2082881
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->k()V

    .line 2082882
    :cond_0
    return-void
.end method

.method public final kU_()V
    .locals 7

    .prologue
    .line 2082883
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2082884
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    .line 2082885
    iget-object v1, v0, LX/2ja;->j:LX/1vC;

    const v2, 0x1e0004

    iget-object v3, v0, LX/2ja;->l:LX/2jY;

    .line 2082886
    iget-object v4, v3, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2082887
    invoke-virtual {v1, v2, v3}, LX/1vC;->c(ILjava/lang/String;)V

    .line 2082888
    iget-object v1, v0, LX/2ja;->g:LX/2j3;

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    .line 2082889
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2082890
    iget-object v3, v0, LX/2ja;->l:LX/2jY;

    .line 2082891
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2082892
    iget v4, v0, LX/2ja;->i:I

    iget-object v5, v0, LX/2ja;->l:LX/2jY;

    .line 2082893
    iget-object v0, v5, LX/2jY;->e:Ljava/lang/String;

    move-object v5, v0

    .line 2082894
    iget-object v6, v1, LX/2j3;->a:LX/0Zb;

    sget-object p0, LX/Cff;->REACTION_PAGE_ERROR:LX/Cff;

    const-string v0, "reaction_overlay"

    invoke-static {p0, v2, v0, v3}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "page_number"

    invoke-virtual {p0, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "error"

    invoke-virtual {p0, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v6, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2082895
    return-void
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082896
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2082897
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2082898
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v0, :cond_0

    const-string v0, "NO_SESSION_ID"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082899
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2082900
    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082901
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082902
    iget-object p0, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2082903
    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 2082904
    sget-object v0, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    invoke-virtual {v0}, LX/Cfc;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_3

    if-ne p2, v1, :cond_3

    .line 2082905
    const-string v0, "reaction_unit_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2082906
    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082907
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2082908
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082909
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2082910
    const-string v1, "place_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082911
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2082912
    const-string v1, "place_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2082913
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->p:LX/03V;

    sget-object v1, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->B:Ljava/lang/String;

    const-string v2, "WRITE_REVIEW_TAP should have value for EXTRA_REACTION_UNIT_ID and PLACE_ID in the return intent"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2082914
    :cond_1
    :goto_0
    return-void

    .line 2082915
    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cga;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082916
    iget-object v2, v1, LX/2jY;->x:Landroid/os/Bundle;

    move-object v1, v2

    .line 2082917
    const-string v2, "place_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    move v1, p2

    move-object v2, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v7}, LX/Cga;->a(ILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;LX/0o8;Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_0

    .line 2082918
    :cond_3
    sget-object v0, LX/Cfc;->PLACE_TIPS_HIDE_TAP:LX/Cfc;

    invoke-virtual {v0}, LX/Cfc;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 2082919
    if-eqz p3, :cond_4

    const-string v0, "gravity_undo_hide_place_tips"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2082920
    :cond_4
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ct;

    sget-object v1, LX/2cx;->FORCE_OFF:LX/2cx;

    invoke-virtual {v0, v1}, LX/2ct;->a(LX/2cx;)LX/0bZ;

    .line 2082921
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0

    .line 2082922
    :cond_5
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    .line 2082923
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2082924
    if-eqz v0, :cond_6

    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2082925
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n()Ljava/lang/String;

    move-result-object v3

    .line 2082926
    iget-object v5, v1, LX/2iz;->g:LX/2jN;

    .line 2082927
    iget-object v1, v5, LX/2jN;->b:Ljava/util/Map;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082928
    :cond_6
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2082929
    const-string v2, "handle_composer_publish"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2082930
    if-eqz v0, :cond_7

    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2082931
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/2iz;->a(Ljava/lang/String;)V

    .line 2082932
    :cond_7
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2082933
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2082934
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->a(Landroid/content/res/Configuration;)V

    .line 2082935
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b8ce5e1

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082936
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2082937
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2082938
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->q:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "reaction_dialog_scroll_perf"

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->S:LX/195;

    .line 2082939
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->L:J

    .line 2082940
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2082941
    const-string v2, "reaction_session_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2082942
    if-nez v1, :cond_0

    .line 2082943
    const-string v1, "NO_SESSION_ID"

    invoke-static {p0, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2082944
    const/16 v1, 0x2b

    const v2, 0x6464a67f

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2082945
    :goto_0
    return-void

    .line 2082946
    :cond_0
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    invoke-virtual {v2, v1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082947
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-nez v1, :cond_1

    .line 2082948
    const-string v1, "NO_SESSION"

    invoke-static {p0, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Ljava/lang/String;)V

    .line 2082949
    const v1, 0x15ace456

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0

    .line 2082950
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A:LX/11i;

    sget-object v2, LX/Cfh;->a:LX/Cfg;

    invoke-interface {v1, v2}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    .line 2082951
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082952
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2082953
    invoke-static {v1, v2}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->R:Landroid/content/Context;

    .line 2082954
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->x:LX/E94;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->R:Landroid/content/Context;

    .line 2082955
    sget-object v3, LX/E1k;->a:LX/E1k;

    move-object v3, v3

    .line 2082956
    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->o:LX/Amz;

    invoke-virtual {v1, v2, v3, v4, p0}, LX/E94;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;)LX/E93;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    .line 2082957
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082958
    iget-object v2, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2082959
    invoke-static {v1}, LX/2s8;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2082960
    const v1, 0x7f0e08ef

    invoke-virtual {p0, v5, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2082961
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->C:Z

    .line 2082962
    :goto_1
    new-instance v1, LX/E8Y;

    invoke-direct {v1, p0}, LX/E8Y;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2082963
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->m:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v2, v3, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->U:LX/0Yb;

    .line 2082964
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->U:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 2082965
    new-instance v1, LX/E8Z;

    invoke-direct {v1, p0}, LX/E8Z;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2082966
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->m:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v2, v3, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->V:LX/0Yb;

    .line 2082967
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->V:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 2082968
    const v1, 0x7ec7f5ec

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto/16 :goto_0

    .line 2082969
    :cond_2
    const v1, 0x7f0e08f0

    invoke-virtual {p0, v5, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x13f817ca

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2082970
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->R:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 2082971
    :cond_0
    const/16 v0, 0x2b

    const v1, 0x71ddbe01

    invoke-static {v4, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-object v0, v2

    .line 2082972
    :goto_0
    return-object v0

    .line 2082973
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->R:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2082974
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v4, LX/Cfg;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082975
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2082976
    const v6, 0x5e60b1b5

    invoke-static {v1, v4, v5, v2, v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2082977
    const v1, 0x7f031166

    invoke-virtual {v0, v1, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2082978
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v4, LX/Cfg;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082979
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2082980
    const v6, -0x60e6eeda

    invoke-static {v0, v4, v5, v2, v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2082981
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v4, LX/Cfg;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082982
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2082983
    const v6, 0x7019ef73

    invoke-static {v0, v4, v5, v2, v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2082984
    const v0, 0x7f0d2909

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ac:Landroid/view/View;

    .line 2082985
    const v0, 0x7f0d290b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ui/ReactionHeaderView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

    .line 2082986
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->aa:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    invoke-virtual {v0, v4, p0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(LX/2jY;Landroid/support/v4/app/Fragment;)V

    .line 2082987
    const v0, 0x7f0d2906

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ad:Landroid/view/ViewStub;

    .line 2082988
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082989
    iget-object v4, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v4

    .line 2082990
    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v4

    .line 2082991
    if-eqz v4, :cond_2

    .line 2082992
    const v0, 0x7f0d2908

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2082993
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2082994
    const v5, 0x7f0d28e5

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ae:Landroid/view/View;

    .line 2082995
    const v5, 0x7f0d290e

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->af:Landroid/view/ViewGroup;

    .line 2082996
    const v0, 0x7f0d290f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v5, LX/E8c;

    invoke-direct {v5, p0}, LX/E8c;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2082997
    :cond_2
    const v0, 0x7f0d290c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2082998
    new-instance v5, LX/E8b;

    invoke-direct {v5, p0}, LX/E8b;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2082999
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v5, LX/Cfg;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083000
    iget-object v7, v6, LX/2jY;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2083001
    const v7, -0x159021aa

    invoke-static {v0, v5, v6, v2, v7}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2083002
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v5, LX/Cfg;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083003
    iget-object v7, v6, LX/2jY;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2083004
    const v7, -0x7e425a7e

    invoke-static {v0, v5, v6, v2, v7}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2083005
    const v0, 0x7f0d28b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2083006
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    .line 2083007
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Z:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2083008
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->q()V

    .line 2083009
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083010
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2083011
    invoke-virtual {v0, v5, p0}, LX/2iz;->a(Ljava/lang/String;LX/CfG;)V

    .line 2083012
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-eqz v0, :cond_3

    .line 2083013
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->W:LX/11o;

    sget-object v4, LX/Cfg;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083014
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2083015
    const v6, 0x55f0ff08

    invoke-static {v0, v4, v5, v2, v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2083016
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y()V

    .line 2083017
    invoke-static {p0, v8}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->c$redex0(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;Z)V

    .line 2083018
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->L:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->K:J

    .line 2083019
    const v0, -0xeed413

    invoke-static {v0, v3}, LX/02F;->f(II)V

    move-object v0, v1

    goto/16 :goto_0

    .line 2083020
    :cond_3
    if-eqz v4, :cond_4

    .line 2083021
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A:LX/11i;

    sget-object v4, LX/Cfh;->a:LX/Cfg;

    invoke-interface {v0, v4}, LX/11i;->d(LX/0Pq;)V

    .line 2083022
    iput-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Q:Ljava/lang/String;

    .line 2083023
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y()V

    .line 2083024
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->G:Z

    .line 2083025
    const v0, -0x2aff32a6

    invoke-static {v0, v3}, LX/02F;->f(II)V

    move-object v0, v1

    goto/16 :goto_0

    .line 2083026
    :cond_4
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A:LX/11i;

    sget-object v1, LX/Cfh;->a:LX/Cfg;

    invoke-interface {v0, v1}, LX/11i;->d(LX/0Pq;)V

    .line 2083027
    const v0, 0x6ebc441

    invoke-static {v0, v3}, LX/02F;->f(II)V

    move-object v0, v2

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x1164756f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2083028
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    if-eqz v1, :cond_0

    .line 2083029
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->y:LX/2iz;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083030
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2083031
    invoke-virtual {v1, v2}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2083032
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2083033
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    if-eqz v1, :cond_1

    .line 2083034
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->n()V

    .line 2083035
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->dispose()V

    .line 2083036
    :cond_1
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 2083037
    iget-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-eqz v1, :cond_2

    .line 2083038
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->J:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->N:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->O:J

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2ja;->a(JJ)V

    .line 2083039
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->A:LX/11i;

    sget-object v2, LX/Cfh;->a:LX/Cfg;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 2083040
    :cond_2
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->U:LX/0Yb;

    if-eqz v1, :cond_3

    .line 2083041
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->U:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2083042
    :cond_3
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->V:LX/0Yb;

    if-eqz v1, :cond_4

    .line 2083043
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->V:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2083044
    :cond_4
    const/16 v1, 0x2b

    const v2, 0x74dbefb7

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3246fb58

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2083045
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2083046
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cga;

    .line 2083047
    iget-object v2, v0, LX/Cga;->f:LX/1B1;

    iget-object p0, v0, LX/Cga;->e:LX/Ch5;

    invoke-virtual {v2, p0}, LX/1B1;->b(LX/0b4;)V

    .line 2083048
    const/16 v0, 0x2b

    const v2, 0x84a549d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x63bfb092

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2083049
    iget-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-eqz v1, :cond_0

    .line 2083050
    iget-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->O:J

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->I:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->O:J

    .line 2083051
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->l()V

    .line 2083052
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 2083053
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2ja;->d(J)V

    .line 2083054
    const/16 v1, 0x2b

    const v2, 0x4ab3cc2d    # 5891606.5f

    invoke-static {v8, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6541ae58

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2083055
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 2083056
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->I:J

    .line 2083057
    iget-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->G:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->D:Z

    if-nez v1, :cond_0

    .line 2083058
    const/16 v1, 0x2b

    const v2, 0x197e9600

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2083059
    :goto_0
    return-void

    .line 2083060
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->k()V

    .line 2083061
    const v1, 0x21d3060e

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method
