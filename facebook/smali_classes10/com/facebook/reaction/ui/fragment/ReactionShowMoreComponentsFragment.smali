.class public Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0o8;


# instance fields
.field public a:LX/E96;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/E95;

.field private h:LX/2ja;

.field public i:LX/1P0;

.field public j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private k:LX/2jY;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2083414
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;)LX/E95;
    .locals 8

    .prologue
    .line 2083401
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    if-eqz v0, :cond_0

    .line 2083402
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    .line 2083403
    :goto_0
    return-object v0

    .line 2083404
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->a:LX/E96;

    .line 2083405
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2083406
    const-string v2, "component_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->k:LX/2jY;

    .line 2083407
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2083408
    const-string v2, "reaction_unit_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2083409
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2083410
    const-string v2, "unit_type_token"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2083411
    new-instance v1, LX/E8j;

    invoke-direct {v1, p0}, LX/E8j;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;)V

    move-object v7, v1

    .line 2083412
    move-object v1, p1

    move-object v2, p0

    invoke-virtual/range {v0 .. v7}, LX/E96;->a(Landroid/content/Context;LX/0o8;Ljava/lang/String;LX/2jY;Ljava/lang/String;Ljava/lang/String;LX/1PY;)LX/E95;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    .line 2083413
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2083388
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;

    const-class v3, LX/E96;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/E96;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    const-class v5, LX/Cfr;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Cfr;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {v0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v7

    check-cast v7, LX/2iz;

    invoke-static {v0}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v0

    check-cast v0, LX/3Fx;

    iput-object v3, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->a:LX/E96;

    iput-object v4, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->b:LX/1Db;

    iput-object v5, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->c:LX/Cfr;

    iput-object v6, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->d:LX/0So;

    iput-object v7, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->e:LX/2iz;

    iput-object v0, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->f:LX/3Fx;

    .line 2083389
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083390
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "REACTION_SESSION_ID_TAG must be present"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2083391
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083392
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "REACTION_SURFACE_TAG must be a (non-null) Surface string"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083393
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083394
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->l:Ljava/lang/String;

    .line 2083395
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->e:LX/2iz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->k:LX/2jY;

    .line 2083396
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->c:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->k:LX/2jY;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->h:LX/2ja;

    .line 2083397
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083398
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->m:Ljava/lang/String;

    .line 2083399
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2083400
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083387
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083385
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083384
    const/4 v0, 0x0

    return-object v0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2083383
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083386
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->h:LX/2ja;

    return-object v0
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2083381
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    invoke-virtual {v0}, LX/E95;->d()V

    .line 2083382
    return-void
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083380
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2083379
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->l:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "NO_SESSION_ID"

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083352
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, -0x7fc84846

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2083364
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->k:LX/2jY;

    .line 2083365
    iget-object v3, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v3

    .line 2083366
    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v3

    .line 2083367
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2083368
    const v1, 0x7f031179

    invoke-virtual {v0, v1, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2083369
    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->i:LX/1P0;

    .line 2083370
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->h:LX/2ja;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->i:LX/1P0;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7, v8}, LX/2ja;->a(LX/1P1;JZ)V

    .line 2083371
    const v1, 0x102000a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2083372
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->i:LX/1P0;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2083373
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->a(Landroid/content/Context;)LX/E95;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2083374
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const v3, 0x1020004

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setEmptyView(Landroid/view/View;)V

    .line 2083375
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/E8g;

    invoke-direct {v3, p0}, LX/E8g;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;)V

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2083376
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/E8h;

    invoke-direct {v3, p0}, LX/E8h;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;)V

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setRecyclerListener(LX/1OU;)V

    .line 2083377
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    invoke-virtual {v1}, LX/E95;->e()V

    .line 2083378
    const/16 v1, 0x2b

    const v3, -0x4cfd27ce

    invoke-static {v9, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19e2be82

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2083361
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreComponentsFragment;->g:LX/E95;

    invoke-virtual {v1}, LX/E95;->dispose()V

    .line 2083362
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2083363
    const/16 v1, 0x2b

    const v2, -0x43a6fc20

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2c0e3b88

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2083353
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2083354
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2083355
    if-eqz v0, :cond_0

    .line 2083356
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2083357
    const-string v3, "show_more_title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2083358
    if-eqz v2, :cond_0

    .line 2083359
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2083360
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x33a708bb    # -5.6876308E7f

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
