.class public abstract Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.source ""


# instance fields
.field public a:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2082765
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;-><init>()V

    return-void
.end method

.method private P()V
    .locals 2

    .prologue
    .line 2082762
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2082763
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2082764
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2082752
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->lZ_()I

    move-result v0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2082753
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2082754
    if-eqz v2, :cond_0

    .line 2082755
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2082756
    const-string v3, "ptr_enabled"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    move v2, v1

    .line 2082757
    :goto_0
    const v1, 0x7f0d0595

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2082758
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v3, LX/E8T;

    invoke-direct {v3, p0}, LX/E8T;-><init>(Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;)V

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2082759
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2082760
    return-object v0

    :cond_0
    move v2, v1

    .line 2082761
    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 2082745
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    invoke-virtual {v2}, LX/2ja;->g()V

    .line 2082746
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    invoke-virtual {v2}, LX/2ja;->i()V

    .line 2082747
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    .line 2082748
    const/4 v0, 0x1

    .line 2082749
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2082750
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iz;->f(Ljava/lang/String;)V

    .line 2082751
    return-void
.end method

.method public final kK_()V
    .locals 0

    .prologue
    .line 2082729
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kK_()V

    .line 2082730
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->P()V

    .line 2082731
    return-void
.end method

.method public final kL_()V
    .locals 0

    .prologue
    .line 2082742
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    .line 2082743
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->P()V

    .line 2082744
    return-void
.end method

.method public lZ_()I
    .locals 1

    .prologue
    .line 2082741
    const v0, 0x7f031152

    return v0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6a7b0110

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082736
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroyView()V

    .line 2082737
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_0

    .line 2082738
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2082739
    iput-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2082740
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x59fe83ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082734
    invoke-super {p0, p1, p2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2082735
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 2082732
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2082733
    return-void
.end method
