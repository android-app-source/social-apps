.class public abstract Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/CfG;
.implements LX/0o8;


# instance fields
.field public a:Landroid/view/View;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Amz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/E94;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/195;

.field public j:LX/E8t;

.field public k:LX/2ja;

.field public l:LX/1P0;

.field public m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public n:Landroid/view/ViewGroup;

.field public o:LX/E8m;

.field private p:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public q:LX/2jY;

.field public r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public s:Z

.field public t:Z

.field public u:J

.field public v:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 2082618
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2082619
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->s:Z

    .line 2082620
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2082621
    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    .line 2082622
    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v:J

    return-void
.end method

.method public static P(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;)V
    .locals 3

    .prologue
    .line 2082623
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2082624
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->f()I

    move-result v1

    .line 2082625
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    sub-int/2addr v0, v1

    invoke-virtual {v2, v0}, LX/E8m;->e(I)I

    move-result v0

    .line 2082626
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v2}, LX/E8m;->d()I

    move-result v2

    invoke-virtual {v1, v2}, LX/E8m;->e(I)I

    move-result v1

    .line 2082627
    const/4 v2, 0x2

    move v2, v2

    .line 2082628
    sub-int/2addr v1, v2

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2082629
    if-eqz v0, :cond_0

    .line 2082630
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->N()V

    .line 2082631
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082632
    if-eqz p0, :cond_0

    .line 2082633
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2082634
    :cond_0
    return-void

    .line 2082635
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082636
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2082637
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final D()I
    .locals 1

    .prologue
    .line 2082638
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    return v0
.end method

.method public final E()LX/2jY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082639
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    return-object v0
.end method

.method public G()LX/1SX;
    .locals 1

    .prologue
    .line 2082616
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SX;

    return-object v0
.end method

.method public H()LX/1PT;
    .locals 1

    .prologue
    .line 2082640
    sget-object v0, LX/E1k;->a:LX/E1k;

    move-object v0, v0

    .line 2082641
    return-object v0
.end method

.method public final I()J
    .locals 2

    .prologue
    .line 2082642
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    return-wide v0
.end method

.method public final J()J
    .locals 2

    .prologue
    .line 2082643
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v:J

    return-wide v0
.end method

.method public M()I
    .locals 1

    .prologue
    .line 2082644
    const v0, 0x7f03114f

    return v0
.end method

.method public N()V
    .locals 1

    .prologue
    .line 2082645
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->h()V

    .line 2082646
    return-void
.end method

.method public O()V
    .locals 0

    .prologue
    .line 2082647
    return-void
.end method

.method public a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P0;
    .locals 1

    .prologue
    .line 2082682
    new-instance v0, LX/1Oz;

    invoke-direct {v0, p1}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;)LX/E8m;
    .locals 3

    .prologue
    .line 2082648
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->e:LX/E94;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->H()LX/1PT;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->G()LX/1SX;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2, p0}, LX/E94;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;)LX/E93;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;LX/2jY;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2082690
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2082691
    iget-object v1, p2, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2082692
    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    .line 2082693
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2082694
    invoke-virtual {p0, v2, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;

    move-result-object v3

    .line 2082695
    const v0, 0x102000a

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2082696
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    .line 2082697
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v6, v7, v5}, LX/2ja;->a(LX/1P1;JZ)V

    .line 2082698
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2082699
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-nez v0, :cond_4

    .line 2082700
    invoke-virtual {p0, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/content/Context;)LX/E8m;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    .line 2082701
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->p:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    invoke-virtual {v0, v4}, LX/E8m;->a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2082702
    const/4 v4, 0x0

    .line 2082703
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-eqz v0, :cond_1

    .line 2082704
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    invoke-virtual {v0}, LX/E8t;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2082705
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2082706
    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v5, :cond_0

    .line 2082707
    new-instance v5, LX/E8V;

    invoke-direct {v5, p0}, LX/E8V;-><init>(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;)V

    iput-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2082708
    :cond_0
    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-object v5, v5

    .line 2082709
    invoke-virtual {v0, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2082710
    :cond_1
    const v0, 0x7f031150

    move v0, v0

    .line 2082711
    invoke-virtual {v2, v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    .line 2082712
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2082713
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->s:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {v0}, LX/2jY;->y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2082714
    :cond_2
    invoke-virtual {p0, v4}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f_(Z)V

    .line 2082715
    :cond_3
    invoke-virtual {p0, v2, v3}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2082716
    :cond_4
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {p0, v0, v2, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/E8m;Landroid/content/Context;)V

    .line 2082717
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-eqz v0, :cond_5

    .line 2082718
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/E8m;->a(LX/E8t;Landroid/content/Context;)V

    .line 2082719
    :cond_5
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0, p2}, LX/E8m;->a(LX/2jY;)Z

    .line 2082720
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/E8U;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->i:LX/195;

    invoke-direct {v1, p0, v2}, LX/E8U;-><init>(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;LX/195;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2082721
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2082722
    return-object v3
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;
    .locals 2

    .prologue
    .line 2082689
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->lZ_()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public a(LX/2jY;)V
    .locals 6
    .param p1    # LX/2jY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2082683
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/2jY;->z()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2082684
    :cond_0
    :goto_0
    return-void

    .line 2082685
    :cond_1
    invoke-virtual {p1}, LX/2jY;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2082686
    iget-object v5, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v5, v0}, LX/E8m;->a(LX/9qT;)V

    .line 2082687
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2082688
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f_(Z)V

    goto :goto_0
.end method

.method public a(LX/9qT;)V
    .locals 2

    .prologue
    .line 2082723
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2082724
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2082725
    if-nez v0, :cond_0

    .line 2082726
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(LX/2jY;)V

    .line 2082727
    :goto_0
    return-void

    .line 2082728
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->a(LX/9qT;)V

    goto :goto_0
.end method

.method public final a(LX/E8t;)V
    .locals 3

    .prologue
    .line 2082677
    iput-object p1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    .line 2082678
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v0, :cond_0

    .line 2082679
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/E8m;->a(LX/E8t;Landroid/content/Context;)V

    .line 2082680
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->x()V

    .line 2082681
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2082655
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2082656
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    const/16 v4, 0x1d60

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, LX/193;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/193;

    const-class v6, LX/Cfr;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Cfr;

    const-class v7, LX/E94;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/E94;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-static {v0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object p1

    check-cast p1, LX/2iz;

    invoke-static {v0}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v0

    check-cast v0, LX/3Fx;

    iput-object v4, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->b:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c:LX/193;

    iput-object v6, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->d:LX/Cfr;

    iput-object v7, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->e:LX/E94;

    iput-object v8, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    iput-object p1, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    iput-object v0, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->h:LX/3Fx;

    .line 2082657
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->y()V

    .line 2082658
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2082659
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2082660
    :goto_0
    new-instance v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2082661
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082662
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2082663
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2082664
    :cond_0
    const-string v3, "unknown"

    .line 2082665
    :goto_1
    move-object v3, v3

    .line 2082666
    const-string v4, "unknown"

    invoke-direct {v2, v0, v3, v4, v1}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->p:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2082667
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_reaction_fragment_scroll_perf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->i:LX/195;

    .line 2082668
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->d:LX/Cfr;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {v0, v2, v1}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    .line 2082669
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082670
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2082671
    invoke-virtual {v0, v1, p0}, LX/2iz;->a(Ljava/lang/String;LX/CfG;)V

    .line 2082672
    return-void

    .line 2082673
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2082674
    const-string v2, "extra_reaction_analytics_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082675
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2082676
    goto :goto_1
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 2082654
    return-void
.end method

.method public a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/E8m;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2082651
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082652
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2082653
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082650
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2082649
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0, p1, p2}, LX/E8m;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2082617
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082547
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0, p1}, LX/E8m;->b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2jY;)V
    .locals 3

    .prologue
    .line 2082535
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-eqz v0, :cond_0

    .line 2082536
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {v0}, LX/2jY;->a()V

    .line 2082537
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082538
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2082539
    invoke-virtual {v0, v1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2082540
    iput-object p1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082541
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->z()V

    .line 2082542
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2082543
    if-eqz v0, :cond_0

    .line 2082544
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2082545
    invoke-virtual {v0, p1}, LX/E8m;->b(LX/2jY;)V

    .line 2082546
    :cond_0
    return-void
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2082528
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-eqz v1, :cond_0

    .line 2082529
    goto :goto_0

    .line 2082530
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->M()I

    move-result v1

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    .line 2082531
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2082532
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    iget-boolean v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->s:Z

    invoke-static {v1, v2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/View;Z)V

    .line 2082533
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    iget-boolean v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->s:Z

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v1, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/View;Z)V

    .line 2082534
    :goto_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2082496
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2082497
    if-eqz v0, :cond_0

    .line 2082498
    invoke-interface {v0, p1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2082499
    :cond_0
    return-void
.end method

.method public final f_(Z)V
    .locals 1

    .prologue
    .line 2082521
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/View;Z)V

    .line 2082522
    return-void
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082520
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public kJ_()Z
    .locals 1

    .prologue
    .line 2082519
    iget-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    return v0
.end method

.method public kK_()V
    .locals 1

    .prologue
    .line 2082515
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f_(Z)V

    .line 2082516
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->x()V

    .line 2082517
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->s:Z

    .line 2082518
    return-void
.end method

.method public kL_()V
    .locals 1

    .prologue
    .line 2082508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t:Z

    .line 2082509
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v0, :cond_0

    .line 2082510
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->j()V

    .line 2082511
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(LX/2jY;)V

    .line 2082512
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->x()V

    .line 2082513
    invoke-static {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->P(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;)V

    .line 2082514
    return-void
.end method

.method public final kU_()V
    .locals 2

    .prologue
    .line 2082503
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2082504
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2082505
    if-eqz v0, :cond_0

    .line 2082506
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2082507
    :cond_0
    return-void
.end method

.method public lZ_()I
    .locals 1

    .prologue
    .line 2082502
    const v0, 0x7f03111d

    return v0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082501
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2082500
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2082548
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-nez v0, :cond_0

    const-string v0, "NO_SESSION_ID"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082549
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2082550
    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082551
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082552
    iget-object p0, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2082553
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x76f43e37

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082554
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2082555
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082556
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    invoke-virtual {p0, p2, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/ViewGroup;LX/2jY;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x26c38b82

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2beb1c6e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082557
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    if-eqz v1, :cond_0

    .line 2082558
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082559
    iget-object v4, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2082560
    invoke-virtual {v1, v2}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2082561
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082562
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v1, :cond_1

    .line 2082563
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->n()V

    .line 2082564
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->dispose()V

    .line 2082565
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v2}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2082566
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_2

    .line 2082567
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2082568
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2082569
    const/16 v1, 0x2b

    const v2, -0x2109a776

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x74161dff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082523
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2082524
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v1, :cond_0

    .line 2082525
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->l()V

    .line 2082526
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u()J

    .line 2082527
    const/16 v1, 0x2b

    const v2, -0x3a97379b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x57e61836

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082570
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2082571
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v1, :cond_0

    .line 2082572
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->m()V

    .line 2082573
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->t()J

    .line 2082574
    const/16 v1, 0x2b

    const v2, -0x49985f39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x376b783b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082610
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2082611
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2082612
    if-eqz v1, :cond_0

    .line 2082613
    const-string v2, "source_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2082614
    invoke-virtual {p0, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c(Ljava/lang/String;)V

    .line 2082615
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x9884729

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public t()J
    .locals 4

    .prologue
    .line 2082575
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    .line 2082576
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    iget-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    invoke-virtual {v0, v2, v3}, LX/2ja;->e(J)V

    .line 2082577
    iget-wide v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    return-wide v0
.end method

.method public u()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 2082578
    iget-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 2082579
    :goto_0
    return-wide v0

    .line 2082580
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2082581
    iget-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v:J

    iget-wide v4, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v:J

    .line 2082582
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    invoke-virtual {v2, v0, v1}, LX/2ja;->d(J)V

    goto :goto_0
.end method

.method public abstract v()LX/2jY;
.end method

.method public w()I
    .locals 1

    .prologue
    .line 2082583
    const/4 v0, 0x0

    return v0
.end method

.method public final x()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2082584
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->d()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->w()I

    move-result v3

    if-gt v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 2082585
    :goto_0
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-eqz v3, :cond_1

    .line 2082586
    goto :goto_4

    :cond_1
    move v3, v2

    .line 2082587
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    move v0, v2

    .line 2082588
    :goto_2
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a:Landroid/view/View;

    invoke-static {v3, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/View;Z)V

    .line 2082589
    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    if-nez v0, :cond_4

    :goto_3
    invoke-static {v3, v2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/View;Z)V

    .line 2082590
    return-void

    :cond_2
    move v0, v1

    .line 2082591
    goto :goto_0

    :goto_4
    move v3, v1

    .line 2082592
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2082593
    goto :goto_2

    :cond_4
    move v2, v1

    .line 2082594
    goto :goto_3
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 2082595
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->A()Ljava/lang/String;

    move-result-object v0

    .line 2082596
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->g:LX/2iz;

    invoke-virtual {v1, v0}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 2082597
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v()LX/2jY;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    .line 2082598
    return-void
.end method

.method public final z()V
    .locals 3

    .prologue
    .line 2082599
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    .line 2082600
    iget-object v1, v0, LX/2ja;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082601
    iget-object v1, v0, LX/2ja;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082602
    iget-object v1, v0, LX/2ja;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082603
    iget-object v1, v0, LX/2ja;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082604
    iget-object v1, v0, LX/2ja;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082605
    iget-object v1, v0, LX/2ja;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082606
    iget-object v1, v0, LX/2ja;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2082607
    iget-object v1, v0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2082608
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->d:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->l:LX/1P0;

    invoke-virtual {v0, v1, v2}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    .line 2082609
    return-void
.end method
