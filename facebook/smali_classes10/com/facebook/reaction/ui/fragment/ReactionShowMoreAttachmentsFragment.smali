.class public Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0o8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0o8;"
    }
.end annotation


# instance fields
.field public a:LX/E6W;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/E6W",
            "<TVH;>;"
        }
    .end annotation
.end field

.field public b:LX/E6X;

.field private c:LX/Cft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cft",
            "<TVH;>;"
        }
    .end annotation
.end field

.field public d:LX/1vj;

.field public e:LX/1P0;

.field public f:Landroid/widget/ImageView;

.field public g:LX/3Fx;

.field public h:LX/2iz;

.field public i:LX/Cfr;

.field public j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private k:Ljava/lang/String;

.field private l:LX/2jY;

.field private m:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private n:LX/2ja;

.field private final o:LX/1OX;

.field private final p:LX/1OD;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2083299
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2083300
    new-instance v0, LX/E8e;

    invoke-direct {v0, p0}, LX/E8e;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->o:LX/1OX;

    .line 2083301
    new-instance v0, LX/E8f;

    invoke-direct {v0, p0}, LX/E8f;-><init>(Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->p:LX/1OD;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083302
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v2, p0

    check-cast v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;

    const-class v3, LX/E6X;

    invoke-interface {v7, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/E6X;

    invoke-static {v7}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v4

    check-cast v4, LX/1vj;

    invoke-static {v7}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v5

    check-cast v5, LX/3Fx;

    invoke-static {v7}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v6

    check-cast v6, LX/2iz;

    const-class v0, LX/Cfr;

    invoke-interface {v7, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Cfr;

    iput-object v3, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->b:LX/E6X;

    iput-object v4, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->d:LX/1vj;

    iput-object v5, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->g:LX/3Fx;

    iput-object v6, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->h:LX/2iz;

    iput-object v7, v2, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->i:LX/Cfr;

    .line 2083303
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083304
    const-string v1, "attachment_style"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "ATTACHMENT_STYLE_TAG must be present"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2083305
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083306
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "REACTION_SESSION_ID_TAG must be present"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2083307
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083308
    const-string v1, "reaction_unit_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "UNIT_ID_TAG must be present"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2083309
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083310
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "REACTION_SURFACE_TAG must be a (non-null) Surface string"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083311
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083312
    const-string v1, "attachment_style"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 2083313
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->d:LX/1vj;

    invoke-virtual {v1, v0}, LX/1vj;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfk;

    move-result-object v0

    .line 2083314
    instance-of v1, v0, LX/Cft;

    const-string v2, "You must inherit from ReactionRecyclableAttachmentHandler to use this fragment"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2083315
    check-cast v0, LX/Cft;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->c:LX/Cft;

    .line 2083316
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083317
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->k:Ljava/lang/String;

    .line 2083318
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->h:LX/2iz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->l:LX/2jY;

    .line 2083319
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->e:LX/1P0;

    .line 2083320
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->i:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->l:LX/2jY;

    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->e:LX/1P0;

    invoke-virtual {v0, v1, v2}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->n:LX/2ja;

    .line 2083321
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083322
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->m:Ljava/lang/String;

    .line 2083323
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->b:LX/E6X;

    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->c:LX/Cft;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->m:Ljava/lang/String;

    .line 2083324
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2083325
    const-string v5, "reaction_unit_id"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v2, p0

    .line 2083326
    new-instance v6, LX/E6W;

    const-class v7, LX/E8P;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/E8P;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v12}, LX/E6W;-><init>(LX/Cft;LX/0o8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/E8P;)V

    .line 2083327
    move-object v0, v6

    .line 2083328
    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->a:LX/E6W;

    .line 2083329
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2083330
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083332
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083331
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083297
    const/4 v0, 0x0

    return-object v0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2083298
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083296
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->n:LX/2ja;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083295
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2083294
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->k:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "NO_SESSION_ID"

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083293
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c781c2e

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2083276
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2083277
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2083278
    const-string v3, "reaction_surface"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v0

    .line 2083279
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2083280
    const v2, 0x7f031178

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2083281
    const v0, 0x7f0d2926

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2083282
    const v0, 0x7f0d2925

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->f:Landroid/widget/ImageView;

    .line 2083283
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->f:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040023

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2083284
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->e:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2083285
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0117

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2083286
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0036

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2083287
    iget-object v4, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v5, LX/62X;

    invoke-direct {v5, v0, v3}, LX/62X;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2083288
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->a:LX/E6W;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2083289
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->o:LX/1OX;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2083290
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->a:LX/E6W;

    iget-object v3, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->p:LX/1OD;

    invoke-virtual {v0, v3}, LX/1OM;->a(LX/1OD;)V

    .line 2083291
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/ReactionShowMoreAttachmentsFragment;->a:LX/E6W;

    invoke-virtual {v0}, LX/E6W;->e()V

    .line 2083292
    const/16 v0, 0x2b

    const v3, -0x32bb7ead

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x9c80542

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2083268
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2083269
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2083270
    if-eqz v0, :cond_0

    .line 2083271
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2083272
    const-string v3, "show_more_title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2083273
    if-eqz v2, :cond_0

    .line 2083274
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2083275
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x146522ba

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
