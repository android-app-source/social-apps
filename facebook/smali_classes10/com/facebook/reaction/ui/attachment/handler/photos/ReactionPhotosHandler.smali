.class public abstract Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;
.super LX/E6s;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "P::",
        "LX/1U8;",
        ">",
        "LX/E6s;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private d:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public e:Z

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:[J

.field public j:LX/E7X;

.field private k:LX/E7b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler",
            "<TT;TP;>.ReactionPhotosScroll",
            "Listener;"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field private n:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public final o:LX/E1i;

.field private final p:LX/3Tz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2081684
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    const-string v1, "reaction_dialog_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/E1i;",
            "LX/3Tx;",
            "LX/3Tz;",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2081685
    invoke-direct {p0, p1, p3}, LX/E6s;-><init>(LX/0Or;LX/3Tx;)V

    .line 2081686
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->o:LX/E1i;

    .line 2081687
    iput-object p4, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->p:LX/3Tz;

    .line 2081688
    iput-object p5, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->d:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 2081689
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/1U8;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2081702
    new-instance v0, LX/E7Y;

    invoke-direct {v0, p0, p3, p1, p2}, LX/E7Y;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;LX/1U8;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public abstract a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "Ljava/util/List",
            "<TP;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2081690
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 2081691
    :goto_0
    return-void

    .line 2081692
    :cond_0
    instance-of v0, p3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    check-cast p3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v4, p3

    .line 2081693
    :goto_1
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "photo_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    .line 2081694
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    iget-object v1, p0, LX/E6s;->a:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    iget-object v2, p0, LX/E6s;->a:LX/1P1;

    invoke-virtual {v2}, LX/1P1;->n()I

    move-result v2

    .line 2081695
    add-int/lit8 v3, v1, -0x2

    const/4 v6, 0x0

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2081696
    add-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-object v7, v0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2081697
    iget-object v7, v0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v7, v3, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    move-object v2, v3

    .line 2081698
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->p:LX/3Tz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    iget-object v3, p2, LX/Cfl;->b:Ljava/lang/String;

    new-instance v6, LX/E7a;

    invoke-direct {v6, p0}, LX/E7a;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;)V

    .line 2081699
    iget-object v7, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v7, v7

    .line 2081700
    invoke-virtual/range {v0 .. v7}, LX/3Tz;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;LX/9hM;Landroid/content/Context;)V

    goto :goto_0

    .line 2081701
    :cond_1
    const v0, 0x7f0d287b

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v4, v0

    goto :goto_1
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V
.end method

.method public abstract a(LX/1U8;)Z
    .param p1    # LX/1U8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)Z"
        }
    .end annotation
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z
    .locals 1

    .prologue
    .line 2081675
    invoke-super {p0, p1}, LX/E6s;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->c()LX/0us;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2081649
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    if-nez v0, :cond_2

    .line 2081650
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)LX/E7X;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    .line 2081651
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    .line 2081652
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->m:Ljava/lang/String;

    .line 2081653
    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->c()LX/0us;

    move-result-object v0

    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    .line 2081654
    new-instance v0, LX/E7b;

    invoke-direct {v0, p0}, LX/E7b;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->k:LX/E7b;

    .line 2081655
    const/4 v3, 0x0

    .line 2081656
    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v6

    .line 2081657
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v4

    new-array v7, v4, [J

    .line 2081658
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    move v4, v3

    :goto_0
    if-ge v5, v8, :cond_1

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2081659
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v3

    .line 2081660
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v9

    invoke-interface {v9}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2081661
    :try_start_0
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v3

    invoke-interface {v3}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    aput-wide v9, v7, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2081662
    add-int/lit8 v3, v4, 0x1

    .line 2081663
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_0

    :catch_0
    :cond_0
    move v3, v4

    goto :goto_1

    .line 2081664
    :cond_1
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    if-ne v4, v3, :cond_3

    .line 2081665
    iput-object v7, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->i:[J

    .line 2081666
    :cond_2
    :goto_2
    iput-boolean v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->e:Z

    .line 2081667
    iget-object v0, p0, LX/E6s;->c:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 2081668
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2081669
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->k:LX/E7b;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2081670
    iget-object v0, p0, LX/E6s;->a:LX/1P1;

    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->g:I

    iget v2, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->f:I

    invoke-virtual {v0, v1, v2}, LX/1P1;->d(II)V

    .line 2081671
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    return v0

    .line 2081672
    :cond_3
    invoke-static {v7, v4}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->i:[J

    goto :goto_2
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081673
    invoke-virtual {p0, p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)LX/1U8;

    move-result-object v0

    .line 2081674
    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(LX/1U8;)Z

    move-result v0

    return v0
.end method

.method public abstract c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)LX/1U8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionStoryAttachmentFragment;",
            ")TP;"
        }
    .end annotation
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)LX/E7X;
.end method

.method public final g()LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2081676
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->n:LX/0Vd;

    if-nez v0, :cond_0

    .line 2081677
    new-instance v0, LX/E7Z;

    invoke-direct {v0, p0}, LX/E7Z;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->n:LX/0Vd;

    .line 2081678
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->n:LX/0Vd;

    return-object v0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 2081679
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->d:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    if-ne v0, v1, :cond_0

    .line 2081680
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2081681
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1645

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    :goto_0
    return v0

    .line 2081682
    :cond_0
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2081683
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1644

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method
