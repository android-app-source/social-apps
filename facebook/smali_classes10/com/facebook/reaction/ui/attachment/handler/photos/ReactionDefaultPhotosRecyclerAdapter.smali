.class public Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;
.super LX/E7X;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E7X",
        "<",
        "LX/E7W;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2081764
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    const-string v1, "reaction_dialog_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2081765
    invoke-direct {p0}, LX/E7X;-><init>()V

    .line 2081766
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->b:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    .line 2081767
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2081768
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2081769
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->a(LX/1U8;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2081770
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081771
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2081772
    :cond_1
    invoke-virtual {p0, v2}, LX/E7X;->a(Ljava/util/List;)V

    .line 2081773
    iput-object p3, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->c:Ljava/lang/String;

    .line 2081774
    iput-object p4, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->d:Ljava/lang/String;

    .line 2081775
    return-void
.end method

.method public static a(LX/1U8;)Z
    .locals 1
    .param p0    # LX/1U8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2081763
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2081757
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031112

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2081758
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->b:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-virtual {v1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->i()I

    move-result v1

    .line 2081759
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2081760
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2081761
    const v1, 0x7f0d287b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081762
    new-instance v1, LX/E7W;

    invoke-direct {v1, p0, v0}, LX/E7W;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method
