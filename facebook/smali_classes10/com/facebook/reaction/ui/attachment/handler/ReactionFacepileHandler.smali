.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;
.super LX/Cfk;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/view/ViewGroup;

.field private final c:LX/E1i;

.field private d:I

.field private e:Landroid/view/ViewGroup;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Z

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2080679
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080676
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080677
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->c:LX/E1i;

    .line 2080678
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 3
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2080672
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    .line 2080673
    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2080674
    const/4 v0, 0x0

    .line 2080675
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->c:LX/E1i;

    sget-object v2, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v1, v0, v2}, LX/E1i;->a(LX/5sc;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 4
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2080666
    const v0, 0x7f03114d

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2080667
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2080668
    const v0, 0x7f0d28e3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2080669
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2080670
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2080671
    return-object v1
.end method

.method public final a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2080640
    invoke-super/range {p0 .. p6}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2080641
    const v0, 0x7f031109

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2080642
    const v0, 0x7f0d286d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->e:Landroid/view/ViewGroup;

    .line 2080643
    iget-object v0, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    move-object v0, v0

    .line 2080644
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080645
    iput-boolean v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->g:Z

    .line 2080646
    iput v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->h:I

    .line 2080647
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2080654
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->h:I

    .line 2080655
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->d:I

    add-int/lit8 v0, v0, -0x4

    .line 2080656
    iget-boolean v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->g:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->h:I

    if-gt v1, v0, :cond_1

    .line 2080657
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080658
    :goto_0
    return-void

    .line 2080659
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2080660
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080661
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->d:I

    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->h:I

    sub-int/2addr v0, v1

    .line 2080662
    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2080663
    const v0, 0x7f03110a

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->b:Landroid/view/ViewGroup;

    .line 2080664
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080665
    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2080649
    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iput v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->d:I

    .line 2080650
    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->d:I

    const/4 v2, 0x6

    if-le v1, v2, :cond_0

    .line 2080651
    :goto_0
    return v0

    .line 2080652
    :cond_0
    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->d:I

    const/4 v2, 0x4

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHandler;->g:Z

    .line 2080653
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080648
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
