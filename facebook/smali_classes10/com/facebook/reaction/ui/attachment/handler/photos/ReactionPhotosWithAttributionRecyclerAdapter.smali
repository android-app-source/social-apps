.class public Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;
.super LX/E7X;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E7X",
        "<",
        "LX/E7e;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:LX/0W9;

.field public final d:LX/11S;

.field public e:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2081899
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    const-string v1, "reaction_dialog_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2081900
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    const-string v1, "privacy"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0W9;LX/11S;Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081901
    invoke-direct {p0}, LX/E7X;-><init>()V

    .line 2081902
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->c:LX/0W9;

    .line 2081903
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->d:LX/11S;

    .line 2081904
    iput-object p3, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->e:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    .line 2081905
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2081906
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2081907
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->P()LX/5kF;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081908
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2081909
    :cond_0
    invoke-virtual {p0, v2}, LX/E7X;->a(Ljava/util/List;)V

    .line 2081910
    iput-object p5, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->f:Ljava/lang/String;

    .line 2081911
    iput-object p6, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->g:Ljava/lang/String;

    .line 2081912
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 8

    .prologue
    .line 2081913
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031113

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2081914
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->e:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-virtual {v0}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->i()I

    move-result v0

    .line 2081915
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2081916
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2081917
    const v0, 0x7f0d287c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081918
    const v0, 0x7f0d287d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2081919
    const v0, 0x7f0d287e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    .line 2081920
    const v0, 0x7f0d287f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/resources/ui/FbTextView;

    .line 2081921
    const v0, 0x7f0d2880

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081922
    new-instance v0, LX/E7e;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/E7e;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v0
.end method
