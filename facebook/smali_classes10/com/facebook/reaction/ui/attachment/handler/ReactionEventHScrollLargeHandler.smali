.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;
.super LX/E6d;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/E6n;

.field private final d:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2080570
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;LX/E6n;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080571
    invoke-direct {p0, p1, p2, p3}, LX/E6d;-><init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;)V

    .line 2080572
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->d:LX/E1i;

    .line 2080573
    iput-object p4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->c:LX/E6n;

    .line 2080574
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 2

    .prologue
    .line 2080575
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->d:LX/E1i;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1i;->c(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2080576
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->c:LX/E6n;

    const v0, 0x7f031107

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventsCardView;

    sget-object v2, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p1, v0, v2}, LX/E6n;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/events/widget/eventcard/EventsCardView;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2080577
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionEventHScrollLargeHandler;->c:LX/E6n;

    .line 2080578
    iget-object v1, v0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    move-object v0, v1

    .line 2080579
    return-object v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 2

    .prologue
    .line 2080580
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->eR_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
