.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;
.super LX/Cfk;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2081479
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;

    const-string v1, "reaction_dialog"

    const-string v2, "attachment_icon"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081472
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081473
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;->b:LX/E1i;

    .line 2081474
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 5
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2081475
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    .line 2081476
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2081477
    const/4 v0, 0x0

    .line 2081478
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;->b:LX/E1i;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v4, LX/Cfc;->STORY_TAP:LX/Cfc;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x21

    const/4 v8, 0x0

    .line 2081433
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v2

    .line 2081434
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 2081435
    const v1, 0x7f031115

    invoke-virtual {p0, v1}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v3

    .line 2081436
    const v1, 0x7f0d2883

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2081437
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2081438
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v5

    .line 2081439
    const v4, 0x7f0d2882

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2081440
    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2081441
    const v5, 0x7f0a010a

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2081442
    :cond_0
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2081443
    const/4 v0, 0x0

    .line 2081444
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2081445
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2081446
    :cond_1
    :goto_0
    if-eqz v0, :cond_4

    .line 2081447
    const-string v5, " - "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2081448
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 2081449
    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2081450
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    .line 2081451
    iget-object v6, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v6, v6

    .line 2081452
    const v7, 0x7f0e08fd

    invoke-direct {v0, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4, v0, v8, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2081453
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    .line 2081454
    iget-object v6, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v6, v6

    .line 2081455
    const v7, 0x7f0e08fe

    invoke-direct {v0, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v4, v0, v5, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2081456
    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081457
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2081458
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2081459
    const v0, 0x7f0d2885

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2081460
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081461
    sget-object v4, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileStoryHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081462
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v0

    .line 2081463
    invoke-virtual {p0, v0, v1}, LX/Cfk;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 2081464
    const v0, 0x7f0d2884

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081465
    new-instance v0, LX/8sz;

    invoke-direct {v0}, LX/8sz;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2081466
    return-object v3

    .line 2081467
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2081468
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2081469
    :cond_4
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    .line 2081470
    iget-object v5, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v5, v5

    .line 2081471
    const v6, 0x7f0e08fd

    invoke-direct {v0, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v4, v0, v8, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2081431
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    .line 2081432
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
