.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;
.super LX/E6d;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/E1i;

.field public final d:LX/961;

.field public final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2081105
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    const-string v1, "reaction_dialog"

    const-string v2, "PROFILE_PHOTO"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;LX/961;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081100
    invoke-direct {p0, p1, p2, p3}, LX/E6d;-><init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;)V

    .line 2081101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    .line 2081102
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->c:LX/E1i;

    .line 2081103
    iput-object p4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->d:LX/961;

    .line 2081104
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 5

    .prologue
    .line 2081106
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->c:LX/E1i;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v2

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v3

    invoke-interface {v3}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2081085
    const v0, 0x7f03110d

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v5

    .line 2081086
    const v0, 0x7f0d2867

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081087
    const v1, 0x7f0d2868

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2081088
    const v2, 0x7f0d2874

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2081089
    const v3, 0x7f0d286b

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2081090
    iget-object v4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v6

    invoke-interface {v6}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2081091
    iget-object v4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v6

    invoke-interface {v6}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v7

    invoke-interface {v7}, LX/5sc;->as_()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2081092
    :cond_0
    iget-object v4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v6

    invoke-interface {v6}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 2081093
    new-instance v4, LX/E76;

    invoke-direct {v4, p0, p1, v3}, LX/E76;-><init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/fbui/glyph/GlyphView;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2081094
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v3

    invoke-interface {v3}, LX/5sc;->l()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081095
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2081096
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081097
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->l()LX/174;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2081098
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->l()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081099
    :cond_1
    return-object v5
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081084
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()F
    .locals 1

    .prologue
    .line 2081083
    const v0, 0x3f733333    # 0.95f

    return v0
.end method
