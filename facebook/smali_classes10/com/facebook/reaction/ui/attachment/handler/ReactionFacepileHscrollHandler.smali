.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;
.super LX/E6s;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final d:LX/E1i;

.field public final e:LX/3Tx;

.field private f:LX/E6p;

.field private g:LX/E6q;

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2080746
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/E1i;LX/3Tx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/E1i;",
            "LX/3Tx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080747
    invoke-direct {p0, p1, p3}, LX/E6s;-><init>(LX/0Or;LX/3Tx;)V

    .line 2080748
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->d:LX/E1i;

    .line 2080749
    iput-object p3, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->e:LX/3Tx;

    .line 2080750
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2080751
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->f:LX/E6p;

    if-nez v0, :cond_0

    .line 2080752
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->j:Ljava/lang/String;

    .line 2080753
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->k:Ljava/lang/String;

    .line 2080754
    new-instance v0, LX/E6p;

    invoke-direct {v0, p0, p3}, LX/E6p;-><init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->f:LX/E6p;

    .line 2080755
    new-instance v0, LX/E6q;

    invoke-direct {v0, p0}, LX/E6q;-><init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;)V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->g:LX/E6q;

    .line 2080756
    :cond_0
    iget-object v0, p0, LX/E6s;->c:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 2080757
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHorizontalScrollBarEnabled(Z)V

    .line 2080758
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->f:LX/E6p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2080759
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->g:LX/E6q;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2080760
    iget-object v0, p0, LX/E6s;->a:LX/1P1;

    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->i:I

    iget v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->h:I

    invoke-virtual {v0, v1, v2}, LX/1P1;->d(II)V

    .line 2080761
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->f:LX/E6p;

    invoke-virtual {v0}, LX/E6p;->ij_()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080762
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
