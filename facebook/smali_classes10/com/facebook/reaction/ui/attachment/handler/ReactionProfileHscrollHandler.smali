.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;
.super LX/E6d;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final d:LX/8sz;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2081337
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;

    const-string v1, "reaction_dialog"

    const-string v2, "COVER_PHOTO"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2081338
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;

    const-string v1, "reaction_dialog"

    const-string v2, "PROFILE_PHOTO"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081339
    invoke-direct {p0, p1, p2, p3}, LX/E6d;-><init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;)V

    .line 2081340
    new-instance v0, LX/8sz;

    invoke-direct {v0}, LX/8sz;-><init>()V

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->d:LX/8sz;

    .line 2081341
    return-void
.end method

.method private a(Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;"
        }
    .end annotation

    .prologue
    .line 2081342
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 2081343
    iget-object v2, p0, LX/E6d;->d:LX/E1i;

    move-object v2, v2

    .line 2081344
    invoke-virtual {v2, v0}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2081345
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;LX/5sc;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 3

    .prologue
    .line 2081346
    invoke-interface {p1}, LX/5sc;->at_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/5sc;->at_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/5sc;->at_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/5sc;->at_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2081347
    invoke-interface {p1}, LX/5sc;->at_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2081348
    sget-object v1, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081349
    :cond_0
    invoke-interface {p1}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2081350
    invoke-interface {p1}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2081351
    const v0, 0x7f0d2871

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081352
    sget-object v2, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081353
    const v0, 0x7f0d2868

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {p1}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081354
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->l()LX/174;

    move-result-object v1

    .line 2081355
    if-eqz v1, :cond_1

    .line 2081356
    const v0, 0x7f0d2870

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081357
    :cond_1
    return-void

    .line 2081358
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 2081359
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->b()LX/174;

    move-result-object v0

    .line 2081360
    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081361
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->d:LX/8sz;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2081362
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 2081363
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 4
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2081364
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    .line 2081365
    const v2, 0x7f0d2876

    if-eq v1, v2, :cond_0

    const v2, 0x7f0d286e

    if-ne v1, v2, :cond_3

    .line 2081366
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    .line 2081367
    if-eqz v1, :cond_1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2081368
    :cond_1
    :goto_0
    return-object v0

    .line 2081369
    :cond_2
    iget-object v0, p0, LX/E6d;->d:LX/E1i;

    move-object v0, v0

    .line 2081370
    sget-object v2, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2}, LX/E1i;->a(LX/5sc;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0

    .line 2081371
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    if-eqz v1, :cond_1

    .line 2081372
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2081373
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->e()LX/0Px;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    move-result-object v1

    .line 2081374
    iget-object v2, p0, LX/E6d;->d:LX/E1i;

    move-object v2, v2

    .line 2081375
    const/4 v3, 0x0

    .line 2081376
    invoke-virtual {v2, v1}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z

    move-result p0

    if-nez p0, :cond_5

    .line 2081377
    :cond_4
    :goto_1
    move-object v0, v3

    .line 2081378
    goto :goto_0

    .line 2081379
    :cond_5
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    if-ne v1, p0, :cond_6

    .line 2081380
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->hb_()Ljava/lang/String;

    move-result-object v3

    .line 2081381
    invoke-static {v3}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    goto :goto_1

    .line 2081382
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object p0

    .line 2081383
    if-eqz p0, :cond_4

    invoke-interface {p0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 2081384
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    if-ne v1, p2, :cond_7

    .line 2081385
    sget-object v3, LX/Cfc;->VIEW_PROFILE_TAP:LX/Cfc;

    invoke-virtual {v2, p0, v3}, LX/E1i;->a(LX/5sc;LX/Cfc;)LX/Cfl;

    move-result-object v3

    goto :goto_1

    .line 2081386
    :cond_7
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    if-ne v1, p2, :cond_4

    .line 2081387
    invoke-interface {p0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->m(Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2081388
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v2

    .line 2081389
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2081390
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2081391
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->e()LX/0Px;

    move-result-object v6

    .line 2081392
    if-eqz v6, :cond_0

    invoke-direct {p0, v6}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2081393
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2081394
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2081395
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2081396
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2081397
    const v0, 0x7f03110b

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2081398
    const v0, 0x7f0d286f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081399
    const v3, 0x408a31d7

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2081400
    invoke-static {p1, v2, v1, v0}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;LX/5sc;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2081401
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->d:LX/8sz;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2081402
    move-object v0, v1

    .line 2081403
    :goto_1
    return-object v0

    :cond_2
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2081404
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v5

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2081405
    const v1, 0x7f03110e

    invoke-virtual {p0, v1}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v6

    .line 2081406
    const v1, 0x7f0d286f

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081407
    const v4, 0x402620ae

    invoke-virtual {v1, v4}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2081408
    invoke-static {p1, v2, v6, v1}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;LX/5sc;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2081409
    const v1, 0x7f0d2876

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2081410
    new-instance v4, LX/8sz;

    invoke-direct {v4}, LX/8sz;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2081411
    const v1, 0x7f0d2877

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2081412
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ne v4, v5, :cond_4

    .line 2081413
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2081414
    const v4, 0x7f031157

    invoke-virtual {v1, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2081415
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2081416
    invoke-static {p0, v3, v1}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;Landroid/widget/TextView;)V

    .line 2081417
    :goto_3
    move-object v0, v6

    .line 2081418
    goto :goto_1

    :cond_3
    move v1, v3

    .line 2081419
    goto :goto_2

    .line 2081420
    :cond_4
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2081421
    const v4, 0x7f031156

    invoke-virtual {v1, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2081422
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2081423
    const v4, 0x7f0d28ee

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2081424
    invoke-static {p0, v3, v4}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;Landroid/widget/TextView;)V

    .line 2081425
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2081426
    const v4, 0x7f0d28ef

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2081427
    invoke-static {p0, v3, v1}, Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;->a(Lcom/facebook/reaction/ui/attachment/handler/ReactionProfileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;Landroid/widget/TextView;)V

    goto :goto_3
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081428
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    .line 2081429
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {v0}, LX/5sc;->k()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object p0

    invoke-interface {p0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2081430
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
