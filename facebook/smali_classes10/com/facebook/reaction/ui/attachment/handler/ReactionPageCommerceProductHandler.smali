.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;
.super LX/Cfk;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/E1i;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2080947
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3Tx;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080948
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080949
    iput-object p2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->b:LX/E1i;

    .line 2080950
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2080951
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionCommerceActionFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionCommerceActionFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionCommerceActionFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2080952
    :cond_0
    const/4 v0, 0x0

    .line 2080953
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->b:LX/E1i;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionCommerceActionFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionCommerceActionFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/7iP;->PAGE:LX/7iP;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/7iP;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2080954
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2080955
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2080956
    new-instance v1, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;

    iget-object v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;-><init>(Landroid/content/Context;)V

    .line 2080957
    invoke-virtual {v1, v0}, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080958
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->h:I

    iget v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->h:I

    iget v3, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->h:I

    iget v4, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->h:I

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->setPadding(IIII)V

    .line 2080959
    new-instance v0, LX/7iy;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->c()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->gF_()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, LX/7iy;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 2080960
    iget-object v2, v1, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2080961
    iget-object v3, v0, LX/7iy;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 2080962
    sget-object v4, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2080963
    iget-object v2, v1, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2080964
    iget-object v3, v0, LX/7iy;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2080965
    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080966
    iget-object v2, v1, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2080967
    iget-object v3, v0, LX/7iy;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2080968
    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080969
    return-object v1
.end method

.method public final a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080970
    invoke-super/range {p0 .. p6}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2080971
    const v0, 0x7f030e59

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->c:Landroid/widget/LinearLayout;

    .line 2080972
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d2300

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->d:Landroid/widget/LinearLayout;

    .line 2080973
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d2301

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->e:Landroid/widget/LinearLayout;

    .line 2080974
    iget-object v0, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    move-object v0, v0

    .line 2080975
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080976
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->h:I

    .line 2080977
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2080978
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->g:I

    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->f:I

    if-lt v0, v1, :cond_0

    .line 2080979
    :goto_0
    return-void

    .line 2080980
    :cond_0
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->g:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2080981
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2080982
    :goto_1
    iget v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->g:I

    goto :goto_0

    .line 2080983
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2080984
    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    .line 2080985
    if-gtz v1, :cond_3

    .line 2080986
    const/4 v1, 0x0

    .line 2080987
    :cond_0
    :goto_0
    move v1, v1

    .line 2080988
    iput v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->f:I

    .line 2080989
    iput v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->g:I

    .line 2080990
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2080991
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2080992
    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->f:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 2080993
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->e:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2080994
    :goto_1
    iget v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->f:I

    if-nez v1, :cond_2

    .line 2080995
    :goto_2
    return v0

    .line 2080996
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageCommerceProductHandler;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 2080997
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    goto :goto_2

    .line 2080998
    :cond_3
    if-eq v1, v3, :cond_0

    .line 2080999
    if-le v1, v3, :cond_4

    if-ge v1, v2, :cond_4

    .line 2081000
    const/4 v1, 0x2

    goto :goto_0

    :cond_4
    move v1, v2

    .line 2081001
    goto :goto_0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081002
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->c()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->c()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->L()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;->gF_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
