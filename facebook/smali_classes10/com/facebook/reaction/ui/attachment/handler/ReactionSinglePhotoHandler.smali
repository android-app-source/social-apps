.class public Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;
.super LX/Cfk;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/E1i;

.field private c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

.field private final d:LX/3Tz;

.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2081539
    const-class v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/3Tx;LX/3Tz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081535
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081536
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->b:LX/E1i;

    .line 2081537
    iput-object p3, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->d:LX/3Tz;

    .line 2081538
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 7
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 2081529
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    .line 2081530
    :try_start_0
    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2081531
    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    .line 2081532
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v1, v0

    .line 2081533
    const/4 v4, 0x0

    invoke-static/range {v1 .. v5}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2081534
    :goto_0
    return-object v0

    :catch_0
    move-object v0, v6

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2081523
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v1

    .line 2081524
    const v0, 0x7f031119

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081525
    invoke-interface {v1}, LX/1U8;->e()LX/1Fb;

    move-result-object v1

    .line 2081526
    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081527
    const v1, 0x40155555

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2081528
    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 2081510
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 2081511
    :goto_0
    return-void

    .line 2081512
    :cond_0
    instance-of v0, p3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    check-cast p3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v4, p3

    .line 2081513
    :goto_1
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "photo_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    .line 2081514
    iget-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->d:LX/3Tz;

    iget-object v1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    iget-object v3, p2, LX/Cfl;->b:Ljava/lang/String;

    .line 2081515
    iget-object v6, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v6, v6

    .line 2081516
    invoke-virtual/range {v0 .. v6}, LX/3Tz;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;Landroid/content/Context;)V

    goto :goto_0

    .line 2081517
    :cond_1
    const v0, 0x7f0d288c

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v4, v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 1

    .prologue
    .line 2081520
    iput-object p1, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->e:Ljava/lang/String;

    .line 2081521
    iput-object p3, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 2081522
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 2

    .prologue
    .line 2081518
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    .line 2081519
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
