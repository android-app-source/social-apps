.class public Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E1y;

.field private final e:LX/E1s;

.field private final f:LX/E2G;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E1y;LX/E1s;LX/E2G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071728
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2071729
    iput-object p2, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->d:LX/E1y;

    .line 2071730
    iput-object p3, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->e:LX/E1s;

    .line 2071731
    iput-object p4, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->f:LX/E2G;

    .line 2071732
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2071699
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 2071700
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 2071701
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2071702
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2071703
    new-instance v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2071704
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2071705
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2071706
    invoke-direct {v6, v0, v7, v8}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2071707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2071708
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->d:LX/E1y;

    invoke-virtual {v0, p1}, LX/E1y;->c(LX/1De;)LX/E1w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->U()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1w;->b(Ljava/lang/String;)LX/E1w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->V()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)LX/E1w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->Y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;)LX/E1w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->Z()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;)LX/E1w;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->f:LX/E2G;

    const/4 v2, 0x0

    .line 2071709
    new-instance v3, LX/E2F;

    invoke-direct {v3, v1}, LX/E2F;-><init>(LX/E2G;)V

    .line 2071710
    iget-object v5, v1, LX/E2G;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/E2E;

    .line 2071711
    if-nez v5, :cond_1

    .line 2071712
    new-instance v5, LX/E2E;

    invoke-direct {v5, v1}, LX/E2E;-><init>(LX/E2G;)V

    .line 2071713
    :cond_1
    invoke-static {v5, p1, v2, v2, v3}, LX/E2E;->a$redex0(LX/E2E;LX/1De;IILX/E2F;)V

    .line 2071714
    move-object v3, v5

    .line 2071715
    move-object v2, v3

    .line 2071716
    move-object v1, v2

    .line 2071717
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2071718
    iget-object v3, v1, LX/E2E;->a:LX/E2F;

    iput-object v2, v3, LX/E2F;->c:LX/0Px;

    .line 2071719
    iget-object v3, v1, LX/E2E;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2071720
    move-object v1, v1

    .line 2071721
    iget-object v2, v1, LX/E2E;->a:LX/E2F;

    iput-object p3, v2, LX/E2F;->a:LX/2km;

    .line 2071722
    iget-object v2, v1, LX/E2E;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2071723
    move-object v1, v1

    .line 2071724
    iget-object v2, v1, LX/E2E;->a:LX/E2F;

    iput-object p2, v2, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2071725
    iget-object v2, v1, LX/E2E;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2071726
    move-object v1, v1

    .line 2071727
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1w;->a(LX/1X1;)LX/E1w;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;
    .locals 7

    .prologue
    .line 2071688
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;

    monitor-enter v1

    .line 2071689
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071690
    sput-object v2, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071691
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071692
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071693
    new-instance p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E1y;->a(LX/0QB;)LX/E1y;

    move-result-object v4

    check-cast v4, LX/E1y;

    invoke-static {v0}, LX/E1s;->a(LX/0QB;)LX/E1s;

    move-result-object v5

    check-cast v5, LX/E1s;

    invoke-static {v0}, LX/E2G;->a(LX/0QB;)LX/E2G;

    move-result-object v6

    check-cast v6, LX/E2G;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;-><init>(Landroid/content/Context;LX/E1y;LX/E1s;LX/E2G;)V

    .line 2071694
    move-object v0, p0

    .line 2071695
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071696
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071697
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071698
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2071673
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2071687
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreHorizontalListComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2071675
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 2071676
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2071677
    instance-of v0, v0, LX/9ud;

    if-nez v0, :cond_0

    move v0, v1

    .line 2071678
    :goto_0
    return v0

    .line 2071679
    :cond_0
    invoke-static {p1}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 2071680
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 2071681
    goto :goto_0

    .line 2071682
    :cond_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2071683
    invoke-static {v0}, LX/E1s;->a(LX/9uc;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2071684
    goto :goto_0

    .line 2071685
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2071686
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2071674
    const/4 v0, 0x0

    return-object v0
.end method
