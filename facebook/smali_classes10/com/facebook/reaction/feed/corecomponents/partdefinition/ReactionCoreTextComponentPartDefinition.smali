.class public Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/E1s;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E1s;LX/2d1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071804
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2071805
    iput-object p2, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->d:LX/E1s;

    .line 2071806
    iput-object p3, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->e:LX/2d1;

    .line 2071807
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2071803
    iget-object v0, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->d:LX/E1s;

    invoke-virtual {v0, p1, p2, p3}, LX/E1s;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;
    .locals 6

    .prologue
    .line 2071792
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;

    monitor-enter v1

    .line 2071793
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071794
    sput-object v2, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071795
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071796
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071797
    new-instance p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E1s;->a(LX/0QB;)LX/E1s;

    move-result-object v4

    check-cast v4, LX/E1s;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/E1s;LX/2d1;)V

    .line 2071798
    move-object v0, p0

    .line 2071799
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071800
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071801
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2071791
    iget-object v0, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2071790
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2071783
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2071789
    invoke-direct {p0}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2071788
    invoke-direct {p0}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionCoreTextComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2071785
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2071786
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2071787
    invoke-static {v0}, LX/E1s;->a(LX/9uc;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2071784
    const/4 v0, 0x0

    return-object v0
.end method
