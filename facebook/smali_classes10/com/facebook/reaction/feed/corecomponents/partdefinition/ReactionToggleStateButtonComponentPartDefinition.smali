.class public Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/E1s;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E1s;LX/2d1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071816
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2071817
    iput-object p2, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->d:LX/E1s;

    .line 2071818
    iput-object p3, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->e:LX/2d1;

    .line 2071819
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2071831
    iget-object v0, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->d:LX/E1s;

    .line 2071832
    iget-object p0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object p0, p0

    .line 2071833
    invoke-static {v0, p1, p3, p0, p2}, LX/E1s;->a(LX/E1s;LX/1De;LX/2km;LX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E2I;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v0, p0

    .line 2071834
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;
    .locals 6

    .prologue
    .line 2071820
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;

    monitor-enter v1

    .line 2071821
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071822
    sput-object v2, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071823
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071824
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071825
    new-instance p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E1s;->a(LX/0QB;)LX/E1s;

    move-result-object v4

    check-cast v4, LX/E1s;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;-><init>(Landroid/content/Context;LX/E1s;LX/2d1;)V

    .line 2071826
    move-object v0, p0

    .line 2071827
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071828
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071829
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071830
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2071815
    iget-object v0, p0, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2071835
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2071814
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2071813
    invoke-direct {p0}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2071808
    invoke-direct {p0}, Lcom/facebook/reaction/feed/corecomponents/partdefinition/ReactionToggleStateButtonComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2071810
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2071811
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2071812
    invoke-static {v0}, LX/E1s;->a(LX/9uc;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2071809
    const/4 v0, 0x0

    return-object v0
.end method
