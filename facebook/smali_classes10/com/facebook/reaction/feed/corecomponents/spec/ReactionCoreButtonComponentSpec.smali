.class public Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;


# instance fields
.field public final b:LX/E28;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2071903
    const-class v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E28;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E28;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2071905
    iput-object p1, p0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->b:LX/E28;

    .line 2071906
    iput-object p2, p0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->c:LX/0Or;

    .line 2071907
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;
    .locals 5

    .prologue
    .line 2071908
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->d:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    if-nez v0, :cond_1

    .line 2071909
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    monitor-enter v1

    .line 2071910
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->d:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2071911
    if-eqz v2, :cond_0

    .line 2071912
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2071913
    new-instance v4, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    invoke-static {v0}, LX/E28;->a(LX/0QB;)LX/E28;

    move-result-object v3

    check-cast v3, LX/E28;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;-><init>(LX/E28;LX/0Or;)V

    .line 2071914
    move-object v0, v4

    .line 2071915
    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->d:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071916
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2071917
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2071918
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->d:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    return-object v0

    .line 2071919
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2071920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
