.class public Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072128
    const-class v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072130
    iput-object p1, p0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->b:LX/0Or;

    .line 2072131
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;
    .locals 4

    .prologue
    .line 2072132
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->c:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    if-nez v0, :cond_1

    .line 2072133
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    monitor-enter v1

    .line 2072134
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->c:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072135
    if-eqz v2, :cond_0

    .line 2072136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072137
    new-instance v3, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;-><init>(LX/0Or;)V

    .line 2072138
    move-object v0, v3

    .line 2072139
    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->c:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072142
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->c:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    return-object v0

    .line 2072143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
