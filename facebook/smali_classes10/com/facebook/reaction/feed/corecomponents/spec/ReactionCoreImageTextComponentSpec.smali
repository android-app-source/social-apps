.class public Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:I

.field private static volatile e:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;


# instance fields
.field public final c:LX/E28;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072217
    const-class v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2072218
    const v0, 0x7f0b163d

    sput v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->b:I

    return-void
.end method

.method public constructor <init>(LX/E28;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E28;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072220
    iput-object p1, p0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->c:LX/E28;

    .line 2072221
    iput-object p2, p0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->d:LX/0Or;

    .line 2072222
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;
    .locals 5

    .prologue
    .line 2072223
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->e:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    if-nez v0, :cond_1

    .line 2072224
    const-class v1, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    monitor-enter v1

    .line 2072225
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->e:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072226
    if-eqz v2, :cond_0

    .line 2072227
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072228
    new-instance v4, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    invoke-static {v0}, LX/E28;->a(LX/0QB;)LX/E28;

    move-result-object v3

    check-cast v3, LX/E28;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;-><init>(LX/E28;LX/0Or;)V

    .line 2072229
    move-object v0, v4

    .line 2072230
    sput-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->e:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072231
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072232
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072233
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->e:Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    return-object v0

    .line 2072234
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072235
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
