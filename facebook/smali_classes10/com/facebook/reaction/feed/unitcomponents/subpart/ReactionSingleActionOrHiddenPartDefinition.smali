.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079647
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079648
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    .line 2079649
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;
    .locals 4

    .prologue
    .line 2079636
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    monitor-enter v1

    .line 2079637
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079638
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079639
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079640
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079641
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;)V

    .line 2079642
    move-object v0, p0

    .line 2079643
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079644
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079645
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079626
    check-cast p2, LX/E67;

    .line 2079627
    if-eqz p2, :cond_0

    .line 2079628
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079629
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x15415f4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079632
    check-cast p1, LX/E67;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 2079633
    if-nez p1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2079634
    const/16 v1, 0x1f

    const v2, 0x6d820953

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2079635
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/E67;)Z
    .locals 1
    .param p1    # LX/E67;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079631
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a(LX/E67;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079630
    check-cast p1, LX/E67;

    invoke-virtual {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a(LX/E67;)Z

    move-result v0

    return v0
.end method
