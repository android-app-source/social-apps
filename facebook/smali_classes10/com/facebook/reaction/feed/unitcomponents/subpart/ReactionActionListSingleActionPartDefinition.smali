.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2kp;",
        ":",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/E1g;

.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;


# direct methods
.method public constructor <init>(LX/E1g;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079113
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079114
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->a:LX/E1g;

    .line 2079115
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2079116
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    .line 2079117
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079118
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    .line 2079119
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;
    .locals 9

    .prologue
    .line 2079120
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    monitor-enter v1

    .line 2079121
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079122
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079125
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    invoke-static {v0}, LX/E1g;->a(LX/0QB;)LX/E1g;

    move-result-object v4

    check-cast v4, LX/E1g;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;-><init>(LX/E1g;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;)V

    .line 2079126
    move-object v0, v3

    .line 2079127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2079131
    check-cast p2, LX/E67;

    const/4 v1, 0x0

    .line 2079132
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v2, LX/E1o;

    iget-object v3, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v4, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079133
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2079134
    iget-object v5, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079135
    iget-object p3, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, p3

    .line 2079136
    invoke-direct {v2, v3, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079137
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->a:LX/E1g;

    iget-object v2, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    const v3, -0x6e685d

    invoke-virtual {v0, v2, v3}, LX/E1g;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2079138
    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    .line 2079139
    :goto_0
    if-eqz v0, :cond_1

    .line 2079140
    const v2, 0x7f0d28b3

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079141
    :goto_1
    const v0, 0x7f0d28b4

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v3, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079142
    return-object v1

    :cond_0
    move-object v0, v1

    .line 2079143
    goto :goto_0

    .line 2079144
    :cond_1
    const v0, 0x7f0d28b3

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    new-instance v4, LX/E5w;

    invoke-direct {v4, v2}, LX/E5w;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2079145
    check-cast p1, LX/E67;

    .line 2079146
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
