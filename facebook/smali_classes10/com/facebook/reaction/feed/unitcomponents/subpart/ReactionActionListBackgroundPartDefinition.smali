.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079012
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079013
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;
    .locals 3

    .prologue
    .line 2079001
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    monitor-enter v1

    .line 2079002
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079003
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079004
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079005
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2079006
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;-><init>()V

    .line 2079007
    move-object v0, v0

    .line 2079008
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079009
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079010
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079011
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3b05fa16

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2078994
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2078995
    if-nez p1, :cond_0

    .line 2078996
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x2526c9cd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2078997
    :cond_0
    sget-object v1, LX/E5q;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2078998
    const v1, 0x7f0215c7

    .line 2078999
    :goto_1
    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 2079000
    :pswitch_0
    const v1, 0x7f0215b5

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
