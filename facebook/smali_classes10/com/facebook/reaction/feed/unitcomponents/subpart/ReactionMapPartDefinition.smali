.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E63;",
        "LX/E64;",
        "LX/1Pn;",
        "Lcom/facebook/maps/FbStaticMapView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0yH;


# direct methods
.method public constructor <init>(LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079570
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079571
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;->a:LX/0yH;

    .line 2079572
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;
    .locals 4

    .prologue
    .line 2079573
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    monitor-enter v1

    .line 2079574
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079575
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079576
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079577
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079578
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    invoke-static {v0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v3

    check-cast v3, LX/0yH;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;-><init>(LX/0yH;)V

    .line 2079579
    move-object v0, p0

    .line 2079580
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079581
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079582
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2079584
    check-cast p2, LX/E63;

    .line 2079585
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "local_serps"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 2079586
    iget-object v1, p2, LX/E63;->a:LX/1k1;

    invoke-interface {v1}, LX/1k1;->a()D

    move-result-wide v2

    iget-object v1, p2, LX/E63;->a:LX/1k1;

    invoke-interface {v1}, LX/1k1;->b()D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2079587
    iget-object v1, p2, LX/E63;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    if-eqz v1, :cond_0

    .line 2079588
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p2, LX/E63;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->d()D

    move-result-wide v2

    double-to-float v2, v2

    iget-object v3, p2, LX/E63;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->b()D

    move-result-wide v4

    double-to-float v3, v4

    iget-object v4, p2, LX/E63;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->a()D

    move-result-wide v4

    double-to-float v4, v4

    iget-object v5, p2, LX/E63;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->c()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2079589
    :goto_0
    new-instance v1, LX/E64;

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;->a:LX/0yH;

    sget-object v3, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v2, v3}, LX/0yH;->a(LX/0yY;)Z

    move-result v2

    invoke-direct {v1, v2, v0}, LX/E64;-><init>(ZLcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    return-object v1

    .line 2079590
    :cond_0
    iget v1, p2, LX/E63;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x548776bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079591
    check-cast p2, LX/E64;

    check-cast p4, Lcom/facebook/maps/FbStaticMapView;

    const/16 v2, 0x8

    .line 2079592
    iget-boolean v1, p2, LX/E64;->a:Z

    if-eqz v1, :cond_0

    .line 2079593
    invoke-virtual {p4, v2}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2079594
    :goto_0
    const/16 v1, 0x1f

    const v2, 0xa0a4f7d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2079595
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2079596
    invoke-virtual {p4, v2}, LX/3BP;->setReportButtonVisibility(I)V

    .line 2079597
    iget-object v1, p2, LX/E64;->b:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {p4, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    goto :goto_0
.end method
