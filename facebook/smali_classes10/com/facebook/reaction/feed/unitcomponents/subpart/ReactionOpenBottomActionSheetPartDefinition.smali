.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E66;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/E6E;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/E6E;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079607
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079608
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079609
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->b:LX/E6E;

    .line 2079610
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;
    .locals 5

    .prologue
    .line 2079611
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    monitor-enter v1

    .line 2079612
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079613
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079616
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/E6E;->b(LX/0QB;)LX/E6E;

    move-result-object v4

    check-cast v4, LX/E6E;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/E6E;)V

    .line 2079617
    move-object v0, p0

    .line 2079618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2079622
    check-cast p2, LX/E66;

    check-cast p3, LX/2km;

    .line 2079623
    new-instance v0, LX/E65;

    invoke-direct {v0, p0, p2, p3}, LX/E65;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;LX/E66;LX/2km;)V

    .line 2079624
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079625
    const/4 v0, 0x0

    return-object v0
.end method
