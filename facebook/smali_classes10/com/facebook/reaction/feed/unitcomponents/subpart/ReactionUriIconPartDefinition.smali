.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2079721
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079722
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079723
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;
    .locals 3

    .prologue
    .line 2079724
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    monitor-enter v1

    .line 2079725
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079726
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079727
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079728
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2079729
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;-><init>()V

    .line 2079730
    move-object v0, v0

    .line 2079731
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079732
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079733
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x193df985

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079735
    check-cast p1, Ljava/lang/String;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2079736
    if-eqz p1, :cond_0

    .line 2079737
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2079738
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2079739
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x374894bc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2079740
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
