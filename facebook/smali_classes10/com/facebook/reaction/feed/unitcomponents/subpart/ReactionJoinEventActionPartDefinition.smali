.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:LX/0wM;

.field private final c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field public final d:LX/7vW;

.field public final e:LX/7vZ;

.field private final f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

.field public final g:LX/1Ck;

.field private final h:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final i:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/7vW;LX/7vZ;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;LX/1Ck;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079486
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079487
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079488
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    .line 2079489
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->b:LX/0wM;

    .line 2079490
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2079491
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->d:LX/7vW;

    .line 2079492
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->e:LX/7vZ;

    .line 2079493
    iput-object p7, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->g:LX/1Ck;

    .line 2079494
    iput-object p8, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->h:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 2079495
    iput-object p9, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->i:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079496
    return-void
.end method

.method public static a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2079560
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->b:LX/0wM;

    const v1, -0xa76f01

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;
    .locals 13

    .prologue
    .line 2079548
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    monitor-enter v1

    .line 2079549
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079550
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079551
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079552
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079553
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v7

    check-cast v7, LX/7vW;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v8

    check-cast v8, LX/7vZ;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/7vW;LX/7vZ;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;LX/1Ck;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2079554
    move-object v0, v3

    .line 2079555
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079556
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079557
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079558
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2079559
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->b:LX/0wM;

    const v1, -0x6e685d

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079547
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2079499
    check-cast p2, LX/E67;

    check-cast p3, LX/1Pn;

    const/4 v10, 0x0

    .line 2079500
    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->p()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v2

    .line 2079501
    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->r()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    .line 2079502
    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v8

    move-object v0, p3

    .line 2079503
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v5, "unknown"

    :goto_0
    move-object v0, p3

    .line 2079504
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v6, "unknown"

    :goto_1
    move-object v0, p3

    .line 2079505
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v7, "unknown"

    .line 2079506
    :goto_2
    new-instance v0, LX/E62;

    move-object v1, p0

    move-object v3, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v9}, LX/E62;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;ZLX/E67;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/1Pn;)V

    .line 2079507
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079508
    const v1, 0x7f0d28b3

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    new-instance v5, LX/E5w;

    if-eqz v2, :cond_5

    .line 2079509
    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->d(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2079510
    const v0, 0x7f0207d6

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2079511
    :goto_3
    move-object v0, v0

    .line 2079512
    :goto_4
    invoke-direct {v5, v0}, LX/E5w;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1, v1, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079513
    const v1, 0x7f0d28b4

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->h:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2079514
    if-eqz v2, :cond_9

    .line 2079515
    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->d(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v6, :cond_1

    :cond_0
    move v0, v5

    .line 2079516
    :cond_1
    :goto_5
    move v0, v0

    .line 2079517
    if-eqz v0, :cond_6

    const v0, 0x7f0e08f5

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079518
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2079519
    if-eqz v2, :cond_e

    .line 2079520
    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->d(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2079521
    const v1, 0x7f08129b

    .line 2079522
    :goto_7
    move v1, v1

    .line 2079523
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2079524
    const v1, 0x7f0d28b4

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->i:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079525
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v10}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079526
    return-object v10

    :cond_2
    move-object v0, p3

    .line 2079527
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    iget-object v5, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    move-object v0, p3

    .line 2079528
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    iget-object v6, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    move-object v0, p3

    .line 2079529
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    iget-object v7, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    goto/16 :goto_2

    .line 2079530
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v8, v0, :cond_10

    .line 2079531
    const v0, 0x7f0207d6

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2079532
    :goto_8
    move-object v0, v0

    .line 2079533
    goto :goto_4

    .line 2079534
    :cond_6
    const v0, 0x7f0e08f6

    goto :goto_6

    .line 2079535
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v0, :cond_8

    .line 2079536
    const v0, 0x7f020855

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_3

    .line 2079537
    :cond_8
    const v0, 0x7f020855

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->b(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v8, v6, :cond_a

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v8, v6, :cond_1

    :cond_a
    move v0, v5

    goto :goto_5

    .line 2079538
    :cond_b
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v1, :cond_c

    .line 2079539
    const v1, 0x7f08129c

    goto :goto_7

    .line 2079540
    :cond_c
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v1, :cond_d

    .line 2079541
    const v1, 0x7f08129d

    goto :goto_7

    .line 2079542
    :cond_d
    const v1, 0x7f08129f

    goto :goto_7

    .line 2079543
    :cond_e
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v8, v1, :cond_f

    const v1, 0x7f08129b

    goto :goto_7

    :cond_f
    const v1, 0x7f08129e

    goto/16 :goto_7

    .line 2079544
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v8, v0, :cond_11

    .line 2079545
    const v0, 0x7f02085a

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_8

    .line 2079546
    :cond_11
    const v0, 0x7f02085a

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->b(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_8
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2079497
    check-cast p1, LX/E67;

    .line 2079498
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->JOIN_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
