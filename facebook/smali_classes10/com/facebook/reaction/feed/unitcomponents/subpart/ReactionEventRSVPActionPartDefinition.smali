.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kl;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field public final e:LX/7vW;

.field public final f:LX/7vZ;

.field private final g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final h:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/7vW;LX/7vZ;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079281
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079282
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->c:Landroid/os/Handler;

    .line 2079283
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079284
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    .line 2079285
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2079286
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->e:LX/7vW;

    .line 2079287
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->f:LX/7vZ;

    .line 2079288
    iput-object p7, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 2079289
    iput-object p8, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->h:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079290
    return-void
.end method

.method public static a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E67;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2079358
    sget-object v0, LX/E5z;->a:[I

    iget-object v1, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v3, v8

    .line 2079359
    :goto_0
    new-instance v0, LX/E5x;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, LX/E5x;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Pq;LX/Cfc;)V

    return-object v0

    .line 2079360
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2079361
    sget-object v8, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    goto :goto_0

    .line 2079362
    :pswitch_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2079363
    sget-object v8, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    goto :goto_0

    .line 2079364
    :pswitch_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2079365
    sget-object v8, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;
    .locals 12

    .prologue
    .line 2079347
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    monitor-enter v1

    .line 2079348
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079349
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079350
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079351
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079352
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v8

    check-cast v8, LX/7vW;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v9

    check-cast v9, LX/7vZ;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;-><init>(Landroid/os/Handler;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/7vW;LX/7vZ;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2079353
    move-object v0, v3

    .line 2079354
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079355
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079356
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079357
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2079346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;LX/Cfc;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E67;",
            "TE;",
            "LX/Cfc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2079330
    iget-object v0, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079331
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2079332
    iget-object v0, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v0, p2

    .line 2079333
    check-cast v0, LX/1Pr;

    new-instance v3, LX/E2Q;

    invoke-direct {v3, v1}, LX/E2Q;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v3, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2R;

    .line 2079334
    iget-object v1, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2079335
    if-nez v1, :cond_0

    .line 2079336
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->c:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition$3;

    invoke-direct {v3, p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition$3;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;)V

    const-wide/16 v4, 0x7d0

    const v6, 0x5a42d63a

    invoke-static {v1, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2079337
    :cond_0
    iget-object v1, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079338
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2079339
    iget-object v3, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v1, v3}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;

    move-result-object v1

    .line 2079340
    iput-object v1, v0, LX/E2R;->a:Ljava/lang/String;

    .line 2079341
    move-object v0, p2

    .line 2079342
    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    iget-object v1, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, p3}, LX/2jc;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V

    move-object v0, p2

    .line 2079343
    check-cast v0, LX/2kk;

    iget-object v1, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2kk;->a(LX/2nq;)V

    .line 2079344
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-interface {p2, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2079345
    return-void
.end method

.method public static b(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E67;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2079322
    sget-object v0, LX/E5z;->a:[I

    iget-object v1, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v3, v8

    .line 2079323
    :goto_0
    new-instance v0, LX/E5y;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, LX/E5y;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Pq;LX/Cfc;)V

    return-object v0

    .line 2079324
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2079325
    sget-object v8, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    goto :goto_0

    .line 2079326
    :pswitch_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2079327
    sget-object v8, LX/Cfc;->EVENT_CARD_INTERESTED_TAP:LX/Cfc;

    goto :goto_0

    .line 2079328
    :pswitch_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2079329
    sget-object v8, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2079293
    check-cast p2, LX/E67;

    check-cast p3, LX/1Pq;

    const/4 v3, 0x0

    .line 2079294
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079295
    move-object v4, p3

    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v7, "unknown"

    :goto_0
    move-object v4, p3

    .line 2079296
    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v8, "unknown"

    :goto_1
    move-object v4, p3

    .line 2079297
    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    if-nez v4, :cond_4

    const-string v9, "unknown"

    .line 2079298
    :goto_2
    iget-object v4, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->q()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v4

    .line 2079299
    if-eqz v4, :cond_5

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    .line 2079300
    invoke-static/range {v4 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 2079301
    :goto_3
    move-object v1, v4

    .line 2079302
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079303
    const v0, 0x7f0d28b4

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->h:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079304
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079305
    const v0, 0x7f0d28b3

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    new-instance v2, LX/E5w;

    invoke-direct {v2, v3}, LX/E5w;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079306
    check-cast p3, LX/1Pr;

    new-instance v0, LX/E2Q;

    iget-object v1, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079307
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2079308
    invoke-direct {v0, v1}, LX/E2Q;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2R;

    .line 2079309
    iget-object v1, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2079310
    if-eqz v1, :cond_0

    .line 2079311
    iget-object v1, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2079312
    iget-object v1, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079313
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2079314
    iget-object v2, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v1, v2}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2079315
    const v1, 0x7f0d28b4

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    if-eqz v0, :cond_1

    const v0, 0x7f0e08f3

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079316
    :cond_0
    return-object v3

    .line 2079317
    :cond_1
    const v0, 0x7f0e08f4

    goto :goto_4

    :cond_2
    move-object v4, p3

    .line 2079318
    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    iget-object v7, v4, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    move-object v4, p3

    .line 2079319
    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    iget-object v8, v4, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    move-object v4, p3

    .line 2079320
    check-cast v4, LX/2kn;

    invoke-interface {v4}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v4

    iget-object v9, v4, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    goto/16 :goto_2

    :cond_5
    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    .line 2079321
    invoke-static/range {v4 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->b(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v4

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2079291
    check-cast p1, LX/E67;

    .line 2079292
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->MAYBE_GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->NOT_GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->q()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
