.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;",
            "LX/1Nt;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field public final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

.field public final d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

.field public final e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079654
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079655
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->b:LX/03V;

    .line 2079656
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    .line 2079657
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    .line 2079658
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    .line 2079659
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;

    .line 2079660
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->g:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    .line 2079661
    iput-object p7, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    .line 2079662
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    .line 2079663
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079664
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNHIGHLIGHT_RICH_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079665
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->JOIN_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079666
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->MAYBE_GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079667
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->NOT_GOING_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079668
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VERTICAL_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079669
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iget-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->g:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079670
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;)LX/1Nt;
    .locals 1

    .prologue
    .line 2079671
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079672
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    .line 2079673
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;
    .locals 11

    .prologue
    .line 2079674
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    monitor-enter v1

    .line 2079675
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079676
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079677
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079678
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079679
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;-><init>(LX/03V;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListOpenBottomMenuPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;)V

    .line 2079680
    move-object v0, v3

    .line 2079681
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079682
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079683
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2079685
    check-cast p2, LX/E67;

    .line 2079686
    iget-object v0, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;)LX/1Nt;

    move-result-object v0

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079687
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/E67;)Z
    .locals 3

    .prologue
    .line 2079688
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;)LX/1Nt;

    move-result-object v0

    .line 2079689
    instance-of v1, v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    if-nez v1, :cond_0

    .line 2079690
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Action subpart does not implement MultiRowPartWithIsNeeded"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2079691
    const/4 v0, 0x0

    .line 2079692
    :goto_0
    return v0

    :cond_0
    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-interface {v0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2079693
    check-cast p1, LX/E67;

    invoke-virtual {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionSelectorPartDefinition;->a(LX/E67;)Z

    move-result v0

    return v0
.end method
