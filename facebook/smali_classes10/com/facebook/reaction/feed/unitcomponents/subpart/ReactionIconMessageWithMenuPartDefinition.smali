.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079452
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079453
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;

    .line 2079454
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    .line 2079455
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079456
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;
    .locals 6

    .prologue
    .line 2079429
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    monitor-enter v1

    .line 2079430
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079431
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079432
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079433
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079434
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2079435
    move-object v0, p0

    .line 2079436
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079437
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079438
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2079444
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 2079445
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2079446
    const v2, 0x7f0d28a2

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079447
    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2079448
    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    .line 2079449
    :goto_0
    const v2, 0x7f0d28a9

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079450
    const v0, 0x7f0d28aa

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionChevronMenuPartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079451
    return-object v1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x806ae55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079440
    check-cast p4, Landroid/widget/LinearLayout;

    .line 2079441
    const v1, 0x7f0d28a7

    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2079442
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2079443
    const/16 v1, 0x1f

    const v2, -0x3d7ec428

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
