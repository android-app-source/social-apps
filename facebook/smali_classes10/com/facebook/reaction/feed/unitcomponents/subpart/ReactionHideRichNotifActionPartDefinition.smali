.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

.field public final c:Landroid/os/Handler;

.field private final d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079402
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079403
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->c:Landroid/os/Handler;

    .line 2079404
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079405
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    .line 2079406
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2079407
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079408
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;
    .locals 9

    .prologue
    .line 2079409
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    monitor-enter v1

    .line 2079410
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079411
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079412
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079413
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079414
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;-><init>(Landroid/os/Handler;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2079415
    move-object v0, v3

    .line 2079416
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079417
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079418
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079419
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2079420
    check-cast p2, LX/E67;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 2079421
    iget-object v0, p2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2079422
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/E60;

    invoke-direct {v2, p0, p2, p3, v0}, LX/E60;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;LX/E67;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079423
    const v0, 0x7f0d28b4

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082234

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079424
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079425
    const v0, 0x7f0d28b3

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    new-instance v2, LX/E5w;

    invoke-direct {v2, v4}, LX/E5w;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079426
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2079427
    check-cast p1, LX/E67;

    .line 2079428
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNHIGHLIGHT_RICH_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
