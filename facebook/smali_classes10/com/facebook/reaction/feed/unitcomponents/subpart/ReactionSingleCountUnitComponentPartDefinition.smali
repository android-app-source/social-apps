.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E68;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:LX/154;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/154;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079699
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079700
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2079701
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->c:LX/154;

    .line 2079702
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2079703
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2079710
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    monitor-enter v1

    .line 2079711
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079712
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079713
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079714
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079715
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v4

    check-cast v4, LX/154;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/154;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2079716
    move-object v0, p0

    .line 2079717
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079718
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079719
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079720
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2079704
    check-cast p2, LX/E68;

    .line 2079705
    const v0, 0x7f0d2929

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/E68;->a:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->c()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079706
    const v0, 0x7f0d292a

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->c:LX/154;

    iget-object v3, p2, LX/E68;->a:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->b()I

    move-result v3

    invoke-virtual {v2, v3}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079707
    iget-object v0, p2, LX/E68;->a:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2079708
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v1, LX/E1o;

    iget-object v2, p2, LX/E68;->a:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p2, LX/E68;->b:Ljava/lang/String;

    iget-object v4, p2, LX/E68;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079709
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
