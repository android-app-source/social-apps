.class public final Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/E2Y;

.field public final synthetic b:LX/9uc;

.field public final synthetic c:LX/2km;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;LX/E2Y;LX/9uc;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2079191
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;

    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->a:LX/E2Y;

    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->b:LX/9uc;

    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->c:LX/2km;

    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2079192
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->a:LX/E2Y;

    const/4 v1, 0x1

    .line 2079193
    iget-object v2, v0, LX/E2Y;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    move v0, v2

    .line 2079194
    if-nez v0, :cond_0

    .line 2079195
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->e:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->a:LX/E1f;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->b:LX/9uc;

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->c:LX/2km;

    check-cast v2, LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->c:LX/2km;

    check-cast v4, LX/2kp;

    invoke-interface {v4}, LX/2kp;->t()LX/2jY;

    move-result-object v4

    .line 2079196
    iget-object v5, v4, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2079197
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->c:LX/2km;

    check-cast v5, LX/2kp;

    invoke-interface {v5}, LX/2kp;->t()LX/2jY;

    move-result-object v5

    .line 2079198
    iget-object v6, v5, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v6

    .line 2079199
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079200
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v7

    .line 2079201
    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079202
    iget-object v8, v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2079203
    invoke-virtual/range {v0 .. v7}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2079204
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->c:LX/2km;

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079205
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2079206
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079207
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2079208
    invoke-interface {v1, v2, v3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2079209
    :cond_0
    return-void
.end method
