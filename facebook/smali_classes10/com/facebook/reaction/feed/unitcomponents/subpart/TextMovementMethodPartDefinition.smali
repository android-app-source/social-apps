.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Landroid/text/method/MovementMethod;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079741
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079742
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;
    .locals 3

    .prologue
    .line 2079743
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    monitor-enter v1

    .line 2079744
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079745
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079746
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079747
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2079748
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;-><init>()V

    .line 2079749
    move-object v0, v0

    .line 2079750
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079751
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079752
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1d213f75

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079754
    check-cast p1, Landroid/text/method/MovementMethod;

    check-cast p4, Landroid/widget/TextView;

    .line 2079755
    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2079756
    const/16 v1, 0x1f

    const v2, -0x43153918

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
