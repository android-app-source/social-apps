.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/E1f;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/E1f;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079215
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079216
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->a:LX/E1f;

    .line 2079217
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079218
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->c:Landroid/os/Handler;

    .line 2079219
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;
    .locals 6

    .prologue
    .line 2079231
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;

    monitor-enter v1

    .line 2079232
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079233
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079234
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079235
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079236
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;-><init>(LX/E1f;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/os/Handler;)V

    .line 2079237
    move-object v0, p0

    .line 2079238
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079239
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079240
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2079220
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 2079221
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 2079222
    move-object v0, p3

    .line 2079223
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2X;

    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-direct {v1, v2}, LX/E2X;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2Y;

    .line 2079224
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition$1;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;LX/E2Y;LX/9uc;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2079225
    invoke-interface {v3}, LX/9uc;->u()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    .line 2079226
    :goto_0
    if-eqz v1, :cond_0

    .line 2079227
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->c:Landroid/os/Handler;

    invoke-interface {v3}, LX/9uc;->u()I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const v3, 0x62bba8db

    invoke-static {v2, v0, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2079228
    :cond_0
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/E5v;

    invoke-direct {v3, p0, v1, v0}, LX/E5v;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDelayedActionPartDefinition;ZLjava/lang/Runnable;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079229
    const/4 v0, 0x0

    return-object v0

    .line 2079230
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
