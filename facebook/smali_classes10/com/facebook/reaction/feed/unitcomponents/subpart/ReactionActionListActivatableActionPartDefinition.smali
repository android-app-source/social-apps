.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E5p;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/E1g;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(LX/E1g;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078965
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2078966
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->a:LX/E1g;

    .line 2078967
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    .line 2078968
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2078969
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->d:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 2078970
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2078971
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2078972
    check-cast p2, LX/E5p;

    check-cast p3, LX/1Pr;

    const/4 v5, 0x0

    .line 2078973
    iget-object v0, p2, LX/E5p;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-boolean v1, p2, LX/E5p;->c:Z

    .line 2078974
    new-instance v2, LX/E2T;

    .line 2078975
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2078976
    invoke-interface {v3}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/E2T;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2S;

    .line 2078977
    iget-object v3, v2, LX/E2S;->a:LX/03R;

    move-object v3, v3

    .line 2078978
    invoke-virtual {v3}, LX/03R;->isSet()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2078979
    iget-object v3, v2, LX/E2S;->a:LX/03R;

    move-object v3, v3

    .line 2078980
    :goto_0
    invoke-virtual {v2, v3}, LX/E2S;->a(LX/03R;)V

    .line 2078981
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v3, v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 2078982
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->a:LX/E1g;

    iget-object v0, p2, LX/E5p;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v3

    if-eqz v1, :cond_0

    const v0, -0xa76f01

    :goto_2
    invoke-virtual {v2, v3, v0}, LX/E1g;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2078983
    const v2, 0x7f0d28b3

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionDrawableIconPartDefinition;

    new-instance v4, LX/E5w;

    invoke-direct {v4, v0}, LX/E5w;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2078984
    const v2, 0x7f0d28b4

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->d:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    if-eqz v1, :cond_1

    const v0, 0x7f0e08f3

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2078985
    const v2, 0x7f0d28b4

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v1, :cond_2

    iget-object v0, p2, LX/E5p;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2078986
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListActivatableActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2078987
    return-object v5

    .line 2078988
    :cond_0
    const v0, -0x6e685d

    goto :goto_2

    .line 2078989
    :cond_1
    const v0, 0x7f0e08f4

    goto :goto_3

    .line 2078990
    :cond_2
    iget-object v0, p2, LX/E5p;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 2078991
    :cond_3
    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    goto :goto_0

    .line 2078992
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
