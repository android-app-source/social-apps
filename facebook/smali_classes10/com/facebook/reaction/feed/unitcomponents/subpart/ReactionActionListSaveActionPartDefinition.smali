.class public Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E67;",
        "LX/E2g;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "LX/E67;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/E2i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2079066
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSingleActionPartDefinition;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E2i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079096
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2079097
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2079098
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2079099
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 2079100
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->e:LX/E2i;

    .line 2079101
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;
    .locals 7

    .prologue
    .line 2079085
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    monitor-enter v1

    .line 2079086
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2079087
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2079088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2079089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2079090
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/E2i;->b(LX/0QB;)LX/E2i;

    move-result-object v6

    check-cast v6, LX/E2i;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E2i;)V

    .line 2079091
    move-object v0, p0

    .line 2079092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2079093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2079094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2079095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;LX/1Pn;LX/5sc;LX/E2g;Landroid/widget/TextView;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/5sc;",
            "LX/E2g;",
            "Landroid/widget/TextView;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2079102
    move-object v0, p1

    check-cast v0, LX/1Pr;

    invoke-static {v0, p2, p3, p5}, LX/E2i;->a(LX/1Pr;LX/5sc;LX/E2g;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v4

    .line 2079103
    if-nez v4, :cond_0

    const/4 v0, 0x1

    move v7, v0

    .line 2079104
    :goto_0
    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7}, LX/E2i;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 2079105
    check-cast v0, LX/1Pr;

    invoke-static {v0, v7, p2, p5}, LX/E2i;->a(LX/1Pr;ZLX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2079106
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->e:LX/E2i;

    new-instance v0, LX/E5s;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v5, p2

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/E5s;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;Landroid/widget/TextView;LX/1Pn;ZLX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-virtual {v8, v7, p2, v0}, LX/E2i;->a(ZLX/5sc;LX/2h0;)V

    .line 2079107
    check-cast p1, LX/3U9;

    invoke-interface {p1}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    .line 2079108
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2079109
    iget-object v2, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2079110
    new-instance v3, LX/Cfl;

    invoke-interface {p2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    invoke-direct {v3, v4, v5}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v0, v1, v2, v3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2079111
    return-void

    .line 2079112
    :cond_0
    const/4 v0, 0x0

    move v7, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2079075
    check-cast p2, LX/E67;

    check-cast p3, LX/1Pn;

    .line 2079076
    const v0, 0x7f0d28b3

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v2

    iget-object v3, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2079077
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2079078
    move-object v2, v2

    .line 2079079
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2079080
    new-instance v0, LX/E2g;

    iget-object v1, p2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E2g;-><init>(LX/5sc;)V

    .line 2079081
    new-instance v1, LX/E5r;

    invoke-direct {v1, p0, p3, p2, v0}, LX/E5r;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;LX/1Pn;LX/E67;LX/E2g;)V

    .line 2079082
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079083
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2079084
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x10376653

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079069
    check-cast p1, LX/E67;

    check-cast p2, LX/E2g;

    check-cast p3, LX/1Pn;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 2079070
    const v1, 0x7f0d28b4

    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2079071
    if-nez v1, :cond_0

    .line 2079072
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6da86445

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v2, p3

    .line 2079073
    check-cast v2, LX/1Pr;

    iget-object v4, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    iget-object p0, p1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v2, v4, p2, p0}, LX/E2i;->a(LX/1Pr;LX/5sc;LX/E2g;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v2

    .line 2079074
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, LX/E2i;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2079067
    check-cast p1, LX/E67;

    .line 2079068
    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-static {v0}, LX/E2i;->a(LX/5sc;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
