.class public Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;


# instance fields
.field public final b:LX/E1f;

.field public final c:LX/1Ad;

.field public final d:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2077200
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1f;LX/1Ad;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2077171
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->b:LX/E1f;

    .line 2077172
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->c:LX/1Ad;

    .line 2077173
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->d:LX/1vg;

    .line 2077174
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;
    .locals 6

    .prologue
    .line 2077187
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    if-nez v0, :cond_1

    .line 2077188
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    monitor-enter v1

    .line 2077189
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077190
    if-eqz v2, :cond_0

    .line 2077191
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077192
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;-><init>(LX/E1f;LX/1Ad;LX/1vg;)V

    .line 2077193
    move-object v0, p0

    .line 2077194
    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077195
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077196
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077197
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    return-object v0

    .line 2077198
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 9
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Ljava/lang/String;",
            "TE;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2077175
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->b:LX/E1f;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2077176
    iget-object v4, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2077177
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2077178
    iget-object v5, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2077179
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v1

    .line 2077180
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v1

    .line 2077181
    move-object v1, p4

    check-cast v1, LX/2kn;

    invoke-interface {v1}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    move-object v1, p2

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    .line 2077182
    iget-object v1, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2077183
    iget-object v2, p5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2077184
    invoke-interface {p4, v1, v2, p3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2077185
    return-void

    .line 2077186
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
