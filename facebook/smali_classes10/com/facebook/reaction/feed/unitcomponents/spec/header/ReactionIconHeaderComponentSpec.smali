.class public Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/E56;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2078861
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/E56;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/E56;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078863
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->b:LX/0Or;

    .line 2078864
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->c:LX/E56;

    .line 2078865
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;
    .locals 5

    .prologue
    .line 2078866
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    if-nez v0, :cond_1

    .line 2078867
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    monitor-enter v1

    .line 2078868
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078869
    if-eqz v2, :cond_0

    .line 2078870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078871
    new-instance v4, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/E56;->a(LX/0QB;)LX/E56;

    move-result-object v3

    check-cast v3, LX/E56;

    invoke-direct {v4, p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;-><init>(LX/0Or;LX/E56;)V

    .line 2078872
    move-object v0, v4

    .line 2078873
    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078876
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    return-object v0

    .line 2078877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
