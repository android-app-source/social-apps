.class public Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/1X1;

.field private static volatile e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;


# instance fields
.field private final c:LX/8yV;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2078206
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2078207
    new-instance v0, LX/E5I;

    invoke-direct {v0}, LX/E5I;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->b:LX/1X1;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/8yV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/8yV;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078203
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->d:LX/0Or;

    .line 2078204
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->c:LX/8yV;

    .line 2078205
    return-void
.end method

.method private static a(LX/1De;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;LX/1Ad;)LX/1Dh;
    .locals 6

    .prologue
    const/16 v2, 0x14

    const/4 v4, 0x2

    .line 2078194
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    .line 2078195
    if-eqz p4, :cond_0

    .line 2078196
    invoke-static {p0}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v1

    invoke-virtual {p5, p4}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v3

    sget-object v5, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2078197
    :goto_0
    move-object v1, v1

    .line 2078198
    invoke-static {p0, v1}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b163c

    invoke-interface {v1, v4, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0112

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0112

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    .line 2078199
    :cond_0
    const/high16 v1, -0x80000000

    if-eq p3, v1, :cond_1

    .line 2078200
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    goto :goto_0

    .line 2078201
    :cond_1
    sget-object v1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->b:LX/1X1;

    goto :goto_0
.end method

.method private static a(LX/1De;Landroid/net/Uri;LX/1Ad;)LX/1X5;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2078208
    if-eqz p1, :cond_0

    .line 2078209
    invoke-static {p0}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    invoke-static {p0}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v1

    const v2, 0x7f0a010a

    invoke-virtual {v1, v2}, LX/1nh;->i(I)LX/1nh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v0

    invoke-virtual {p2, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    .line 2078210
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    .line 2078211
    iget-object v2, v0, LX/1o5;->a:LX/1o3;

    invoke-virtual {v0, v1, v1}, LX/1Dp;->d(II)LX/1dc;

    move-result-object p0

    iput-object p0, v2, LX/1o3;->a:LX/1dc;

    .line 2078212
    iget-object v2, v0, LX/1o5;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2078213
    move-object v0, v0

    .line 2078214
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;
    .locals 5

    .prologue
    .line 2078181
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    if-nez v0, :cond_1

    .line 2078182
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    monitor-enter v1

    .line 2078183
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078184
    if-eqz v2, :cond_0

    .line 2078185
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078186
    new-instance v4, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v3

    check-cast v3, LX/8yV;

    invoke-direct {v4, p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;-><init>(LX/0Or;LX/8yV;)V

    .line 2078187
    move-object v0, v4

    .line 2078188
    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078189
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078190
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078191
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->e:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    return-object v0

    .line 2078192
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILandroid/net/Uri;)LX/1Dg;
    .locals 9
    .param p2    # Ljava/util/List;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "I",
            "Landroid/net/Uri;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2078179
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Ad;

    .line 2078180
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1, p5, v6}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a(LX/1De;Landroid/net/Uri;LX/1Ad;)LX/1X5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f0215c4

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b163a

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->c:LX/8yV;

    invoke-virtual {v1, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v1

    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v1

    const v2, 0x7f0b1618

    invoke-virtual {v1, v2}, LX/8yU;->h(I)LX/8yU;

    move-result-object v1

    const v2, 0x7f0b1619

    invoke-virtual {v1, v2}, LX/8yU;->l(I)LX/8yU;

    move-result-object v1

    const v2, 0x7f0c0057

    invoke-virtual {v1, v2}, LX/8yU;->n(I)LX/8yU;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b1616

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p6

    move-object/from16 v5, p7

    invoke-static/range {v1 .. v6}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a(LX/1De;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;LX/1Ad;)LX/1Dh;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
