.class public final Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5Z;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:LX/1U8;

.field public c:Z

.field public d:LX/2km;

.field public e:LX/1Pn;

.field public f:[J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078584
    const-string v0, "ReactionPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078585
    if-ne p0, p1, :cond_1

    .line 2078586
    :cond_0
    :goto_0
    return v0

    .line 2078587
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078588
    goto :goto_0

    .line 2078589
    :cond_3
    check-cast p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;

    .line 2078590
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078591
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078592
    if-eq v2, v3, :cond_0

    .line 2078593
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078594
    goto :goto_0

    .line 2078595
    :cond_5
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 2078596
    :cond_6
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2078597
    goto :goto_0

    .line 2078598
    :cond_8
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    if-nez v2, :cond_7

    .line 2078599
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->c:Z

    iget-boolean v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2078600
    goto :goto_0

    .line 2078601
    :cond_a
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->d:LX/2km;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->d:LX/2km;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->d:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2078602
    goto :goto_0

    .line 2078603
    :cond_c
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->d:LX/2km;

    if-nez v2, :cond_b

    .line 2078604
    :cond_d
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->e:LX/1Pn;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->e:LX/1Pn;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->e:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2078605
    goto :goto_0

    .line 2078606
    :cond_f
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->e:LX/1Pn;

    if-nez v2, :cond_e

    .line 2078607
    :cond_10
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->f:[J

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->f:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 2078608
    goto :goto_0

    .line 2078609
    :cond_11
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->g:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 2078610
    goto/16 :goto_0

    .line 2078611
    :cond_13
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->g:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 2078612
    :cond_14
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->h:Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078613
    goto/16 :goto_0

    .line 2078614
    :cond_15
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
