.class public Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;


# instance fields
.field private final b:LX/E1f;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2077721
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/E1f;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1f;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2077718
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->b:LX/E1f;

    .line 2077719
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->c:LX/0Or;

    .line 2077720
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I
    .locals 2

    .prologue
    .line 2077692
    sget-object v0, LX/E4z;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2077693
    const v0, 0x7f0b1636

    :goto_0
    return v0

    .line 2077694
    :pswitch_0
    const v0, 0x7f0b1637

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;
    .locals 5

    .prologue
    .line 2077704
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    if-nez v0, :cond_1

    .line 2077705
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    monitor-enter v1

    .line 2077706
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077707
    if-eqz v2, :cond_0

    .line 2077708
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077709
    new-instance v4, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;-><init>(LX/E1f;LX/0Or;)V

    .line 2077710
    move-object v0, v4

    .line 2077711
    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077714
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->d:Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    return-object v0

    .line 2077715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/2km;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Ljava/lang/String;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2077695
    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p6, :cond_1

    .line 2077696
    :cond_0
    :goto_0
    return-void

    .line 2077697
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->b:LX/E1f;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v3, 0x0

    :goto_1
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2077698
    iget-object v4, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2077699
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2077700
    iget-object v5, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2077701
    move-object v1, p4

    check-cast v1, LX/2kn;

    invoke-interface {v1}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    move-object v1, p2

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    .line 2077702
    invoke-interface {p4, p5, p6, p3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0

    .line 2077703
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
