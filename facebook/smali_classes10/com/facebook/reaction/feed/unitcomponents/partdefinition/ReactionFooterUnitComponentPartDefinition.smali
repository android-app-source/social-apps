.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E4b;

.field private final e:LX/E5d;

.field private final f:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E4b;LX/2d1;LX/E5d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074708
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2074709
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->d:LX/E4b;

    .line 2074710
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->e:LX/E5d;

    .line 2074711
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->f:LX/2d1;

    .line 2074712
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/E4b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2074678
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074679
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->d:LX/E4b;

    invoke-virtual {v1, p1}, LX/E4b;->c(LX/1De;)LX/E4Z;

    move-result-object v1

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/E4Z;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4Z;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->e:LX/E5d;

    const/4 v3, 0x0

    .line 2074680
    new-instance v4, LX/E5c;

    invoke-direct {v4, v2}, LX/E5c;-><init>(LX/E5d;)V

    .line 2074681
    sget-object p0, LX/E5d;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E5b;

    .line 2074682
    if-nez p0, :cond_0

    .line 2074683
    new-instance p0, LX/E5b;

    invoke-direct {p0}, LX/E5b;-><init>()V

    .line 2074684
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/E5b;->a$redex0(LX/E5b;LX/1De;IILX/E5c;)V

    .line 2074685
    move-object v4, p0

    .line 2074686
    move-object v3, v4

    .line 2074687
    move-object v2, v3

    .line 2074688
    const/4 v3, 0x1

    .line 2074689
    iget-object v4, v2, LX/E5b;->a:LX/E5c;

    iput-boolean v3, v4, LX/E5c;->b:Z

    .line 2074690
    iget-object v4, v2, LX/E5b;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2074691
    move-object v2, v2

    .line 2074692
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2074693
    iget-object v3, v2, LX/E5b;->a:LX/E5c;

    iput-object v0, v3, LX/E5c;->a:Ljava/lang/String;

    .line 2074694
    iget-object v3, v2, LX/E5b;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2074695
    move-object v0, v2

    .line 2074696
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/E4Z;->a(LX/1X1;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/E4Z;->a(LX/2km;)LX/E4Z;

    move-result-object v0

    .line 2074697
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2074698
    invoke-virtual {v0, v1}, LX/E4Z;->c(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    .line 2074699
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2074700
    invoke-virtual {v0, v1}, LX/E4Z;->d(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 2074714
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074715
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074716
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074719
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E4b;->a(LX/0QB;)LX/E4b;

    move-result-object v4

    check-cast v4, LX/E4b;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-static {v0}, LX/E5d;->a(LX/0QB;)LX/E5d;

    move-result-object v6

    check-cast v6, LX/E5d;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/E4b;LX/2d1;LX/E5d;)V

    .line 2074720
    move-object v0, p0

    .line 2074721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2074713
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->f:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2074706
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2074707
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2074705
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2074704
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFooterUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2074701
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074702
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074703
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_0

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2074677
    const/4 v0, 0x0

    return-object v0
.end method
