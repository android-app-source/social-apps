.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/E6I;

.field private final b:LX/1vo;

.field private final c:LX/Cfw;


# direct methods
.method public constructor <init>(LX/E6I;LX/1vo;LX/Cfw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076278
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2076279
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->a:LX/E6I;

    .line 2076280
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->b:LX/1vo;

    .line 2076281
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->c:LX/Cfw;

    .line 2076282
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;
    .locals 6

    .prologue
    .line 2076283
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;

    monitor-enter v1

    .line 2076284
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076285
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076286
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076287
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076288
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;

    invoke-static {v0}, LX/E6I;->b(LX/0QB;)LX/E6I;

    move-result-object v3

    check-cast v3, LX/E6I;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v4

    check-cast v4, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v5

    check-cast v5, LX/Cfw;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;-><init>(LX/E6I;LX/1vo;LX/Cfw;)V

    .line 2076289
    move-object v0, p0

    .line 2076290
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076291
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076292
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2076294
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076295
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2076296
    check-cast v0, LX/9ud;

    .line 2076297
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v4

    .line 2076298
    invoke-interface {v0}, LX/9ud;->aJ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2076299
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->a:LX/E6I;

    .line 2076300
    iget-object v2, v1, LX/E6I;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    move-object v1, v2

    .line 2076301
    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076302
    :cond_0
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 2076303
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9uc;

    .line 2076304
    invoke-interface {v0}, LX/9ud;->aI()Z

    move-result v2

    if-eqz v2, :cond_1

    if-lez v3, :cond_1

    .line 2076305
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->a:LX/E6I;

    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9uc;

    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, LX/E6I;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v2

    .line 2076306
    if-eqz v2, :cond_1

    .line 2076307
    invoke-virtual {p1, v2, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076308
    :cond_1
    new-instance v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076309
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v5

    .line 2076310
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2076311
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2076312
    invoke-direct {v2, v5, v1, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2076313
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2076314
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->b:LX/1vo;

    invoke-virtual {v5, v1}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2076315
    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076316
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2076317
    :cond_2
    invoke-interface {v0}, LX/9ud;->aG()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2076318
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->a:LX/E6I;

    .line 2076319
    iget-object v1, v0, LX/E6I;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    move-object v0, v1

    .line 2076320
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076321
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2076322
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076323
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;->c:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
