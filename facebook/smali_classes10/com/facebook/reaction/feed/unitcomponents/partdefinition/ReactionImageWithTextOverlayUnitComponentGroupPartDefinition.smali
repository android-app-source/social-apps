.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075377
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2075378
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;

    .line 2075379
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;

    .line 2075380
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;
    .locals 5

    .prologue
    .line 2075387
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;

    monitor-enter v1

    .line 2075388
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075389
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075390
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075391
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075392
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;)V

    .line 2075393
    move-object v0, p0

    .line 2075394
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075395
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075396
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075397
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2075383
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075384
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2075385
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithTextOverlayUnitComponentGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2075386
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2075381
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075382
    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionLargeImageUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
