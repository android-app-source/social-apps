.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074052
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2074053
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    .line 2074054
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    .line 2074055
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;
    .locals 5

    .prologue
    .line 2074028
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    monitor-enter v1

    .line 2074029
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074030
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074031
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074032
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074033
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;)V

    .line 2074034
    move-object v0, p0

    .line 2074035
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074036
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074037
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 2074056
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074057
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2074040
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    .line 2074041
    check-cast p3, LX/1Pr;

    .line 2074042
    new-instance v0, LX/E2U;

    .line 2074043
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2074044
    invoke-direct {v0, v1}, LX/E2U;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2V;

    .line 2074045
    iget-boolean v1, v0, LX/E2V;->b:Z

    move v1, v1

    .line 2074046
    if-eqz v1, :cond_1

    .line 2074047
    iget-boolean v1, v0, LX/E2V;->a:Z

    move v0, v1

    .line 2074048
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2074049
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    :goto_1
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074050
    const/4 v0, 0x0

    return-object v0

    .line 2074051
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2074039
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
