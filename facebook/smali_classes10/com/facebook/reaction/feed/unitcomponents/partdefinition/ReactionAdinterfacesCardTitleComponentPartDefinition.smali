.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/E4f;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E4f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073906
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2073907
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->d:LX/E4f;

    .line 2073908
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/1X1",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2073880
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2073881
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->d:LX/E4f;

    const/4 v2, 0x0

    .line 2073882
    new-instance p0, LX/E4e;

    invoke-direct {p0, v1}, LX/E4e;-><init>(LX/E4f;)V

    .line 2073883
    iget-object p2, v1, LX/E4f;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/E4d;

    .line 2073884
    if-nez p2, :cond_0

    .line 2073885
    new-instance p2, LX/E4d;

    invoke-direct {p2, v1}, LX/E4d;-><init>(LX/E4f;)V

    .line 2073886
    :cond_0
    invoke-static {p2, p1, v2, v2, p0}, LX/E4d;->a$redex0(LX/E4d;LX/1De;IILX/E4e;)V

    .line 2073887
    move-object p0, p2

    .line 2073888
    move-object v2, p0

    .line 2073889
    move-object v1, v2

    .line 2073890
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    .line 2073891
    iget-object v2, v1, LX/E4d;->a:LX/E4e;

    iput-object v0, v2, LX/E4e;->a:Ljava/lang/String;

    .line 2073892
    iget-object v2, v1, LX/E4d;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2073893
    move-object v0, v1

    .line 2073894
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;
    .locals 5

    .prologue
    .line 2073895
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;

    monitor-enter v1

    .line 2073896
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073897
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073900
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E4f;->a(LX/0QB;)LX/E4f;

    move-result-object v4

    check-cast v4, LX/E4f;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;-><init>(Landroid/content/Context;LX/E4f;)V

    .line 2073901
    move-object v0, p0

    .line 2073902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2073909
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2073876
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesCardTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2073877
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2073878
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2073879
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
