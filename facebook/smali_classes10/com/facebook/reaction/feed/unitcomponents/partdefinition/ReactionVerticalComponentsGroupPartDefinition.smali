.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

.field private final c:LX/Cfw;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;LX/Cfw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076132
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2076133
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    .line 2076134
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    .line 2076135
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->c:LX/Cfw;

    .line 2076136
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;
    .locals 6

    .prologue
    .line 2076137
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;

    monitor-enter v1

    .line 2076138
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076139
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076140
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076141
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076142
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v5

    check-cast v5, LX/Cfw;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;LX/Cfw;)V

    .line 2076143
    move-object v0, p0

    .line 2076144
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076145
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076146
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Cfo;)Z
    .locals 2

    .prologue
    .line 2076148
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/Cfo;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2076149
    invoke-interface {v0}, LX/9qX;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-eq v1, p0, :cond_0

    invoke-interface {v0}, LX/9qX;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-eq v1, p0, :cond_0

    invoke-interface {v0}, LX/9qX;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-eq v1, p0, :cond_0

    invoke-interface {v0}, LX/9qX;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-ne v1, p0, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2076150
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2076151
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076152
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076153
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076154
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2076155
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076156
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->c:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
