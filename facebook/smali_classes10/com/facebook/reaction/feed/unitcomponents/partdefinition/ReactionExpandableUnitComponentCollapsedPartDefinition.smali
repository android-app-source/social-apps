.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

.field private final b:LX/1vo;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;LX/1vo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074511
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2074512
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    .line 2074513
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->b:LX/1vo;

    .line 2074514
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;
    .locals 5

    .prologue
    .line 2074500
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;

    monitor-enter v1

    .line 2074501
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074502
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074505
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v4

    check-cast v4, LX/1vo;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;LX/1vo;)V

    .line 2074506
    move-object v0, p0

    .line 2074507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 1

    .prologue
    .line 2074499
    invoke-static {p0}, LX/Cfu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2074489
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074490
    invoke-static {p2}, LX/Cfu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v0

    .line 2074491
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2074492
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2074493
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->b:LX/1vo;

    invoke-virtual {v2, v1}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2074494
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    invoke-static {p1, v2, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    .line 2074495
    iget-boolean p0, v2, LX/1RG;->b:Z

    if-nez p0, :cond_0

    .line 2074496
    iget-object p0, v2, LX/1RG;->a:LX/1RF;

    invoke-virtual {p0, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result p0

    iput-boolean p0, v2, LX/1RG;->b:Z

    .line 2074497
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2074498
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentCollapsedPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
