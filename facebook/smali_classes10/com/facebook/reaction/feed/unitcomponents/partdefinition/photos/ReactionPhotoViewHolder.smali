.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2076332
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V
    .locals 3

    .prologue
    .line 2076333
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2076334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->m:Ljava/lang/String;

    .line 2076335
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2076336
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundColor(I)V

    .line 2076337
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2076338
    return-void
.end method
