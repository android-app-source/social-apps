.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E46;",
        "LX/8pS;",
        "TE;",
        "Lcom/facebook/widget/ratingbar/BetterRatingBar;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/E1i;


# direct methods
.method public constructor <init>(LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075978
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2075979
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;->a:LX/E1i;

    .line 2075980
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;
    .locals 4

    .prologue
    .line 2075967
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    monitor-enter v1

    .line 2075968
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075969
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075970
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075971
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075972
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;-><init>(LX/E1i;)V

    .line 2075973
    move-object v0, p0

    .line 2075974
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075975
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075976
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2075959
    check-cast p2, LX/E46;

    check-cast p3, LX/2km;

    .line 2075960
    new-instance v0, LX/E45;

    invoke-direct {v0, p0, p2, p3}, LX/E45;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;LX/E46;LX/2km;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x317ce358    # 3.6800056E-9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075964
    check-cast p2, LX/8pS;

    check-cast p4, Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2075965
    invoke-virtual {p4, p2}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(LX/8pS;)V

    .line 2075966
    const/16 v1, 0x1f

    const v2, 0x6a2faef1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2075961
    check-cast p4, Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 2075962
    invoke-virtual {p4}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a()V

    .line 2075963
    return-void
.end method
