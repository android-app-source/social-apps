.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1vo;

.field private final b:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(LX/1vo;Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075570
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2075571
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->a:LX/1vo;

    .line 2075572
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->b:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    .line 2075573
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;
    .locals 5

    .prologue
    .line 2075574
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;

    monitor-enter v1

    .line 2075575
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075576
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075577
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075578
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075579
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v3

    check-cast v3, LX/1vo;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;-><init>(LX/1vo;Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;)V

    .line 2075580
    move-object v0, p0

    .line 2075581
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075582
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075583
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2075585
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 2075586
    invoke-static {p2}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 2075587
    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2075588
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2075589
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9uc;

    .line 2075590
    if-eq v1, v0, :cond_0

    .line 2075591
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->b:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-virtual {p1, v5, v8}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2075592
    :cond_0
    new-instance v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075593
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2075594
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2075595
    invoke-direct {v5, v1, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2075596
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedVScrollGenericComponentsListPartDefinition;->a:LX/1vo;

    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2075597
    invoke-virtual {p1, v1, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2075598
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2075599
    :cond_1
    return-object v8
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2075600
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075601
    invoke-static {p1}, LX/Cfu;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
