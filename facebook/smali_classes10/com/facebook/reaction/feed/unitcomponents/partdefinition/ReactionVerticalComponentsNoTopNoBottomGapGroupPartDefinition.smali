.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

.field private final b:LX/1vo;

.field private final c:LX/Cfw;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;LX/1vo;LX/Cfw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076193
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2076194
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    .line 2076195
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->b:LX/1vo;

    .line 2076196
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->c:LX/Cfw;

    .line 2076197
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;
    .locals 6

    .prologue
    .line 2076198
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    monitor-enter v1

    .line 2076199
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076200
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076203
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v4

    check-cast v4, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v5

    check-cast v5, LX/Cfw;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;LX/1vo;LX/Cfw;)V

    .line 2076204
    move-object v0, p0

    .line 2076205
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076206
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076207
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2076209
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076210
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2076211
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2076212
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2076213
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    .line 2076214
    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->b:LX/1vo;

    invoke-virtual {v4, v3}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v3

    .line 2076215
    if-eqz v3, :cond_0

    .line 2076216
    new-instance v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2076217
    invoke-virtual {p1, v3, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076218
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2076220
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076221
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->c:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
