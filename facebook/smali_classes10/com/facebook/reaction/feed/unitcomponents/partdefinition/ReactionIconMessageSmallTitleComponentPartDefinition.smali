.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E4b;

.field private final e:LX/E4y;

.field private final f:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E4b;LX/2d1;LX/E4y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075147
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2075148
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->d:LX/E4b;

    .line 2075149
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->e:LX/E4y;

    .line 2075150
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->f:LX/2d1;

    .line 2075151
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/E4b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2075152
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 2075153
    invoke-interface {v2}, LX/9uc;->aj()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, LX/9uc;->aj()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2075154
    invoke-interface {v2}, LX/9uc;->aj()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2075155
    :goto_0
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2075156
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2075157
    :cond_0
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->e:LX/E4y;

    invoke-virtual {v3, p1}, LX/E4y;->c(LX/1De;)LX/E4w;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/E4w;->b(Landroid/net/Uri;)LX/E4w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->aS()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/E4w;->a(Landroid/net/Uri;)LX/E4w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/E4w;->b(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/E4w;->c(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E4w;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4w;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2075158
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->d:LX/E4b;

    invoke-virtual {v1, p1}, LX/E4b;->c(LX/1De;)LX/E4Z;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/E4Z;->a(LX/1X1;)LX/E4Z;

    move-result-object v0

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E4Z;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/E4Z;->a(LX/2km;)LX/E4Z;

    move-result-object v0

    .line 2075159
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2075160
    invoke-virtual {v0, v1}, LX/E4Z;->c(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    .line 2075161
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2075162
    invoke-virtual {v0, v1}, LX/E4Z;->d(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;
    .locals 7

    .prologue
    .line 2075163
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;

    monitor-enter v1

    .line 2075164
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075165
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075166
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075167
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075168
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E4b;->a(LX/0QB;)LX/E4b;

    move-result-object v4

    check-cast v4, LX/E4b;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-static {v0}, LX/E4y;->a(LX/0QB;)LX/E4y;

    move-result-object v6

    check-cast v6, LX/E4y;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;-><init>(Landroid/content/Context;LX/E4b;LX/2d1;LX/E4y;)V

    .line 2075169
    move-object v0, p0

    .line 2075170
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075171
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075172
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2075174
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->f:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2075175
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2075176
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2075177
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2075178
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageSmallTitleComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2075179
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075180
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075181
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2075182
    const/4 v0, 0x0

    return-object v0
.end method
