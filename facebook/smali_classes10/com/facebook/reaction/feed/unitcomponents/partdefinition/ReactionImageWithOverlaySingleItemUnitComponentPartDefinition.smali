.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/E3n;",
        "LX/E3o;",
        "TE;",
        "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/1Ad;

.field private final d:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2075344
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2075345
    new-instance v0, LX/E3m;

    invoke-direct {v0}, LX/E3m;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075346
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2075347
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->c:LX/1Ad;

    .line 2075348
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2075349
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2075350
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075351
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075352
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075353
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075354
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075355
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;-><init>(LX/1Ad;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2075356
    move-object v0, p0

    .line 2075357
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075358
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075359
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2075361
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2075362
    check-cast p2, LX/E3n;

    const/4 v1, 0x0

    .line 2075363
    iget-object v2, p2, LX/E3n;->a:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;

    .line 2075364
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v3, LX/E1o;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    iget-object v5, p2, LX/E3n;->b:Ljava/lang/String;

    iget-object v6, p2, LX/E3n;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2075365
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->e()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->e()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2075366
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->c:LX/1Ad;

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->e()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2075367
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->c()LX/5sY;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->c()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2075368
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->c:LX/1Ad;

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->c()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2075369
    :cond_0
    new-instance v3, LX/E3o;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v1, v0, v2}, LX/E3o;-><init>(Ljava/lang/String;LX/1aZ;LX/1aZ;Ljava/lang/String;)V

    return-object v3

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3ba4f9ce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075370
    check-cast p2, LX/E3o;

    check-cast p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;

    .line 2075371
    iget-object v1, p2, LX/E3o;->a:Ljava/lang/String;

    iget-object v2, p2, LX/E3o;->c:LX/1aZ;

    iget-object p0, p2, LX/E3o;->b:LX/1aZ;

    iget-object p1, p2, LX/E3o;->d:Ljava/lang/String;

    invoke-virtual {p4, v1, v2, p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a(Ljava/lang/String;LX/1aZ;LX/1aZ;Ljava/lang/String;)V

    .line 2075372
    const/16 v1, 0x1f

    const v2, 0x2bbaa266

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2075373
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2075374
    check-cast p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;

    const/4 v0, 0x0

    .line 2075375
    invoke-virtual {p4, v0, v0, v0, v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a(Ljava/lang/String;LX/1aZ;LX/1aZ;Ljava/lang/String;)V

    .line 2075376
    return-void
.end method
