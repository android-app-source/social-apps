.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/widget/CustomFrameLayout;"
    }
.end annotation


# instance fields
.field private a:LX/1aX;

.field private b:LX/1aX;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2076796
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2076797
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a()V

    .line 2076798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2076793
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2076794
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a()V

    .line 2076795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2076790
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2076791
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a()V

    .line 2076792
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2076772
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2076773
    const v0, 0x7f03115b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2076774
    const v0, 0x7f0d28f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2076775
    const v0, 0x7f0d28f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2076776
    const v0, 0x7f0d28f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->e:Landroid/widget/ImageView;

    .line 2076777
    const v0, 0x7f0a00ff

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2076778
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2076779
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2076780
    move-object v0, v2

    .line 2076781
    const v2, 0x7f0b167b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v2}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v2

    .line 2076782
    iput-object v2, v0, LX/1Uo;->u:LX/4Ab;

    .line 2076783
    move-object v0, v0

    .line 2076784
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2076785
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    .line 2076786
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V

    .line 2076787
    new-instance v0, LX/1Uo;

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    .line 2076788
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2076789
    return-void
.end method

.method private a(LX/1aZ;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2076765
    if-nez p1, :cond_0

    .line 2076766
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 2076767
    :goto_0
    return-void

    .line 2076768
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2076769
    const/16 v1, 0x33

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 2076770
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2076771
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method private setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 2076762
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2076763
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->setPadding(IIII)V

    .line 2076764
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1aZ;LX/1aZ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2076758
    invoke-virtual {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->setLabel(Ljava/lang/String;)V

    .line 2076759
    invoke-virtual {p0, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->setIcon(LX/1aZ;)V

    .line 2076760
    invoke-direct {p0, p3, p4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a(LX/1aZ;Ljava/lang/String;)V

    .line 2076761
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7cba5869

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076754
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2076755
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2076756
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2076757
    const/16 v1, 0x2d

    const v2, 0x5dc333c8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x69217b47

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076732
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2076733
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2076734
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2076735
    const/16 v1, 0x2d

    const v2, 0x486224be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2076750
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 2076751
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2076752
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2076753
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2076746
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 2076747
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->b:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2076748
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2076749
    return-void
.end method

.method public setIcon(LX/1aZ;)V
    .locals 2

    .prologue
    .line 2076741
    if-nez p1, :cond_0

    .line 2076742
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2076743
    :goto_0
    return-void

    .line 2076744
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2076745
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->a:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076736
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076737
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2076738
    :goto_0
    return-void

    .line 2076739
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2076740
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionImageWithOverlayGridItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
