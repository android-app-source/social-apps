.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/E3x;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2075675
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2075676
    const v0, 0x7f031172

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075670
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2075671
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2075672
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 2075673
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->e:LX/E1i;

    .line 2075674
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;
    .locals 6

    .prologue
    .line 2075659
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    monitor-enter v1

    .line 2075660
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075661
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075662
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075663
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075664
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v5

    check-cast v5, LX/E1i;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E1i;)V

    .line 2075665
    move-object v0, p0

    .line 2075666
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075667
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075668
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2075658
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2075647
    check-cast p2, LX/E3x;

    check-cast p3, LX/2km;

    .line 2075648
    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p2, LX/E3x;->a:LX/1U8;

    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p2, LX/E3x;->b:[J

    iget-object v5, p2, LX/E3x;->a:LX/1U8;

    invoke-interface {v5}, LX/1U8;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v1 .. v5}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2075649
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/E3w;

    invoke-direct {v2, p0, p3, p2, v0}, LX/E3w;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;LX/2km;LX/E3x;LX/Cfl;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2075650
    const v0, 0x7f0d291b

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v2

    iget-object v3, p2, LX/E3x;->a:LX/1U8;

    invoke-interface {v3}, LX/1U8;->e()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2075651
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2075652
    move-object v2, v2

    .line 2075653
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 2075654
    iput v3, v2, LX/2f8;->b:F

    .line 2075655
    move-object v2, v2

    .line 2075656
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2075657
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2075645
    check-cast p1, LX/E3x;

    .line 2075646
    iget-object v0, p1, LX/E3x;->a:LX/1U8;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/E3x;->a:LX/1U8;

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/E3x;->a:LX/1U8;

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/E3x;->a:LX/1U8;

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
