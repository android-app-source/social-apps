.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E5n;

.field private final e:LX/2d1;

.field private f:LX/E2f;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E5n;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074930
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2074931
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->d:LX/E5n;

    .line 2074932
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->e:LX/2d1;

    .line 2074933
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/E5n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2074934
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v0

    .line 2074935
    new-instance v0, LX/E2e;

    .line 2074936
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2074937
    invoke-direct {v0, v2}, LX/E2e;-><init>(Ljava/lang/String;)V

    .line 2074938
    check-cast p3, LX/1Pr;

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2f;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->f:LX/E2f;

    .line 2074939
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->f:LX/E2f;

    .line 2074940
    iget-boolean v2, v0, LX/E2f;->g:Z

    move v0, v2

    .line 2074941
    if-eqz v0, :cond_0

    .line 2074942
    const/4 v0, 0x0

    .line 2074943
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->d:LX/E5n;

    invoke-virtual {v0, p1}, LX/E5n;->c(LX/1De;)LX/E5l;

    move-result-object v0

    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E5l;->b(Ljava/lang/String;)LX/E5l;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2074918
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074919
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074920
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074923
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E5n;->a(LX/0QB;)LX/E5n;

    move-result-object v5

    check-cast v5, LX/E5n;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E5n;)V

    .line 2074924
    move-object v0, p0

    .line 2074925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2074929
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2074917
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2074916
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2074915
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2074909
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x0

    .line 2074910
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2074911
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->f:LX/E2f;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutHeaderUnitComponentPartDefinition;->f:LX/E2f;

    .line 2074912
    iget-boolean v3, v2, LX/E2f;->g:Z

    move v2, v3

    .line 2074913
    if-eqz v2, :cond_1

    .line 2074914
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v2, v3, :cond_0

    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2074908
    const/4 v0, 0x0

    return-object v0
.end method
