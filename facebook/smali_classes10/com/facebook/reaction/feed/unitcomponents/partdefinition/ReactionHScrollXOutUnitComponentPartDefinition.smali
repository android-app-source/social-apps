.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2ko;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3h;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/2dq;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1vo;

.field private final d:LX/Cfw;

.field public final e:LX/1Qx;

.field public f:LX/E2f;

.field public g:I


# direct methods
.method public constructor <init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;LX/1Qx;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075084
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2075085
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->a:LX/2dq;

    .line 2075086
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2075087
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->c:LX/1vo;

    .line 2075088
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->d:LX/Cfw;

    .line 2075089
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->e:LX/1Qx;

    .line 2075090
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->g:I

    .line 2075091
    return-void
.end method

.method private a(LX/0Px;LX/E2e;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;",
            "LX/E2e;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2075083
    new-instance v0, LX/E3g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/E3g;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2e;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;
    .locals 9

    .prologue
    .line 2075026
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075027
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075028
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075031
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v4

    check-cast v4, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v6

    check-cast v6, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v7

    check-cast v7, LX/Cfw;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v8

    check-cast v8, LX/1Qx;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;-><init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;LX/1Qx;)V

    .line 2075032
    move-object v0, v3

    .line 2075033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2075069
    invoke-static {p1}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v2

    .line 2075070
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->g:I

    .line 2075071
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2075072
    iget-object p1, v3, LX/E2f;->f:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    move v3, p1

    .line 2075073
    if-ne v0, v3, :cond_2

    .line 2075074
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2075075
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 2075076
    goto :goto_0

    .line 2075077
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2075078
    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 2075079
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    invoke-virtual {v3, v1}, LX/E2f;->d(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2075080
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2075081
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2075082
    :cond_4
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2075068
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2075053
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x1

    .line 2075054
    new-instance v3, LX/E2e;

    .line 2075055
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2075056
    invoke-direct {v3, v0}, LX/E2e;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2075057
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2f;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2075058
    invoke-direct {p0, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v4

    .line 2075059
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1625

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 2075060
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-le v1, v5, :cond_0

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->a:LX/2dq;

    int-to-float v0, v0

    const/high16 v2, 0x41000000    # 8.0f

    add-float/2addr v0, v2

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v0, v2, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    .line 2075061
    :goto_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    .line 2075062
    iput v2, v0, LX/E2f;->c:I

    .line 2075063
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    move-object v2, p3

    check-cast v2, LX/1Pr;

    invoke-interface {v2, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2f;

    .line 2075064
    iget v5, v2, LX/E2f;->d:I

    move v2, v5

    .line 2075065
    invoke-direct {p0, v4, v3, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->a(LX/0Px;LX/E2e;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2075066
    new-instance v0, LX/E3h;

    check-cast p3, LX/2ko;

    invoke-interface {p3, p2}, LX/2ko;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E3h;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-object v0

    .line 2075067
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->a:LX/2dq;

    sget-object v1, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v0, v1}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xa03a860

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075044
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/E3h;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2075045
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2075046
    iget-boolean v2, v1, LX/E2f;->g:Z

    move v1, v2

    .line 2075047
    if-eqz v1, :cond_0

    .line 2075048
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 2075049
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 2075050
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V

    .line 2075051
    :cond_0
    iget-object v1, p2, LX/E3h;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2075052
    const/16 v1, 0x1f

    const v2, 0x4a7b8e2f    # 4121483.8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2075037
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x0

    .line 2075038
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2075039
    iget-boolean v2, v1, LX/E2f;->g:Z

    move v1, v2

    .line 2075040
    if-eqz v1, :cond_1

    .line 2075041
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "SUCCESS"

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->d:LX/Cfw;

    invoke-virtual {v2, p1}, LX/Cfw;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2075042
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2075043
    instance-of v1, v1, LX/9ud;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
