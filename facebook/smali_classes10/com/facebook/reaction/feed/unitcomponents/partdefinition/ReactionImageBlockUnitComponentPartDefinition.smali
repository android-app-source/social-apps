.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/E4y;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E4y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075246
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2075247
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->d:LX/E4y;

    .line 2075248
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->e:LX/2d1;

    .line 2075249
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/E4y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2075202
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v0

    .line 2075203
    const/4 v0, 0x0

    .line 2075204
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2075205
    invoke-interface {v1}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2075206
    :cond_0
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->d:LX/E4y;

    invoke-virtual {v2, p1}, LX/E4y;->c(LX/1De;)LX/E4w;

    move-result-object v2

    invoke-interface {v1}, LX/9uc;->aV()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E4w;->a(Landroid/net/Uri;)LX/E4w;

    move-result-object v2

    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E4w;->b(Ljava/lang/String;)LX/E4w;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/E4w;->c(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/E4w;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4w;

    move-result-object v0

    .line 2075207
    invoke-interface {v1}, LX/9uc;->cG()LX/0Px;

    move-result-object v3

    .line 2075208
    invoke-interface {v1}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Landroid/net/Uri;

    move-result-object v2

    .line 2075209
    if-nez v2, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 2075210
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Landroid/net/Uri;

    move-result-object v2

    .line 2075211
    :cond_1
    if-nez v2, :cond_2

    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2075212
    invoke-interface {v1}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2075213
    :cond_2
    move-object v2, v2

    .line 2075214
    invoke-virtual {v0, v2}, LX/E4w;->b(Landroid/net/Uri;)LX/E4w;

    move-result-object v0

    invoke-interface {v1}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 2075215
    iget-object v3, v0, LX/E4w;->a:LX/E4x;

    iput-object v2, v3, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2075216
    move-object v0, v0

    .line 2075217
    invoke-interface {v1}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/E4w;->d(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/E4w;->a(LX/2km;)LX/E4w;

    move-result-object v0

    .line 2075218
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2075219
    invoke-virtual {v0, v2}, LX/E4w;->e(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    .line 2075220
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2075221
    invoke-virtual {v0, v2}, LX/E4w;->f(Ljava/lang/String;)LX/E4w;

    move-result-object v0

    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E4w;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4w;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2075222
    return-object v0
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2075223
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2075224
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2075225
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2075226
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075227
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075228
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075229
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075230
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075231
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E4y;->a(LX/0QB;)LX/E4y;

    move-result-object v5

    check-cast v5, LX/E4y;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E4y;)V

    .line 2075232
    move-object v0, p0

    .line 2075233
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075234
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075235
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2075237
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2075238
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2075239
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2075240
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2075241
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2075242
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075243
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075244
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2075245
    const/4 v0, 0x0

    return-object v0
.end method
