.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

.field public final c:LX/3U3;

.field private final d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;LX/3U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074162
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2074163
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;

    .line 2074164
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    .line 2074165
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->c:LX/3U3;

    .line 2074166
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    .line 2074167
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 2074441
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074442
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074443
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074444
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074445
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074446
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    invoke-static {v0}, LX/3U3;->a(LX/0QB;)LX/3U3;

    move-result-object v6

    check-cast v6, LX/3U3;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;LX/3U3;)V

    .line 2074447
    move-object v0, p0

    .line 2074448
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074449
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074450
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 2074172
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 2074173
    move-object v0, p3

    .line 2074174
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2T;

    .line 2074175
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2074176
    invoke-interface {v2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/E2T;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2S;

    .line 2074177
    iget-boolean v0, v2, LX/E2S;->b:Z

    move v0, v0

    .line 2074178
    if-nez v0, :cond_1

    .line 2074179
    invoke-interface {v3}, LX/9uc;->cI()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    move-result-object v0

    .line 2074180
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    :goto_0
    invoke-virtual {v2, v0}, LX/E2S;->a(LX/03R;)V

    .line 2074181
    :cond_1
    iget-object v0, v2, LX/E2S;->a:LX/03R;

    move-object v0, v0

    .line 2074182
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2074183
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074184
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2074185
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    :cond_3
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 2074186
    :cond_4
    iget-object v0, v2, LX/E2S;->a:LX/03R;

    move-object v0, v0

    .line 2074187
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2074188
    iget-object v10, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    new-instance v11, LX/E4B;

    const v9, 0x7f0207da

    invoke-interface {v3}, LX/9uc;->dj()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;->a()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0a00d4

    invoke-interface {v3}, LX/9uc;->dh()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;->a()Ljava/lang/String;

    move-result-object v8

    new-instance v0, LX/E3O;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/E3O;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v4, v11

    move v5, v9

    move-object v9, v0

    invoke-direct/range {v4 .. v9}, LX/E4B;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v10, v11}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074189
    :goto_2
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListGroupPartDefinition;

    new-instance v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074190
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2074191
    invoke-interface {v3}, LX/9uc;->aB()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v7, :cond_5

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074192
    invoke-static {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2074193
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 2074194
    :cond_5
    new-instance v2, LX/9vT;

    invoke-direct {v2}, LX/9vT;-><init>()V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2074195
    iput-object v4, v2, LX/9vT;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2074196
    move-object v2, v2

    .line 2074197
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2074198
    iput-object v4, v2, LX/9vT;->c:LX/0Px;

    .line 2074199
    move-object v2, v2

    .line 2074200
    invoke-virtual {v2}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v2

    move-object v2, v2

    .line 2074201
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2074202
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2074203
    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2074204
    :cond_6
    iget-object v10, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    new-instance v11, LX/E4B;

    const v9, 0x7f02081c

    invoke-interface {v3}, LX/9uc;->di()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;->a()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0a010e

    invoke-interface {v3}, LX/9uc;->dh()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;->a()Ljava/lang/String;

    move-result-object v8

    new-instance v0, LX/E3P;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/E3P;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v4, v11

    move v5, v9

    move-object v9, v0

    invoke-direct/range {v4 .. v9}, LX/E4B;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v10, v11}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static a(LX/2kk;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/enums/GraphQLSelectedActionState;)V
    .locals 5

    .prologue
    .line 2074205
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074206
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    .line 2074207
    new-instance v1, LX/9wT;

    invoke-direct {v1}, LX/9wT;-><init>()V

    .line 2074208
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2074209
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dy()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074210
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->l()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->c:LX/0Px;

    .line 2074211
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->m()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->d:LX/0Px;

    .line 2074212
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dz()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074213
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dA()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074214
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dB()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074215
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dC()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074216
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dD()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074217
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dE()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    .line 2074218
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    .line 2074219
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->u()I

    move-result v2

    iput v2, v1, LX/9wT;->l:I

    .line 2074220
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->v()I

    move-result v2

    iput v2, v1, LX/9wT;->m:I

    .line 2074221
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074222
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dH()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074223
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->p:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074224
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->z()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->q:Ljava/lang/String;

    .line 2074225
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->A()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->r:LX/0Px;

    .line 2074226
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->s:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    .line 2074227
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dK()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074228
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 2074229
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->E()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->v:Ljava/lang/String;

    .line 2074230
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->F()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->w:LX/0Px;

    .line 2074231
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->G()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->x:Ljava/lang/String;

    .line 2074232
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->H()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->y:Ljava/lang/String;

    .line 2074233
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074234
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dM()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->A:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    .line 2074235
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dN()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074236
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dO()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->C:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2074237
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dP()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->D:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    .line 2074238
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->N()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->E:Ljava/lang/String;

    .line 2074239
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dQ()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->F:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074240
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dR()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->G:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074241
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->H:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 2074242
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->R()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->I:Ljava/lang/String;

    .line 2074243
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2074244
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->S()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->K:Ljava/lang/String;

    .line 2074245
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->T()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->L:D

    .line 2074246
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->U()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->M:Ljava/lang/String;

    .line 2074247
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dT()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->N:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 2074248
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->O:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    .line 2074249
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->X()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->P:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    .line 2074250
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dU()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->Q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 2074251
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dV()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->R:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 2074252
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dW()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->S:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074253
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dX()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->T:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074254
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ac()I

    move-result v2

    iput v2, v1, LX/9wT;->U:I

    .line 2074255
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ad()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->V:LX/0Px;

    .line 2074256
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dY()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->W:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074257
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dZ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->X:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    .line 2074258
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ea()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    .line 2074259
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ah()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->Z:Ljava/lang/String;

    .line 2074260
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eb()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aa:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074261
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ec()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ab:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074262
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ak()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ac:Ljava/lang/String;

    .line 2074263
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->al()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ad:Ljava/lang/String;

    .line 2074264
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ed()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ae:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074265
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->an()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->af:LX/0Px;

    .line 2074266
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ao()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ag:LX/0Px;

    .line 2074267
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ap()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ah:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2074268
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aq()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ai:Ljava/lang/String;

    .line 2074269
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ee()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aj:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074270
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ef()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074271
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eg()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ExampleFrameImageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->al:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ExampleFrameImageModel;

    .line 2074272
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eh()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->am:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074273
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->av()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->an:LX/0Px;

    .line 2074274
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2074275
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ei()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ap:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074276
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ej()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aq:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074277
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ek()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ar:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    .line 2074278
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->el()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->as:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074279
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aB()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->at:LX/0Px;

    .line 2074280
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aC()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->au:LX/0Px;

    .line 2074281
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->em()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->av:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    .line 2074282
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aE()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aw:LX/0Px;

    .line 2074283
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ax:Ljava/lang/String;

    .line 2074284
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aG()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->ay:Z

    .line 2074285
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aH()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->az:Z

    .line 2074286
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aI()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->aA:Z

    .line 2074287
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aJ()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->aB:Z

    .line 2074288
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aK()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2074289
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->en()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aD:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074290
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eo()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aE:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074291
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ep()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aF:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074292
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eq()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aG:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074293
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aP()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aH:Ljava/lang/String;

    .line 2074294
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aQ()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aI:LX/0Px;

    .line 2074295
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->er()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aJ:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    .line 2074296
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->es()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aK:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074297
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->et()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aL:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    .line 2074298
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aU()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aM:Ljava/lang/String;

    .line 2074299
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eu()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aN:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074300
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ev()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aO:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2074301
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aX()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aP:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 2074302
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aY()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aQ:LX/0Px;

    .line 2074303
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->aZ()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aR:LX/0Px;

    .line 2074304
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ew()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aS:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074305
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ex()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aT:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074306
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ey()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aU:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    .line 2074307
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bd()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aV:LX/0Px;

    .line 2074308
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ez()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aW:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 2074309
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bf()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aX:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 2074310
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aY:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074311
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bh()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->aZ:Ljava/lang/String;

    .line 2074312
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bi()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->ba:Z

    .line 2074313
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bj()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->bb:Z

    .line 2074314
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bk()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->bc:Z

    .line 2074315
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bl()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->bd:Z

    .line 2074316
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bm()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->be:Z

    .line 2074317
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bn()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->bf:Z

    .line 2074318
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eB()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bg:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    .line 2074319
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eC()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bh:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074320
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bq()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bi:Ljava/lang/String;

    .line 2074321
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->br()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bj:Ljava/lang/String;

    .line 2074322
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bs()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->bk:D

    .line 2074323
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eD()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bl:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    .line 2074324
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bu()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bm:LX/0Px;

    .line 2074325
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eE()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bn:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074326
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eF()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bo:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    .line 2074327
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eG()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bp:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;

    .line 2074328
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eH()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bq:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;

    .line 2074329
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eI()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->br:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    .line 2074330
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bs:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    .line 2074331
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bB()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bt:Ljava/lang/String;

    .line 2074332
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eK()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bu:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074333
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bD()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bv:Ljava/lang/String;

    .line 2074334
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bE()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bw:Ljava/lang/String;

    .line 2074335
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eL()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bx:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    .line 2074336
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eM()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->by:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    .line 2074337
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eN()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bz:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074338
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eO()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bA:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074339
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eP()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bB:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    .line 2074340
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bK()Z

    move-result v2

    iput-boolean v2, v1, LX/9wT;->bC:Z

    .line 2074341
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bL()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bD:LX/0Px;

    .line 2074342
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bM()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bE:LX/0Px;

    .line 2074343
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eQ()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bF:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074344
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bG:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    .line 2074345
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bP()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->bH:D

    .line 2074346
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eS()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bI:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074347
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eT()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bJ:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074348
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eU()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bK:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    .line 2074349
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bT()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bL:LX/0Px;

    .line 2074350
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bU()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bM:LX/0Px;

    .line 2074351
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eV()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bN:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074352
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bW()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bO:LX/0Px;

    .line 2074353
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eW()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bP:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    .line 2074354
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->bY()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->bQ:D

    .line 2074355
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eX()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bR:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    .line 2074356
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ca()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bS:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2074357
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cb()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bT:LX/0Px;

    .line 2074358
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cc()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bU:LX/0Px;

    .line 2074359
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cd()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bV:Ljava/lang/String;

    .line 2074360
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eY()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bW:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    .line 2074361
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->eZ()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bX:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074362
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fa()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bY:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 2074363
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fb()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ProfilePhotoModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->bZ:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ProfilePhotoModel;

    .line 2074364
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cg()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ca:LX/0Px;

    .line 2074365
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ch()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cb:Ljava/lang/String;

    .line 2074366
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ci()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cc:LX/0Px;

    .line 2074367
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cj()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cd:Ljava/lang/String;

    .line 2074368
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ck()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ce:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 2074369
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fc()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cf:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074370
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fd()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cg:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel;

    .line 2074371
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cn()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ch:LX/0Px;

    .line 2074372
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->co()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->ci:D

    .line 2074373
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fe()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cj:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074374
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cq()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->ck:D

    .line 2074375
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cr()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->cl:D

    .line 2074376
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cm:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 2074377
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ff()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cn:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074378
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fg()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->co:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;

    .line 2074379
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fh()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cp:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074380
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fi()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cq:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2074381
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cx()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cr:Ljava/lang/String;

    .line 2074382
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cs:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074383
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fk()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ct:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074384
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cA()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cu:Ljava/lang/String;

    .line 2074385
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cB()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cv:Ljava/lang/String;

    .line 2074386
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cC()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->cw:D

    .line 2074387
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fl()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cx:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074388
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cy:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074389
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fn()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cz:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;

    .line 2074390
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cG()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cA:LX/0Px;

    .line 2074391
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fo()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074392
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fp()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cC:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 2074393
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cD:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074394
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cI()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 2074395
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fr()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cF:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 2074396
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cK()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cG:LX/0Px;

    .line 2074397
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fs()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cH:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    .line 2074398
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->ft()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cI:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    .line 2074399
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cN()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cJ:Ljava/lang/String;

    .line 2074400
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fu()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cK:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074401
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cP()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cL:Ljava/lang/String;

    .line 2074402
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fv()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cM:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 2074403
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cR()D

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->cN:D

    .line 2074404
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->as()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cO:LX/0Px;

    .line 2074405
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fw()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cP:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    .line 2074406
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fx()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cQ:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074407
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fy()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cR:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074408
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fz()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cS:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074409
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fA()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074410
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fB()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cU:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074411
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fC()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cV:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 2074412
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fD()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cW:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074413
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->cZ()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cX:Ljava/lang/String;

    .line 2074414
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->da()J

    move-result-wide v3

    iput-wide v3, v1, LX/9wT;->cY:J

    .line 2074415
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->db()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->cZ:Ljava/lang/String;

    .line 2074416
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fE()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->da:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074417
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dd()I

    move-result v2

    iput v2, v1, LX/9wT;->db:I

    .line 2074418
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fF()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dc:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074419
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->df()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dd:LX/0Px;

    .line 2074420
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dg()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->de:LX/0Px;

    .line 2074421
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fG()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->df:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    .line 2074422
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fH()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dg:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;

    .line 2074423
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fI()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dh:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;

    .line 2074424
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dk()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->di:Ljava/lang/String;

    .line 2074425
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fJ()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dj:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074426
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fK()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dk:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    .line 2074427
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fL()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dl:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2074428
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->do()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dm:Ljava/lang/String;

    .line 2074429
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fM()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dn:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;

    .line 2074430
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fN()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->do:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074431
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fO()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dp:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2074432
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fP()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dq:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074433
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->fQ()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->dr:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074434
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->du()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/9wT;->ds:Ljava/lang/String;

    .line 2074435
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->dv()I

    move-result v2

    iput v2, v1, LX/9wT;->dt:I

    .line 2074436
    move-object v0, v1

    .line 2074437
    iput-object p2, v0, LX/9wT;->cE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 2074438
    move-object v0, v0

    .line 2074439
    invoke-virtual {v0}, LX/9wT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    invoke-interface {p0, p1, v0}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2074440
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2074171
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->a(LX/1RF;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2074168
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074169
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074170
    invoke-interface {v0}, LX/9uc;->di()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->di()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dj()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dj()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dh()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dh()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
