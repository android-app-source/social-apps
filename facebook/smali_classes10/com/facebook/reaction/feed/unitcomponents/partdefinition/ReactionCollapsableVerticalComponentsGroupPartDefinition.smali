.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

.field private final d:LX/Cfw;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;LX/Cfw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074058
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2074059
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    .line 2074060
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    .line 2074061
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    .line 2074062
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->d:LX/Cfw;

    .line 2074063
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;
    .locals 7

    .prologue
    .line 2074064
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;

    monitor-enter v1

    .line 2074065
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074066
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074067
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074068
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074069
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v6

    check-cast v6, LX/Cfw;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;LX/Cfw;)V

    .line 2074070
    move-object v0, p0

    .line 2074071
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074072
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074073
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;
    .locals 4

    .prologue
    .line 2074075
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2074076
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2074077
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2074078
    new-instance v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2074079
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    check-cast p3, LX/1Pr;

    const/4 v1, 0x0

    .line 2074080
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2074081
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2074082
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v2

    .line 2074083
    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074084
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074085
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074086
    new-instance v0, LX/E2U;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, LX/E2U;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2V;

    .line 2074087
    iget-boolean v2, v0, LX/E2V;->b:Z

    move v2, v2

    .line 2074088
    if-nez v2, :cond_2

    .line 2074089
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->COLLAPSED:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {v0, v1}, LX/E2V;->a(Z)V

    .line 2074090
    :cond_2
    iget-boolean v1, v0, LX/E2V;->a:Z

    move v0, v1

    .line 2074091
    if-nez v0, :cond_3

    .line 2074092
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074093
    :cond_3
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2074094
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2074095
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2074096
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->d:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableVerticalComponentsGroupPartDefinition;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentSelector;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
