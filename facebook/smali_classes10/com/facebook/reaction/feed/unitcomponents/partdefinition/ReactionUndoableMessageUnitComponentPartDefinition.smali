.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/E4B;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/E5X;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E5X;LX/2d1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076128
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2076129
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->d:LX/E5X;

    .line 2076130
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->e:LX/2d1;

    .line 2076131
    return-void
.end method

.method private a(LX/1De;LX/E4B;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/E4B;",
            ")",
            "LX/1X1",
            "<",
            "LX/E5X;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2076094
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->d:LX/E5X;

    const/4 v1, 0x0

    .line 2076095
    new-instance v2, LX/E5W;

    invoke-direct {v2, v0}, LX/E5W;-><init>(LX/E5X;)V

    .line 2076096
    sget-object p0, LX/E5X;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E5V;

    .line 2076097
    if-nez p0, :cond_0

    .line 2076098
    new-instance p0, LX/E5V;

    invoke-direct {p0}, LX/E5V;-><init>()V

    .line 2076099
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/E5V;->a$redex0(LX/E5V;LX/1De;IILX/E5W;)V

    .line 2076100
    move-object v2, p0

    .line 2076101
    move-object v1, v2

    .line 2076102
    move-object v0, v1

    .line 2076103
    iget-object v1, p2, LX/E4B;->b:Ljava/lang/String;

    .line 2076104
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput-object v1, v2, LX/E5W;->a:Ljava/lang/CharSequence;

    .line 2076105
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076106
    move-object v0, v0

    .line 2076107
    iget v1, p2, LX/E4B;->c:I

    .line 2076108
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput v1, v2, LX/E5W;->b:I

    .line 2076109
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076110
    move-object v0, v0

    .line 2076111
    iget-object v1, p2, LX/E4B;->d:Ljava/lang/String;

    .line 2076112
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput-object v1, v2, LX/E5W;->c:Ljava/lang/CharSequence;

    .line 2076113
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076114
    move-object v0, v0

    .line 2076115
    iget-object v1, p2, LX/E4B;->e:Landroid/view/View$OnClickListener;

    .line 2076116
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput-object v1, v2, LX/E5W;->f:Landroid/view/View$OnClickListener;

    .line 2076117
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x5

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076118
    move-object v0, v0

    .line 2076119
    iget v1, p2, LX/E4B;->a:I

    .line 2076120
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput v1, v2, LX/E5W;->d:I

    .line 2076121
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076122
    move-object v0, v0

    .line 2076123
    iget v1, p2, LX/E4B;->c:I

    .line 2076124
    iget-object v2, v0, LX/E5V;->a:LX/E5W;

    iput v1, v2, LX/E5W;->e:I

    .line 2076125
    iget-object v2, v0, LX/E5V;->d:Ljava/util/BitSet;

    const/4 p0, 0x4

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076126
    move-object v0, v0

    .line 2076127
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 2076083
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    monitor-enter v1

    .line 2076084
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076085
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076086
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076087
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076088
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E5X;->a(LX/0QB;)LX/E5X;

    move-result-object v4

    check-cast v4, LX/E5X;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/E5X;LX/2d1;)V

    .line 2076089
    move-object v0, p0

    .line 2076090
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076091
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076092
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076093
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2076082
    check-cast p2, LX/E4B;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->a(LX/1De;LX/E4B;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2076081
    check-cast p2, LX/E4B;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->a(LX/1De;LX/E4B;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2076077
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2076080
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionUndoableMessageUnitComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2076079
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2076078
    const/4 v0, 0x0

    return-object v0
.end method
