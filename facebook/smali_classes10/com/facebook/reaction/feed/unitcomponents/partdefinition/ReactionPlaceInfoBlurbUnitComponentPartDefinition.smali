.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E4u;

.field private final e:LX/E5A;

.field private final f:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E4u;LX/E5A;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075812
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2075813
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->d:LX/E4u;

    .line 2075814
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->e:LX/E5A;

    .line 2075815
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->f:LX/2d1;

    .line 2075816
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2075774
    move-object v0, p3

    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2Z;

    .line 2075775
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2075776
    invoke-direct {v1, v2}, LX/E2Z;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2a;

    .line 2075777
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->d:LX/E4u;

    invoke-virtual {v1, p1}, LX/E4u;->c(LX/1De;)LX/E4s;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->e:LX/E5A;

    const/4 v3, 0x0

    .line 2075778
    new-instance v4, LX/E59;

    invoke-direct {v4, v1}, LX/E59;-><init>(LX/E5A;)V

    .line 2075779
    sget-object p0, LX/E5A;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E58;

    .line 2075780
    if-nez p0, :cond_0

    .line 2075781
    new-instance p0, LX/E58;

    invoke-direct {p0}, LX/E58;-><init>()V

    .line 2075782
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/E58;->a$redex0(LX/E58;LX/1De;IILX/E59;)V

    .line 2075783
    move-object v4, p0

    .line 2075784
    move-object v3, v4

    .line 2075785
    move-object v1, v3

    .line 2075786
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2075787
    invoke-interface {v3}, LX/9uc;->ar()LX/174;

    move-result-object v3

    .line 2075788
    iget-object v4, v1, LX/E58;->a:LX/E59;

    iput-object v3, v4, LX/E59;->c:LX/174;

    .line 2075789
    move-object v1, v1

    .line 2075790
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2075791
    invoke-interface {v3}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2075792
    iget-object v4, v1, LX/E58;->a:LX/E59;

    iput-object v3, v4, LX/E59;->a:Ljava/lang/String;

    .line 2075793
    iget-object v4, v1, LX/E58;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075794
    move-object v1, v1

    .line 2075795
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2075796
    invoke-interface {v3}, LX/9uc;->bX()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    move-result-object v3

    .line 2075797
    iget-object v4, v1, LX/E58;->a:LX/E59;

    iput-object v3, v4, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    .line 2075798
    iget-object v4, v1, LX/E58;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075799
    move-object v1, v1

    .line 2075800
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2075801
    invoke-interface {v3}, LX/9uc;->de()LX/174;

    move-result-object v3

    .line 2075802
    iget-object v4, v1, LX/E58;->a:LX/E59;

    iput-object v3, v4, LX/E59;->d:LX/174;

    .line 2075803
    move-object v3, v1

    .line 2075804
    iget-boolean v1, v0, LX/E2a;->a:Z

    move v1, v1

    .line 2075805
    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 2075806
    :goto_0
    iget-object v4, v3, LX/E58;->a:LX/E59;

    iput-boolean v1, v4, LX/E59;->e:Z

    .line 2075807
    move-object v1, v3

    .line 2075808
    iget-object v3, v2, LX/E4s;->a:LX/E4t;

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    iput-object v4, v3, LX/E4t;->a:LX/1X1;

    .line 2075809
    iget-object v3, v2, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2075810
    move-object v1, v2

    .line 2075811
    invoke-virtual {v1, p2}, LX/E4s;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E4s;

    move-result-object v2

    move-object v1, p3

    check-cast v1, LX/3U9;

    invoke-interface {v1}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/E4s;->a(LX/2ja;)LX/E4s;

    move-result-object v1

    check-cast p3, LX/1Pq;

    invoke-virtual {v1, p3}, LX/E4s;->a(LX/1Pq;)LX/E4s;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/E4s;->a(LX/E2a;)LX/E4s;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 2075763
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075764
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075765
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075768
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E4u;->a(LX/0QB;)LX/E4u;

    move-result-object v5

    check-cast v5, LX/E4u;

    invoke-static {v0}, LX/E5A;->a(LX/0QB;)LX/E5A;

    move-result-object v6

    check-cast v6, LX/E5A;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E4u;LX/E5A;)V

    .line 2075769
    move-object v0, p0

    .line 2075770
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075771
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075772
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2075762
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2075761
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2075760
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2075759
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbUnitComponentPartDefinition;->f:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2075752
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075753
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075754
    invoke-interface {v0}, LX/9uc;->bX()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2075755
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075756
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2075757
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075758
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2075751
    const/4 v0, 0x0

    return-object v0
.end method
