.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3q;",
        "TE;",
        "LX/E4V;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2075443
    new-instance v0, LX/E3p;

    invoke-direct {v0}, LX/E3p;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075444
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2075445
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2075446
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;
    .locals 4

    .prologue
    .line 2075447
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;

    monitor-enter v1

    .line 2075448
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075449
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075450
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075451
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075452
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2075453
    move-object v0, p0

    .line 2075454
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075455
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075456
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075457
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2075458
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2075459
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v6, 0x0

    .line 2075460
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 2075461
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageContextualRecommendationEventFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v1, LX/E1o;

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 2075462
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2075463
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2075464
    invoke-direct {v1, v3, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2075465
    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    .line 2075466
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v4

    .line 2075467
    invoke-interface {v2}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v5

    .line 2075468
    invoke-interface {v2}, LX/9uc;->hk_()LX/174;

    move-result-object v7

    move-object v0, p3

    .line 2075469
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2075470
    check-cast p3, LX/2kn;

    invoke-interface {p3}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    .line 2075471
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2075472
    iput-object v1, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    .line 2075473
    :cond_0
    new-instance v0, LX/E3q;

    invoke-interface {v2}, LX/9uc;->bE()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->ah()Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v4, :cond_3

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    :goto_1
    if-eqz v7, :cond_1

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    :cond_1
    invoke-direct/range {v0 .. v6}, LX/E3q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;Ljava/lang/String;)V

    return-object v0

    :cond_2
    move-object v3, v6

    goto :goto_0

    :cond_3
    move-object v4, v6

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6682aa35

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075474
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/E3q;

    check-cast p3, LX/2km;

    check-cast p4, LX/E4V;

    .line 2075475
    move-object v1, p3

    check-cast v1, LX/3U9;

    invoke-interface {v1}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    .line 2075476
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2075477
    iget-object v4, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2075478
    iput-object v1, p4, LX/E4V;->o:LX/2ja;

    .line 2075479
    iput-object v2, p4, LX/E4V;->p:Ljava/lang/String;

    .line 2075480
    iput-object v4, p4, LX/E4V;->q:Ljava/lang/String;

    .line 2075481
    iget-object v1, p2, LX/E3q;->b:Ljava/lang/String;

    iget-object v2, p2, LX/E3q;->a:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2075482
    iget-object v1, p2, LX/E3q;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2075483
    iget-object v1, p2, LX/E3q;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p4, v1, v2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2075484
    iget-object v1, p2, LX/E3q;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 2075485
    iget-object v2, p4, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-object v2, v2

    .line 2075486
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 2075487
    iput-object v1, p4, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 2075488
    iget-object v4, p4, LX/E4V;->j:LX/38v;

    invoke-virtual {v4, p4}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->p()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->r()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object p1

    invoke-virtual {v4, v5, p0, p1}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2075489
    iget-object v1, p2, LX/E3q;->f:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 2075490
    check-cast p3, LX/2kn;

    invoke-interface {p3}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v1

    .line 2075491
    iput-object v1, p4, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2075492
    const/16 v1, 0x1f

    const v2, 0x6dfd3bd2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2075493
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075494
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075495
    invoke-interface {v0}, LX/9uc;->ah()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bE()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2075496
    check-cast p4, LX/E4V;

    .line 2075497
    invoke-virtual {p4}, LX/E4V;->g()V

    .line 2075498
    return-void
.end method
