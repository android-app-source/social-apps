.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/E5E;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E5E;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075817
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2075818
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->d:LX/E5E;

    .line 2075819
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2075820
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075821
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->d:LX/E5E;

    const/4 v2, 0x0

    .line 2075822
    new-instance v3, LX/E5D;

    invoke-direct {v3, v1}, LX/E5D;-><init>(LX/E5E;)V

    .line 2075823
    iget-object p0, v1, LX/E5E;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E5C;

    .line 2075824
    if-nez p0, :cond_0

    .line 2075825
    new-instance p0, LX/E5C;

    invoke-direct {p0, v1}, LX/E5C;-><init>(LX/E5E;)V

    .line 2075826
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/E5C;->a$redex0(LX/E5C;LX/1De;IILX/E5D;)V

    .line 2075827
    move-object v3, p0

    .line 2075828
    move-object v2, v3

    .line 2075829
    move-object v1, v2

    .line 2075830
    invoke-interface {v0}, LX/9uc;->bW()LX/0Px;

    move-result-object v2

    .line 2075831
    iget-object v3, v1, LX/E5C;->a:LX/E5D;

    iput-object v2, v3, LX/E5D;->a:LX/0Px;

    .line 2075832
    iget-object v3, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075833
    move-object v1, v1

    .line 2075834
    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2075835
    iget-object v3, v1, LX/E5C;->a:LX/E5D;

    iput-object v2, v3, LX/E5D;->b:Ljava/lang/String;

    .line 2075836
    iget-object v3, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075837
    move-object v1, v1

    .line 2075838
    invoke-interface {v0}, LX/9uc;->aQ()LX/0Px;

    move-result-object v2

    .line 2075839
    iget-object v3, v1, LX/E5C;->a:LX/E5D;

    iput-object v2, v3, LX/E5D;->c:LX/0Px;

    .line 2075840
    iget-object v3, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075841
    move-object v1, v1

    .line 2075842
    invoke-interface {v0}, LX/9uc;->cd()Ljava/lang/String;

    move-result-object v2

    .line 2075843
    iget-object v3, v1, LX/E5C;->a:LX/E5D;

    iput-object v2, v3, LX/E5D;->d:Ljava/lang/String;

    .line 2075844
    iget-object v3, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2075845
    move-object v1, v1

    .line 2075846
    invoke-interface {v0}, LX/9uc;->bY()D

    move-result-wide v2

    .line 2075847
    iget-object v4, v1, LX/E5C;->a:LX/E5D;

    iput-wide v2, v4, LX/E5D;->e:D

    .line 2075848
    iget-object v4, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2075849
    move-object v1, v1

    .line 2075850
    invoke-interface {v0}, LX/9uc;->co()D

    move-result-wide v2

    .line 2075851
    iget-object v4, v1, LX/E5C;->a:LX/E5D;

    iput-wide v2, v4, LX/E5D;->f:D

    .line 2075852
    iget-object v4, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2075853
    move-object v1, v1

    .line 2075854
    invoke-interface {v0}, LX/9uc;->db()Ljava/lang/String;

    move-result-object v0

    .line 2075855
    iget-object v2, v1, LX/E5C;->a:LX/E5D;

    iput-object v0, v2, LX/E5D;->g:Ljava/lang/String;

    .line 2075856
    iget-object v2, v1, LX/E5C;->e:Ljava/util/BitSet;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2075857
    move-object v0, v1

    .line 2075858
    iget-object v1, v0, LX/E5C;->a:LX/E5D;

    iput-object p3, v1, LX/E5D;->h:LX/2km;

    .line 2075859
    iget-object v1, v0, LX/E5C;->e:Ljava/util/BitSet;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2075860
    move-object v0, v0

    .line 2075861
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2075862
    iget-object v2, v0, LX/E5C;->a:LX/E5D;

    iput-object v1, v2, LX/E5D;->i:Ljava/lang/String;

    .line 2075863
    iget-object v2, v0, LX/E5C;->e:Ljava/util/BitSet;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2075864
    move-object v0, v0

    .line 2075865
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2075866
    iget-object v2, v0, LX/E5C;->a:LX/E5D;

    iput-object v1, v2, LX/E5D;->j:Ljava/lang/String;

    .line 2075867
    iget-object v2, v0, LX/E5C;->e:Ljava/util/BitSet;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2075868
    move-object v0, v0

    .line 2075869
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2075870
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075871
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075872
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075873
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075874
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075875
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E5E;->a(LX/0QB;)LX/E5E;

    move-result-object v4

    check-cast v4, LX/E5E;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/E5E;)V

    .line 2075876
    move-object v0, p0

    .line 2075877
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075878
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075879
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075880
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2075881
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2075882
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceInfoBlurbWithBreadcrumbsUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2075883
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075884
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075885
    invoke-interface {v0}, LX/9uc;->bW()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2075886
    const/4 v0, 0x0

    return-object v0
.end method
