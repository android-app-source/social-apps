.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2kp;",
        ":",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private d:LX/E5P;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E5P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076011
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2076012
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->d:LX/E5P;

    .line 2076013
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->e:LX/2d1;

    .line 2076014
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/1X1",
            "<",
            "LX/E5P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2076026
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->d:LX/E5P;

    const/4 v1, 0x0

    .line 2076027
    new-instance v2, LX/E5O;

    invoke-direct {v2, v0}, LX/E5O;-><init>(LX/E5P;)V

    .line 2076028
    sget-object p0, LX/E5P;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E5N;

    .line 2076029
    if-nez p0, :cond_0

    .line 2076030
    new-instance p0, LX/E5N;

    invoke-direct {p0}, LX/E5N;-><init>()V

    .line 2076031
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/E5N;->a$redex0(LX/E5N;LX/1De;IILX/E5O;)V

    .line 2076032
    move-object v2, p0

    .line 2076033
    move-object v1, v2

    .line 2076034
    move-object v0, v1

    .line 2076035
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2076036
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2076037
    iget-object v2, v0, LX/E5N;->a:LX/E5O;

    iput-object v1, v2, LX/E5O;->a:Ljava/lang/String;

    .line 2076038
    iget-object v2, v0, LX/E5N;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2076039
    move-object v0, v0

    .line 2076040
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;
    .locals 6

    .prologue
    .line 2076015
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;

    monitor-enter v1

    .line 2076016
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076017
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076018
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076019
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076020
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E5P;->a(LX/0QB;)LX/E5P;

    move-result-object v5

    check-cast v5, LX/E5P;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E5P;)V

    .line 2076021
    move-object v0, p0

    .line 2076022
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076023
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076024
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2076009
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2076010
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2076008
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2076007
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2076006
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSectionHeaderPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2076001
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076002
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2076003
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2076004
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2076005
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2076000
    const/4 v0, 0x0

    return-object v0
.end method
