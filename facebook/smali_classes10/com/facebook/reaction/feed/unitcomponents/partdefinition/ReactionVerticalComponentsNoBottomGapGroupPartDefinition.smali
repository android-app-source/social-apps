.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

.field private final d:LX/1vo;

.field private final e:LX/Cfw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2076191
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->f:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;LX/1vo;LX/Cfw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076157
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2076158
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    .line 2076159
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    .line 2076160
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    .line 2076161
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->d:LX/1vo;

    .line 2076162
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->e:LX/Cfw;

    .line 2076163
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;
    .locals 9

    .prologue
    .line 2076180
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    monitor-enter v1

    .line 2076181
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076182
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076183
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076184
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076185
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v7

    check-cast v7, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v8

    check-cast v8, LX/Cfw;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;LX/1vo;LX/Cfw;)V

    .line 2076186
    move-object v0, v3

    .line 2076187
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076188
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076189
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076190
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z
    .locals 2

    .prologue
    .line 2076192
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->f:LX/0Rf;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2076166
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    const/4 v1, 0x0

    .line 2076167
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2076168
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 2076169
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2076170
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    .line 2076171
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->d:LX/1vo;

    invoke-virtual {v5, v4}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v5

    .line 2076172
    if-eqz v5, :cond_1

    .line 2076173
    new-instance v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {v6, v0, v2, v3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2076174
    invoke-interface {v5, v6}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-virtual {v0, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 2076175
    :goto_0
    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 2076176
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076177
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076178
    const/4 v0, 0x0

    return-object v0

    :cond_2
    move v0, v1

    .line 2076179
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2076164
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076165
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;->e:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
