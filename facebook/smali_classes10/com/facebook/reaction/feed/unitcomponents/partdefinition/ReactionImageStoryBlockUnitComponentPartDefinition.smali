.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3l;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/E1i;

.field public b:Landroid/text/style/TextAppearanceSpan;

.field public c:Landroid/text/style/TextAppearanceSpan;


# direct methods
.method public constructor <init>(LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075269
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2075270
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->a:LX/E1i;

    .line 2075271
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 2075272
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075273
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075274
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075275
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075276
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075277
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;-><init>(LX/E1i;)V

    .line 2075278
    move-object v0, p0

    .line 2075279
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075280
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075281
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075282
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2075283
    sget-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2075284
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 2075285
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 2075286
    invoke-interface {v3}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2075287
    invoke-interface {v3}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2075288
    invoke-interface {v3}, LX/9uc;->aj()LX/5sY;

    move-result-object v4

    .line 2075289
    new-instance v5, LX/E3k;

    invoke-direct {v5, p0, v3, p3, p2}, LX/E3k;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;LX/9uc;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2075290
    new-instance v0, LX/E3l;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 p2, 0x0

    const/16 p3, 0x21

    .line 2075291
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2075292
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->b:Landroid/text/style/TextAppearanceSpan;

    if-nez v8, :cond_0

    .line 2075293
    new-instance v8, Landroid/text/style/TextAppearanceSpan;

    const p1, 0x7f0e08fd

    invoke-direct {v8, v6, p1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->b:Landroid/text/style/TextAppearanceSpan;

    .line 2075294
    :cond_0
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->c:Landroid/text/style/TextAppearanceSpan;

    if-nez v8, :cond_1

    .line 2075295
    new-instance v8, Landroid/text/style/TextAppearanceSpan;

    const p1, 0x7f0e08fe

    invoke-direct {v8, v6, p1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->c:Landroid/text/style/TextAppearanceSpan;

    .line 2075296
    :cond_1
    if-eqz v2, :cond_3

    .line 2075297
    const-string v8, " - "

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2075298
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 2075299
    invoke-virtual {v7, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2075300
    iget-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->b:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v7, p1, p2, v8, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2075301
    iget-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->c:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    invoke-virtual {v7, p1, v8, p2, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2075302
    :goto_0
    move-object v1, v7

    .line 2075303
    invoke-interface {v3}, LX/9uc;->hk_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3}, LX/9uc;->aV()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v4, :cond_2

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_1
    new-instance v6, LX/3Cx;

    invoke-direct {v6}, LX/3Cx;-><init>()V

    invoke-direct/range {v0 .. v6}, LX/E3l;-><init>(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 2075304
    :cond_3
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->b:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-virtual {v7, v8, p2, p1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1d306b18

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075305
    check-cast p2, LX/E3l;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;

    .line 2075306
    iget-object v1, p2, LX/E3l;->c:Landroid/net/Uri;

    .line 2075307
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2075308
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const p0, 0x7f0a010a

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2075309
    iget-object v1, p2, LX/E3l;->d:Landroid/net/Uri;

    const/4 p1, 0x0

    .line 2075310
    if-nez v1, :cond_0

    .line 2075311
    :goto_0
    iget-object v1, p2, LX/E3l;->a:Landroid/text/SpannableStringBuilder;

    .line 2075312
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2075313
    iget-object v1, p2, LX/E3l;->b:Ljava/lang/String;

    .line 2075314
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2075315
    iget-object v1, p2, LX/E3l;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2075316
    iget-object v1, p2, LX/E3l;->f:Landroid/view/View$OnTouchListener;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2075317
    const/16 v1, 0x1f

    const v2, -0x3c985032

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2075318
    :cond_0
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v2, :cond_1

    .line 2075319
    const v2, 0x7f0d2885

    invoke-virtual {p4, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 2075320
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2075321
    :cond_1
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2075322
    iget-object v2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p1, p1, p1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2075323
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075324
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075325
    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->hk_()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->hk_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2075326
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/E3l;

    check-cast p3, LX/2km;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;

    const/4 v0, 0x0

    .line 2075327
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 2075328
    invoke-virtual {p4, v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2075329
    invoke-virtual {p4, v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2075330
    return-void
.end method
