.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;
.super Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074461
    invoke-direct {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2074462
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 2074463
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074464
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074465
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074466
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074467
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074468
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2074469
    move-object v0, p0

    .line 2074470
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074471
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowWithCoverPhotoUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074472
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 2074474
    const/4 v0, 0x1

    return v0
.end method
