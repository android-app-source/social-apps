.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/E6I;

.field private final b:LX/Cfw;

.field private final c:LX/1vo;


# direct methods
.method public constructor <init>(LX/E6I;LX/Cfw;LX/1vo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076243
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2076244
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a:LX/E6I;

    .line 2076245
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->b:LX/Cfw;

    .line 2076246
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->c:LX/1vo;

    .line 2076247
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;
    .locals 6

    .prologue
    .line 2076248
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    monitor-enter v1

    .line 2076249
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2076250
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2076251
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076252
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2076253
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;

    invoke-static {v0}, LX/E6I;->b(LX/0QB;)LX/E6I;

    move-result-object v3

    check-cast v3, LX/E6I;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v4

    check-cast v4, LX/Cfw;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v5

    check-cast v5, LX/1vo;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;-><init>(LX/E6I;LX/Cfw;LX/1vo;)V

    .line 2076254
    move-object v0, p0

    .line 2076255
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2076256
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076257
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2076258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2076259
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2076260
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    .line 2076261
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 2076262
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 2076263
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 2076264
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2076265
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    .line 2076266
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->c:LX/1vo;

    invoke-virtual {v6, v5}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v6

    .line 2076267
    new-instance v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {v7, v0, v3, v4}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2076268
    if-eqz v6, :cond_1

    .line 2076269
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076270
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a:LX/E6I;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    add-int/lit8 v9, v1, -0x1

    invoke-virtual {v0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    invoke-virtual {v8, v0, v5}, LX/E6I;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2076271
    if-eqz v0, :cond_0

    .line 2076272
    invoke-virtual {p1, v0, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076273
    :cond_0
    invoke-virtual {p1, v6, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2076274
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2076275
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2076276
    const-string v1, "SUCCESS"

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->b:LX/Cfw;

    invoke-virtual {v2, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2076277
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsWithoutHeaderGroupPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z

    move-result v0

    return v0
.end method
