.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2ko;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3d;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/E4S;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final c:LX/2dq;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/1vo;

.field private final f:LX/Cfw;

.field public final g:LX/1Qx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2074906
    new-instance v0, LX/E3a;

    invoke-direct {v0}, LX/E3a;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    .line 2074907
    new-instance v0, LX/E3b;

    invoke-direct {v0}, LX/E3b;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;LX/1Qx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074899
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2074900
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->c:LX/2dq;

    .line 2074901
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2074902
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->e:LX/1vo;

    .line 2074903
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->f:LX/Cfw;

    .line 2074904
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    .line 2074905
    return-void
.end method

.method private a(LX/0Px;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;",
            "LX/E2b;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2074898
    new-instance v0, LX/E3c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/E3c;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2b;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;
    .locals 9

    .prologue
    .line 2074861
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074862
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074863
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074864
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074865
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074866
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v4

    check-cast v4, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v6

    check-cast v6, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v7

    check-cast v7, LX/Cfw;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v8

    check-cast v8, LX/1Qx;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;-><init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;LX/1Qx;)V

    .line 2074867
    move-object v0, v3

    .line 2074868
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074869
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074870
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074871
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2074897
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2074879
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x1

    .line 2074880
    new-instance v3, LX/E2b;

    .line 2074881
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2074882
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    .line 2074883
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    .line 2074884
    if-nez v0, :cond_0

    .line 2074885
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2074886
    :cond_0
    move-object v4, v0

    .line 2074887
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1625

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 2074888
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-le v1, v5, :cond_1

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->c:LX/2dq;

    int-to-float v0, v0

    const/high16 v2, 0x41000000    # 8.0f

    add-float/2addr v0, v2

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v0, v2, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    :goto_0
    move-object v0, p3

    .line 2074889
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2074890
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    .line 2074891
    iput v2, v0, LX/E2c;->c:I

    .line 2074892
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    move-object v2, p3

    check-cast v2, LX/1Pr;

    invoke-interface {v2, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2c;

    .line 2074893
    iget v5, v2, LX/E2c;->d:I

    move v2, v5

    .line 2074894
    invoke-direct {p0, v4, v3, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->a(LX/0Px;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2074895
    new-instance v0, LX/E3d;

    check-cast p3, LX/2ko;

    invoke-interface {p3, p2}, LX/2ko;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E3d;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-object v0

    .line 2074896
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->c:LX/2dq;

    sget-object v1, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v0, v1}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x60ab8ce2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2074876
    check-cast p2, LX/E3d;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2074877
    iget-object v1, p2, LX/E3d;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2074878
    const/16 v1, 0x1f

    const v2, -0x60a54829

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2074872
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074873
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->f:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074874
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074875
    instance-of v0, v0, LX/9ud;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
