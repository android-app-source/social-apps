.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/E4j;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E4j;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073910
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2073911
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->d:LX/E4j;

    .line 2073912
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2073913
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2073914
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->d:LX/E4j;

    const/4 v2, 0x0

    .line 2073915
    new-instance v3, LX/E4i;

    invoke-direct {v3, v1}, LX/E4i;-><init>(LX/E4j;)V

    .line 2073916
    iget-object p0, v1, LX/E4j;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E4h;

    .line 2073917
    if-nez p0, :cond_0

    .line 2073918
    new-instance p0, LX/E4h;

    invoke-direct {p0, v1}, LX/E4h;-><init>(LX/E4j;)V

    .line 2073919
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/E4h;->a$redex0(LX/E4h;LX/1De;IILX/E4i;)V

    .line 2073920
    move-object v3, p0

    .line 2073921
    move-object v2, v3

    .line 2073922
    move-object v1, v2

    .line 2073923
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2073924
    iget-object v3, v1, LX/E4h;->a:LX/E4i;

    iput-object v2, v3, LX/E4i;->b:Ljava/lang/String;

    .line 2073925
    iget-object v3, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2073926
    move-object v1, v1

    .line 2073927
    invoke-interface {v0}, LX/9uc;->cU()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2073928
    iget-object v3, v1, LX/E4h;->a:LX/E4i;

    iput-object v2, v3, LX/E4i;->c:Ljava/lang/String;

    .line 2073929
    iget-object v3, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2073930
    move-object v1, v1

    .line 2073931
    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2073932
    iget-object v3, v1, LX/E4h;->a:LX/E4i;

    iput-object v2, v3, LX/E4i;->a:Ljava/lang/String;

    .line 2073933
    iget-object v3, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2073934
    move-object v1, v1

    .line 2073935
    iget-object v2, v1, LX/E4h;->a:LX/E4i;

    iput-object p2, v2, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2073936
    iget-object v2, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2073937
    move-object v1, v1

    .line 2073938
    iget-object v2, v1, LX/E4h;->a:LX/E4i;

    iput-object p3, v2, LX/E4i;->f:LX/2km;

    .line 2073939
    iget-object v2, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2073940
    move-object v1, v1

    .line 2073941
    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    .line 2073942
    iget-object v3, v1, LX/E4h;->a:LX/E4i;

    iput-object v2, v3, LX/E4i;->e:Ljava/lang/String;

    .line 2073943
    iget-object v3, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 p0, 0x4

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2073944
    move-object v1, v1

    .line 2073945
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 2073946
    iget-object v2, v1, LX/E4h;->a:LX/E4i;

    iput-object v0, v2, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2073947
    iget-object v2, v1, LX/E4h;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2073948
    move-object v0, v1

    .line 2073949
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;
    .locals 5

    .prologue
    .line 2073950
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;

    monitor-enter v1

    .line 2073951
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073952
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073955
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/E4j;->a(LX/0QB;)LX/E4j;

    move-result-object v4

    check-cast v4, LX/E4j;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;-><init>(Landroid/content/Context;LX/E4j;)V

    .line 2073956
    move-object v0, p0

    .line 2073957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2073961
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2073962
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionAdinterfacesObjectiveBlockComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2073963
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2073964
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2073965
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cU()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cU()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_PAGE_ADS_AND_PROMOTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
