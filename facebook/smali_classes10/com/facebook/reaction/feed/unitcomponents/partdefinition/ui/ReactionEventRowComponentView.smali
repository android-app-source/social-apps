.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/Bni;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;

.field public static final o:[I


# instance fields
.field public A:Ljava/lang/String;

.field public j:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/7vZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/7vW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/widget/text/BetterTextView;

.field public q:Lcom/facebook/widget/text/BetterTextView;

.field public r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

.field public s:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

.field public t:Lcom/facebook/widget/text/BetterTextView;

.field public u:Landroid/content/res/Resources;

.field public v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

.field public w:LX/1aX;

.field public x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public y:LX/2ja;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2076562
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;

    const-string v1, "event_profile_pic"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 2076563
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010730

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f010731

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->o:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2076542
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2076543
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;

    invoke-static {v0, p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2076544
    const v0, 0x7f03114b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2076545
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->u:Landroid/content/res/Resources;

    .line 2076546
    const v0, 0x7f0d28dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    .line 2076547
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->o:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2076548
    const/4 v1, 0x0

    const v2, 0x7f0e090b

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2076549
    const/4 v2, 0x1

    const p1, 0x7f0e090c

    invoke-virtual {v0, v2, p1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 2076550
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2076551
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(II)V

    .line 2076552
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->u:Landroid/content/res/Resources;

    const v1, 0x7f02065f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2076553
    new-instance v1, LX/1Uo;

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->u:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2076554
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->w:LX/1aX;

    .line 2076555
    const v0, 0x7f0d28de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2076556
    const v0, 0x7f0d28df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2076557
    const v0, 0x7f0d28e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->s:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    .line 2076558
    const v0, 0x7f0d28e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2076559
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2076560
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->u:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2076561
    return-void
.end method

.method public static a(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;LX/Cfc;)V
    .locals 4

    .prologue
    .line 2076539
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->y:LX/2ja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2076540
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->y:LX/2ja;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p1, v3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    .line 2076541
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;

    const-class v1, LX/38v;

    invoke-interface {v4, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/38v;

    invoke-static {v4}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v2

    check-cast v2, LX/7vZ;

    invoke-static {v4}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v3

    check-cast v3, LX/7vW;

    const/16 p0, 0x509

    invoke-static {v4, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    iput-object v1, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->j:LX/38v;

    iput-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->k:LX/7vZ;

    iput-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->l:LX/7vW;

    iput-object v4, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->m:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 7

    .prologue
    .line 2076529
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    if-nez v0, :cond_0

    .line 2076530
    :goto_0
    return-void

    .line 2076531
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->l:LX/7vW;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v3, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v4, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    :goto_2
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v5, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    :goto_4
    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2076532
    sget-object v0, LX/E4M;->b:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2076533
    :goto_5
    goto :goto_0

    .line 2076534
    :cond_1
    const-string v3, "unknown"

    goto :goto_1

    :cond_2
    const-string v4, "unknown"

    goto :goto_2

    :cond_3
    const-string v5, "unknown"

    goto :goto_3

    :cond_4
    const-string v6, "unknown"

    goto :goto_4

    .line 2076535
    :pswitch_0
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    .line 2076536
    :goto_6
    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->a(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;LX/Cfc;)V

    goto :goto_5

    .line 2076537
    :pswitch_1
    sget-object v0, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    goto :goto_6

    .line 2076538
    :pswitch_2
    sget-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 7

    .prologue
    .line 2076518
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    if-nez v0, :cond_0

    .line 2076519
    :goto_0
    return-void

    .line 2076520
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->k:LX/7vZ;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v3, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v4, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    :goto_2
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v5, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    :goto_4
    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2076521
    sget-object v0, LX/E4M;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2076522
    :goto_5
    goto :goto_0

    .line 2076523
    :cond_1
    const-string v3, "unknown"

    goto :goto_1

    :cond_2
    const-string v4, "unknown"

    goto :goto_2

    :cond_3
    const-string v5, "unknown"

    goto :goto_3

    :cond_4
    const-string v6, "unknown"

    goto :goto_4

    .line 2076524
    :pswitch_0
    sget-object v0, LX/Cfc;->EVENT_CARD_WATCHED_TAP:LX/Cfc;

    .line 2076525
    :goto_6
    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->a(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;LX/Cfc;)V

    goto :goto_5

    .line 2076526
    :pswitch_1
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    goto :goto_6

    .line 2076527
    :pswitch_2
    sget-object v0, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    goto :goto_6

    .line 2076528
    :pswitch_3
    sget-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076499
    if-nez p1, :cond_0

    .line 2076500
    :goto_0
    return-void

    .line 2076501
    :cond_0
    if-nez p2, :cond_1

    .line 2076502
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->w:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 2076503
    :goto_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->w:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2076504
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2076505
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->w:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076515
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->r:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setVisibility(I)V

    .line 2076516
    return-void

    .line 2076517
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setEventInfo(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2076513
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2076514
    return-void
.end method

.method public setEventSocialContext(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076508
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076509
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->t:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2076510
    :goto_0
    return-void

    .line 2076511
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->t:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2076512
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->t:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setEventTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2076506
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2076507
    return-void
.end method
