.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/E4y;

.field private final e:LX/E4P;

.field private final f:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E4y;LX/E4P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074808
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2074809
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->d:LX/E4y;

    .line 2074810
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->f:LX/2d1;

    .line 2074811
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->e:LX/E4P;

    .line 2074812
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<",
            "LX/E4y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2074771
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 2074772
    const/4 v0, 0x0

    .line 2074773
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2074774
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2074775
    :goto_0
    new-instance v3, LX/E2T;

    invoke-interface {v2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, LX/E2T;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2074776
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2S;

    .line 2074777
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->d:LX/E4y;

    invoke-virtual {v3, p1}, LX/E4y;->c(LX/1De;)LX/E4w;

    move-result-object v3

    invoke-interface {v2}, LX/9uc;->aW()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/E4w;->a(Landroid/net/Uri;)LX/E4w;

    move-result-object v3

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/E4w;->b(Ljava/lang/String;)LX/E4w;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/E4w;->c(Ljava/lang/String;)LX/E4w;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/E4w;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4w;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/E4w;->d(Ljava/lang/String;)LX/E4w;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/E4w;->a(LX/2km;)LX/E4w;

    move-result-object v1

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v3

    .line 2074778
    iget-object v4, v1, LX/E4w;->a:LX/E4x;

    iput-object v3, v4, LX/E4x;->g:LX/4Ab;

    .line 2074779
    move-object v1, v1

    .line 2074780
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2074781
    invoke-virtual {v1, v3}, LX/E4w;->e(Ljava/lang/String;)LX/E4w;

    move-result-object v1

    .line 2074782
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2074783
    invoke-virtual {v1, v3}, LX/E4w;->f(Ljava/lang/String;)LX/E4w;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/E4w;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4w;

    move-result-object v1

    .line 2074784
    invoke-interface {v2}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->b()LX/174;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2074785
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 2074786
    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->e:LX/E4P;

    const/4 v5, 0x0

    .line 2074787
    new-instance p0, LX/E4O;

    invoke-direct {p0, v4}, LX/E4O;-><init>(LX/E4P;)V

    .line 2074788
    iget-object p2, v4, LX/E4P;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/E4N;

    .line 2074789
    if-nez p2, :cond_0

    .line 2074790
    new-instance p2, LX/E4N;

    invoke-direct {p2, v4}, LX/E4N;-><init>(LX/E4P;)V

    .line 2074791
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/E4N;->a$redex0(LX/E4N;LX/1De;IILX/E4O;)V

    .line 2074792
    move-object p0, p2

    .line 2074793
    move-object v5, p0

    .line 2074794
    move-object v4, v5

    .line 2074795
    invoke-interface {v2}, LX/9uc;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 2074796
    iget-object v5, v4, LX/E4N;->a:LX/E4O;

    iput-object v2, v5, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2074797
    move-object v2, v4

    .line 2074798
    iget-object v4, v2, LX/E4N;->a:LX/E4O;

    iput-object v0, v4, LX/E4O;->b:LX/E2S;

    .line 2074799
    move-object v0, v2

    .line 2074800
    check-cast p3, LX/1Pq;

    .line 2074801
    iget-object v2, v0, LX/E4N;->a:LX/E4O;

    iput-object p3, v2, LX/E4O;->c:LX/1Pq;

    .line 2074802
    iget-object v2, v0, LX/E4N;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 2074803
    move-object v0, v0

    .line 2074804
    iget-object v2, v0, LX/E4N;->a:LX/E4O;

    iput-object v3, v2, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2074805
    move-object v0, v0

    .line 2074806
    iget-object v2, v1, LX/E4w;->a:LX/E4x;

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    iput-object v3, v2, LX/E4x;->f:LX/1X1;

    .line 2074807
    :cond_1
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 2074751
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 2074752
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2074753
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2074754
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074755
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2074756
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E4y;->a(LX/0QB;)LX/E4y;

    move-result-object v5

    check-cast v5, LX/E4y;

    invoke-static {v0}, LX/E4P;->a(LX/0QB;)LX/E4P;

    move-result-object v6

    check-cast v6, LX/E4P;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E4y;LX/E4P;)V

    .line 2074757
    move-object v0, p0

    .line 2074758
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2074759
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074760
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2074761
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2074770
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->f:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2074769
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2074768
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2074767
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2074766
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupImageBlockUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2074763
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074764
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2074765
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aV()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2074762
    const/4 v0, 0x0

    return-object v0
.end method
