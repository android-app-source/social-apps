.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;
.super Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# direct methods
.method public constructor <init>(LX/1Ad;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2075691
    invoke-direct {p0, p1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;-><init>(LX/1Ad;LX/E1i;)V

    .line 2075692
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 2075693
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;

    monitor-enter v1

    .line 2075694
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2075695
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2075696
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075697
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2075698
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v4

    check-cast v4, LX/E1i;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;-><init>(LX/1Ad;LX/E1i;)V

    .line 2075699
    move-object v0, p0

    .line 2075700
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2075701
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoOneAspectRatioUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2075702
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2075703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()F
    .locals 1

    .prologue
    .line 2075704
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method
