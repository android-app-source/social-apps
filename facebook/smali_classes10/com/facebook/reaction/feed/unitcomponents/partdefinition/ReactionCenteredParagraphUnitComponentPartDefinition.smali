.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/E4q;

.field private final e:LX/E4u;

.field private final f:LX/2d1;

.field private final g:LX/E5T;

.field private final h:LX/1Uf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E4q;LX/E4u;LX/2d1;LX/E5T;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2074014
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2074015
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->d:LX/E4q;

    .line 2074016
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->e:LX/E4u;

    .line 2074017
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->f:LX/2d1;

    .line 2074018
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->g:LX/E5T;

    .line 2074019
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->h:LX/1Uf;

    .line 2074020
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2073990
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 2073991
    move-object v0, p3

    .line 2073992
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2Z;

    .line 2073993
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2073994
    invoke-direct {v1, v3}, LX/E2Z;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2a;

    .line 2073995
    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v3, :cond_0

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2073996
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->h:LX/1Uf;

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v4

    invoke-static {v4}, LX/9tr;->a(LX/5sZ;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2073997
    :goto_0
    iget-boolean v3, v0, LX/E2a;->a:Z

    move v3, v3

    .line 2073998
    if-eqz v3, :cond_1

    .line 2073999
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->d:LX/E4q;

    invoke-virtual {v3, p1}, LX/E4q;->c(LX/1De;)LX/E4o;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/E4o;->a(Landroid/text/SpannableStringBuilder;)LX/E4o;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/E4o;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4o;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2074000
    :goto_1
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->e:LX/E4u;

    invoke-virtual {v2, p1}, LX/E4u;->c(LX/1De;)LX/E4s;

    move-result-object v2

    .line 2074001
    iget-object v3, v2, LX/E4s;->a:LX/E4t;

    iput-object v1, v3, LX/E4t;->a:LX/1X1;

    .line 2074002
    iget-object v3, v2, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2074003
    move-object v1, v2

    .line 2074004
    invoke-virtual {v1, p2}, LX/E4s;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E4s;

    move-result-object v2

    move-object v1, p3

    check-cast v1, LX/3U9;

    invoke-interface {v1}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/E4s;->a(LX/2ja;)LX/E4s;

    move-result-object v1

    check-cast p3, LX/1Pq;

    invoke-virtual {v1, p3}, LX/E4s;->a(LX/1Pq;)LX/E4s;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/E4s;->a(LX/E2a;)LX/E4s;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2074005
    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2074006
    :cond_1
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->g:LX/E5T;

    invoke-virtual {v3, p1}, LX/E5T;->c(LX/1De;)LX/E5R;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/E5R;->a(Landroid/text/SpannableStringBuilder;)LX/E5R;

    move-result-object v1

    .line 2074007
    iget-boolean v3, v0, LX/E2a;->b:Z

    move v3, v3

    .line 2074008
    iget-object v4, v1, LX/E5R;->a:LX/E5S;

    iput-boolean v3, v4, LX/E5S;->b:Z

    .line 2074009
    move-object v1, v1

    .line 2074010
    invoke-interface {v2}, LX/9uc;->de()LX/174;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/E5R;->a(LX/174;)LX/E5R;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    .line 2074011
    iget-object v3, v1, LX/E5R;->a:LX/E5S;

    iput-object v2, v3, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2074012
    move-object v1, v1

    .line 2074013
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;
    .locals 10

    .prologue
    .line 2073970
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;

    monitor-enter v1

    .line 2073971
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073972
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073973
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073974
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073975
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/E4q;->a(LX/0QB;)LX/E4q;

    move-result-object v5

    check-cast v5, LX/E4q;

    invoke-static {v0}, LX/E4u;->a(LX/0QB;)LX/E4u;

    move-result-object v6

    check-cast v6, LX/E4u;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v7

    check-cast v7, LX/2d1;

    invoke-static {v0}, LX/E5T;->a(LX/0QB;)LX/E5T;

    move-result-object v8

    check-cast v8, LX/E5T;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v9

    check-cast v9, LX/1Uf;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/E4q;LX/E4u;LX/2d1;LX/E5T;LX/1Uf;)V

    .line 2073976
    move-object v0, v3

    .line 2073977
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073978
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073979
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073980
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 2073989
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->f:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2073988
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2073987
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2073986
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 2073985
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCenteredParagraphUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2073982
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2073983
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2073984
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2073981
    const/4 v0, 0x0

    return-object v0
.end method
