.class public Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/E2P;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

.field private final m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072750
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2072751
    iput-object p12, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2072752
    iput-object p11, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2072753
    iput-object p10, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 2072754
    iput-object p5, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2072755
    iput-object p4, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2072756
    iput-object p3, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    .line 2072757
    iput-object p2, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    .line 2072758
    iput-object p1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2072759
    iput-object p6, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    .line 2072760
    iput-object p7, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    .line 2072761
    iput-object p8, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    .line 2072762
    iput-object p9, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    .line 2072763
    iput-object p13, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 2072764
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;
    .locals 3

    .prologue
    .line 2072720
    const-class v1, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;

    monitor-enter v1

    .line 2072721
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2072722
    sput-object v2, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2072723
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2072724
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2072725
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072726
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2072727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;
    .locals 14

    .prologue
    .line 2072728
    new-instance v0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;)V

    .line 2072729
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2072730
    check-cast p2, LX/E2P;

    .line 2072731
    invoke-virtual {p2}, LX/E2O;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2072732
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2072733
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072734
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072735
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072736
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072737
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072738
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072739
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072740
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072741
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072742
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072743
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072744
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072745
    iget-object v1, p0, Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072746
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2072747
    check-cast p1, LX/E2P;

    .line 2072748
    iget-boolean v0, p1, LX/E2P;->a:Z

    move v0, v0

    .line 2072749
    return v0
.end method
