.class public Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071459
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2071460
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;
    .locals 3

    .prologue
    .line 2071461
    const-class v1, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    monitor-enter v1

    .line 2071462
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071463
    sput-object v2, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071464
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071465
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2071466
    new-instance v0, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;-><init>()V

    .line 2071467
    move-object v0, v0

    .line 2071468
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071469
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071470
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5f185742

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2071472
    check-cast p1, Ljava/lang/String;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2071473
    const v1, 0x7f0a010a

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2071474
    invoke-virtual {p4, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2071475
    const/16 v1, 0x1f

    const v2, -0x61e002e4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
