.class public Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071441
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2071442
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;
    .locals 3

    .prologue
    .line 2071448
    const-class v1, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    monitor-enter v1

    .line 2071449
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071450
    sput-object v2, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071451
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071452
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2071453
    new-instance v0, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;-><init>()V

    .line 2071454
    move-object v0, v0

    .line 2071455
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071456
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071457
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071458
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2071446
    check-cast p2, Ljava/lang/String;

    .line 2071447
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Void;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7cd7063b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2071443
    check-cast p1, Ljava/lang/String;

    check-cast p4, Landroid/widget/TextView;

    .line 2071444
    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2071445
    const/16 v1, 0x1f

    const v2, 0x6af5407f    # 1.482457E26f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
