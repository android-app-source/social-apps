.class public Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "LX/6UY;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/facepile/FacepileView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071499
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2071500
    return-void
.end method

.method public static a(ILX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "LX/5sX;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2071492
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2071493
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2071494
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 2071495
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sX;

    .line 2071496
    new-instance v4, LX/6UY;

    invoke-interface {v0}, LX/5sX;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v4, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2071497
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2071498
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;
    .locals 3

    .prologue
    .line 2071476
    const-class v1, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    monitor-enter v1

    .line 2071477
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071478
    sput-object v2, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071479
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071480
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2071481
    new-instance v0, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;-><init>()V

    .line 2071482
    move-object v0, v0

    .line 2071483
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071484
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071485
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2071490
    check-cast p2, LX/0Px;

    .line 2071491
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Void;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6dda0f49

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2071487
    check-cast p1, LX/0Px;

    check-cast p4, Lcom/facebook/fbui/facepile/FacepileView;

    .line 2071488
    invoke-virtual {p4, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2071489
    const/16 v1, 0x1f

    const v2, -0x3732fb35

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
