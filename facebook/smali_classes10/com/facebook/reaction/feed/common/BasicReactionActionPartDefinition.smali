.class public Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2kp;",
        ":",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E1o;",
        "LX/E1p;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/E1f;


# direct methods
.method public constructor <init>(LX/E1f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071416
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2071417
    iput-object p1, p0, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a:LX/E1f;

    .line 2071418
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;
    .locals 4

    .prologue
    .line 2071419
    const-class v1, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    monitor-enter v1

    .line 2071420
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071421
    sput-object v2, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071422
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071423
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071424
    new-instance p0, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;-><init>(LX/E1f;)V

    .line 2071425
    move-object v0, p0

    .line 2071426
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071427
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071428
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071429
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2071430
    check-cast p2, LX/E1o;

    check-cast p3, LX/2kp;

    .line 2071431
    new-instance v0, LX/E1n;

    invoke-direct {v0, p0, p2, p3}, LX/E1n;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;LX/E1o;LX/2kp;)V

    .line 2071432
    new-instance v1, LX/E1p;

    invoke-direct {v1, v0}, LX/E1p;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5cd6de79

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2071433
    check-cast p1, LX/E1o;

    check-cast p2, LX/E1p;

    .line 2071434
    iget-object v1, p2, LX/E1p;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2071435
    iget-object v1, p1, LX/E1o;->c:Landroid/view/View$OnTouchListener;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2071436
    const/16 v1, 0x1f

    const v2, 0x274223c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2071437
    const/4 v0, 0x0

    .line 2071438
    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2071439
    invoke-virtual {p4, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2071440
    return-void
.end method
