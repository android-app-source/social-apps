.class public Lcom/facebook/reaction/feed/ReactionRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/pages/ReactionPagesPinnedPostGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoBottomGapGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsNoTopNoBottomGapGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071311
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2071312
    iput-object p1, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->c:LX/0Ot;

    .line 2071313
    iput-object p2, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->e:LX/0Ot;

    .line 2071314
    iput-object p9, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->d:LX/0Ot;

    .line 2071315
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b:Ljava/util/Map;

    .line 2071316
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071317
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071318
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071319
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    .line 2071320
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071321
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071322
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071323
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-interface {v0, v1, p8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2071324
    return-void
.end method

.method public static a(Lcom/facebook/reaction/feed/ReactionRootPartDefinition;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 2

    .prologue
    .line 2071325
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    .line 2071326
    if-eqz v0, :cond_0

    .line 2071327
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2071328
    invoke-interface {v0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2071329
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/ReactionRootPartDefinition;
    .locals 13

    .prologue
    .line 2071330
    const-class v1, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;

    monitor-enter v1

    .line 2071331
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071332
    sput-object v2, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071333
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071334
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071335
    new-instance v3, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;

    const/16 v4, 0x6bd

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x30be

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x10ac

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x30c7

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1081

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3105

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3107

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3106

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x6fa

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2071336
    move-object v0, v3

    .line 2071337
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071338
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071339
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071340
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/Object;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 2

    .prologue
    .line 2071341
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 2071342
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    .line 2071343
    invoke-interface {v0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2071344
    :cond_0
    :goto_0
    return-object v0

    .line 2071345
    :cond_1
    instance-of v0, p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-eqz v0, :cond_2

    .line 2071346
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-static {p0, p1}, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a(Lcom/facebook/reaction/feed/ReactionRootPartDefinition;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    goto :goto_0

    .line 2071347
    :cond_2
    instance-of v0, p1, LX/E2P;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2071348
    check-cast v0, LX/E2P;

    .line 2071349
    iget-boolean v1, v0, LX/E2P;->a:Z

    move v0, v1

    .line 2071350
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2071351
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;

    .line 2071352
    invoke-virtual {v0, p1}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 2071353
    :cond_3
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2071354
    const/4 v2, 0x0

    .line 2071355
    instance-of v0, p2, Lcom/facebook/reaction/common/ReactionCardNode;

    if-eqz v0, :cond_1

    .line 2071356
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2071357
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGroupPartDefinition;->a(LX/Cfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071358
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->a(Lcom/facebook/reaction/feed/ReactionRootPartDefinition;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    move-object v0, v0

    .line 2071359
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2071360
    :goto_0
    return-object v2

    .line 2071361
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    .line 2071362
    if-eqz v0, :cond_2

    .line 2071363
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2071364
    invoke-interface {v0, p2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2071365
    :goto_1
    move-object v0, v0

    .line 2071366
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2071367
    :cond_1
    invoke-direct {p0, p2}, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->b(Ljava/lang/Object;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/feed/ReactionRootPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2071368
    const/4 v0, 0x1

    return v0
.end method
