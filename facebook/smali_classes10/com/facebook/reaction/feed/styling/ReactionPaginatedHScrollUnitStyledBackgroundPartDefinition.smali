.class public Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1X6;",
        "Ljava/lang/Integer;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:LX/1V8;

.field private final c:LX/1V2;

.field private final d:LX/E3H;

.field private final e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public constructor <init>(LX/14w;LX/1V8;LX/1V2;LX/E3H;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073808
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073809
    iput-object p1, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->a:LX/14w;

    .line 2073810
    iput-object p2, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->b:LX/1V8;

    .line 2073811
    iput-object p3, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->c:LX/1V2;

    .line 2073812
    iput-object p4, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->d:LX/E3H;

    .line 2073813
    iput-object p5, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2073814
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;
    .locals 9

    .prologue
    .line 2073815
    const-class v1, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    monitor-enter v1

    .line 2073816
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073817
    sput-object v2, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073818
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073819
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073820
    new-instance v3, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V8;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v6

    check-cast v6, LX/1V2;

    invoke-static {v0}, LX/E3H;->a(LX/0QB;)LX/E3H;

    move-result-object v7

    check-cast v7, LX/E3H;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;-><init>(LX/14w;LX/1V8;LX/1V2;LX/E3H;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 2073821
    move-object v0, v3

    .line 2073822
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073823
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073824
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2073826
    check-cast p2, LX/1X6;

    check-cast p3, LX/1Pn;

    .line 2073827
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 2073828
    iget-object v0, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->a:LX/14w;

    invoke-static {v0, v1}, LX/1X7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)I

    move-result v0

    .line 2073829
    iget-object v1, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/1X6;->e:LX/1X9;

    iget-object v3, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->c:LX/1V2;

    move-object v4, p3

    check-cast v4, LX/1Ps;

    invoke-interface {v4}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/1Ps;

    invoke-interface {v5}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    move-object v6, p3

    check-cast v6, LX/1Ps;

    invoke-interface {v6}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    move-object v7, p3

    check-cast v7, LX/1Ps;

    invoke-interface {v7}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    move-object v8, p3

    check-cast v8, LX/1Ps;

    invoke-interface {v8}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v1

    .line 2073830
    iget-object v3, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->b:LX/1V8;

    iget-object v4, p2, LX/1X6;->b:LX/1Ua;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    move v2, v0

    move-object v6, v9

    invoke-static/range {v1 .. v6}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 2073831
    iget-object v0, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    iget v3, v9, Landroid/graphics/Rect;->left:I

    iget v4, v9, Landroid/graphics/Rect;->top:I

    iget v5, v9, Landroid/graphics/Rect;->right:I

    iget v6, v9, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v3, v4, v5, v6}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2073832
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2073833
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 2073834
    iget-object v3, p0, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->d:LX/E3H;

    .line 2073835
    if-nez v1, :cond_0

    .line 2073836
    const v4, 0x7f01073c

    .line 2073837
    :goto_0
    move v1, v4

    .line 2073838
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2073839
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 2073840
    :cond_0
    sget-object v4, LX/E3G;->a:[I

    invoke-virtual {v1}, LX/1X9;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2073841
    iget-object v4, v3, LX/E3H;->a:LX/03V;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unsupported reaction feedunit position: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/1X9;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " passed."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2073842
    const v4, 0x7f01073c

    goto :goto_0

    .line 2073843
    :pswitch_0
    const v4, 0x7f010738

    goto :goto_0

    .line 2073844
    :pswitch_1
    const v4, 0x7f010739

    goto :goto_0

    .line 2073845
    :pswitch_2
    const v4, 0x7f01073b

    goto :goto_0

    .line 2073846
    :pswitch_3
    const v4, 0x7f01073a

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4f552c97

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073847
    check-cast p2, Ljava/lang/Integer;

    .line 2073848
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2073849
    const/16 v1, 0x1f

    const v2, -0x6f0de0eb    # -9.550006E-29f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
