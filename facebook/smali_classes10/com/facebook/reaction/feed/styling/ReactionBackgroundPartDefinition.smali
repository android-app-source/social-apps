.class public Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E3A;",
        "Landroid/graphics/drawable/Drawable;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/E3B;

.field private final b:LX/E3F;

.field private final c:LX/1V2;

.field private final d:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public constructor <init>(LX/E3B;LX/E3F;LX/1V2;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073658
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073659
    iput-object p3, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->c:LX/1V2;

    .line 2073660
    iput-object p1, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->a:LX/E3B;

    .line 2073661
    iput-object p2, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->b:LX/E3F;

    .line 2073662
    iput-object p4, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->d:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2073663
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;
    .locals 7

    .prologue
    .line 2073664
    const-class v1, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    monitor-enter v1

    .line 2073665
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073666
    sput-object v2, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073667
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073668
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073669
    new-instance p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    invoke-static {v0}, LX/E3B;->a(LX/0QB;)LX/E3B;

    move-result-object v3

    check-cast v3, LX/E3B;

    invoke-static {v0}, LX/E3F;->a(LX/0QB;)LX/E3F;

    move-result-object v4

    check-cast v4, LX/E3F;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v5

    check-cast v5, LX/1V2;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;-><init>(LX/E3B;LX/E3F;LX/1V2;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 2073670
    move-object v0, p0

    .line 2073671
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073672
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073673
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073674
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2073675
    check-cast p2, LX/E3A;

    check-cast p3, LX/1Pn;

    .line 2073676
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 2073677
    const/4 v0, 0x0

    iget-object v1, p2, LX/E3A;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->c:LX/1V2;

    move-object v4, p3

    check-cast v4, LX/1Ps;

    invoke-interface {v4}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/1Ps;

    invoke-interface {v5}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    move-object v6, p3

    check-cast v6, LX/1Ps;

    invoke-interface {v6}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    move-object v7, p3

    check-cast v7, LX/1Ps;

    invoke-interface {v7}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    move-object v8, p3

    check-cast v8, LX/1Ps;

    invoke-interface {v8}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v0

    .line 2073678
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->b:LX/E3F;

    iget-object v3, p2, LX/E3A;->b:LX/1Ua;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object v5, v10

    invoke-static/range {v0 .. v5}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 2073679
    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->a:LX/E3B;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v7, p2, LX/E3A;->b:LX/1Ua;

    iget-object v8, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->b:LX/E3F;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-object v6, v10

    invoke-static/range {v0 .. v9}, LX/1X7;->a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2073680
    iget-object v1, p0, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->d:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    iget v3, v10, Landroid/graphics/Rect;->left:I

    iget v4, v10, Landroid/graphics/Rect;->top:I

    iget v5, v10, Landroid/graphics/Rect;->right:I

    iget v6, v10, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v3, v4, v5, v6}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2073681
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xc8f2c6d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073682
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 2073683
    invoke-virtual {p4, p2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2073684
    const/16 v1, 0x1f

    const v2, 0x4217b5ce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
