.class public Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final h:Lcom/facebook/common/callercontext/CallerContext;

.field public static final i:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public a:LX/0y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1sS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public k:Lcom/facebook/location/ImmutableLocation;

.field public l:Lcom/facebook/location/ImmutableLocation;

.field public m:Lcom/facebook/maps/FbStaticMapView;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2073509
    const-class v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 2073510
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x1d4c0

    .line 2073511
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2073512
    move-object v0, v0

    .line 2073513
    const/high16 v1, 0x43fa0000    # 500.0f

    .line 2073514
    iput v1, v0, LX/1S7;->c:F

    .line 2073515
    move-object v0, v0

    .line 2073516
    const-wide/16 v2, 0x1388

    .line 2073517
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2073518
    move-object v0, v0

    .line 2073519
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->i:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2073520
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2073521
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "pages_single_location_map"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->j:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2073522
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;

    invoke-static {p1}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-static {p1}, LX/1sS;->b(LX/0QB;)LX/1sS;

    move-result-object v4

    check-cast v4, LX/1sS;

    invoke-static {p1}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v5

    check-cast v5, LX/0y3;

    invoke-static {p1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {p1}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v7

    check-cast v7, LX/6aG;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    const/16 v0, 0x1483

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->a:LX/0y2;

    iput-object v4, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->b:LX/1sS;

    iput-object v5, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->c:LX/0y3;

    iput-object v6, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->d:LX/0wM;

    iput-object v7, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->e:LX/6aG;

    iput-object v8, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->f:LX/1Ck;

    iput-object p1, v2, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->g:LX/0Or;

    .line 2073523
    const v0, 0x7f03116c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2073524
    const v0, 0x7f0d2916

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    .line 2073525
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->d:LX/0wM;

    const v2, 0x7f020837

    const v3, -0xc4a668

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2073526
    const v0, 0x7f0d2912

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->m:Lcom/facebook/maps/FbStaticMapView;

    .line 2073527
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2073528
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2073529
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->m:Lcom/facebook/maps/FbStaticMapView;

    sget-object v2, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/maps/FbStaticMapView;->a(LX/0yY;LX/0gc;LX/6Zs;)V

    .line 2073530
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->m:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0213b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2073531
    const v0, 0x7f0d2915

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2073532
    invoke-static {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->h(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    .line 2073533
    return-void
.end method

.method public static g(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V
    .locals 3

    .prologue
    .line 2073534
    invoke-direct {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->getSubtitleString()Ljava/lang/String;

    move-result-object v1

    .line 2073535
    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2073536
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2073537
    return-void

    .line 2073538
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSubtitleString()Ljava/lang/String;
    .locals 13

    .prologue
    .line 2073539
    invoke-static {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->h(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    .line 2073540
    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->k:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->l:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_3

    .line 2073541
    iget-object v7, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->e:LX/6aG;

    iget-object v8, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->k:Lcom/facebook/location/ImmutableLocation;

    iget-object v9, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->l:Lcom/facebook/location/ImmutableLocation;

    const-wide v10, 0x40f82b8000000000L    # 99000.0

    iget-object v12, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->o:Ljava/lang/String;

    invoke-virtual/range {v7 .. v12}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2073542
    :goto_0
    move-object v0, v6

    .line 2073543
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->p:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2073544
    const/4 v0, 0x0

    .line 2073545
    :cond_0
    :goto_1
    return-object v0

    .line 2073546
    :cond_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2073547
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->p:Ljava/lang/String;

    goto :goto_1

    .line 2073548
    :cond_2
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->p:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2073549
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082235

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->p:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public static h(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V
    .locals 4

    .prologue
    .line 2073550
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->k:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    .line 2073551
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->a:LX/0y2;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v2, v3}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->k:Lcom/facebook/location/ImmutableLocation;

    .line 2073552
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->k:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    .line 2073553
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->c:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2073554
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->b:LX/1sS;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2073555
    :cond_0
    :goto_1
    return-void

    .line 2073556
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->f:LX/1Ck;

    const-string v1, "page_map_with_navigation_get_location_task_key"

    new-instance v2, LX/E30;

    invoke-direct {v2, p0}, LX/E30;-><init>(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    new-instance v3, LX/E31;

    invoke-direct {v3, p0}, LX/E31;-><init>(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
