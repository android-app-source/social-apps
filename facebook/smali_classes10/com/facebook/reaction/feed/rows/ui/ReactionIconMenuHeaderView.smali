.class public Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;
.super LX/E2y;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/widget/ImageView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2073487
    const-class v0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2073488
    invoke-direct {p0, p1}, LX/E2y;-><init>(Landroid/content/Context;)V

    .line 2073489
    const v0, 0x7f0d28a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2073490
    const v0, 0x7f0d28aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->b:Landroid/widget/ImageView;

    .line 2073491
    return-void
.end method


# virtual methods
.method public getContentViewId()I
    .locals 1

    .prologue
    .line 2073492
    const v0, 0x7f031128

    return v0
.end method

.method public setIconUri(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2073493
    if-eqz p1, :cond_0

    .line 2073494
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2073495
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2073496
    return-void

    .line 2073497
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
