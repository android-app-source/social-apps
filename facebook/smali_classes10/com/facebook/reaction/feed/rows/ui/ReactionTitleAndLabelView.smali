.class public Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2073629
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2073630
    invoke-direct {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a()V

    .line 2073631
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2073626
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2073627
    invoke-direct {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a()V

    .line 2073628
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2073619
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2073620
    invoke-direct {p0}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a()V

    .line 2073621
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2073622
    const v0, 0x7f031180

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2073623
    const v0, 0x7f0d292e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2073624
    const v0, 0x7f0d292f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2073625
    return-void
.end method


# virtual methods
.method public setLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2073632
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2073633
    return-void
.end method

.method public setLabelTextAppearance(I)V
    .locals 2

    .prologue
    .line 2073613
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2073614
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2073615
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2073616
    return-void
.end method

.method public setTitleTextAppearance(I)V
    .locals 2

    .prologue
    .line 2073617
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2073618
    return-void
.end method
