.class public Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

.field private final e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072897
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2072898
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    .line 2072899
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    .line 2072900
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    .line 2072901
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->d:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    .line 2072902
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    .line 2072903
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;
    .locals 9

    .prologue
    .line 2072904
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    monitor-enter v1

    .line 2072905
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2072906
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2072907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2072908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2072909
    new-instance v3, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;)V

    .line 2072910
    move-object v0, v3

    .line 2072911
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2072912
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072913
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2072914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2072915
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/1Pn;

    const/4 v3, 0x0

    .line 2072916
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072917
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->d:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072918
    check-cast p3, LX/1Pr;

    new-instance v0, LX/E2U;

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E2U;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2V;

    .line 2072919
    iget-boolean v1, v0, LX/E2V;->b:Z

    move v1, v1

    .line 2072920
    if-nez v1, :cond_0

    .line 2072921
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->COLLAPSED:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, LX/E2V;->a(Z)V

    .line 2072922
    :cond_0
    iget-boolean v1, v0, LX/E2V;->a:Z

    move v0, v1

    .line 2072923
    if-nez v0, :cond_1

    .line 2072924
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072925
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072926
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2072927
    return-object v3

    .line 2072928
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2072929
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2072930
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2072931
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
