.class public Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E2s;",
        "LX/E2t;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

.field public final b:LX/E36;

.field private final c:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;LX/E36;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073291
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073292
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    .line 2073293
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->b:LX/E36;

    .line 2073294
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    .line 2073295
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;
    .locals 6

    .prologue
    .line 2073296
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    monitor-enter v1

    .line 2073297
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073298
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073299
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073300
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073301
    new-instance p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    const-class v4, LX/E36;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/E36;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;LX/E36;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V

    .line 2073302
    move-object v0, p0

    .line 2073303
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073304
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073305
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2073307
    check-cast p2, LX/E2s;

    check-cast p3, LX/2km;

    const/4 v1, 0x0

    .line 2073308
    iget-object v2, p2, LX/E2s;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 2073309
    iget-object v3, p2, LX/E2s;->b:Ljava/lang/String;

    .line 2073310
    iget-object v4, p2, LX/E2s;->c:Ljava/lang/String;

    .line 2073311
    const v0, 0x7f0d28a2

    iget-object v5, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v6, LX/E2v;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v7

    invoke-direct {v6, v7, v3, v4}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2073312
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2073313
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v0

    .line 2073314
    :goto_0
    const v5, 0x7f0d28a7

    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v7, LX/E2v;

    invoke-direct {v7, v0, v3, v4}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v5, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2073315
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->d()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2073316
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    .line 2073317
    :goto_1
    const v2, 0x7f0d28a9

    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->a:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2073318
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2073319
    iget-object v0, p2, LX/E2s;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gY_()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2073320
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2073321
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v6

    invoke-interface {v6}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    const/4 v6, 0x1

    :goto_3
    move v6, v6

    .line 2073322
    if-eqz v6, :cond_0

    .line 2073323
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2073324
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2073325
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2073326
    :goto_4
    new-instance v0, LX/E2t;

    invoke-direct {v0, v1}, LX/E2t;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0

    .line 2073327
    :cond_2
    new-instance v1, LX/E2r;

    invoke-direct {v1, p0, p3, v3, p2}, LX/E2r;-><init>(Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;LX/2km;Ljava/util/List;LX/E2s;)V

    goto :goto_4

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2a103538

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073328
    check-cast p2, LX/E2t;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;

    .line 2073329
    iget-object v1, p2, LX/E2t;->a:Landroid/view/View$OnClickListener;

    .line 2073330
    iget-object p2, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->b:Landroid/widget/ImageView;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2073331
    const/16 v1, 0x1f

    const v2, 0x21cf1072

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
