.class public Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/7oX;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073351
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073352
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;
    .locals 3

    .prologue
    .line 2073356
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    monitor-enter v1

    .line 2073357
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073358
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2073361
    new-instance v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;-><init>()V

    .line 2073362
    move-object v0, v0

    .line 2073363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x17e5db82

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073353
    check-cast p1, LX/7oX;

    check-cast p4, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    .line 2073354
    invoke-interface {p1}, LX/7oX;->c()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {p1}, LX/7oX;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v5

    const-string v6, "reaction_events"

    invoke-virtual {p4, v4, v5, v6}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/net/Uri;Ljava/util/Date;Ljava/lang/String;)V

    .line 2073355
    const/16 v1, 0x1f

    const v2, 0x52430053

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
