.class public Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "LX/2km;",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073390
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073391
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;
    .locals 3

    .prologue
    .line 2073392
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    monitor-enter v1

    .line 2073393
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073394
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073395
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073396
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2073397
    new-instance v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;-><init>()V

    .line 2073398
    move-object v0, v0

    .line 2073399
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073400
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073401
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073402
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x26ae0430

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073403
    check-cast p1, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2073404
    if-eqz p1, :cond_0

    .line 2073405
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextColor(I)V

    .line 2073406
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x406a3a32    # 3.659802f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
