.class public Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "LX/3U9;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/E8M;


# direct methods
.method public constructor <init>(LX/E8M;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073367
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073368
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->a:LX/E8M;

    .line 2073369
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;
    .locals 4

    .prologue
    .line 2073370
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    monitor-enter v1

    .line 2073371
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073372
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073373
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073374
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073375
    new-instance p0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    invoke-static {v0}, LX/E8M;->b(LX/0QB;)LX/E8M;

    move-result-object v3

    check-cast v3, LX/E8M;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;-><init>(LX/E8M;)V

    .line 2073376
    move-object v0, p0

    .line 2073377
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073378
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073379
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x686f16d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073381
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/3U9;

    check-cast p4, Landroid/widget/TextView;

    .line 2073382
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->a:LX/E8M;

    invoke-interface {p3}, LX/3U9;->n()LX/2ja;

    move-result-object v2

    invoke-virtual {v1, p1, p4, v2}, LX/E8M;->a(Lcom/facebook/reaction/common/ReactionCardNode;Landroid/widget/TextView;LX/2ja;)V

    .line 2073383
    const/16 v1, 0x1f

    const v2, -0x6da1a729

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2073384
    check-cast p4, Landroid/widget/TextView;

    .line 2073385
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->a:LX/E8M;

    const/4 p2, 0x0

    .line 2073386
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2073387
    iget-object p0, v0, LX/E8M;->h:LX/CXj;

    iget-object p1, v0, LX/E8M;->c:LX/CXm;

    invoke-virtual {p0, p1}, LX/0b4;->b(LX/0b2;)Z

    .line 2073388
    iput-object p2, v0, LX/E8M;->c:LX/CXm;

    .line 2073389
    return-void
.end method
