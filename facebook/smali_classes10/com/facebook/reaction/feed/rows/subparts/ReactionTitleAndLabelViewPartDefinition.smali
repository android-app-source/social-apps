.class public Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E2x;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073451
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073452
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;
    .locals 3

    .prologue
    .line 2073453
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    monitor-enter v1

    .line 2073454
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073455
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073456
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073457
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2073458
    new-instance v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;-><init>()V

    .line 2073459
    move-object v0, v0

    .line 2073460
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073461
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073462
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073463
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x11688763

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073464
    check-cast p1, LX/E2x;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;

    .line 2073465
    iget-boolean v1, p1, LX/E2x;->e:Z

    if-eqz v1, :cond_0

    .line 2073466
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setVisibility(I)V

    .line 2073467
    iget-object v1, p1, LX/E2x;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitle(Ljava/lang/String;)V

    .line 2073468
    iget-object v1, p1, LX/E2x;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabel(Ljava/lang/String;)V

    .line 2073469
    iget v1, p1, LX/E2x;->c:I

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitleTextAppearance(I)V

    .line 2073470
    iget v1, p1, LX/E2x;->d:I

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabelTextAppearance(I)V

    .line 2073471
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x7d064a95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2073472
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setVisibility(I)V

    goto :goto_0
.end method
