.class public Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3Ab;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073332
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073333
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;
    .locals 3

    .prologue
    .line 2073334
    const-class v1, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    monitor-enter v1

    .line 2073335
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073336
    sput-object v2, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073337
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073338
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2073339
    new-instance v0, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;-><init>()V

    .line 2073340
    move-object v0, v0

    .line 2073341
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073342
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073343
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x191ce821

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073345
    check-cast p1, LX/3Ab;

    check-cast p4, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2073346
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2073347
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2073348
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Ljava/lang/String;LX/0Px;)V

    .line 2073349
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6bc086af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2073350
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0
.end method
