.class public Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

.field public final f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;LX/2d1;Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073052
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2073053
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    .line 2073054
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    .line 2073055
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    .line 2073056
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->d:Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;

    .line 2073057
    iput-object p6, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    .line 2073058
    invoke-virtual {p3}, LX/2d1;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->f:Z

    .line 2073059
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;
    .locals 10

    .prologue
    .line 2073060
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    monitor-enter v1

    .line 2073061
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073062
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073063
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073064
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073065
    new-instance v3, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v6

    check-cast v6, LX/2d1;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;LX/2d1;Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;)V

    .line 2073066
    move-object v0, v3

    .line 2073067
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073068
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073069
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2073071
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    const/4 v2, 0x0

    .line 2073072
    iget-boolean v0, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->f:Z

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2073073
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2073074
    :goto_1
    if-eqz v0, :cond_0

    .line 2073075
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->e:Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073076
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->d:Lcom/facebook/reaction/feed/rows/ReactionStoryHeaderGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073077
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073078
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->c:Lcom/facebook/reaction/feed/rows/ReactionStoryFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073079
    if-eqz v0, :cond_1

    .line 2073080
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073081
    :cond_1
    return-object v2

    .line 2073082
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2073083
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2073084
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2073085
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
