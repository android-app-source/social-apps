.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073195
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2073196
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;

    .line 2073197
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    .line 2073198
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2073184
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 2073185
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073186
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073187
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073188
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073189
    new-instance p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;)V

    .line 2073190
    move-object v0, p0

    .line 2073191
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073192
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073193
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2073180
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 2073181
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2073182
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2073183
    const/4 v0, 0x1

    return v0
.end method
