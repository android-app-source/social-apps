.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/E2o;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073117
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073118
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;
    .locals 3

    .prologue
    .line 2073124
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    monitor-enter v1

    .line 2073125
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073126
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073127
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073128
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2073129
    new-instance v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;-><init>()V

    .line 2073130
    move-object v0, v0

    .line 2073131
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073132
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073133
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073134
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2073135
    check-cast p2, LX/E2o;

    check-cast p3, LX/2km;

    .line 2073136
    new-instance v0, LX/E2n;

    invoke-direct {v0, p0, p3, p2}, LX/E2n;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;LX/2km;LX/E2o;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x10887d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2073121
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 2073122
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2073123
    const/16 v1, 0x1f

    const v2, 0x9b2d08f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2073119
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2073120
    return-void
.end method
