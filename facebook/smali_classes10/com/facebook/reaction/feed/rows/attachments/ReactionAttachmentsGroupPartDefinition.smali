.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

.field private final b:LX/1vj;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;LX/1vj;LX/2d1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073140
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2073141
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    .line 2073142
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->b:LX/1vj;

    .line 2073143
    iget-object v0, p3, LX/2d1;->a:LX/0Uh;

    const/16 p1, 0x5dc

    const/4 p2, 0x0

    invoke-virtual {v0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2073144
    iput-boolean v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->c:Z

    .line 2073145
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;
    .locals 6

    .prologue
    .line 2073146
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    monitor-enter v1

    .line 2073147
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073148
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073151
    new-instance p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    invoke-static {v0}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v4

    check-cast v4, LX/1vj;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;LX/1vj;LX/2d1;)V

    .line 2073152
    move-object v0, p0

    .line 2073153
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073154
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073155
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2073157
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2073158
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2073159
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->b:LX/1vj;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v2

    .line 2073160
    iget-object p3, v1, LX/1vj;->a:LX/1vl;

    invoke-interface {p3, v2}, LX/1vl;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfm;

    move-result-object p3

    .line 2073161
    if-nez p3, :cond_2

    const/4 p3, 0x0

    :goto_0
    move-object v1, p3

    .line 2073162
    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->b:LX/1vj;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v0

    .line 2073163
    iget-object p3, v2, LX/1vj;->a:LX/1vl;

    invoke-interface {p3, v0}, LX/1vl;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfm;

    move-result-object p3

    .line 2073164
    if-nez p3, :cond_3

    const/4 p3, 0x0

    :goto_1
    move-object v0, p3

    .line 2073165
    iget-boolean v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->c:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_4

    if-nez v1, :cond_4

    .line 2073166
    :cond_0
    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsGroupPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    invoke-virtual {p1, v2, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073167
    :cond_1
    :goto_2
    const/4 v0, 0x0

    return-object v0

    :cond_2
    invoke-virtual {p3}, LX/Cfm;->a()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object p3

    goto :goto_0

    .line 2073168
    :cond_3
    const/4 p3, 0x0

    move-object p3, p3

    .line 2073169
    goto :goto_1

    .line 2073170
    :cond_4
    if-eqz v0, :cond_5

    .line 2073171
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    .line 2073172
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v2

    .line 2073173
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2073174
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 2073175
    new-instance v6, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object p3

    invoke-direct {v6, v2, v7, p3}, Lcom/facebook/reaction/common/ReactionAttachmentNode;-><init>(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073176
    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2073177
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2073178
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2073179
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
