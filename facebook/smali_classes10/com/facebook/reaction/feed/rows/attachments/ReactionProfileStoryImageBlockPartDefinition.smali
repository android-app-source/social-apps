.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

.field private final b:LX/11R;

.field private final c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final d:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

.field public final e:LX/E1i;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;LX/11R;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;LX/E1i;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073199
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2073200
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    .line 2073201
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->b:LX/11R;

    .line 2073202
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 2073203
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->d:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    .line 2073204
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->e:LX/E1i;

    .line 2073205
    iput-object p6, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2073206
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;
    .locals 10

    .prologue
    .line 2073207
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    monitor-enter v1

    .line 2073208
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073209
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073212
    new-instance v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v5

    check-cast v5, LX/11R;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v8

    check-cast v8, LX/E1i;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;LX/11R;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;LX/E1i;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2073213
    move-object v0, v3

    .line 2073214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2073218
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    check-cast p3, LX/2km;

    const/4 v10, 0x0

    .line 2073219
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 2073220
    const v1, 0x7f0d2883

    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v9, 0x21

    const/4 v8, 0x0

    .line 2073221
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v5

    .line 2073222
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 2073223
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2073224
    const/4 v4, 0x0

    .line 2073225
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2073226
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2073227
    :cond_0
    :goto_0
    if-eqz v4, :cond_3

    .line 2073228
    const-string v5, " - "

    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2073229
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 2073230
    invoke-virtual {v6, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2073231
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v7, 0x7f0e08fd

    invoke-direct {v4, v3, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6, v4, v8, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2073232
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v7, 0x7f0e08fe

    invoke-direct {v4, v3, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {v6, v4, v5, v7, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2073233
    :goto_1
    move-object v3, v6

    .line 2073234
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2073235
    const v1, 0x7f0d2884

    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->b:LX/11R;

    sget-object v4, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {v3, v4, v6, v7}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2073236
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->a:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    new-instance v2, LX/E2o;

    .line 2073237
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v3

    .line 2073238
    iget-object v4, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->e:LX/E1i;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v7, LX/Cfc;->STORY_TAP:LX/Cfc;

    invoke-virtual {v4, v5, v6, v3, v7}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v3

    move-object v3, v3

    .line 2073239
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->b:Ljava/lang/String;

    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, LX/E2o;-><init>(LX/Cfl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2073240
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->c:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v1, v10}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2073241
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 2073242
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2073243
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->d:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2073244
    :cond_1
    return-object v10

    .line 2073245
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v7

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2073246
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2073247
    :cond_3
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0e08fd

    invoke-direct {v4, v3, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v6, v4, v8, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_1
.end method
