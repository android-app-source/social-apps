.class public Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072971
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2072972
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;

    .line 2072973
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    .line 2072974
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2072975
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;

    monitor-enter v1

    .line 2072976
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2072977
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2072978
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2072979
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2072980
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;)V

    .line 2072981
    move-object v0, p0

    .line 2072982
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2072983
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072984
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2072985
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2072986
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2072987
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2072988
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2072989
    const/4 v0, 0x1

    return v0
.end method
