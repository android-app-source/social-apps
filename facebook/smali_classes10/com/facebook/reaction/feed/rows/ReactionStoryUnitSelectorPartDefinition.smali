.class public Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073105
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2073106
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    .line 2073107
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    .line 2073108
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2073089
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;

    monitor-enter v1

    .line 2073090
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073091
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073092
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073093
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073094
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;)V

    .line 2073095
    move-object v0, p0

    .line 2073096
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073097
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073098
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073099
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2073100
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2073101
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->NOT_COLLAPSIBLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    if-ne v0, v1, :cond_0

    .line 2073102
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->b:Lcom/facebook/reaction/feed/rows/ReactionStoryUnitGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2073103
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2073104
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionStoryUnitSelectorPartDefinition;->a:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryUnitGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2073086
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2073087
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2073088
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
