.class public Lcom/facebook/localcontent/photos/PhotoCategoryFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/0Or;
    .annotation runtime Lcom/facebook/localcontent/constants/IsPhotosByCategoryUploadEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Dcq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Dcd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field private u:Z

.field public v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field public x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2018838
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    .line 2018839
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    const/16 v2, 0x32e

    invoke-static {v6, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    new-instance p1, LX/Dcq;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v6}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v6}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v5

    check-cast v5, LX/1EZ;

    invoke-static {v6}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object p0

    check-cast p0, LX/8LV;

    invoke-direct {p1, v3, v4, v5, p0}, LX/Dcq;-><init>(Landroid/content/res/Resources;LX/0kL;LX/1EZ;LX/8LV;)V

    move-object v3, p1

    check-cast v3, LX/Dcq;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/Dcd;->a(LX/0QB;)LX/Dcd;

    move-result-object v5

    check-cast v5, LX/Dcd;

    const/16 p0, 0x2ead

    invoke-static {v6, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    iput-object v2, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->n:LX/0Or;

    iput-object v3, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->o:LX/Dcq;

    iput-object v4, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->q:LX/Dcd;

    iput-object v6, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->r:LX/0Ot;

    return-void
.end method

.method public static k(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2018837
    iget-boolean v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2018822
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2018823
    const-class v0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    invoke-static {v0, p0}, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2018824
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018825
    const-string v1, "photo_category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->s:Ljava/lang/String;

    .line 2018826
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018827
    const-string v1, "local_content_entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018828
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018829
    const-string v1, "local_content_photo_upload_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->u:Z

    .line 2018830
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018831
    const-string v1, "local_content_upload_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->v:Ljava/lang/String;

    .line 2018832
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->v:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2018833
    const v0, 0x7f082900

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->v:Ljava/lang/String;

    .line 2018834
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018835
    const-string v1, "local_content_secondary_upload_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->w:Ljava/lang/String;

    .line 2018836
    return-void
.end method

.method public final a(Landroid/widget/ListView;)V
    .locals 3

    .prologue
    .line 2018797
    invoke-static {p0}, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->k(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2018798
    :goto_0
    return-void

    .line 2018799
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f54

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2018800
    const v1, 0x7f0d250f

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    iput-object v1, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    .line 2018801
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setVisibility(I)V

    .line 2018802
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setSecondaryUploadMessage(Ljava/lang/String;)V

    .line 2018803
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    new-instance v2, LX/Dcg;

    invoke-direct {v2, p0}, LX/Dcg;-><init>(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setUploadButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2018804
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 10
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018840
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018841
    iget-object v6, v5, LX/Dvb;->i:LX/Dvc;

    move-object v5, v6

    .line 2018842
    invoke-virtual {v5}, LX/Dvc;->d()LX/0Px;

    move-result-object v7

    sget-object v8, LX/74S;->PHOTOS_BY_CATEGORY:LX/74S;

    const/4 v9, 0x0

    move-object v5, p1

    move-object v6, p2

    const/4 p1, 0x0

    .line 2018843
    invoke-static {v2, v3, v4}, LX/9hF;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/9hE;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object p2

    if-eqz v6, :cond_0

    invoke-static {v6}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object p0

    :goto_0
    invoke-virtual {p2, p0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object p0

    .line 2018844
    iput-boolean v9, p0, LX/9hD;->m:Z

    .line 2018845
    move-object p0, p0

    .line 2018846
    invoke-virtual {p0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object p2

    .line 2018847
    iget-object p0, v0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/23R;

    invoke-interface {p0, v1, p2, p1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2018848
    return-void

    :cond_0
    move-object p0, p1

    .line 2018849
    goto :goto_0
.end method

.method public final c()LX/Dcc;
    .locals 1

    .prologue
    .line 2018821
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->q:LX/Dcd;

    return-object v0
.end method

.method public final d()LX/Dch;
    .locals 2

    .prologue
    .line 2018820
    new-instance v0, LX/Dci;

    invoke-direct {v0, p0}, LX/Dci;-><init>(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)V

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2018812
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2018813
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->o:LX/Dcq;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2018814
    const-string v1, "extra_media_items"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2018815
    if-nez v1, :cond_1

    .line 2018816
    :cond_0
    :goto_0
    return-void

    .line 2018817
    :cond_1
    iget-object v4, v0, LX/Dcq;->d:LX/8LV;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v2, v3, v5}, LX/8LV;->a(LX/0Px;JLjava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 2018818
    iget-object v4, v0, LX/Dcq;->b:LX/0kL;

    new-instance v5, LX/27k;

    iget-object p0, v0, LX/Dcq;->a:Landroid/content/res/Resources;

    const p1, 0x7f0828e1

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v5, p0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2018819
    iget-object v4, v0, LX/Dcq;->c:LX/1EZ;

    invoke-virtual {v4, v1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47fa5b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018809
    invoke-super {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onDestroyView()V

    .line 2018810
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/DxJ;->a(LX/Dce;)V

    .line 2018811
    const/16 v1, 0x2b

    const v2, -0x7ef49d57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018805
    invoke-super {p0, p1, p2}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2018806
    invoke-static {p0}, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->k(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    if-eqz v0, :cond_0

    .line 2018807
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    new-instance v1, LX/Dcf;

    invoke-direct {v1, p0}, LX/Dcf;-><init>(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)V

    invoke-virtual {v0, v1}, LX/DxJ;->a(LX/Dce;)V

    .line 2018808
    :cond_0
    return-void
.end method
