.class public Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;
.super Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:I

.field public c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2018933
    new-instance v0, LX/Dcm;

    invoke-direct {v0}, LX/Dcm;-><init>()V

    sput-object v0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2018915
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2018916
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    .line 2018917
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    .line 2018918
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    .line 2018919
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018920
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
        .end annotation
    .end param

    .prologue
    .line 2018934
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2018935
    iput-object p1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    .line 2018936
    iput p2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    .line 2018937
    iput-object p3, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    .line 2018938
    iput-object p4, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018939
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2018928
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2018929
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;

    if-nez v1, :cond_1

    .line 2018930
    :cond_0
    :goto_0
    return v0

    .line 2018931
    :cond_1
    check-cast p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;

    .line 2018932
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    iget v2, p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iget-object v2, p1, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2018927
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2018926
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2018921
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2018922
    iget v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2018923
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2018924
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2018925
    return-void
.end method
