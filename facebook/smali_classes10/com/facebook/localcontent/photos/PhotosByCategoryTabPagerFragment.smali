.class public Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Dck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DbN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/8KR;

.field private g:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public h:Ljava/lang/String;

.field private i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private j:Landroid/support/v4/view/ViewPager;

.field private k:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019038
    const-class v0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2019035
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2019036
    new-instance v0, LX/Dco;

    invoke-direct {v0, p0}, LX/Dco;-><init>(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->f:LX/8KR;

    .line 2019037
    return-void
.end method

.method public static d(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V
    .locals 2

    .prologue
    .line 2019032
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f0828d7

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 2019033
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2019034
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2019031
    const-string v0, "photos_by_category"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018959
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2018960
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    invoke-static {v0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v2

    check-cast v2, LX/0b3;

    invoke-static {v0}, LX/Dck;->a(LX/0QB;)LX/Dck;

    move-result-object v3

    check-cast v3, LX/Dck;

    new-instance v1, LX/DbN;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-direct {v1, p1}, LX/DbN;-><init>(LX/0Zb;)V

    move-object p1, v1

    check-cast p1, LX/DbN;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->a:LX/0b3;

    iput-object v3, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->b:LX/Dck;

    iput-object p1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->c:LX/DbN;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->d:LX/0kL;

    .line 2018961
    return-void
.end method

.method public final a(Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;)V
    .locals 12
    .param p1    # Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v4, -0x3749127d

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 2018999
    if-nez p1, :cond_0

    move v0, v5

    :goto_0
    if-eqz v0, :cond_2

    move v0, v5

    :goto_1
    if-eqz v0, :cond_4

    .line 2019000
    invoke-static {p0}, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->d(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V

    .line 2019001
    :goto_2
    return-void

    .line 2019002
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2019003
    if-nez v0, :cond_1

    move v0, v5

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_0

    .line 2019004
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v8, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2019005
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2019006
    :cond_4
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v8}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2019007
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2019008
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2019009
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2019010
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v8, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2019011
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2019012
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 2019013
    if-nez v1, :cond_8

    .line 2019014
    :cond_5
    :goto_5
    move v3, v6

    .line 2019015
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 2019016
    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v8, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v9

    .line 2019017
    new-instance v0, LX/Dcl;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    sget-object v2, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->k:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v6, v7, v5}, LX/15i;->h(II)Z

    move-result v5

    iget-object v6, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->h:Ljava/lang/String;

    if-eqz v9, :cond_7

    invoke-static {v9}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v7

    :goto_6
    invoke-direct/range {v0 .. v7}, LX/Dcl;-><init>(LX/0gc;Lcom/facebook/common/callercontext/CallerContext;ILcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;ZLjava/lang/String;LX/2uF;)V

    .line 2019018
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2019019
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3, v8}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2019020
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->B_(I)V

    .line 2019021
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2019022
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Dcn;

    invoke-direct {v1, p0}, LX/Dcn;-><init>(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V

    .line 2019023
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2019024
    goto/16 :goto_2

    .line 2019025
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2019026
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v7

    goto :goto_6

    :cond_8
    move v3, v6

    .line 2019027
    :goto_7
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v7

    if-ge v3, v7, :cond_5

    .line 2019028
    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v7

    iget-object v9, v7, LX/1vs;->a:LX/15i;

    iget v10, v7, LX/1vs;->b:I

    const-class v7, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    invoke-virtual {v9, v10, v6, v7, v11}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v7

    const-class v9, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    invoke-virtual {v2, v1, v6, v9, v11}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v9

    if-ne v7, v9, :cond_9

    move v6, v3

    .line 2019029
    goto :goto_5

    .line 2019030
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_7
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x875be5c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2018986
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018987
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->h:Ljava/lang/String;

    .line 2018988
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018989
    const-string v2, "local_content_entry_point"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->k:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018990
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->a:LX/0b3;

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->f:LX/8KR;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2018991
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2018992
    if-nez p3, :cond_0

    .line 2018993
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->c:LX/DbN;

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->h:Ljava/lang/String;

    .line 2018994
    iget-object v3, v0, LX/DbN;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p3, "photos_by_category_impression"

    invoke-direct {p0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p3, "photos_by_category"

    .line 2018995
    iput-object p3, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2018996
    move-object p0, p0

    .line 2018997
    const-string p3, "page_id"

    invoke-virtual {p0, p3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2018998
    :cond_0
    const v0, 0x7f030f53

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x2b

    const v3, -0x50754eaf

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6a878fc1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018983
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2018984
    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->a:LX/0b3;

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->f:LX/8KR;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2018985
    const/16 v1, 0x2b

    const v2, 0xe4b80c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xbd5bbce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018974
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2018975
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2018976
    const-string v2, "fragment_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2018977
    if-nez v1, :cond_1

    .line 2018978
    const v1, 0x7f0828ff

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2018979
    :goto_0
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2018980
    if-eqz v1, :cond_0

    .line 2018981
    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2018982
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6667cd94

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018962
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2018963
    const v0, 0x7f0d250e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2018964
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018965
    const v0, 0x7f0d250c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->i:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2018966
    const v0, 0x7f0d250d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->j:Landroid/support/v4/view/ViewPager;

    .line 2018967
    iget-object v0, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->b:LX/Dck;

    iget-object v1, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->k:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018968
    new-instance v3, LX/8AL;

    invoke-direct {v3}, LX/8AL;-><init>()V

    move-object v4, v3

    .line 2018969
    const-string p1, "entry_point"

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v4, p1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string p1, "page_id"

    invoke-virtual {v3, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2018970
    iget-object v3, v0, LX/Dck;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2018971
    iget-object v4, v0, LX/Dck;->b:LX/1Ck;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "task_key_load_initial_data"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, LX/Dcj;

    invoke-direct {p2, v0, p0}, LX/Dcj;-><init>(LX/Dck;Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V

    invoke-virtual {v4, p1, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2018972
    return-void

    .line 2018973
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->name()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
