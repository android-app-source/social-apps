.class public Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2019060
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2019061
    invoke-direct {p0}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->a()V

    .line 2019062
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019081
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2019082
    invoke-direct {p0}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->a()V

    .line 2019083
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019078
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2019079
    invoke-direct {p0}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->a()V

    .line 2019080
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2019071
    const v0, 0x7f031550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2019072
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setOrientation(I)V

    .line 2019073
    const v0, 0x7f0d2ff0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->a:Landroid/widget/TextView;

    .line 2019074
    const v0, 0x7f0d2ff1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->b:Landroid/widget/TextView;

    .line 2019075
    const v0, 0x7f0d2ff2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->c:Landroid/widget/ImageView;

    .line 2019076
    const v0, 0x7f0d0544

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->d:Landroid/widget/Button;

    .line 2019077
    return-void
.end method


# virtual methods
.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2019084
    iget-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2019085
    return-void
.end method

.method public setPrimaryUploadMessage(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2019069
    iget-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2019070
    return-void
.end method

.method public setSecondaryUploadMessage(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019065
    iget-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2019066
    iget-object v1, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->b:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2019067
    return-void

    .line 2019068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUploadButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019063
    iget-object v0, p0, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->d:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2019064
    return-void
.end method
