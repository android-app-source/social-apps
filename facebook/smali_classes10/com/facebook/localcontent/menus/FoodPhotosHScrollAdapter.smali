.class public Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/DbT;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/DbM;

.field public final c:LX/23R;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLInterfaces$PhotosQuery$PhotosByCategory$Nodes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2017124
    const-class v0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/DbM;LX/23R;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017125
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2017126
    iput-object p1, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->b:LX/DbM;

    .line 2017127
    iput-object p2, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->c:LX/23R;

    .line 2017128
    iput-object p3, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->d:Ljava/lang/String;

    .line 2017129
    iput-object p4, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->e:Ljava/lang/String;

    .line 2017130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->f:Ljava/util/ArrayList;

    .line 2017131
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2017132
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031112

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2017133
    const v1, 0x7f0d287b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017134
    new-instance v1, LX/DbT;

    invoke-direct {v1, p0, v0}, LX/DbT;-><init>(Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2017135
    check-cast p1, LX/DbT;

    .line 2017136
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017137
    iget-object v1, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    .line 2017138
    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2017139
    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2017140
    new-instance v2, LX/DbS;

    invoke-direct {v2, p0, v1, v0}, LX/DbS;-><init>(Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017141
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2017142
    iget-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
