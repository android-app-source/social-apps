.class public Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dby;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public d:Landroid/widget/Button;

.field public e:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2017097
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;

    invoke-static {p0}, LX/Dby;->a(LX/0QB;)LX/Dby;

    move-result-object p0

    check-cast p0, LX/Dby;

    iput-object p0, p1, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->a:LX/Dby;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017077
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2017078
    const-class v0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;

    invoke-static {v0, p0}, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2017079
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017080
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->b:Ljava/lang/String;

    .line 2017081
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017082
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2017083
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2017084
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2017085
    const/16 v0, 0x6591

    if-ne p1, v0, :cond_1

    .line 2017086
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2017087
    :cond_0
    :goto_0
    return-void

    .line 2017088
    :cond_1
    const/16 v0, 0x6593

    if-eq p1, v0, :cond_2

    const/16 v0, 0x6592

    if-ne p1, v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2017089
    if-eqz v0, :cond_0

    .line 2017090
    iget-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->a:LX/Dby;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    move v1, p1

    move-object v2, p0

    move-object v5, p3

    .line 2017091
    const/16 p0, 0x6592

    if-ne v1, p0, :cond_5

    .line 2017092
    const-string p0, "extra_media_items"

    invoke-virtual {v5, p0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    invoke-static {v0, v2, v3, v4, p0}, LX/Dby;->a$redex0(LX/Dby;Landroid/support/v4/app/Fragment;Ljava/lang/Long;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/List;)V

    .line 2017093
    :cond_3
    :goto_2
    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2017094
    :cond_5
    const/16 p0, 0x6593

    if-ne v1, p0, :cond_3

    .line 2017095
    iget-object p0, v0, LX/Dby;->c:LX/9iY;

    sget-object p1, LX/9iX;->IMAGE:LX/9iX;

    new-instance v1, LX/Dbx;

    invoke-direct {v1, v0, v2, v3, v4}, LX/Dbx;-><init>(LX/Dby;Landroid/support/v4/app/Fragment;Ljava/lang/Long;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    invoke-virtual {p0, p1, v5, v1}, LX/9iY;->a(LX/9iX;Landroid/content/Intent;LX/9Tq;)V

    .line 2017096
    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x50b75d14

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017076
    const v1, 0x7f0300a1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4207aeee

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x64afce22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017071
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2017072
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2017073
    if-eqz v1, :cond_0

    .line 2017074
    const v2, 0x7f0828e5

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2017075
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2e6e58a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017065
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2017066
    const v0, 0x7f0d04aa

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->d:Landroid/widget/Button;

    .line 2017067
    const v0, 0x7f0d04ab

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->e:Landroid/widget/Button;

    .line 2017068
    iget-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->d:Landroid/widget/Button;

    new-instance p1, LX/DbP;

    invoke-direct {p1, p0}, LX/DbP;-><init>(Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017069
    iget-object v0, p0, Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;->e:Landroid/widget/Button;

    new-instance p1, LX/DbQ;

    invoke-direct {p1, p0}, LX/DbQ;-><init>(Lcom/facebook/localcontent/menus/AddPhotoMenuFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017070
    return-void
.end method
