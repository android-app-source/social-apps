.class public Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final k:Lcom/facebook/common/callercontext/CallerContext;

.field private static final l:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DbM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1EZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/8LV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public n:Landroid/widget/LinearLayout;

.field private o:Landroid/view/LayoutInflater;

.field public p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public q:J

.field public r:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/1ZF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2017674
    const-class v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 2017675
    const-class v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2017673
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017659
    if-nez p1, :cond_0

    .line 2017660
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->l:Ljava/lang/String;

    const-string v2, "Null media items were provided."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017661
    :goto_0
    return-void

    .line 2017662
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2017663
    new-instance v3, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;

    invoke-direct {v3, v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;-><init>(Lcom/facebook/ipc/media/MediaItem;)V

    .line 2017664
    iget-object v4, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2017665
    invoke-static {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)Landroid/view/View;

    move-result-object v3

    .line 2017666
    iget-object v4, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2017667
    iget-object v4, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {p0, v4}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V

    .line 2017668
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 2017669
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2017670
    :cond_1
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iget-wide v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2017671
    iget-object v4, v0, LX/DbM;->a:LX/0Zb;

    const-string p0, "upload_photo_menu_photos_selected"

    invoke-static {p0, v1}, LX/DbM;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "photos_selected_count"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "photos_total_count"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v4, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017672
    goto :goto_0
.end method

.method public static d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2017658
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->o:Landroid/view/LayoutInflater;

    const v1, 0x7f030f46

    iget-object v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V
    .locals 6

    .prologue
    .line 2017643
    :goto_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2017644
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;

    sget-object v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 2017645
    invoke-virtual {v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->clearFocus()V

    .line 2017646
    iget-object v3, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2017647
    invoke-virtual {v0, v3}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->setDescription(Ljava/lang/String;)V

    .line 2017648
    iget-object v3, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    move-object v3, v3

    .line 2017649
    iget v4, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->b:I

    move v4, v4

    .line 2017650
    iget v5, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->c:I

    move v5, v5

    .line 2017651
    invoke-virtual {v0, v3, v4, v5, v2}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a(Landroid/net/Uri;IILcom/facebook/common/callercontext/CallerContext;)V

    .line 2017652
    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->setPhotoNumber(I)V

    .line 2017653
    new-instance v3, LX/Dbr;

    invoke-direct {v3, v1}, LX/Dbr;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;)V

    invoke-virtual {v0, v3}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->setDescriptionWatcher(Landroid/text/TextWatcher;)V

    .line 2017654
    new-instance v3, LX/Dbs;

    invoke-direct {v3, p0, p1}, LX/Dbs;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V

    invoke-virtual {v0, v3}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->setRemoveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017655
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2017656
    :cond_0
    invoke-static {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->l(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    .line 2017657
    return-void
.end method

.method public static l(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V
    .locals 4

    .prologue
    .line 2017633
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 2017634
    :goto_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2017635
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f0828e0

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2017636
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 2017637
    move-object v2, v2

    .line 2017638
    iput-boolean v1, v2, LX/108;->d:Z

    .line 2017639
    move-object v1, v2

    .line 2017640
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2017641
    :cond_0
    return-void

    .line 2017642
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V
    .locals 1

    .prologue
    .line 2017629
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2017630
    if-eqz v0, :cond_0

    .line 2017631
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2017632
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2017624
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2017625
    invoke-static {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->u(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    .line 2017626
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2017627
    :cond_0
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0828ea

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0828eb

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f0828ec

    new-instance v2, LX/Dbp;

    invoke-direct {v2, p0}, LX/Dbp;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0828ed

    new-instance v2, LX/Dbo;

    invoke-direct {v2, p0}, LX/Dbo;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2017628
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017610
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2017611
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {p1}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v5

    check-cast v5, LX/DbM;

    invoke-static {p1}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v6

    check-cast v6, LX/CXj;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    const/16 v9, 0x12c4

    invoke-static {p1, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p1}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v10

    check-cast v10, LX/1EZ;

    invoke-static {p1}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v11

    check-cast v11, LX/8LV;

    const/16 v0, 0xf07

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->b:LX/0wM;

    iput-object v5, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iput-object v6, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d:LX/CXj;

    iput-object v7, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->f:LX/1Ck;

    iput-object v9, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->g:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->h:LX/1EZ;

    iput-object v11, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->i:LX/8LV;

    iput-object p1, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->j:LX/0Ot;

    .line 2017612
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017613
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    .line 2017614
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->s:LX/0am;

    .line 2017615
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017616
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->r:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2017617
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->r:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-nez v0, :cond_0

    .line 2017618
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080024

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v4, v5, v6, v7, v8}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v5

    .line 2017619
    iget-object v4, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3iH;

    iget-wide v6, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2017620
    iget-object v6, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->f:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "fetch_viewer_context"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/Dbq;

    invoke-direct {v8, p0, v5}, LX/Dbq;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;LX/4BY;)V

    invoke-virtual {v6, v7, v4, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2017621
    :cond_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iget-wide v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 2017622
    const-string v2, "upload_photo_menu_impression"

    invoke-static {v0, v2, v1}, LX/DbM;->d(LX/DbM;Ljava/lang/String;Ljava/lang/String;)V

    .line 2017623
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x52bf9fd2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017608
    iput-object p1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->o:Landroid/view/LayoutInflater;

    .line 2017609
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030f45

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7e93b5d1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2017581
    const-string v0, "photo_menu_uploads_models"

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2017582
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x14df4ccb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017603
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2017604
    invoke-static {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->l(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    .line 2017605
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->s:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2017606
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->s:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    new-instance v2, LX/Dbl;

    invoke-direct {v2, p0}, LX/Dbl;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2017607
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x678a8c74

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017583
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2017584
    const v0, 0x7f0d24f9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->m:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2017585
    const v0, 0x7f0d24f8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    .line 2017586
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    new-instance v1, LX/Dbk;

    invoke-direct {v1, p0}, LX/Dbk;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2017587
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->m:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v1, 0x7f0828dc

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 2017588
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->m:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->b:LX/0wM;

    const v2, 0x7f0207b3

    const p1, -0xa76f01

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2017589
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->m:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v1, LX/Dbm;

    invoke-direct {v1, p0}, LX/Dbm;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017590
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    .line 2017591
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 2017592
    const/4 v1, 0x0

    .line 2017593
    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2017594
    iget-object v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2017595
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2017596
    :cond_0
    invoke-static {p0, v1}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V

    .line 2017597
    :goto_2
    return-void

    .line 2017598
    :cond_1
    const-string v0, "photo_menu_uploads_models"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 2017599
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    .line 2017600
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017601
    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->a(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;Ljava/util/ArrayList;)V

    .line 2017602
    goto :goto_2
.end method
