.class public Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/DbX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DbM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Dbb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Dbe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1L3;

.field public l:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private m:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

.field public n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

.field public o:Ljava/lang/String;

.field public p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2017372
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2017373
    const-string v0, "photo_menu_viewer"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017374
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2017375
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    invoke-static {v0}, LX/DbX;->b(LX/0QB;)LX/DbX;

    move-result-object v3

    check-cast v3, LX/DbX;

    invoke-static {v0}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v4

    check-cast v4, LX/DbM;

    const-class v5, LX/Dbb;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Dbb;

    invoke-static {v0}, LX/Dbe;->a(LX/0QB;)LX/Dbe;

    move-result-object v6

    check-cast v6, LX/Dbe;

    invoke-static {v0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v7

    check-cast v7, LX/1L1;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v8

    check-cast v8, LX/0hB;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v10

    check-cast v10, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v11

    check-cast v11, LX/0SI;

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v0

    check-cast v0, LX/3iH;

    iput-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->a:LX/DbX;

    iput-object v4, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->b:LX/DbM;

    iput-object v5, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->c:LX/Dbb;

    iput-object v6, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->d:LX/Dbe;

    iput-object v7, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->e:LX/1L1;

    iput-object v8, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->f:LX/0hB;

    iput-object v9, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->g:LX/1Ck;

    iput-object v10, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v11, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->i:LX/0SI;

    iput-object v0, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->j:LX/3iH;

    .line 2017376
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2017377
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    .line 2017378
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2017379
    if-nez p1, :cond_0

    .line 2017380
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->b:LX/DbM;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    .line 2017381
    iget-object v2, v0, LX/DbM;->a:LX/0Zb;

    const-string v3, "photo_menu_viewer"

    const-string v4, "photo_menu_viewer_impression"

    invoke-static {v3, v4, v1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017382
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x60970ed9

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2017383
    const v0, 0x7f030e9d

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2017384
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->c:LX/Dbb;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    .line 2017385
    new-instance p3, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    invoke-static {v0}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v6

    check-cast v6, LX/DbM;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object p2

    check-cast p2, LX/23R;

    invoke-direct {p3, v6, p2, v1}, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;-><init>(LX/DbM;LX/23R;Ljava/lang/String;)V

    .line 2017386
    move-object v0, p3

    .line 2017387
    iput-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    .line 2017388
    const v0, 0x7f0d23c9

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2017389
    const v0, 0x7f0d23c8

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2017390
    const v1, 0x7f030e9c

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 2017391
    const v2, 0x7f0d23c7

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    iput-object v2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->m:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    .line 2017392
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2017393
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 2017394
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2017395
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 2017396
    new-instance v0, LX/Dbg;

    invoke-direct {v0, p0}, LX/Dbg;-><init>(Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;)V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->k:LX/1L3;

    .line 2017397
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->e:LX/1L1;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->k:LX/1L3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2017398
    const/16 v0, 0x2b

    const v1, 0x707b258e

    invoke-static {v5, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v4
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x680a7644

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017399
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2017400
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->e:LX/1L1;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->k:LX/1L3;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2017401
    const/16 v1, 0x2b

    const v2, -0x4ff58c43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4f9d5a23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017402
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2017403
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2017404
    if-eqz v1, :cond_0

    .line 2017405
    const v2, 0x7f0828d8

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2017406
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x8be784d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017407
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2017408
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2017409
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->d:LX/Dbe;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->f:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    .line 2017410
    new-instance v3, LX/8AC;

    invoke-direct {v3}, LX/8AC;-><init>()V

    move-object v3, v3

    .line 2017411
    const-string v4, "page_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string p1, "count"

    const/16 p2, 0x3e8

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v4, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string p1, "image_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v4, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2017412
    iget-object v4, v0, LX/Dbe;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2017413
    iget-object v4, v0, LX/Dbe;->c:LX/1Ck;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "task_key_load_photo_menus"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2017414
    new-instance p2, LX/Dbd;

    invoke-direct {p2, v0}, LX/Dbd;-><init>(LX/Dbe;)V

    iget-object v1, v0, LX/Dbe;->a:LX/0TD;

    invoke-static {v3, p2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    move-object v3, p2

    .line 2017415
    new-instance p2, LX/Dbc;

    invoke-direct {p2, v0, p0}, LX/Dbc;-><init>(LX/Dbe;Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;)V

    invoke-virtual {v4, p1, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2017416
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->a:LX/DbX;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->m:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/DbX;->a(Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;Ljava/lang/String;Ljava/lang/String;)V

    .line 2017417
    return-void
.end method
