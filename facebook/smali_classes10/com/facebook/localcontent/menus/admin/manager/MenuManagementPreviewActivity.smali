.class public Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/Dc2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2017801
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2017802
    return-void
.end method

.method private a(LX/Dc2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2017803
    sget-object v0, LX/Dc2;->STRUCTURED_MENU:LX/Dc2;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/Dc2;->PHOTO_MENU:LX/Dc2;

    if-eq p1, v0, :cond_0

    .line 2017804
    const/4 v0, 0x0

    .line 2017805
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f0828fd

    invoke-virtual {p0, v1}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2017806
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2017807
    move-object v0, v0

    .line 2017808
    const/4 v1, 0x1

    .line 2017809
    iput-boolean v1, v0, LX/108;->d:Z

    .line 2017810
    move-object v0, v0

    .line 2017811
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2017812
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2017813
    const v0, 0x7f0d002f

    invoke-virtual {v1, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2017814
    if-nez v0, :cond_0

    .line 2017815
    sget-object v2, LX/Dc1;->a:[I

    iget-object v3, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->q:LX/Dc2;

    invoke-virtual {v3}, LX/Dc2;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2017816
    iget-object v2, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->p:LX/03V;

    const-class v3, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Trying to preview an unsupported menu type"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017817
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2017818
    :cond_0
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2017819
    return-void

    .line 2017820
    :pswitch_0
    new-instance v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    invoke-direct {v0}, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;-><init>()V

    goto :goto_0

    .line 2017821
    :pswitch_1
    new-instance v0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

    invoke-direct {v0}, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;-><init>()V

    goto :goto_0

    .line 2017822
    :pswitch_2
    new-instance v0, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    invoke-direct {v0}, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;-><init>()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->p:LX/03V;

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2017823
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 2017824
    new-instance v1, LX/Dbz;

    invoke-direct {v1, p0}, LX/Dbz;-><init>(Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;)V

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2017825
    new-instance v1, LX/94Z;

    new-instance v2, LX/94Y;

    invoke-direct {v2}, LX/94Y;-><init>()V

    iget-object v3, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->q:LX/Dc2;

    invoke-direct {p0, v3}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->a(LX/Dc2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2017826
    iput-object v3, v2, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2017827
    move-object v2, v2

    .line 2017828
    new-instance v3, LX/Dc0;

    invoke-direct {v3, p0}, LX/Dc0;-><init>(Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;)V

    .line 2017829
    iput-object v3, v2, LX/94Y;->c:LX/63W;

    .line 2017830
    move-object v2, v2

    .line 2017831
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->l()Ljava/lang/String;

    move-result-object v3

    .line 2017832
    iput-object v3, v2, LX/94Y;->a:Ljava/lang/String;

    .line 2017833
    move-object v2, v2

    .line 2017834
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 2017835
    iput-object v3, v2, LX/94Y;->d:LX/94c;

    .line 2017836
    move-object v2, v2

    .line 2017837
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    .line 2017838
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2017839
    sget-object v0, LX/Dc1;->a:[I

    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->q:LX/Dc2;

    invoke-virtual {v1}, LX/Dc2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2017840
    const v0, 0x7f0828ef

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2017841
    :pswitch_0
    const v0, 0x7f0828da

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2017842
    :pswitch_1
    const v0, 0x7f0828fe

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2017843
    :pswitch_2
    const v0, 0x7f0828f4

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017844
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2017845
    invoke-static {p0, p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2017846
    const v0, 0x7f030ac7

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->setContentView(I)V

    .line 2017847
    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_menu_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dc2;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->q:LX/Dc2;

    .line 2017848
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->a()V

    .line 2017849
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;->b()V

    .line 2017850
    return-void
.end method
