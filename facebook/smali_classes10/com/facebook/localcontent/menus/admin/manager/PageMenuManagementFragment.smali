.class public Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DbM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DcE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/Set;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mMenuTypeHandlers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Dc9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/DcG;",
            "LX/Dc9;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/8KR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8KR",
            "<",
            "LX/0bP;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public k:LX/4BY;

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2018073
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/fbui/widget/contentview/CheckedContentView;LX/DcG;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 6
    .param p2    # LX/DcG;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2018058
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dc9;

    .line 2018059
    if-nez v0, :cond_0

    .line 2018060
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a:LX/03V;

    const-class v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No handler available for menu type"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/DcG;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018061
    invoke-virtual {p1, v5}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2018062
    :goto_0
    return-void

    .line 2018063
    :cond_0
    if-nez p3, :cond_2

    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    const/4 v2, 0x0

    .line 2018064
    :goto_2
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2018065
    invoke-interface {v0, v2, v1}, LX/Dc9;->b(LX/15i;I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2018066
    invoke-virtual {p1, v5}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    goto :goto_0

    .line 2018067
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_1

    move v2, v1

    goto :goto_1

    .line 2018068
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v3

    iget-object v2, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2018069
    invoke-virtual {v2, v3, v1}, LX/15i;->g(II)I

    move-result v1

    goto :goto_2

    .line 2018070
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2018071
    :cond_4
    invoke-virtual {p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-interface {v0, v2, v1}, LX/Dc9;->a(LX/15i;I)Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2018072
    new-instance v1, LX/DcB;

    invoke-direct {v1, p0, p2, v0, p3}, LX/DcB;-><init>(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;LX/DcG;LX/Dc9;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v1, p1

    check-cast v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v7}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v3

    check-cast v3, LX/0b3;

    invoke-static {v7}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v4

    check-cast v4, LX/DbM;

    new-instance p0, LX/DcE;

    invoke-static {v7}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v7}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 p1, 0xf07

    invoke-static {v7, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {p0, v5, v6, p1}, LX/DcE;-><init>(LX/0tX;LX/1Ck;LX/0Ot;)V

    move-object v5, p0

    check-cast v5, LX/DcE;

    new-instance v6, LX/0U8;

    invoke-interface {v7}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    new-instance p1, LX/DcK;

    invoke-direct {p1, v7}, LX/DcK;-><init>(LX/0QB;)V

    invoke-direct {v6, p0, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    const/16 p0, 0x12c4

    invoke-static {v7, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    iput-object v2, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a:LX/03V;

    iput-object v3, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->b:LX/0b3;

    iput-object v4, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c:LX/DbM;

    iput-object v5, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->d:LX/DcE;

    iput-object v6, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->e:Ljava/util/Set;

    iput-object v7, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->f:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018049
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2018050
    const-class v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-static {v0, p0}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2018051
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2018052
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dc9;

    .line 2018053
    invoke-interface {v0}, LX/Dc8;->a()LX/DcG;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 2018054
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->g:Ljava/util/Map;

    .line 2018055
    new-instance v0, LX/DcC;

    invoke-direct {v0, p0}, LX/DcC;-><init>(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;)V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->h:LX/8KR;

    .line 2018056
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->b:LX/0b3;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->h:LX/8KR;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2018057
    return-void
.end method

.method public final a(Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 5
    .param p1    # Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018041
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c:LX/DbM;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    .line 2018042
    iget-object v2, v0, LX/DbM;->a:LX/0Zb;

    const-string v3, "page_menu_management"

    const-string v4, "menu_management_load_successful"

    invoke-static {v3, v4, v1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2018043
    const v0, 0x7f0d23b6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    sget-object v1, LX/DcG;->PHOTO_MENU:LX/DcG;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/fbui/widget/contentview/CheckedContentView;LX/DcG;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2018044
    const v0, 0x7f0d23b7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    sget-object v1, LX/DcG;->STRUCTURED_MENU:LX/DcG;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/fbui/widget/contentview/CheckedContentView;LX/DcG;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2018045
    const v0, 0x7f0d23b8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    sget-object v1, LX/DcG;->LINK_MENU:LX/DcG;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/fbui/widget/contentview/CheckedContentView;LX/DcG;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2018046
    const v0, 0x7f0d23b9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    sget-object v1, LX/DcG;->NONE:LX/DcG;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/fbui/widget/contentview/CheckedContentView;LX/DcG;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2018047
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2018048
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2018031
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    if-eqz v0, :cond_0

    .line 2018032
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 2018033
    :goto_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_1

    .line 2018034
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->d:LX/DcE;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    .line 2018035
    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, LX/DcE;->a(LX/DcE;Ljava/lang/String;Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Z)V

    .line 2018036
    :goto_1
    return-void

    .line 2018037
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    goto :goto_0

    .line 2018038
    :cond_1
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->d:LX/DcE;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    .line 2018039
    const/4 v2, 0x1

    invoke-static {v0, v1, p0, v2}, LX/DcE;->a(LX/DcE;Ljava/lang/String;Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Z)V

    .line 2018040
    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2018003
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2018004
    iget-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dc9;

    .line 2018005
    invoke-interface {v0, p1}, LX/Dc8;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2018006
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    invoke-interface {v0, p0, v1, p1, p2}, LX/Dc8;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;II)V

    .line 2018007
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6615a8b7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018030
    const v1, 0x7f030e96

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x737f3e69

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4bf05b15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018022
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->d:LX/DcE;

    .line 2018023
    iget-object v2, v1, LX/DcE;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2018024
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    if-eqz v1, :cond_0

    .line 2018025
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->cancel()V

    .line 2018026
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2018027
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->h:LX/8KR;

    if-eqz v1, :cond_1

    .line 2018028
    iget-object v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->b:LX/0b3;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->h:LX/8KR;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2018029
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x48c26ac5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1feb21af

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018015
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2018016
    iget-boolean v1, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->l:Z

    if-eqz v1, :cond_0

    .line 2018017
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2018018
    :cond_0
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2018019
    if-eqz v1, :cond_1

    .line 2018020
    const v2, 0x7f0828ef

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2018021
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x6d359ee6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018008
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2018009
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018010
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    .line 2018011
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018012
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2018013
    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c()V

    .line 2018014
    return-void
.end method
