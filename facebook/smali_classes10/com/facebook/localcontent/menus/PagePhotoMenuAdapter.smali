.class public Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:LX/DbM;

.field public final b:LX/23R;

.field public final c:Ljava/lang/String;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLInterfaces$PhotoMenusData$PagePhotoMenus$Nodes$PagePhotoMenuPhotos$Edges;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DbM;LX/23R;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017290
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2017291
    iput-object p1, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->a:LX/DbM;

    .line 2017292
    iput-object p2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->b:LX/23R;

    .line 2017293
    iput-object p3, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->c:Ljava/lang/String;

    .line 2017294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    .line 2017295
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2017269
    new-instance v0, LX/Dbi;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Dbi;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2017278
    check-cast p3, LX/Dbi;

    .line 2017279
    check-cast p2, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;

    .line 2017280
    invoke-virtual {p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2017281
    invoke-virtual {p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2017282
    iget-object v2, p3, LX/Dbi;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2017283
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->d()J

    move-result-wide v2

    invoke-virtual {p3, v2, v3}, LX/Dbi;->setTimestamp(J)V

    .line 2017284
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->c()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2017285
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->c()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->c()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->c()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->a()I

    move-result v3

    .line 2017286
    iget-object p1, p3, LX/Dbi;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    int-to-float p2, v2

    int-to-float p4, v3

    div-float/2addr p2, p4

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2017287
    iget-object p1, p3, LX/Dbi;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p4

    invoke-static {p4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p4

    invoke-virtual {p1, p2, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2017288
    :cond_0
    new-instance v1, LX/Dba;

    invoke-direct {v1, p0, v0, p3}, LX/Dba;-><init>(Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;LX/Dbi;)V

    invoke-virtual {p3, v1}, LX/Dbi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017289
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLInterfaces$PhotoMenusData$PagePhotoMenus$Nodes$PagePhotoMenuPhotos$Edges;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017273
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;

    .line 2017274
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2017275
    iget-object v2, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2017276
    :cond_1
    const v0, -0x45df411d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2017277
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2017272
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2017271
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2017270
    int-to-long v0, p1

    return-wide v0
.end method
