.class public Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1ZF;


# instance fields
.field private p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

.field private q:LX/94V;

.field private r:LX/94Z;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2017507
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2017500
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2017501
    const v0, 0x7f0d002f

    invoke-virtual {v1, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    .line 2017502
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    if-nez v0, :cond_0

    .line 2017503
    new-instance v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-direct {v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    .line 2017504
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2017505
    :cond_0
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2017506
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 2017453
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->q:LX/94V;

    .line 2017454
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->q:LX/94V;

    new-instance v1, LX/Dbj;

    invoke-direct {v1, p0}, LX/Dbj;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;)V

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2017455
    new-instance v0, LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->q:LX/94V;

    new-instance v2, LX/94Y;

    invoke-direct {v2}, LX/94Y;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0828da

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2017456
    iput-object v3, v2, LX/94Y;->a:Ljava/lang/String;

    .line 2017457
    move-object v2, v2

    .line 2017458
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 2017459
    iput-object v3, v2, LX/94Y;->d:LX/94c;

    .line 2017460
    move-object v2, v2

    .line 2017461
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017462
    return-void
.end method


# virtual methods
.method public final a(LX/63W;)V
    .locals 2

    .prologue
    .line 2017493
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017494
    iget-object p0, v1, LX/94Z;->b:LX/94X;

    move-object v1, p0

    .line 2017495
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    .line 2017496
    iput-object p1, v1, LX/94Y;->c:LX/63W;

    .line 2017497
    move-object v1, v1

    .line 2017498
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2017499
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2017486
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017487
    iget-object p0, v1, LX/94Z;->b:LX/94X;

    move-object v1, p0

    .line 2017488
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    .line 2017489
    iput-object p1, v1, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2017490
    move-object v1, v1

    .line 2017491
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2017492
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2017479
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017480
    iget-object p0, v1, LX/94Z;->b:LX/94X;

    move-object v1, p0

    .line 2017481
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    .line 2017482
    iput-object p1, v1, LX/94Y;->a:Ljava/lang/String;

    .line 2017483
    move-object v1, v1

    .line 2017484
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2017485
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017474
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2017475
    const v0, 0x7f030f44

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->setContentView(I)V

    .line 2017476
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->a()V

    .line 2017477
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->b()V

    .line 2017478
    return-void
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2017508
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017509
    iget-object p0, v1, LX/94Z;->b:LX/94X;

    move-object v1, p0

    .line 2017510
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    .line 2017511
    iput-object p1, v1, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2017512
    move-object v1, v1

    .line 2017513
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2017514
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 2017473
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final k_(Z)V
    .locals 1

    .prologue
    .line 2017472
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final lH_()V
    .locals 1

    .prologue
    .line 2017471
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2017465
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    .line 2017466
    const/4 p0, -0x1

    if-eq p2, p0, :cond_1

    .line 2017467
    :cond_0
    :goto_0
    return-void

    .line 2017468
    :cond_1
    const/16 p0, 0x6592

    if-ne p1, p0, :cond_0

    .line 2017469
    const-string p0, "extra_media_items"

    invoke-virtual {p3, p0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->a(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;Ljava/util/ArrayList;)V

    .line 2017470
    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2017463
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->p:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-virtual {v0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->S_()Z

    .line 2017464
    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2017452
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final x_(I)V
    .locals 3

    .prologue
    .line 2017445
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->r:LX/94Z;

    .line 2017446
    iget-object v2, v1, LX/94Z;->b:LX/94X;

    move-object v1, v2

    .line 2017447
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2017448
    iput-object v2, v1, LX/94Y;->a:Ljava/lang/String;

    .line 2017449
    move-object v1, v1

    .line 2017450
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2017451
    return-void
.end method
