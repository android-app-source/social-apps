.class public Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/support/v7/widget/RecyclerView;

.field private b:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2017230
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2017231
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a()V

    .line 2017232
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017233
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2017234
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a()V

    .line 2017235
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017236
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2017237
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a()V

    .line 2017238
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2017239
    const v0, 0x7f030684

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2017240
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setOrientation(I)V

    .line 2017241
    const v0, 0x7f0d11e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a:Landroid/support/v7/widget/RecyclerView;

    .line 2017242
    const v0, 0x7f0d11e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->b:Landroid/widget/FrameLayout;

    .line 2017243
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2017244
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2017245
    iget-object v1, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2017246
    return-void
.end method


# virtual methods
.method public setRecyclerAdapter(LX/1OM;)V
    .locals 1

    .prologue
    .line 2017247
    iget-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2017248
    return-void
.end method

.method public setRecyclerOnScrollListener(LX/1OX;)V
    .locals 1

    .prologue
    .line 2017249
    iget-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2017250
    return-void
.end method

.method public setSeeMoreOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2017251
    iget-object v0, p0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017252
    return-void
.end method
