.class public Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/ipc/media/MediaItem;

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2017696
    new-instance v0, LX/Dbu;

    invoke-direct {v0}, LX/Dbu;-><init>()V

    sput-object v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2017697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017698
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 2017699
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->b:I

    .line 2017700
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->c:I

    .line 2017701
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    .line 2017702
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 2

    .prologue
    .line 2017703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017704
    iput-object p1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 2017705
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2017706
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2017707
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2017708
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->b:I

    .line 2017709
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->c:I

    .line 2017710
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2017711
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2017712
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2017713
    iget v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2017714
    iget v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2017715
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2017716
    return-void
.end method
