.class public Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/EditText;

.field public b:Landroid/widget/TextView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2017752
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2017753
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a()V

    .line 2017754
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017722
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2017723
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a()V

    .line 2017724
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017749
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2017750
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a()V

    .line 2017751
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2017741
    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f47

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2017742
    const v0, 0x7f0d24fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a:Landroid/widget/EditText;

    .line 2017743
    const v0, 0x7f0d24fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->b:Landroid/widget/TextView;

    .line 2017744
    const v0, 0x7f0d24fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017745
    const v0, 0x7f0d24fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->d:Landroid/widget/TextView;

    .line 2017746
    const v0, 0x7f0d24fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->e:Landroid/widget/ImageView;

    .line 2017747
    invoke-direct {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->b()V

    .line 2017748
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2017738
    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 2017739
    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a:Landroid/widget/EditText;

    new-instance v2, LX/Dbv;

    invoke-direct {v2, p0, v0}, LX/Dbv;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;I)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2017740
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;IILcom/facebook/common/callercontext/CallerContext;)V
    .locals 3

    .prologue
    .line 2017735
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2017736
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2017737
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017733
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2017734
    return-void
.end method

.method public setDescriptionWatcher(Landroid/text/TextWatcher;)V
    .locals 2

    .prologue
    .line 2017729
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2017730
    iput-object p1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->f:Landroid/text/TextWatcher;

    .line 2017731
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2017732
    return-void
.end method

.method public setPhotoNumber(I)V
    .locals 2

    .prologue
    .line 2017727
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2017728
    return-void
.end method

.method public setRemoveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2017725
    iget-object v0, p0, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017726
    return-void
.end method
