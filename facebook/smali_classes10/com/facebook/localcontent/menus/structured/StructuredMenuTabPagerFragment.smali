.class public Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/DbX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DbM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DcP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DcR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DcQ;

.field public g:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public h:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

.field public i:Z

.field private j:Landroid/widget/LinearLayout;

.field public k:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public l:Landroid/support/v4/view/ViewPager;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2018443
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static e(Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V
    .locals 2

    .prologue
    .line 2018429
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f0828d7

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 2018430
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018431
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2018442
    const-string v0, "structured_menu_viewer"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018432
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2018433
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

    invoke-static {v0}, LX/DbX;->b(LX/0QB;)LX/DbX;

    move-result-object v3

    check-cast v3, LX/DbX;

    invoke-static {v0}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v4

    check-cast v4, LX/DbM;

    invoke-static {v0}, LX/DcP;->a(LX/0QB;)LX/DcP;

    move-result-object v5

    check-cast v5, LX/DcP;

    const-class v6, LX/DcR;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/DcR;

    invoke-static {v0}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v0

    check-cast v0, LX/3Fx;

    iput-object v3, v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->a:LX/DbX;

    iput-object v4, v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->b:LX/DbM;

    iput-object v5, v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->c:LX/DcP;

    iput-object v6, v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->d:LX/DcR;

    iput-object v0, v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->e:LX/3Fx;

    .line 2018434
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018435
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    .line 2018436
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018437
    const-string v1, "local_content_food_photos_header_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->i:Z

    .line 2018438
    if-nez p1, :cond_0

    .line 2018439
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->b:LX/DbM;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    .line 2018440
    iget-object v2, v0, LX/DbM;->a:LX/0Zb;

    const-string v3, "structured_menu_viewer"

    const-string v4, "structured_menu_viewer_impression"

    invoke-static {v3, v4, v1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2018441
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4cba6168    # 9.7717056E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018444
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2018445
    const v2, 0x7f031403

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6d2a82c3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4aca79e4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018407
    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2018408
    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->c:LX/DcP;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    .line 2018409
    iget-object v4, v1, LX/DcP;->b:LX/1Ck;

    invoke-static {v2}, LX/DcP;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2018410
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2018411
    const/16 v1, 0x2b

    const v2, 0x68d74bb7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x193864dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018398
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2018399
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2018400
    const-string v2, "profile_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2018401
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v1, 0x7f0828d8

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2018402
    :goto_0
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2018403
    if-eqz v1, :cond_0

    .line 2018404
    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2018405
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6accc497

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v2, v1

    .line 2018406
    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018412
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2018413
    const v0, 0x7f0d2e06

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2018414
    const v0, 0x7f0d2e07

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->j:Landroid/widget/LinearLayout;

    .line 2018415
    const v0, 0x7f0d23c7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->h:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    .line 2018416
    const v0, 0x7f0d2e08

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->k:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2018417
    const v0, 0x7f0d2e05

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->l:Landroid/support/v4/view/ViewPager;

    .line 2018418
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->c:LX/DcP;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    .line 2018419
    new-instance v2, LX/8B4;

    invoke-direct {v2}, LX/8B4;-><init>()V

    move-object v2, v2

    .line 2018420
    const-string v3, "page_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2018421
    iget-object v3, v0, LX/DcP;->a:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2018422
    iget-object v3, v0, LX/DcP;->b:LX/1Ck;

    invoke-static {v1}, LX/DcP;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, LX/DcO;

    invoke-direct {p2, v0, p0}, LX/DcO;-><init>(LX/DcP;Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V

    invoke-virtual {v3, p1, v2, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2018423
    iget-boolean v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->i:Z

    if-eqz v0, :cond_0

    .line 2018424
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->a:LX/DbX;

    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->h:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    iget-object v2, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->m:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/DbX;->a(Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;Ljava/lang/String;Ljava/lang/String;)V

    .line 2018425
    :goto_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018426
    return-void

    .line 2018427
    :cond_0
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->h:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setVisibility(I)V

    .line 2018428
    goto :goto_0
.end method
