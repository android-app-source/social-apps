.class public Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DcZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DcN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private d:Lcom/facebook/widget/listview/BetterListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2018237
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    new-instance v3, LX/DcZ;

    new-instance v4, LX/DcW;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    const/16 v10, 0x13a4

    invoke-static {v1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, LX/DcW;-><init>(LX/03V;LX/0tX;Landroid/content/res/Resources;LX/1Ck;LX/0kL;LX/0Or;)V

    move-object v2, v4

    check-cast v2, LX/DcW;

    invoke-direct {v3, v2}, LX/DcZ;-><init>(LX/DcW;)V

    move-object v0, v3

    check-cast v0, LX/DcZ;

    invoke-static {v1}, LX/DcN;->a(LX/0QB;)LX/DcN;

    move-result-object v1

    check-cast v1, LX/DcN;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->a:LX/DcZ;

    iput-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->b:LX/DcN;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018238
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2018239
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2018240
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x2cc08e0f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2018241
    const v0, 0x7f031405

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2018242
    const v0, 0x7f0d2e0b

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2018243
    const v0, 0x7f0d2e0a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    .line 2018244
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2018245
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2018246
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2018247
    const-string v4, "local_content_padding_top"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setMinimumHeight(I)V

    .line 2018248
    iget-object v3, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2018249
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->a:LX/DcZ;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2018250
    const/16 v0, 0x2b

    const v3, -0x2d2032c2

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018251
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2018252
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018253
    const-string v1, "local_content_menu_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2018254
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018255
    iget-object v1, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->b:LX/DcN;

    const/16 p2, 0x3e8

    .line 2018256
    new-instance v2, LX/8AV;

    invoke-direct {v2}, LX/8AV;-><init>()V

    move-object v2, v2

    .line 2018257
    const-string v3, "node_id"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "sublist_count"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "items_count"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2018258
    iget-object v3, v1, LX/DcN;->a:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2018259
    iget-object v3, v1, LX/DcN;->b:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "task_key_fetch_structured_menu"

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance p1, LX/DcM;

    invoke-direct {p1, v1, p0}, LX/DcM;-><init>(LX/DcN;Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;)V

    invoke-virtual {v3, v4, v2, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2018260
    iget-object v0, p0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018261
    return-void
.end method
