.class public Lcom/facebook/localcontent/criticreviews/CriticReviewView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2017010
    const-class v0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2017050
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2017051
    invoke-direct {p0}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->b()V

    .line 2017052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017047
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2017048
    invoke-direct {p0}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->b()V

    .line 2017049
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017044
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2017045
    invoke-direct {p0}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->b()V

    .line 2017046
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017040
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2017041
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2017042
    return-void

    .line 2017043
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2017030
    const v0, 0x7f0303b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2017031
    const v0, 0x7f0d0bad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2017032
    const v0, 0x7f0d0bae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017033
    const v0, 0x7f0d0baf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->d:Landroid/widget/TextView;

    .line 2017034
    const v0, 0x7f0d0bb0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->e:Landroid/widget/TextView;

    .line 2017035
    const v0, 0x7f0d0bb1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017036
    const v0, 0x7f0d0bb3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->g:Landroid/widget/TextView;

    .line 2017037
    const v0, 0x7f0d0bb4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->h:Landroid/widget/TextView;

    .line 2017038
    const v0, 0x7f0d0bb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->i:Landroid/widget/TextView;

    .line 2017039
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2017029
    iget-boolean v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->j:Z

    return v0
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 2017027
    iput-boolean p1, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->j:Z

    .line 2017028
    return-void
.end method

.method public setPublishTime(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017025
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2017026
    return-void
.end method

.method public setPublisherContainerOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2017023
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017024
    return-void
.end method

.method public setPublisherName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017021
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->d:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2017022
    return-void
.end method

.method public setPublisherThumbnail(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2017019
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2017020
    return-void
.end method

.method public setReviewAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017017
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->i:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2017018
    return-void
.end method

.method public setReviewSummary(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017015
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2017016
    return-void
.end method

.method public setReviewThumbnail(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2017013
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2017014
    return-void
.end method

.method public setReviewTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017011
    iget-object v0, p0, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->g:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/localcontent/criticreviews/CriticReviewView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2017012
    return-void
.end method
