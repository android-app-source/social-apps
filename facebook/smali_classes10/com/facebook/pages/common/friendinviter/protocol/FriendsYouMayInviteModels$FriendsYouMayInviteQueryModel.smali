.class public final Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d7fc609
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2050867
    const-class v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2050866
    const-class v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2050864
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2050865
    return-void
.end method

.method private j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2050862
    iget-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->e:Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    iput-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->e:Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    .line 2050863
    iget-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->e:Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2050838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2050839
    invoke-direct {p0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2050840
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2050841
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2050842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2050843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2050854
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2050855
    invoke-direct {p0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2050856
    invoke-direct {p0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    .line 2050857
    invoke-direct {p0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2050858
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;

    .line 2050859
    iput-object v0, v1, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->e:Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    .line 2050860
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2050861
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2050853
    new-instance v0, LX/Dsq;

    invoke-direct {v0, p1}, LX/Dsq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2050852
    invoke-direct {p0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->j()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2050850
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2050851
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2050849
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2050846
    new-instance v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;-><init>()V

    .line 2050847
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2050848
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2050845
    const v0, -0x15e230dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2050844
    const v0, 0x25d6af

    return v0
.end method
