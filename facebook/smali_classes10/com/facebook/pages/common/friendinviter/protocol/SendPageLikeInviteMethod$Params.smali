.class public final Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2050939
    new-instance v0, LX/Dsv;

    invoke-direct {v0}, LX/Dsv;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2050940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050941
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->a:Ljava/lang/String;

    .line 2050942
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->b:Ljava/lang/String;

    .line 2050943
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2050944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050945
    iput-object p1, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->a:Ljava/lang/String;

    .line 2050946
    iput-object p2, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->b:Ljava/lang/String;

    .line 2050947
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2050948
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2050949
    iget-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2050950
    iget-object v0, p0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2050951
    return-void
.end method
