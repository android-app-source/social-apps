.class public Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# instance fields
.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2050637
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;

    const/16 v1, 0x271

    invoke-static {v2, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v2}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    const/16 p0, 0x12c4

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v2}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    iput-object v3, p1, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->u:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->v:LX/0aG;

    iput-object p0, p1, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->w:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->x:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2050614
    const-class v0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2050615
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2050616
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2050617
    if-eqz v0, :cond_0

    .line 2050618
    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->y:Ljava/lang/String;

    .line 2050619
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2050636
    const/4 v0, 0x1

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2050635
    const/4 v0, 0x1

    return v0
.end method

.method public final p()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2050630
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2050631
    new-instance v1, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;

    iget-object v2, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->y:Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x12c

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;-><init>(Ljava/lang/String;II)V

    .line 2050632
    const-string v2, "fetchFriendsYouMayInviteParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2050633
    iget-object v1, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->v:LX/0aG;

    const-string v2, "friends_you_may_invite"

    const v3, 0x51a981c0

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    move-object v0, v0

    .line 2050634
    new-instance v1, LX/Dsh;

    invoke-direct {v1, p0}, LX/Dsh;-><init>(Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;)V

    iget-object v2, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->x:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final y()V
    .locals 8

    .prologue
    .line 2050620
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v2

    .line 2050621
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2050622
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2050623
    iget-object v5, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->y:Ljava/lang/String;

    .line 2050624
    new-instance v6, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;

    invoke-direct {v6, v5, v0}, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2050625
    const-string v5, "sendPageLikeInviteParams"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2050626
    iget-object v5, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->v:LX/0aG;

    const-string v6, "send_page_like_invite"

    const v7, -0x51ea151a

    invoke-static {v5, v6, v4, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    move-object v4, v4

    .line 2050627
    iget-object v0, p0, Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v5, LX/Dsi;

    invoke-direct {v5, p0}, LX/Dsi;-><init>(Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;)V

    invoke-virtual {v0, v4, v5}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2050628
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2050629
    :cond_0
    return-void
.end method
