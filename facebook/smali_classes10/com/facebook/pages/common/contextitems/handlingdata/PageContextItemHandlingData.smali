.class public Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Z

.field public final c:Z

.field public d:I

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/1k1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Landroid/os/ParcelUuid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2050557
    new-instance v0, LX/Dsf;

    invoke-direct {v0}, LX/Dsf;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;LX/1k1;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;ZZ)V
    .locals 1

    .prologue
    .line 2050558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050559
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->d:I

    .line 2050560
    iput-wide p1, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    .line 2050561
    iput-object p3, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    .line 2050562
    iput-object p4, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    .line 2050563
    iput-object p5, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    .line 2050564
    iput-object p6, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    .line 2050565
    iput-object p7, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2050566
    iput-boolean p8, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->b:Z

    .line 2050567
    iput-boolean p9, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->c:Z

    .line 2050568
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2050544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050545
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->d:I

    .line 2050546
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    .line 2050547
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    .line 2050548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    .line 2050549
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->d:I

    .line 2050550
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->b:Z

    .line 2050551
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->c:Z

    .line 2050552
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/1k1;

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    .line 2050553
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    .line 2050554
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2050555
    const-class v0, Landroid/os/ParcelUuid;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->j:Landroid/os/ParcelUuid;

    .line 2050556
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2050543
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2050532
    iget-wide v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2050533
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2050534
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2050535
    iget v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2050536
    iget-boolean v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2050537
    iget-boolean v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2050538
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2050539
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2050540
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2050541
    iget-object v0, p0, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->j:Landroid/os/ParcelUuid;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2050542
    return-void
.end method
