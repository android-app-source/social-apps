.class public LX/Dcw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1fX;

.field private final b:LX/0Sh;

.field private final c:LX/2S5;

.field private final d:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mQueueLock"
    .end annotation
.end field

.field private volatile g:LX/Dcv;


# direct methods
.method public constructor <init>(LX/1fX;LX/0Sh;LX/2S5;Lcom/facebook/ui/media/attachments/MediaResourceHelper;)V
    .locals 1
    .param p1    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/Dcw;->e:Ljava/lang/Object;

    .line 2019115
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Dcw;->f:Ljava/util/Queue;

    .line 2019116
    sget-object v0, LX/Dcv;->OPEN:LX/Dcv;

    iput-object v0, p0, LX/Dcw;->g:LX/Dcv;

    .line 2019117
    iput-object p1, p0, LX/Dcw;->a:LX/1fX;

    .line 2019118
    iput-object p2, p0, LX/Dcw;->b:LX/0Sh;

    .line 2019119
    iput-object p3, p0, LX/Dcw;->c:LX/2S5;

    .line 2019120
    iput-object p4, p0, LX/Dcw;->d:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2019121
    return-void
.end method
