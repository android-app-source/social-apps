.class public final LX/EYj;
.super LX/EYh;
.source ""


# instance fields
.field public final k:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWp;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2138004
    invoke-direct {p0, p2, p3, p4}, LX/EYh;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 2138005
    iget-object v0, p0, LX/EYh;->a:Ljava/lang/Class;

    const-string v1, "newBuilder"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/EYj;->k:Ljava/lang/reflect/Method;

    .line 2138006
    return-void
.end method


# virtual methods
.method public final b(LX/EWj;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2138007
    iget-object v0, p0, LX/EYh;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138008
    :goto_0
    move-object v0, p2

    .line 2138009
    invoke-super {p0, p1, v0}, LX/EYh;->b(LX/EWj;Ljava/lang/Object;)V

    .line 2138010
    return-void

    :cond_0
    iget-object v0, p0, LX/EYj;->k:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWU;

    check-cast p2, LX/EWY;

    invoke-interface {v0, p2}, LX/EWU;->c(LX/EWY;)LX/EWU;

    move-result-object v0

    invoke-interface {v0}, LX/EWU;->i()LX/EWY;

    move-result-object p2

    goto :goto_0
.end method

.method public final newBuilder()LX/EWU;
    .locals 3

    .prologue
    .line 2138011
    iget-object v0, p0, LX/EYj;->k:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWU;

    return-object v0
.end method
