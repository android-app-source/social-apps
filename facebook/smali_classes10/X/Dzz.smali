.class public final LX/Dzz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinSearchQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/E00;


# direct methods
.method public constructor <init>(LX/E00;LX/0TF;)V
    .locals 0

    .prologue
    .line 2067695
    iput-object p1, p0, LX/Dzz;->b:LX/E00;

    iput-object p2, p0, LX/Dzz;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2067696
    iget-object v0, p0, LX/Dzz;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2067697
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2067698
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    .line 2067699
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2067700
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->b()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;

    .line 2067701
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2067702
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2067703
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2067704
    new-instance v1, LX/E02;

    invoke-direct {v1, v2}, LX/E02;-><init>(Ljava/util/List;)V

    .line 2067705
    iput-object v0, v1, LX/E02;->b:Ljava/lang/String;

    .line 2067706
    iget-object v0, p0, LX/Dzz;->a:LX/0TF;

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2067707
    return-void
.end method
