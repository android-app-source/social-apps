.class public LX/DVI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DVI;


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2004929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2004930
    iput-object p1, p0, LX/DVI;->a:Landroid/content/res/Resources;

    .line 2004931
    return-void
.end method

.method public static a(LX/0QB;)LX/DVI;
    .locals 4

    .prologue
    .line 2004932
    sget-object v0, LX/DVI;->b:LX/DVI;

    if-nez v0, :cond_1

    .line 2004933
    const-class v1, LX/DVI;

    monitor-enter v1

    .line 2004934
    :try_start_0
    sget-object v0, LX/DVI;->b:LX/DVI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2004935
    if-eqz v2, :cond_0

    .line 2004936
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2004937
    new-instance p0, LX/DVI;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/DVI;-><init>(Landroid/content/res/Resources;)V

    .line 2004938
    move-object v0, p0

    .line 2004939
    sput-object v0, LX/DVI;->b:LX/DVI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2004940
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2004941
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2004942
    :cond_1
    sget-object v0, LX/DVI;->b:LX/DVI;

    return-object v0

    .line 2004943
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2004944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
