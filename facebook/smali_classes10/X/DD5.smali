.class public final LX/DD5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/DD9;


# direct methods
.method public constructor <init>(LX/DD9;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 1974849
    iput-object p1, p0, LX/DD5;->c:LX/DD9;

    iput-object p2, p0, LX/DD5;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    iput-object p3, p0, LX/DD5;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1974850
    iget-object v0, p0, LX/DD5;->c:LX/DD9;

    iget-object v1, p0, LX/DD5;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DD5;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 1974851
    iget-object v4, v0, LX/DD9;->b:LX/0tX;

    .line 1974852
    new-instance v3, LX/80y;

    invoke-direct {v3}, LX/80y;-><init>()V

    move-object v3, v3

    .line 1974853
    const-string v5, "node_id"

    invoke-virtual {v3, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "gpymi_size_param"

    iget-object p0, v0, LX/DD9;->g:LX/0sa;

    invoke-virtual {p0}, LX/0sa;->h()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "after_param"

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/80y;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1974854
    new-instance v4, LX/DD7;

    invoke-direct {v4, v0}, LX/DD7;-><init>(LX/DD9;)V

    iget-object v5, v0, LX/DD9;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1974855
    return-object v0
.end method
