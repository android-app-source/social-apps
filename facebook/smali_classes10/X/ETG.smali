.class public LX/ETG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CfG;


# instance fields
.field public final a:LX/0Sh;

.field private final b:Ljava/lang/String;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/CfK;

.field public final e:LX/CfW;

.field public final f:LX/2iz;

.field private final g:Ljava/lang/String;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/ReactionUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0xX;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0JZ;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field private m:Z

.field private n:LX/0ho;

.field public o:LX/CfJ;

.field public p:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$VideoHomeDataFetchingListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/lang/String;LX/0Or;LX/CfK;LX/CfW;LX/2iz;LX/0Ot;LX/0Ot;LX/0xX;LX/0Ot;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/video/videohome/data/IsVideoHomeDataFetchToastEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/CfK;",
            "LX/CfW;",
            "LX/2iz;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/ReactionUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0xX;",
            "LX/0Ot",
            "<",
            "LX/0JZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2124577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124578
    const-string v0, "VIDEO_HOME"

    iput-object v0, p0, LX/ETG;->g:Ljava/lang/String;

    .line 2124579
    iput-object p1, p0, LX/ETG;->a:LX/0Sh;

    .line 2124580
    iput-object p2, p0, LX/ETG;->b:Ljava/lang/String;

    .line 2124581
    iput-object p3, p0, LX/ETG;->c:LX/0Or;

    .line 2124582
    iput-object p4, p0, LX/ETG;->d:LX/CfK;

    .line 2124583
    iput-object p5, p0, LX/ETG;->e:LX/CfW;

    .line 2124584
    iput-object p6, p0, LX/ETG;->f:LX/2iz;

    .line 2124585
    iput-object p7, p0, LX/ETG;->h:LX/0Ot;

    .line 2124586
    iput-object p8, p0, LX/ETG;->i:LX/0Ot;

    .line 2124587
    iput-object p9, p0, LX/ETG;->j:LX/0xX;

    .line 2124588
    iput-object p10, p0, LX/ETG;->k:LX/0Ot;

    .line 2124589
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ETG;->q:Ljava/util/List;

    .line 2124590
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9qT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2124539
    iget-object v0, p0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ET2;

    .line 2124540
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v2}, LX/ETB;->l()V

    .line 2124541
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v2}, LX/ETB;->k()V

    .line 2124542
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_0

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9qT;

    .line 2124543
    iget-object v5, v0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v5, v2}, LX/ETB;->a(LX/9qT;)V

    .line 2124544
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2124545
    :cond_0
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v2}, LX/ETB;->m()V

    .line 2124546
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    iget-object v2, v2, LX/ETB;->w:LX/ETH;

    invoke-virtual {v2}, LX/ETH;->c()V

    .line 2124547
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    .line 2124548
    iget-object v3, v2, LX/ETB;->r:LX/19j;

    iget-boolean v3, v3, LX/19j;->aT:Z

    if-nez v3, :cond_5

    .line 2124549
    iget-object v3, v2, LX/ETB;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2xj;

    invoke-virtual {v3}, LX/2xj;->i()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2124550
    :goto_2
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    .line 2124551
    iget-object v6, v2, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EUW;

    .line 2124552
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2124553
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 2124554
    iput-wide v10, v8, LX/ETy;->I:J

    .line 2124555
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    invoke-virtual {v8}, LX/2zB;->a()V

    .line 2124556
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V

    .line 2124557
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {v8}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 2124558
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->az:LX/0fu;

    if-eqz v8, :cond_1

    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v8, :cond_1

    .line 2124559
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    iget-object v9, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v9, v9, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->az:LX/0fu;

    invoke-virtual {v8, v9}, LX/0g7;->b(LX/0fu;)V

    .line 2124560
    :cond_1
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-boolean v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v8, :cond_2

    .line 2124561
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    .line 2124562
    iget-object v9, v8, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v10, 0x1d0009

    const/16 v11, 0x1f

    invoke-interface {v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2124563
    :cond_2
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {v8}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->U(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 2124564
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-virtual {v8}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k()V

    .line 2124565
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {v8}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 2124566
    iget-object v8, v6, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v8, v8, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 2124567
    iget-object v9, v8, LX/ESm;->b:LX/1Aa;

    move-object v8, v9

    .line 2124568
    invoke-virtual {v8}, LX/1Aa;->f()V

    .line 2124569
    goto :goto_3

    .line 2124570
    :cond_3
    goto/16 :goto_0

    .line 2124571
    :cond_4
    return-void

    .line 2124572
    :cond_5
    iget-object v3, v2, LX/ETB;->r:LX/19j;

    iget v4, v3, LX/19j;->aR:I

    .line 2124573
    sget-object v5, LX/379;->VIDEO_HOME_OCCLUSION:LX/379;

    .line 2124574
    iget-object v3, v2, LX/ETB;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    .line 2124575
    iget-object v6, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->h:Landroid/os/Handler;

    new-instance v7, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$2;

    invoke-direct {v7, v3, p1, v4, v5}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$2;-><init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/0Px;ILX/379;)V

    const v2, -0x2bac3a29

    invoke-static {v6, v7, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2124576
    goto/16 :goto_2
.end method

.method private static a(Lcom/facebook/reaction/ReactionQueryParams;LX/ETD;)V
    .locals 3

    .prologue
    .line 2124526
    iget v0, p1, LX/ETD;->c:I

    move v0, v0

    .line 2124527
    int-to-long v0, v0

    .line 2124528
    iput-wide v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2124529
    move-object v0, p0

    .line 2124530
    iget-object v1, p1, LX/ETD;->b:LX/2rJ;

    move-object v1, v1

    .line 2124531
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2124532
    sget-object v2, LX/ETF;->a:[I

    invoke-virtual {v1}, LX/2rJ;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 2124533
    const-string v2, "normal"

    :goto_0
    move-object v1, v2

    .line 2124534
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    .line 2124535
    return-void

    .line 2124536
    :pswitch_0
    const-string v2, "video_home_perceived_perf_data"

    goto :goto_0

    .line 2124537
    :pswitch_1
    const-string v2, "prefetch"

    goto :goto_0

    .line 2124538
    :pswitch_2
    const-string v2, "pull_to_refresh"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 2124522
    iget-object v0, p0, LX/ETG;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124523
    iget-object v0, p0, LX/ETG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "VideoHome data fetch "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    const-string v1, "succeed"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2124524
    :cond_0
    return-void

    .line 2124525
    :cond_1
    const-string v1, "fail"

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/ETG;
    .locals 11

    .prologue
    .line 2124417
    new-instance v0, LX/ETG;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/16 v3, 0x159a

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const-class v4, LX/CfK;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/CfK;

    invoke-static {p0}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v5

    check-cast v5, LX/CfW;

    invoke-static {p0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v6

    check-cast v6, LX/2iz;

    const/16 v7, 0x1064

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x12c4

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v9

    check-cast v9, LX/0xX;

    const/16 v10, 0x37b2

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, LX/ETG;-><init>(LX/0Sh;Ljava/lang/String;LX/0Or;LX/CfK;LX/CfW;LX/2iz;LX/0Ot;LX/0Ot;LX/0xX;LX/0Ot;)V

    .line 2124418
    return-object v0
.end method

.method public static b(LX/ETG;LX/ETD;LX/0ho;)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    .line 2124490
    iget-object v1, p0, LX/ETG;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2124491
    iget-object v1, p0, LX/ETG;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance v2, LX/27k;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "VideoHome data fetch. Reason: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2124492
    iget-object v4, p1, LX/ETD;->b:LX/2rJ;

    move-object v4, v4

    .line 2124493
    invoke-virtual {v4}, LX/2rJ;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2124494
    :cond_0
    iput-object p2, p0, LX/ETG;->n:LX/0ho;

    .line 2124495
    iput-boolean v0, p0, LX/ETG;->l:Z

    .line 2124496
    iput-boolean v0, p0, LX/ETG;->m:Z

    .line 2124497
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    if-nez v0, :cond_1

    .line 2124498
    sget-object v2, LX/2rJ;->CACHED_SECTION:LX/2rJ;

    .line 2124499
    iget-object v3, p1, LX/ETD;->b:LX/2rJ;

    move-object v3, v3

    .line 2124500
    invoke-virtual {v2, v3}, LX/2rJ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2124501
    new-instance v5, LX/ETE;

    invoke-direct {v5, p0, p0}, LX/ETE;-><init>(LX/ETG;LX/CfG;)V

    .line 2124502
    iget-object v6, p0, LX/ETG;->d:LX/CfK;

    invoke-virtual {v6, p0, v5}, LX/CfK;->a(LX/CfG;LX/CfI;)LX/CfJ;

    move-result-object v5

    iput-object v5, p0, LX/ETG;->o:LX/CfJ;

    .line 2124503
    iget-object v5, p0, LX/ETG;->o:LX/CfJ;

    invoke-static {p0, p1}, LX/ETG;->e(LX/ETG;LX/ETD;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v6

    .line 2124504
    new-instance v7, LX/ETC;

    invoke-direct {v7}, LX/ETC;-><init>()V

    .line 2124505
    iget-object v8, p1, LX/ETD;->b:LX/2rJ;

    move-object v8, v8

    .line 2124506
    iput-object v8, v7, LX/ETC;->a:LX/2rJ;

    .line 2124507
    iget v8, p1, LX/ETD;->c:I

    move v8, v8

    .line 2124508
    iput v8, v7, LX/ETC;->b:I

    .line 2124509
    move-object v7, v7

    .line 2124510
    sget-object v8, LX/2rJ;->NORMAL:LX/2rJ;

    .line 2124511
    iput-object v8, v7, LX/ETC;->a:LX/2rJ;

    .line 2124512
    move-object v7, v7

    .line 2124513
    invoke-virtual {v7}, LX/ETC;->a()LX/ETD;

    move-result-object v7

    invoke-static {p0, v7}, LX/ETG;->e(LX/ETG;LX/ETD;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v7

    const-string v8, "VIDEO_HOME"

    sget v9, LX/0xX;->a:I

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, LX/CfJ;->a(Lcom/facebook/reaction/ReactionQueryParams;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/Long;Z)LX/2jY;

    move-result-object v5

    iput-object v5, p0, LX/ETG;->p:LX/2jY;

    .line 2124514
    iget-object v5, p0, LX/ETG;->p:LX/2jY;

    iget-object v6, p0, LX/ETG;->p:LX/2jY;

    .line 2124515
    new-instance v7, Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$3;

    invoke-direct {v7, p0, v6}, Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$3;-><init>(LX/ETG;LX/2jY;)V

    move-object v6, v7

    .line 2124516
    iput-object v6, v5, LX/2jY;->A:Ljava/lang/Runnable;

    .line 2124517
    :goto_0
    return-void

    .line 2124518
    :cond_1
    invoke-static {p0, p1}, LX/ETG;->f(LX/ETG;LX/ETD;)V

    .line 2124519
    iget-object v0, p0, LX/ETG;->f:LX/2iz;

    invoke-static {p0}, LX/ETG;->j(LX/ETG;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iz;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 2124520
    :cond_2
    iget-object v2, p0, LX/ETG;->e:LX/CfW;

    const-string v3, "VIDEO_HOME"

    invoke-static {p0, p1}, LX/ETG;->e(LX/ETG;LX/ETD;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v2

    iput-object v2, p0, LX/ETG;->p:LX/2jY;

    .line 2124521
    iget-object v2, p0, LX/ETG;->p:LX/2jY;

    invoke-virtual {v2, p0}, LX/2jY;->a(LX/CfG;)V

    goto :goto_0
.end method

.method public static e(LX/ETG;LX/ETD;)Lcom/facebook/reaction/ReactionQueryParams;
    .locals 4

    .prologue
    .line 2124482
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-object v1, p0, LX/ETG;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2124483
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2124484
    move-object v0, v0

    .line 2124485
    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    .line 2124486
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 2124487
    move-object v0, v0

    .line 2124488
    invoke-static {v0, p1}, LX/ETG;->a(Lcom/facebook/reaction/ReactionQueryParams;LX/ETD;)V

    .line 2124489
    return-object v0
.end method

.method public static f(LX/ETG;LX/ETD;)V
    .locals 2

    .prologue
    .line 2124469
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    .line 2124470
    iget-object v1, v0, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v1

    .line 2124471
    if-nez v0, :cond_1

    .line 2124472
    :cond_0
    :goto_0
    return-void

    .line 2124473
    :cond_1
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    .line 2124474
    iget-object v1, v0, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v1

    .line 2124475
    invoke-static {v0, p1}, LX/ETG;->a(Lcom/facebook/reaction/ReactionQueryParams;LX/ETD;)V

    .line 2124476
    sget-object v0, LX/2rJ;->PAGINATION:LX/2rJ;

    .line 2124477
    iget-object v1, p1, LX/ETD;->b:LX/2rJ;

    move-object v1, v1

    .line 2124478
    if-eq v0, v1, :cond_0

    .line 2124479
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    const/4 v1, 0x0

    .line 2124480
    iput-boolean v1, v0, LX/2jY;->p:Z

    .line 2124481
    goto :goto_0
.end method

.method private h()V
    .locals 5

    .prologue
    .line 2124591
    iget-object v0, p0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ET2;

    .line 2124592
    iget-object v2, v0, LX/ET2;->a:LX/ETB;

    .line 2124593
    iget-object v3, v2, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EUW;

    .line 2124594
    const/4 v2, 0x0

    .line 2124595
    iget-object p0, v3, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {p0, v2}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V

    .line 2124596
    iget-object p0, v3, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->U(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 2124597
    iget-object p0, v3, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    const/4 v0, 0x1

    .line 2124598
    iput-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->av:Z

    .line 2124599
    iget-object p0, v3, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 2124600
    iget-object p0, v3, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object p0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    invoke-virtual {p0, v2}, LX/0JZ;->a(Z)V

    .line 2124601
    goto :goto_1

    .line 2124602
    :cond_0
    goto :goto_0

    .line 2124603
    :cond_1
    return-void
.end method

.method public static j(LX/ETG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2124466
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    if-nez v0, :cond_0

    const-string v0, "NO_SESSION_ID"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    .line 2124467
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2124468
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9qT;)V
    .locals 2

    .prologue
    .line 2124463
    iget-object v0, p0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ET2;

    .line 2124464
    invoke-virtual {v0, p1}, LX/ET2;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 2124465
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;LX/0Ve;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2124460
    iget-object v0, p0, LX/ETG;->p:LX/2jY;

    if-nez v0, :cond_0

    .line 2124461
    :goto_0
    return-void

    .line 2124462
    :cond_0
    iget-object v0, p0, LX/ETG;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ReactionUtil;

    iget-object v5, p0, LX/ETG;->p:LX/2jY;

    move-object v1, p1

    move-object v2, p4

    move v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;LX/0Ve;ILjava/lang/String;LX/2jY;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2124459
    return-void
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2124458
    iget-boolean v0, p0, LX/ETG;->m:Z

    return v0
.end method

.method public final kK_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2124452
    iput-boolean v1, p0, LX/ETG;->l:Z

    .line 2124453
    iget-object v0, p0, LX/ETG;->n:LX/0ho;

    if-eqz v0, :cond_0

    .line 2124454
    const/4 v0, 0x0

    iput-object v0, p0, LX/ETG;->n:LX/0ho;

    .line 2124455
    :cond_0
    invoke-direct {p0}, LX/ETG;->h()V

    .line 2124456
    invoke-direct {p0, v1}, LX/ETG;->a(Z)V

    .line 2124457
    return-void
.end method

.method public final kL_()V
    .locals 15

    .prologue
    const/4 v0, 0x0

    .line 2124423
    iput-boolean v0, p0, LX/ETG;->l:Z

    .line 2124424
    iput-boolean v0, p0, LX/ETG;->m:Z

    .line 2124425
    iget-object v1, p0, LX/ETG;->p:LX/2jY;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ETG;->p:LX/2jY;

    invoke-virtual {v1}, LX/2jY;->z()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2124426
    :cond_0
    invoke-direct {p0}, LX/ETG;->h()V

    .line 2124427
    :goto_0
    return-void

    .line 2124428
    :cond_1
    iget-object v1, p0, LX/ETG;->n:LX/0ho;

    if-eqz v1, :cond_3

    .line 2124429
    iget-object v1, p0, LX/ETG;->n:LX/0ho;

    const/4 v3, 0x0

    .line 2124430
    iget-object v2, p0, LX/ETG;->j:LX/0xX;

    sget-object v4, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v2, v4}, LX/0xX;->a(LX/1vy;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/ETG;->p:LX/2jY;

    if-nez v2, :cond_5

    :cond_2
    move v2, v3

    .line 2124431
    :goto_1
    move v2, v2

    .line 2124432
    invoke-interface {v1, v2}, LX/0ho;->a(I)V

    .line 2124433
    const/4 v1, 0x0

    iput-object v1, p0, LX/ETG;->n:LX/0ho;

    .line 2124434
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2124435
    iget-object v1, p0, LX/ETG;->p:LX/2jY;

    invoke-virtual {v1}, LX/2jY;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2124436
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2124437
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2124438
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ETG;->a(LX/0Px;)V

    .line 2124439
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/ETG;->a(Z)V

    goto :goto_0

    .line 2124440
    :cond_5
    iget-object v2, p0, LX/ETG;->p:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->o()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v3

    :goto_3
    if-ge v6, v8, :cond_9

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9qT;

    .line 2124441
    invoke-interface {v2}, LX/9qT;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v3

    :goto_4
    if-ge v5, v10, :cond_8

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2124442
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2124443
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v11

    .line 2124444
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v4, v3

    .line 2124445
    :goto_5
    if-ge v4, v12, :cond_7

    invoke-virtual {v11, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2124446
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v13

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v13, v14, :cond_6

    .line 2124447
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->ac()I

    move-result v2

    goto :goto_1

    .line 2124448
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 2124449
    :cond_7
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 2124450
    :cond_8
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    :cond_9
    move v2, v3

    .line 2124451
    goto/16 :goto_1
.end method

.method public final kU_()V
    .locals 2

    .prologue
    .line 2124419
    iget-object v0, p0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ET2;

    .line 2124420
    iget-object p0, v0, LX/ET2;->a:LX/ETB;

    invoke-static {p0}, LX/ETB;->u(LX/ETB;)V

    .line 2124421
    goto :goto_0

    .line 2124422
    :cond_0
    return-void
.end method
