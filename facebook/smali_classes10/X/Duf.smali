.class public LX/Duf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/5fv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/5fv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057536
    iput-object p1, p0, LX/Duf;->a:Landroid/content/Context;

    .line 2057537
    iput-object p2, p0, LX/Duf;->b:LX/5fv;

    .line 2057538
    return-void
.end method

.method public static a(LX/0QB;)LX/Duf;
    .locals 5

    .prologue
    .line 2057539
    const-class v1, LX/Duf;

    monitor-enter v1

    .line 2057540
    :try_start_0
    sget-object v0, LX/Duf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2057541
    sput-object v2, LX/Duf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2057542
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057543
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2057544
    new-instance p0, LX/Duf;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v4

    check-cast v4, LX/5fv;

    invoke-direct {p0, v3, v4}, LX/Duf;-><init>(Landroid/content/Context;LX/5fv;)V

    .line 2057545
    move-object v0, p0

    .line 2057546
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2057547
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Duf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2057548
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2057549
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2057550
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->OUT_OF_STOCK:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    if-ne v0, v1, :cond_0

    const-string v0, "(%s) %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Duf;->a:Landroid/content/Context;

    const v4, 0x7f082d0f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2057551
    invoke-virtual {p0, p1}, LX/Duf;->c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v0

    .line 2057552
    iget-object v1, p0, LX/Duf;->a:Landroid/content/Context;

    const v2, 0x7f082d10

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2057553
    iget-object v0, p0, LX/Duf;->b:LX/5fv;

    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyAmountModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyAmountModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->e()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyAmountModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyAmountModel;->a()I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sget-object v2, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v0, v1, v2}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
