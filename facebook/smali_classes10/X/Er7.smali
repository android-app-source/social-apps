.class public final LX/Er7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field public final synthetic a:LX/Er9;


# direct methods
.method public constructor <init>(LX/Er9;)V
    .locals 0

    .prologue
    .line 2172447
    iput-object p1, p0, LX/Er7;->a:LX/Er9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 2172448
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->E:LX/3kp;

    .line 2172449
    iput v2, v0, LX/3kp;->b:I

    .line 2172450
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->E:LX/3kp;

    sget-object v1, LX/7vb;->e:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2172451
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->E:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    .line 2172452
    iget-object v1, v0, LX/Er9;->z:LX/0iA;

    sget-object v3, LX/EqO;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v4, LX/EqO;

    invoke-virtual {v1, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/EqO;

    .line 2172453
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/EqO;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "4157"

    invoke-static {v1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2172454
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-boolean v0, v0, LX/Er9;->c:Z

    if-nez v0, :cond_0

    .line 2172455
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/Er7;->a:LX/Er9;

    iget-object v1, v1, LX/Er9;->g:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2172456
    const v1, 0x7f081eb1

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2172457
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2172458
    const/4 v1, -0x1

    .line 2172459
    iput v1, v0, LX/0hs;->t:I

    .line 2172460
    const v1, 0x7f0d0f38

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2172461
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    const/4 v1, 0x1

    .line 2172462
    iput-boolean v1, v0, LX/Er9;->c:Z

    .line 2172463
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->E:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2172464
    iget-object v0, p0, LX/Er7;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->z:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4157"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2172465
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2172466
    return-void
.end method
