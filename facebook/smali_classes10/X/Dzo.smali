.class public final LX/Dzo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2067179
    iput-object p1, p0, LX/Dzo;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067180
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dzo;->b:Ljava/lang/String;

    .line 2067181
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2067175
    iget-object v0, p0, LX/Dzo;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p1}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2067176
    iget-object v0, p0, LX/Dzo;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067177
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dzo;->b:Ljava/lang/String;

    .line 2067178
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2067174
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2067173
    return-void
.end method
