.class public final LX/ET7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETB;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/ETB;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2123938
    iput-object p1, p0, LX/ET7;->a:LX/ETB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2123939
    iput-object p2, p0, LX/ET7;->b:Ljava/lang/String;

    .line 2123940
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2123941
    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    iget-object v0, v0, LX/ETB;->I:LX/ETU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    iget-object v0, v0, LX/ETB;->I:LX/ETU;

    .line 2123942
    iget-object v1, v0, LX/ETU;->e:Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-object v0, v1

    .line 2123943
    if-eqz v0, :cond_0

    .line 2123944
    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    iget-object v0, v0, LX/ETB;->t:LX/ETQ;

    iget-object v1, p0, LX/ET7;->a:LX/ETB;

    iget-object v1, v1, LX/ETB;->I:LX/ETU;

    .line 2123945
    iget-object p0, v1, LX/ETU;->e:Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-object v1, p0

    .line 2123946
    invoke-virtual {v0, v1}, LX/ETQ;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    .line 2123947
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2123948
    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    const/4 v1, 0x0

    .line 2123949
    iput-boolean v1, v0, LX/ETB;->H:Z

    .line 2123950
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2123951
    invoke-direct {p0}, LX/ET7;->b()V

    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2123952
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2123953
    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    iget-object v0, v0, LX/ETB;->I:LX/ETU;

    if-nez v0, :cond_1

    .line 2123954
    :cond_0
    :goto_0
    return-void

    .line 2123955
    :cond_1
    invoke-static {p1}, LX/ETB;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    .line 2123956
    if-eqz v0, :cond_0

    .line 2123957
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v1

    .line 2123958
    iget-object v2, p0, LX/ET7;->a:LX/ETB;

    iget-object v2, v2, LX/ETB;->I:LX/ETU;

    .line 2123959
    iget-object v3, v2, LX/ETU;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2123960
    iget-object v3, p0, LX/ET7;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2123961
    iget-object v2, p0, LX/ET7;->a:LX/ETB;

    iget-object v2, v2, LX/ETB;->I:LX/ETU;

    .line 2123962
    iput-object v1, v2, LX/ETU;->d:LX/0us;

    .line 2123963
    :cond_2
    new-instance v2, LX/9qr;

    invoke-direct {v2}, LX/9qr;-><init>()V

    iget-object v3, p0, LX/ET7;->a:LX/ETB;

    iget-object v3, v3, LX/ETB;->I:LX/ETU;

    .line 2123964
    iget-object p1, v3, LX/ETU;->a:Ljava/lang/String;

    move-object v3, p1

    .line 2123965
    iput-object v3, v2, LX/9qr;->d:Ljava/lang/String;

    .line 2123966
    move-object v2, v2

    .line 2123967
    iget-object v3, p0, LX/ET7;->a:LX/ETB;

    iget-object v3, v3, LX/ETB;->I:LX/ETU;

    .line 2123968
    iget-object p1, v3, LX/ETU;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2123969
    iput-object v3, v2, LX/9qr;->m:Ljava/lang/String;

    .line 2123970
    move-object v2, v2

    .line 2123971
    invoke-virtual {v2}, LX/9qr;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    .line 2123972
    iget-object v3, p0, LX/ET7;->a:LX/ETB;

    .line 2123973
    invoke-static {v3, v0}, LX/ETB;->a(LX/ETB;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2123974
    const/4 v4, 0x0

    .line 2123975
    :goto_1
    move-object v4, v4

    .line 2123976
    if-nez v4, :cond_5

    .line 2123977
    const/4 v4, 0x0

    .line 2123978
    :goto_2
    move v0, v4

    .line 2123979
    if-eqz v0, :cond_4

    .line 2123980
    invoke-static {v1}, LX/ETB;->b(LX/0us;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2123981
    invoke-direct {p0}, LX/ET7;->a()V

    .line 2123982
    :cond_3
    iget-object v0, p0, LX/ET7;->a:LX/ETB;

    invoke-static {v0}, LX/ETB;->u(LX/ETB;)V

    .line 2123983
    :cond_4
    invoke-direct {p0}, LX/ET7;->b()V

    goto :goto_0

    :cond_5
    iget-object v5, v3, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    .line 2123984
    move p1, v3

    :goto_3
    invoke-virtual {v5}, LX/ETQ;->size()I

    move-result v2

    if-ge p1, v2, :cond_6

    .line 2123985
    invoke-virtual {v5, p1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v2

    .line 2123986
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2123987
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v5}, LX/ETQ;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 2123988
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v5, v2}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v2

    .line 2123989
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2123990
    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v5, p1, v4}, LX/ETQ;->a(ILjava/util/Collection;)Z

    move-result v3

    .line 2123991
    :cond_6
    :goto_4
    move v4, v3

    .line 2123992
    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Ljava/util/List;

    move-result-object v4

    goto :goto_1

    .line 2123993
    :cond_8
    invoke-virtual {v5, v4}, LX/ETQ;->addAll(Ljava/util/Collection;)Z

    move-result v3

    goto :goto_4

    .line 2123994
    :cond_9
    add-int/lit8 p1, p1, 0x1

    goto :goto_3
.end method
