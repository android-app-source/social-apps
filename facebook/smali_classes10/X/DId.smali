.class public final LX/DId;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1983876
    const-class v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel;

    const v0, 0x57de5cfe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "WeatherPermalinkQuery"

    const-string v6, "588f8b00f8c4a226442436da4aeb748d"

    const-string v7, "viewer"

    const-string v8, "10155255659631729"

    const-string v9, "10155259090651729"

    .line 1983877
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1983878
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1983879
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1983880
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1983881
    sparse-switch v0, :sswitch_data_0

    .line 1983882
    :goto_0
    return-object p1

    .line 1983883
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1983884
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1983885
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1983886
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1983887
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1983888
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1983889
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1983890
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1983891
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1983892
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1983893
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_a
        -0x680de62a -> :sswitch_3
        -0x6326fdb3 -> :sswitch_8
        -0x4496acc9 -> :sswitch_6
        -0x25a646c8 -> :sswitch_2
        -0x1b87b280 -> :sswitch_5
        -0x14283bca -> :sswitch_0
        -0x12efdeb3 -> :sswitch_4
        0xa1fa812 -> :sswitch_1
        0x214100e0 -> :sswitch_7
        0x73a026b5 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1983894
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1983895
    :goto_1
    return v0

    .line 1983896
    :pswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1983897
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
