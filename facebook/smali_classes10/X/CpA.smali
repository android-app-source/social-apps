.class public final LX/CpA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/CpB;


# direct methods
.method public constructor <init>(LX/CpB;)V
    .locals 0

    .prologue
    .line 1936619
    iput-object p1, p0, LX/CpA;->a:LX/CpB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1936620
    iget-object v0, p0, LX/CpA;->a:LX/CpB;

    iget-object v0, v0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1936621
    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    .line 1936622
    iget-object v0, p0, LX/CpA;->a:LX/CpB;

    iget-object v0, v0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936623
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936624
    iget-object v1, p0, LX/CpA;->a:LX/CpB;

    iget-object v1, v1, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936625
    iget-object v0, p0, LX/CpA;->a:LX/CpB;

    iget-object v0, v0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081c77

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/CpA;->a:LX/CpB;

    iget-object v1, v1, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1936626
    iget-object v1, p0, LX/CpA;->a:LX/CpB;

    iget-object v1, v1, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936627
    iget-object p0, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, p0

    .line 1936628
    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936629
    const/4 v0, 0x1

    return v0
.end method
