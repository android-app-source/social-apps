.class public final LX/DsK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 0

    .prologue
    .line 2050346
    iput-object p1, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2050347
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2050348
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const/4 v1, 0x1

    .line 2050349
    iput-boolean v1, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    .line 2050350
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const/4 v1, 0x0

    .line 2050351
    iput-boolean v1, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->ad:Z

    .line 2050352
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2050353
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DsJ;

    invoke-direct {v2, p0}, LX/DsJ;-><init>(LX/DsK;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2050354
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2050355
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2050356
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const/4 v1, 0x0

    .line 2050357
    iput-boolean v1, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->ad:Z

    .line 2050358
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2050359
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->u()I

    move-result v0

    if-lez v0, :cond_0

    .line 2050360
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->b(Landroid/view/View;)V

    .line 2050361
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 2050362
    const/4 v1, 0x2

    .line 2050363
    if-eqz v0, :cond_5

    .line 2050364
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    .line 2050365
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2050366
    iget-object v2, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const/4 v3, 0x1

    .line 2050367
    iput-boolean v3, v2, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    .line 2050368
    :cond_1
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v2

    .line 2050369
    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-ne v0, v2, :cond_4

    .line 2050370
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2050371
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    .line 2050372
    if-eqz v0, :cond_2

    .line 2050373
    const/16 v2, 0x64

    invoke-virtual {v0, v2}, LX/0k5;->b(I)LX/0k9;

    move-result-object v0

    .line 2050374
    check-cast v0, Lcom/facebook/notifications/loader/NotificationsLoader;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2050375
    iput v2, v0, Lcom/facebook/notifications/loader/NotificationsLoader;->x:I

    .line 2050376
    :cond_2
    iget-object v2, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v1}, LX/3T7;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, LX/3T7;->a()LX/0Px;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/util/List;

    .line 2050377
    iput-object v0, v2, Lcom/facebook/notifications/widget/NotificationsFragment;->ao:Ljava/util/List;

    .line 2050378
    iget-object v0, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->i(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 2050379
    const/16 v0, 0x1a

    .line 2050380
    :goto_1
    iget-object v1, p0, LX/DsK;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000d

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2050381
    return-void

    .line 2050382
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 2050383
    :cond_4
    const/16 v0, 0x1b

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1
.end method
