.class public LX/EtE;
.super LX/EtA;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EtA",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:LX/1Er;

.field public final d:LX/9iU;

.field private final e:J

.field private final f:J

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Landroid/content/Context;

.field private final l:Landroid/net/Uri;

.field private final m:Landroid/net/Uri;

.field private final n:Ljava/lang/String;

.field private final o:Lcom/facebook/common/callercontext/CallerContext;

.field private final p:LX/1Fs;

.field private final q:LX/EsA;

.field private final r:LX/0if;

.field private final s:LX/EtG;

.field private final t:LX/EtH;

.field private final u:LX/EtD;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1Er;LX/9iU;Lcom/facebook/common/callercontext/CallerContext;LX/1Fs;LX/0if;LX/EsA;)V
    .locals 10

    .prologue
    .line 2177426
    invoke-direct {p0}, LX/EtA;-><init>()V

    .line 2177427
    const-wide/16 v2, 0x96

    iput-wide v2, p0, LX/EtE;->e:J

    .line 2177428
    const-wide/16 v2, 0x1388

    iput-wide v2, p0, LX/EtE;->f:J

    .line 2177429
    const-string v2, "flow_succeed"

    iput-object v2, p0, LX/EtE;->g:Ljava/lang/String;

    .line 2177430
    const-string v2, "flow_fail"

    iput-object v2, p0, LX/EtE;->h:Ljava/lang/String;

    .line 2177431
    const-string v2, "flow_cancel"

    iput-object v2, p0, LX/EtE;->i:Ljava/lang/String;

    .line 2177432
    const-string v2, "flow_signal"

    iput-object v2, p0, LX/EtE;->j:Ljava/lang/String;

    .line 2177433
    iput-object p1, p0, LX/EtE;->k:Landroid/content/Context;

    .line 2177434
    iput-object p2, p0, LX/EtE;->l:Landroid/net/Uri;

    .line 2177435
    iput-object p3, p0, LX/EtE;->m:Landroid/net/Uri;

    .line 2177436
    iput-object p4, p0, LX/EtE;->n:Ljava/lang/String;

    .line 2177437
    move-object/from16 v0, p10

    iput-object v0, p0, LX/EtE;->q:LX/EsA;

    .line 2177438
    iput-object p5, p0, LX/EtE;->c:LX/1Er;

    .line 2177439
    move-object/from16 v0, p6

    iput-object v0, p0, LX/EtE;->d:LX/9iU;

    .line 2177440
    move-object/from16 v0, p7

    iput-object v0, p0, LX/EtE;->o:Lcom/facebook/common/callercontext/CallerContext;

    .line 2177441
    move-object/from16 v0, p8

    iput-object v0, p0, LX/EtE;->p:LX/1Fs;

    .line 2177442
    move-object/from16 v0, p9

    iput-object v0, p0, LX/EtE;->r:LX/0if;

    .line 2177443
    new-instance v2, LX/EtG;

    iget-object v4, p0, LX/EtE;->k:Landroid/content/Context;

    iget-object v5, p0, LX/EtE;->n:Ljava/lang/String;

    const-wide/16 v6, 0x96

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, LX/EtG;-><init>(LX/EtA;Landroid/content/Context;Ljava/lang/String;J)V

    iput-object v2, p0, LX/EtE;->s:LX/EtG;

    .line 2177444
    iget-object v2, p0, LX/EtA;->b:Ljava/util/List;

    iget-object v3, p0, LX/EtE;->s:LX/EtG;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2177445
    new-instance v2, LX/EtH;

    const-wide/16 v4, 0x1388

    invoke-direct {v2, p0, v4, v5}, LX/EtH;-><init>(LX/EtA;J)V

    iput-object v2, p0, LX/EtE;->t:LX/EtH;

    .line 2177446
    iget-object v2, p0, LX/EtA;->b:Ljava/util/List;

    iget-object v3, p0, LX/EtE;->t:LX/EtH;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2177447
    new-instance v2, LX/EtD;

    iget-object v4, p0, LX/EtE;->c:LX/1Er;

    iget-object v5, p0, LX/EtE;->d:LX/9iU;

    iget-object v3, p0, LX/EtE;->p:LX/1Fs;

    invoke-virtual {v3}, LX/1Fs;->d()Ljava/util/concurrent/Executor;

    move-result-object v6

    iget-object v7, p0, LX/EtE;->l:Landroid/net/Uri;

    iget-object v8, p0, LX/EtE;->m:Landroid/net/Uri;

    iget-object v9, p0, LX/EtE;->o:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, LX/EtD;-><init>(LX/EtA;LX/1Er;LX/9iU;Ljava/util/concurrent/Executor;Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    iput-object v2, p0, LX/EtE;->u:LX/EtD;

    .line 2177448
    iget-object v2, p0, LX/EtA;->a:Ljava/util/List;

    iget-object v3, p0, LX/EtE;->u:LX/EtD;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2177449
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2177423
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2177424
    invoke-super {p0}, LX/EtA;->a()V

    .line 2177425
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2177450
    check-cast p1, Landroid/net/Uri;

    .line 2177451
    if-nez p1, :cond_0

    .line 2177452
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EtA;->d(Ljava/lang/Object;)V

    .line 2177453
    :goto_0
    return-void

    .line 2177454
    :cond_0
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    const-string v2, "flow_succeed"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177455
    invoke-virtual {p0}, LX/EtA;->g()V

    .line 2177456
    invoke-virtual {p0}, LX/EtA;->h()V

    .line 2177457
    iget-object v0, p0, LX/EtE;->q:LX/EsA;

    .line 2177458
    iget-object v3, v0, LX/EsA;->d:LX/EsB;

    iget-object v3, v3, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->f:LX/1Cn;

    const-string v4, "friend_birthday_promotion"

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->e:Ljava/lang/String;

    iget-object v6, v0, LX/EsA;->d:LX/EsB;

    iget-object v6, v6, LX/EsB;->e:Ljava/lang/String;

    const-string v7, "friend_birthday_campaign"

    const-string v8, "promotion"

    iget-object v9, v0, LX/EsA;->d:LX/EsB;

    iget-object v9, v9, LX/EsB;->h:Ljava/lang/String;

    invoke-virtual/range {v3 .. v9}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2177459
    iget-object v3, v0, LX/EsA;->d:LX/EsB;

    iget-object v3, v3, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    sget-object v4, LX/0ig;->aD:LX/0ih;

    const-string v5, "composer_open_photo"

    invoke-virtual {v3, v4, v5}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177460
    iget-object v3, v0, LX/EsA;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082a18

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2177461
    new-instance v4, LX/89I;

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    sget-object v7, LX/2rw;->USER:LX/2rw;

    invoke-direct {v4, v5, v6, v7}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->c:Ljava/lang/String;

    .line 2177462
    iput-object v5, v4, LX/89I;->d:Ljava/lang/String;

    .line 2177463
    move-object v4, v4

    .line 2177464
    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->d:Ljava/lang/String;

    .line 2177465
    iput-object v5, v4, LX/89I;->c:Ljava/lang/String;

    .line 2177466
    move-object v4, v4

    .line 2177467
    invoke-virtual {v4}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 2177468
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2177469
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2177470
    sget-object v6, LX/21D;->NEWSFEED:LX/21D;

    const-string v7, "goodwillPostPhotoFooterPartDefinition"

    invoke-static {v6, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    iget-object v7, v0, LX/EsA;->d:LX/EsB;

    iget-object v7, v7, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v7, v7, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->g:LX/1Nq;

    invoke-static {v3}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v6, v0, LX/EsA;->b:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-virtual {v3, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v4, "top_friend_birthday_promotion"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, v0, LX/EsA;->d:LX/EsB;

    iget-object v4, v4, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->h:LX/74n;

    invoke-static {v5, v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/74n;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 2177471
    iget-object v4, v0, LX/EsA;->d:LX/EsB;

    iget-object v4, v4, LX/EsB;->i:LX/EsC;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/EsC;->b:Ljava/lang/String;

    .line 2177472
    iget-object v4, v0, LX/EsA;->d:LX/EsB;

    iget-object v4, v4, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->e:LX/1Kf;

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->i:LX/EsC;

    iget-object v5, v5, LX/EsC;->b:Ljava/lang/String;

    iget-object v6, v0, LX/EsA;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v4, v5, v3, v6}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2177473
    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2177419
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    const-string v2, "flow_cancel"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177420
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2177421
    invoke-super {p0}, LX/EtA;->b()V

    .line 2177422
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2177400
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    const-string v2, "flow_fail"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177401
    invoke-virtual {p0}, LX/EtA;->g()V

    .line 2177402
    invoke-virtual {p0}, LX/EtA;->h()V

    .line 2177403
    iget-object v0, p0, LX/EtE;->q:LX/EsA;

    const/4 v10, 0x1

    .line 2177404
    iget-object v3, v0, LX/EsA;->c:Landroid/content/Context;

    const v4, 0x7f082a1a

    invoke-static {v3, v4, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 2177405
    iget-object v3, v0, LX/EsA;->d:LX/EsB;

    iget-object v3, v3, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->f:LX/1Cn;

    const-string v4, "friend_birthday_promotion"

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->e:Ljava/lang/String;

    iget-object v6, v0, LX/EsA;->d:LX/EsB;

    iget-object v6, v6, LX/EsB;->e:Ljava/lang/String;

    const-string v7, "friend_birthday_campaign"

    const-string v8, "promotion"

    iget-object v9, v0, LX/EsA;->d:LX/EsB;

    iget-object v9, v9, LX/EsB;->h:Ljava/lang/String;

    invoke-virtual/range {v3 .. v9}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2177406
    iget-object v3, v0, LX/EsA;->d:LX/EsB;

    iget-object v3, v3, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    sget-object v4, LX/0ig;->aD:LX/0ih;

    const-string v5, "composer_open_empty"

    invoke-virtual {v3, v4, v5}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177407
    iget-object v3, v0, LX/EsA;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082a18

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2177408
    new-instance v4, LX/89I;

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    sget-object v7, LX/2rw;->USER:LX/2rw;

    invoke-direct {v4, v5, v6, v7}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->c:Ljava/lang/String;

    .line 2177409
    iput-object v5, v4, LX/89I;->d:Ljava/lang/String;

    .line 2177410
    move-object v4, v4

    .line 2177411
    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->d:Ljava/lang/String;

    .line 2177412
    iput-object v5, v4, LX/89I;->c:Ljava/lang/String;

    .line 2177413
    move-object v4, v4

    .line 2177414
    invoke-virtual {v4}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 2177415
    sget-object v5, LX/21D;->NEWSFEED:LX/21D;

    const-string v6, "goodwillPostPhotoFooterPartDefinition"

    invoke-static {v5, v6}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    iget-object v6, v0, LX/EsA;->d:LX/EsB;

    iget-object v6, v6, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v6, v6, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->g:LX/1Nq;

    invoke-static {v3}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v5, v0, LX/EsA;->b:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-virtual {v3, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v4, "top_friend_birthday_promotion"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 2177416
    iget-object v4, v0, LX/EsA;->d:LX/EsB;

    iget-object v4, v4, LX/EsB;->i:LX/EsC;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/EsC;->b:Ljava/lang/String;

    .line 2177417
    iget-object v4, v0, LX/EsA;->d:LX/EsB;

    iget-object v4, v4, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->e:LX/1Kf;

    iget-object v5, v0, LX/EsA;->d:LX/EsB;

    iget-object v5, v5, LX/EsB;->i:LX/EsC;

    iget-object v5, v5, LX/EsC;->b:Ljava/lang/String;

    iget-object v6, v0, LX/EsA;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v4, v5, v3, v6}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2177418
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2177397
    iget-object v0, p0, LX/EtE;->r:LX/0if;

    sget-object v1, LX/0ig;->aD:LX/0ih;

    const-string v2, "flow_signal"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2177398
    invoke-super {p0}, LX/EtA;->c()V

    .line 2177399
    return-void
.end method
