.class public final LX/Dtn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2054426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;
    .locals 14

    .prologue
    .line 2054427
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2054428
    iget-object v1, p0, LX/Dtn;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2054429
    iget-object v2, p0, LX/Dtn;->b:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2054430
    iget-object v3, p0, LX/Dtn;->c:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2054431
    iget-object v4, p0, LX/Dtn;->d:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2054432
    iget-object v5, p0, LX/Dtn;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2054433
    iget-object v5, p0, LX/Dtn;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2054434
    iget-object v5, p0, LX/Dtn;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2054435
    iget-object v5, p0, LX/Dtn;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2054436
    iget-object v5, p0, LX/Dtn;->k:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2054437
    iget-object v5, p0, LX/Dtn;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2054438
    iget-object v5, p0, LX/Dtn;->m:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 2054439
    iget-object v5, p0, LX/Dtn;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2054440
    const/16 v5, 0xf

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2054441
    const/4 v5, 0x0

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2054442
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2054443
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2054444
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2054445
    const/4 v1, 0x4

    iget-wide v2, p0, LX/Dtn;->e:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054446
    const/4 v1, 0x5

    iget-wide v2, p0, LX/Dtn;->f:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054447
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2054448
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2054449
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2054450
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2054451
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2054452
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2054453
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2054454
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2054455
    const/16 v1, 0xe

    iget-wide v2, p0, LX/Dtn;->o:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054456
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2054457
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2054458
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2054459
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2054460
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2054461
    new-instance v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    invoke-direct {v1, v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;-><init>(LX/15i;)V

    .line 2054462
    return-object v1
.end method
