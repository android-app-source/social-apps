.class public final LX/EPE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/Cxe;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V
    .locals 0

    .prologue
    .line 2116194
    iput-object p1, p0, LX/EPE;->c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    iput-object p2, p0, LX/EPE;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/EPE;->b:LX/Cxe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x14de2886

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2116195
    iget-object v0, p0, LX/EPE;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 2116196
    if-nez v0, :cond_0

    .line 2116197
    const v0, -0x5586fc29

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2116198
    :goto_0
    return-void

    .line 2116199
    :cond_0
    iget-object v0, p0, LX/EPE;->b:LX/Cxe;

    iget-object v2, p0, LX/EPE;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v2}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2116200
    iget-object v0, p0, LX/EPE;->c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->b:LX/1nD;

    iget-object v0, p0, LX/EPE;->b:LX/Cxe;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    .line 2116201
    iget-object v3, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v3

    .line 2116202
    iget-object v3, p0, LX/EPE;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/EPE;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/8ci;->k:LX/8ci;

    invoke-virtual {v2, v0, v3, v4, v5}, LX/1nD;->a(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v0

    .line 2116203
    iget-object v2, p0, LX/EPE;->c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2116204
    const v0, -0xdd5bbf6

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
