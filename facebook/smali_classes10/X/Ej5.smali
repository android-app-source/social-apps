.class public LX/Ej5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/Eiz;

.field public final c:LX/Ej4;

.field public final d:LX/Ej1;

.field public final e:LX/Ej3;


# direct methods
.method public constructor <init>(LX/0Or;LX/Eiz;LX/Ej4;LX/Ej1;LX/Ej3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/Eiz;",
            "LX/Ej4;",
            "LX/Ej1;",
            "LX/Ej3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160842
    iput-object p1, p0, LX/Ej5;->a:LX/0Or;

    .line 2160843
    iput-object p2, p0, LX/Ej5;->b:LX/Eiz;

    .line 2160844
    iput-object p3, p0, LX/Ej5;->c:LX/Ej4;

    .line 2160845
    iput-object p4, p0, LX/Ej5;->d:LX/Ej1;

    .line 2160846
    iput-object p5, p0, LX/Ej5;->e:LX/Ej3;

    .line 2160847
    return-void
.end method

.method public static a(LX/0QB;)LX/Ej5;
    .locals 9

    .prologue
    .line 2160792
    const-class v1, LX/Ej5;

    monitor-enter v1

    .line 2160793
    :try_start_0
    sget-object v0, LX/Ej5;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2160794
    sput-object v2, LX/Ej5;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2160795
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2160796
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2160797
    new-instance v3, LX/Ej5;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/Eiz;->a(LX/0QB;)LX/Eiz;

    move-result-object v5

    check-cast v5, LX/Eiz;

    .line 2160798
    new-instance v6, LX/Ej4;

    invoke-direct {v6}, LX/Ej4;-><init>()V

    .line 2160799
    move-object v6, v6

    .line 2160800
    move-object v6, v6

    .line 2160801
    check-cast v6, LX/Ej4;

    .line 2160802
    new-instance v7, LX/Ej1;

    invoke-direct {v7}, LX/Ej1;-><init>()V

    .line 2160803
    move-object v7, v7

    .line 2160804
    move-object v7, v7

    .line 2160805
    check-cast v7, LX/Ej1;

    invoke-static {v0}, LX/Ej3;->a(LX/0QB;)LX/Ej3;

    move-result-object v8

    check-cast v8, LX/Ej3;

    invoke-direct/range {v3 .. v8}, LX/Ej5;-><init>(LX/0Or;LX/Eiz;LX/Ej4;LX/Ej1;LX/Ej3;)V

    .line 2160806
    move-object v0, v3

    .line 2160807
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2160808
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ej5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2160809
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2160810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2160811
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2160812
    const-string v1, "confirmation_confirm_contactpoint"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2160813
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2160814
    const-string v1, "confirmationConfirmContactpointParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;

    .line 2160815
    iget-object v1, p0, LX/Ej5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Ej5;->b:LX/Eiz;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2160816
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2160817
    move-object v0, v0

    .line 2160818
    :goto_0
    return-object v0

    .line 2160819
    :cond_0
    const-string v1, "confirmation_send_confirmation_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2160820
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2160821
    const-string v1, "confirmationSendConfirmationCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    .line 2160822
    iget-object v1, p0, LX/Ej5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Ej5;->c:LX/Ej4;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2160823
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2160824
    move-object v0, v0

    .line 2160825
    goto :goto_0

    .line 2160826
    :cond_1
    const-string v1, "confirmation_edit_registration_contactpoint"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2160827
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2160828
    const-string v1, "confirmationEditRegistrationContactpointParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    .line 2160829
    iget-object v1, p0, LX/Ej5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Ej5;->d:LX/Ej1;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2160830
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2160831
    move-object v0, v0

    .line 2160832
    goto :goto_0

    .line 2160833
    :cond_2
    const-string v1, "confirmation_openid_connect_email_confirmation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2160834
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2160835
    const-string v1, "confirmationOpenIDConnectEmailConfirmationParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;

    .line 2160836
    iget-object v1, p0, LX/Ej5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Ej5;->e:LX/Ej3;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2160837
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2160838
    move-object v0, v0

    .line 2160839
    goto :goto_0

    .line 2160840
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method
