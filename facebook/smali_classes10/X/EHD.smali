.class public final enum LX/EHD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EHD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EHD;

.field public static final enum AUDIO_CONFERENCE:LX/EHD;

.field public static final enum ROSTER_CONFERENCE:LX/EHD;

.field public static final enum VIDEO:LX/EHD;

.field public static final enum VIDEO_CONFERENCE:LX/EHD;

.field public static final enum VOICE:LX/EHD;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2098792
    new-instance v0, LX/EHD;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v2}, LX/EHD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHD;->VOICE:LX/EHD;

    .line 2098793
    new-instance v0, LX/EHD;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/EHD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHD;->VIDEO:LX/EHD;

    .line 2098794
    new-instance v0, LX/EHD;

    const-string v1, "AUDIO_CONFERENCE"

    invoke-direct {v0, v1, v4}, LX/EHD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHD;->AUDIO_CONFERENCE:LX/EHD;

    .line 2098795
    new-instance v0, LX/EHD;

    const-string v1, "VIDEO_CONFERENCE"

    invoke-direct {v0, v1, v5}, LX/EHD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    .line 2098796
    new-instance v0, LX/EHD;

    const-string v1, "ROSTER_CONFERENCE"

    invoke-direct {v0, v1, v6}, LX/EHD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    .line 2098797
    const/4 v0, 0x5

    new-array v0, v0, [LX/EHD;

    sget-object v1, LX/EHD;->VOICE:LX/EHD;

    aput-object v1, v0, v2

    sget-object v1, LX/EHD;->VIDEO:LX/EHD;

    aput-object v1, v0, v3

    sget-object v1, LX/EHD;->AUDIO_CONFERENCE:LX/EHD;

    aput-object v1, v0, v4

    sget-object v1, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    aput-object v1, v0, v5

    sget-object v1, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    aput-object v1, v0, v6

    sput-object v0, LX/EHD;->$VALUES:[LX/EHD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2098791
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EHD;
    .locals 1

    .prologue
    .line 2098789
    const-class v0, LX/EHD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EHD;

    return-object v0
.end method

.method public static values()[LX/EHD;
    .locals 1

    .prologue
    .line 2098790
    sget-object v0, LX/EHD;->$VALUES:[LX/EHD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EHD;

    return-object v0
.end method
