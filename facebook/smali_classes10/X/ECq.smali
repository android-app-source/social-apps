.class public final LX/ECq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/ECr;


# direct methods
.method public constructor <init>(LX/ECr;)V
    .locals 0

    .prologue
    .line 2090415
    iput-object p1, p0, LX/ECq;->a:LX/ECr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x439a806c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2090416
    iget-object v1, p0, LX/ECq;->a:LX/ECr;

    .line 2090417
    iget-object v3, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v3, v3, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    iget v4, v1, LX/ECr;->v:I

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EGF;

    .line 2090418
    iget-object v4, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget p0, v1, LX/ECr;->v:I

    .line 2090419
    iput p0, v4, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->i:I

    .line 2090420
    iget-object v4, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v4, v4, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    .line 2090421
    invoke-virtual {v4, v3}, LX/ECw;->a(LX/EGF;)LX/ECv;

    move-result-object p0

    sget-object p1, LX/ECv;->COMPLETED:LX/ECv;

    if-ne p0, p1, :cond_2

    const/4 p0, 0x1

    :goto_0
    move v4, p0

    .line 2090422
    if-eqz v4, :cond_0

    .line 2090423
    iget-object v3, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget v4, v1, LX/ECr;->v:I

    invoke-virtual {v3, v4}, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e(I)V

    .line 2090424
    :goto_1
    const v1, 0x6b5ba8f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2090425
    :cond_0
    iget-object v4, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v4, v4, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    invoke-virtual {v4}, LX/ECw;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2090426
    iget-object v4, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v4, v4, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    invoke-virtual {v4, v3}, LX/ECw;->c(LX/EGF;)V

    .line 2090427
    invoke-static {v1}, LX/ECr;->x(LX/ECr;)V

    goto :goto_1

    .line 2090428
    :cond_1
    iget-object v3, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v3, v3, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    iget-object v4, v1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v4, v4, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->b:Landroid/content/Context;

    .line 2090429
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f080807

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2090430
    iget-object p1, v3, LX/EDx;->S:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2090431
    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method
