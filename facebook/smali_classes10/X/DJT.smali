.class public final LX/DJT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985290
    iput-object p1, p0, LX/DJT;->a:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/model/ProductItemPlace;)V
    .locals 2

    .prologue
    .line 1985291
    if-eqz p1, :cond_0

    .line 1985292
    iget-object v0, p0, LX/DJT;->a:LX/DJc;

    iget-object v0, v0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v1, p1, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setStructuredLocationText(Ljava/lang/CharSequence;)V

    .line 1985293
    :cond_0
    iget-object v0, p0, LX/DJT;->a:LX/DJc;

    invoke-static {v0}, LX/DJc;->aS(LX/DJc;)V

    .line 1985294
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1985295
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DJT;->a:LX/DJc;

    iget-object v0, v0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getZipcodeText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1985296
    const/4 v0, 0x5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1985297
    iget-object v1, p0, LX/DJT;->a:LX/DJc;

    iget-object v1, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setZipcodeText(Ljava/lang/CharSequence;)V

    .line 1985298
    :cond_0
    iget-object v0, p0, LX/DJT;->a:LX/DJc;

    invoke-static {v0}, LX/DJc;->aS(LX/DJc;)V

    .line 1985299
    return-void
.end method
