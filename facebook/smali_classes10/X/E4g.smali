.class public LX/E4g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E4g;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/E4g;
    .locals 3

    .prologue
    .line 2077061
    sget-object v0, LX/E4g;->a:LX/E4g;

    if-nez v0, :cond_1

    .line 2077062
    const-class v1, LX/E4g;

    monitor-enter v1

    .line 2077063
    :try_start_0
    sget-object v0, LX/E4g;->a:LX/E4g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077064
    if-eqz v2, :cond_0

    .line 2077065
    :try_start_1
    new-instance v0, LX/E4g;

    invoke-direct {v0}, LX/E4g;-><init>()V

    .line 2077066
    move-object v0, v0

    .line 2077067
    sput-object v0, LX/E4g;->a:LX/E4g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077068
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077069
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077070
    :cond_1
    sget-object v0, LX/E4g;->a:LX/E4g;

    return-object v0

    .line 2077071
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077072
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
