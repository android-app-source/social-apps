.class public LX/Ctk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cre;


# instance fields
.field public final a:LX/Ctg;

.field private final b:LX/CsO;


# direct methods
.method public constructor <init>(LX/Ctg;LX/CsO;)V
    .locals 0

    .prologue
    .line 1945547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945548
    iput-object p1, p0, LX/Ctk;->a:LX/Ctg;

    .line 1945549
    iput-object p2, p0, LX/Ctk;->b:LX/CsO;

    .line 1945550
    return-void
.end method

.method public static b(LX/Ctk;)LX/Cqw;
    .locals 1

    .prologue
    .line 1945551
    iget-object v0, p0, LX/Ctk;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->f()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    return-object v0
.end method

.method public static c(LX/Ctk;)LX/Cqw;
    .locals 1

    .prologue
    .line 1945552
    iget-object v0, p0, LX/Ctk;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    return-object v0
.end method


# virtual methods
.method public final a(LX/Crd;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1945553
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    move-object v2, v2

    .line 1945554
    if-eqz v2, :cond_0

    .line 1945555
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    move-object v2, v2

    .line 1945556
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1945557
    :cond_0
    :goto_0
    return v0

    .line 1945558
    :cond_1
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    .line 1945559
    invoke-virtual {v2}, LX/Cqj;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945560
    sget-object v2, LX/Ctj;->a:[I

    invoke-virtual {p1}, LX/Crd;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1945561
    :pswitch_0
    invoke-static {p0}, LX/Ctk;->b(LX/Ctk;)LX/Cqw;

    move-result-object v2

    .line 1945562
    sget-object v3, LX/Cqw;->a:LX/Cqw;

    if-ne v2, v3, :cond_2

    .line 1945563
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v3, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v2, v3}, LX/Ctg;->b(LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945564
    iget-object v0, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v0, v2}, LX/Ctg;->a(LX/Cqw;)V

    move v0, v1

    .line 1945565
    goto :goto_0

    .line 1945566
    :cond_2
    sget-object v3, LX/Cqw;->b:LX/Cqw;

    if-ne v2, v3, :cond_0

    .line 1945567
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v3, LX/Cqw;->a:LX/Cqw;

    invoke-interface {v2, v3}, LX/Ctg;->b(LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1945568
    iget-object v0, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v2, LX/Cqw;->a:LX/Cqw;

    invoke-interface {v0, v2}, LX/Ctg;->a(LX/Cqw;)V

    move v0, v1

    .line 1945569
    goto :goto_0

    .line 1945570
    :cond_3
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v3, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v2, v3}, LX/Ctg;->b(LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945571
    iget-object v0, p0, LX/Ctk;->a:LX/Ctg;

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v0, v2}, LX/Ctg;->a(LX/Cqw;)V

    move v0, v1

    .line 1945572
    goto :goto_0

    .line 1945573
    :pswitch_1
    iget-object v2, p0, LX/Ctk;->b:LX/CsO;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/CsO;->a(Landroid/view/View;)V

    .line 1945574
    :pswitch_2
    invoke-static {p0}, LX/Ctk;->b(LX/Ctk;)LX/Cqw;

    move-result-object v2

    invoke-static {p0}, LX/Ctk;->c(LX/Ctk;)LX/Cqw;

    move-result-object v3

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1945575
    if-nez v2, :cond_0

    .line 1945576
    iget-object v2, p0, LX/Ctk;->a:LX/Ctg;

    invoke-static {p0}, LX/Ctk;->c(LX/Ctk;)LX/Cqw;

    move-result-object v3

    invoke-interface {v2, v3}, LX/Ctg;->a(LX/Cqw;)V

    .line 1945577
    invoke-static {p0}, LX/Ctk;->c(LX/Ctk;)LX/Cqw;

    move-result-object v2

    sget-object v3, LX/Cqw;->b:LX/Cqw;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
