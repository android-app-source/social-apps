.class public LX/Exs;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:LX/2dl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EuP;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ExM;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hd;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/Ey4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/EyP;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:LX/Ew6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 2185435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/Exs;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2185437
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185438
    iput-object v0, p0, LX/Exs;->d:LX/0Ot;

    .line 2185439
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185440
    iput-object v0, p0, LX/Exs;->e:LX/0Ot;

    .line 2185441
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185442
    iput-object v0, p0, LX/Exs;->f:LX/0Ot;

    .line 2185443
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185444
    iput-object v0, p0, LX/Exs;->i:LX/0Ot;

    .line 2185445
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185446
    iput-object v0, p0, LX/Exs;->j:LX/0Ot;

    .line 2185447
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185448
    iput-object v0, p0, LX/Exs;->k:LX/0Ot;

    .line 2185449
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185450
    iput-object v0, p0, LX/Exs;->l:LX/0Ot;

    .line 2185451
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185452
    iput-object v0, p0, LX/Exs;->m:LX/0Ot;

    .line 2185453
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185454
    iput-object v0, p0, LX/Exs;->n:LX/0Ot;

    .line 2185455
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Exs;->q:Ljava/util/Map;

    .line 2185456
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Exs;->t:Z

    .line 2185457
    const-string v0, "unknown"

    iput-object v0, p0, LX/Exs;->u:Ljava/lang/String;

    .line 2185458
    return-void
.end method

.method public static a(LX/Exs;J)LX/EyP;
    .locals 3

    .prologue
    .line 2185459
    iget-object v0, p0, LX/Exs;->q:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2185460
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EyP;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/Exs;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2185461
    if-lez p1, :cond_0

    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v1, 0x7f0f005c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(LX/Exs;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/2do;LX/2dl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Ey4;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Exs;",
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/2do;",
            "LX/2dl;",
            "LX/0Ot",
            "<",
            "LX/EuP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ExM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/Ey4;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2185462
    iput-object p1, p0, LX/Exs;->a:LX/0Or;

    iput-object p2, p0, LX/Exs;->c:Landroid/content/Context;

    iput-object p3, p0, LX/Exs;->d:LX/0Ot;

    iput-object p4, p0, LX/Exs;->e:LX/0Ot;

    iput-object p5, p0, LX/Exs;->f:LX/0Ot;

    iput-object p6, p0, LX/Exs;->g:LX/2do;

    iput-object p7, p0, LX/Exs;->h:LX/2dl;

    iput-object p8, p0, LX/Exs;->i:LX/0Ot;

    iput-object p9, p0, LX/Exs;->j:LX/0Ot;

    iput-object p10, p0, LX/Exs;->k:LX/0Ot;

    iput-object p11, p0, LX/Exs;->l:LX/0Ot;

    iput-object p12, p0, LX/Exs;->m:LX/0Ot;

    iput-object p13, p0, LX/Exs;->n:LX/0Ot;

    iput-object p14, p0, LX/Exs;->o:LX/Ey4;

    iput-object p15, p0, LX/Exs;->p:Landroid/content/res/Resources;

    return-void
.end method

.method public static a(LX/Exs;LX/EyT;)V
    .locals 2

    .prologue
    .line 2185463
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/EyT;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185464
    return-void
.end method

.method public static a(LX/Exs;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2185465
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 2185466
    sget-object v1, LX/Exr;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2185467
    :goto_0
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 2185468
    iget-object v0, p0, LX/Exs;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/2ez;->p:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2185469
    iget-object v0, p0, LX/Exs;->h:LX/2dl;

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 2185470
    :goto_1
    const-string v0, "FC_FRIENDS_QUERY"

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185471
    iget-object v0, p0, LX/Exs;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuP;

    invoke-virtual {v0}, LX/EuP;->d()V

    .line 2185472
    :cond_0
    const-string v0, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2185473
    iget-object v0, p0, LX/Exs;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuP;

    invoke-virtual {v0}, LX/EuP;->b()V

    .line 2185474
    :cond_1
    return-void

    .line 2185475
    :pswitch_0
    const-string v1, "FC_FRIENDS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2185476
    const-string v1, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2185477
    :pswitch_1
    const-string v1, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2185478
    :pswitch_2
    const-string v1, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2185479
    :pswitch_3
    const-string v1, "FC_REQUESTS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2185480
    const-string v1, "FC_FRIENDS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2185481
    const-string v1, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2185482
    :cond_2
    iget-object v0, p0, LX/Exs;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/EyP;)V
    .locals 3

    .prologue
    .line 2185483
    const-string v0, "%s %s"

    invoke-interface {p0}, LX/EyP;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p0}, LX/EyP;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, LX/EyP;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2185484
    return-void
.end method

.method public static b(LX/0QB;)LX/Exs;
    .locals 17

    .prologue
    .line 2185485
    new-instance v1, LX/Exs;

    invoke-direct {v1}, LX/Exs;-><init>()V

    .line 2185486
    const/16 v2, 0x5cb

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0xa79

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2eb

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa71

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v7

    check-cast v7, LX/2do;

    invoke-static/range {p0 .. p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v8

    check-cast v8, LX/2dl;

    const/16 v9, 0x2232

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xb0b

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2246

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2241

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xa7e

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1032

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/Ey4;->a(LX/0QB;)LX/Ey4;

    move-result-object v15

    check-cast v15, LX/Ey4;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v16

    check-cast v16, Landroid/content/res/Resources;

    invoke-static/range {v1 .. v16}, LX/Exs;->a(LX/Exs;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/2do;LX/2dl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Ey4;Landroid/content/res/Resources;)V

    .line 2185487
    return-object v1
.end method

.method public static b(LX/Exs;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # LX/Exs;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2185488
    iget-object v0, p0, LX/Exs;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23P;

    .line 2185489
    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/Exs;->p:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/Exs;LX/EyT;)V
    .locals 1

    .prologue
    .line 2185361
    const v0, 0x7f080f78

    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyT;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2185362
    return-void
.end method

.method public static b(LX/Exs;LX/EyT;LX/83X;)V
    .locals 1

    .prologue
    .line 2185490
    new-instance v0, LX/Exk;

    invoke-direct {v0, p0, p2}, LX/Exk;-><init>(LX/Exs;LX/83X;)V

    invoke-interface {p1, v0}, LX/EyT;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185491
    return-void
.end method

.method public static b(LX/Exs;J)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2185492
    invoke-static {p0, p1, p2}, LX/Exs;->a(LX/Exs;J)LX/EyP;

    move-result-object v0

    .line 2185493
    if-nez v0, :cond_0

    move v0, v1

    .line 2185494
    :goto_0
    return v0

    .line 2185495
    :cond_0
    const v2, 0x7f0d00f6

    invoke-interface {v0, v2}, LX/EyP;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2185496
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static b$redex0(LX/Exs;LX/EyQ;LX/83X;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2185423
    sget-object v0, LX/Exr;->a:[I

    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2185424
    invoke-interface {p1, v2, v2}, LX/EyQ;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185425
    :goto_0
    return-void

    .line 2185426
    :pswitch_0
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/EyQ;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185427
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v1, 0x7f080f9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyQ;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185428
    :pswitch_1
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/EyQ;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185429
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v1, 0x7f080f9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyQ;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185430
    :pswitch_2
    if-eqz p3, :cond_0

    const v0, 0x7f080f7d

    :goto_1
    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080017

    invoke-static {p0, v1}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/EyQ;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185431
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v1, 0x7f080f9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyQ;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185432
    :cond_0
    const v0, 0x7f080017

    goto :goto_1

    .line 2185433
    :pswitch_3
    const v0, 0x7f080f76

    invoke-static {p0, v0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0, v2}, LX/EyQ;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2185434
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v1, 0x7f080f9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyQ;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(LX/Exs;LX/EyP;LX/83X;)V
    .locals 3

    .prologue
    .line 2185497
    invoke-interface {p2}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setThumbnailUri(Ljava/lang/String;)V

    .line 2185498
    invoke-interface {p2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2185499
    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2185500
    invoke-interface {p2}, LX/83X;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2185501
    sget-object v2, LX/Exr;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2185502
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_b

    move-object v0, p2

    .line 2185503
    check-cast v0, LX/Eut;

    .line 2185504
    iget-boolean v1, v0, LX/Eut;->e:Z

    move v0, v1

    .line 2185505
    if-eqz v0, :cond_a

    .line 2185506
    const v0, 0x7f080f8b

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    .line 2185507
    :goto_0
    invoke-static {p1}, LX/Exs;->a(LX/EyP;)V

    .line 2185508
    return-void

    .line 2185509
    :pswitch_0
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, LX/Eut;

    .line 2185510
    iget-boolean v2, v0, LX/Eut;->c:Z

    move v0, v2

    .line 2185511
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2185512
    :goto_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 2185513
    const v0, 0x7f081fe5

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto :goto_0

    .line 2185514
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2185515
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2185516
    const v0, 0x7f080f86

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto :goto_0

    .line 2185517
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2185518
    const v0, 0x7f080f8b

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto :goto_0

    .line 2185519
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2185520
    const v0, 0x7f080fa2

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto :goto_0

    .line 2185521
    :cond_4
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_5

    .line 2185522
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185523
    :cond_5
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/Exs;->a(LX/Exs;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185524
    :pswitch_1
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_6

    .line 2185525
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185526
    :cond_6
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/Exs;->a(LX/Exs;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2185527
    :pswitch_2
    const v0, 0x7f080f85

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto/16 :goto_0

    .line 2185528
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2185529
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_7

    check-cast p2, LX/Eut;

    .line 2185530
    iget-boolean v0, p2, LX/Eut;->e:Z

    move v0, v0

    .line 2185531
    if-eqz v0, :cond_7

    .line 2185532
    const v0, 0x7f080f8c

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto/16 :goto_0

    .line 2185533
    :cond_7
    const v0, 0x7f080f8a

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(I)V

    goto/16 :goto_0

    .line 2185534
    :cond_8
    instance-of v0, p2, LX/Eut;

    if-eqz v0, :cond_9

    .line 2185535
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2185536
    :cond_9
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/Exs;->a(LX/Exs;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2185537
    :cond_a
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2185538
    :cond_b
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/Exs;->a(LX/Exs;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(LX/Exs;LX/EyQ;LX/83X;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2185355
    sget-object v1, LX/Exr;->a:[I

    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2185356
    :goto_0
    return-void

    .line 2185357
    :pswitch_0
    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    if-eqz p3, :cond_0

    :goto_1
    invoke-interface {p1, v1, v0}, LX/EyQ;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v2, 0x7f020b78

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 2185358
    :pswitch_1
    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    if-eqz p3, :cond_1

    :goto_2
    invoke-interface {p1, v1, v0}, LX/EyQ;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v2, 0x7f020b78

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2

    .line 2185359
    :pswitch_2
    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    if-eqz p3, :cond_2

    :goto_3
    invoke-interface {p1, v1, v0}, LX/EyQ;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v2, 0x7f020b7a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_3

    .line 2185360
    :pswitch_3
    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    if-eqz p3, :cond_3

    :goto_4
    invoke-interface {p1, v1, v0}, LX/EyQ;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/Exs;->p:Landroid/content/res/Resources;

    const v2, 0x7f020b7e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(LX/Exs;LX/EyT;LX/83X;)V
    .locals 1

    .prologue
    .line 2185363
    new-instance v0, LX/Exl;

    invoke-direct {v0, p0, p2}, LX/Exl;-><init>(LX/Exs;LX/83X;)V

    invoke-interface {p1, v0}, LX/EyT;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185364
    return-void
.end method

.method public static c(LX/83X;)Z
    .locals 2

    .prologue
    .line 2185365
    invoke-interface {p0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_0

    invoke-interface {p0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/Exs;LX/EyQ;LX/83X;Z)V
    .locals 1

    .prologue
    .line 2185366
    new-instance v0, LX/Exn;

    invoke-direct {v0, p0, p2, p1, p3}, LX/Exn;-><init>(LX/Exs;LX/83X;LX/EyQ;Z)V

    invoke-interface {p1, v0}, LX/EyQ;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185367
    return-void
.end method

.method private static d(LX/83X;)Z
    .locals 2

    .prologue
    .line 2185368
    invoke-interface {p0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    instance-of v0, p0, LX/Eut;

    if-eqz v0, :cond_0

    check-cast p0, LX/Eut;

    .line 2185369
    iget-boolean v0, p0, LX/Eut;->e:Z

    move v0, v0

    .line 2185370
    if-eqz v0, :cond_0

    .line 2185371
    const/4 v0, 0x0

    .line 2185372
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(LX/Exs;LX/EyP;LX/83X;)V
    .locals 1

    .prologue
    .line 2185373
    new-instance v0, LX/Exo;

    invoke-direct {v0, p0, p2}, LX/Exo;-><init>(LX/Exs;LX/83X;)V

    invoke-interface {p1, v0}, LX/EyP;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2185374
    return-void
.end method

.method public static e(LX/Exs;LX/EyQ;LX/83X;Z)V
    .locals 12

    .prologue
    .line 2185375
    invoke-interface {p2}, LX/83X;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2185376
    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 2185377
    sget-object v0, LX/Exs;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2185378
    invoke-static {p0, v3}, LX/Exs;->a(LX/Exs;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185379
    invoke-interface {p2, v0}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185380
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v5, 0x1

    .line 2185381
    :goto_0
    if-eqz v5, :cond_3

    if-eqz p3, :cond_3

    .line 2185382
    iget-object v0, p0, LX/Exs;->r:LX/Ew6;

    if-eqz v0, :cond_1

    .line 2185383
    iget-object v0, p0, LX/Exs;->r:LX/Ew6;

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v6

    invoke-interface {v0, v6, v7}, LX/Ew6;->a(J)V

    .line 2185384
    :cond_1
    :goto_1
    iget-object v0, p0, LX/Exs;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/2iT;

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v10

    invoke-interface {p2}, LX/83W;->g()LX/2h7;

    move-result-object v8

    new-instance v0, LX/Exq;

    move-object v1, p0

    move-object v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, LX/Exq;-><init>(LX/Exs;LX/83X;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;ZZ)V

    move-object v5, v7

    move-wide v6, v10

    move-object v9, v3

    move-object v10, v0

    invoke-virtual/range {v5 .. v10}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 2185385
    return-void

    .line 2185386
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 2185387
    :cond_3
    invoke-static {p0, p1, p2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185388
    invoke-virtual {p0, p1, p2}, LX/Exs;->b(LX/EyQ;LX/83X;)V

    .line 2185389
    invoke-virtual {p0, p1, p2}, LX/Exs;->c(LX/EyQ;LX/83X;)V

    .line 2185390
    invoke-static {p0, p1, p2, p3}, LX/Exs;->d(LX/Exs;LX/EyQ;LX/83X;Z)V

    goto :goto_1
.end method

.method public static f(LX/Exs;LX/EyP;LX/83X;)V
    .locals 6

    .prologue
    .line 2185391
    const v0, 0x7f0d00f6

    invoke-interface {p1, v0}, LX/EyP;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2185392
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v2

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2185393
    iget-object v1, p0, LX/Exs;->q:Ljava/util/Map;

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185394
    :cond_0
    const v0, 0x7f0d00f6

    invoke-interface {p1, v0, p2}, LX/EyP;->setTag(ILjava/lang/Object;)V

    .line 2185395
    iget-object v0, p0, LX/Exs;->q:Ljava/util/Map;

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185396
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2185397
    iget-object v0, p0, LX/Exs;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2185398
    iget-object v0, p0, LX/Exs;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iT;

    const/4 v1, 0x1

    .line 2185399
    iput-boolean v1, v0, LX/2hY;->d:Z

    .line 2185400
    return-void
.end method

.method public final a(LX/EyQ;LX/83X;)V
    .locals 1

    .prologue
    .line 2185401
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/Exs;->a(LX/EyQ;LX/83X;Z)V

    .line 2185402
    return-void
.end method

.method public final a(LX/EyQ;LX/83X;Z)V
    .locals 1

    .prologue
    .line 2185403
    invoke-static {p0, p1, p2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185404
    invoke-static {p2}, LX/Exs;->c(LX/83X;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/Exs;->d(LX/83X;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2185405
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/EyQ;->setShowActionButton(Z)V

    .line 2185406
    :goto_0
    return-void

    .line 2185407
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/Exs;->b(LX/EyQ;LX/83X;)V

    .line 2185408
    invoke-virtual {p0, p1, p2}, LX/Exs;->c(LX/EyQ;LX/83X;)V

    .line 2185409
    invoke-static {p0, p1, p2, p3}, LX/Exs;->d(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185410
    invoke-static {p0, p1, p2}, LX/Exs;->f(LX/Exs;LX/EyP;LX/83X;)V

    goto :goto_0
.end method

.method public final a(LX/EyT;LX/83X;)V
    .locals 1

    .prologue
    .line 2185411
    invoke-static {p0, p1, p2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185412
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LX/EyT;->setFriendRequestButtonsVisible(Z)V

    .line 2185413
    invoke-static {p0, p1}, LX/Exs;->a(LX/Exs;LX/EyT;)V

    .line 2185414
    invoke-static {p0, p1}, LX/Exs;->b(LX/Exs;LX/EyT;)V

    .line 2185415
    invoke-static {p0, p1, p2}, LX/Exs;->b(LX/Exs;LX/EyT;LX/83X;)V

    .line 2185416
    invoke-static {p0, p1, p2}, LX/Exs;->c(LX/Exs;LX/EyT;LX/83X;)V

    .line 2185417
    invoke-static {p0, p1, p2}, LX/Exs;->f(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185418
    return-void
.end method

.method public final b(LX/EyQ;LX/83X;)V
    .locals 1

    .prologue
    .line 2185419
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/Exs;->b$redex0(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185420
    return-void
.end method

.method public final c(LX/EyQ;LX/83X;)V
    .locals 1

    .prologue
    .line 2185421
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/Exs;->c(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185422
    return-void
.end method
