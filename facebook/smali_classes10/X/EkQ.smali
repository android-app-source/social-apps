.class public abstract LX/EkQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/AU0;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/AU0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public c:J

.field private final d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2163448
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/EkQ;-><init>(Z)V

    .line 2163449
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 2163419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163420
    iput-boolean p1, p0, LX/EkQ;->d:Z

    .line 2163421
    return-void
.end method

.method private a()Z
    .locals 6

    .prologue
    .line 2163428
    iget-object v2, p0, LX/EkQ;->b:LX/AU0;

    if-nez v2, :cond_2

    .line 2163429
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "DAOItem is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 2163430
    :goto_0
    move-object v0, v2

    .line 2163431
    if-nez v0, :cond_0

    .line 2163432
    const/4 v0, 0x1

    .line 2163433
    :goto_1
    return v0

    .line 2163434
    :cond_0
    iget-boolean v1, p0, LX/EkQ;->d:Z

    if-eqz v1, :cond_1

    .line 2163435
    throw v0

    .line 2163436
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2163437
    :cond_2
    iget-object v2, p0, LX/EkQ;->b:LX/AU0;

    invoke-interface {v2}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2163438
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cursor is closed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/EkQ;->b:LX/AU0;

    invoke-interface {v4}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2163439
    :cond_3
    iget-object v2, p0, LX/EkQ;->b:LX/AU0;

    iget v3, p0, LX/EkQ;->a:I

    invoke-interface {v2, v3}, LX/AU0;->a(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2163440
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t move dao to position: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, LX/EkQ;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2163441
    :cond_4
    iget-object v2, p0, LX/EkQ;->b:LX/AU0;

    invoke-interface {v2}, LX/AU0;->d()J

    move-result-wide v2

    iget-wide v4, p0, LX/EkQ;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 2163442
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dao row ID mismatch! old = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/EkQ;->c:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " new = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/EkQ;->b:LX/AU0;

    invoke-interface {v4}, LX/AU0;->d()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2163443
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/AU0;I)LX/EkQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)",
            "LX/EkQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2163444
    iput-object p1, p0, LX/EkQ;->b:LX/AU0;

    .line 2163445
    iput p2, p0, LX/EkQ;->a:I

    .line 2163446
    invoke-interface {p1}, LX/AU0;->d()J

    move-result-wide v0

    iput-wide v0, p0, LX/EkQ;->c:J

    .line 2163447
    return-object p0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x2

    const v0, 0x71929036

    invoke-static {v8, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2163422
    invoke-direct {p0}, LX/EkQ;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2163423
    const-string v1, "OnDAOItemClickListener"

    const-string v2, "Cursor is invalid: ignoring user\'s click of row id=%d"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, LX/EkQ;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2163424
    const v1, 0x7b9216ce

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2163425
    :goto_0
    return-void

    .line 2163426
    :cond_0
    iget-object v1, p0, LX/EkQ;->b:LX/AU0;

    invoke-virtual {p0, p1, v1}, LX/EkQ;->onClick(Landroid/view/View;LX/AU0;)V

    .line 2163427
    const v1, 0x2a15e56e

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public abstract onClick(Landroid/view/View;LX/AU0;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation
.end method
