.class public LX/Er3;
.super LX/7HT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HT",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/Eqc;

.field private final c:LX/03V;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172394
    const-class v0, LX/Er3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Er3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Eqc;LX/ErO;LX/03V;LX/1Ck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172395
    invoke-direct {p0, p4, p2}, LX/7HT;-><init>(LX/1Ck;LX/7Hq;)V

    .line 2172396
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172397
    iput-object v0, p0, LX/Er3;->d:LX/0Px;

    .line 2172398
    iput-object p3, p0, LX/Er3;->c:LX/03V;

    .line 2172399
    iput-object p1, p0, LX/Er3;->b:LX/Eqc;

    .line 2172400
    return-void
.end method


# virtual methods
.method public final a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2172401
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2172402
    iget-object v1, p0, LX/Er3;->b:LX/Eqc;

    new-instance v2, LX/Er2;

    invoke-direct {v2, p0, v0}, LX/Er2;-><init>(LX/Er3;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 2172403
    new-instance p0, LX/Eqb;

    invoke-static {v1}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v3

    check-cast v3, LX/0Zr;

    invoke-static {v1}, LX/8RL;->b(LX/0QB;)LX/8RL;

    move-result-object v4

    check-cast v4, LX/8RL;

    invoke-direct {p0, v3, v4, v2}, LX/Eqb;-><init>(LX/0Zr;LX/8RL;LX/Er2;)V

    .line 2172404
    move-object v1, p0

    .line 2172405
    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/3Mm;->a(Ljava/lang/CharSequence;)V

    .line 2172406
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2172407
    const-string v0, "FETCH_LOCAL_FRIENDS_TASK"

    return-object v0
.end method

.method public final a(LX/7B6;Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2172408
    iget-object v0, p0, LX/Er3;->c:LX/03V;

    sget-object v1, LX/Er3;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch local invitees"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172409
    iput-object p2, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172410
    move-object v1, v1

    .line 2172411
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172412
    return-void
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2172413
    sget-object v0, LX/7HY;->LOCAL:LX/7HY;

    return-object v0
.end method
