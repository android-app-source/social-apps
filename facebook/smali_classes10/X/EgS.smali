.class public LX/EgS;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EgR;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2156122
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EgS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2156123
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2156124
    iput-object p1, p0, LX/EgS;->b:LX/0Ot;

    .line 2156125
    return-void
.end method

.method public static a(LX/0QB;)LX/EgS;
    .locals 4

    .prologue
    .line 2156126
    const-class v1, LX/EgS;

    monitor-enter v1

    .line 2156127
    :try_start_0
    sget-object v0, LX/EgS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2156128
    sput-object v2, LX/EgS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2156129
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2156130
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2156131
    new-instance v3, LX/EgS;

    const/16 p0, 0x180b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EgS;-><init>(LX/0Ot;)V

    .line 2156132
    move-object v0, v3

    .line 2156133
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2156134
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EgS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2156135
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2156136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2156137
    invoke-static {}, LX/1dS;->b()V

    .line 2156138
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 8

    .prologue
    .line 2156139
    check-cast p3, LX/EgQ;

    .line 2156140
    iget-object v0, p0, LX/EgS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;

    iget-object v1, p3, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156141
    if-nez v1, :cond_1

    .line 2156142
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v2

    invoke-static {p1}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Bcc;->a(LX/1X5;)LX/Bcc;

    move-result-object v2

    invoke-virtual {v2}, LX/Bcc;->b()LX/BcO;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2156143
    :cond_0
    return-void

    .line 2156144
    :cond_1
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->b:LX/EtW;

    invoke-virtual {v3, p1}, LX/EtW;->c(LX/1De;)LX/EtV;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EtV;->b(Ljava/lang/String;)LX/EtV;

    move-result-object v3

    const-string v4, "FACEBOOK"

    .line 2156145
    iget-object v5, v3, LX/EtV;->a:Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    iput-object v4, v5, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    .line 2156146
    move-object v3, v3

    .line 2156147
    sget-object v4, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/EtV;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/EtV;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EtV;->a(Landroid/net/Uri;)LX/EtV;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Bcc;->a(LX/1X5;)LX/Bcc;

    move-result-object v2

    const-string v3, "profile"

    invoke-virtual {v2, v3}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object v2

    invoke-virtual {v2}, LX/Bcc;->b()LX/BcO;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2156148
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v2

    invoke-static {p1}, LX/Egu;->c(LX/1De;)LX/Egt;

    move-result-object v3

    const-string v4, "EXPLORE"

    invoke-virtual {v3, v4}, LX/Egt;->b(Ljava/lang/String;)LX/Egt;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v2

    const-string v3, "explore"

    invoke-virtual {v2, v3}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object v2

    invoke-virtual {v2}, LX/Bcc;->b()LX/BcO;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2156149
    invoke-virtual {v1}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v2

    .line 2156150
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2156151
    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;

    .line 2156152
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->c:LX/Egy;

    const/4 p0, 0x0

    .line 2156153
    new-instance p3, LX/Egw;

    invoke-direct {p3, v7}, LX/Egw;-><init>(LX/Egy;)V

    .line 2156154
    sget-object v1, LX/Egy;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Egx;

    .line 2156155
    if-nez v1, :cond_2

    .line 2156156
    new-instance v1, LX/Egx;

    invoke-direct {v1}, LX/Egx;-><init>()V

    .line 2156157
    :cond_2
    invoke-static {v1, p1, p0, p0, p3}, LX/Egx;->a$redex0(LX/Egx;LX/1De;IILX/Egw;)V

    .line 2156158
    move-object p3, v1

    .line 2156159
    move-object p0, p3

    .line 2156160
    move-object v7, p0

    .line 2156161
    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->a()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object p0

    .line 2156162
    iget-object p3, v7, LX/Egx;->a:LX/Egw;

    iput-object p0, p3, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2156163
    iget-object p3, v7, LX/Egx;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Ljava/util/BitSet;->set(I)V

    .line 2156164
    move-object v7, v7

    .line 2156165
    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->a()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object v2

    invoke-virtual {v2}, LX/Bcc;->b()LX/BcO;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2156166
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0
.end method
