.class public final LX/Dzn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

.field private final b:LX/969;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/widget/EditText;LX/969;)V
    .locals 1

    .prologue
    .line 2067163
    iput-object p1, p0, LX/Dzn;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067164
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dzn;->c:Ljava/lang/String;

    .line 2067165
    iput-object p3, p0, LX/Dzn;->b:LX/969;

    .line 2067166
    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 2067167
    check-cast p1, Landroid/widget/EditText;

    .line 2067168
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2067169
    if-nez p2, :cond_0

    iget-object v1, p0, LX/Dzn;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2067170
    iget-object v1, p0, LX/Dzn;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v2, p0, LX/Dzn;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, p0, LX/Dzn;->b:LX/969;

    invoke-virtual {v1, v2, v3}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/969;)V

    .line 2067171
    :cond_0
    iput-object v0, p0, LX/Dzn;->c:Ljava/lang/String;

    .line 2067172
    return-void
.end method
