.class public LX/DH0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DH1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DH0",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DH1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981133
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1981134
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DH0;->b:LX/0Zi;

    .line 1981135
    iput-object p1, p0, LX/DH0;->a:LX/0Ot;

    .line 1981136
    return-void
.end method

.method public static a(LX/0QB;)LX/DH0;
    .locals 4

    .prologue
    .line 1981137
    const-class v1, LX/DH0;

    monitor-enter v1

    .line 1981138
    :try_start_0
    sget-object v0, LX/DH0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981139
    sput-object v2, LX/DH0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981140
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981141
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981142
    new-instance v3, LX/DH0;

    const/16 p0, 0x21c2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DH0;-><init>(LX/0Ot;)V

    .line 1981143
    move-object v0, v3

    .line 1981144
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981145
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DH0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981146
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1981148
    check-cast p2, LX/DGz;

    .line 1981149
    iget-object v0, p0, LX/DH0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DH1;

    iget-object v1, p2, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DGz;->b:LX/1Wk;

    iget-object v3, p2, LX/DGz;->c:LX/1Pr;

    const/high16 p2, 0x3f000000    # 0.5f

    const/4 p0, 0x1

    .line 1981150
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b00a6

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    .line 1981151
    iget-object v5, v0, LX/DH1;->a:LX/39c;

    invoke-virtual {v5, p1}, LX/39c;->c(LX/1De;)LX/39u;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v5

    sget-object v6, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v5, v6}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/39u;->a(Z)LX/39u;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 1981152
    const v6, 0x41aaf3c1

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1981153
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/DH1;->a:LX/39c;

    invoke-virtual {v6, p1}, LX/39c;->c(LX/1De;)LX/39u;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v6

    sget-object v7, LX/20X;->SAVE:LX/20X;

    invoke-virtual {v6, v7}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/39u;->a(Z)LX/39u;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    .line 1981154
    const v7, 0x41aaf5ef

    const/4 p0, 0x0

    invoke-static {p1, v7, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 1981155
    invoke-interface {v6, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1981156
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1981157
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1981158
    invoke-static {}, LX/1dS;->b()V

    .line 1981159
    iget v0, p1, LX/1dQ;->b:I

    .line 1981160
    sparse-switch v0, :sswitch_data_0

    .line 1981161
    :goto_0
    return-object v2

    .line 1981162
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1981163
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1981164
    check-cast v1, LX/DGz;

    .line 1981165
    iget-object p1, p0, LX/DH0;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/DGz;->d:LX/20Z;

    .line 1981166
    sget-object p0, LX/20X;->SHARE:LX/20X;

    invoke-interface {p1, v0, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 1981167
    goto :goto_0

    .line 1981168
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1981169
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1981170
    check-cast v1, LX/DGz;

    .line 1981171
    iget-object p1, p0, LX/DH0;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/DGz;->d:LX/20Z;

    .line 1981172
    sget-object p0, LX/20X;->SAVE:LX/20X;

    invoke-interface {p1, v0, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 1981173
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x41aaf3c1 -> :sswitch_0
        0x41aaf5ef -> :sswitch_1
    .end sparse-switch
.end method
