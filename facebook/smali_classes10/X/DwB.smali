.class public LX/DwB;
.super Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;
.source ""


# instance fields
.field private k:D

.field private l:D

.field private m:D

.field private n:D

.field private o:D

.field private p:D

.field private q:D

.field private r:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 2060058
    invoke-direct {p0, p1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;-><init>(Landroid/content/Context;)V

    .line 2060059
    iput-wide v0, p0, LX/DwB;->k:D

    .line 2060060
    iput-wide v0, p0, LX/DwB;->l:D

    .line 2060061
    iput-wide v0, p0, LX/DwB;->m:D

    .line 2060062
    iput-wide v0, p0, LX/DwB;->n:D

    .line 2060063
    iput-wide v0, p0, LX/DwB;->o:D

    .line 2060064
    iput-wide v0, p0, LX/DwB;->p:D

    .line 2060065
    iput-wide v0, p0, LX/DwB;->q:D

    .line 2060066
    invoke-virtual {p0}, LX/DwB;->a()V

    .line 2060067
    return-void
.end method

.method private a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2060053
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->H()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2060054
    :cond_0
    :goto_0
    return-void

    .line 2060055
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    iget-wide v2, p0, LX/DwB;->m:D

    double-to-int v0, v2

    iget-wide v2, p0, LX/DwB;->n:D

    double-to-int v2, v2

    invoke-direct {v1, v4, v4, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2060056
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->H()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060057
    iget-object v3, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    const-string v5, "LoadLandscapeImageThumbnail"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2060047
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2060048
    :cond_0
    :goto_0
    return-void

    .line 2060049
    :cond_1
    iget-wide v0, p0, LX/DwB;->o:D

    iget-object v2, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget-object v2, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iput-wide v0, p0, LX/DwB;->p:D

    .line 2060050
    new-instance v1, Landroid/graphics/Rect;

    iget-wide v2, p0, LX/DwB;->o:D

    double-to-int v0, v2

    iget-wide v2, p0, LX/DwB;->p:D

    double-to-int v2, v2

    invoke-direct {v1, v4, v4, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2060051
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060052
    iget-object v3, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    const-string v5, "LoadPortraitImageThumbnail"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private c(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2060009
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2060010
    :cond_0
    :goto_0
    return-void

    .line 2060011
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    iget-wide v2, p0, LX/DwB;->q:D

    double-to-int v0, v2

    iget-wide v2, p0, LX/DwB;->q:D

    double-to-int v2, v2

    invoke-direct {v1, v4, v4, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2060012
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060013
    iget-object v3, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    const-string v5, "LoadSquareImageThumbnail"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 2060038
    invoke-super {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a()V

    .line 2060039
    invoke-virtual {p0}, LX/DwB;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2060040
    invoke-virtual {p0}, LX/DwB;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, LX/DwB;->k:D

    .line 2060041
    int-to-double v2, v0

    iget-wide v4, p0, LX/DwB;->k:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    iput-wide v2, p0, LX/DwB;->l:D

    .line 2060042
    int-to-double v0, v0

    iput-wide v0, p0, LX/DwB;->m:D

    .line 2060043
    iget-wide v0, p0, LX/DwB;->l:D

    mul-double/2addr v0, v6

    iget-wide v2, p0, LX/DwB;->k:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/DwB;->n:D

    .line 2060044
    iget-wide v0, p0, LX/DwB;->m:D

    iput-wide v0, p0, LX/DwB;->o:D

    .line 2060045
    iget-wide v0, p0, LX/DwB;->m:D

    iput-wide v0, p0, LX/DwB;->q:D

    .line 2060046
    return-void
.end method

.method public final a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V
    .locals 3
    .param p8    # LX/Dw7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060024
    invoke-super/range {p0 .. p8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2060025
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2060026
    :cond_0
    :goto_0
    return-void

    .line 2060027
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c()V

    .line 2060028
    iput-object p1, p0, LX/DwB;->r:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2060029
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060030
    if-eqz v0, :cond_0

    .line 2060031
    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v2, LX/Dvw;->LANDSCAPE:LX/Dvw;

    if-ne v1, v2, :cond_2

    .line 2060032
    invoke-direct {p0, v0}, LX/DwB;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V

    .line 2060033
    :goto_1
    invoke-virtual {p0}, LX/DwB;->forceLayout()V

    .line 2060034
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b()V

    goto :goto_0

    .line 2060035
    :cond_2
    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v2, LX/Dvw;->PORTRAIT:LX/Dvw;

    if-ne v1, v2, :cond_3

    .line 2060036
    invoke-direct {p0, v0}, LX/DwB;->b(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V

    goto :goto_1

    .line 2060037
    :cond_3
    invoke-direct {p0, v0}, LX/DwB;->c(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;)V

    goto :goto_1
.end method

.method public getRowHeight()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2060014
    iget-object v0, p0, LX/DwB;->r:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060015
    if-nez v0, :cond_0

    move v0, v1

    .line 2060016
    :goto_0
    return v0

    .line 2060017
    :cond_0
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v3, LX/Dvw;->LANDSCAPE:LX/Dvw;

    if-ne v2, v3, :cond_1

    .line 2060018
    iget-wide v0, p0, LX/DwB;->n:D

    double-to-int v0, v0

    goto :goto_0

    .line 2060019
    :cond_1
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v3, LX/Dvw;->PORTRAIT:LX/Dvw;

    if-ne v2, v3, :cond_2

    .line 2060020
    iget-wide v0, p0, LX/DwB;->p:D

    double-to-int v0, v0

    goto :goto_0

    .line 2060021
    :cond_2
    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v2, LX/Dvw;->SQUARE:LX/Dvw;

    if-ne v0, v2, :cond_3

    .line 2060022
    iget-wide v0, p0, LX/DwB;->q:D

    double-to-int v0, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2060023
    goto :goto_0
.end method
