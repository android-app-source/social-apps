.class public final LX/Dn1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dju;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;)V
    .locals 0

    .prologue
    .line 2039967
    iput-object p1, p0, LX/Dn1;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iput-object p2, p0, LX/Dn1;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;)V
    .locals 4
    .param p1    # Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039968
    if-eqz p1, :cond_0

    .line 2039969
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2039970
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2039971
    iget-object v2, p0, LX/Dn1;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    new-instance v3, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    invoke-direct {v3, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039972
    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    .line 2039973
    iget-object v0, p0, LX/Dn1;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2039974
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    .line 2039975
    iget-object v0, p0, LX/Dn1;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2039976
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2039977
    iget-object v0, p0, LX/Dn1;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2039978
    :cond_0
    return-void
.end method
