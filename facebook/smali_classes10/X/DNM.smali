.class public final LX/DNM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/44w",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        "LX/0ta;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DNR;


# direct methods
.method public constructor <init>(LX/DNR;)V
    .locals 0

    .prologue
    .line 1991402
    iput-object p1, p0, LX/DNM;->a:LX/DNR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1991403
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-boolean v0, v0, LX/DNR;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    if-eqz v0, :cond_0

    .line 1991404
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    .line 1991405
    sget-object v1, LX/2lT;->FRESH_STORIES:LX/2lT;

    invoke-static {v0, v1}, LX/2lS;->a(LX/2lS;LX/2lT;)V

    .line 1991406
    :cond_0
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->o:LX/16I;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->p:LX/2hU;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->o:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1991407
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->p:LX/2hU;

    new-instance v1, LX/BEF;

    iget-object v2, p0, LX/DNM;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->q:Landroid/app/Activity;

    invoke-direct {v1, v2}, LX/BEF;-><init>(Landroid/content/Context;)V

    const/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 1991408
    :cond_1
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-boolean v0, v0, LX/DNR;->w:Z

    if-eqz v0, :cond_5

    const-string v0, "cold_start_cursor"

    .line 1991409
    :goto_0
    iget-object v1, p0, LX/DNM;->a:LX/DNR;

    invoke-static {v1}, LX/DNR;->p(LX/DNR;)V

    .line 1991410
    iget-object v1, p0, LX/DNM;->a:LX/DNR;

    iget-object v1, v1, LX/DNR;->r:LX/DNT;

    iget-object v2, p0, LX/DNM;->a:LX/DNR;

    iget-boolean v2, v2, LX/DNR;->z:Z

    iget-object v3, p0, LX/DNM;->a:LX/DNR;

    iget-boolean v3, v3, LX/DNR;->w:Z

    .line 1991411
    if-eqz v2, :cond_2

    .line 1991412
    iget-object v4, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991413
    iget-object v5, v4, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v4, v5

    .line 1991414
    check-cast v4, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    .line 1991415
    iget-object v5, v4, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1991416
    iget-object v5, v4, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 1991417
    :cond_2
    new-instance v5, LX/0rT;

    invoke-direct {v5}, LX/0rT;-><init>()V

    if-eqz v3, :cond_6

    iget v4, v1, LX/DNT;->f:I

    .line 1991418
    :goto_1
    iput v4, v5, LX/0rT;->c:I

    .line 1991419
    move-object v4, v5

    .line 1991420
    iget-object v5, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991421
    iput-object v5, v4, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1991422
    move-object v4, v4

    .line 1991423
    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1991424
    iput-object v5, v4, LX/0rT;->a:LX/0rS;

    .line 1991425
    move-object v4, v4

    .line 1991426
    iget-object v5, v1, LX/DNT;->h:Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991427
    iput-object v5, v4, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991428
    move-object v4, v4

    .line 1991429
    if-eqz v2, :cond_7

    .line 1991430
    sget-object v5, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    .line 1991431
    :goto_2
    move-object v5, v5

    .line 1991432
    invoke-virtual {v4, v5}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v4

    .line 1991433
    iput-object v0, v4, LX/0rT;->g:Ljava/lang/String;

    .line 1991434
    move-object v4, v4

    .line 1991435
    iget-object v5, v1, LX/DNT;->d:LX/1BY;

    invoke-virtual {v5}, LX/1BY;->a()LX/0Px;

    move-result-object v5

    .line 1991436
    iput-object v5, v4, LX/0rT;->n:LX/0Px;

    .line 1991437
    move-object v4, v4

    .line 1991438
    iget-object v5, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991439
    iget-object v6, v5, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v5, v6

    .line 1991440
    sget-object v6, Lcom/facebook/api/feedtype/FeedType$Name;->v:Lcom/facebook/api/feedtype/FeedType$Name;

    if-eq v5, v6, :cond_3

    iget-object v5, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991441
    iget-object v6, v5, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v5, v6

    .line 1991442
    sget-object v6, Lcom/facebook/api/feedtype/FeedType$Name;->q:Lcom/facebook/api/feedtype/FeedType$Name;

    if-eq v5, v6, :cond_3

    iget-object v5, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991443
    iget-object v6, v5, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v5, v6

    .line 1991444
    sget-object v6, Lcom/facebook/api/feedtype/FeedType$Name;->t:Lcom/facebook/api/feedtype/FeedType$Name;

    if-ne v5, v6, :cond_4

    .line 1991445
    :cond_3
    const/4 v5, 0x0

    .line 1991446
    iput-object v5, v4, LX/0rT;->g:Ljava/lang/String;

    .line 1991447
    :cond_4
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1991448
    const-string v6, "fetchFeedParams"

    invoke-virtual {v4}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1991449
    iget-object v4, v1, LX/DNT;->b:LX/0aG;

    const-string v6, "feed_fetch_news_feed_before"

    const p0, -0xf2354ed

    invoke-static {v4, v6, v5, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v4

    .line 1991450
    sget-object v5, LX/DNT;->a:LX/DNS;

    iget-object v6, v1, LX/DNT;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 1991451
    return-object v0

    .line 1991452
    :cond_5
    iget-object v0, p0, LX/DNM;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->j()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1991453
    :cond_6
    iget v4, v1, LX/DNT;->e:I

    goto :goto_1

    .line 1991454
    :cond_7
    if-eqz v3, :cond_8

    .line 1991455
    sget-object v5, LX/0gf;->INITIALIZATION:LX/0gf;

    goto :goto_2

    .line 1991456
    :cond_8
    sget-object v5, LX/0gf;->SCROLLING:LX/0gf;

    goto :goto_2
.end method
