.class public final LX/EpP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V
    .locals 0

    .prologue
    .line 2170252
    iput-object p1, p0, LX/EpP;->a:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 13
    .param p1    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2170253
    iget-object v0, p0, LX/EpP;->a:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->a:LX/EpN;

    if-eqz v0, :cond_0

    .line 2170254
    iget-object v0, p0, LX/EpP;->a:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->a:LX/EpN;

    iget-object v1, p0, LX/EpP;->a:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    .line 2170255
    invoke-virtual {v1}, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/5wM;

    .line 2170256
    iget-object v2, v0, LX/EpN;->i:LX/1R7;

    invoke-virtual {v2, p1}, LX/1R7;->d(I)I

    move-result v6

    .line 2170257
    if-gez v6, :cond_1

    .line 2170258
    iget-object v2, v0, LX/EpN;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    .line 2170259
    const-string v3, "person_card_action_bar_clicked_removed_item"

    const-string v4, "Clicked on an action bar item that is no longer visible: %s"

    invoke-static {p1}, LX/BS2;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170260
    :cond_0
    :goto_0
    return-void

    .line 2170261
    :cond_1
    iget-object v2, v0, LX/EpN;->f:LX/Emj;

    sget-object v3, LX/Emo;->ACTION_BAR:LX/Emo;

    invoke-interface {v7}, LX/5wM;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, LX/BS2;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    invoke-interface/range {v2 .. v7}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2170262
    iget-object v6, v0, LX/EpN;->h:LX/EpB;

    iget-object v10, v0, LX/EpN;->c:LX/2h7;

    iget-object v11, v0, LX/EpN;->d:LX/5P2;

    iget-object v12, v0, LX/EpN;->b:LX/EpL;

    move-object v8, v1

    move v9, p1

    invoke-interface/range {v6 .. v12}, LX/EpB;->a(Ljava/lang/Object;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V

    goto :goto_0
.end method
