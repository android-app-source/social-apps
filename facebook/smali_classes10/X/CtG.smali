.class public final LX/CtG;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/CtE;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public b:I

.field private c:F

.field private d:F

.field public e:D

.field public f:D

.field private final g:LX/CtF;

.field public h:Z


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1944312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/CtG;-><init>(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944313
    return-void
.end method

.method private constructor <init>(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p3    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1944283
    iput-object p1, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1944284
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CtG;->h:Z

    .line 1944286
    if-eqz p3, :cond_2

    .line 1944287
    sget-object v0, LX/03r;->RichText:[I

    invoke-virtual {p2, p3, v0, p4, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1944288
    const/16 v0, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1944289
    const/16 v0, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1944290
    invoke-direct {p0, v3}, LX/CtG;->setFallbackFont(Landroid/content/res/TypedArray;)V

    .line 1944291
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1944292
    :goto_0
    iget-object v3, p1, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v3, v0}, LX/Cju;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1944293
    iget-object v3, p1, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v3, v0}, LX/Cju;->b(I)F

    move-result v0

    .line 1944294
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 1944295
    invoke-virtual {p0, v1, v0}, LX/CtG;->setTextSize(IF)V

    .line 1944296
    :cond_0
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v0, v2}, LX/Cju;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1944297
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944298
    :cond_1
    new-instance v0, LX/CtF;

    invoke-direct {v0, p0, p0}, LX/CtF;-><init>(LX/CtG;LX/CtG;)V

    iput-object v0, p0, LX/CtG;->g:LX/CtF;

    .line 1944299
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    const v2, 0x7f0d016c

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    .line 1944300
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p0, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1944301
    iput-boolean v1, p0, LX/CtG;->h:Z

    .line 1944302
    return-void

    :cond_2
    move v0, v1

    move v2, v1

    goto :goto_0
.end method

.method private static a(Landroid/text/Layout;)F
    .locals 5

    .prologue
    .line 1944314
    const/4 v1, 0x0

    .line 1944315
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 1944316
    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    move v1, v4

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1944317
    invoke-virtual {p0, v1}, Landroid/text/Layout;->getLineMax(I)F

    move-result v3

    cmpl-float v3, v3, v0

    if-lez v3, :cond_0

    .line 1944318
    invoke-virtual {p0, v1}, Landroid/text/Layout;->getLineMax(I)F

    move-result v0

    .line 1944319
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1944320
    :cond_1
    return v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 1944321
    invoke-virtual {p0}, LX/CtG;->getLineHeight()I

    move-result v0

    invoke-virtual {p0}, LX/CtG;->getLineSpacingExtra()F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    .line 1944322
    iget v1, p0, LX/CtG;->b:I

    sub-int v0, v1, v0

    return v0
.end method

.method private setFallbackFont(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1944323
    if-nez p1, :cond_1

    .line 1944324
    :cond_0
    :goto_0
    return-void

    .line 1944325
    :cond_1
    const/16 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1944326
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1944327
    invoke-static {v0}, LX/8Yg;->a(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1944328
    if-nez v0, :cond_2

    .line 1944329
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 1944330
    :cond_2
    invoke-virtual {p0, v0}, LX/CtG;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method public static setLineHeightScale(LX/CtG;D)V
    .locals 1

    .prologue
    .line 1944331
    iput-wide p1, p0, LX/CtG;->f:D

    .line 1944332
    return-void
.end method

.method public static setTextSizeScale(LX/CtG;D)V
    .locals 1

    .prologue
    .line 1944333
    iput-wide p1, p0, LX/CtG;->e:D

    .line 1944334
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1944335
    iget-object v0, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1944336
    iput v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    .line 1944337
    iget-object v0, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1944338
    iput v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->h:I

    .line 1944339
    const-string v0, ""

    invoke-virtual {p0, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1944340
    iget-object v0, p0, LX/CtG;->g:LX/CtF;

    invoke-virtual {v0, p0}, LX/CtF;->a(LX/CtG;)V

    .line 1944341
    const v0, 0x800033

    invoke-virtual {p0, v0}, LX/CtG;->setGravity(I)V

    .line 1944342
    return-void
.end method

.method public final a(D)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1944343
    iget-wide v0, p0, LX/CtG;->e:D

    cmpl-double v0, v0, p1

    if-eqz v0, :cond_1

    cmpl-double v0, p1, v2

    if-eqz v0, :cond_1

    .line 1944344
    iget-wide v0, p0, LX/CtG;->e:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1944345
    invoke-virtual {p0}, LX/CtG;->getTextSize()F

    move-result v0

    iput v0, p0, LX/CtG;->c:F

    .line 1944346
    :cond_0
    iput-wide p1, p0, LX/CtG;->e:D

    .line 1944347
    const/4 v0, 0x0

    iget v1, p0, LX/CtG;->c:F

    double-to-float v2, p1

    mul-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, LX/CtG;->setTextSize(IF)V

    .line 1944348
    :cond_1
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1944349
    iput p1, p0, LX/CtG;->b:I

    .line 1944350
    if-gtz p1, :cond_0

    .line 1944351
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/CtG;->setLineSpacing(FF)V

    .line 1944352
    :goto_0
    return-void

    .line 1944353
    :cond_0
    invoke-direct {p0}, LX/CtG;->b()I

    move-result v0

    .line 1944354
    int-to-float v0, v0

    invoke-virtual {p0, v0, v1}, LX/CtG;->setLineSpacing(FF)V

    goto :goto_0
.end method

.method public final b(D)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1944303
    iget-wide v0, p0, LX/CtG;->f:D

    cmpl-double v0, v0, p1

    if-eqz v0, :cond_1

    cmpl-double v0, p1, v2

    if-eqz v0, :cond_1

    .line 1944304
    iget-wide v0, p0, LX/CtG;->f:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1944305
    iget v0, p0, LX/CtG;->b:I

    int-to-float v0, v0

    iput v0, p0, LX/CtG;->d:F

    .line 1944306
    :cond_0
    iput-wide p1, p0, LX/CtG;->f:D

    .line 1944307
    iget v0, p0, LX/CtG;->d:F

    float-to-double v0, v0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1944308
    invoke-virtual {p0}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v1

    .line 1944309
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944310
    :cond_1
    return-void
.end method

.method public getRichTextLineHeight()I
    .locals 1

    .prologue
    .line 1944311
    iget v0, p0, LX/CtG;->b:I

    return v0
.end method

.method public getWindowAttachmentCount()I
    .locals 1

    .prologue
    .line 1944238
    invoke-virtual {p0}, LX/CtG;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1944239
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 1944240
    invoke-virtual {p0}, LX/CtG;->getLineSpacingExtra()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1944241
    invoke-virtual {p0}, LX/CtG;->getBottom()I

    move-result v0

    invoke-virtual {p0}, LX/CtG;->getLineSpacingExtra()F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/CtG;->setBottom(I)V

    .line 1944242
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2ed4c2a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1944243
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1944244
    iget-boolean v1, p0, LX/CtG;->h:Z

    if-eqz v1, :cond_0

    .line 1944245
    const/16 v1, 0x2d

    const v2, 0x3bf0c40f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1944246
    :goto_0
    return-void

    .line 1944247
    :cond_0
    invoke-virtual {p0}, LX/CtG;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1944248
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1944249
    :cond_1
    const v1, 0x1c1e05be

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0

    .line 1944250
    :cond_2
    invoke-static {v1}, LX/CtG;->a(Landroid/text/Layout;)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {p0}, LX/CtG;->getCompoundPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, LX/CtG;->getCompoundPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 1944251
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1944252
    const v1, 0x3a1e66f1

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public setMatchParentWidth(Z)V
    .locals 0

    .prologue
    .line 1944253
    iput-boolean p1, p0, LX/CtG;->h:Z

    .line 1944254
    return-void
.end method

.method public setText(LX/Clf;)V
    .locals 1

    .prologue
    .line 1944255
    iget-object v0, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->a:LX/CIg;

    invoke-virtual {v0, p0, p1}, LX/CIg;->a(Landroid/widget/TextView;LX/Clf;)V

    .line 1944256
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1944257
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1944258
    sget-object v0, LX/03r;->RichText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1944259
    if-eqz v3, :cond_2

    .line 1944260
    const/16 v0, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1944261
    const/16 v0, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1944262
    invoke-direct {p0, v3}, LX/CtG;->setFallbackFont(Landroid/content/res/TypedArray;)V

    .line 1944263
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1944264
    :goto_0
    iget-object v3, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget-object v3, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v3, v0}, LX/Cju;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1944265
    iget-object v3, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget-object v3, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v3, v0}, LX/Cju;->b(I)F

    move-result v0

    .line 1944266
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 1944267
    invoke-virtual {p0, v1, v0}, LX/CtG;->setTextSize(IF)V

    .line 1944268
    :cond_0
    iget-object v0, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v0, v2}, LX/Cju;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1944269
    iget-object v0, p0, LX/CtG;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944270
    :cond_1
    return-void

    :cond_2
    move v0, v1

    move v2, v1

    goto :goto_0
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 1944271
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 1944272
    iget v0, p0, LX/CtG;->b:I

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944273
    return-void
.end method

.method public final setTextSize(IF)V
    .locals 1

    .prologue
    .line 1944274
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1944275
    iget v0, p0, LX/CtG;->b:I

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944276
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1944277
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1944278
    iget v0, p0, LX/CtG;->b:I

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944279
    return-void
.end method

.method public final setTypeface(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 1944280
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1944281
    iget v0, p0, LX/CtG;->b:I

    invoke-virtual {p0, v0}, LX/CtG;->a(I)V

    .line 1944282
    return-void
.end method
