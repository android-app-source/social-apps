.class public final LX/EZF;
.super Ljava/util/AbstractSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EZ8;


# direct methods
.method public constructor <init>(LX/EZ8;)V
    .locals 0

    .prologue
    .line 2138884
    iput-object p1, p0, LX/EZF;->a:LX/EZ8;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2138885
    check-cast p1, Ljava/util/Map$Entry;

    .line 2138886
    invoke-virtual {p0, p1}, LX/EZF;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2138887
    iget-object v1, p0, LX/EZF;->a:LX/EZ8;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138888
    const/4 v0, 0x1

    .line 2138889
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 2138890
    iget-object v0, p0, LX/EZF;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->clear()V

    .line 2138891
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2138892
    check-cast p1, Ljava/util/Map$Entry;

    .line 2138893
    iget-object v0, p0, LX/EZF;->a:LX/EZ8;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EZ8;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2138894
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 2138895
    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 2138896
    new-instance v0, LX/EZE;

    iget-object v1, p0, LX/EZF;->a:LX/EZ8;

    invoke-direct {v0, v1}, LX/EZE;-><init>(LX/EZ8;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2138897
    check-cast p1, Ljava/util/Map$Entry;

    .line 2138898
    invoke-virtual {p0, p1}, LX/EZF;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138899
    iget-object v0, p0, LX/EZF;->a:LX/EZ8;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EZ8;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138900
    const/4 v0, 0x1

    .line 2138901
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2138902
    iget-object v0, p0, LX/EZF;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->size()I

    move-result v0

    return v0
.end method
