.class public final LX/EsV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EsW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyScope;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/EsW;


# direct methods
.method public constructor <init>(LX/EsW;)V
    .locals 1

    .prologue
    .line 2175710
    iput-object p1, p0, LX/EsV;->b:LX/EsW;

    .line 2175711
    move-object v0, p1

    .line 2175712
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2175713
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2175714
    const-string v0, "ThrowbackPrivacyLabelComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2175715
    if-ne p0, p1, :cond_1

    .line 2175716
    :cond_0
    :goto_0
    return v0

    .line 2175717
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2175718
    goto :goto_0

    .line 2175719
    :cond_3
    check-cast p1, LX/EsV;

    .line 2175720
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2175721
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2175722
    if-eq v2, v3, :cond_0

    .line 2175723
    iget-object v2, p0, LX/EsV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EsV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/EsV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2175724
    goto :goto_0

    .line 2175725
    :cond_4
    iget-object v2, p1, LX/EsV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
