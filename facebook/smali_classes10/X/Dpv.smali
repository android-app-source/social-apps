.class public final LX/Dpv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39A;


# instance fields
.field public final synthetic a:LX/Dpx;


# direct methods
.method public constructor <init>(LX/Dpx;)V
    .locals 0

    .prologue
    .line 2047701
    iput-object p1, p0, LX/Dpv;->a:LX/Dpx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2047702
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2047703
    iget-object v0, p0, LX/Dpv;->a:LX/Dpx;

    check-cast p1, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;

    .line 2047704
    iget-object v3, p1, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->a:Ljava/lang/String;

    .line 2047705
    iget-boolean v1, p1, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;->b:Z

    .line 2047706
    if-eqz v1, :cond_0

    .line 2047707
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page/"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 2047708
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string p0, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2047709
    iget-object v1, v0, LX/Dpx;->c:Landroid/content/Context;

    invoke-static {v1, v2}, LX/2A3;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p0

    .line 2047710
    if-nez p0, :cond_3

    .line 2047711
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v0, LX/Dpx;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/Dpx;->e:LX/DbA;

    invoke-virtual {v1}, LX/DbA;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile.php"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2047712
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2047713
    const-string v1, "id"

    invoke-virtual {v2, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2047714
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2047715
    :goto_2
    if-eqz p0, :cond_2

    .line 2047716
    iget-object v2, v0, LX/Dpx;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/Dpx;->c:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2047717
    :goto_3
    return-void

    .line 2047718
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile/"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    goto :goto_0

    .line 2047719
    :cond_1
    iget-object v1, v0, LX/Dpx;->e:LX/DbA;

    invoke-virtual {v1}, LX/DbA;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2047720
    :cond_2
    iget-object v2, v0, LX/Dpx;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/Dpx;->c:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_3

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method
