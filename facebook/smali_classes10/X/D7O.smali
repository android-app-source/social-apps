.class public final LX/D7O;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1967076
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1967077
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1967078
    if-eqz v0, :cond_0

    .line 1967079
    const-string v1, "viewer_has_chosen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1967080
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1967081
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1967082
    if-eqz v0, :cond_1

    .line 1967083
    const-string v1, "vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1967084
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1967085
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1967086
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1967087
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1967088
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1967089
    :goto_0
    return v1

    .line 1967090
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 1967091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1967092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1967093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1967094
    const-string v7, "viewer_has_chosen"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1967095
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 1967096
    :cond_1
    const-string v7, "vote_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1967097
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1967098
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1967099
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1967100
    if-eqz v3, :cond_4

    .line 1967101
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1967102
    :cond_4
    if-eqz v0, :cond_5

    .line 1967103
    invoke-virtual {p1, v2, v4, v1}, LX/186;->a(III)V

    .line 1967104
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1967105
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1967106
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1967107
    if-eqz v0, :cond_1

    .line 1967108
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1967109
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1967110
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 1967111
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/D7O;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1967112
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1967113
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1967114
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1967115
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1967116
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1967117
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1967118
    :goto_0
    return v1

    .line 1967119
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1967120
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1967121
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1967122
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1967123
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1967124
    const-string v3, "node"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1967125
    invoke-static {p0, p1}, LX/D7O;->a$redex0(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1967126
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1967127
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1967128
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1967129
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1967130
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1967131
    if-eqz v0, :cond_0

    .line 1967132
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1967133
    invoke-static {p0, v0, p2}, LX/D7O;->a(LX/15i;ILX/0nX;)V

    .line 1967134
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1967135
    return-void
.end method
