.class public LX/Cig;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/Cih;",
        "LX/Cif;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Cig;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1928852
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1928853
    return-void
.end method

.method public static a(LX/0QB;)LX/Cig;
    .locals 3

    .prologue
    .line 1928854
    sget-object v0, LX/Cig;->a:LX/Cig;

    if-nez v0, :cond_1

    .line 1928855
    const-class v1, LX/Cig;

    monitor-enter v1

    .line 1928856
    :try_start_0
    sget-object v0, LX/Cig;->a:LX/Cig;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1928857
    if-eqz v2, :cond_0

    .line 1928858
    :try_start_1
    new-instance v0, LX/Cig;

    invoke-direct {v0}, LX/Cig;-><init>()V

    .line 1928859
    move-object v0, v0

    .line 1928860
    sput-object v0, LX/Cig;->a:LX/Cig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928861
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1928862
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1928863
    :cond_1
    sget-object v0, LX/Cig;->a:LX/Cig;

    return-object v0

    .line 1928864
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1928865
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
