.class public LX/Dov;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042786
    iput-object p1, p0, LX/Dov;->a:LX/0Or;

    .line 2042787
    return-void
.end method

.method private static a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 2042788
    if-eqz p0, :cond_0

    .line 2042789
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2042790
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2042791
    :try_start_0
    const-string v3, "SELECT COUNT(*) FROM messages"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2042792
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2042793
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 2042794
    if-lez v3, :cond_0

    .line 2042795
    :goto_0
    invoke-static {v2}, LX/Dov;->a(Landroid/database/Cursor;)V

    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2042796
    goto :goto_0

    .line 2042797
    :cond_1
    invoke-static {v2}, LX/Dov;->a(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, LX/Dov;->a(Landroid/database/Cursor;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
