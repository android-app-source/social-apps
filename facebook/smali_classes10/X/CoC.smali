.class public LX/CoC;
.super LX/3x6;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/CkJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1935221
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1935222
    const-class v0, LX/CoC;

    invoke-static {v0, p0, p1}, LX/CoC;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1935223
    iget-object v0, p0, LX/CoC;->b:LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, LX/CoC;->c:I

    .line 1935224
    return-void
.end method

.method private a(Landroid/view/View;ILX/Clr;LX/Clr;)I
    .locals 7

    .prologue
    .line 1935225
    invoke-static {p3}, LX/Cjt;->from(LX/Clr;)LX/Cjt;

    move-result-object v1

    .line 1935226
    invoke-static {p4}, LX/Cjt;->from(LX/Clr;)LX/Cjt;

    move-result-object v4

    .line 1935227
    if-nez p2, :cond_1

    sget-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

    if-eq v1, v0, :cond_0

    sget-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

    if-eq v1, v0, :cond_0

    sget-object v0, LX/Cjt;->MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

    if-eq v1, v0, :cond_0

    sget-object v0, LX/Cjt;->MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

    if-ne v1, v0, :cond_1

    .line 1935228
    :cond_0
    const/4 v0, 0x0

    .line 1935229
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/CoC;->a:LX/CkJ;

    const/4 v6, 0x0

    move-object v2, p3

    move-object v3, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, LX/CkJ;->a(LX/Cjt;LX/Clr;Landroid/view/View;LX/Cjt;LX/Clr;Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/CoJ;I)LX/Clr;
    .locals 1

    .prologue
    .line 1935230
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1935231
    :cond_0
    const/4 v0, 0x0

    .line 1935232
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CoC;

    invoke-static {p0}, LX/CkJ;->a(LX/0QB;)LX/CkJ;

    move-result-object v0

    check-cast v0, LX/CkJ;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object v0, p1, LX/CoC;->a:LX/CkJ;

    iput-object p0, p1, LX/CoC;->b:LX/Cju;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1935233
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v8

    .line 1935234
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move v7, v6

    .line 1935235
    :goto_0
    if-ge v7, v8, :cond_a

    .line 1935236
    invoke-virtual {p2, v7}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1935237
    invoke-virtual {p2, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    check-cast v0, LX/Cs4;

    .line 1935238
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1935239
    iget-object v1, v0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 1935240
    instance-of v0, v1, LX/CoW;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1935241
    check-cast v0, LX/CoW;

    .line 1935242
    invoke-interface {v0}, LX/CoW;->a()I

    move-result v4

    .line 1935243
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1935244
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1935245
    if-nez v7, :cond_1

    move v2, v6

    .line 1935246
    :goto_1
    add-int/lit8 v0, v7, 0x1

    if-ne v0, v8, :cond_6

    .line 1935247
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    .line 1935248
    :goto_2
    const/4 v1, 0x0

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1935249
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1935250
    :cond_1
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1935251
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    check-cast v0, LX/Cs4;

    .line 1935252
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1935253
    iget-object v2, v0, LX/CnT;->d:LX/CnG;

    move-object v0, v2

    .line 1935254
    instance-of v2, v0, LX/CoW;

    if-nez v2, :cond_2

    instance-of v2, v1, LX/Con;

    if-eqz v2, :cond_2

    .line 1935255
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    move-object v0, v1

    check-cast v0, LX/Con;

    .line 1935256
    iget v9, v0, LX/Con;->c:I

    move v0, v9

    .line 1935257
    sub-int v0, v2, v0

    move v2, v0

    goto :goto_1

    .line 1935258
    :cond_2
    instance-of v2, v0, LX/CoW;

    if-eqz v2, :cond_3

    check-cast v0, LX/CoW;

    invoke-interface {v0}, LX/CoW;->a()I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 1935259
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 1935260
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1935261
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1935262
    sub-int v9, v2, v0

    .line 1935263
    const/4 v0, 0x1

    .line 1935264
    and-int/lit8 p0, v9, 0x1

    if-ne p0, v0, :cond_b

    :goto_3
    move v0, v0

    .line 1935265
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    add-int/2addr v0, v9

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    move v2, v0

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_4

    .line 1935266
    :cond_6
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1935267
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    check-cast v0, LX/Cs4;

    .line 1935268
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1935269
    iget-object v9, v0, LX/CnT;->d:LX/CnG;

    move-object v0, v9

    .line 1935270
    instance-of v9, v0, LX/CoW;

    if-nez v9, :cond_7

    instance-of v9, v1, LX/Con;

    if-eqz v9, :cond_7

    .line 1935271
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    check-cast v1, LX/Con;

    .line 1935272
    iget v3, v1, LX/Con;->c:I

    move v1, v3

    .line 1935273
    add-int/2addr v0, v1

    goto/16 :goto_2

    .line 1935274
    :cond_7
    instance-of v1, v0, LX/CoW;

    if-eqz v1, :cond_8

    check-cast v0, LX/CoW;

    invoke-interface {v0}, LX/CoW;->a()I

    move-result v0

    if-eq v0, v4, :cond_9

    .line 1935275
    :cond_8
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    goto/16 :goto_2

    .line 1935276
    :cond_9
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1935277
    add-int/lit8 v1, v7, 0x1

    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1935278
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto/16 :goto_2

    .line 1935279
    :cond_a
    return-void

    :cond_b
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1935280
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v1

    .line 1935281
    iget-object v0, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1935282
    check-cast v0, LX/CoJ;

    .line 1935283
    invoke-static {v0, v1}, LX/CoC;->a(LX/CoJ;I)LX/Clr;

    move-result-object v2

    .line 1935284
    iget-object v3, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v3

    .line 1935285
    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    .line 1935286
    iget v0, p0, LX/CoC;->c:I

    invoke-virtual {p1, v4, v4, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1935287
    :goto_0
    return-void

    .line 1935288
    :cond_0
    add-int/lit8 v3, v1, 0x1

    invoke-static {v0, v3}, LX/CoC;->a(LX/CoJ;I)LX/Clr;

    move-result-object v0

    .line 1935289
    invoke-direct {p0, p2, v1, v2, v0}, LX/CoC;->a(Landroid/view/View;ILX/Clr;LX/Clr;)I

    move-result v0

    .line 1935290
    invoke-virtual {p1, v4, v4, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method
