.class public final enum LX/EmZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EmZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EmZ;

.field public static final enum CANCEL_DOWNLOAD:LX/EmZ;

.field public static final enum CREATED_FILE:LX/EmZ;

.field public static final enum DOWNLOAD_AT_EXTERNAL_DESTINATION:LX/EmZ;

.field public static final enum QUEUE_DOWNLOAD:LX/EmZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2165954
    new-instance v0, LX/EmZ;

    const-string v1, "QUEUE_DOWNLOAD"

    invoke-direct {v0, v1, v2}, LX/EmZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmZ;->QUEUE_DOWNLOAD:LX/EmZ;

    .line 2165955
    new-instance v0, LX/EmZ;

    const-string v1, "CANCEL_DOWNLOAD"

    invoke-direct {v0, v1, v3}, LX/EmZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmZ;->CANCEL_DOWNLOAD:LX/EmZ;

    .line 2165956
    new-instance v0, LX/EmZ;

    const-string v1, "DOWNLOAD_AT_EXTERNAL_DESTINATION"

    invoke-direct {v0, v1, v4}, LX/EmZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmZ;->DOWNLOAD_AT_EXTERNAL_DESTINATION:LX/EmZ;

    .line 2165957
    new-instance v0, LX/EmZ;

    const-string v1, "CREATED_FILE"

    invoke-direct {v0, v1, v5}, LX/EmZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmZ;->CREATED_FILE:LX/EmZ;

    .line 2165958
    const/4 v0, 0x4

    new-array v0, v0, [LX/EmZ;

    sget-object v1, LX/EmZ;->QUEUE_DOWNLOAD:LX/EmZ;

    aput-object v1, v0, v2

    sget-object v1, LX/EmZ;->CANCEL_DOWNLOAD:LX/EmZ;

    aput-object v1, v0, v3

    sget-object v1, LX/EmZ;->DOWNLOAD_AT_EXTERNAL_DESTINATION:LX/EmZ;

    aput-object v1, v0, v4

    sget-object v1, LX/EmZ;->CREATED_FILE:LX/EmZ;

    aput-object v1, v0, v5

    sput-object v0, LX/EmZ;->$VALUES:[LX/EmZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2165953
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EmZ;
    .locals 1

    .prologue
    .line 2165952
    const-class v0, LX/EmZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EmZ;

    return-object v0
.end method

.method public static values()[LX/EmZ;
    .locals 1

    .prologue
    .line 2165951
    sget-object v0, LX/EmZ;->$VALUES:[LX/EmZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EmZ;

    return-object v0
.end method
