.class public final LX/DsP;
.super LX/3Eh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2050420
    iput-object p1, p0, LX/DsP;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {p0, p2}, LX/3Eh;-><init>(Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    .line 2050422
    invoke-super {p0, p1}, LX/3Eh;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2050423
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2050424
    if-eqz v0, :cond_1

    .line 2050425
    iget-object v0, p0, LX/DsP;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    .line 2050426
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 2050427
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v2}, LX/3T7;->c()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v1, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2050428
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V

    .line 2050429
    :cond_0
    invoke-static {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->m(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 2050430
    iget-object v1, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x350005

    const/4 p0, 0x2

    invoke-interface {v1, v2, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2050431
    :goto_0
    return-void

    .line 2050432
    :cond_1
    iget-object v0, p0, LX/DsP;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    .line 2050433
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2050434
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 2050435
    invoke-virtual {v2}, LX/1nY;->ordinal()I

    move-result v2

    .line 2050436
    iget-object v3, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v3, v3

    .line 2050437
    if-nez v2, :cond_2

    .line 2050438
    :goto_1
    move-object v1, v1

    .line 2050439
    sget-object v2, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2050440
    iget-object v2, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sync_failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2050441
    iget-object v0, p0, LX/DsP;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->q(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081154

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2050442
    invoke-super {p0, p1}, LX/3Eh;->onFailure(Ljava/lang/Throwable;)V

    .line 2050443
    iget-object v0, p0, LX/DsP;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->q(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 2050444
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2050421
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/DsP;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
