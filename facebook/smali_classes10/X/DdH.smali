.class public final enum LX/DdH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DdH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DdH;

.field public static final enum DELIVERY_RECEIPT:LX/DdH;

.field public static final enum MESSAGE_QUEUED:LX/DdH;

.field public static final enum MESSAGE_SENT:LX/DdH;

.field public static final enum MESSAGE_SENT_DELTA:LX/DdH;

.field public static final enum NOT_PROVIDED:LX/DdH;

.field public static final enum READ_RECEIPT:LX/DdH;

.field public static final enum STICKER_SENT:LX/DdH;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2019413
    new-instance v0, LX/DdH;

    const-string v1, "NOT_PROVIDED"

    invoke-direct {v0, v1, v3}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->NOT_PROVIDED:LX/DdH;

    .line 2019414
    new-instance v0, LX/DdH;

    const-string v1, "READ_RECEIPT"

    invoke-direct {v0, v1, v4}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->READ_RECEIPT:LX/DdH;

    .line 2019415
    new-instance v0, LX/DdH;

    const-string v1, "DELIVERY_RECEIPT"

    invoke-direct {v0, v1, v5}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->DELIVERY_RECEIPT:LX/DdH;

    .line 2019416
    new-instance v0, LX/DdH;

    const-string v1, "MESSAGE_QUEUED"

    invoke-direct {v0, v1, v6}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->MESSAGE_QUEUED:LX/DdH;

    .line 2019417
    new-instance v0, LX/DdH;

    const-string v1, "MESSAGE_SENT"

    invoke-direct {v0, v1, v7}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->MESSAGE_SENT:LX/DdH;

    .line 2019418
    new-instance v0, LX/DdH;

    const-string v1, "STICKER_SENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->STICKER_SENT:LX/DdH;

    .line 2019419
    new-instance v0, LX/DdH;

    const-string v1, "MESSAGE_SENT_DELTA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DdH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdH;->MESSAGE_SENT_DELTA:LX/DdH;

    .line 2019420
    const/4 v0, 0x7

    new-array v0, v0, [LX/DdH;

    sget-object v1, LX/DdH;->NOT_PROVIDED:LX/DdH;

    aput-object v1, v0, v3

    sget-object v1, LX/DdH;->READ_RECEIPT:LX/DdH;

    aput-object v1, v0, v4

    sget-object v1, LX/DdH;->DELIVERY_RECEIPT:LX/DdH;

    aput-object v1, v0, v5

    sget-object v1, LX/DdH;->MESSAGE_QUEUED:LX/DdH;

    aput-object v1, v0, v6

    sget-object v1, LX/DdH;->MESSAGE_SENT:LX/DdH;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DdH;->STICKER_SENT:LX/DdH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DdH;->MESSAGE_SENT_DELTA:LX/DdH;

    aput-object v2, v0, v1

    sput-object v0, LX/DdH;->$VALUES:[LX/DdH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019423
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DdH;
    .locals 1

    .prologue
    .line 2019422
    const-class v0, LX/DdH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DdH;

    return-object v0
.end method

.method public static values()[LX/DdH;
    .locals 1

    .prologue
    .line 2019421
    sget-object v0, LX/DdH;->$VALUES:[LX/DdH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DdH;

    return-object v0
.end method
