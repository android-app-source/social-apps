.class public final LX/DgS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V
    .locals 0

    .prologue
    .line 2029381
    iput-object p1, p0, LX/DgS;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 2029382
    iget-object v0, p0, LX/DgS;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    const/4 p0, 0x0

    const/4 v1, 0x1

    .line 2029383
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    move p1, v1

    .line 2029384
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_1
    move v0, v1

    .line 2029385
    return v0

    :cond_0
    move p1, p0

    .line 2029386
    goto :goto_0

    :cond_1
    move v1, p0

    .line 2029387
    goto :goto_1
.end method
