.class public LX/DKJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/8OJ;

.field public final b:LX/73w;

.field public final c:LX/8Ne;

.field public final d:LX/03V;

.field private final e:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/8OJ;LX/73w;LX/03V;Ljava/util/concurrent/ExecutorService;LX/0cX;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986874
    iput-object p1, p0, LX/DKJ;->a:LX/8OJ;

    .line 1986875
    iput-object p2, p0, LX/DKJ;->b:LX/73w;

    .line 1986876
    new-instance v0, LX/8Nf;

    invoke-direct {v0, p5}, LX/8Nf;-><init>(LX/0cX;)V

    iput-object v0, p0, LX/DKJ;->c:LX/8Ne;

    .line 1986877
    iput-object p3, p0, LX/DKJ;->d:LX/03V;

    .line 1986878
    iput-object p4, p0, LX/DKJ;->e:Ljava/util/concurrent/ExecutorService;

    .line 1986879
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1986855
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1986856
    :try_start_0
    new-instance v0, LX/74k;

    invoke-direct {v0}, LX/74k;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v0

    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    .line 1986857
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1986858
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 1986859
    new-instance v4, LX/8NJ;

    new-instance v5, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    .line 1986860
    iget-wide v10, v0, Lcom/facebook/ipc/media/MediaItem;->e:J

    move-wide v8, v10

    .line 1986861
    invoke-direct {v5, v6, v8, v9}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v4, v5}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1986862
    iput-wide v6, v4, LX/8NJ;->c:J

    .line 1986863
    move-object v0, v4

    .line 1986864
    invoke-virtual {v0}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v0

    .line 1986865
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1986866
    iget-object v0, p0, LX/DKJ;->b:LX/73w;

    invoke-virtual {v0, v2}, LX/73w;->a(Ljava/lang/String;)V

    .line 1986867
    new-instance v0, LX/8OL;

    invoke-direct {v0}, LX/8OL;-><init>()V

    .line 1986868
    iget-object v2, p0, LX/DKJ;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/groups/create/coverphoto/UploadGroupPhotoHandler$1;

    invoke-direct {v4, p0, v3, v1, v0}, Lcom/facebook/groups/create/coverphoto/UploadGroupPhotoHandler$1;-><init>(LX/DKJ;Ljava/util/HashSet;Lcom/google/common/util/concurrent/SettableFuture;LX/8OL;)V

    const v0, -0x4c95c842

    invoke-static {v2, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1986869
    :goto_0
    return-object v1

    .line 1986870
    :catch_0
    move-exception v0

    .line 1986871
    iget-object v2, p0, LX/DKJ;->d:LX/03V;

    const-class v3, LX/DKJ;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to upload group cover photo"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986872
    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
