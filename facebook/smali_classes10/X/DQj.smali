.class public final LX/DQj;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1995171
    const-class v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    const v0, -0x419bad9a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchGroupInfoPageData"

    const-string v6, "092e2f3843e228538bce877946e8b3ce"

    const-string v7, "group_address"

    const-string v8, "10155256044931729"

    const-string v9, "10155259131201729"

    .line 1995172
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1995173
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1995174
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1995175
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1995176
    sparse-switch v0, :sswitch_data_0

    .line 1995177
    :goto_0
    return-object p1

    .line 1995178
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1995179
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1995180
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1995181
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1995182
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1995183
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1995184
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1995185
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1995186
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1995187
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1995188
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1995189
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1995190
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1995191
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1995192
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1995193
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1995194
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1995195
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1995196
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1995197
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1995198
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1995199
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1995200
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1995201
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1995202
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1995203
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7b1f3560 -> :sswitch_6
        -0x6a24640d -> :sswitch_16
        -0x680de62a -> :sswitch_d
        -0x6326fdb3 -> :sswitch_c
        -0x573e7580 -> :sswitch_5
        -0x4496acc9 -> :sswitch_e
        -0x41a91745 -> :sswitch_15
        -0x3eb5f790 -> :sswitch_a
        -0x1b87b280 -> :sswitch_b
        -0x1ac6d3a2 -> :sswitch_14
        -0x12efdeb3 -> :sswitch_f
        -0x8cf3c8a -> :sswitch_4
        0x180aba4 -> :sswitch_18
        0xa1fa812 -> :sswitch_0
        0x1e2e76db -> :sswitch_8
        0x214100e0 -> :sswitch_10
        0x291d8de0 -> :sswitch_19
        0x3052e0ff -> :sswitch_1
        0x4b46b5f1 -> :sswitch_2
        0x4c6d50cb -> :sswitch_11
        0x5679e1cb -> :sswitch_7
        0x5f424068 -> :sswitch_17
        0x61bc9553 -> :sswitch_12
        0x6d2645b9 -> :sswitch_3
        0x73a026b5 -> :sswitch_13
        0x7c41dc05 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1995204
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1995205
    :goto_1
    return v0

    .line 1995206
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1995207
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
