.class public final LX/DOe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/DOl;


# direct methods
.method public constructor <init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1992609
    iput-object p1, p0, LX/DOe;->b:LX/DOl;

    iput-object p2, p0, LX/DOe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1992610
    const v0, 0x7f0824c2

    .line 1992611
    iget-object v1, p0, LX/DOe;->b:LX/DOl;

    iget-object v1, v1, LX/DOl;->c:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1992612
    iget-object v0, p0, LX/DOe;->b:LX/DOl;

    iget-object v0, v0, LX/DOl;->k:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992613
    iget-object v0, p0, LX/DOe;->b:LX/DOl;

    iget-object v0, v0, LX/DOl;->b:LX/0bH;

    new-instance v1, LX/DNw;

    iget-object v2, p0, LX/DOe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, LX/DNw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1992614
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1992615
    iget-object v0, p0, LX/DOe;->b:LX/DOl;

    iget-object v0, v0, LX/DOl;->k:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992616
    iget-object v0, p0, LX/DOe;->b:LX/DOl;

    iget-object v0, v0, LX/DOl;->b:LX/0bH;

    new-instance v1, LX/DNw;

    iget-object v2, p0, LX/DOe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/DNw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1992617
    :cond_0
    iget-object v0, p0, LX/DOe;->b:LX/DOl;

    iget-object v0, v0, LX/DOl;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0824c3

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1992618
    return-void
.end method
