.class public final enum LX/EEm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EEm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EEm;

.field public static final enum Add_People:LX/EEm;

.field public static final enum InCallHeader:LX/EEm;

.field public static final enum NotInCallHeader:LX/EEm;

.field public static final enum Participant:LX/EEm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2094163
    new-instance v0, LX/EEm;

    const-string v1, "Participant"

    invoke-direct {v0, v1, v2}, LX/EEm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEm;->Participant:LX/EEm;

    new-instance v0, LX/EEm;

    const-string v1, "NotInCallHeader"

    invoke-direct {v0, v1, v3}, LX/EEm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEm;->NotInCallHeader:LX/EEm;

    new-instance v0, LX/EEm;

    const-string v1, "InCallHeader"

    invoke-direct {v0, v1, v4}, LX/EEm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEm;->InCallHeader:LX/EEm;

    new-instance v0, LX/EEm;

    const-string v1, "Add_People"

    invoke-direct {v0, v1, v5}, LX/EEm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEm;->Add_People:LX/EEm;

    .line 2094164
    const/4 v0, 0x4

    new-array v0, v0, [LX/EEm;

    sget-object v1, LX/EEm;->Participant:LX/EEm;

    aput-object v1, v0, v2

    sget-object v1, LX/EEm;->NotInCallHeader:LX/EEm;

    aput-object v1, v0, v3

    sget-object v1, LX/EEm;->InCallHeader:LX/EEm;

    aput-object v1, v0, v4

    sget-object v1, LX/EEm;->Add_People:LX/EEm;

    aput-object v1, v0, v5

    sput-object v0, LX/EEm;->$VALUES:[LX/EEm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2094165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EEm;
    .locals 1

    .prologue
    .line 2094166
    const-class v0, LX/EEm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EEm;

    return-object v0
.end method

.method public static values()[LX/EEm;
    .locals 1

    .prologue
    .line 2094167
    sget-object v0, LX/EEm;->$VALUES:[LX/EEm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EEm;

    return-object v0
.end method
