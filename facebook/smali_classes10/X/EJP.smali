.class public final LX/EJP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pr;

.field public final synthetic b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;LX/1Pr;Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)V
    .locals 0

    .prologue
    .line 2103904
    iput-object p1, p0, LX/EJP;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    iput-object p2, p0, LX/EJP;->a:LX/1Pr;

    iput-object p3, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x2

    const v1, -0x4825d5db

    invoke-static {v0, v7, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2103905
    iget-object v0, p0, LX/EJP;->a:LX/1Pr;

    iget-object v1, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-static {v1}, LX/EJT;->a(Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)LX/1KL;

    move-result-object v1

    iget-object v2, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/EJT;

    .line 2103906
    iget-object v0, p0, LX/EJP;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->a:LX/CvY;

    iget-object v1, p0, LX/EJP;->a:LX/1Pr;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v6}, LX/EJT;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/8ch;->EXPAND:LX/8ch;

    :goto_0
    iget-object v3, p0, LX/EJP;->a:LX/1Pr;

    check-cast v3, LX/CxG;

    iget-object v4, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-interface {v3, v4}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    iget-object v5, p0, LX/EJP;->a:LX/1Pr;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    iget-object v5, p0, LX/EJP;->a:LX/1Pr;

    check-cast v5, LX/CxG;

    iget-object v10, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-interface {v5, v10}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v6}, LX/EJT;->a()Z

    move-result v11

    iget-object v5, p0, LX/EJP;->b:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-virtual {v5}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->m()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v9, v10, v11, v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2103907
    invoke-virtual {v6}, LX/EJT;->a()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    .line 2103908
    :goto_1
    iput-boolean v0, v6, LX/EJT;->a:Z

    .line 2103909
    iget-object v0, p0, LX/EJP;->a:LX/1Pr;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2103910
    const v0, -0x2aa6b6b0

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 2103911
    :cond_0
    sget-object v2, LX/8ch;->COLLAPSE:LX/8ch;

    goto :goto_0

    .line 2103912
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
