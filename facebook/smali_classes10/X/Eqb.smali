.class public LX/Eqb;
.super LX/3Mm;
.source ""


# instance fields
.field private a:Z

.field private final b:LX/Er2;

.field private final c:LX/8RL;


# direct methods
.method public constructor <init>(LX/0Zr;LX/8RL;LX/Er2;)V
    .locals 0
    .param p3    # LX/Er2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2171729
    invoke-direct {p0, p1}, LX/3Mm;-><init>(LX/0Zr;)V

    .line 2171730
    iput-object p2, p0, LX/Eqb;->c:LX/8RL;

    .line 2171731
    iput-object p3, p0, LX/Eqb;->b:LX/Er2;

    .line 2171732
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;LX/39y;)V
    .locals 2

    .prologue
    .line 2171733
    iget-boolean v0, p0, LX/Eqb;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p2, LX/39y;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 2171734
    iget-object v1, p0, LX/Eqb;->b:LX/Er2;

    iget-object v0, p2, LX/39y;->a:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    .line 2171735
    iget-object p0, v1, LX/Er2;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance p1, LX/7Hc;

    invoke-direct {p1, v0}, LX/7Hc;-><init>(LX/0Px;)V

    const p2, 0x4a5bbf6e    # 3600347.5f

    invoke-static {p0, p1, p2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2171736
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2171707
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 2171708
    iget-boolean v1, p0, LX/Eqb;->a:Z

    if-eqz v1, :cond_0

    move-object v0, v2

    .line 2171709
    :goto_0
    return-object v0

    .line 2171710
    :cond_0
    iget-object v1, p0, LX/Eqb;->b:LX/Er2;

    .line 2171711
    iget-object v3, v1, LX/Er2;->b:LX/Er3;

    iget-object v3, v3, LX/Er3;->d:LX/0Px;

    move-object v4, v3

    .line 2171712
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2171713
    iput-object v4, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2171714
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    iput v0, v2, LX/39y;->b:I

    move-object v0, v2

    .line 2171715
    goto :goto_0

    .line 2171716
    :cond_1
    iget-object v1, p0, LX/Eqb;->c:LX/8RL;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/8RL;->a(Ljava/lang/String;)V

    .line 2171717
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2171718
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v3, v0

    move v1, v0

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2171719
    iget-object v7, p0, LX/Eqb;->c:LX/8RL;

    invoke-virtual {v7, v0}, LX/8RL;->a(LX/8QL;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2171720
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2171721
    add-int/lit8 v0, v1, 0x1

    .line 2171722
    const/4 v7, 0x3

    move v1, v7

    .line 2171723
    if-ge v0, v1, :cond_2

    .line 2171724
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 2171725
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2171726
    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2171727
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iput v0, v2, LX/39y;->b:I

    move-object v0, v2

    .line 2171728
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method
