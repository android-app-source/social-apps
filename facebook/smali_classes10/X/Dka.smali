.class public LX/Dka;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1Ck;

.field public final c:LX/Dih;

.field public final d:LX/03V;

.field public final e:LX/0kL;

.field public final f:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/Dih;LX/03V;LX/0kL;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2034340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2034341
    iput-object p1, p0, LX/Dka;->a:LX/0tX;

    .line 2034342
    iput-object p2, p0, LX/Dka;->b:LX/1Ck;

    .line 2034343
    iput-object p3, p0, LX/Dka;->c:LX/Dih;

    .line 2034344
    iput-object p4, p0, LX/Dka;->d:LX/03V;

    .line 2034345
    iput-object p5, p0, LX/Dka;->e:LX/0kL;

    .line 2034346
    iput-object p6, p0, LX/Dka;->f:Ljava/util/concurrent/Executor;

    .line 2034347
    return-void
.end method

.method public static b(LX/0QB;)LX/Dka;
    .locals 7

    .prologue
    .line 2034338
    new-instance v0, LX/Dka;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v3

    check-cast v3, LX/Dih;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v6}, LX/Dka;-><init>(LX/0tX;LX/1Ck;LX/Dih;LX/03V;LX/0kL;Ljava/util/concurrent/Executor;)V

    .line 2034339
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2034313
    invoke-static {}, LX/DlU;->a()LX/DlT;

    move-result-object v1

    .line 2034314
    if-nez p5, :cond_0

    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "ADMIN_CANCEL"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    .line 2034315
    :goto_0
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2034316
    iget-object v0, p0, LX/Dka;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034317
    new-instance v1, LX/DkW;

    invoke-direct {v1, p0, p4, p3}, LX/DkW;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/Dka;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2034318
    return-object v0

    .line 2034319
    :cond_0
    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "ADMIN_CANCEL"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/4HK;->f(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2034336
    iget-object v0, p0, LX/Dka;->b:LX/1Ck;

    const-string v1, "user_accept_appointment"

    new-instance v2, LX/DkR;

    invoke-direct {v2, p0, p1, p2, p3}, LX/DkR;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/DkS;

    invoke-direct {v3, p0, p4, p3}, LX/DkS;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2034337
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/DjX;)V
    .locals 8
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/DjX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2034334
    iget-object v6, p0, LX/Dka;->b:LX/1Ck;

    const-string v7, "user_decline_appointment"

    new-instance v0, LX/DkT;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/DkT;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, LX/DkU;

    invoke-direct {v1, p0, p6, p4, p3}, LX/DkU;-><init>(LX/Dka;LX/DjX;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2034335
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2034327
    invoke-static {}, LX/DlU;->a()LX/DlT;

    move-result-object v1

    .line 2034328
    if-nez p5, :cond_0

    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "ADMIN_DECLINE"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    .line 2034329
    :goto_0
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2034330
    iget-object v0, p0, LX/Dka;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034331
    new-instance v1, LX/DkX;

    invoke-direct {v1, p0, p4, p3}, LX/DkX;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/Dka;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2034332
    return-object v0

    .line 2034333
    :cond_0
    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "ADMIN_DECLINE"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/4HK;->f(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2034320
    invoke-static {}, LX/DlU;->a()LX/DlT;

    move-result-object v1

    .line 2034321
    if-nez p5, :cond_0

    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "USER_CANCEL"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    .line 2034322
    :goto_0
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2034323
    iget-object v0, p0, LX/Dka;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034324
    new-instance v1, LX/DkY;

    invoke-direct {v1, p0, p4, p3}, LX/DkY;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/Dka;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2034325
    return-object v0

    .line 2034326
    :cond_0
    new-instance v0, LX/4HK;

    invoke-direct {v0}, LX/4HK;-><init>()V

    invoke-virtual {v0, p1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    const-string v2, "USER_CANCEL"

    invoke-virtual {v0, v2}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/4HK;->f(Ljava/lang/String;)LX/4HK;

    move-result-object v0

    goto :goto_0
.end method
