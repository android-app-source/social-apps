.class public final LX/ELl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ELm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CxA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/CharSequence;

.field public h:Ljava/lang/CharSequence;

.field public i:Z

.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/CharSequence;

.field public final synthetic l:LX/ELm;


# direct methods
.method public constructor <init>(LX/ELm;)V
    .locals 1

    .prologue
    .line 2109639
    iput-object p1, p0, LX/ELl;->l:LX/ELm;

    .line 2109640
    move-object v0, p1

    .line 2109641
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2109642
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2109643
    const-string v0, "SearchResultsPlaceComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2109644
    if-ne p0, p1, :cond_1

    .line 2109645
    :cond_0
    :goto_0
    return v0

    .line 2109646
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2109647
    goto :goto_0

    .line 2109648
    :cond_3
    check-cast p1, LX/ELl;

    .line 2109649
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2109650
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2109651
    if-eq v2, v3, :cond_0

    .line 2109652
    iget-object v2, p0, LX/ELl;->a:LX/CxA;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ELl;->a:LX/CxA;

    iget-object v3, p1, LX/ELl;->a:LX/CxA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2109653
    goto :goto_0

    .line 2109654
    :cond_5
    iget-object v2, p1, LX/ELl;->a:LX/CxA;

    if-nez v2, :cond_4

    .line 2109655
    :cond_6
    iget-object v2, p0, LX/ELl;->b:LX/CzL;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ELl;->b:LX/CzL;

    iget-object v3, p1, LX/ELl;->b:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2109656
    goto :goto_0

    .line 2109657
    :cond_8
    iget-object v2, p1, LX/ELl;->b:LX/CzL;

    if-nez v2, :cond_7

    .line 2109658
    :cond_9
    iget-object v2, p0, LX/ELl;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ELl;->c:Ljava/lang/String;

    iget-object v3, p1, LX/ELl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2109659
    goto :goto_0

    .line 2109660
    :cond_b
    iget-object v2, p1, LX/ELl;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2109661
    :cond_c
    iget-object v2, p0, LX/ELl;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/ELl;->d:Ljava/lang/String;

    iget-object v3, p1, LX/ELl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2109662
    goto :goto_0

    .line 2109663
    :cond_e
    iget-object v2, p1, LX/ELl;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2109664
    :cond_f
    iget-boolean v2, p0, LX/ELl;->e:Z

    iget-boolean v3, p1, LX/ELl;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2109665
    goto :goto_0

    .line 2109666
    :cond_10
    iget-object v2, p0, LX/ELl;->f:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/ELl;->f:Ljava/lang/String;

    iget-object v3, p1, LX/ELl;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2109667
    goto :goto_0

    .line 2109668
    :cond_12
    iget-object v2, p1, LX/ELl;->f:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 2109669
    :cond_13
    iget-object v2, p0, LX/ELl;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/ELl;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELl;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 2109670
    goto/16 :goto_0

    .line 2109671
    :cond_15
    iget-object v2, p1, LX/ELl;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_14

    .line 2109672
    :cond_16
    iget-object v2, p0, LX/ELl;->h:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/ELl;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELl;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 2109673
    goto/16 :goto_0

    .line 2109674
    :cond_18
    iget-object v2, p1, LX/ELl;->h:Ljava/lang/CharSequence;

    if-nez v2, :cond_17

    .line 2109675
    :cond_19
    iget-boolean v2, p0, LX/ELl;->i:Z

    iget-boolean v3, p1, LX/ELl;->i:Z

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 2109676
    goto/16 :goto_0

    .line 2109677
    :cond_1a
    iget-object v2, p0, LX/ELl;->j:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1c

    iget-object v2, p0, LX/ELl;->j:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELl;->j:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 2109678
    goto/16 :goto_0

    .line 2109679
    :cond_1c
    iget-object v2, p1, LX/ELl;->j:Ljava/lang/CharSequence;

    if-nez v2, :cond_1b

    .line 2109680
    :cond_1d
    iget-object v2, p0, LX/ELl;->k:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1e

    iget-object v2, p0, LX/ELl;->k:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELl;->k:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2109681
    goto/16 :goto_0

    .line 2109682
    :cond_1e
    iget-object v2, p1, LX/ELl;->k:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
