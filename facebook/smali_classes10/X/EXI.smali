.class public final enum LX/EXI;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/EXH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EXI;",
        ">;",
        "LX/EXH;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EXI;

.field public static final enum LABEL_OPTIONAL:LX/EXI;

.field public static final enum LABEL_REPEATED:LX/EXI;

.field public static final enum LABEL_REQUIRED:LX/EXI;

.field private static final VALUES:[LX/EXI;

.field private static internalValueMap:LX/EXE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EXE",
            "<",
            "LX/EXI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2132958
    new-instance v0, LX/EXI;

    const-string v1, "LABEL_OPTIONAL"

    invoke-direct {v0, v1, v4, v4, v2}, LX/EXI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXI;->LABEL_OPTIONAL:LX/EXI;

    .line 2132959
    new-instance v0, LX/EXI;

    const-string v1, "LABEL_REQUIRED"

    invoke-direct {v0, v1, v2, v2, v3}, LX/EXI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXI;->LABEL_REQUIRED:LX/EXI;

    .line 2132960
    new-instance v0, LX/EXI;

    const-string v1, "LABEL_REPEATED"

    invoke-direct {v0, v1, v3, v3, v5}, LX/EXI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXI;->LABEL_REPEATED:LX/EXI;

    .line 2132961
    new-array v0, v5, [LX/EXI;

    sget-object v1, LX/EXI;->LABEL_OPTIONAL:LX/EXI;

    aput-object v1, v0, v4

    sget-object v1, LX/EXI;->LABEL_REQUIRED:LX/EXI;

    aput-object v1, v0, v2

    sget-object v1, LX/EXI;->LABEL_REPEATED:LX/EXI;

    aput-object v1, v0, v3

    sput-object v0, LX/EXI;->$VALUES:[LX/EXI;

    .line 2132962
    new-instance v0, LX/EXF;

    invoke-direct {v0}, LX/EXF;-><init>()V

    sput-object v0, LX/EXI;->internalValueMap:LX/EXE;

    .line 2132963
    invoke-static {}, LX/EXI;->values()[LX/EXI;

    move-result-object v0

    sput-object v0, LX/EXI;->VALUES:[LX/EXI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2132954
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2132955
    iput p3, p0, LX/EXI;->index:I

    .line 2132956
    iput p4, p0, LX/EXI;->value:I

    .line 2132957
    return-void
.end method

.method public static final getDescriptor()LX/EYL;
    .locals 2

    .prologue
    .line 2132953
    sget-object v0, LX/EYC;->i:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->g()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYL;

    return-object v0
.end method

.method public static internalGetValueMap()LX/EXE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EXE",
            "<",
            "LX/EXI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132952
    sget-object v0, LX/EXI;->internalValueMap:LX/EXE;

    return-object v0
.end method

.method public static valueOf(I)LX/EXI;
    .locals 1

    .prologue
    .line 2132947
    packed-switch p0, :pswitch_data_0

    .line 2132948
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2132949
    :pswitch_0
    sget-object v0, LX/EXI;->LABEL_OPTIONAL:LX/EXI;

    goto :goto_0

    .line 2132950
    :pswitch_1
    sget-object v0, LX/EXI;->LABEL_REQUIRED:LX/EXI;

    goto :goto_0

    .line 2132951
    :pswitch_2
    sget-object v0, LX/EXI;->LABEL_REPEATED:LX/EXI;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(LX/EYM;)LX/EXI;
    .locals 2

    .prologue
    .line 2132964
    iget-object v0, p0, LX/EYM;->e:LX/EYL;

    move-object v0, v0

    .line 2132965
    invoke-static {}, LX/EXI;->getDescriptor()LX/EYL;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2132966
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EnumValueDescriptor is not for this type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2132967
    :cond_0
    sget-object v0, LX/EXI;->VALUES:[LX/EXI;

    .line 2132968
    iget v1, p0, LX/EYM;->a:I

    move v1, v1

    .line 2132969
    aget-object v0, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EXI;
    .locals 1

    .prologue
    .line 2132946
    const-class v0, LX/EXI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EXI;

    return-object v0
.end method

.method public static values()[LX/EXI;
    .locals 1

    .prologue
    .line 2132945
    sget-object v0, LX/EXI;->$VALUES:[LX/EXI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EXI;

    return-object v0
.end method


# virtual methods
.method public final getDescriptorForType()LX/EYL;
    .locals 1

    .prologue
    .line 2132944
    invoke-static {}, LX/EXI;->getDescriptor()LX/EYL;

    move-result-object v0

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .prologue
    .line 2132943
    iget v0, p0, LX/EXI;->value:I

    return v0
.end method

.method public final getValueDescriptor()LX/EYM;
    .locals 2

    .prologue
    .line 2132942
    invoke-static {}, LX/EXI;->getDescriptor()LX/EYL;

    move-result-object v0

    invoke-virtual {v0}, LX/EYL;->d()Ljava/util/List;

    move-result-object v0

    iget v1, p0, LX/EXI;->index:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYM;

    return-object v0
.end method
