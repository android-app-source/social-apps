.class public LX/E6I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2079966
    iput-object p1, p0, LX/E6I;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    .line 2079967
    iput-object p2, p0, LX/E6I;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;

    .line 2079968
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z
    .locals 1

    .prologue
    .line 2079956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_OBJECTIVE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/E6I;
    .locals 3

    .prologue
    .line 2079963
    new-instance v2, LX/E6I;

    invoke-static {p0}, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;

    invoke-direct {v2, v0, v1}, LX/E6I;-><init>(Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;)V

    .line 2079964
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;",
            "LX/1PW;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2079957
    invoke-static {p1}, LX/E6I;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/E6I;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2079958
    :cond_0
    const/4 v0, 0x0

    .line 2079959
    :goto_0
    return-object v0

    .line 2079960
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p2, v0, :cond_2

    .line 2079961
    iget-object v0, p0, LX/E6I;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;

    goto :goto_0

    .line 2079962
    :cond_2
    iget-object v0, p0, LX/E6I;->a:Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;

    goto :goto_0
.end method
