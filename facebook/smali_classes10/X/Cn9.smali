.class public LX/Cn9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cmm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1933766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmo;LX/Cmx;)V
    .locals 6

    .prologue
    .line 1933767
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1933768
    :cond_0
    :goto_0
    return-void

    .line 1933769
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v0

    .line 1933770
    iget-object v0, p1, LX/Cmo;->a:LX/Cmn;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/Cmo;->a:LX/Cmn;

    iget v0, v0, LX/Cmn;->b:I

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 1933771
    :goto_1
    iget-object v1, p1, LX/Cmo;->b:LX/Cmn;

    if-eqz v1, :cond_3

    iget-object v1, p1, LX/Cmo;->b:LX/Cmn;

    iget v1, v1, LX/Cmn;->b:I

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 1933772
    :goto_2
    iget-object v2, p1, LX/Cmo;->c:LX/Cmn;

    if-eqz v2, :cond_4

    iget-object v2, p1, LX/Cmo;->c:LX/Cmn;

    iget v2, v2, LX/Cmn;->b:I

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    .line 1933773
    :goto_3
    iget-object v3, p1, LX/Cmo;->d:LX/Cmn;

    if-eqz v3, :cond_5

    iget-object v3, p1, LX/Cmo;->d:LX/Cmn;

    iget v3, v3, LX/Cmn;->b:I

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v3, v5

    .line 1933774
    :goto_4
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1933775
    invoke-interface {p2, p1}, LX/Cmx;->setBorders(LX/Cmo;)V

    goto :goto_0

    .line 1933776
    :cond_2
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    goto :goto_1

    .line 1933777
    :cond_3
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    goto :goto_2

    .line 1933778
    :cond_4
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v2

    goto :goto_3

    .line 1933779
    :cond_5
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    goto :goto_4
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1933780
    instance-of v0, p2, LX/Cn6;

    if-eqz v0, :cond_0

    .line 1933781
    check-cast p1, Landroid/view/ViewGroup;

    .line 1933782
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v0, :cond_0

    .line 1933783
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    check-cast p2, LX/Cn6;

    .line 1933784
    iget-object v2, p2, LX/Cn6;->a:LX/Cmo;

    move-object v2, v2

    .line 1933785
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v0, v2, v1}, LX/Cn9;->a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmo;LX/Cmx;)V

    .line 1933786
    :cond_0
    return-void
.end method
