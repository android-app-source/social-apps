.class public final LX/EwU;
.super LX/1Cd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183227
    iput-object p1, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, LX/1Cd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2183228
    instance-of v0, p1, LX/Ewo;

    if-eqz v0, :cond_3

    .line 2183229
    check-cast p1, LX/Ewo;

    .line 2183230
    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v4

    .line 2183231
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2183232
    :cond_0
    :goto_0
    return-void

    .line 2183233
    :cond_1
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2183234
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ag:LX/2hd;

    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v2, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    .line 2183235
    iget-object v0, p1, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v0

    .line 2183236
    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2183237
    invoke-virtual {p1}, LX/Eus;->p()V

    .line 2183238
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2183239
    :cond_2
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2183240
    :cond_3
    instance-of v0, p1, LX/Ewi;

    if-eqz v0, :cond_0

    .line 2183241
    check-cast p1, LX/Ewi;

    .line 2183242
    iget-boolean v0, p1, LX/Eut;->g:Z

    move v0, v0

    .line 2183243
    if-nez v0, :cond_4

    .line 2183244
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->s(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183245
    :cond_4
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ab:LX/2hW;

    invoke-virtual {v0}, LX/2hW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183246
    iget-boolean v0, p1, LX/Eus;->j:Z

    move v0, v0

    .line 2183247
    if-nez v0, :cond_0

    .line 2183248
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2183249
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ab:LX/2hW;

    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v2

    sget-object v1, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    iget-object v4, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2hW;->a(JLX/2hA;Ljava/lang/String;)V

    .line 2183250
    invoke-virtual {p1}, LX/Eus;->p()V

    goto :goto_0

    .line 2183251
    :cond_5
    iget-object v0, p0, LX/EwU;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
