.class public final LX/DRW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:I

.field public final synthetic e:LX/DRX;


# direct methods
.method public constructor <init>(LX/DRX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1998329
    iput-object p1, p0, LX/DRW;->e:LX/DRX;

    iput-object p2, p0, LX/DRW;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DRW;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DRW;->c:Ljava/lang/String;

    iput p5, p0, LX/DRW;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x2ed27127

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998330
    iget-object v1, p0, LX/DRW;->e:LX/DRX;

    iget-object v2, p0, LX/DRW;->a:Ljava/lang/String;

    iget-object v3, p0, LX/DRW;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DRW;->c:Ljava/lang/String;

    iget v5, p0, LX/DRW;->d:I

    .line 1998331
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v7, v1, LX/DRX;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    invoke-virtual {p0, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 1998332
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUP_LEARNING_STORIES_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v7, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1998333
    const-string p0, "group_learning_unit_id"

    invoke-virtual {v7, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1998334
    const-string p0, "group_feed_title"

    invoke-virtual {v7, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1998335
    const-string p0, "feed_description"

    invoke-virtual {v7, p0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1998336
    const-string p0, "learning_unit_progress"

    invoke-virtual {v7, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1998337
    iget-object p0, v1, LX/DRX;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/DRX;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p0, v7, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1998338
    const v1, -0x7f66c14a

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
