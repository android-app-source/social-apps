.class public final enum LX/EEy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EEy;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EEy;

.field public static final enum FbWebRTCCallStateContacting:LX/EEy;

.field public static final enum FbWebRTCCallStateContactingConferenceCall:LX/EEy;

.field public static final enum FbWebRTCCallStateContactingDirectVideo:LX/EEy;

.field public static final enum FbWebRTCCallStateEnded:LX/EEy;

.field public static final enum FbWebRTCCallStateInAudioCall:LX/EEy;

.field public static final enum FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

.field public static final enum FbWebRTCCallStateInVideoCall:LX/EEy;

.field public static final enum FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

.field public static final enum FbWebRTCCallStateInit:LX/EEy;

.field public static final enum FbWebRTCCallStateInitInbound:LX/EEy;

.field public static final enum FbWebRTCCallStateInitInboundDirectVideo:LX/EEy;

.field public static final enum FbWebRTCCallStateInitOutbound:LX/EEy;

.field public static final enum FbWebRTCCallStateInitOutboundDirectVideo:LX/EEy;

.field public static final enum FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

.field public static final enum FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

.field public static final enum FbWebRTCCallStateLocalRequestingVideoCall:LX/EEy;

.field public static final enum FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

.field public static final enum FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

.field public static final enum FbWebRTCCallStateRemoteRequestingVideoCall:LX/EEy;

.field public static final enum FbWebRTCCallStateRingingInbound:LX/EEy;

.field public static final enum FbWebRTCCallStateRingingInboundDirectVideo:LX/EEy;

.field public static final enum FbWebRTCCallStateRingingOutbound:LX/EEy;

.field public static final enum FbWebRTCCallStateRingingOutboundDirectVideo:LX/EEy;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2094514
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInit"

    invoke-direct {v0, v1, v3}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInit:LX/EEy;

    .line 2094515
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInitInbound"

    invoke-direct {v0, v1, v4}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInitInbound:LX/EEy;

    .line 2094516
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInitInboundDirectVideo"

    invoke-direct {v0, v1, v5}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInitInboundDirectVideo:LX/EEy;

    .line 2094517
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInitOutbound"

    invoke-direct {v0, v1, v6}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInitOutbound:LX/EEy;

    .line 2094518
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInitOutboundDirectVideo"

    invoke-direct {v0, v1, v7}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInitOutboundDirectVideo:LX/EEy;

    .line 2094519
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateContacting"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateContacting:LX/EEy;

    .line 2094520
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateContactingConferenceCall"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateContactingConferenceCall:LX/EEy;

    .line 2094521
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateContactingDirectVideo"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateContactingDirectVideo:LX/EEy;

    .line 2094522
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRingingInbound"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRingingInbound:LX/EEy;

    .line 2094523
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRingingInboundDirectVideo"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRingingInboundDirectVideo:LX/EEy;

    .line 2094524
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRingingOutbound"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRingingOutbound:LX/EEy;

    .line 2094525
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRingingOutboundDirectVideo"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRingingOutboundDirectVideo:LX/EEy;

    .line 2094526
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInAudioCall"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    .line 2094527
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInVideoCall"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    .line 2094528
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInAudioConferenceCall"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    .line 2094529
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateInVideoConferenceCall"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

    .line 2094530
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateLocalPausedVideoCall"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    .line 2094531
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRemotePausedVideoCall"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    .line 2094532
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateLocalPausedVideoConferenceCall"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

    .line 2094533
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRemotePausedVideoConferenceCall"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

    .line 2094534
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateLocalRequestingVideoCall"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateLocalRequestingVideoCall:LX/EEy;

    .line 2094535
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateRemoteRequestingVideoCall"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateRemoteRequestingVideoCall:LX/EEy;

    .line 2094536
    new-instance v0, LX/EEy;

    const-string v1, "FbWebRTCCallStateEnded"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/EEy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    .line 2094537
    const/16 v0, 0x17

    new-array v0, v0, [LX/EEy;

    sget-object v1, LX/EEy;->FbWebRTCCallStateInit:LX/EEy;

    aput-object v1, v0, v3

    sget-object v1, LX/EEy;->FbWebRTCCallStateInitInbound:LX/EEy;

    aput-object v1, v0, v4

    sget-object v1, LX/EEy;->FbWebRTCCallStateInitInboundDirectVideo:LX/EEy;

    aput-object v1, v0, v5

    sget-object v1, LX/EEy;->FbWebRTCCallStateInitOutbound:LX/EEy;

    aput-object v1, v0, v6

    sget-object v1, LX/EEy;->FbWebRTCCallStateInitOutboundDirectVideo:LX/EEy;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EEy;->FbWebRTCCallStateContacting:LX/EEy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EEy;->FbWebRTCCallStateContactingConferenceCall:LX/EEy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EEy;->FbWebRTCCallStateContactingDirectVideo:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingInbound:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingInboundDirectVideo:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutbound:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutboundDirectVideo:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalRequestingVideoCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemoteRequestingVideoCall:LX/EEy;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    aput-object v2, v0, v1

    sput-object v0, LX/EEy;->$VALUES:[LX/EEy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2094512
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2094513
    return-void
.end method

.method public static validateStateTransition(LX/EEy;LX/EEy;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2094538
    if-ne p0, p1, :cond_1

    .line 2094539
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 2094540
    :cond_1
    sget-object v2, LX/EEx;->a:[I

    invoke-virtual {p0}, LX/EEy;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2094541
    :pswitch_1
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingInbound:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094542
    :pswitch_2
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInitInboundDirectVideo:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094543
    :pswitch_3
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateContacting:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateContactingConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutbound:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094544
    :pswitch_4
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateContactingDirectVideo:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutboundDirectVideo:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094545
    :pswitch_5
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalRequestingVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemoteRequestingVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094546
    :pswitch_6
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2094547
    :pswitch_7
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094548
    :pswitch_8
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094549
    :pswitch_9
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094550
    :pswitch_a
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094551
    :pswitch_b
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094552
    :pswitch_c
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094553
    :pswitch_d
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateRemotePausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094554
    :pswitch_e
    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateLocalPausedVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094555
    :pswitch_f
    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutbound:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094556
    :pswitch_10
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094557
    :pswitch_11
    sget-object v2, LX/EEy;->FbWebRTCCallStateRingingOutboundDirectVideo:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094558
    :pswitch_12
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateContacting:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateContactingConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094559
    :pswitch_13
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioConferenceCall:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094560
    :pswitch_14
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 2094561
    :pswitch_15
    sget-object v2, LX/EEy;->FbWebRTCCallStateInAudioCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateInVideoCall:LX/EEy;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EEy;->FbWebRTCCallStateEnded:LX/EEy;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :pswitch_16
    move v0, v1

    .line 2094562
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/EEy;
    .locals 1

    .prologue
    .line 2094511
    const-class v0, LX/EEy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EEy;

    return-object v0
.end method

.method public static values()[LX/EEy;
    .locals 1

    .prologue
    .line 2094510
    sget-object v0, LX/EEy;->$VALUES:[LX/EEy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EEy;

    return-object v0
.end method
