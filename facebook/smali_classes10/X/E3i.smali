.class public final LX/E3i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

.field private final b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2075117
    iput-object p1, p0, LX/E3i;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2075118
    iput-object p2, p0, LX/E3i;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075119
    iput-object p3, p0, LX/E3i;->c:Landroid/content/Context;

    .line 2075120
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2075121
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2075122
    iget-object v0, p0, LX/E3i;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075123
    iget-object v4, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v4

    .line 2075124
    invoke-interface {v6}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2075125
    invoke-interface {v6}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    move-object v5, v0

    .line 2075126
    :goto_0
    const v4, 0x7f021a25

    .line 2075127
    invoke-interface {v6}, LX/9uc;->bn()Z

    move-result v0

    .line 2075128
    invoke-interface {v6}, LX/9uc;->dm()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 2075129
    invoke-interface {v6}, LX/9uc;->dm()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, LX/9uc;->dm()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 2075130
    :goto_1
    invoke-interface {v6}, LX/9uc;->dm()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2075131
    const v4, 0x7f021a23

    .line 2075132
    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v6, p0, LX/E3i;->c:Landroid/content/Context;

    iget-object v5, p0, LX/E3i;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0b1660

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v5, p0, LX/E3i;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0b1661

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    move v5, v1

    move-object v9, v3

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2075133
    new-instance v2, LX/1yT;

    invoke-direct {v2, v0, v1}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v2

    .line 2075134
    :cond_2
    iget-object v0, p0, LX/E3i;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->f:LX/1Uf;

    invoke-interface {v6}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2075135
    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2075136
    iget-object v0, p0, LX/E3i;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
