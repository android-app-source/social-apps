.class public final LX/EWo;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EWn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EWo;",
        ">;",
        "LX/EWn;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2131388
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2131389
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 0

    .prologue
    .line 2131364
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2131365
    return-void
.end method

.method private d(LX/EWY;)LX/EWo;
    .locals 1

    .prologue
    .line 2131366
    instance-of v0, p1, LX/EWq;

    if-eqz v0, :cond_0

    .line 2131367
    check-cast p1, LX/EWq;

    invoke-virtual {p0, p1}, LX/EWo;->a(LX/EWq;)LX/EWo;

    move-result-object p0

    .line 2131368
    :goto_0
    return-object p0

    .line 2131369
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EWo;
    .locals 4

    .prologue
    .line 2131370
    const/4 v2, 0x0

    .line 2131371
    :try_start_0
    sget-object v0, LX/EWq;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWq;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2131372
    if-eqz v0, :cond_0

    .line 2131373
    invoke-virtual {p0, v0}, LX/EWo;->a(LX/EWq;)LX/EWo;

    .line 2131374
    :cond_0
    return-object p0

    .line 2131375
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2131376
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2131377
    check-cast v0, LX/EWq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2131378
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2131379
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2131380
    invoke-virtual {p0, v1}, LX/EWo;->a(LX/EWq;)LX/EWo;

    :cond_1
    throw v0

    .line 2131381
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static m()LX/EWo;
    .locals 1

    .prologue
    .line 2131382
    new-instance v0, LX/EWo;

    invoke-direct {v0}, LX/EWo;-><init>()V

    return-object v0
.end method

.method private n()LX/EWo;
    .locals 2

    .prologue
    .line 2131383
    invoke-static {}, LX/EWo;->m()LX/EWo;

    move-result-object v0

    invoke-direct {p0}, LX/EWo;->x()LX/EWq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EWo;->a(LX/EWq;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/EWq;
    .locals 2

    .prologue
    .line 2131384
    invoke-direct {p0}, LX/EWo;->x()LX/EWq;

    move-result-object v0

    .line 2131385
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2131386
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2131387
    :cond_0
    return-object v0
.end method

.method private x()LX/EWq;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2131350
    new-instance v2, LX/EWq;

    invoke-direct {v2, p0}, LX/EWq;-><init>(LX/EWj;)V

    .line 2131351
    iget v3, p0, LX/EWo;->a:I

    .line 2131352
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2131353
    :goto_0
    iget v1, p0, LX/EWo;->b:I

    .line 2131354
    iput v1, v2, LX/EWq;->start_:I

    .line 2131355
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2131356
    or-int/lit8 v0, v0, 0x2

    .line 2131357
    :cond_0
    iget v1, p0, LX/EWo;->c:I

    .line 2131358
    iput v1, v2, LX/EWq;->end_:I

    .line 2131359
    iput v0, v2, LX/EWq;->bitField0_:I

    .line 2131360
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2131361
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2131390
    invoke-direct {p0, p1}, LX/EWo;->d(LX/EWY;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2131391
    invoke-direct {p0, p1, p2}, LX/EWo;->d(LX/EWd;LX/EYZ;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWq;)LX/EWo;
    .locals 2

    .prologue
    .line 2131392
    sget-object v0, LX/EWq;->c:LX/EWq;

    move-object v0, v0

    .line 2131393
    if-ne p1, v0, :cond_0

    .line 2131394
    :goto_0
    return-object p0

    .line 2131395
    :cond_0
    const/4 v0, 0x1

    .line 2131396
    iget v1, p1, LX/EWq;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2131397
    if-eqz v0, :cond_1

    .line 2131398
    iget v0, p1, LX/EWq;->start_:I

    move v0, v0

    .line 2131399
    iget v1, p0, LX/EWo;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EWo;->a:I

    .line 2131400
    iput v0, p0, LX/EWo;->b:I

    .line 2131401
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131402
    :cond_1
    iget v0, p1, LX/EWq;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2131403
    if-eqz v0, :cond_2

    .line 2131404
    iget v0, p1, LX/EWq;->end_:I

    move v0, v0

    .line 2131405
    iget v1, p0, LX/EWo;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/EWo;->a:I

    .line 2131406
    iput v0, p0, LX/EWo;->c:I

    .line 2131407
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131408
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2131409
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2131362
    invoke-direct {p0, p1, p2}, LX/EWo;->d(LX/EWd;LX/EYZ;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2131363
    invoke-direct {p0}, LX/EWo;->n()LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2131337
    invoke-direct {p0, p1, p2}, LX/EWo;->d(LX/EWd;LX/EYZ;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2131338
    invoke-direct {p0}, LX/EWo;->n()LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2131339
    invoke-direct {p0, p1}, LX/EWo;->d(LX/EWY;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2131340
    invoke-direct {p0}, LX/EWo;->n()LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2131341
    sget-object v0, LX/EYC;->h:LX/EYn;

    const-class v1, LX/EWq;

    const-class v2, LX/EWo;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2131342
    sget-object v0, LX/EYC;->g:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2131343
    invoke-direct {p0}, LX/EWo;->n()LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2131344
    invoke-direct {p0}, LX/EWo;->x()LX/EWq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2131345
    invoke-direct {p0}, LX/EWo;->w()LX/EWq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2131346
    invoke-direct {p0}, LX/EWo;->x()LX/EWq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2131347
    invoke-direct {p0}, LX/EWo;->w()LX/EWq;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2131348
    sget-object v0, LX/EWq;->c:LX/EWq;

    move-object v0, v0

    .line 2131349
    return-object v0
.end method
