.class public LX/Cns;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/LogoBlockView;",
        "Lcom/facebook/richdocument/model/data/LogoBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalPageLiker;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/8Yp;

.field public k:Z

.field public l:Z

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;)V
    .locals 8

    .prologue
    .line 1934502
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934503
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/Cns;

    const/16 v3, 0x2eb

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xb19

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12b1

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3225

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v0, 0x3216

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, LX/Cns;->d:LX/0Ot;

    iput-object v4, v2, LX/Cns;->e:LX/0Ot;

    iput-object v5, v2, LX/Cns;->f:LX/0Ot;

    iput-object v6, v2, LX/Cns;->g:LX/0Ot;

    iput-object v7, v2, LX/Cns;->h:LX/0Ot;

    iput-object p1, v2, LX/Cns;->i:LX/0Ot;

    .line 1934504
    return-void
.end method

.method public static c(LX/Cns;)V
    .locals 3

    .prologue
    .line 1934505
    iget-boolean v0, p0, LX/Cns;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Cns;->k:Z

    .line 1934506
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934507
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v1, p0, LX/Cns;->j:LX/8Yp;

    invoke-interface {v1}, LX/8Yp;->b()Z

    move-result v1

    iget-boolean v2, p0, LX/Cns;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a(ZZ)V

    .line 1934508
    return-void

    .line 1934509
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 5

    .prologue
    .line 1934510
    check-cast p1, LX/CmQ;

    const/4 v3, 0x0

    .line 1934511
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934512
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-interface {v0, v3}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934513
    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cns;->m:Ljava/lang/String;

    .line 1934514
    iget-object v0, p1, LX/CmQ;->b:LX/8Yp;

    move-object v0, v0

    .line 1934515
    iput-object v0, p0, LX/Cns;->j:LX/8Yp;

    .line 1934516
    iget-object v0, p1, LX/CmQ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object v2, v0

    .line 1934517
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0612

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1934518
    if-nez v2, :cond_1

    .line 1934519
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934520
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a(Ljava/lang/String;I)V

    .line 1934521
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934522
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    .line 1934523
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->l:Landroid/view/View;

    invoke-static {v2, v1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1934524
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1934525
    iget-object v0, p0, LX/Cns;->j:LX/8Yp;

    if-nez v0, :cond_5

    .line 1934526
    :cond_0
    :goto_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934527
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934528
    return-void

    .line 1934529
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934530
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->en_()I

    move-result v3

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->d()I

    invoke-virtual {v0, v1, v3}, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a(Ljava/lang/String;I)V

    .line 1934531
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1934532
    iget-object v0, p0, LX/Cns;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "instant_articles"

    const-string v3, "IA Logo URL is invalid"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934533
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1934534
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934535
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v1

    .line 1934536
    iput v1, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->m:I

    .line 1934537
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v0

    .line 1934538
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 1934539
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0613

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    move v1, v0

    .line 1934540
    :goto_2
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934541
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v2

    .line 1934542
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-static {v3, v2, v2}, LX/CoV;->a(Lcom/facebook/richdocument/view/widget/PressStateButton;II)V

    .line 1934543
    goto/16 :goto_0

    :cond_4
    move v1, v0

    goto :goto_2

    .line 1934544
    :cond_5
    iget-object v0, p0, LX/Cns;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const v0, 0x25d6af

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/Cns;->j:LX/8Yp;

    invoke-interface {v4}, LX/8Yp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1934545
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934546
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    new-instance v4, LX/Cnp;

    invoke-direct {v4, p0, v3}, LX/Cnp;-><init>(LX/Cns;Ljava/lang/String;)V

    .line 1934547
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934548
    iget-object v0, p0, LX/Cns;->j:LX/8Yp;

    invoke-interface {v0}, LX/8Yp;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/Cns;->k:Z

    .line 1934549
    iget-object v0, p0, LX/Cns;->j:LX/8Yp;

    invoke-interface {v0}, LX/8Yp;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/Cns;->k:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, LX/Cns;->l:Z

    .line 1934550
    iget-boolean v0, p0, LX/Cns;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Cns;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1934551
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934552
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v1, p0, LX/Cns;->j:LX/8Yp;

    invoke-interface {v1}, LX/8Yp;->b()Z

    move-result v1

    iget-boolean v2, p0, LX/Cns;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a(ZZ)V

    .line 1934553
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934554
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    new-instance v1, LX/Cnr;

    invoke-direct {v1, p0}, LX/Cnr;-><init>(LX/Cns;)V

    .line 1934555
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934556
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1934557
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 1934558
    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1934559
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1934560
    const-string v0, "block_type"

    const-string v2, "logo"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1934561
    const-string v2, "is_page_like_button_shown"

    iget-boolean v0, p0, LX/Cns;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cns;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1934562
    iget-object v0, p0, LX/Cns;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    iget-object v2, p0, LX/Cns;->m:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1934563
    return-void

    .line 1934564
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
