.class public LX/Dqp;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 2048672
    const-string v0, "notifs"

    const-string v1, "lockscreen_on"

    const-string v2, "vibrate"

    const-string v3, "use_led"

    const-string v4, "wall_posts"

    const-string v5, "comments"

    const-string v6, "friend_requests"

    const-string v7, "friend_confirmations"

    const-string v8, "photo_tags"

    const-string v9, "event_invites"

    const-string v10, "app_requests"

    const-string v11, "groups"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "place_tips"

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Dqp;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048674
    iput-object p1, p0, LX/Dqp;->b:LX/0Ot;

    .line 2048675
    iput-object p2, p0, LX/Dqp;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2048676
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 2048677
    iget-object v0, p0, LX/Dqp;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0hM;->L:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2048678
    return-void
.end method
