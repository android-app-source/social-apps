.class public final LX/DO2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/DO3;


# direct methods
.method public constructor <init>(LX/DO3;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1992065
    iput-object p1, p0, LX/DO2;->b:LX/DO3;

    iput-object p2, p0, LX/DO2;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1992066
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1992067
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1992068
    iget-object v0, p0, LX/DO2;->b:LX/DO3;

    iget-object v1, v0, LX/DO3;->d:LX/DOK;

    iget-object v2, p0, LX/DO2;->a:Ljava/lang/String;

    .line 1992069
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1992070
    check-cast v0, LX/9Mz;

    invoke-virtual {v1, v2, v0}, LX/DOK;->a(Ljava/lang/String;LX/9Mz;)V

    .line 1992071
    const/4 v0, 0x0

    .line 1992072
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1992073
    if-eqz v1, :cond_0

    .line 1992074
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1992075
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    const-string v1, "gk_group_discussion_topics"

    invoke-static {v0, v1}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result v0

    .line 1992076
    :cond_0
    iget-object v1, p0, LX/DO2;->b:LX/DO3;

    iget-object v1, v1, LX/DO3;->d:LX/DOK;

    invoke-virtual {v1, v0}, LX/DOK;->a(Z)V

    .line 1992077
    return-void
.end method
