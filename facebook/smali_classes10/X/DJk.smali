.class public final LX/DJk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

.field public final synthetic c:LX/21D;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/DJr;

.field public final synthetic f:LX/DJl;


# direct methods
.method public constructor <init>(LX/DJl;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/21D;Landroid/content/Context;LX/DJr;)V
    .locals 0

    .prologue
    .line 1985766
    iput-object p1, p0, LX/DJk;->f:LX/DJl;

    iput-object p2, p0, LX/DJk;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DJk;->b:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iput-object p4, p0, LX/DJk;->c:LX/21D;

    iput-object p5, p0, LX/DJk;->d:Landroid/content/Context;

    iput-object p6, p0, LX/DJk;->e:LX/DJr;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1985767
    iget-object v0, p0, LX/DJk;->e:LX/DJr;

    if-eqz v0, :cond_0

    .line 1985768
    iget-object v0, p0, LX/DJk;->e:LX/DJr;

    invoke-interface {v0}, LX/DJr;->d()V

    .line 1985769
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1985770
    check-cast p1, Ljava/util/List;

    .line 1985771
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1985772
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 1985773
    check-cast v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    .line 1985774
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1985775
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 1985776
    check-cast v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;

    .line 1985777
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1985778
    iget-object v2, p0, LX/DJk;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/DJk;->b:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    .line 1985779
    new-instance v5, LX/5Rj;

    invoke-direct {v5}, LX/5Rj;-><init>()V

    .line 1985780
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1985781
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 1985782
    iput-object v6, v5, LX/5Rj;->a:Ljava/lang/String;

    .line 1985783
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->j()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1985784
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 1985785
    iput-object v6, v5, LX/5Rj;->e:Ljava/lang/Long;

    .line 1985786
    :cond_1
    iput-object v3, v5, LX/5Rj;->f:Ljava/lang/String;

    .line 1985787
    move-object v5, v5

    .line 1985788
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 1985789
    iput-object v6, v5, LX/5Rj;->d:Ljava/lang/String;

    .line 1985790
    move-object v5, v5

    .line 1985791
    invoke-virtual {v5}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v5

    move-object v2, v5

    .line 1985792
    iget-object v0, p0, LX/DJk;->f:LX/DJl;

    iget-object v0, v0, LX/DJl;->e:LX/B0k;

    iget-object v3, p0, LX/DJk;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/DJk;->c:LX/21D;

    invoke-virtual {v0, v3, v4}, LX/B0k;->a(Ljava/lang/String;LX/21D;)V

    .line 1985793
    const/4 v0, 0x0

    .line 1985794
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1985795
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1985796
    :cond_2
    new-instance v1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;-><init>()V

    .line 1985797
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1985798
    const-string v4, "ARG_PRODUCT_ITEM_ATTACHMENT"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1985799
    const-string v4, "ARG_LOCATION_NAME"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985800
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1985801
    move-object v1, v1

    .line 1985802
    new-instance v0, LX/DJi;

    invoke-direct {v0, p0, v2}, LX/DJi;-><init>(LX/DJk;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V

    .line 1985803
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->q:Landroid/content/DialogInterface$OnClickListener;

    .line 1985804
    new-instance v0, LX/DJj;

    invoke-direct {v0, p0}, LX/DJj;-><init>(LX/DJk;)V

    .line 1985805
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->r:Landroid/content/DialogInterface$OnClickListener;

    .line 1985806
    iget-object v0, p0, LX/DJk;->d:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "GroupCommerceNLUDialogFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1985807
    :cond_3
    :goto_0
    return-void

    .line 1985808
    :cond_4
    iget-object v0, p0, LX/DJk;->e:LX/DJr;

    if-eqz v0, :cond_3

    .line 1985809
    iget-object v0, p0, LX/DJk;->e:LX/DJr;

    invoke-interface {v0}, LX/DJr;->c()V

    goto :goto_0
.end method
