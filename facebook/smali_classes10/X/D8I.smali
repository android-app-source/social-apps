.class public LX/D8I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9DK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/189;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/189;LX/0Or;)V
    .locals 0
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/189;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968224
    iput-object p1, p0, LX/D8I;->a:LX/0QK;

    .line 1968225
    iput-object p2, p0, LX/D8I;->c:LX/189;

    .line 1968226
    iput-object p3, p0, LX/D8I;->b:LX/0Or;

    .line 1968227
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1968218
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/D8I;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968219
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1968222
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/D8I;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method

.method public final a()[LX/0b2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1968220
    const/4 v0, 0x1

    new-array v0, v0, [LX/0b2;

    new-instance v1, LX/D8H;

    invoke-direct {v1, p0}, LX/D8H;-><init>(LX/D8I;)V

    aput-object v1, v0, v2

    .line 1968221
    return-object v0
.end method
