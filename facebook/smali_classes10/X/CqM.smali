.class public LX/CqM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CqE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public a:LX/1m0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CoM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/Cm4;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Cm4;",
            "LX/Cli;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1939609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939610
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/CqM;->d:Ljava/util/Queue;

    .line 1939611
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CqM;->e:Ljava/util/HashMap;

    .line 1939612
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqM;->f:Z

    .line 1939613
    return-void
.end method

.method public static a(LX/0QB;)LX/CqM;
    .locals 6

    .prologue
    .line 1939614
    const-class v1, LX/CqM;

    monitor-enter v1

    .line 1939615
    :try_start_0
    sget-object v0, LX/CqM;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1939616
    sput-object v2, LX/CqM;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1939617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1939619
    new-instance p0, LX/CqM;

    invoke-direct {p0}, LX/CqM;-><init>()V

    .line 1939620
    invoke-static {v0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v3

    check-cast v3, LX/1m0;

    invoke-static {v0}, LX/CoM;->a(LX/0QB;)LX/CoM;

    move-result-object v4

    check-cast v4, LX/CoM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    .line 1939621
    iput-object v3, p0, LX/CqM;->a:LX/1m0;

    iput-object v4, p0, LX/CqM;->b:LX/CoM;

    iput-object v5, p0, LX/CqM;->c:LX/0Uh;

    .line 1939622
    move-object v0, p0

    .line 1939623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1939624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CqM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1939625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1939626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/Cm4;Z)LX/Cli;
    .locals 2

    .prologue
    .line 1939627
    monitor-enter p0

    :try_start_0
    iput-boolean p2, p0, LX/CqM;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1939628
    if-nez p1, :cond_0

    .line 1939629
    const/4 v0, 0x0

    .line 1939630
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1939631
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/CqM;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939632
    iget-object v0, p0, LX/CqM;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cli;

    goto :goto_0

    .line 1939633
    :cond_1
    iget-object v0, p0, LX/CqM;->a:LX/1m0;

    iget-object v1, p0, LX/CqM;->b:LX/CoM;

    invoke-static {p1, v0, v1, p2}, LX/Cli;->a(LX/Cm4;LX/1m0;LX/CoM;Z)LX/Cli;

    move-result-object v0

    .line 1939634
    iget-object v1, p0, LX/CqM;->d:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 1939635
    iget-object v1, p0, LX/CqM;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1939636
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/Clo;)V
    .locals 4

    .prologue
    .line 1939637
    monitor-enter p0

    if-nez p1, :cond_1

    .line 1939638
    :cond_0
    monitor-exit p0

    return-void

    .line 1939639
    :cond_1
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    :try_start_0
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 1939640
    invoke-virtual {p1, v3}, LX/Clo;->a(I)LX/Clr;

    move-result-object v2

    .line 1939641
    instance-of v1, v2, LX/Cm4;

    if-eqz v1, :cond_2

    .line 1939642
    move-object v0, v2

    check-cast v0, LX/Cm4;

    move-object v1, v0

    invoke-interface {v1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v1

    .line 1939643
    if-eqz v1, :cond_2

    .line 1939644
    iget-object v1, p0, LX/CqM;->d:Ljava/util/Queue;

    check-cast v2, LX/Cm4;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1939645
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1939646
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1939647
    iget-object v0, p0, LX/CqM;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 4

    .prologue
    .line 1939648
    iget-object v0, p0, LX/CqM;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cm4;

    .line 1939649
    if-eqz v0, :cond_0

    .line 1939650
    iget-object v1, p0, LX/CqM;->a:LX/1m0;

    iget-object v2, p0, LX/CqM;->b:LX/CoM;

    iget-boolean v3, p0, LX/CqM;->f:Z

    invoke-static {v0, v1, v2, v3}, LX/Cli;->a(LX/Cm4;LX/1m0;LX/CoM;Z)LX/Cli;

    move-result-object v1

    .line 1939651
    iget-object v2, p0, LX/CqM;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939652
    :cond_0
    return-void
.end method
