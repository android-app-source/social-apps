.class public final LX/E7S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/0yL;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2081568
    iput-object p1, p0, LX/E7S;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2081569
    iput-object p2, p0, LX/E7S;->b:Landroid/view/View$OnClickListener;

    .line 2081570
    return-void
.end method


# virtual methods
.method public final e_(Z)V
    .locals 0

    .prologue
    .line 2081571
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2081572
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x5d5629c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2081573
    iget-object v0, p0, LX/E7S;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

    .line 2081574
    iget-object v1, v0, LX/Cfk;->d:Landroid/content/Context;

    move-object v1, v1

    .line 2081575
    move-object v0, v1

    .line 2081576
    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2081577
    if-nez v0, :cond_0

    .line 2081578
    const v0, 0x2edb510b

    invoke-static {v3, v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2081579
    :goto_0
    return-void

    .line 2081580
    :cond_0
    iget-object v1, p0, LX/E7S;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    sget-object v3, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v4, p0, LX/E7S;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

    .line 2081581
    iget-object v5, v4, LX/Cfk;->d:Landroid/content/Context;

    move-object v5, v5

    .line 2081582
    move-object v4, v5

    .line 2081583
    const v5, 0x7f080e4a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/E7R;

    invoke-direct {v5, p0, p1}, LX/E7R;-><init>(LX/E7S;Landroid/view/View;)V

    invoke-virtual {v1, v3, v4, v5}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2081584
    iget-object v1, p0, LX/E7S;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/ReactionVideoHscrollHandler;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    sget-object v3, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 2081585
    const v0, 0x1333b25d

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
