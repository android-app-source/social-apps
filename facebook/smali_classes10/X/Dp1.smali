.class public final LX/Dp1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/Eaf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dp2;


# direct methods
.method public constructor <init>(LX/Dp2;)V
    .locals 0

    .prologue
    .line 2043012
    iput-object p1, p0, LX/Dp1;->a:LX/Dp2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2042995
    const/4 v4, 0x0

    .line 2042996
    const/4 v1, 0x0

    .line 2042997
    iget-object v0, p0, LX/Dp1;->a:LX/Dp2;

    iget-object v0, v0, LX/Dp2;->c:LX/Dof;

    sget-object v2, LX/Dp2;->a:LX/2PC;

    invoke-virtual {v0, v2}, LX/Dod;->a(LX/2PC;)Ljava/lang/String;

    move-result-object v2

    .line 2042998
    if-eqz v2, :cond_1

    .line 2042999
    :try_start_0
    new-instance v0, LX/Eaf;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-direct {v0, v2}, LX/Eaf;-><init>([B)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    .line 2043000
    :goto_0
    if-nez v0, :cond_0

    .line 2043001
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v0

    .line 2043002
    new-instance v1, LX/Eae;

    .line 2043003
    iget-object v2, v0, LX/Eau;->a:LX/Eat;

    move-object v2, v2

    .line 2043004
    invoke-direct {v1, v2}, LX/Eae;-><init>(LX/Eat;)V

    .line 2043005
    new-instance v2, LX/Eaf;

    .line 2043006
    iget-object v3, v0, LX/Eau;->b:LX/Eas;

    move-object v0, v3

    .line 2043007
    invoke-direct {v2, v1, v0}, LX/Eaf;-><init>(LX/Eae;LX/Eas;)V

    move-object v0, v2

    .line 2043008
    iget-object v1, p0, LX/Dp1;->a:LX/Dp2;

    iget-object v1, v1, LX/Dp2;->c:LX/Dof;

    sget-object v2, LX/Dp2;->a:LX/2PC;

    invoke-virtual {v0}, LX/Eaf;->c()[B

    move-result-object v3

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Dod;->a(LX/2PC;Ljava/lang/String;)V

    .line 2043009
    :cond_0
    return-object v0

    .line 2043010
    :catch_0
    move-exception v0

    .line 2043011
    const-string v2, "MessengerIdentityKeyGenerator"

    const-string v3, "Couldn\'t retrieve local identity key pair, generating new one"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
