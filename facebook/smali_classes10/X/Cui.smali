.class public final enum LX/Cui;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cui;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cui;

.field public static final enum NONE:LX/Cui;

.field public static final enum PAUSE_ICON:LX/Cui;

.field public static final enum PLAY_ICON:LX/Cui;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1947098
    new-instance v0, LX/Cui;

    const-string v1, "PLAY_ICON"

    invoke-direct {v0, v1, v2}, LX/Cui;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cui;->PLAY_ICON:LX/Cui;

    .line 1947099
    new-instance v0, LX/Cui;

    const-string v1, "PAUSE_ICON"

    invoke-direct {v0, v1, v3}, LX/Cui;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cui;->PAUSE_ICON:LX/Cui;

    .line 1947100
    new-instance v0, LX/Cui;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/Cui;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cui;->NONE:LX/Cui;

    .line 1947101
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cui;

    sget-object v1, LX/Cui;->PLAY_ICON:LX/Cui;

    aput-object v1, v0, v2

    sget-object v1, LX/Cui;->PAUSE_ICON:LX/Cui;

    aput-object v1, v0, v3

    sget-object v1, LX/Cui;->NONE:LX/Cui;

    aput-object v1, v0, v4

    sput-object v0, LX/Cui;->$VALUES:[LX/Cui;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1947102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cui;
    .locals 1

    .prologue
    .line 1947103
    const-class v0, LX/Cui;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cui;

    return-object v0
.end method

.method public static values()[LX/Cui;
    .locals 1

    .prologue
    .line 1947104
    sget-object v0, LX/Cui;->$VALUES:[LX/Cui;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cui;

    return-object v0
.end method
