.class public LX/Da3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Da4;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Da3;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2014454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Da3;
    .locals 3

    .prologue
    .line 2014455
    sget-object v0, LX/Da3;->a:LX/Da3;

    if-nez v0, :cond_1

    .line 2014456
    const-class v1, LX/Da3;

    monitor-enter v1

    .line 2014457
    :try_start_0
    sget-object v0, LX/Da3;->a:LX/Da3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2014458
    if-eqz v2, :cond_0

    .line 2014459
    :try_start_1
    new-instance v0, LX/Da3;

    invoke-direct {v0}, LX/Da3;-><init>()V

    .line 2014460
    move-object v0, v0

    .line 2014461
    sput-object v0, LX/Da3;->a:LX/Da3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014462
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2014463
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2014464
    :cond_1
    sget-object v0, LX/Da3;->a:LX/Da3;

    return-object v0

    .line 2014465
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2014466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2014467
    check-cast p1, LX/Da4;

    .line 2014468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2014469
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "thread_id"

    .line 2014470
    iget-object v3, p1, LX/Da4;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2014471
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014472
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "associateThreadToGroupMethod"

    .line 2014473
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2014474
    move-object v1, v1

    .line 2014475
    const-string v2, "POST"

    .line 2014476
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2014477
    move-object v1, v1

    .line 2014478
    const-string v2, "%s/group_threads"

    .line 2014479
    iget-object v3, p1, LX/Da4;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2014480
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2014481
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2014482
    move-object v1, v1

    .line 2014483
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2014484
    move-object v0, v1

    .line 2014485
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2014486
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2014487
    move-object v0, v0

    .line 2014488
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2014489
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2014490
    const/4 v0, 0x0

    return-object v0
.end method
