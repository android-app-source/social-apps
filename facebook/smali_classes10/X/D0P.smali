.class public final enum LX/D0P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D0P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D0P;

.field public static final enum ELECTIONS:LX/D0P;

.field public static final enum PHOTO_SNAPSHOT:LX/D0P;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1955439
    new-instance v0, LX/D0P;

    const-string v1, "ELECTIONS"

    invoke-direct {v0, v1, v2}, LX/D0P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D0P;->ELECTIONS:LX/D0P;

    .line 1955440
    new-instance v0, LX/D0P;

    const-string v1, "PHOTO_SNAPSHOT"

    invoke-direct {v0, v1, v3}, LX/D0P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D0P;->PHOTO_SNAPSHOT:LX/D0P;

    .line 1955441
    const/4 v0, 0x2

    new-array v0, v0, [LX/D0P;

    sget-object v1, LX/D0P;->ELECTIONS:LX/D0P;

    aput-object v1, v0, v2

    sget-object v1, LX/D0P;->PHOTO_SNAPSHOT:LX/D0P;

    aput-object v1, v0, v3

    sput-object v0, LX/D0P;->$VALUES:[LX/D0P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1955442
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D0P;
    .locals 1

    .prologue
    .line 1955443
    const-class v0, LX/D0P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D0P;

    return-object v0
.end method

.method public static values()[LX/D0P;
    .locals 1

    .prologue
    .line 1955444
    sget-object v0, LX/D0P;->$VALUES:[LX/D0P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D0P;

    return-object v0
.end method
