.class public final LX/EYo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private asBytes:[B

.field private messageClassName:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/EWW;)V
    .locals 1

    .prologue
    .line 2138082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138083
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYo;->messageClassName:Ljava/lang/String;

    .line 2138084
    invoke-interface {p1}, LX/EWW;->jZ_()[B

    move-result-object v0

    iput-object v0, p0, LX/EYo;->asBytes:[B

    .line 2138085
    return-void
.end method


# virtual methods
.method public final readResolve()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2138086
    :try_start_0
    iget-object v0, p0, LX/EYo;->messageClassName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2138087
    const-string v1, "newBuilder"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2138088
    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWR;

    .line 2138089
    iget-object v1, p0, LX/EYo;->asBytes:[B

    invoke-interface {v0, v1}, LX/EWR;->b([B)LX/EWR;

    .line 2138090
    invoke-interface {v0}, LX/EWR;->j()LX/EWW;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    return-object v0

    .line 2138091
    :catch_0
    move-exception v0

    .line 2138092
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find proto buffer class"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2138093
    :catch_1
    move-exception v0

    .line 2138094
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find newBuilder method"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2138095
    :catch_2
    move-exception v0

    .line 2138096
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to call newBuilder method"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2138097
    :catch_3
    move-exception v0

    .line 2138098
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error calling newBuilder"

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2138099
    :catch_4
    move-exception v0

    .line 2138100
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to understand proto buffer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
