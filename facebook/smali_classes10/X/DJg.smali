.class public LX/DJg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1985682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/DJf;Landroid/view/View;Ljava/lang/String;)LX/0hs;
    .locals 6

    .prologue
    .line 1985683
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1985684
    sget-object v0, LX/DJf;->BLUE:LX/DJf;

    if-ne p0, v0, :cond_0

    .line 1985685
    new-instance v0, LX/0hs;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1985686
    :goto_0
    invoke-virtual {v0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1985687
    new-instance v2, LX/8uB;

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const-string v4, "/help/809497125833105"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/8uB;-><init>(Ljava/lang/String;)V

    .line 1985688
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 1985689
    iput v3, v2, LX/8uB;->a:I

    .line 1985690
    const/4 v3, 0x0

    .line 1985691
    iput-boolean v3, v2, LX/8uB;->b:Z

    .line 1985692
    new-instance v3, LX/47x;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1985693
    invoke-virtual {v3, p2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1985694
    const-string v4, "[[learn_more]]"

    const v5, 0x7f08002c

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v1, v2, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1985695
    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1985696
    const/16 v1, 0x1388

    .line 1985697
    iput v1, v0, LX/0hs;->t:I

    .line 1985698
    return-object v0

    .line 1985699
    :cond_0
    new-instance v0, LX/0hs;

    invoke-direct {v0, v1}, LX/0hs;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static b(LX/DJf;Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1985700
    new-instance v0, LX/DJe;

    invoke-direct {v0, p0, p1, p2}, LX/DJe;-><init>(LX/DJf;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1985701
    return-void
.end method
