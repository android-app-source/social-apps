.class public final LX/D2Z;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D2b;


# direct methods
.method public constructor <init>(LX/D2b;)V
    .locals 0

    .prologue
    .line 1958850
    iput-object p1, p0, LX/D2Z;->a:LX/D2b;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1958849
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1958834
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    .line 1958835
    sget-object v0, LX/D2Y;->a:[I

    .line 1958836
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1958837
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v2

    .line 1958838
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1958839
    :cond_0
    :goto_0
    return-void

    .line 1958840
    :pswitch_0
    iget-object v0, p0, LX/D2Z;->a:LX/D2b;

    iget-object v0, v0, LX/D2b;->a:Ljava/lang/String;

    .line 1958841
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1958842
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1958843
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1958844
    iget-object v0, p0, LX/D2Z;->a:LX/D2b;

    iget-object v0, v0, LX/D2b;->c:LX/D2T;

    iget-object v1, p0, LX/D2Z;->a:LX/D2b;

    iget-object v1, v1, LX/D2b;->a:Ljava/lang/String;

    .line 1958845
    invoke-static {v0, v1}, LX/D2T;->e(LX/D2T;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1958846
    invoke-static {v0}, LX/D2T;->c(LX/D2T;)V

    .line 1958847
    :cond_1
    iget-object v0, p0, LX/D2Z;->a:LX/D2b;

    iget-object v0, v0, LX/D2b;->d:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->e()V

    .line 1958848
    iget-object v0, p0, LX/D2Z;->a:LX/D2b;

    invoke-static {v0}, LX/D2b;->b(LX/D2b;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
