.class public final LX/DWd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 2007959
    const/4 v8, 0x0

    .line 2007960
    const-wide/16 v6, 0x0

    .line 2007961
    const/4 v5, 0x0

    .line 2007962
    const/4 v4, 0x0

    .line 2007963
    const/4 v3, 0x0

    .line 2007964
    const/4 v2, 0x0

    .line 2007965
    const/4 v1, 0x0

    .line 2007966
    const/4 v0, 0x0

    .line 2007967
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 2007968
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007969
    const/4 v0, 0x0

    .line 2007970
    :goto_0
    return v0

    .line 2007971
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v5, :cond_8

    .line 2007972
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 2007973
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2007974
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v11, :cond_0

    if-eqz v1, :cond_0

    .line 2007975
    const-string v5, "bylines"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2007976
    invoke-static {p0, p1}, LX/DWT;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 2007977
    :cond_1
    const-string v5, "communicationRank"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2007978
    const/4 v0, 0x1

    .line 2007979
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    goto :goto_1

    .line 2007980
    :cond_2
    const-string v5, "id"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2007981
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 2007982
    :cond_3
    const-string v5, "name"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2007983
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 2007984
    :cond_4
    const-string v5, "name_search_tokens"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2007985
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 2007986
    :cond_5
    const-string v5, "profilePicture50"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2007987
    invoke-static {p0, p1}, LX/DWV;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 2007988
    :cond_6
    const-string v5, "structured_name"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2007989
    invoke-static {p0, p1}, LX/DWS;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 2007990
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2007991
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2007992
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2007993
    if-eqz v0, :cond_9

    .line 2007994
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2007995
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2007996
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2007997
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2007998
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2007999
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2008000
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v4

    move v10, v5

    move v4, v8

    move v8, v3

    move v12, v2

    move-wide v2, v6

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const-wide/16 v2, 0x0

    .line 2008001
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2008002
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008003
    if-eqz v0, :cond_0

    .line 2008004
    const-string v1, "bylines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008005
    invoke-static {p0, v0, p2, p3}, LX/DWT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2008006
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2008007
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 2008008
    const-string v2, "communicationRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008009
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2008010
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2008011
    if-eqz v0, :cond_2

    .line 2008012
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008013
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2008014
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2008015
    if-eqz v0, :cond_3

    .line 2008016
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008017
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2008018
    :cond_3
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2008019
    if-eqz v0, :cond_4

    .line 2008020
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008021
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2008022
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008023
    if-eqz v0, :cond_5

    .line 2008024
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008025
    invoke-static {p0, v0, p2}, LX/DWV;->a(LX/15i;ILX/0nX;)V

    .line 2008026
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008027
    if-eqz v0, :cond_6

    .line 2008028
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008029
    invoke-static {p0, v0, p2, p3}, LX/DWS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2008030
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2008031
    return-void
.end method
