.class public LX/EtL;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/3qu;
.implements LX/3u7;
.implements LX/3uw;
.implements LX/3ux;


# static fields
.field private static l:LX/EtJ;


# instance fields
.field public a:LX/EtR;

.field public b:LX/EtN;

.field private c:LX/3v0;

.field private final d:Landroid/graphics/Paint;

.field private e:Z

.field private f:Z

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/view/accessibility/AccessibilityManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2177687
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2177688
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/EtL;->d:Landroid/graphics/Paint;

    .line 2177689
    const/4 v0, 0x4

    iput v0, p0, LX/EtL;->j:I

    .line 2177690
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EtL;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2177691
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2177692
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2177693
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/EtL;->d:Landroid/graphics/Paint;

    .line 2177694
    const/4 v0, 0x4

    iput v0, p0, LX/EtL;->j:I

    .line 2177695
    invoke-direct {p0, p1, p2}, LX/EtL;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2177696
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2177697
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2177698
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/EtL;->d:Landroid/graphics/Paint;

    .line 2177699
    const/4 v0, 0x4

    iput v0, p0, LX/EtL;->j:I

    .line 2177700
    invoke-direct {p0, p1, p2}, LX/EtL;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2177701
    return-void
.end method

.method public static a(LX/EtL;I)LX/EtO;
    .locals 4

    .prologue
    .line 2177702
    invoke-virtual {p0}, LX/EtL;->getChildCount()I

    move-result v2

    .line 2177703
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2177704
    invoke-virtual {p0, v1}, LX/EtL;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/EtO;

    .line 2177705
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/EtO;->getItemData()LX/3v3;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, LX/EtO;->getItemData()LX/3v3;

    move-result-object v3

    invoke-virtual {v3}, LX/3v3;->getItemId()I

    move-result v3

    if-ne p1, v3, :cond_0

    .line 2177706
    :goto_1
    return-object v0

    .line 2177707
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177708
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/EtL;LX/3qu;LX/3uE;)V
    .locals 2

    .prologue
    .line 2177709
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    if-ne p1, v0, :cond_0

    .line 2177710
    :goto_0
    return-void

    .line 2177711
    :cond_0
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    if-eqz v0, :cond_1

    .line 2177712
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    iget-object v1, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v0, v1}, LX/3v0;->b(LX/3us;)V

    .line 2177713
    :cond_1
    iget-object v0, p0, LX/EtL;->a:LX/EtR;

    .line 2177714
    iput-object p2, v0, LX/3ut;->g:LX/3uE;

    .line 2177715
    check-cast p1, LX/3v0;

    iput-object p1, p0, LX/EtL;->c:LX/3v0;

    .line 2177716
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    iget-object v1, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v0, v1}, LX/3v0;->a(LX/3us;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2177655
    sget-object v0, LX/03r;->InlineActionBar:[I

    invoke-virtual {p0}, LX/EtL;->getDefaultStyle()I

    move-result v1

    invoke-virtual {p1, p2, v0, v7, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 2177656
    :try_start_0
    const/16 v0, 0x0

    const v1, 0x7f0e0b44

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 2177657
    const/16 v0, 0xb

    const/4 v1, 0x4

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, LX/EtL;->j:I

    .line 2177658
    const/16 v0, 0xc

    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2177659
    new-instance v0, LX/EtR;

    invoke-virtual {p0}, LX/EtL;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v4, p0, LX/EtL;->j:I

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, LX/EtR;-><init>(Landroid/content/Context;LX/3ux;IILandroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, LX/EtL;->a:LX/EtR;

    .line 2177660
    iget-object v0, p0, LX/EtL;->d:Landroid/graphics/Paint;

    const/16 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v8, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2177661
    const/16 v0, 0xf

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 2177662
    iget-object v1, p0, LX/EtL;->d:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2177663
    const/16 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 2177664
    and-int/lit8 v0, v1, 0x1

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    iput-boolean v0, p0, LX/EtL;->e:Z

    .line 2177665
    and-int/lit8 v0, v1, 0x4

    if-eqz v0, :cond_1

    move v0, v6

    :goto_1
    iput-boolean v0, p0, LX/EtL;->f:Z

    .line 2177666
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, LX/EtL;->g:I

    .line 2177667
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, LX/EtL;->h:I

    .line 2177668
    const/16 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2177669
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2177670
    if-lez v0, :cond_2

    .line 2177671
    invoke-static {p0}, LX/EtL;->c(LX/EtL;)LX/3v0;

    move-result-object v1

    .line 2177672
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/EtL;->a(LX/EtL;LX/3qu;LX/3uE;)V

    .line 2177673
    invoke-virtual {v1}, LX/3v0;->f()V

    .line 2177674
    new-instance v2, LX/3ui;

    invoke-virtual {p0}, LX/EtL;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/3ui;-><init>(Landroid/content/Context;)V

    .line 2177675
    invoke-virtual {v2, v0, p0}, LX/3ui;->inflate(ILandroid/view/Menu;)V

    .line 2177676
    invoke-virtual {v1}, LX/3v0;->g()V

    .line 2177677
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EtL;->setWillNotDraw(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177678
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 2177679
    return-void

    :cond_0
    move v0, v7

    .line 2177680
    goto :goto_0

    :cond_1
    move v0, v7

    .line 2177681
    goto :goto_1

    .line 2177682
    :cond_2
    :try_start_1
    invoke-static {p0}, LX/EtL;->c(LX/EtL;)LX/3v0;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/EtL;->a(LX/EtL;LX/3qu;LX/3uE;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2177683
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public static c(LX/EtL;)LX/3v0;
    .locals 2

    .prologue
    .line 2177717
    new-instance v0, LX/3v0;

    invoke-virtual {p0}, LX/EtL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3v0;-><init>(Landroid/content/Context;)V

    .line 2177718
    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3u7;)V

    .line 2177719
    return-object v0
.end method

.method public static getAccessibilityManager(LX/EtL;)Landroid/view/accessibility/AccessibilityManager;
    .locals 2

    .prologue
    .line 2177720
    iget-object v0, p0, LX/EtL;->k:Landroid/view/accessibility/AccessibilityManager;

    if-nez v0, :cond_0

    .line 2177721
    invoke-virtual {p0}, LX/EtL;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/EtL;->k:Landroid/view/accessibility/AccessibilityManager;

    .line 2177722
    :cond_0
    iget-object v0, p0, LX/EtL;->k:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method private getLeftMargin()I
    .locals 1

    .prologue
    .line 2177723
    invoke-virtual {p0}, LX/EtL;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2177724
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method private getRightMargin()I
    .locals 1

    .prologue
    .line 2177725
    invoke-virtual {p0}, LX/EtL;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2177726
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)LX/3qv;
    .locals 2

    .prologue
    .line 2177727
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3v0;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 2177728
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3v3;->setShowAsAction(I)V

    .line 2177729
    return-object v0
.end method

.method public final a(IIILjava/lang/CharSequence;)LX/3qv;
    .locals 2

    .prologue
    .line 2177730
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3v0;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 2177731
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3v3;->setShowAsAction(I)V

    .line 2177732
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2177733
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->f()V

    .line 2177734
    return-void
.end method

.method public final a(LX/3v0;)V
    .locals 0

    .prologue
    .line 2177765
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2177735
    iput-boolean p1, p0, LX/EtL;->e:Z

    .line 2177736
    iput-boolean p2, p0, LX/EtL;->f:Z

    .line 2177737
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2177738
    invoke-static {p0}, LX/EtL;->getAccessibilityManager(LX/EtL;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, LX/EtL;->getAccessibilityManager(LX/EtL;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2177739
    if-nez v2, :cond_1

    .line 2177740
    iget-object v2, p0, LX/EtL;->b:LX/EtN;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/EtL;->b:LX/EtN;

    invoke-interface {v2, p2}, LX/EtN;->a(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2177741
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2177742
    goto :goto_1

    .line 2177743
    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {p0, v2}, LX/EtL;->a(LX/EtL;I)LX/EtO;

    move-result-object v2

    invoke-virtual {v2}, LX/EtO;->c()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2177744
    invoke-interface {p2}, Landroid/view/MenuItem;->isCheckable()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p2}, Landroid/view/MenuItem;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2177745
    :goto_2
    invoke-interface {p2}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    .line 2177746
    iget-object v1, p0, LX/EtL;->b:LX/EtN;

    invoke-interface {v1, p2}, LX/EtN;->a(Landroid/view/MenuItem;)Z

    move-result v1

    .line 2177747
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2177748
    const-string v5, "TEXT_BEFORE_CLICK"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2177749
    const-string v2, "CHECKED_BEFORE_CLICK"

    invoke-virtual {v4, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2177750
    const-string v0, "ENABLED_BEFORE_CLICK"

    invoke-virtual {v4, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2177751
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {p0, v0}, LX/EtL;->a(LX/EtL;I)LX/EtO;

    move-result-object v0

    const/16 v2, 0x10

    invoke-static {v0, v2, v4}, LX/0vv;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move v0, v1

    .line 2177752
    goto :goto_1

    :cond_2
    move v0, v1

    .line 2177753
    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/3v3;)Z
    .locals 2

    .prologue
    .line 2177754
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public final add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177755
    const/4 v0, 0x0

    .line 2177756
    invoke-virtual {p0, v0, v0, v0, p1}, LX/EtL;->a(IIII)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177757
    invoke-virtual {p0, p1, p2, p3, p4}, LX/EtL;->a(IIII)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177758
    invoke-virtual {p0, p1, p2, p3, p4}, LX/EtL;->a(IIILjava/lang/CharSequence;)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177759
    const/4 v0, 0x0

    .line 2177760
    invoke-virtual {p0, v0, v0, v0, p1}, LX/EtL;->a(IIILjava/lang/CharSequence;)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177761
    const/4 v0, 0x0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177762
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177763
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177764
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177684
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2177685
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->g()V

    .line 2177686
    return-void
.end method

.method public final b_(LX/3v0;)V
    .locals 0

    .prologue
    .line 2177596
    return-void
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 2177607
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->clear()V

    .line 2177608
    return-void
.end method

.method public final close()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177606
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 2177605
    const/4 v0, 0x0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2177598
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2177599
    iget-boolean v0, p0, LX/EtL;->e:Z

    if-eqz v0, :cond_0

    .line 2177600
    invoke-virtual {p0}, LX/EtL;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, LX/EtL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2177601
    :cond_0
    iget-boolean v0, p0, LX/EtL;->f:Z

    if-eqz v0, :cond_1

    .line 2177602
    invoke-virtual {p0}, LX/EtL;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2177603
    int-to-float v2, v0

    invoke-virtual {p0}, LX/EtL;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/EtL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2177604
    :cond_1
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177597
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getBottomSheetMenuStrategy()LX/EtJ;
    .locals 1

    .prologue
    .line 2177593
    sget-object v0, LX/EtL;->l:LX/EtJ;

    if-nez v0, :cond_0

    .line 2177594
    new-instance v0, LX/EtM;

    invoke-direct {v0, p0}, LX/EtM;-><init>(LX/EtL;)V

    sput-object v0, LX/EtL;->l:LX/EtJ;

    .line 2177595
    :cond_0
    sget-object v0, LX/EtL;->l:LX/EtJ;

    return-object v0
.end method

.method public getDefaultStyle()I
    .locals 1

    .prologue
    .line 2177592
    const v0, 0x7f0e0b43

    return v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 2177591
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getMaxVisibleButtons()I
    .locals 1

    .prologue
    .line 2177590
    iget v0, p0, LX/EtL;->j:I

    return v0
.end method

.method public getWindowAnimations()I
    .locals 1

    .prologue
    .line 2177589
    const/4 v0, 0x0

    return v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    .prologue
    .line 2177588
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177587
    const/4 v0, 0x0

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2177609
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2177610
    iget-object v0, p0, LX/EtL;->a:LX/EtR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EtR;->b(Z)V

    .line 2177611
    iget-object v0, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v0}, LX/EtR;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177612
    iget-object v0, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v0}, LX/EtR;->d()Z

    .line 2177613
    iget-object v0, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v0}, LX/EtR;->c()Z

    .line 2177614
    :cond_0
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2ecd473e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2177615
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 2177616
    iget-object v1, p0, LX/EtL;->a:LX/EtR;

    invoke-virtual {v1}, LX/EtR;->e()Z

    .line 2177617
    const/16 v1, 0x2d

    const v2, -0x23dd9427

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 2177618
    invoke-virtual {p0}, LX/EtL;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v3

    .line 2177619
    invoke-virtual {p0}, LX/EtL;->getChildCount()I

    move-result v4

    .line 2177620
    sub-int v0, p4, p2

    invoke-direct {p0}, LX/EtL;->getLeftMargin()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0}, LX/EtL;->getRightMargin()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2177621
    iget v1, p0, LX/EtL;->i:I

    mul-int/2addr v1, v4

    .line 2177622
    shr-int/lit8 v0, v0, 0x1

    shr-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    .line 2177623
    invoke-virtual {p0}, LX/EtL;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, LX/EtL;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/EtL;->getPaddingBottom()I

    move-result v2

    sub-int v5, v1, v2

    .line 2177624
    invoke-virtual {p0}, LX/EtL;->getPaddingTop()I

    move-result v6

    .line 2177625
    const/4 v1, 0x0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 2177626
    if-eqz v3, :cond_0

    sub-int v0, v4, v1

    add-int/lit8 v0, v0, -0x1

    .line 2177627
    :goto_1
    invoke-virtual {p0, v0}, LX/EtL;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v7, p0, LX/EtL;->i:I

    add-int/2addr v7, v2

    add-int v8, v6, v5

    invoke-virtual {v0, v2, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 2177628
    iget v0, p0, LX/EtL;->i:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 2177629
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2177630
    goto :goto_1

    .line 2177631
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2177632
    invoke-virtual {p0}, LX/EtL;->getChildCount()I

    move-result v3

    .line 2177633
    if-lez v3, :cond_0

    .line 2177634
    invoke-virtual {p0}, LX/EtL;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v1, p1}, LX/EtL;->getDefaultSize(II)I

    move-result v1

    .line 2177635
    iget v2, p0, LX/EtL;->g:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/EtL;->h:I

    sub-int/2addr v1, v2

    invoke-direct {p0}, LX/EtL;->getLeftMargin()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0}, LX/EtL;->getRightMargin()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2177636
    iget v2, p0, LX/EtL;->j:I

    div-int/2addr v1, v2

    iput v1, p0, LX/EtL;->i:I

    .line 2177637
    iget v1, p0, LX/EtL;->i:I

    invoke-static {p1, v0, v1}, LX/EtL;->getChildMeasureSpec(III)I

    move-result v4

    move v1, v0

    .line 2177638
    :goto_0
    if-ge v1, v3, :cond_0

    .line 2177639
    invoke-virtual {p0, v1}, LX/EtL;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4, p2}, Landroid/view/View;->measure(II)V

    .line 2177640
    invoke-virtual {p0, v1}, LX/EtL;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2177641
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 2177642
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/EtL;->setMeasuredDimension(II)V

    .line 2177643
    return-void
.end method

.method public final performIdentifierAction(II)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177644
    const/4 v0, 0x0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177645
    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1

    .prologue
    .line 2177646
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->removeGroup(I)V

    .line 2177647
    return-void
.end method

.method public final removeItem(I)V
    .locals 1

    .prologue
    .line 2177648
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->removeItem(I)V

    .line 2177649
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177650
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177651
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177652
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177653
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2177654
    iget-object v0, p0, LX/EtL;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->size()I

    move-result v0

    return v0
.end method
