.class public final LX/D2G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public f:Ljava/lang/String;

.field public g:J

.field public h:Lcom/facebook/share/model/ComposerAppAttribution;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1958383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V
    .locals 2

    .prologue
    .line 1958384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958385
    iget-object v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/D2G;->a:Landroid/net/Uri;

    .line 1958386
    iget v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    iput v0, p0, LX/D2G;->b:I

    .line 1958387
    iget v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    iput v0, p0, LX/D2G;->c:I

    .line 1958388
    iget v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    iput v0, p0, LX/D2G;->d:I

    .line 1958389
    iget-object v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, LX/D2G;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958390
    iget-object v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->f:Ljava/lang/String;

    iput-object v0, p0, LX/D2G;->f:Ljava/lang/String;

    .line 1958391
    iget-wide v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->g:J

    iput-wide v0, p0, LX/D2G;->g:J

    .line 1958392
    iget-object v0, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, LX/D2G;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1958393
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;
    .locals 2

    .prologue
    .line 1958382
    new-instance v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;-><init>(LX/D2G;)V

    return-object v0
.end method
