.class public final LX/EOc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOd;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsMediaCombinedModuleInterfaces$SearchResultsMediaCombinedModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/EOS;

.field public c:I

.field public final synthetic d:LX/EOd;


# direct methods
.method public constructor <init>(LX/EOd;)V
    .locals 1

    .prologue
    .line 2114869
    iput-object p1, p0, LX/EOc;->d:LX/EOd;

    .line 2114870
    move-object v0, p1

    .line 2114871
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114872
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114852
    const-string v0, "SearchResultsMediaCombinedGridComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2114853
    if-ne p0, p1, :cond_1

    .line 2114854
    :cond_0
    :goto_0
    return v0

    .line 2114855
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114856
    goto :goto_0

    .line 2114857
    :cond_3
    check-cast p1, LX/EOc;

    .line 2114858
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114859
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114860
    if-eq v2, v3, :cond_0

    .line 2114861
    iget-object v2, p0, LX/EOc;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOc;->a:LX/CzL;

    iget-object v3, p1, LX/EOc;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2114862
    goto :goto_0

    .line 2114863
    :cond_5
    iget-object v2, p1, LX/EOc;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2114864
    :cond_6
    iget-object v2, p0, LX/EOc;->b:LX/EOS;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOc;->b:LX/EOS;

    iget-object v3, p1, LX/EOc;->b:LX/EOS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2114865
    goto :goto_0

    .line 2114866
    :cond_8
    iget-object v2, p1, LX/EOc;->b:LX/EOS;

    if-nez v2, :cond_7

    .line 2114867
    :cond_9
    iget v2, p0, LX/EOc;->c:I

    iget v3, p1, LX/EOc;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2114868
    goto :goto_0
.end method
