.class public LX/D7X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Sj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/BUA;

.field public final c:LX/0tQ;

.field private final d:LX/19w;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/15V;

.field public final g:LX/BYZ;

.field private final h:Ljava/lang/String;

.field public final i:Lcom/facebook/graphql/model/GraphQLStory;

.field private final j:LX/04D;

.field private final k:Z

.field public final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final m:LX/15X;

.field private final n:LX/0kb;

.field public o:LX/2ft;

.field private p:Z


# direct methods
.method public constructor <init>(LX/BUA;LX/0tQ;LX/15X;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/19w;LX/0kb;LX/15V;LX/0Or;LX/04D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;ZLX/BYZ;)V
    .locals 0
    .param p9    # LX/04D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BUA;",
            "LX/0tQ;",
            "LX/15X;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/0kb;",
            "LX/15V;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/04D;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Z",
            "LX/BYZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1967285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967286
    iput-object p1, p0, LX/D7X;->b:LX/BUA;

    .line 1967287
    iput-object p2, p0, LX/D7X;->c:LX/0tQ;

    .line 1967288
    iput-object p3, p0, LX/D7X;->m:LX/15X;

    .line 1967289
    iput-object p4, p0, LX/D7X;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1967290
    iput-object p5, p0, LX/D7X;->d:LX/19w;

    .line 1967291
    iput-object p7, p0, LX/D7X;->f:LX/15V;

    .line 1967292
    iput-object p8, p0, LX/D7X;->e:LX/0Or;

    .line 1967293
    iput-object p9, p0, LX/D7X;->j:LX/04D;

    .line 1967294
    iput-object p10, p0, LX/D7X;->h:Ljava/lang/String;

    .line 1967295
    iput-object p11, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1967296
    iput-boolean p12, p0, LX/D7X;->k:Z

    .line 1967297
    iput-object p13, p0, LX/D7X;->g:LX/BYZ;

    .line 1967298
    iput-object p6, p0, LX/D7X;->n:LX/0kb;

    .line 1967299
    return-void
.end method

.method public static a(LX/D7X;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D7X;",
            "LX/0Or",
            "<",
            "LX/1Sj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1967284
    iput-object p1, p0, LX/D7X;->a:LX/0Or;

    return-void
.end method

.method public static a$redex0(LX/D7X;LX/2ft;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1967271
    iget-object v0, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1967272
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1967273
    :cond_0
    :goto_0
    return-void

    .line 1967274
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1967275
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v1

    int-to-long v6, v1

    .line 1967276
    new-instance v1, LX/BUL;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, LX/D7X;->h:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/D7X;->j:LX/04D;

    invoke-virtual {v0}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v5

    move-object v8, p1

    invoke-direct/range {v1 .. v8}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;)V

    .line 1967277
    iput-object p2, v1, LX/BUL;->o:Ljava/lang/String;

    .line 1967278
    iget-object v0, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0, v1}, LX/15V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/BUL;)V

    .line 1967279
    iget-object v0, p0, LX/D7X;->b:LX/BUA;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1967280
    iget-object v0, p0, LX/D7X;->b:LX/BUA;

    iget-object v1, p0, LX/D7X;->h:Ljava/lang/String;

    iget-object v2, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/BUA;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1967281
    iget-object v0, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    .line 1967282
    iget-object v0, p0, LX/D7X;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sj;

    .line 1967283
    iget-object v1, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    const-string v2, "video_download_button"

    const-string v3, "native_story"

    invoke-virtual {v0, v1, v2, v3}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 15

    .prologue
    .line 1967227
    iget-object v0, p0, LX/D7X;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 1967228
    invoke-virtual {v0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1967229
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1967230
    :cond_0
    :goto_0
    return-void

    .line 1967231
    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    new-instance v3, LX/D7S;

    invoke-direct {v3, p0, v1, v0}, LX/D7S;-><init>(LX/D7X;Landroid/app/Activity;LX/3Af;)V

    invoke-virtual {v2, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1967232
    new-instance v4, LX/34b;

    invoke-direct {v4, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 1967233
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0301d1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1967234
    const v2, 0x7f0d075a

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 1967235
    const v3, 0x7f0d075b

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 1967236
    sget-object v6, LX/D7V;->b:[I

    iget-object v7, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v7}, LX/0tQ;->m()LX/2qY;

    move-result-object v7

    invoke-virtual {v7}, LX/2qY;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1967237
    const v6, 0x7f080daf

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    move-object v6, v6

    .line 1967238
    invoke-virtual {v2, v6}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1967239
    invoke-virtual {v2, v6}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1967240
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f080db5

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1967241
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1967242
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1967243
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0a44

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v4, v5, v2}, LX/34b;->a(Landroid/view/View;F)V

    .line 1967244
    iget-object v2, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1967245
    const/4 v8, 0x0

    .line 1967246
    iget-object v7, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v7}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    .line 1967247
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    if-nez v9, :cond_5

    .line 1967248
    :cond_2
    iget-object v2, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->u()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1967249
    iget-object v7, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v7}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    .line 1967250
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    if-nez v8, :cond_6

    .line 1967251
    :cond_3
    :goto_2
    iget-object v2, p0, LX/D7X;->m:LX/15X;

    invoke-virtual {v2, v1}, LX/15X;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/D7X;->m:LX/15X;

    invoke-virtual {v3, v1}, LX/15X;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/34c;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f0207fd

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, LX/D7U;

    invoke-direct {v3, p0}, LX/D7U;-><init>(LX/D7X;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1967252
    :cond_4
    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1967253
    invoke-virtual {v0, v4}, LX/3Af;->a(LX/1OM;)V

    .line 1967254
    invoke-static {v0}, LX/4ml;->a(Landroid/app/Dialog;)V

    goto/16 :goto_0

    .line 1967255
    :pswitch_0
    const v6, 0x7f080dae

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 1967256
    :pswitch_1
    const v6, 0x7f080db0

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 1967257
    :cond_5
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 1967258
    invoke-static {v7}, LX/15V;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/Map;

    move-result-object v10

    .line 1967259
    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v7

    const/4 v9, 0x1

    if-le v7, v9, :cond_2

    .line 1967260
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    new-array v9, v8, [Ljava/lang/String;

    invoke-interface {v7, v9}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 1967261
    invoke-static {v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 1967262
    array-length v11, v7

    move v9, v8

    :goto_3
    if-ge v9, v11, :cond_2

    aget-object v12, v7, v9

    .line 1967263
    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-static {v13, v14}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v12, v8}, LX/34c;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    const v13, 0x7f020841

    invoke-virtual {v8, v13}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v8

    new-instance v13, LX/D7W;

    invoke-direct {v13, p0, v12}, LX/D7W;-><init>(LX/D7X;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1967264
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_3

    .line 1967265
    :cond_6
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 1967266
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v7

    int-to-long v7, v7

    .line 1967267
    iget-object v9, p0, LX/D7X;->m:LX/15X;

    invoke-virtual {v9, v1}, LX/15X;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v10}, LX/0tQ;->p()Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v7, p0, LX/D7X;->m:LX/15X;

    invoke-virtual {v7, v1}, LX/15X;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    :goto_4
    invoke-virtual {v4, v9, v7}, LX/34c;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 1967268
    const v8, 0x7f020841

    invoke-virtual {v7, v8}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1967269
    new-instance v8, LX/D7T;

    invoke-direct {v8, p0}, LX/D7T;-><init>(LX/D7X;)V

    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 1967270
    :cond_7
    invoke-static {v7, v8}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1967172
    iget-object v0, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1967173
    iget-object v0, p0, LX/D7X;->n:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    iput-boolean v0, p0, LX/D7X;->p:Z

    .line 1967174
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0xc22536c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1967175
    iget-object v1, p0, LX/D7X;->d:LX/19w;

    iget-object v2, p0, LX/D7X;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    .line 1967176
    sget-object v2, LX/D7V;->a:[I

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1967177
    :goto_0
    const v1, 0x252ecd14

    invoke-static {v1, v0}, LX/02F;->a(II)V

    :goto_1
    return-void

    .line 1967178
    :pswitch_0
    iget-boolean v1, p0, LX/D7X;->k:Z

    if-nez v1, :cond_0

    .line 1967179
    const v1, -0x58f75528

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 1967180
    :cond_0
    iget-object v1, p0, LX/D7X;->f:LX/15V;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FEED_ROW:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v1, v2}, LX/15V;->a(Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)V

    .line 1967181
    const v1, -0xd0cde3f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 1967182
    :pswitch_1
    iget-object v1, p0, LX/D7X;->b:LX/BUA;

    iget-object v2, p0, LX/D7X;->h:Ljava/lang/String;

    sget-object v3, LX/7Jb;->USER_INITIATED:LX/7Jb;

    invoke-virtual {v1, v2, v3}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1967183
    invoke-virtual {p0}, LX/D7X;->a()V

    .line 1967184
    const v1, 0x45c3a0ab

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 1967185
    :pswitch_2
    iget-object v1, p0, LX/D7X;->b:LX/BUA;

    iget-object v2, p0, LX/D7X;->h:Ljava/lang/String;

    iget-object v3, p0, LX/D7X;->j:LX/04D;

    invoke-virtual {v3}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v3

    .line 1967186
    new-instance v5, LX/BTw;

    invoke-direct {v5, v1, v2, v3}, LX/BTw;-><init>(LX/BUA;Ljava/lang/String;Ljava/lang/String;)V

    .line 1967187
    iget-object v6, v1, LX/BUA;->i:LX/1fW;

    invoke-virtual {v6, v5}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1967188
    iget-object v1, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/D7X;->p:Z

    if-eqz v1, :cond_1

    .line 1967189
    sget-object v1, LX/2ft;->NONE:LX/2ft;

    invoke-static {p0, v1, v4}, LX/D7X;->a$redex0(LX/D7X;LX/2ft;Ljava/lang/String;)V

    .line 1967190
    const v1, -0x6e9b2cdb

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 1967191
    :cond_1
    iget-object v1, p0, LX/D7X;->c:LX/0tQ;

    const/4 v2, 0x0

    .line 1967192
    invoke-virtual {v1}, LX/0tQ;->s()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, LX/0tQ;->a:LX/0ad;

    sget-short v5, LX/0wh;->R:S

    invoke-interface {v3, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    move v1, v2

    .line 1967193
    if-eqz v1, :cond_5

    .line 1967194
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1967195
    iget-object v5, p0, LX/D7X;->o:LX/2ft;

    if-nez v5, :cond_3

    .line 1967196
    iget-object v5, p0, LX/D7X;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/BUK;->b:LX/0Tn;

    iget-object v7, p0, LX/D7X;->c:LX/0tQ;

    .line 1967197
    iget-object v8, v7, LX/0tQ;->a:LX/0ad;

    sget v9, LX/0wh;->h:I

    sget-object v10, LX/2ft;->NONE:LX/2ft;

    iget v10, v10, LX/2ft;->mValue:I

    invoke-interface {v8, v9, v10}, LX/0ad;->a(II)I

    move-result v8

    move v7, v8

    .line 1967198
    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v5

    invoke-static {v5}, LX/2ft;->fromVal(I)LX/2ft;

    move-result-object v5

    iput-object v5, p0, LX/D7X;->o:LX/2ft;

    .line 1967199
    :cond_3
    iget-object v5, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1967200
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-nez v6, :cond_c

    .line 1967201
    :cond_4
    :goto_2
    const v1, 0x66ca3723

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 1967202
    :cond_5
    iget-object v1, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->r()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    .line 1967203
    iget-object v2, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_6

    .line 1967204
    iget-object v2, p0, LX/D7X;->i:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1967205
    if-nez v2, :cond_e

    .line 1967206
    :cond_6
    :goto_3
    move v1, v1

    .line 1967207
    if-nez v1, :cond_8

    :cond_7
    iget-object v1, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->u()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1967208
    :cond_8
    invoke-direct {p0}, LX/D7X;->d()V

    .line 1967209
    const v1, -0x60292055

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 1967210
    :cond_9
    iget-object v1, p0, LX/D7X;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Af;

    .line 1967211
    invoke-virtual {v1}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1967212
    new-instance v2, LX/D7Q;

    invoke-direct {v2, p0}, LX/D7Q;-><init>(LX/D7X;)V

    .line 1967213
    iget-object v3, p0, LX/D7X;->g:LX/BYZ;

    invoke-virtual {v3, v1, v2}, LX/BYZ;->a(Landroid/app/Activity;LX/BYY;)Z

    move-result v1

    move v1, v1

    .line 1967214
    if-eqz v1, :cond_a

    .line 1967215
    const v1, -0x64cb625a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 1967216
    :cond_a
    iget-object v1, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->t()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-boolean v1, p0, LX/D7X;->p:Z

    if-nez v1, :cond_b

    .line 1967217
    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    invoke-static {p0, v1, v4}, LX/D7X;->a$redex0(LX/D7X;LX/2ft;Ljava/lang/String;)V

    .line 1967218
    const v1, 0xf86075c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 1967219
    :cond_b
    sget-object v1, LX/2ft;->NONE:LX/2ft;

    invoke-static {p0, v1, v4}, LX/D7X;->a$redex0(LX/D7X;LX/2ft;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1967220
    :cond_c
    iget-object v6, p0, LX/D7X;->c:LX/0tQ;

    invoke-virtual {v6}, LX/0tQ;->p()Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v5, p0, LX/D7X;->m:LX/15X;

    invoke-virtual {v5, v1}, LX/15X;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1967221
    :goto_4
    new-instance v6, LX/D7Z;

    iget-object v7, p0, LX/D7X;->m:LX/15X;

    iget-object v8, p0, LX/D7X;->o:LX/2ft;

    invoke-direct {v6, v1, v7, v8, v5}, LX/D7Z;-><init>(Landroid/content/Context;LX/15X;LX/2ft;Ljava/lang/String;)V

    .line 1967222
    new-instance v5, LX/0ju;

    invoke-direct {v5, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v5

    const v7, 0x7f080da8

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const v7, 0x7f080da9

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/D7R;

    invoke-direct {v8, p0, v6}, LX/D7R;-><init>(LX/D7X;LX/D7Z;)V

    invoke-virtual {v5, v7, v8}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    .line 1967223
    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_2

    .line 1967224
    :cond_d
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v5

    int-to-long v5, v5

    invoke-static {v5, v6}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 1967225
    :cond_e
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v2

    .line 1967226
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v1, 0x1

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
