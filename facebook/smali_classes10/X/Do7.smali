.class public LX/Do7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Do7;


# instance fields
.field public a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2041471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041472
    iput-object p1, p0, LX/Do7;->a:LX/0Uh;

    .line 2041473
    return-void
.end method

.method public static a(LX/0QB;)LX/Do7;
    .locals 4

    .prologue
    .line 2041474
    sget-object v0, LX/Do7;->b:LX/Do7;

    if-nez v0, :cond_1

    .line 2041475
    const-class v1, LX/Do7;

    monitor-enter v1

    .line 2041476
    :try_start_0
    sget-object v0, LX/Do7;->b:LX/Do7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2041477
    if-eqz v2, :cond_0

    .line 2041478
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2041479
    new-instance p0, LX/Do7;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/Do7;-><init>(LX/0Uh;)V

    .line 2041480
    move-object v0, p0

    .line 2041481
    sput-object v0, LX/Do7;->b:LX/Do7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041482
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2041483
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2041484
    :cond_1
    sget-object v0, LX/Do7;->b:LX/Do7;

    return-object v0

    .line 2041485
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2041486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 2041487
    iget-object v0, p0, LX/Do7;->a:LX/0Uh;

    const/16 v1, 0x12a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
