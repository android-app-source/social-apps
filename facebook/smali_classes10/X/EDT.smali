.class public LX/EDT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ECA;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/EDO;

.field public d:LX/EDP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/media/AudioManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2S7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:J

.field public h:Z

.field public i:Z

.field public j:LX/EDR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2091402
    const-class v0, LX/EDT;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EDT;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2091392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2091393
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2091394
    iput-object v0, p0, LX/EDT;->b:LX/0Ot;

    .line 2091395
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2091396
    iput-object v0, p0, LX/EDT;->e:LX/0Ot;

    .line 2091397
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2091398
    iput-object v0, p0, LX/EDT;->f:LX/0Ot;

    .line 2091399
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2091400
    iput-object v0, p0, LX/EDT;->k:LX/0Ot;

    .line 2091401
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2091388
    iget-object v0, p0, LX/EDT;->c:LX/EDO;

    invoke-virtual {v0, p1, p2}, LX/EDO;->a(ILjava/lang/String;)V

    .line 2091389
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    if-eqz v0, :cond_0

    .line 2091390
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    invoke-virtual {v0, p1, p2}, LX/EDR;->a(ILjava/lang/String;)V

    .line 2091391
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2091361
    if-lez p1, :cond_1

    .line 2091362
    iget-object v0, p0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    iget-object v1, p0, LX/EDT;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v2

    iget-object v1, p0, LX/EDT;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v3

    iget-wide v4, p0, LX/EDT;->g:J

    move v1, p1

    invoke-virtual/range {v0 .. v5}, LX/2S7;->a(IZZJ)V

    .line 2091363
    iget-object v0, p0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "rating5"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091364
    if-eqz p2, :cond_0

    .line 2091365
    iget-object v0, p0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "survey_choice"

    invoke-virtual {v0, v1, p2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091366
    :cond_0
    if-eqz p3, :cond_1

    .line 2091367
    iget-object v0, p0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "survey_details"

    invoke-virtual {v0, v1, p3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091368
    :cond_1
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    if-eqz v0, :cond_2

    .line 2091369
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    invoke-virtual {v0, p1, p2, p3}, LX/EDR;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2091370
    :cond_2
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2091383
    const-wide/32 v0, 0x1d4c0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2091384
    iget-object v0, p0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "survey_shown"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091385
    :cond_0
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    if-eqz v0, :cond_1

    .line 2091386
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    invoke-virtual {v0, p1, p2}, LX/EDR;->a(J)V

    .line 2091387
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2091382
    iget-object v0, p0, LX/EDT;->c:LX/EDO;

    invoke-virtual {v0}, LX/EDO;->b()Z

    move-result v0

    return v0
.end method

.method public final b(LX/EDS;)J
    .locals 6

    .prologue
    const-wide/32 v2, 0xea60

    .line 2091379
    sget-object v0, LX/EDQ;->a:[I

    invoke-virtual {p1}, LX/EDS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-wide v0, v2

    .line 2091380
    :goto_0
    return-wide v0

    .line 2091381
    :pswitch_0
    iget-object v0, p0, LX/EDT;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v4, LX/3Dx;->ea:J

    invoke-interface {v0, v4, v5, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2091375
    iget-object v0, p0, LX/EDT;->c:LX/EDO;

    iget-boolean v1, p0, LX/EDT;->h:Z

    invoke-virtual {v0, p1, v1}, LX/EDO;->a(IZ)V

    .line 2091376
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    if-eqz v0, :cond_0

    .line 2091377
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    invoke-virtual {v0, p1}, LX/EDR;->b(I)V

    .line 2091378
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2091371
    iget-object v0, p0, LX/EDT;->c:LX/EDO;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/EDO;->a(IZ)V

    .line 2091372
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    if-eqz v0, :cond_0

    .line 2091373
    iget-object v0, p0, LX/EDT;->j:LX/EDR;

    invoke-virtual {v0, p1}, LX/EDR;->d(I)V

    .line 2091374
    :cond_0
    return-void
.end method
