.class public final LX/Csz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 0

    .prologue
    .line 1943665
    iput-object p1, p0, LX/Csz;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943660
    if-eqz p1, :cond_0

    .line 1943661
    iget-object v0, p0, LX/Csz;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1943662
    iget-object v0, p0, LX/Csz;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-boolean v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    if-nez v0, :cond_0

    .line 1943663
    iget-object v0, p0, LX/Csz;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget v1, LX/CoL;->L:I

    invoke-virtual {v0, v1}, LX/1af;->a(I)V

    .line 1943664
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1943666
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943659
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, LX/Csz;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
