.class public LX/Cxu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxh;


# instance fields
.field private final a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final b:LX/CzE;

.field private final c:LX/CvY;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CvY;)V
    .locals 0
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952287
    iput-object p1, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1952288
    iput-object p2, p0, LX/Cxu;->b:LX/CzE;

    .line 1952289
    iput-object p3, p0, LX/Cxu;->c:LX/CvY;

    .line 1952290
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/CvV;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/CvV;",
            ":",
            "LX/Cz6;",
            ">(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 1952240
    instance-of v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 1952241
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v1

    .line 1952242
    :goto_0
    iget-object v7, p0, LX/Cxu;->c:LX/CvY;

    iget-object v8, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v9, LX/8ch;->CLICK:LX/8ch;

    iget-object v0, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, p2

    check-cast v1, LX/Cz6;

    invoke-interface {v1}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    move-object v3, p2

    check-cast v3, LX/Cz6;

    invoke-interface {v3}, LX/Cz6;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    move v4, p3

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v7

    move-object v1, v8

    move-object v2, v9

    move v3, p3

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952243
    return-void

    .line 1952244
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CzL;LX/0P1;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1952282
    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v8

    .line 1952283
    iget-object v10, p0, LX/Cxu;->c:LX/CvY;

    iget-object v11, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v12, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v13

    iget-object v0, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ax()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CzE;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v3

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v4

    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    invoke-static {v5}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v5

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v7

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v9, p2

    invoke-static/range {v0 .. v9}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v10

    move-object v1, v11

    move-object v2, v12

    move v3, v13

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952284
    return-void

    .line 1952285
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V
    .locals 16

    .prologue
    .line 1952277
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxu;->b:LX/CzE;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v8

    .line 1952278
    invoke-virtual {v8}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1952279
    move-object/from16 v0, p0

    iget-object v11, v0, LX/Cxu;->c:LX/CvY;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v13, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v14

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->jD_()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v5, v4}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v4

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v5}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a()I

    move-result v8

    invoke-static {v8}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->jE_()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move v4, v14

    move-object v5, v15

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952280
    :cond_0
    return-void

    .line 1952281
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 12

    .prologue
    .line 1952248
    iget-object v0, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    .line 1952249
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1952250
    iget-object v7, p0, LX/Cxu;->c:LX/CvY;

    iget-object v8, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v9, LX/8ch;->CLICK:LX/8ch;

    iget-object v1, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v10

    iget-object v0, p0, LX/Cxu;->b:LX/CzE;

    .line 1952251
    iget-object v1, v0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_6

    iget-object v1, v0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1952252
    iget-object v4, v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    move-object v4, v4

    .line 1952253
    invoke-virtual {v4, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1952254
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    move-object v1, v2

    .line 1952255
    invoke-virtual {v1, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1952256
    :goto_1
    move-object v0, v1

    .line 1952257
    invoke-static {v0}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v11

    iget-object v0, p0, LX/Cxu;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v4, v3}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v3

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v5}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    .line 1952258
    sget-object p0, LX/CvJ;->ITEM_IN_MODULE_TAPPED:LX/CvJ;

    invoke-static {p0, v0}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_result_entity_id"

    invoke-virtual {p0, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "results_module_role"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "results_module_extra_logging"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_result_position"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_result_sub_position"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "results_module_items_count"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v5, p0

    .line 1952259
    move-object v0, v7

    move-object v1, v8

    move-object v2, v9

    move v3, v10

    move-object v4, v11

    invoke-virtual/range {v0 .. v5}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952260
    :cond_0
    :goto_2
    return-void

    .line 1952261
    :cond_1
    iget-object v0, p0, LX/Cxu;->b:LX/CzE;

    .line 1952262
    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1952263
    instance-of v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1952264
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 1952265
    :goto_3
    move-object v0, v1

    .line 1952266
    if-eqz v0, :cond_3

    .line 1952267
    iget-object v1, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v1, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/Cxu;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/CvV;I)V

    goto :goto_2

    .line 1952268
    :cond_3
    iget-object v0, p0, LX/Cxu;->b:LX/CzE;

    .line 1952269
    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1952270
    instance-of v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1952271
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    .line 1952272
    :goto_4
    move-object v0, v1

    .line 1952273
    if-eqz v0, :cond_0

    .line 1952274
    iget-object v1, p0, LX/Cxu;->b:LX/CzE;

    invoke-virtual {v1, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/Cxu;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/CvV;I)V

    goto :goto_2

    .line 1952275
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    .line 1952276
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final c(LX/CzL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1952245
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1952246
    invoke-virtual {p0, p1, v0}, LX/Cxu;->a(LX/CzL;LX/0P1;)V

    .line 1952247
    return-void
.end method
