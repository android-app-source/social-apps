.class public final enum LX/Exi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Exi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Exi;

.field public static final enum LOADING_ITEM:LX/Exi;

.field public static final enum PERSON_YOU_MAY_KNOW:LX/Exi;

.field public static final enum REGULAR_LIST_ITEM:LX/Exi;

.field public static final enum RESPONDED_PERSON_YOU_MAY_KNOW:LX/Exi;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2185146
    new-instance v0, LX/Exi;

    const-string v1, "PERSON_YOU_MAY_KNOW"

    invoke-direct {v0, v1, v2}, LX/Exi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Exi;->PERSON_YOU_MAY_KNOW:LX/Exi;

    .line 2185147
    new-instance v0, LX/Exi;

    const-string v1, "RESPONDED_PERSON_YOU_MAY_KNOW"

    invoke-direct {v0, v1, v3}, LX/Exi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Exi;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Exi;

    .line 2185148
    new-instance v0, LX/Exi;

    const-string v1, "REGULAR_LIST_ITEM"

    invoke-direct {v0, v1, v4}, LX/Exi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Exi;->REGULAR_LIST_ITEM:LX/Exi;

    .line 2185149
    new-instance v0, LX/Exi;

    const-string v1, "LOADING_ITEM"

    invoke-direct {v0, v1, v5}, LX/Exi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Exi;->LOADING_ITEM:LX/Exi;

    .line 2185150
    const/4 v0, 0x4

    new-array v0, v0, [LX/Exi;

    sget-object v1, LX/Exi;->PERSON_YOU_MAY_KNOW:LX/Exi;

    aput-object v1, v0, v2

    sget-object v1, LX/Exi;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Exi;

    aput-object v1, v0, v3

    sget-object v1, LX/Exi;->REGULAR_LIST_ITEM:LX/Exi;

    aput-object v1, v0, v4

    sget-object v1, LX/Exi;->LOADING_ITEM:LX/Exi;

    aput-object v1, v0, v5

    sput-object v0, LX/Exi;->$VALUES:[LX/Exi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2185145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Exi;
    .locals 1

    .prologue
    .line 2185144
    const-class v0, LX/Exi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Exi;

    return-object v0
.end method

.method public static values()[LX/Exi;
    .locals 1

    .prologue
    .line 2185143
    sget-object v0, LX/Exi;->$VALUES:[LX/Exi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Exi;

    return-object v0
.end method
