.class public final LX/DsZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/3Cv;


# direct methods
.method public constructor <init>(LX/3Cv;)V
    .locals 0

    .prologue
    .line 2050499
    iput-object p1, p0, LX/DsZ;->a:LX/3Cv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2050506
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 2050507
    iget-object v0, p0, LX/DsZ;->a:LX/3Cv;

    invoke-static {v0}, LX/3Cv;->g(LX/3Cv;)V

    .line 2050508
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2050505
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2050500
    iget-object v0, p0, LX/DsZ;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setPct(F)V

    .line 2050501
    iget-object v0, p0, LX/DsZ;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setVisibility(I)V

    .line 2050502
    iget-object v0, p0, LX/DsZ;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/PostFeedbackView;->setAlpha(F)V

    .line 2050503
    iget-object v0, p0, LX/DsZ;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/PostFeedbackView;->setVisibility(I)V

    .line 2050504
    return-void
.end method
