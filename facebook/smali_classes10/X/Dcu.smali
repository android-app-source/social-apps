.class public final enum LX/Dcu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dcu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dcu;

.field public static final enum CANCELLED:LX/Dcu;

.field public static final enum ERROR:LX/Dcu;

.field public static final enum SUCCESS:LX/Dcu;

.field public static final enum TIME_LIMIT_REACHED_CANCELLED:LX/Dcu;

.field public static final enum TIME_LIMIT_REACHED_SUCCESS:LX/Dcu;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2019100
    new-instance v0, LX/Dcu;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v2}, LX/Dcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcu;->CANCELLED:LX/Dcu;

    .line 2019101
    new-instance v0, LX/Dcu;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, LX/Dcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcu;->SUCCESS:LX/Dcu;

    .line 2019102
    new-instance v0, LX/Dcu;

    const-string v1, "TIME_LIMIT_REACHED_SUCCESS"

    invoke-direct {v0, v1, v4}, LX/Dcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcu;->TIME_LIMIT_REACHED_SUCCESS:LX/Dcu;

    .line 2019103
    new-instance v0, LX/Dcu;

    const-string v1, "TIME_LIMIT_REACHED_CANCELLED"

    invoke-direct {v0, v1, v5}, LX/Dcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcu;->TIME_LIMIT_REACHED_CANCELLED:LX/Dcu;

    .line 2019104
    new-instance v0, LX/Dcu;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, LX/Dcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcu;->ERROR:LX/Dcu;

    .line 2019105
    const/4 v0, 0x5

    new-array v0, v0, [LX/Dcu;

    sget-object v1, LX/Dcu;->CANCELLED:LX/Dcu;

    aput-object v1, v0, v2

    sget-object v1, LX/Dcu;->SUCCESS:LX/Dcu;

    aput-object v1, v0, v3

    sget-object v1, LX/Dcu;->TIME_LIMIT_REACHED_SUCCESS:LX/Dcu;

    aput-object v1, v0, v4

    sget-object v1, LX/Dcu;->TIME_LIMIT_REACHED_CANCELLED:LX/Dcu;

    aput-object v1, v0, v5

    sget-object v1, LX/Dcu;->ERROR:LX/Dcu;

    aput-object v1, v0, v6

    sput-object v0, LX/Dcu;->$VALUES:[LX/Dcu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019097
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dcu;
    .locals 1

    .prologue
    .line 2019099
    const-class v0, LX/Dcu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dcu;

    return-object v0
.end method

.method public static values()[LX/Dcu;
    .locals 1

    .prologue
    .line 2019098
    sget-object v0, LX/Dcu;->$VALUES:[LX/Dcu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dcu;

    return-object v0
.end method
