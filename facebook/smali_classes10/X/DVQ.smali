.class public final LX/DVQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberpicker/MemberPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V
    .locals 0

    .prologue
    .line 2005014
    iput-object p1, p0, LX/DVQ;->a:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x7a557725

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2005015
    iget-object v1, p0, LX/DVQ;->a:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L:LX/DVO;

    iget-object v2, p0, LX/DVQ;->a:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    .line 2005016
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v4, v1, LX/DVO;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    invoke-virtual {p0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    .line 2005017
    const-string p0, "group_feed_id"

    invoke-virtual {v4, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2005018
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUP_SHARE_LINK_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v4, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2005019
    iget-object p0, v1, LX/DVO;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, v1, LX/DVO;->e:Landroid/content/Context;

    invoke-interface {p0, v4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2005020
    const v1, -0x2311d28e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
