.class public LX/DaR;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;

.field public h:Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

.field public i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

.field public j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

.field public k:Lcom/facebook/events/model/Event;

.field private l:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
    .end annotation
.end field

.field public m:LX/DMP;

.field private n:Z

.field public final o:Landroid/graphics/Paint;

.field public p:F

.field public q:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2015036
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2015037
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/DaR;->o:Landroid/graphics/Paint;

    .line 2015038
    const-class v0, LX/DaR;

    invoke-static {v0, p0}, LX/DaR;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2015039
    const v0, 0x7f030804

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2015040
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DaR;->setOrientation(I)V

    .line 2015041
    const v0, 0x7f0d151d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    iput-object v0, p0, LX/DaR;->d:Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    .line 2015042
    const v0, 0x7f0d151e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DaR;->e:Landroid/widget/TextView;

    .line 2015043
    const v0, 0x7f0d151f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DaR;->f:Landroid/widget/TextView;

    .line 2015044
    const v0, 0x7f0d1520

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DaR;->g:Landroid/widget/TextView;

    .line 2015045
    const v0, 0x7f0d1521

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

    iput-object v0, p0, LX/DaR;->h:Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

    .line 2015046
    const v0, 0x7f0d1522

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    iput-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    .line 2015047
    const v0, 0x7f0d1523

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    iput-object v0, p0, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    .line 2015048
    iget-object v0, p0, LX/DaR;->o:Landroid/graphics/Paint;

    iget-object v1, p0, LX/DaR;->a:Landroid/content/res/Resources;

    const p1, 0x7f0a05ed

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2015049
    iget-object v0, p0, LX/DaR;->o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2015050
    iget-object v0, p0, LX/DaR;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/DaR;->p:F

    .line 2015051
    iget-object v0, p0, LX/DaR;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/DaR;->q:F

    .line 2015052
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DaR;->setWillNotDraw(Z)V

    .line 2015053
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DaR;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v2

    check-cast v2, LX/6RZ;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    iput-object v1, p1, LX/DaR;->a:Landroid/content/res/Resources;

    iput-object v2, p1, LX/DaR;->b:LX/6RZ;

    iput-object p0, p1, LX/DaR;->c:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2014969
    iget-object v0, p0, LX/DaR;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2014970
    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2014971
    iget-object v1, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 2014972
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2014973
    iget-object v0, p0, LX/DaR;->g:Landroid/widget/TextView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2014974
    iget-object p0, v1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, p0

    .line 2014975
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2014976
    :goto_0
    return-void

    .line 2014977
    :cond_0
    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2014978
    iget-object v1, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v1

    .line 2014979
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2014980
    iget-object v0, p0, LX/DaR;->g:Landroid/widget/TextView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2014981
    iget-object p0, v1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, p0

    .line 2014982
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2014983
    :cond_1
    iget-object v0, p0, LX/DaR;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Ljava/lang/String;Z)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0x8

    .line 2014995
    iput-object p1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2014996
    iput-object p2, p0, LX/DaR;->l:Ljava/lang/String;

    .line 2014997
    iput-boolean p3, p0, LX/DaR;->n:Z

    .line 2014998
    iget-object v0, p0, LX/DaR;->d:Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a(Lcom/facebook/events/model/Event;)V

    .line 2014999
    iget-object v0, p0, LX/DaR;->e:Landroid/widget/TextView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015000
    iget-object v2, v1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2015001
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2015002
    iget-object v0, p0, LX/DaR;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/DaR;->b:LX/6RZ;

    iget-object v2, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015003
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->N:Z

    move v2, v3

    .line 2015004
    iget-object v3, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v3

    iget-object v4, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    invoke-virtual {v4}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2015005
    invoke-direct {p0}, LX/DaR;->c()V

    .line 2015006
    const-string v0, "past"

    iget-object v1, p0, LX/DaR;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015007
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->B:Z

    move v0, v1

    .line 2015008
    if-nez v0, :cond_2

    .line 2015009
    :cond_0
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-virtual {v0, v5}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015010
    iget-object v0, p0, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-virtual {v0, v5}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->setVisibility(I)V

    .line 2015011
    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015012
    iget-object v1, p0, LX/DaR;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/DaR;->c:Ljava/lang/String;

    .line 2015013
    iget-object v2, v0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v2, v2

    .line 2015014
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2015015
    if-eqz v0, :cond_1

    .line 2015016
    iget-object v0, p0, LX/DaR;->h:Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

    const v1, 0x7f08306f

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(I)V

    .line 2015017
    :goto_1
    return-void

    .line 2015018
    :cond_1
    iget-object v0, p0, LX/DaR;->h:Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

    invoke-virtual {v0, v5}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    goto :goto_1

    .line 2015019
    :cond_2
    iget-object v0, p0, LX/DaR;->h:Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a(Lcom/facebook/events/model/Event;)V

    .line 2015020
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 2015021
    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015022
    iget-boolean v2, v0, Lcom/facebook/events/model/Event;->B:Z

    move v0, v2

    .line 2015023
    if-eqz v0, :cond_5

    .line 2015024
    iget-object v0, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    .line 2015025
    iget-boolean v2, v0, Lcom/facebook/events/model/Event;->H:Z

    move v0, v2

    .line 2015026
    if-eqz v0, :cond_4

    .line 2015027
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015028
    iget-object v0, p0, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->setVisibility(I)V

    .line 2015029
    iget-object v0, p0, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DaR;->m:LX/DMP;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/events/model/Event;LX/DMP;)V

    .line 2015030
    :goto_2
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 2015031
    :cond_4
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015032
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    iget-object v1, p0, LX/DaR;->k:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DaR;->m:LX/DMP;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a(Lcom/facebook/events/model/Event;LX/DMP;)V

    .line 2015033
    iget-object v0, p0, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->setVisibility(I)V

    goto :goto_2

    .line 2015034
    :cond_5
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015035
    iget-object v0, p0, LX/DaR;->i:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2014987
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2014988
    iget-boolean v0, p0, LX/DaR;->n:Z

    if-eqz v0, :cond_0

    .line 2014989
    iget v1, p0, LX/DaR;->p:F

    .line 2014990
    invoke-virtual {p0}, LX/DaR;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/DaR;->p:F

    sub-float v3, v0, v2

    .line 2014991
    invoke-virtual {p0}, LX/DaR;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/DaR;->q:F

    sub-float v2, v0, v2

    .line 2014992
    invoke-virtual {p0}, LX/DaR;->getHeight()I

    move-result v0

    int-to-float v4, v0

    .line 2014993
    iget-object v5, p0, LX/DaR;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2014994
    :cond_0
    return-void
.end method

.method public setGroupEventRsvpUpdateListener(LX/DMP;)V
    .locals 1

    .prologue
    .line 2014984
    iget-object v0, p0, LX/DaR;->m:LX/DMP;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2014985
    iput-object p1, p0, LX/DaR;->m:LX/DMP;

    .line 2014986
    :cond_0
    return-void
.end method
