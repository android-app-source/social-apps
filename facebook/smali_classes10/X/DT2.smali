.class public final LX/DT2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupRemoveMemberMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/DT7;


# direct methods
.method public constructor <init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000140
    iput-object p1, p0, LX/DT2;->d:LX/DT7;

    iput-object p2, p0, LX/DT2;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DT2;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DT2;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2000138
    iget-object v0, p0, LX/DT2;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DT2;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fa4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000139
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2000135
    iget-object v0, p0, LX/DT2;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DT2;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082f9e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/DT2;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000136
    iget-object v0, p0, LX/DT2;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->f:LX/DTh;

    new-instance v1, LX/DTl;

    iget-object v2, p0, LX/DT2;->b:Ljava/lang/String;

    iget-object v3, p0, LX/DT2;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/DTl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2000137
    return-void
.end method
