.class public final LX/EYn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/EYF;

.field private final b:[LX/EYg;

.field private c:[Ljava/lang/String;

.field private volatile d:Z


# direct methods
.method public constructor <init>(LX/EYF;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2138046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138047
    iput-object p1, p0, LX/EYn;->a:LX/EYF;

    .line 2138048
    iput-object p2, p0, LX/EYn;->c:[Ljava/lang/String;

    .line 2138049
    invoke-virtual {p1}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LX/EYg;

    iput-object v0, p0, LX/EYn;->b:[LX/EYg;

    .line 2138050
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EYn;->d:Z

    .line 2138051
    return-void
.end method

.method public static a$redex0(LX/EYn;LX/EYP;)LX/EYg;
    .locals 2

    .prologue
    .line 2138074
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2138075
    iget-object v1, p0, LX/EYn;->a:LX/EYF;

    if-eq v0, v1, :cond_0

    .line 2138076
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138077
    :cond_0
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2138078
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This type does not have extensions."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138079
    :cond_1
    iget-object v0, p0, LX/EYn;->b:[LX/EYg;

    .line 2138080
    iget v1, p1, LX/EYP;->b:I

    move v1, v1

    .line 2138081
    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/EWp;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWj;",
            ">;)",
            "LX/EYn;"
        }
    .end annotation

    .prologue
    .line 2138052
    iget-boolean v0, p0, LX/EYn;->d:Z

    if-eqz v0, :cond_0

    .line 2138053
    :goto_0
    return-object p0

    .line 2138054
    :cond_0
    monitor-enter p0

    .line 2138055
    :try_start_0
    iget-boolean v0, p0, LX/EYn;->d:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    goto :goto_0

    .line 2138056
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2138057
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    :try_start_1
    iget-object v0, p0, LX/EYn;->b:[LX/EYg;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 2138058
    iget-object v0, p0, LX/EYn;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2138059
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2138060
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v3, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v2, v3, :cond_2

    .line 2138061
    iget-object v2, p0, LX/EYn;->b:[LX/EYg;

    new-instance v3, LX/EYj;

    iget-object v4, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {v3, v0, v4, p1, p2}, LX/EYj;-><init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    .line 2138062
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2138063
    :cond_2
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v3, LX/EYN;->ENUM:LX/EYN;

    if-ne v2, v3, :cond_3

    .line 2138064
    iget-object v2, p0, LX/EYn;->b:[LX/EYg;

    new-instance v3, LX/EYi;

    iget-object v4, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {v3, v0, v4, p1, p2}, LX/EYi;-><init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_2

    .line 2138065
    :cond_3
    iget-object v0, p0, LX/EYn;->b:[LX/EYg;

    new-instance v2, LX/EYh;

    iget-object v3, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-direct {v2, v3, p1, p2}, LX/EYh;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    goto :goto_2

    .line 2138066
    :cond_4
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v3, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v2, v3, :cond_5

    .line 2138067
    iget-object v2, p0, LX/EYn;->b:[LX/EYg;

    new-instance v3, LX/EYm;

    iget-object v4, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {v3, v0, v4, p1, p2}, LX/EYm;-><init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_2

    .line 2138068
    :cond_5
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v3, LX/EYN;->ENUM:LX/EYN;

    if-ne v2, v3, :cond_6

    .line 2138069
    iget-object v2, p0, LX/EYn;->b:[LX/EYg;

    new-instance v3, LX/EYl;

    iget-object v4, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {v3, v0, v4, p1, p2}, LX/EYl;-><init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_2

    .line 2138070
    :cond_6
    iget-object v0, p0, LX/EYn;->b:[LX/EYg;

    new-instance v2, LX/EYk;

    iget-object v3, p0, LX/EYn;->c:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-direct {v2, v3, p1, p2}, LX/EYk;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    goto :goto_2

    .line 2138071
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EYn;->d:Z

    .line 2138072
    const/4 v0, 0x0

    iput-object v0, p0, LX/EYn;->c:[Ljava/lang/String;

    .line 2138073
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
