.class public LX/DpV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final unix_time_micros:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045715
    new-instance v0, LX/1sv;

    const-string v1, "ReceiptPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpV;->b:LX/1sv;

    .line 2045716
    new-instance v0, LX/1sw;

    const-string v1, "unix_time_micros"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpV;->c:LX/1sw;

    .line 2045717
    const/4 v0, 0x1

    sput-boolean v0, LX/DpV;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2045718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045719
    iput-object p1, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    .line 2045720
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2045669
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045670
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045671
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045672
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ReceiptPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045673
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045674
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045675
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045676
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045677
    const-string v4, "unix_time_micros"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045678
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045679
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045680
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 2045681
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045682
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045683
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045684
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045685
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2045686
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2045687
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2045688
    :cond_3
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2045708
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045709
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2045710
    sget-object v0, LX/DpV;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045711
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2045712
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045713
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045714
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045693
    if-nez p1, :cond_1

    .line 2045694
    :cond_0
    :goto_0
    return v0

    .line 2045695
    :cond_1
    instance-of v1, p1, LX/DpV;

    if-eqz v1, :cond_0

    .line 2045696
    check-cast p1, LX/DpV;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045697
    if-nez p1, :cond_3

    .line 2045698
    :cond_2
    :goto_1
    move v0, v2

    .line 2045699
    goto :goto_0

    .line 2045700
    :cond_3
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2045701
    :goto_2
    iget-object v3, p1, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2045702
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045703
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045704
    iget-object v0, p0, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    iget-object v3, p1, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2045705
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2045706
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2045707
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045692
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045689
    sget-boolean v0, LX/DpV;->a:Z

    .line 2045690
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpV;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045691
    return-object v0
.end method
