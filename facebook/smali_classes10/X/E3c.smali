.class public final LX/E3c;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:LX/E2b;

.field public final synthetic e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2b;)V
    .locals 0

    .prologue
    .line 2074832
    iput-object p1, p0, LX/E3c;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3c;->a:LX/0Px;

    iput-object p3, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/E3c;->c:LX/1Pn;

    iput-object p5, p0, LX/E3c;->d:LX/E2b;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2074833
    iget-object v0, p0, LX/E3c;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    move-object v1, v0

    .line 2074834
    :goto_0
    iget-object v0, p0, LX/E3c;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v0, p0, LX/E3c;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2074835
    new-instance v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v5, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074836
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v6

    .line 2074837
    iget-object v6, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074838
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v7

    .line 2074839
    iget-object v7, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074840
    iget-object v8, v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2074841
    invoke-direct {v4, v5, v0, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2074842
    iget-object v5, p0, LX/E3c;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    iget-object v5, v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->e:LX/1vo;

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2074843
    instance-of v5, v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v5, :cond_2

    .line 2074844
    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v5, p0, LX/E3c;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/E3c;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    invoke-static {v0, v1, v5, v6}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2074845
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2074846
    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->b:LX/1Cz;

    move-object v1, v0

    goto :goto_0

    .line 2074847
    :cond_2
    instance-of v5, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v5, :cond_0

    .line 2074848
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v5, p0, LX/E3c;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/E3c;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    invoke-static {v0, v1, v5, v6}, LX/6Vo;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_2

    .line 2074849
    :cond_3
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2074850
    iget-object v0, p0, LX/E3c;->c:LX/1Pn;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/E3c;->d:LX/E2b;

    iget-object v2, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2074851
    iput p1, v0, LX/E2c;->d:I

    .line 2074852
    iget-object v0, p0, LX/E3c;->c:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074853
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2074854
    iget-object v2, p0, LX/E3c;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074855
    iget-object p0, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2074856
    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074857
    return-void
.end method
