.class public final enum LX/CvL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CvL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CvL;

.field public static final enum ENTER:LX/CvL;

.field public static final enum EXIT:LX/CvL;

.field public static final enum FIRST_SCROLL:LX/CvL;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1948070
    new-instance v0, LX/CvL;

    const-string v1, "ENTER"

    const-string v2, "enter"

    invoke-direct {v0, v1, v3, v2}, LX/CvL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvL;->ENTER:LX/CvL;

    .line 1948071
    new-instance v0, LX/CvL;

    const-string v1, "EXIT"

    const-string v2, "exit"

    invoke-direct {v0, v1, v4, v2}, LX/CvL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvL;->EXIT:LX/CvL;

    .line 1948072
    new-instance v0, LX/CvL;

    const-string v1, "FIRST_SCROLL"

    const-string v2, "first_scroll"

    invoke-direct {v0, v1, v5, v2}, LX/CvL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvL;->FIRST_SCROLL:LX/CvL;

    .line 1948073
    const/4 v0, 0x3

    new-array v0, v0, [LX/CvL;

    sget-object v1, LX/CvL;->ENTER:LX/CvL;

    aput-object v1, v0, v3

    sget-object v1, LX/CvL;->EXIT:LX/CvL;

    aput-object v1, v0, v4

    sget-object v1, LX/CvL;->FIRST_SCROLL:LX/CvL;

    aput-object v1, v0, v5

    sput-object v0, LX/CvL;->$VALUES:[LX/CvL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1948074
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1948075
    iput-object p3, p0, LX/CvL;->value:Ljava/lang/String;

    .line 1948076
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CvL;
    .locals 1

    .prologue
    .line 1948077
    const-class v0, LX/CvL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CvL;

    return-object v0
.end method

.method public static values()[LX/CvL;
    .locals 1

    .prologue
    .line 1948078
    sget-object v0, LX/CvL;->$VALUES:[LX/CvL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CvL;

    return-object v0
.end method
