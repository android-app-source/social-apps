.class public LX/Coi;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CoY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnn;",
        ">;",
        "LX/CoY;"
    }
.end annotation


# instance fields
.field public d:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final g:Lcom/facebook/richdocument/view/widget/RichTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1935976
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1935977
    const-class v0, LX/Coi;

    invoke-static {v0, p0}, LX/Coi;->a(Ljava/lang/Class;LX/02k;)V

    .line 1935978
    const v0, 0x7f0d1846

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935979
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setEnableCopy(Z)V

    .line 1935980
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    new-instance v1, LX/Cpi;

    invoke-direct {v1, p0}, LX/Cpi;-><init>(LX/Coi;)V

    .line 1935981
    iput-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->k:LX/Cjq;

    .line 1935982
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935983
    iget-object v0, p0, LX/Coi;->f:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1935984
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935985
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1935986
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1935987
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935988
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935989
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1935990
    invoke-virtual {v0, v2}, LX/CtG;->setLayoutDirection(I)V

    .line 1935991
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutDirection(I)V

    .line 1935992
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1935993
    :cond_0
    :goto_0
    new-instance v0, LX/Cn7;

    new-instance v1, LX/CnB;

    iget-object v2, p0, LX/Coi;->d:LX/Cju;

    invoke-direct {v1, v2}, LX/CnB;-><init>(LX/Cju;)V

    new-instance v2, LX/Cn5;

    iget-object v3, p0, LX/Coi;->e:LX/CIg;

    invoke-direct {v2, v3}, LX/Cn5;-><init>(LX/CIg;)V

    new-instance v3, LX/CnA;

    invoke-direct {v3}, LX/CnA;-><init>()V

    new-instance v4, LX/Cn8;

    invoke-direct {v4}, LX/Cn8;-><init>()V

    new-instance v5, LX/Cn9;

    invoke-direct {v5}, LX/Cn9;-><init>()V

    new-instance v6, LX/CnC;

    iget-object v7, p0, LX/Coi;->d:LX/Cju;

    invoke-direct {v6, v7}, LX/CnC;-><init>(LX/Cju;)V

    invoke-direct/range {v0 .. v6}, LX/Cn7;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;LX/Cmm;LX/Cmt;)V

    .line 1935994
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1935995
    return-void

    .line 1935996
    :cond_1
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935997
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1935998
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1935999
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1936000
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936001
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936002
    invoke-virtual {v0, v3}, LX/CtG;->setLayoutDirection(I)V

    .line 1936003
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutDirection(I)V

    .line 1936004
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutDirection(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Coi;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object v2

    check-cast v2, LX/CIg;

    invoke-static {p0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object p0

    check-cast p0, LX/Crz;

    iput-object v1, p1, LX/Coi;->d:LX/Cju;

    iput-object v2, p1, LX/Coi;->e:LX/CIg;

    iput-object p0, p1, LX/Coi;->f:LX/Crz;

    return-void
.end method


# virtual methods
.method public final a(LX/Clf;)V
    .locals 1

    .prologue
    .line 1936005
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936006
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1936007
    invoke-virtual {v0, p1}, LX/CtG;->setText(LX/Clf;)V

    .line 1936008
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1935973
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1935974
    iget-object v0, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1935975
    return-void
.end method
