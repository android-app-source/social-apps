.class public LX/DPT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/DPT;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0Or;LX/0tX;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993428
    iput-object p1, p0, LX/DPT;->a:LX/0Or;

    .line 1993429
    iput-object p2, p0, LX/DPT;->b:LX/0tX;

    .line 1993430
    return-void
.end method

.method public static a(LX/0QB;)LX/DPT;
    .locals 5

    .prologue
    .line 1993404
    sget-object v0, LX/DPT;->c:LX/DPT;

    if-nez v0, :cond_1

    .line 1993405
    const-class v1, LX/DPT;

    monitor-enter v1

    .line 1993406
    :try_start_0
    sget-object v0, LX/DPT;->c:LX/DPT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1993407
    if-eqz v2, :cond_0

    .line 1993408
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1993409
    new-instance v4, LX/DPT;

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {v4, p0, v3}, LX/DPT;-><init>(LX/0Or;LX/0tX;)V

    .line 1993410
    move-object v0, v4

    .line 1993411
    sput-object v0, LX/DPT;->c:LX/DPT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1993412
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1993413
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1993414
    :cond_1
    sget-object v0, LX/DPT;->c:LX/DPT;

    return-object v0

    .line 1993415
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1993416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/groupsgrid/mutations/FavoriteGroupsMutationsModels$GroupsBookmarkAddToFavoritesMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1993422
    new-instance v1, LX/4D9;

    invoke-direct {v1}, LX/4D9;-><init>()V

    .line 1993423
    const-string v0, "treehouse_android_groups_tab"

    invoke-virtual {v1, v0}, LX/4D9;->c(Ljava/lang/String;)LX/4D9;

    move-result-object v2

    iget-object v0, p0, LX/DPT;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/4D9;->a(Ljava/lang/String;)LX/4D9;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4D9;->b(Ljava/lang/String;)LX/4D9;

    .line 1993424
    new-instance v0, LX/DPE;

    invoke-direct {v0}, LX/DPE;-><init>()V

    move-object v0, v0

    .line 1993425
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1993426
    iget-object v1, p0, LX/DPT;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/groupsgrid/mutations/FavoriteGroupsMutationsModels$GroupsBookmarkRemoveFromFavoritesMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1993417
    new-instance v1, LX/2rA;

    invoke-direct {v1}, LX/2rA;-><init>()V

    .line 1993418
    const-string v0, "treehouse_android_groups_tab"

    invoke-virtual {v1, v0}, LX/2rA;->c(Ljava/lang/String;)LX/2rA;

    move-result-object v2

    iget-object v0, p0, LX/DPT;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/2rA;->a(Ljava/lang/String;)LX/2rA;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2rA;->b(Ljava/lang/String;)LX/2rA;

    .line 1993419
    new-instance v0, LX/DPF;

    invoke-direct {v0}, LX/DPF;-><init>()V

    move-object v0, v0

    .line 1993420
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1993421
    iget-object v1, p0, LX/DPT;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
