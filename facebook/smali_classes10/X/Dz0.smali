.class public LX/Dz0;
.super LX/Dyu;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:LX/9jG;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066338
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066339
    iput-object p1, p0, LX/Dz0;->a:Landroid/view/LayoutInflater;

    .line 2066340
    return-void
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066337
    sget-object v0, LX/9jL;->SocialSearchAddPlaceSeekerHeader:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2066332
    if-nez p1, :cond_0

    .line 2066333
    iget-object v0, p0, LX/Dz0;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0312eb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 2066334
    :cond_0
    const v0, 0x7f0d2c10

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2066335
    const v1, 0x7f081771

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2066336
    return-object p1
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066329
    iget-object v0, p0, LX/Dz0;->b:LX/9jG;

    sget-object v1, LX/9jG;->SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

    if-ne v0, v1, :cond_0

    .line 2066330
    sget-object v0, LX/9jL;->SocialSearchAddPlaceSeekerHeader:LX/9jL;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066331
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 2066328
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066327
    const/4 v0, 0x0

    return v0
.end method
