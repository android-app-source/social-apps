.class public final LX/DrS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

.field public final synthetic b:LX/Drj;

.field public final synthetic c:LX/1Pr;

.field public final synthetic d:LX/Dq2;

.field public final synthetic e:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/Drj;LX/1Pr;LX/Dq2;)V
    .locals 0

    .prologue
    .line 2049460
    iput-object p1, p0, LX/DrS;->e:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    iput-object p2, p0, LX/DrS;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    iput-object p3, p0, LX/DrS;->b:LX/Drj;

    iput-object p4, p0, LX/DrS;->c:LX/1Pr;

    iput-object p5, p0, LX/DrS;->d:LX/Dq2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x2

    const v0, 0x1eafac08    # 1.8599996E-20f

    invoke-static {v3, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049461
    iget-object v0, p0, LX/DrS;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049462
    iget-object v2, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v2

    .line 2049463
    invoke-interface {v0}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v0

    .line 2049464
    iget-object v2, p0, LX/DrS;->b:LX/Drj;

    .line 2049465
    iget-object v4, v2, LX/Drj;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2049466
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2049467
    const v0, 0x50f3c544

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2049468
    :goto_0
    return-void

    .line 2049469
    :cond_0
    iget-object v2, p0, LX/DrS;->b:LX/Drj;

    .line 2049470
    iput-object v0, v2, LX/Drj;->a:Ljava/lang/String;

    .line 2049471
    iget-object v0, p0, LX/DrS;->e:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    iget-object v2, v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->b:LX/33W;

    iget-object v0, p0, LX/DrS;->c:LX/1Pr;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, LX/DrS;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049472
    iget-object v4, v3, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v3, v4

    .line 2049473
    sget-object v4, LX/8D0;->SINGLE_SELECTOR:LX/8D0;

    iget-object v5, p0, LX/DrS;->d:LX/Dq2;

    invoke-virtual {v2, v0, v3, v4, v5}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;LX/Dq2;)V

    .line 2049474
    iget-object v0, p0, LX/DrS;->c:LX/1Pr;

    check-cast v0, LX/1Pq;

    new-array v2, v6, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/DrS;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2049475
    const v0, 0x6e725a03

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
