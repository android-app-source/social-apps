.class public final LX/D8e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:F

.field public final synthetic c:LX/D8f;


# direct methods
.method public constructor <init>(LX/D8f;FF)V
    .locals 0

    .prologue
    .line 1969075
    iput-object p1, p0, LX/D8e;->c:LX/D8f;

    iput p2, p0, LX/D8e;->a:F

    iput p3, p0, LX/D8e;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1969076
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1969077
    iget v0, p0, LX/D8e;->a:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    move v0, v1

    .line 1969078
    :goto_0
    iget v3, p0, LX/D8e;->b:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    move v2, v1

    .line 1969079
    :cond_0
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    neg-int v0, v0

    move v1, v0

    .line 1969080
    :goto_1
    if-eqz v2, :cond_4

    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    neg-int v0, v0

    .line 1969081
    :goto_2
    iget-object v2, p0, LX/D8e;->c:LX/D8f;

    iget-object v2, v2, LX/D8f;->b:LX/D8a;

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, LX/D8a;->a(FF)V

    .line 1969082
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->a:LX/D7g;

    iget-object v1, p0, LX/D8e;->c:LX/D8f;

    iget-object v1, v1, LX/D8f;->b:LX/D8a;

    .line 1969083
    iget v2, v1, LX/D8a;->j:F

    move v1, v2

    .line 1969084
    invoke-interface {v0, v1}, LX/D7g;->a(F)V

    .line 1969085
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->a:LX/D7g;

    iget-object v1, p0, LX/D8e;->c:LX/D8f;

    iget-object v1, v1, LX/D8f;->b:LX/D8a;

    .line 1969086
    iget v2, v1, LX/D8a;->k:I

    move v1, v2

    .line 1969087
    invoke-interface {v0, v1}, LX/D7g;->a(I)V

    .line 1969088
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->a:LX/D7g;

    iget-object v1, p0, LX/D8e;->c:LX/D8f;

    iget-object v1, v1, LX/D8f;->b:LX/D8a;

    .line 1969089
    iget v2, v1, LX/D8a;->l:I

    move v1, v2

    .line 1969090
    invoke-interface {v0, v1}, LX/D7g;->b(I)V

    .line 1969091
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->e:LX/D8S;

    if-eqz v0, :cond_1

    .line 1969092
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->e:LX/D8S;

    iget-object v1, p0, LX/D8e;->c:LX/D8f;

    iget-object v1, v1, LX/D8f;->b:LX/D8a;

    .line 1969093
    iget v2, v1, LX/D8a;->j:F

    move v1, v2

    .line 1969094
    invoke-virtual {v0, v1}, LX/D8S;->a(F)Z

    .line 1969095
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1969096
    goto :goto_0

    .line 1969097
    :cond_3
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    move v1, v0

    goto :goto_1

    .line 1969098
    :cond_4
    iget-object v0, p0, LX/D8e;->c:LX/D8f;

    iget-object v0, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    goto :goto_2
.end method
