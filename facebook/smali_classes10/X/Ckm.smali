.class public LX/Ckm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:F

.field public c:F

.field public d:F

.field public e:I

.field public f:LX/Ckk;

.field public g:I

.field public h:LX/Ckl;


# direct methods
.method public constructor <init>(ZLX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1931298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931299
    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Ckm;->a:Ljava/lang/String;

    .line 1931300
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    :goto_0
    double-to-float v0, v0

    iput v0, p0, LX/Ckm;->b:F

    .line 1931301
    if-eqz p1, :cond_1

    const/4 v0, 0x5

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    :goto_1
    double-to-float v0, v0

    iput v0, p0, LX/Ckm;->c:F

    .line 1931302
    if-eqz p1, :cond_2

    const/4 v0, 0x6

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    :goto_2
    double-to-float v0, v0

    iput v0, p0, LX/Ckm;->d:F

    .line 1931303
    if-eqz p1, :cond_3

    const/4 v0, 0x3

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, LX/CkY;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Ckm;->e:I

    .line 1931304
    sget-object v0, LX/Ckk;->StyleNone:LX/Ckk;

    iput-object v0, p0, LX/Ckm;->f:LX/Ckk;

    .line 1931305
    const/high16 v0, -0x1000000

    iput v0, p0, LX/Ckm;->g:I

    .line 1931306
    sget-object v0, LX/Ckl;->UnderlineStyleNone:LX/Ckl;

    iput-object v0, p0, LX/Ckm;->h:LX/Ckl;

    .line 1931307
    return-void

    .line 1931308
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    goto :goto_0

    .line 1931309
    :cond_1
    const/16 v0, 0x9

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    goto :goto_1

    .line 1931310
    :cond_2
    const/16 v0, 0xa

    invoke-virtual {p2, p3, v0}, LX/15i;->l(II)D

    move-result-wide v0

    goto :goto_2

    .line 1931311
    :cond_3
    const/4 v0, 0x7

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(F)F
    .locals 0

    .prologue
    .line 1931295
    return p0
.end method

.method public static a(FLX/Cka;)F
    .locals 1

    .prologue
    .line 1931296
    iget v0, p1, LX/Cka;->b:F

    sub-float v0, p0, v0

    return v0
.end method

.method public static b(Landroid/graphics/RectF;)F
    .locals 1

    .prologue
    .line 1931297
    iget v0, p0, Landroid/graphics/RectF;->top:F

    return v0
.end method
