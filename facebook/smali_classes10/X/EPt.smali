.class public LX/EPt;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;
.implements LX/1a7;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2117953
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EPt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2117954
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2117955
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2117956
    const v0, 0x7f030926

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2117957
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2117958
    iget-boolean v0, p0, LX/EPt;->a:Z

    return v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2117959
    const/4 v0, 0x1

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xe87d05d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117960
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2117961
    const/4 v1, 0x1

    .line 2117962
    iput-boolean v1, p0, LX/EPt;->a:Z

    .line 2117963
    const/16 v1, 0x2d

    const v2, 0x706c4b5c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2f69f2e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117964
    const/4 v1, 0x0

    .line 2117965
    iput-boolean v1, p0, LX/EPt;->a:Z

    .line 2117966
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2117967
    const/16 v1, 0x2d

    const v2, 0x4e0c68d9    # 5.8892038E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
