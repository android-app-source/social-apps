.class public LX/Evx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Etz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bcw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1rs;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2182651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2182652
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2182653
    iput-object v0, p0, LX/Evx;->a:LX/0Ot;

    .line 2182654
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2182655
    iput-object v0, p0, LX/Evx;->b:LX/0Ot;

    .line 2182656
    return-void
.end method

.method public static a(LX/0QB;)LX/Evx;
    .locals 5

    .prologue
    .line 2182657
    const-class v1, LX/Evx;

    monitor-enter v1

    .line 2182658
    :try_start_0
    sget-object v0, LX/Evx;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2182659
    sput-object v2, LX/Evx;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2182660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2182661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2182662
    new-instance v3, LX/Evx;

    invoke-direct {v3}, LX/Evx;-><init>()V

    .line 2182663
    const/16 v4, 0x2228

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1943

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2182664
    iput-object v4, v3, LX/Evx;->a:LX/0Ot;

    iput-object p0, v3, LX/Evx;->b:LX/0Ot;

    .line 2182665
    move-object v0, v3

    .line 2182666
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2182667
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Evx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2182668
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2182669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
