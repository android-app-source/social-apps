.class public LX/DAx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/net/Uri;

.field private static volatile f:LX/DAx;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2c4;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1971638
    sget-object v0, LX/007;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1971639
    sput-object v0, LX/DAx;->a:Ljava/lang/String;

    .line 1971640
    sget-object v0, LX/0ax;->fk:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/DAx;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2c4;LX/0Or;LX/1qt;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;",
            "LX/2c4;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1qt;",
            "LX/0Or",
            "<",
            "LX/1HI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971634
    iput-object p2, p0, LX/DAx;->d:LX/2c4;

    .line 1971635
    iput-object p1, p0, LX/DAx;->c:LX/0Or;

    .line 1971636
    iput-object p5, p0, LX/DAx;->e:LX/0Or;

    .line 1971637
    return-void
.end method

.method public static a(LX/0QB;)LX/DAx;
    .locals 9

    .prologue
    .line 1971620
    sget-object v0, LX/DAx;->f:LX/DAx;

    if-nez v0, :cond_1

    .line 1971621
    const-class v1, LX/DAx;

    monitor-enter v1

    .line 1971622
    :try_start_0
    sget-object v0, LX/DAx;->f:LX/DAx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1971623
    if-eqz v2, :cond_0

    .line 1971624
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1971625
    new-instance v3, LX/DAx;

    const/16 v4, 0x2acf

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v5

    check-cast v5, LX/2c4;

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/1qt;->a(LX/0QB;)LX/1qt;

    move-result-object v7

    check-cast v7, LX/1qt;

    const/16 v8, 0xb99

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/DAx;-><init>(LX/0Or;LX/2c4;LX/0Or;LX/1qt;LX/0Or;)V

    .line 1971626
    move-object v0, v3

    .line 1971627
    sput-object v0, LX/DAx;->f:LX/DAx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1971628
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1971629
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1971630
    :cond_1
    sget-object v0, LX/DAx;->f:LX/DAx;

    return-object v0

    .line 1971631
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1971632
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/res/Resources;LX/246;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1971641
    iget-object v0, p1, LX/246;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1971642
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082ac8

    new-array v1, v4, [Ljava/lang/Object;

    .line 1971643
    iget-object v2, p1, LX/246;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1971644
    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f082ac7

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 1971645
    iget-object v2, p1, LX/246;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1971646
    aput-object v2, v1, v3

    .line 1971647
    iget-object v2, p1, LX/246;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1971648
    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/DAx;LX/BAa;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1971618
    iget-object v0, p0, LX/DAx;->d:LX/2c4;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v4, LX/8D4;->ACTIVITY:LX/8D4;

    new-instance v5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 1971619
    return-void
.end method

.method public static b(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1971613
    if-nez p0, :cond_1

    .line 1971614
    :cond_0
    :goto_0
    return v0

    .line 1971615
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1971616
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 1971617
    if-eqz v1, :cond_2

    sget-object v3, LX/DAx;->b:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    if-eqz v2, :cond_0

    const-string v1, "app_id"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "nonce"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "user_code"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "force_confirmation"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
