.class public final LX/Efb;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/direct/ui/SnacksInboxView;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxView;)V
    .locals 0

    .prologue
    .line 2154894
    iput-object p1, p0, LX/Efb;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 2154889
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, LX/Efb;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    iget v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->i:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 2154890
    iget-object v0, p0, LX/Efb;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->k:LX/Ef5;

    if-eqz v0, :cond_0

    .line 2154891
    iget-object v0, p0, LX/Efb;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView;->k:LX/Ef5;

    invoke-interface {v0}, LX/Ef5;->b()V

    .line 2154892
    :cond_0
    const/4 v0, 0x1

    .line 2154893
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
