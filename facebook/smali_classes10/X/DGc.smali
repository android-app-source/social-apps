.class public final enum LX/DGc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DGc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DGc;

.field public static final enum APP_INSTALL:LX/DGc;

.field public static final enum LOADING:LX/DGc;

.field public static final enum PHOTO:LX/DGc;

.field public static final enum VIDEO:LX/DGc;

.field public static final enum VIDEO_AUTOPLAY:LX/DGc;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1980189
    new-instance v0, LX/DGc;

    const-string v1, "APP_INSTALL"

    invoke-direct {v0, v1, v2}, LX/DGc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DGc;->APP_INSTALL:LX/DGc;

    .line 1980190
    new-instance v0, LX/DGc;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/DGc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DGc;->VIDEO:LX/DGc;

    .line 1980191
    new-instance v0, LX/DGc;

    const-string v1, "VIDEO_AUTOPLAY"

    invoke-direct {v0, v1, v4}, LX/DGc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DGc;->VIDEO_AUTOPLAY:LX/DGc;

    .line 1980192
    new-instance v0, LX/DGc;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, LX/DGc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DGc;->LOADING:LX/DGc;

    .line 1980193
    new-instance v0, LX/DGc;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v6}, LX/DGc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DGc;->PHOTO:LX/DGc;

    .line 1980194
    const/4 v0, 0x5

    new-array v0, v0, [LX/DGc;

    sget-object v1, LX/DGc;->APP_INSTALL:LX/DGc;

    aput-object v1, v0, v2

    sget-object v1, LX/DGc;->VIDEO:LX/DGc;

    aput-object v1, v0, v3

    sget-object v1, LX/DGc;->VIDEO_AUTOPLAY:LX/DGc;

    aput-object v1, v0, v4

    sget-object v1, LX/DGc;->LOADING:LX/DGc;

    aput-object v1, v0, v5

    sget-object v1, LX/DGc;->PHOTO:LX/DGc;

    aput-object v1, v0, v6

    sput-object v0, LX/DGc;->$VALUES:[LX/DGc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1980195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DGc;
    .locals 1

    .prologue
    .line 1980196
    const-class v0, LX/DGc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DGc;

    return-object v0
.end method

.method public static values()[LX/DGc;
    .locals 1

    .prologue
    .line 1980197
    sget-object v0, LX/DGc;->$VALUES:[LX/DGc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DGc;

    return-object v0
.end method
