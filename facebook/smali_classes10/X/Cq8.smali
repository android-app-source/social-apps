.class public final LX/Cq8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/CqA;


# direct methods
.method public constructor <init>(LX/CqA;)V
    .locals 0

    .prologue
    .line 1939136
    iput-object p1, p0, LX/Cq8;->a:LX/CqA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 1939120
    const/4 v4, 0x0

    .line 1939121
    iget-object v3, p0, LX/Cq8;->a:LX/CqA;

    invoke-virtual {v3}, LX/Cod;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v5, 0xfa

    invoke-virtual {v3, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, LX/Cq7;

    invoke-direct {v4, p0}, LX/Cq7;-><init>(LX/Cq8;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1939122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1939123
    iget-object v1, p0, LX/Cq8;->a:LX/CqA;

    iget-object v1, v1, LX/CqA;->aj:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1939124
    iget-object v1, p0, LX/Cq8;->a:LX/CqA;

    iget-object v1, v1, LX/CqA;->aj:Ljava/util/List;

    invoke-static {v1}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v1

    .line 1939125
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 1939126
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1939127
    const-string v1, "ad_images_json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939128
    :cond_0
    iget-object v1, p0, LX/Cq8;->a:LX/CqA;

    iget-object v1, v1, LX/CqA;->ak:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1939129
    iget-object v1, p0, LX/Cq8;->a:LX/CqA;

    iget-object v1, v1, LX/CqA;->ak:Ljava/util/List;

    invoke-static {v1}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v1

    .line 1939130
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 1939131
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1939132
    const-string v1, "ad_videos_json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939133
    :cond_1
    const-string v1, "is_ad_network"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939134
    iget-object v1, p0, LX/Cq8;->a:LX/CqA;

    iget-object v1, v1, LX/CqA;->f:LX/Ckw;

    const-string v2, "android_native_article_webview_ad_report"

    invoke-virtual {v1, v2, v0}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1939135
    const/4 v0, 0x1

    return v0
.end method
