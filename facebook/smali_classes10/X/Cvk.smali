.class public LX/Cvk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Cvk;


# instance fields
.field private final b:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1949219
    const-class v0, LX/Cvk;

    sput-object v0, LX/Cvk;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949221
    iput-object p1, p0, LX/Cvk;->b:LX/0Zb;

    .line 1949222
    return-void
.end method

.method public static a(LX/0QB;)LX/Cvk;
    .locals 4

    .prologue
    .line 1949223
    sget-object v0, LX/Cvk;->c:LX/Cvk;

    if-nez v0, :cond_1

    .line 1949224
    const-class v1, LX/Cvk;

    monitor-enter v1

    .line 1949225
    :try_start_0
    sget-object v0, LX/Cvk;->c:LX/Cvk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1949226
    if-eqz v2, :cond_0

    .line 1949227
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1949228
    new-instance p0, LX/Cvk;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Cvk;-><init>(LX/0Zb;)V

    .line 1949229
    move-object v0, p0

    .line 1949230
    sput-object v0, LX/Cvk;->c:LX/Cvk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1949231
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1949232
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1949233
    :cond_1
    sget-object v0, LX/Cvk;->c:LX/Cvk;

    return-object v0

    .line 1949234
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1949235
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1949236
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "search_unit_data_payload"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "browse"

    .line 1949237
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1949238
    move-object v0, v0

    .line 1949239
    const-string v1, "logging_unit_id"

    invoke-virtual {v0, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1949240
    iget-object v0, p0, LX/Cvk;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1949241
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1949242
    invoke-virtual {p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 1949243
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/Cw0;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1949244
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb4a_rl:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1949245
    invoke-static {v0}, LX/Cvk;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1949246
    iget-object v2, p1, LX/Cw0;->a:LX/Cw2;

    invoke-virtual {v2, v1}, LX/Cw2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949247
    const-string v2, "tapped_result_entity_id"

    iget-object v3, p1, LX/Cw0;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949248
    invoke-direct {p0, v1}, LX/Cvk;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949249
    return-object v0
.end method

.method public final a(LX/Cw2;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1949250
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb4a_ml:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1949251
    invoke-static {v0}, LX/Cvk;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1949252
    invoke-virtual {p1, v1}, LX/Cw2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949253
    invoke-direct {p0, v1}, LX/Cvk;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949254
    return-object v0
.end method

.method public final a(LX/Cw4;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1949255
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb4a_serp:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1949256
    invoke-static {v0}, LX/Cvk;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1949257
    invoke-virtual {p1, v1}, LX/Cw4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949258
    invoke-direct {p0, v1}, LX/Cvk;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949259
    return-object v0
.end method
