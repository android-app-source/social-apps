.class public abstract LX/Dz7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:Ljava/lang/String;

.field public final c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0zw;Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2066465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066466
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Dz7;->d:Landroid/util/SparseArray;

    .line 2066467
    iput-object p1, p0, LX/Dz7;->c:LX/0zw;

    .line 2066468
    iput-object p2, p0, LX/Dz7;->b:Ljava/lang/String;

    .line 2066469
    iput-object p3, p0, LX/Dz7;->a:Landroid/content/res/Resources;

    .line 2066470
    return-void
.end method


# virtual methods
.method public final varargs a(I[Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2066459
    iget-object v0, p0, LX/Dz7;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2066460
    if-nez v0, :cond_0

    .line 2066461
    iget-object v0, p0, LX/Dz7;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2066462
    iget-object v1, p0, LX/Dz7;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2066463
    :cond_0
    return-object v0
.end method

.method public abstract a()V
.end method

.method public abstract a(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract a(Landroid/view/View$OnClickListener;LX/AhV;)Z
.end method

.method public abstract b()Landroid/view/View;
.end method

.method public abstract b(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract c(Landroid/view/View$OnClickListener;)V
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2066464
    iget-object v0, p0, LX/Dz7;->c:LX/0zw;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Dz7;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Dz7;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract d(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract e(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract f(Landroid/view/View$OnClickListener;)V
.end method
