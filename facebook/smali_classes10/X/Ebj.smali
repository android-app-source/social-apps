.class public LX/Ebj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Ecf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2144379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144380
    invoke-static {}, LX/EcK;->x()LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144381
    return-void
.end method

.method public constructor <init>(LX/Ebj;)V
    .locals 1

    .prologue
    .line 2144376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144377
    iget-object v0, p1, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144378
    return-void
.end method

.method public constructor <init>(LX/Ecf;)V
    .locals 0

    .prologue
    .line 2144348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144349
    iput-object p1, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144350
    return-void
.end method

.method public static c(LX/Ebj;LX/Eat;)LX/Ecr;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            ")",
            "LX/Ecr",
            "<",
            "LX/EcW;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2144351
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144352
    iget-object v2, v1, LX/Ecf;->receiverChains_:Ljava/util/List;

    move-object v1, v2

    .line 2144353
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcW;

    .line 2144354
    :try_start_0
    iget-object v2, v0, LX/EcW;->senderRatchetKey_:LX/EWc;

    move-object v2, v2

    .line 2144355
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v2, v4}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v2

    .line 2144356
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2144357
    new-instance v2, LX/Ecr;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v0, v4}, LX/Ecr;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 2144358
    :goto_1
    return-object v0

    .line 2144359
    :catch_0
    move-exception v0

    .line 2144360
    if-nez v0, :cond_2

    .line 2144361
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2144362
    goto :goto_0

    .line 2144363
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object v4, v0

    .line 2144364
    :goto_3
    if-eqz v4, :cond_4

    .line 2144365
    instance-of v2, v4, Ljava/net/UnknownHostException;

    if-eqz v2, :cond_3

    .line 2144366
    goto :goto_2

    .line 2144367
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    goto :goto_3

    .line 2144368
    :cond_4
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 2144369
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 2144370
    invoke-virtual {v0, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 2144371
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 2144372
    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2144382
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EcK;->a(I)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144383
    return-void
.end method

.method public final a(LX/Eae;)V
    .locals 2

    .prologue
    .line 2144384
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {p1}, LX/Eae;->b()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcK;->b(LX/EWc;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144385
    return-void
.end method

.method public final a(LX/Eat;LX/Eba;)V
    .locals 2

    .prologue
    .line 2144386
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    .line 2144387
    iget-object v1, p2, LX/Eba;->d:[B

    move-object v1, v1

    .line 2144388
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcQ;->a(LX/EWc;)LX/EcQ;

    move-result-object v0

    .line 2144389
    iget v1, p2, LX/Eba;->e:I

    move v1, v1

    .line 2144390
    invoke-virtual {v0, v1}, LX/EcQ;->a(I)LX/EcQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EcQ;->l()LX/EcR;

    move-result-object v0

    .line 2144391
    invoke-static {}, LX/EcN;->y()LX/EcN;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EcN;->a(LX/EcR;)LX/EcN;

    move-result-object v0

    invoke-virtual {p1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcN;->a(LX/EWc;)LX/EcN;

    move-result-object v0

    invoke-virtual {v0}, LX/EcN;->l()LX/EcW;

    move-result-object v0

    .line 2144392
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->O()LX/EcK;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EcK;->b(LX/EcW;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144393
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144394
    iget-object v1, v0, LX/Ecf;->receiverChains_:Ljava/util/List;

    move-object v0, v1

    .line 2144395
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 2144396
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    const/4 v1, 0x0

    .line 2144397
    iget-object p1, v0, LX/EcK;->j:LX/EZ2;

    if-nez p1, :cond_1

    .line 2144398
    invoke-static {v0}, LX/EcK;->B(LX/EcK;)V

    .line 2144399
    iget-object p1, v0, LX/EcK;->i:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2144400
    invoke-virtual {v0}, LX/EWj;->t()V

    .line 2144401
    :goto_0
    move-object v0, v0

    .line 2144402
    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144403
    :cond_0
    return-void

    .line 2144404
    :cond_1
    iget-object p1, v0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {p1, v1}, LX/EZ2;->d(I)V

    goto :goto_0
.end method

.method public final a(LX/Eau;LX/Eba;)V
    .locals 3

    .prologue
    .line 2144405
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    .line 2144406
    iget-object v1, p2, LX/Eba;->d:[B

    move-object v1, v1

    .line 2144407
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcQ;->a(LX/EWc;)LX/EcQ;

    move-result-object v0

    .line 2144408
    iget v1, p2, LX/Eba;->e:I

    move v1, v1

    .line 2144409
    invoke-virtual {v0, v1}, LX/EcQ;->a(I)LX/EcQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EcQ;->l()LX/EcR;

    move-result-object v0

    .line 2144410
    invoke-static {}, LX/EcN;->y()LX/EcN;

    move-result-object v1

    .line 2144411
    iget-object v2, p1, LX/Eau;->a:LX/Eat;

    move-object v2, v2

    .line 2144412
    invoke-virtual {v2}, LX/Eat;->a()[B

    move-result-object v2

    invoke-static {v2}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EcN;->a(LX/EWc;)LX/EcN;

    move-result-object v1

    .line 2144413
    iget-object v2, p1, LX/Eau;->b:LX/Eas;

    move-object v2, v2

    .line 2144414
    iget-object p1, v2, LX/Eas;->a:[B

    move-object v2, p1

    .line 2144415
    invoke-static {v2}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EcN;->b(LX/EWc;)LX/EcN;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EcN;->a(LX/EcR;)LX/EcN;

    move-result-object v0

    invoke-virtual {v0}, LX/EcN;->l()LX/EcW;

    move-result-object v0

    .line 2144416
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->O()LX/EcK;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EcK;->a(LX/EcW;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144417
    return-void
.end method

.method public final a(LX/Ebe;)V
    .locals 2

    .prologue
    .line 2144418
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    .line 2144419
    iget-object v1, p1, LX/Ebe;->b:[B

    move-object v1, v1

    .line 2144420
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcK;->c(LX/EWc;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144421
    return-void
.end method

.method public final a([B)V
    .locals 2

    .prologue
    .line 2144422
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-static {p1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcK;->d(LX/EWc;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144423
    return-void
.end method

.method public final a(LX/Eat;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2144424
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v0

    .line 2144425
    iget-object v2, v0, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 2144426
    check-cast v0, LX/EcW;

    .line 2144427
    if-nez v0, :cond_0

    move v0, v1

    .line 2144428
    :goto_0
    return v0

    .line 2144429
    :cond_0
    iget-object v2, v0, LX/EcW;->messageKeys_:Ljava/util/List;

    move-object v0, v2

    .line 2144430
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcV;

    .line 2144431
    iget p0, v0, LX/EcV;->index_:I

    move v0, p0

    .line 2144432
    if-ne v0, p2, :cond_1

    .line 2144433
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2144434
    goto :goto_0
.end method

.method public final b(LX/Eae;)V
    .locals 2

    .prologue
    .line 2144435
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {p1}, LX/Eae;->b()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcK;->a(LX/EWc;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144436
    return-void
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 2144373
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144374
    iget-object p0, v0, LX/Ecf;->aliceBaseKey_:LX/EWc;

    move-object v0, p0

    .line 2144375
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2144277
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144278
    iget p0, v0, LX/Ecf;->sessionVersion_:I

    move v0, p0

    .line 2144279
    if-nez v0, :cond_0

    const/4 v0, 0x2

    .line 2144280
    :cond_0
    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2144281
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EcK;->d(I)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144282
    return-void
.end method

.method public final d()LX/Eae;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2144283
    :try_start_0
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2144284
    :goto_0
    return-object v0

    .line 2144285
    :cond_0
    new-instance v1, LX/Eae;

    iget-object v2, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144286
    iget-object v3, v2, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    move-object v2, v3

    .line 2144287
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/Eae;-><init>([BI)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 2144288
    :catch_0
    move-exception v1

    .line 2144289
    if-nez v1, :cond_1

    .line 2144290
    :goto_1
    goto :goto_0

    :cond_1
    move-object v3, v1

    .line 2144291
    :goto_2
    if-eqz v3, :cond_3

    .line 2144292
    instance-of v2, v3, Ljava/net/UnknownHostException;

    if-eqz v2, :cond_2

    .line 2144293
    goto :goto_1

    .line 2144294
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    goto :goto_2

    .line 2144295
    :cond_3
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 2144296
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 2144297
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 2144298
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 2144299
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 2144300
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EcK;->e(I)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144301
    return-void
.end method

.method public final e()LX/Eae;
    .locals 3

    .prologue
    .line 2144302
    :try_start_0
    new-instance v0, LX/Eae;

    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144303
    iget-object v2, v1, LX/Ecf;->localIdentityPublic_:LX/EWc;

    move-object v1, v2

    .line 2144304
    invoke-virtual {v1}, LX/EWc;->d()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Eae;-><init>([BI)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2144305
    :catch_0
    move-exception v0

    .line 2144306
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2144307
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144308
    iget p0, v0, LX/Ecf;->previousCounter_:I

    move v0, p0

    .line 2144309
    return v0
.end method

.method public final g()LX/Ebe;
    .locals 3

    .prologue
    .line 2144310
    new-instance v0, LX/Ebe;

    invoke-virtual {p0}, LX/Ebj;->c()I

    move-result v1

    invoke-static {v1}, LX/Eb5;->a(I)LX/Eb5;

    move-result-object v1

    iget-object v2, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144311
    iget-object p0, v2, LX/Ecf;->rootKey_:LX/EWc;

    move-object v2, p0

    .line 2144312
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Ebe;-><init>(LX/Eb5;[B)V

    return-object v0
.end method

.method public final h()LX/Eat;
    .locals 2

    .prologue
    .line 2144313
    :try_start_0
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144314
    iget-object v1, v0, LX/Ecf;->senderChain_:LX/EcW;

    move-object v0, v1

    .line 2144315
    iget-object v1, v0, LX/EcW;->senderRatchetKey_:LX/EWc;

    move-object v0, v1

    .line 2144316
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ear;->a([BI)LX/Eat;
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2144317
    :catch_0
    move-exception v0

    .line 2144318
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final k()LX/Eba;
    .locals 4

    .prologue
    .line 2144319
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144320
    iget-object v1, v0, LX/Ecf;->senderChain_:LX/EcW;

    move-object v0, v1

    .line 2144321
    iget-object v1, v0, LX/EcW;->chainKey_:LX/EcR;

    move-object v0, v1

    .line 2144322
    new-instance v1, LX/Eba;

    invoke-virtual {p0}, LX/Ebj;->c()I

    move-result v2

    invoke-static {v2}, LX/Eb5;->a(I)LX/Eb5;

    move-result-object v2

    .line 2144323
    iget-object v3, v0, LX/EcR;->key_:LX/EWc;

    move-object v3, v3

    .line 2144324
    invoke-virtual {v3}, LX/EWc;->d()[B

    move-result-object v3

    .line 2144325
    iget p0, v0, LX/EcR;->index_:I

    move v0, p0

    .line 2144326
    invoke-direct {v1, v2, v3, v0}, LX/Eba;-><init>(LX/Eb5;[BI)V

    return-object v1
.end method

.method public final m()LX/Ebi;
    .locals 5

    .prologue
    .line 2144327
    :try_start_0
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144328
    iget-object v1, v0, LX/Ecf;->pendingPreKey_:LX/Ece;

    move-object v0, v1

    .line 2144329
    invoke-virtual {v0}, LX/Ece;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2144330
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144331
    iget-object v1, v0, LX/Ecf;->pendingPreKey_:LX/Ece;

    move-object v0, v1

    .line 2144332
    iget v1, v0, LX/Ece;->preKeyId_:I

    move v0, v1

    .line 2144333
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/Ecs;->a(Ljava/lang/Object;)LX/Ecs;

    move-result-object v0

    .line 2144334
    :goto_0
    new-instance v1, LX/Ebi;

    iget-object v2, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144335
    iget-object v3, v2, LX/Ecf;->pendingPreKey_:LX/Ece;

    move-object v2, v3

    .line 2144336
    iget v3, v2, LX/Ece;->signedPreKeyId_:I

    move v2, v3

    .line 2144337
    iget-object v3, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144338
    iget-object v4, v3, LX/Ecf;->pendingPreKey_:LX/Ece;

    move-object v3, v4

    .line 2144339
    iget-object v4, v3, LX/Ece;->baseKey_:LX/EWc;

    move-object v3, v4

    .line 2144340
    invoke-virtual {v3}, LX/EWc;->d()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/Ebi;-><init>(LX/Ecs;ILX/Eat;)V

    return-object v1

    .line 2144341
    :cond_0
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    .line 2144342
    goto :goto_0

    .line 2144343
    :catch_0
    move-exception v0

    .line 2144344
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 2144345
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    .line 2144346
    iget p0, v0, LX/Ecf;->localRegistrationId_:I

    move v0, p0

    .line 2144347
    return v0
.end method
