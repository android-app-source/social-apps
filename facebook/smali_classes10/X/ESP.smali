.class public final enum LX/ESP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ESP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ESP;

.field public static final enum INVISIBLE:LX/ESP;

.field public static final enum LOW_BATTERY:LX/ESP;

.field public static final enum NO_INTERNET:LX/ESP;

.field public static final enum REQUIRES_WIFI:LX/ESP;

.field public static final enum SYNC_IS_OFF:LX/ESP;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2122971
    new-instance v0, LX/ESP;

    const-string v1, "SYNC_IS_OFF"

    invoke-direct {v0, v1, v2}, LX/ESP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESP;->SYNC_IS_OFF:LX/ESP;

    new-instance v0, LX/ESP;

    const-string v1, "REQUIRES_WIFI"

    invoke-direct {v0, v1, v3}, LX/ESP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESP;->REQUIRES_WIFI:LX/ESP;

    new-instance v0, LX/ESP;

    const-string v1, "LOW_BATTERY"

    invoke-direct {v0, v1, v4}, LX/ESP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESP;->LOW_BATTERY:LX/ESP;

    new-instance v0, LX/ESP;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v5}, LX/ESP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESP;->NO_INTERNET:LX/ESP;

    new-instance v0, LX/ESP;

    const-string v1, "INVISIBLE"

    invoke-direct {v0, v1, v6}, LX/ESP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESP;->INVISIBLE:LX/ESP;

    .line 2122972
    const/4 v0, 0x5

    new-array v0, v0, [LX/ESP;

    sget-object v1, LX/ESP;->SYNC_IS_OFF:LX/ESP;

    aput-object v1, v0, v2

    sget-object v1, LX/ESP;->REQUIRES_WIFI:LX/ESP;

    aput-object v1, v0, v3

    sget-object v1, LX/ESP;->LOW_BATTERY:LX/ESP;

    aput-object v1, v0, v4

    sget-object v1, LX/ESP;->NO_INTERNET:LX/ESP;

    aput-object v1, v0, v5

    sget-object v1, LX/ESP;->INVISIBLE:LX/ESP;

    aput-object v1, v0, v6

    sput-object v0, LX/ESP;->$VALUES:[LX/ESP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2122973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ESP;
    .locals 1

    .prologue
    .line 2122974
    const-class v0, LX/ESP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ESP;

    return-object v0
.end method

.method public static values()[LX/ESP;
    .locals 1

    .prologue
    .line 2122975
    sget-object v0, LX/ESP;->$VALUES:[LX/ESP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ESP;

    return-object v0
.end method
