.class public LX/D5I;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1963504
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D5I;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1963505
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1963506
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D5I;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963507
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1963508
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963509
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    check-cast p0, LX/D5I;

    invoke-static {p3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p2

    check-cast p2, Lcom/facebook/content/SecureContextHelper;

    const/16 v0, 0x15e7

    invoke-static {p3, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    iput-object p2, p0, LX/D5I;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, LX/D5I;->b:LX/0Or;

    .line 1963510
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 3

    .prologue
    .line 1963511
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1963512
    const-string v0, "Video Channel Feed"

    invoke-virtual {p0, v0}, LX/D5I;->setTitle(Ljava/lang/CharSequence;)V

    .line 1963513
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, LX/D5I;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1963514
    const-string v1, "Live Video Channel"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1963515
    new-instance v1, LX/D5H;

    invoke-direct {v1, p0}, LX/D5H;-><init>(LX/D5I;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1963516
    invoke-virtual {p0, v0}, LX/D5I;->addPreference(Landroid/preference/Preference;)Z

    .line 1963517
    return-void
.end method
