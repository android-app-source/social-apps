.class public final LX/Etr;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ets;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public final synthetic b:LX/Ets;


# direct methods
.method public constructor <init>(LX/Ets;)V
    .locals 1

    .prologue
    .line 2178717
    iput-object p1, p0, LX/Etr;->b:LX/Ets;

    .line 2178718
    move-object v0, p1

    .line 2178719
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2178720
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2178721
    const-string v0, "CancelAddFriendActionButton"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2178722
    if-ne p0, p1, :cond_1

    .line 2178723
    :cond_0
    :goto_0
    return v0

    .line 2178724
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2178725
    goto :goto_0

    .line 2178726
    :cond_3
    check-cast p1, LX/Etr;

    .line 2178727
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2178728
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2178729
    if-eq v2, v3, :cond_0

    .line 2178730
    iget-object v2, p0, LX/Etr;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Etr;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v3, p1, LX/Etr;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2178731
    goto :goto_0

    .line 2178732
    :cond_4
    iget-object v2, p1, LX/Etr;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
