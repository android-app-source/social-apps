.class public final LX/Esv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Cn;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1Ps;

.field public final synthetic e:LX/03V;

.field public final synthetic f:LX/1Nq;

.field public final synthetic g:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

.field public final synthetic h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic i:LX/1Kf;


# direct methods
.method public constructor <init>(LX/1Cn;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Ljava/lang/String;LX/1Ps;LX/03V;LX/1Nq;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Kf;)V
    .locals 0

    .prologue
    .line 2176643
    iput-object p1, p0, LX/Esv;->a:LX/1Cn;

    iput-object p2, p0, LX/Esv;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p3, p0, LX/Esv;->c:Ljava/lang/String;

    iput-object p4, p0, LX/Esv;->d:LX/1Ps;

    iput-object p5, p0, LX/Esv;->e:LX/03V;

    iput-object p6, p0, LX/Esv;->f:LX/1Nq;

    iput-object p7, p0, LX/Esv;->g:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    iput-object p8, p0, LX/Esv;->h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p9, p0, LX/Esv;->i:LX/1Kf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x2

    const v0, -0x8c23f72

    invoke-static {v7, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2176644
    iget-object v0, p0, LX/Esv;->a:LX/1Cn;

    iget-object v2, p0, LX/Esv;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Esv;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2176645
    iget-object v0, p0, LX/Esv;->d:LX/1Ps;

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v0

    .line 2176646
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, LX/2rt;->STATUS:LX/2rt;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    const-string v3, "onThisDayFriendversaryCard"

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/Esv;->f:LX/1Nq;

    const-string v3, "throwback_promotion"

    iget-object v4, p0, LX/Esv;->g:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v4}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, LX/89G;->a()LX/89G;

    move-result-object v2

    iget-object v3, p0, LX/Esv;->h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2176647
    iput-object v3, v2, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2176648
    move-object v2, v2

    .line 2176649
    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/Esv;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-static {v2}, LX/CFE;->a(Lcom/facebook/graphql/model/GraphQLUser;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2176650
    iget-object v3, p0, LX/Esv;->i:LX/1Kf;

    const/4 v4, 0x0

    const/16 v5, 0x6dc

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Landroid/app/Activity;

    invoke-static {v0, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v4, v2, v5, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2176651
    const v0, -0x37bae540    # -201835.0f

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
