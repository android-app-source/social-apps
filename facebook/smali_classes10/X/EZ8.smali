.class public LX/EZ8;
.super Ljava/util/AbstractMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable",
        "<TK;>;V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EZ8",
            "<TK;TV;>.Entry;>;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public d:Z

.field private volatile e:LX/EZF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ8",
            "<TK;TV;>.EntrySet;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2138756
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 2138757
    iput p1, p0, LX/EZ8;->a:I

    .line 2138758
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    .line 2138759
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    .line 2138760
    return-void
.end method

.method private a(Ljava/lang/Comparable;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 2138761
    const/4 v2, 0x0

    .line 2138762
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 2138763
    if-ltz v1, :cond_4

    .line 2138764
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    .line 2138765
    iget-object v3, v0, LX/EZD;->b:Ljava/lang/Comparable;

    move-object v0, v3

    .line 2138766
    invoke-interface {p1, v0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 2138767
    if-lez v0, :cond_0

    .line 2138768
    add-int/lit8 v0, v1, 0x2

    neg-int v0, v0

    .line 2138769
    :goto_0
    return v0

    .line 2138770
    :cond_0
    if-nez v0, :cond_4

    move v0, v1

    .line 2138771
    goto :goto_0

    .line 2138772
    :goto_1
    if-gt v3, v2, :cond_3

    .line 2138773
    add-int v0, v3, v2

    div-int/lit8 v1, v0, 0x2

    .line 2138774
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    .line 2138775
    iget-object v4, v0, LX/EZD;->b:Ljava/lang/Comparable;

    move-object v0, v4

    .line 2138776
    invoke-interface {p1, v0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 2138777
    if-gez v0, :cond_1

    .line 2138778
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    goto :goto_1

    .line 2138779
    :cond_1
    if-lez v0, :cond_2

    .line 2138780
    add-int/lit8 v0, v1, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2138781
    goto :goto_0

    .line 2138782
    :cond_3
    add-int/lit8 v0, v3, 0x1

    neg-int v0, v0

    goto :goto_0

    :cond_4
    move v3, v2

    move v2, v1

    goto :goto_1
.end method

.method public static a(I)LX/EZ8;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<FieldDescriptorType::",
            "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
            "<TFieldDescriptorType;>;>(I)",
            "LX/EZ8",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2138783
    new-instance v0, LX/EZ9;

    invoke-direct {v0, p0}, LX/EZ9;-><init>(I)V

    return-object v0
.end method

.method public static c(LX/EZ8;I)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 2138787
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138788
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    invoke-virtual {v0}, LX/EZD;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 2138789
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2138790
    invoke-direct {p0}, LX/EZ8;->f()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2138791
    iget-object v3, p0, LX/EZ8;->b:Ljava/util/List;

    new-instance v4, LX/EZD;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-direct {v4, p0, v0}, LX/EZD;-><init>(LX/EZ8;Ljava/util/Map$Entry;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138792
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 2138793
    :cond_0
    return-object v1
.end method

.method public static e(LX/EZ8;)V
    .locals 1

    .prologue
    .line 2138784
    iget-boolean v0, p0, LX/EZ8;->d:Z

    if-eqz v0, :cond_0

    .line 2138785
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2138786
    :cond_0
    return-void
.end method

.method private f()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2138812
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138813
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/TreeMap;

    if-nez v0, :cond_0

    .line 2138814
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    .line 2138815
    :cond_0
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 2138794
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138795
    invoke-direct {p0, p1}, LX/EZ8;->a(Ljava/lang/Comparable;)I

    move-result v0

    .line 2138796
    if-ltz v0, :cond_0

    .line 2138797
    iget-object v1, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    invoke-virtual {v0, p2}, LX/EZD;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2138798
    :goto_0
    return-object v0

    .line 2138799
    :cond_0
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138800
    iget-object v1, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EZ8;->b:Ljava/util/List;

    instance-of v1, v1, Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 2138801
    new-instance v1, Ljava/util/ArrayList;

    iget v2, p0, LX/EZ8;->a:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/EZ8;->b:Ljava/util/List;

    .line 2138802
    :cond_1
    add-int/lit8 v0, v0, 0x1

    neg-int v1, v0

    .line 2138803
    iget v0, p0, LX/EZ8;->a:I

    if-lt v1, v0, :cond_2

    .line 2138804
    invoke-direct {p0}, LX/EZ8;->f()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2138805
    :cond_2
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, LX/EZ8;->a:I

    if-ne v0, v2, :cond_3

    .line 2138806
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    iget v2, p0, LX/EZ8;->a:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    .line 2138807
    invoke-direct {p0}, LX/EZ8;->f()Ljava/util/SortedMap;

    move-result-object v2

    .line 2138808
    iget-object v3, v0, LX/EZD;->b:Ljava/lang/Comparable;

    move-object v3, v3

    .line 2138809
    invoke-virtual {v0}, LX/EZD;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138810
    :cond_3
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    new-instance v2, LX/EZD;

    invoke-direct {v2, p0, p1, p2}, LX/EZD;-><init>(LX/EZ8;Ljava/lang/Comparable;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2138811
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 2138750
    iget-boolean v0, p0, LX/EZ8;->d:Z

    if-nez v0, :cond_0

    .line 2138751
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    .line 2138752
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EZ8;->d:Z

    .line 2138753
    :cond_0
    return-void

    .line 2138754
    :cond_1
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(I)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2138755
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2138749
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 2138743
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138744
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2138745
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2138746
    :cond_0
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2138747
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2138748
    :cond_1
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2138741
    check-cast p1, Ljava/lang/Comparable;

    .line 2138742
    invoke-direct {p0, p1}, LX/EZ8;->a(Ljava/lang/Comparable;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 2138738
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138739
    sget-object v0, LX/EZC;->b:Ljava/lang/Iterable;

    move-object v0, v0

    .line 2138740
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 2138735
    iget-object v0, p0, LX/EZ8;->e:LX/EZF;

    if-nez v0, :cond_0

    .line 2138736
    new-instance v0, LX/EZF;

    invoke-direct {v0, p0}, LX/EZF;-><init>(LX/EZ8;)V

    iput-object v0, p0, LX/EZ8;->e:LX/EZF;

    .line 2138737
    :cond_0
    iget-object v0, p0, LX/EZ8;->e:LX/EZF;

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 2138730
    check-cast p1, Ljava/lang/Comparable;

    .line 2138731
    invoke-direct {p0, p1}, LX/EZ8;->a(Ljava/lang/Comparable;)I

    move-result v0

    .line 2138732
    if-ltz v0, :cond_0

    .line 2138733
    iget-object v1, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZD;

    invoke-virtual {v0}, LX/EZD;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2138734
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138729
    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 2138720
    invoke-static {p0}, LX/EZ8;->e(LX/EZ8;)V

    .line 2138721
    check-cast p1, Ljava/lang/Comparable;

    .line 2138722
    invoke-direct {p0, p1}, LX/EZ8;->a(Ljava/lang/Comparable;)I

    move-result v0

    .line 2138723
    if-ltz v0, :cond_0

    .line 2138724
    invoke-static {p0, v0}, LX/EZ8;->c(LX/EZ8;I)Ljava/lang/Object;

    move-result-object v0

    .line 2138725
    :goto_0
    return-object v0

    .line 2138726
    :cond_0
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2138727
    const/4 v0, 0x0

    goto :goto_0

    .line 2138728
    :cond_1
    iget-object v0, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 2138719
    iget-object v0, p0, LX/EZ8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/EZ8;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
