.class public LX/EGJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final phoneAppPayload:LX/EGI;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2097071
    new-instance v0, LX/1sv;

    const-string v1, "WebrtcMessageServerExtension"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/EGJ;->b:LX/1sv;

    .line 2097072
    new-instance v0, LX/1sw;

    const-string v1, "phoneAppPayload"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/EGJ;->c:LX/1sw;

    .line 2097073
    sput-boolean v3, LX/EGJ;->a:Z

    return-void
.end method

.method public constructor <init>(LX/EGI;)V
    .locals 0

    .prologue
    .line 2097074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2097075
    iput-object p1, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    .line 2097076
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2097077
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2097078
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 2097079
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 2097080
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WebrtcMessageServerExtension"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2097081
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097082
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097083
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097084
    iget-object v4, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-eqz v4, :cond_0

    .line 2097085
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097086
    const-string v4, "phoneAppPayload"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097087
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097088
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097089
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-nez v0, :cond_4

    .line 2097090
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097091
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097092
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097093
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2097094
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2097095
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2097096
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 2097097
    :cond_4
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2097098
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2097099
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-eqz v0, :cond_0

    .line 2097100
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-eqz v0, :cond_0

    .line 2097101
    sget-object v0, LX/EGJ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2097102
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    invoke-virtual {v0, p1}, LX/EGI;->a(LX/1su;)V

    .line 2097103
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2097104
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2097105
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2097106
    if-nez p1, :cond_1

    .line 2097107
    :cond_0
    :goto_0
    return v0

    .line 2097108
    :cond_1
    instance-of v1, p1, LX/EGJ;

    if-eqz v1, :cond_0

    .line 2097109
    check-cast p1, LX/EGJ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2097110
    if-nez p1, :cond_3

    .line 2097111
    :cond_2
    :goto_1
    move v0, v2

    .line 2097112
    goto :goto_0

    .line 2097113
    :cond_3
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2097114
    :goto_2
    iget-object v3, p1, LX/EGJ;->phoneAppPayload:LX/EGI;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2097115
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2097116
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2097117
    iget-object v0, p0, LX/EGJ;->phoneAppPayload:LX/EGI;

    iget-object v3, p1, LX/EGJ;->phoneAppPayload:LX/EGI;

    invoke-virtual {v0, v3}, LX/EGI;->a(LX/EGI;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2097118
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2097119
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2097120
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2097121
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2097122
    sget-boolean v0, LX/EGJ;->a:Z

    .line 2097123
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/EGJ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2097124
    return-object v0
.end method
