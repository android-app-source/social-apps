.class public final LX/Ete;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/FriendsCenterHomeFragment;)V
    .locals 0

    .prologue
    .line 2178386
    iput-object p1, p0, LX/Ete;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2178385
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2178363
    check-cast p1, Ljava/lang/Integer;

    .line 2178364
    iget-object v0, p0, LX/Ete;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->s:LX/EtZ;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/EtZ;->c(I)V

    .line 2178365
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    .line 2178366
    iget-object v0, p0, LX/Ete;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x1

    .line 2178367
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->m:LX/0ad;

    sget-short v3, LX/2hr;->E:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2178368
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->p:LX/3kp;

    sget-object v3, LX/1nR;->e:LX/0Tn;

    invoke-virtual {v2, v3}, LX/3kp;->a(LX/0Tn;)V

    .line 2178369
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->p:LX/3kp;

    .line 2178370
    iput v5, v2, LX/3kp;->b:I

    .line 2178371
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->p:LX/3kp;

    invoke-virtual {v2}, LX/3kp;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2178372
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    if-nez v2, :cond_2

    .line 2178373
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    sget-object v3, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v2, v3}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2178374
    if-gez v2, :cond_1

    .line 2178375
    :cond_0
    :goto_0
    return-void

    .line 2178376
    :cond_1
    new-instance v3, LX/0hs;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0hs;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    .line 2178377
    if-ne v1, v5, :cond_3

    .line 2178378
    iget-object v3, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    iget-object v4, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->m:LX/0ad;

    sget-char v5, LX/2hr;->G:C

    iget-object p0, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->n:Landroid/content/res/Resources;

    const p1, 0x7f080f91

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v4, v5, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2178379
    :goto_1
    iget-object v3, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    const/4 v4, -0x1

    .line 2178380
    iput v4, v3, LX/0hs;->t:I

    .line 2178381
    iget-object v3, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    iget-object v4, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->q:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 2178382
    :cond_2
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    invoke-virtual {v2}, LX/0ht;->d()V

    .line 2178383
    iget-object v2, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->p:LX/3kp;

    invoke-virtual {v2}, LX/3kp;->a()V

    goto :goto_0

    .line 2178384
    :cond_3
    iget-object v3, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->r:LX/0hs;

    iget-object v4, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->m:LX/0ad;

    sget-char v5, LX/2hr;->F:C

    iget-object p0, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->n:Landroid/content/res/Resources;

    const p1, 0x7f080f92

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v4, v5, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
