.class public final LX/Dn9;
.super LX/Dn8;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;LX/Dlo;)V
    .locals 0

    .prologue
    .line 2040029
    iput-object p1, p0, LX/Dn9;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-direct {p0, p2, p3}, LX/Dn8;-><init>(Landroid/view/View;LX/Dlo;)V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)LX/Dlm;
    .locals 3

    .prologue
    .line 2040030
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne p0, v0, :cond_0

    .line 2040031
    sget-object v0, LX/Dlm;->ORANGE_BACKGROUND:LX/Dlm;

    .line 2040032
    :goto_0
    return-object v0

    .line 2040033
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne p0, v0, :cond_1

    .line 2040034
    sget-object v0, LX/Dlm;->GREEN_BACKGROUND:LX/Dlm;

    goto :goto_0

    .line 2040035
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid booking status "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 5

    .prologue
    .line 2040036
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    invoke-static {v0}, LX/Dn9;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)LX/Dlm;

    move-result-object v0

    .line 2040037
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->s()Ljava/lang/String;

    move-result-object v1

    .line 2040038
    iget-object v2, p0, LX/Dn8;->q:LX/Dln;

    iget-object v3, p0, LX/Dn9;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082ba2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2040039
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2040040
    iget-object v4, v2, LX/Dln;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040041
    if-nez v3, :cond_0

    .line 2040042
    iget-object v4, v2, LX/Dln;->f:Landroid/widget/TextView;

    const/16 p0, 0x8

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2040043
    :goto_0
    iget-object v4, v2, LX/Dln;->c:Landroid/view/View;

    new-instance p0, Landroid/graphics/drawable/ColorDrawable;

    iget-object p1, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iget v1, v0, LX/Dlm;->backgroundColorResId:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-direct {p0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v4, p0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2040044
    iget-object v4, v2, LX/Dln;->e:Landroid/widget/TextView;

    iget-object p0, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    iget p1, v0, LX/Dlm;->titleColorResId:I

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2040045
    iget-object v4, v2, LX/Dln;->f:Landroid/widget/TextView;

    iget-object p0, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    iget p1, v0, LX/Dlm;->subtitleColorResId:I

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2040046
    iget-object v4, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p0, 0x7f021505

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/LayerDrawable;

    .line 2040047
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2040048
    const p0, 0x7f0d31ef

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 2040049
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2040050
    iget-object p1, v2, LX/Dln;->b:LX/0wM;

    iget-object v1, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, v0, LX/Dlm;->photoTintColorResId:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, p0, v1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 2040051
    const p1, 0x7f0d31ef

    invoke-virtual {v4, p1, p0}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    move-result p0

    .line 2040052
    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 2040053
    const p0, 0x7f0d31f0

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/GradientDrawable;

    .line 2040054
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2040055
    iget-object p1, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f0b1e70

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v1, v2, LX/Dln;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, v0, LX/Dlm;->photoTintColorResId:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, p1, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 2040056
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 2040057
    iget-object p0, v2, LX/Dln;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2040058
    return-void

    .line 2040059
    :cond_0
    iget-object v4, v2, LX/Dln;->f:Landroid/widget/TextView;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2040060
    iget-object v4, v2, LX/Dln;->f:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
