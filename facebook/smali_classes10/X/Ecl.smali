.class public LX/Ecl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoP;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2147958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2147959
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ecl;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(I)LX/Ebg;
    .locals 3

    .prologue
    .line 2147960
    :try_start_0
    iget-object v0, p0, LX/Ecl;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2147961
    new-instance v0, LX/Eah;

    const-string v1, "No such prekeyrecord!"

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2147962
    :catch_0
    move-exception v0

    .line 2147963
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2147964
    :cond_0
    :try_start_1
    new-instance v1, LX/Ebg;

    iget-object v0, p0, LX/Ecl;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, LX/Ebg;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2147965
    iget-object v0, p0, LX/Ecl;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2147966
    return-void
.end method
