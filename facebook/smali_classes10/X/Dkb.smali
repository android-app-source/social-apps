.class public final LX/Dkb;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2034348
    const-class v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    const v0, 0x26b16d0c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "AppointmentDetailQuery"

    const-string v6, "a51cf323e304f90bfce7e48630a3be7b"

    const-string v7, "node"

    const-string v8, "10155141742646729"

    const-string v9, "10155259086291729"

    .line 2034349
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2034350
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2034351
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2034352
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2034353
    sparse-switch v0, :sswitch_data_0

    .line 2034354
    :goto_0
    return-object p1

    .line 2034355
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2034356
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2034357
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2034358
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2034359
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x48c76ed9 -> :sswitch_1
        -0x23113c94 -> :sswitch_4
        0x6be2dc6 -> :sswitch_2
        0xd727dbb -> :sswitch_0
        0x5db720c8 -> :sswitch_3
    .end sparse-switch
.end method
