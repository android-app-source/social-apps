.class public LX/EVg;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128708
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EVg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128709
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128700
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128701
    const v0, 0x7f0315c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128702
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2128707
    iget-boolean v0, p0, LX/EVg;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x538e35b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128703
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2128704
    const/4 v1, 0x1

    .line 2128705
    iput-boolean v1, p0, LX/EVg;->a:Z

    .line 2128706
    const/16 v1, 0x2d

    const v2, 0x5183ec6a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x63a3f334

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128696
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2128697
    const/4 v1, 0x0

    .line 2128698
    iput-boolean v1, p0, LX/EVg;->a:Z

    .line 2128699
    const/16 v1, 0x2d

    const v2, 0x69f8f711

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
