.class public final LX/ER4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2120411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2120412
    new-instance v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-direct {v0, p1}, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2120413
    new-array v0, p1, [Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    return-object v0
.end method
