.class public final LX/DWn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 2008500
    const/4 v8, 0x0

    .line 2008501
    const-wide/16 v6, 0x0

    .line 2008502
    const/4 v5, 0x0

    .line 2008503
    const/4 v4, 0x0

    .line 2008504
    const/4 v3, 0x0

    .line 2008505
    const/4 v2, 0x0

    .line 2008506
    const/4 v1, 0x0

    .line 2008507
    const/4 v0, 0x0

    .line 2008508
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 2008509
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2008510
    const/4 v0, 0x0

    .line 2008511
    :goto_0
    return v0

    .line 2008512
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v5, :cond_8

    .line 2008513
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 2008514
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2008515
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v11, :cond_0

    if-eqz v1, :cond_0

    .line 2008516
    const-string v5, "bylines"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2008517
    invoke-static {p0, p1}, LX/DWT;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 2008518
    :cond_1
    const-string v5, "communicationRank"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2008519
    const/4 v0, 0x1

    .line 2008520
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    goto :goto_1

    .line 2008521
    :cond_2
    const-string v5, "id"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2008522
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 2008523
    :cond_3
    const-string v5, "name"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2008524
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 2008525
    :cond_4
    const-string v5, "name_search_tokens"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2008526
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 2008527
    :cond_5
    const-string v5, "profilePicture50"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2008528
    invoke-static {p0, p1}, LX/DWV;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 2008529
    :cond_6
    const-string v5, "structured_name"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2008530
    invoke-static {p0, p1}, LX/DWS;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 2008531
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2008532
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2008533
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2008534
    if-eqz v0, :cond_9

    .line 2008535
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2008536
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2008537
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2008538
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2008539
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2008540
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2008541
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v4

    move v10, v5

    move v4, v8

    move v8, v3

    move v12, v2

    move-wide v2, v6

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const-wide/16 v2, 0x0

    .line 2008542
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2008543
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008544
    if-eqz v0, :cond_0

    .line 2008545
    const-string v1, "bylines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008546
    invoke-static {p0, v0, p2, p3}, LX/DWT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2008547
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2008548
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 2008549
    const-string v2, "communicationRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008550
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2008551
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2008552
    if-eqz v0, :cond_2

    .line 2008553
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008554
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2008555
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2008556
    if-eqz v0, :cond_3

    .line 2008557
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008558
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2008559
    :cond_3
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2008560
    if-eqz v0, :cond_4

    .line 2008561
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008562
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2008563
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008564
    if-eqz v0, :cond_5

    .line 2008565
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008566
    invoke-static {p0, v0, p2}, LX/DWV;->a(LX/15i;ILX/0nX;)V

    .line 2008567
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2008568
    if-eqz v0, :cond_6

    .line 2008569
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2008570
    invoke-static {p0, v0, p2, p3}, LX/DWS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2008571
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2008572
    return-void
.end method
