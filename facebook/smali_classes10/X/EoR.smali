.class public LX/EoR;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/2eZ;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field public b:Z

.field public c:LX/EoP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2168170
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2168171
    new-instance v0, LX/EoQ;

    invoke-direct {v0, p0}, LX/EoQ;-><init>(LX/EoR;)V

    iput-object v0, p0, LX/EoR;->a:Landroid/view/View$OnClickListener;

    .line 2168172
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EoR;->b:Z

    .line 2168173
    const v0, 0x7f030496

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2168174
    const v0, 0x7f020634

    invoke-virtual {p0, v0}, LX/EoR;->setBackgroundResource(I)V

    .line 2168175
    const v0, 0x7f0d0d90

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2168176
    iget-object v1, p0, LX/EoR;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2168177
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2168178
    iget-boolean v0, p0, LX/EoR;->b:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4f76efd7    # 4.14291328E9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2168179
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2168180
    const/4 v1, 0x1

    .line 2168181
    iput-boolean v1, p0, LX/EoR;->b:Z

    .line 2168182
    const/16 v1, 0x2d

    const v2, 0x5fa383a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x84c16d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2168183
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2168184
    const/4 v1, 0x0

    .line 2168185
    iput-boolean v1, p0, LX/EoR;->b:Z

    .line 2168186
    const/16 v1, 0x2d

    const v2, 0x4142a8f7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public bridge synthetic setPresenter(LX/Emf;)V
    .locals 0

    .prologue
    .line 2168187
    check-cast p1, LX/EoP;

    .line 2168188
    iput-object p1, p0, LX/EoR;->c:LX/EoP;

    .line 2168189
    return-void
.end method
