.class public LX/DoZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile c:LX/DoZ;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2041999
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/3Rv;->a:LX/0U1;

    .line 2042000
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2042001
    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/3Rv;->b:LX/0U1;

    .line 2042002
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2042003
    aput-object v2, v0, v1

    sput-object v0, LX/DoZ;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042005
    iput-object p1, p0, LX/DoZ;->b:LX/0Or;

    .line 2042006
    return-void
.end method

.method public static a(LX/0QB;)LX/DoZ;
    .locals 4

    .prologue
    .line 2042007
    sget-object v0, LX/DoZ;->c:LX/DoZ;

    if-nez v0, :cond_1

    .line 2042008
    const-class v1, LX/DoZ;

    monitor-enter v1

    .line 2042009
    :try_start_0
    sget-object v0, LX/DoZ;->c:LX/DoZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042010
    if-eqz v2, :cond_0

    .line 2042011
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042012
    new-instance v3, LX/DoZ;

    const/16 p0, 0xdc6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DoZ;-><init>(LX/0Or;)V

    .line 2042013
    move-object v0, v3

    .line 2042014
    sput-object v0, LX/DoZ;->c:LX/DoZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042015
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042016
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042017
    :cond_1
    sget-object v0, LX/DoZ;->c:LX/DoZ;

    return-object v0

    .line 2042018
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042019
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)[B
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2042020
    sget-object v0, LX/3Rv;->a:LX/0U1;

    .line 2042021
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2042022
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2042023
    iget-object v0, p0, LX/DoZ;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2042024
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2042025
    const-string v1, "identity_keys"

    sget-object v2, LX/DoZ;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 2042026
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2042027
    sget-object v0, LX/3Rv;->b:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->f(Landroid/database/Cursor;)[B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 2042028
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2042029
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 2042030
    :cond_1
    :goto_0
    return-object v0

    .line 2042031
    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2042032
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    move-object v0, v9

    .line 2042033
    goto :goto_0

    .line 2042034
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2042035
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_7

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 2042036
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2042037
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v9, :cond_8

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 2042038
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2042039
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 2042040
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 2042041
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 2042042
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method
