.class public LX/D6k;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/D6k;


# instance fields
.field private final a:LX/0sV;


# direct methods
.method public constructor <init>(LX/0sV;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1966098
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1966099
    iput-object p1, p0, LX/D6k;->a:LX/0sV;

    .line 1966100
    sget-object v0, LX/0ax;->iz:Ljava/lang/String;

    const-string v1, "{video_channel_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/D6j;

    invoke-direct {v1, p0}, LX/D6j;-><init>(LX/D6k;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1966101
    return-void
.end method

.method public static a(LX/0QB;)LX/D6k;
    .locals 4

    .prologue
    .line 1966102
    sget-object v0, LX/D6k;->b:LX/D6k;

    if-nez v0, :cond_1

    .line 1966103
    const-class v1, LX/D6k;

    monitor-enter v1

    .line 1966104
    :try_start_0
    sget-object v0, LX/D6k;->b:LX/D6k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1966105
    if-eqz v2, :cond_0

    .line 1966106
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1966107
    new-instance p0, LX/D6k;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-direct {p0, v3}, LX/D6k;-><init>(LX/0sV;)V

    .line 1966108
    move-object v0, p0

    .line 1966109
    sput-object v0, LX/D6k;->b:LX/D6k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1966110
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1966111
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1966112
    :cond_1
    sget-object v0, LX/D6k;->b:LX/D6k;

    return-object v0

    .line 1966113
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1966114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1966115
    iget-object v0, p0, LX/D6k;->a:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->a:Z

    return v0
.end method
