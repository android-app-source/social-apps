.class public final LX/Enf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/En3",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/Enq;

.field public final synthetic c:LX/Eni;


# direct methods
.method public constructor <init>(LX/Eni;Lcom/google/common/util/concurrent/SettableFuture;LX/Enq;)V
    .locals 0

    .prologue
    .line 2167343
    iput-object p1, p0, LX/Enf;->c:LX/Eni;

    iput-object p2, p0, LX/Enf;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/Enf;->b:LX/Enq;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2167344
    iget-object v0, p0, LX/Enf;->c:LX/Eni;

    iget-object v0, v0, LX/Eni;->d:LX/03V;

    const-string v1, "people_entity_cards_page_ids"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2167345
    iget-object v0, p0, LX/Enf;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2167346
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2167347
    check-cast p1, LX/En3;

    .line 2167348
    invoke-virtual {p1}, LX/En2;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167349
    iget-object v0, p0, LX/Enf;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 p0, 0x0

    .line 2167350
    new-instance v1, LX/Enp;

    sget-object v2, LX/En3;->a:LX/En3;

    .line 2167351
    sget-object v3, LX/0Rg;->a:LX/0Rg;

    move-object v3, v3

    .line 2167352
    invoke-direct {v1, v2, v3, p0, p0}, LX/Enp;-><init>(LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v1

    .line 2167353
    const v2, -0x7a45eddc

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2167354
    :goto_0
    return-void

    .line 2167355
    :cond_0
    iget-object v0, p0, LX/Enf;->c:LX/Eni;

    iget-object v0, v0, LX/Eni;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/Enf;->b:LX/Enq;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167356
    iget-object v0, p0, LX/Enf;->c:LX/Eni;

    iget-object v0, v0, LX/Eni;->b:LX/Eoc;

    invoke-virtual {p1}, LX/En2;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Eoc;->a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2167357
    iget-object v1, p0, LX/Enf;->c:LX/Eni;

    iget-object v2, p0, LX/Enf;->b:LX/Enq;

    iget-object v3, p0, LX/Enf;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2167358
    new-instance v4, LX/Eng;

    invoke-direct {v4, v1, v2, v3, p1}, LX/Eng;-><init>(LX/Eni;LX/Enq;Lcom/google/common/util/concurrent/SettableFuture;LX/En3;)V

    move-object v1, v4

    .line 2167359
    iget-object v2, p0, LX/Enf;->c:LX/Eni;

    iget-object v2, v2, LX/Eni;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
