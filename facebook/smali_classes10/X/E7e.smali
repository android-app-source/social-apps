.class public final LX/E7e;
.super LX/E7V;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E7V",
        "<",
        "LX/5kF;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

.field private m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private o:Lcom/facebook/resources/ui/FbTextView;

.field private p:Lcom/facebook/resources/ui/FbTextView;

.field private q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 2081880
    iput-object p1, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    .line 2081881
    invoke-direct {p0, p2}, LX/E7V;-><init>(Landroid/view/View;)V

    .line 2081882
    iput-object p3, p0, LX/E7e;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081883
    iput-object p4, p0, LX/E7e;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2081884
    iput-object p5, p0, LX/E7e;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2081885
    iput-object p6, p0, LX/E7e;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2081886
    iput-object p7, p0, LX/E7e;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081887
    return-void
.end method


# virtual methods
.method public final a(LX/1U8;)V
    .locals 9

    .prologue
    .line 2081888
    check-cast p1, LX/5kF;

    const/4 v8, 0x0

    .line 2081889
    invoke-interface {p1}, LX/5kE;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2081890
    iget-object v1, p0, LX/E7e;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081891
    iget-object v0, p0, LX/E7e;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->e:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->f:Ljava/lang/String;

    iget-object v3, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    iget-object v3, v3, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(Ljava/lang/String;Ljava/lang/String;LX/1U8;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2081892
    iget-object v0, p0, LX/E7e;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v8}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2081893
    iget-object v0, p0, LX/E7e;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2081894
    iget-object v0, p0, LX/E7e;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p1}, LX/5kE;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081895
    iget-object v0, p0, LX/E7e;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->d:LX/11S;

    sget-object v2, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {p1}, LX/5kE;->k()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E7e;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->c:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081896
    invoke-interface {p1}, LX/5kF;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v0

    .line 2081897
    iget-object v2, p0, LX/E7e;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081898
    return-void
.end method
