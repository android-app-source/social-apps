.class public final enum LX/Cmu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cmu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cmu;

.field public static final enum HAM_VALUE:LX/Cmu;

.field public static final enum PIXEL:LX/Cmu;

.field public static final enum UNSET:LX/Cmu;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1933615
    new-instance v0, LX/Cmu;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/Cmu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cmu;->UNSET:LX/Cmu;

    .line 1933616
    new-instance v0, LX/Cmu;

    const-string v1, "PIXEL"

    invoke-direct {v0, v1, v3}, LX/Cmu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cmu;->PIXEL:LX/Cmu;

    .line 1933617
    new-instance v0, LX/Cmu;

    const-string v1, "HAM_VALUE"

    invoke-direct {v0, v1, v4}, LX/Cmu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cmu;->HAM_VALUE:LX/Cmu;

    .line 1933618
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cmu;

    sget-object v1, LX/Cmu;->UNSET:LX/Cmu;

    aput-object v1, v0, v2

    sget-object v1, LX/Cmu;->PIXEL:LX/Cmu;

    aput-object v1, v0, v3

    sget-object v1, LX/Cmu;->HAM_VALUE:LX/Cmu;

    aput-object v1, v0, v4

    sput-object v0, LX/Cmu;->$VALUES:[LX/Cmu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1933619
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cmu;
    .locals 1

    .prologue
    .line 1933620
    const-class v0, LX/Cmu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cmu;

    return-object v0
.end method

.method public static values()[LX/Cmu;
    .locals 1

    .prologue
    .line 1933621
    sget-object v0, LX/Cmu;->$VALUES:[LX/Cmu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cmu;

    return-object v0
.end method
