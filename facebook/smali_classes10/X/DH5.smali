.class public final LX/DH5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BUv;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:LX/DH6;


# direct methods
.method public constructor <init>(LX/DH6;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1981264
    iput-object p1, p0, LX/DH5;->d:LX/DH6;

    iput-object p2, p0, LX/DH5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/DH5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object p4, p0, LX/DH5;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1981265
    iget-object v0, p0, LX/DH5;->d:LX/DH6;

    iget-object v6, v0, LX/DH6;->b:LX/0bH;

    new-instance v0, LX/1Za;

    iget-object v1, p0, LX/DH5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/DH5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DH5;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/DH5;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string v4, "feed_story"

    invoke-direct/range {v0 .. v5}, LX/1Za;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1981266
    iget-object v0, p0, LX/DH5;->d:LX/DH6;

    iget-object v1, v0, LX/DH6;->d:LX/6W7;

    iget-object v0, p0, LX/DH5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981267
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1981268
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    const-string v2, "spc"

    invoke-virtual {v1, v0, v2}, LX/6W7;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Ljava/lang/String;)V

    .line 1981269
    iget-object v0, p0, LX/DH5;->d:LX/DH6;

    iget-object v1, v0, LX/DH6;->e:LX/2yK;

    sget-object v2, LX/0ig;->A:LX/0ih;

    iget-object v0, p0, LX/DH5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981270
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1981271
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/16 v3, 0x9

    invoke-virtual {v1, v2, v0, v3}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;I)V

    .line 1981272
    return-void

    :cond_0
    move-object v3, v5

    .line 1981273
    goto :goto_0
.end method
