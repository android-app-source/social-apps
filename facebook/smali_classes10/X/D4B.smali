.class public final LX/D4B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/feed/autoplay/VideoDisplayedCoordinator$VideoVisibilityChangedListener",
        "<",
        "LX/D5z;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D4G;


# direct methods
.method public constructor <init>(LX/D4G;)V
    .locals 0

    .prologue
    .line 1961890
    iput-object p1, p0, LX/D4B;->a:LX/D4G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1961891
    check-cast p1, LX/D5z;

    .line 1961892
    if-nez p1, :cond_0

    .line 1961893
    :goto_0
    return-void

    .line 1961894
    :cond_0
    iget-object v1, p0, LX/D4B;->a:LX/D4G;

    monitor-enter v1

    .line 1961895
    :try_start_0
    iget-object v0, p0, LX/D4B;->a:LX/D4G;

    iget-object v0, v0, LX/D4G;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1961896
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1961897
    check-cast p1, LX/D5z;

    .line 1961898
    if-nez p1, :cond_0

    .line 1961899
    :goto_0
    return-void

    .line 1961900
    :cond_0
    iget-object v1, p0, LX/D4B;->a:LX/D4G;

    monitor-enter v1

    .line 1961901
    :try_start_0
    iget-object v0, p0, LX/D4B;->a:LX/D4G;

    iget-object v0, v0, LX/D4G;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1961902
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
