.class public final LX/EFB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/view/ViewGroup;

.field public final synthetic c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2094921
    iput-object p1, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    iput p2, p0, LX/EFB;->a:I

    iput-object p3, p0, LX/EFB;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    const v0, -0x6abc6576

    invoke-static {v1, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 2094922
    iget-object v0, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2094923
    const v0, -0x283ad468

    invoke-static {v1, v1, v0, v5}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2094924
    :goto_0
    return-void

    .line 2094925
    :cond_0
    iget-object v0, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    const-wide/32 v6, 0x15f90

    invoke-interface {v0, v6, v7}, LX/ECA;->a(J)V

    .line 2094926
    iget-object v0, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->l()V

    .line 2094927
    iget v0, p0, LX/EFB;->a:I

    add-int/lit8 v6, v0, 0x1

    move v1, v2

    .line 2094928
    :goto_1
    iget-object v0, p0, LX/EFB;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2094929
    iget-object v0, p0, LX/EFB;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 2094930
    iget v3, p0, LX/EFB;->a:I

    if-gt v1, v3, :cond_1

    move v3, v4

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2094931
    iget-object v0, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    .line 2094932
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    if-nez v3, :cond_3

    .line 2094933
    :goto_3
    iget-object v0, p0, LX/EFB;->c:Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    .line 2094934
    iput v6, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->t:I

    .line 2094935
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v3, v2

    .line 2094936
    goto :goto_2

    .line 2094937
    :cond_2
    const v0, -0x65d993f1

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto :goto_0

    .line 2094938
    :cond_3
    packed-switch v6, :pswitch_data_0

    .line 2094939
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const-string v7, "____"

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2094940
    :pswitch_0
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v7, 0x7f080775

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2094941
    :pswitch_1
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v7, 0x7f080776

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2094942
    :pswitch_2
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v7, 0x7f080777

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2094943
    :pswitch_3
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v7, 0x7f080778

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2094944
    :pswitch_4
    iget-object v3, v0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    const v7, 0x7f080779

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
