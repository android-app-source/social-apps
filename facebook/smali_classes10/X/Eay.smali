.class public LX/Eay;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/Eap;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/Eap;)V
    .locals 0

    .prologue
    .line 2142816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142817
    iput-object p1, p0, LX/Eay;->a:Ljava/lang/String;

    .line 2142818
    iput-object p2, p0, LX/Eay;->b:LX/Eap;

    .line 2142819
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2142820
    if-nez p1, :cond_1

    .line 2142821
    :cond_0
    :goto_0
    return v0

    .line 2142822
    :cond_1
    instance-of v1, p1, LX/Eay;

    if-eqz v1, :cond_0

    .line 2142823
    check-cast p1, LX/Eay;

    .line 2142824
    iget-object v1, p0, LX/Eay;->a:Ljava/lang/String;

    iget-object v2, p1, LX/Eay;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Eay;->b:LX/Eap;

    iget-object v2, p1, LX/Eay;->b:LX/Eap;

    invoke-virtual {v1, v2}, LX/Eap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2142825
    iget-object v0, p0, LX/Eay;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/Eay;->b:LX/Eap;

    invoke-virtual {v1}, LX/Eap;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
