.class public LX/ECa;
.super LX/ECT;
.source ""


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field public h:LX/3RT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/EG8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2090248
    const-class v0, LX/ECa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ECa;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/ECY;JJJ)V
    .locals 10

    .prologue
    .line 2090245
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    invoke-direct/range {v0 .. v8}, LX/ECT;-><init>(LX/ECY;JJJLjava/lang/String;)V

    .line 2090246
    const-class v0, LX/ECa;

    invoke-virtual {p1}, LX/ECY;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/ECa;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2090247
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/ECa;

    invoke-static {p0}, LX/EBW;->a(LX/0QB;)LX/3RT;

    move-result-object v0

    check-cast v0, LX/3RT;

    invoke-static {p0}, LX/EBY;->a(LX/0QB;)LX/EG8;

    move-result-object p0

    check-cast p0, LX/EG8;

    iput-object v0, p1, LX/ECa;->h:LX/3RT;

    iput-object p0, p1, LX/ECa;->i:LX/EG8;

    return-void
.end method


# virtual methods
.method public final f()Z
    .locals 1

    .prologue
    .line 2090244
    iget-object v0, p0, LX/ECT;->a:LX/ECY;

    invoke-virtual {v0}, LX/ECY;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ECT;->a:LX/ECY;

    invoke-virtual {v0}, LX/ECY;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2090240
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2090242
    iget-object v0, p0, LX/ECa;->h:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->f()Z

    .line 2090243
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2090241
    const/4 v0, 0x1

    return v0
.end method
