.class public final LX/DDd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/DDe;

.field public final synthetic c:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/DDe;)V
    .locals 0

    .prologue
    .line 1975874
    iput-object p1, p0, LX/DDd;->c:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    iput-object p2, p0, LX/DDd;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DDd;->b:LX/DDe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2d82cb72

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1975875
    iget-object v1, p0, LX/DDd;->c:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/DDd;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 1975876
    iget-object v2, p0, LX/DDd;->c:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/DDd;->c:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->f:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1975877
    iget-object v1, p0, LX/DDd;->b:LX/DDe;

    iget-object v1, v1, LX/DDe;->b:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    .line 1975878
    iget-object v1, p0, LX/DDd;->b:LX/DDe;

    iget-object v1, v1, LX/DDe;->b:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1975879
    :cond_0
    const v1, -0x75cd2baa

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
