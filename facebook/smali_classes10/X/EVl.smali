.class public final LX/EVl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/EVo;


# direct methods
.method public constructor <init>(LX/EVo;)V
    .locals 0

    .prologue
    .line 2128935
    iput-object p1, p0, LX/EVl;->a:LX/EVo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 2128936
    iget-object v0, p0, LX/EVl;->a:LX/EVo;

    .line 2128937
    iget-object v1, v0, LX/EVo;->e:Landroid/view/View;

    iget-object v2, v0, LX/EVo;->d:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2128938
    iget-object v1, v0, LX/EVo;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 2128939
    iget-object v2, v0, LX/EVo;->d:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 2128940
    iget-object v3, v0, LX/EVo;->c:Landroid/graphics/Rect;

    iget-object v4, v0, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, v0, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 2128941
    iget-object v1, v0, LX/EVo;->c:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    move v1, v1

    .line 2128942
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2128943
    :goto_0
    const/4 v1, 0x1

    move v0, v1

    .line 2128944
    return v0

    .line 2128945
    :pswitch_0
    if-eqz v1, :cond_0

    .line 2128946
    invoke-static {v0}, LX/EVo;->i(LX/EVo;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2128947
    :goto_1
    iget-object v2, v0, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 2128948
    :cond_0
    invoke-static {v0}, LX/EVo;->e(LX/EVo;)V

    goto :goto_1

    .line 2128949
    :pswitch_1
    if-eqz v1, :cond_1

    .line 2128950
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 2128951
    :cond_1
    :pswitch_2
    invoke-static {v0}, LX/EVo;->e(LX/EVo;)V

    .line 2128952
    iget-object v1, v0, LX/EVo;->e:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 2128953
    :cond_2
    iget-object v3, v0, LX/EVo;->f:LX/0wd;

    const-wide v5, 0x3fee666660000000L    # 0.949999988079071

    invoke-virtual {v3, v5, v6}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
