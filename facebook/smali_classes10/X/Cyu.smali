.class public LX/Cyu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cyq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Cyq",
        "<",
        "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;",
        "LX/0Px",
        "<",
        "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Cyu;


# instance fields
.field private final a:LX/0Sh;

.field public final b:LX/0tX;

.field public final c:LX/2Sc;

.field public d:LX/Cwy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cwy",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:LX/Cx0;


# direct methods
.method public constructor <init>(LX/0Sh;LX/0tX;LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953905
    iput-object p1, p0, LX/Cyu;->a:LX/0Sh;

    .line 1953906
    iput-object p2, p0, LX/Cyu;->b:LX/0tX;

    .line 1953907
    iput-object p3, p0, LX/Cyu;->c:LX/2Sc;

    .line 1953908
    return-void
.end method

.method public static a(LX/0QB;)LX/Cyu;
    .locals 6

    .prologue
    .line 1953875
    sget-object v0, LX/Cyu;->f:LX/Cyu;

    if-nez v0, :cond_1

    .line 1953876
    const-class v1, LX/Cyu;

    monitor-enter v1

    .line 1953877
    :try_start_0
    sget-object v0, LX/Cyu;->f:LX/Cyu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1953878
    if-eqz v2, :cond_0

    .line 1953879
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1953880
    new-instance p0, LX/Cyu;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v5

    check-cast v5, LX/2Sc;

    invoke-direct {p0, v3, v4, v5}, LX/Cyu;-><init>(LX/0Sh;LX/0tX;LX/2Sc;)V

    .line 1953881
    move-object v0, p0

    .line 1953882
    sput-object v0, LX/Cyu;->f:LX/Cyu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953883
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1953884
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1953885
    :cond_1
    sget-object v0, LX/Cyu;->f:LX/Cyu;

    return-object v0

    .line 1953886
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1953887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953901
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1953902
    :cond_0
    const/4 v0, 0x0

    .line 1953903
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b$redex0(LX/Cyu;)V
    .locals 1

    .prologue
    .line 1953898
    iget-object v0, p0, LX/Cyu;->e:LX/Cx0;

    if-eqz v0, :cond_0

    .line 1953899
    iget-object v0, p0, LX/Cyu;->e:LX/Cx0;

    invoke-interface {v0}, LX/Cx0;->a()V

    .line 1953900
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1953892
    iget-object v0, p0, LX/Cyu;->a:LX/0Sh;

    .line 1953893
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x3c

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 1953894
    iget-object v4, p0, LX/Cyu;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v1, v3

    .line 1953895
    new-instance v2, LX/Cyt;

    invoke-direct {v2, p0}, LX/Cyt;-><init>(LX/Cyu;)V

    move-object v2, v2

    .line 1953896
    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1953897
    return-void
.end method

.method public final a(LX/Cwy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cwy",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1953890
    iput-object p1, p0, LX/Cyu;->d:LX/Cwy;

    .line 1953891
    return-void
.end method

.method public final a(LX/Cx0;)V
    .locals 0

    .prologue
    .line 1953888
    iput-object p1, p0, LX/Cyu;->e:LX/Cx0;

    .line 1953889
    return-void
.end method
