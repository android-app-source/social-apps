.class public final LX/Ehd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkpoint/api/CheckpointMutationModels$CheckpointCancelMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1ih;


# direct methods
.method public constructor <init>(LX/1ih;)V
    .locals 0

    .prologue
    .line 2158601
    iput-object p1, p0, LX/Ehd;->a:LX/1ih;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2158602
    iget-object v0, p0, LX/Ehd;->a:LX/1ih;

    invoke-virtual {v0}, LX/1ih;->b()V

    .line 2158603
    sget-object v0, LX/1ih;->a:Ljava/lang/Class;

    const-string v1, "Clear user from non-blocking checkpoint failed"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2158604
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2158605
    iget-object v0, p0, LX/Ehd;->a:LX/1ih;

    invoke-virtual {v0}, LX/1ih;->b()V

    .line 2158606
    return-void
.end method
