.class public final LX/E6b;
.super LX/0gG;
.source ""


# instance fields
.field public final synthetic a:LX/E6d;

.field public b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionStoryAttachmentFragment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/E6d;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2080376
    iput-object p1, p0, LX/E6b;->a:LX/E6d;

    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2080377
    invoke-static {p0, p2}, LX/E6b;->b(LX/E6b;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)V

    .line 2080378
    iput-object p3, p0, LX/E6b;->g:Ljava/lang/String;

    .line 2080379
    iput-object p4, p0, LX/E6b;->e:Ljava/lang/String;

    .line 2080380
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->c()LX/0us;

    move-result-object v0

    invoke-static {p0, v0}, LX/E6b;->a(LX/E6b;LX/0us;)V

    .line 2080381
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2080382
    return-void
.end method

.method public static a(LX/E6b;LX/0us;)V
    .locals 1

    .prologue
    .line 2080383
    if-eqz p1, :cond_0

    .line 2080384
    invoke-interface {p1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E6b;->f:Ljava/lang/String;

    .line 2080385
    invoke-interface {p1}, LX/0us;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/E6b;->d:Z

    .line 2080386
    :goto_0
    return-void

    .line 2080387
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E6b;->d:Z

    goto :goto_0
.end method

.method private static a(Landroid/view/View;ILandroid/view/View$OnClickListener;)Z
    .locals 1

    .prologue
    .line 2080353
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2080354
    if-eqz v0, :cond_0

    .line 2080355
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080356
    const/4 v0, 0x1

    .line 2080357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/E6b;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)V
    .locals 6

    .prologue
    .line 2080388
    iput-object p1, p0, LX/E6b;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 2080389
    iget-object v0, p0, LX/E6b;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2080390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E6b;->c:Ljava/util/List;

    .line 2080391
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2080392
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/E6b;->a:LX/E6d;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    .line 2080393
    invoke-virtual {v4, v5}, LX/Cfk;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result p1

    move v4, p1

    .line 2080394
    if-eqz v4, :cond_0

    .line 2080395
    iget-object v4, p0, LX/E6b;->c:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2080396
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2080397
    :cond_1
    iget-object v0, p0, LX/E6b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 2080398
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2080358
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2080359
    iget-object v0, p0, LX/E6b;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 2080360
    iget-object v1, p0, LX/E6b;->a:LX/E6d;

    .line 2080361
    invoke-virtual {v1, v0}, LX/Cfk;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result v2

    move v1, v2

    .line 2080362
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2080363
    iget-object v1, p0, LX/E6b;->a:LX/E6d;

    .line 2080364
    invoke-virtual {v1, v0}, LX/Cfk;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;

    move-result-object v2

    move-object v1, v2

    .line 2080365
    if-eqz v1, :cond_0

    .line 2080366
    iget-object v2, p0, LX/E6b;->a:LX/E6d;

    iget-object v3, p0, LX/E6b;->g:Ljava/lang/String;

    iget-object v4, p0, LX/E6b;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2080367
    const v2, 0x7f0d2876

    invoke-static {v1, v2, v0}, LX/E6b;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2080368
    const v2, 0x7f0d28f0

    invoke-static {v1, v2, v0}, LX/E6b;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)Z

    .line 2080369
    const v2, 0x7f0d28ee

    invoke-static {v1, v2, v0}, LX/E6b;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)Z

    .line 2080370
    const v2, 0x7f0d28ef

    invoke-static {v1, v2, v0}, LX/E6b;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)Z

    .line 2080371
    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080372
    iget-object v0, p0, LX/E6b;->a:LX/E6d;

    iget-object v2, p0, LX/E6b;->a:LX/E6d;

    iget-object v2, v2, LX/E6d;->a:Ljava/lang/String;

    add-int/lit8 v3, p2, 0x1

    .line 2080373
    invoke-virtual {v0, v2, v3}, LX/Cfk;->a(Ljava/lang/String;I)V

    .line 2080374
    :cond_0
    return-object v1

    .line 2080375
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2080349
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2080350
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2080348
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2080351
    iget-object v0, p0, LX/E6b;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E6b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)F
    .locals 1

    .prologue
    .line 2080352
    iget-object v0, p0, LX/E6b;->a:LX/E6d;

    invoke-virtual {v0}, LX/E6d;->i()F

    move-result v0

    return v0
.end method
