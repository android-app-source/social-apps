.class public final LX/EWr;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EWk;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWr;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EWr;


# instance fields
.field public bitField0_:I

.field public enumType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWv;",
            ">;"
        }
    .end annotation
.end field

.field public extensionRange_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWq;",
            ">;"
        }
    .end annotation
.end field

.field public extension_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field public field_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/lang/Object;

.field public nestedType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWr;",
            ">;"
        }
    .end annotation
.end field

.field public options_:LX/EXf;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2131739
    new-instance v0, LX/EWi;

    invoke-direct {v0}, LX/EWi;-><init>()V

    sput-object v0, LX/EWr;->a:LX/EWZ;

    .line 2131740
    new-instance v0, LX/EWr;

    invoke-direct {v0}, LX/EWr;-><init>()V

    .line 2131741
    sput-object v0, LX/EWr;->c:LX/EWr;

    invoke-direct {v0}, LX/EWr;->y()V

    .line 2131742
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2131734
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131735
    iput-byte v0, p0, LX/EWr;->memoizedIsInitialized:B

    .line 2131736
    iput v0, p0, LX/EWr;->memoizedSerializedSize:I

    .line 2131737
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2131738
    iput-object v0, p0, LX/EWr;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 10

    .prologue
    const/16 v9, 0x20

    const/16 v8, 0x10

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    .line 2131656
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131657
    const/4 v0, -0x1

    iput-byte v0, p0, LX/EWr;->memoizedIsInitialized:B

    .line 2131658
    const/4 v0, -0x1

    iput v0, p0, LX/EWr;->memoizedSerializedSize:I

    .line 2131659
    invoke-direct {p0}, LX/EWr;->y()V

    .line 2131660
    const/4 v1, 0x0

    .line 2131661
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v4

    .line 2131662
    const/4 v0, 0x0

    move v3, v0

    .line 2131663
    :cond_0
    :goto_0
    if-nez v3, :cond_c

    .line 2131664
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2131665
    sparse-switch v0, :sswitch_data_0

    .line 2131666
    invoke-virtual {p0, p1, v4, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2131667
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 2131668
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 2131669
    goto :goto_0

    .line 2131670
    :sswitch_1
    iget v0, p0, LX/EWr;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EWr;->bitField0_:I

    .line 2131671
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2131672
    :catch_0
    move-exception v0

    .line 2131673
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131674
    move-object v0, v0

    .line 2131675
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2131676
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v5, :cond_1

    .line 2131677
    iget-object v2, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EWr;->field_:Ljava/util/List;

    .line 2131678
    :cond_1
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v7, :cond_2

    .line 2131679
    iget-object v2, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131680
    :cond_2
    and-int/lit8 v2, v1, 0x10

    if-ne v2, v8, :cond_3

    .line 2131681
    iget-object v2, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131682
    :cond_3
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v9, :cond_4

    .line 2131683
    iget-object v2, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131684
    :cond_4
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_5

    .line 2131685
    iget-object v1, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWr;->extension_:Ljava/util/List;

    .line 2131686
    :cond_5
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EWr;->unknownFields:LX/EZQ;

    .line 2131687
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2131688
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v5, :cond_6

    .line 2131689
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    .line 2131690
    or-int/lit8 v1, v1, 0x2

    .line 2131691
    :cond_6
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    sget-object v2, LX/EXL;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2131692
    :catch_1
    move-exception v0

    .line 2131693
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2131694
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131695
    move-object v0, v2

    .line 2131696
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2131697
    :sswitch_3
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_7

    .line 2131698
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131699
    or-int/lit8 v1, v1, 0x8

    .line 2131700
    :cond_7
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    sget-object v2, LX/EWr;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2131701
    :sswitch_4
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v8, :cond_8

    .line 2131702
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131703
    or-int/lit8 v1, v1, 0x10

    .line 2131704
    :cond_8
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    sget-object v2, LX/EWv;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2131705
    :sswitch_5
    and-int/lit8 v0, v1, 0x20

    if-eq v0, v9, :cond_9

    .line 2131706
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131707
    or-int/lit8 v1, v1, 0x20

    .line 2131708
    :cond_9
    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    sget-object v2, LX/EWq;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2131709
    :sswitch_6
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v6, :cond_a

    .line 2131710
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    .line 2131711
    or-int/lit8 v1, v1, 0x4

    .line 2131712
    :cond_a
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    sget-object v2, LX/EXL;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2131713
    :sswitch_7
    const/4 v0, 0x0

    .line 2131714
    iget v2, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_12

    .line 2131715
    iget-object v0, p0, LX/EWr;->options_:LX/EXf;

    invoke-virtual {v0}, LX/EXf;->n()LX/EXe;

    move-result-object v0

    move-object v2, v0

    .line 2131716
    :goto_1
    sget-object v0, LX/EXf;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXf;

    iput-object v0, p0, LX/EWr;->options_:LX/EXf;

    .line 2131717
    if-eqz v2, :cond_b

    .line 2131718
    iget-object v0, p0, LX/EWr;->options_:LX/EXf;

    invoke-virtual {v2, v0}, LX/EXe;->a(LX/EXf;)LX/EXe;

    .line 2131719
    invoke-virtual {v2}, LX/EXe;->l()LX/EXf;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->options_:LX/EXf;

    .line 2131720
    :cond_b
    iget v0, p0, LX/EWr;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EWr;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2131721
    :cond_c
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v5, :cond_d

    .line 2131722
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    .line 2131723
    :cond_d
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_e

    .line 2131724
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131725
    :cond_e
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v8, :cond_f

    .line 2131726
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131727
    :cond_f
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v9, :cond_10

    .line 2131728
    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131729
    :cond_10
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v6, :cond_11

    .line 2131730
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    .line 2131731
    :cond_11
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->unknownFields:LX/EZQ;

    .line 2131732
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2131733
    return-void

    :cond_12
    move-object v2, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2131651
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2131652
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    .line 2131653
    iput v1, p0, LX/EWr;->memoizedSerializedSize:I

    .line 2131654
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->unknownFields:LX/EZQ;

    .line 2131655
    return-void
.end method

.method private static g(LX/EWr;)LX/EWl;
    .locals 1

    .prologue
    .line 2131650
    invoke-static {}, LX/EWl;->n()LX/EWl;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EWl;->a(LX/EWr;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EWc;
    .locals 2

    .prologue
    .line 2131645
    iget-object v0, p0, LX/EWr;->name_:Ljava/lang/Object;

    .line 2131646
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2131647
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2131648
    iput-object v0, p0, LX/EWr;->name_:Ljava/lang/Object;

    .line 2131649
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private y()V
    .locals 1

    .prologue
    .line 2131636
    const-string v0, ""

    iput-object v0, p0, LX/EWr;->name_:Ljava/lang/Object;

    .line 2131637
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    .line 2131638
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    .line 2131639
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131640
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131641
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131642
    sget-object v0, LX/EXf;->c:LX/EXf;

    move-object v0, v0

    .line 2131643
    iput-object v0, p0, LX/EWr;->options_:LX/EXf;

    .line 2131644
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2131634
    new-instance v0, LX/EWl;

    invoke-direct {v0, p1}, LX/EWl;-><init>(LX/EYd;)V

    .line 2131635
    return-object v0
.end method

.method public final a(I)LX/EXL;
    .locals 1

    .prologue
    .line 2131633
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXL;

    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2131611
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2131612
    iget v0, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2131613
    invoke-direct {p0}, LX/EWr;->x()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    :cond_0
    move v1, v2

    .line 2131614
    :goto_0
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2131615
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v4, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2131616
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2131617
    :goto_1
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2131618
    const/4 v3, 0x3

    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2131619
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2131620
    :goto_2
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2131621
    const/4 v3, 0x4

    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2131622
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 2131623
    :goto_3
    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2131624
    const/4 v3, 0x5

    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2131625
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2131626
    :cond_4
    :goto_4
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 2131627
    const/4 v1, 0x6

    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v1, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2131628
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2131629
    :cond_5
    iget v0, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 2131630
    const/4 v0, 0x7

    iget-object v1, p0, LX/EWr;->options_:LX/EXf;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2131631
    :cond_6
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2131632
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2131586
    iget-byte v0, p0, LX/EWr;->memoizedIsInitialized:B

    .line 2131587
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2131588
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2131589
    :goto_1
    invoke-virtual {p0}, LX/EWr;->l()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 2131590
    invoke-virtual {p0, v0}, LX/EWr;->a(I)LX/EXL;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2131591
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131592
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2131593
    :goto_2
    invoke-virtual {p0}, LX/EWr;->m()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 2131594
    invoke-virtual {p0, v0}, LX/EWr;->b(I)LX/EXL;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2131595
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131596
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 2131597
    :goto_3
    invoke-virtual {p0}, LX/EWr;->n()I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 2131598
    invoke-virtual {p0, v0}, LX/EWr;->c(I)LX/EWr;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_6

    .line 2131599
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131600
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v0, v1

    .line 2131601
    :goto_4
    invoke-virtual {p0}, LX/EWr;->o()I

    move-result v3

    if-ge v0, v3, :cond_9

    .line 2131602
    invoke-virtual {p0, v0}, LX/EWr;->d(I)LX/EWv;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_8

    .line 2131603
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131604
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2131605
    :cond_9
    invoke-virtual {p0}, LX/EWr;->q()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2131606
    iget-object v0, p0, LX/EWr;->options_:LX/EXf;

    move-object v0, v0

    .line 2131607
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2131608
    iput-byte v1, p0, LX/EWr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131609
    :cond_a
    iput-byte v2, p0, LX/EWr;->memoizedIsInitialized:B

    move v1, v2

    .line 2131610
    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2131562
    iget v0, p0, LX/EWr;->memoizedSerializedSize:I

    .line 2131563
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2131564
    :goto_0
    return v0

    .line 2131565
    :cond_0
    iget v0, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 2131566
    invoke-direct {p0}, LX/EWr;->x()LX/EWc;

    move-result-object v0

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 2131567
    :goto_2
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2131568
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v5, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131569
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    move v2, v1

    .line 2131570
    :goto_3
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2131571
    const/4 v4, 0x3

    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131572
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    move v2, v1

    .line 2131573
    :goto_4
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2131574
    const/4 v4, 0x4

    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131575
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_3
    move v2, v1

    .line 2131576
    :goto_5
    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2131577
    const/4 v4, 0x5

    iget-object v0, p0, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131578
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2131579
    :cond_4
    :goto_6
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2131580
    const/4 v2, 0x6

    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v2, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131581
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2131582
    :cond_5
    iget v0, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    .line 2131583
    const/4 v0, 0x7

    iget-object v1, p0, LX/EWr;->options_:LX/EXf;

    invoke-static {v0, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2131584
    :cond_6
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2131585
    iput v0, p0, LX/EWr;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final b(I)LX/EXL;
    .locals 1

    .prologue
    .line 2131561
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXL;

    return-object v0
.end method

.method public final c(I)LX/EWr;
    .locals 1

    .prologue
    .line 2131743
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWr;

    return-object v0
.end method

.method public final d(I)LX/EWv;
    .locals 1

    .prologue
    .line 2131539
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWv;

    return-object v0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2131541
    iget-object v0, p0, LX/EWr;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2131542
    sget-object v0, LX/EYC;->f:LX/EYn;

    const-class v1, LX/EWr;

    const-class v2, LX/EWl;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131540
    sget-object v0, LX/EWr;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2131543
    iget-object v0, p0, LX/EWr;->name_:Ljava/lang/Object;

    .line 2131544
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2131545
    check-cast v0, Ljava/lang/String;

    .line 2131546
    :goto_0
    return-object v0

    .line 2131547
    :cond_0
    check-cast v0, LX/EWc;

    .line 2131548
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2131549
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2131550
    iput-object v1, p0, LX/EWr;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2131551
    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 2131552
    iget-object v0, p0, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2131553
    iget-object v0, p0, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2131554
    iget-object v0, p0, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 2131555
    iget-object v0, p0, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2131556
    iget v0, p0, LX/EWr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2131557
    invoke-static {p0}, LX/EWr;->g(LX/EWr;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2131558
    invoke-static {}, LX/EWl;->n()LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2131559
    invoke-static {p0}, LX/EWr;->g(LX/EWr;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2131560
    sget-object v0, LX/EWr;->c:LX/EWr;

    return-object v0
.end method
