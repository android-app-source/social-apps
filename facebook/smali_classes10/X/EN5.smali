.class public final LX/EN5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 2111979
    iput-object p1, p0, LX/EN5;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iput-object p2, p0, LX/EN5;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/EN5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x399f501a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2111980
    iget-object v1, p0, LX/EN5;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EN5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v1, :cond_1

    .line 2111981
    :cond_0
    const v1, -0x24c5a880

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2111982
    :goto_0
    return-void

    .line 2111983
    :cond_1
    iget-object v1, p0, LX/EN5;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->j:LX/1nG;

    iget-object v2, p0, LX/EN5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    iget-object v3, p0, LX/EN5;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2111984
    iget-object v2, p0, LX/EN5;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->c:LX/17W;

    iget-object v3, p0, LX/EN5;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v3, v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2111985
    const v1, 0x4dea62f5    # 4.91544224E8f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
