.class public LX/CmO;
.super LX/CmN;
.source ""

# interfaces
.implements LX/Clu;
.implements LX/Cly;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CmM;)V
    .locals 5

    .prologue
    .line 1933353
    invoke-direct {p0, p1}, LX/CmN;-><init>(LX/CmL;)V

    .line 1933354
    iget v0, p1, LX/CmM;->a:I

    iput v0, p0, LX/CmO;->a:I

    .line 1933355
    iget-boolean v0, p1, LX/CmM;->c:Z

    iput-boolean v0, p0, LX/CmO;->b:Z

    .line 1933356
    iget-boolean v0, p0, LX/CmO;->b:Z

    if-eqz v0, :cond_0

    .line 1933357
    iget-object v0, p1, LX/CmM;->d:Landroid/content/Context;

    const v1, 0x7f081c56

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p1, LX/CmM;->b:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CmO;->c:Ljava/lang/String;

    .line 1933358
    :goto_0
    return-void

    .line 1933359
    :cond_0
    iget-object v0, p1, LX/CmM;->d:Landroid/content/Context;

    const v1, 0x7f081c57

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CmO;->c:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method
