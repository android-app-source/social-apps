.class public final LX/D0w;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/D0w;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/D0u;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/D0x;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1956134
    const/4 v0, 0x0

    sput-object v0, LX/D0w;->a:LX/D0w;

    .line 1956135
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/D0w;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1956148
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1956149
    new-instance v0, LX/D0x;

    invoke-direct {v0}, LX/D0x;-><init>()V

    iput-object v0, p0, LX/D0w;->c:LX/D0x;

    .line 1956150
    return-void
.end method

.method public static c(LX/1De;)LX/D0u;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1956140
    new-instance v1, LX/D0v;

    invoke-direct {v1}, LX/D0v;-><init>()V

    .line 1956141
    sget-object v2, LX/D0w;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0u;

    .line 1956142
    if-nez v2, :cond_0

    .line 1956143
    new-instance v2, LX/D0u;

    invoke-direct {v2}, LX/D0u;-><init>()V

    .line 1956144
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/D0u;->a$redex0(LX/D0u;LX/1De;IILX/D0v;)V

    .line 1956145
    move-object v1, v2

    .line 1956146
    move-object v0, v1

    .line 1956147
    return-object v0
.end method

.method public static declared-synchronized q()LX/D0w;
    .locals 2

    .prologue
    .line 1956151
    const-class v1, LX/D0w;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/D0w;->a:LX/D0w;

    if-nez v0, :cond_0

    .line 1956152
    new-instance v0, LX/D0w;

    invoke-direct {v0}, LX/D0w;-><init>()V

    sput-object v0, LX/D0w;->a:LX/D0w;

    .line 1956153
    :cond_0
    sget-object v0, LX/D0w;->a:LX/D0w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1956154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1956138
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x2

    invoke-interface {v0, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/16 p0, 0x8

    const p2, 0x7f0b00d6

    invoke-interface {v0, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f082a44

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a00ab

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p0, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    const p2, 0x7f020916

    invoke-virtual {p0, p2}, LX/1o5;->h(I)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1956139
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1956136
    invoke-static {}, LX/1dS;->b()V

    .line 1956137
    const/4 v0, 0x0

    return-object v0
.end method
