.class public abstract LX/EWy;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EWx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "LX/EX1;",
        "BuilderType:",
        "LX/EWy;",
        ">",
        "LX/EWj",
        "<TBuilderType;>;",
        "LX/EWx",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field public a:LX/EYc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2132047
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2132048
    sget-object v0, LX/EYc;->d:LX/EYc;

    move-object v0, v0

    .line 2132049
    iput-object v0, p0, LX/EWy;->a:LX/EYc;

    .line 2132050
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2132051
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2132052
    sget-object v0, LX/EYc;->d:LX/EYc;

    move-object v0, v0

    .line 2132053
    iput-object v0, p0, LX/EWy;->a:LX/EYc;

    .line 2132054
    return-void
.end method

.method private d(LX/EYP;)V
    .locals 2

    .prologue
    .line 2132055
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2132056
    invoke-virtual {p0}, LX/EWj;->e()LX/EYF;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2132057
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2132058
    :cond_0
    return-void
.end method

.method private e(LX/EYP;Ljava/lang/Object;)LX/EWy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2132059
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132060
    invoke-direct {p0, p1}, LX/EWy;->d(LX/EYP;)V

    .line 2132061
    invoke-direct {p0}, LX/EWy;->l()V

    .line 2132062
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0, p1, p2}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    .line 2132063
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132064
    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, LX/EWj;->c(LX/EYP;Ljava/lang/Object;)LX/EWj;

    move-result-object v0

    check-cast v0, LX/EWy;

    move-object p0, v0

    goto :goto_0
.end method

.method private f(LX/EYP;Ljava/lang/Object;)LX/EWy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2132065
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132066
    invoke-direct {p0, p1}, LX/EWy;->d(LX/EYP;)V

    .line 2132067
    invoke-direct {p0}, LX/EWy;->l()V

    .line 2132068
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0, p1, p2}, LX/EYc;->b(LX/EYP;Ljava/lang/Object;)V

    .line 2132069
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132070
    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, LX/EWj;->d(LX/EYP;Ljava/lang/Object;)LX/EWj;

    move-result-object v0

    check-cast v0, LX/EWy;

    move-object p0, v0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2132071
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    .line 2132072
    iget-boolean v1, v0, LX/EYc;->b:Z

    move v0, v1

    .line 2132073
    if-eqz v0, :cond_0

    .line 2132074
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->e()LX/EYc;

    move-result-object v0

    iput-object v0, p0, LX/EWy;->a:LX/EYc;

    .line 2132075
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2132076
    invoke-direct {p0, p1, p2}, LX/EWy;->f(LX/EYP;Ljava/lang/Object;)LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EX1;)V
    .locals 2

    .prologue
    .line 2132077
    invoke-direct {p0}, LX/EWy;->l()V

    .line 2132078
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    iget-object v1, p1, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0, v1}, LX/EYc;->a(LX/EYc;)V

    .line 2132079
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132080
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2132081
    invoke-super {p0}, LX/EWj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2132042
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132043
    invoke-direct {p0, p1}, LX/EWy;->d(LX/EYP;)V

    .line 2132044
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    .line 2132045
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EYP;)Z

    move-result v0

    goto :goto_0
.end method

.method public final synthetic b(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2132046
    invoke-direct {p0, p1, p2}, LX/EWy;->e(LX/EYP;Ljava/lang/Object;)LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2132022
    invoke-virtual {p0}, LX/EWy;->m()LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EYP;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2132023
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2132024
    invoke-direct {p0, p1}, LX/EWy;->d(LX/EYP;)V

    .line 2132025
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    .line 2132026
    if-nez v0, :cond_0

    .line 2132027
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_1

    .line 2132028
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    .line 2132029
    :cond_0
    :goto_0
    return-object v0

    .line 2132030
    :cond_1
    invoke-virtual {p1}, LX/EYP;->p()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2132031
    :cond_2
    invoke-super {p0, p1}, LX/EWj;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2132032
    invoke-virtual {p0}, LX/EWy;->m()LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EYP;Ljava/lang/Object;)LX/EWj;
    .locals 1

    .prologue
    .line 2132033
    invoke-direct {p0, p1, p2}, LX/EWy;->e(LX/EYP;Ljava/lang/Object;)LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132034
    invoke-virtual {p0}, LX/EWy;->m()LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(LX/EYP;Ljava/lang/Object;)LX/EWj;
    .locals 1

    .prologue
    .line 2132035
    invoke-direct {p0, p1, p2}, LX/EWy;->f(LX/EYP;Ljava/lang/Object;)LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2132036
    invoke-virtual {p0}, LX/EWy;->m()LX/EWy;

    move-result-object v0

    return-object v0
.end method

.method public final kb_()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132037
    invoke-static {p0}, LX/EWj;->l(LX/EWj;)Ljava/util/Map;

    move-result-object v0

    .line 2132038
    iget-object v1, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v1}, LX/EYc;->f()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2132039
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public m()LX/EWy;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2132040
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2132041
    iget-object v0, p0, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->h()Z

    move-result v0

    return v0
.end method
