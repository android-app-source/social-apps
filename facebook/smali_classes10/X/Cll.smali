.class public abstract LX/Cll;
.super Landroid/text/style/ClickableSpan;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

.field public f:Z

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1933056
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1933057
    iput-object p2, p0, LX/Cll;->d:Landroid/content/Context;

    .line 1933058
    iput-object p1, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    .line 1933059
    const-class v0, LX/Cll;

    invoke-static {v0, p0}, LX/Cll;->a(Ljava/lang/Class;LX/02k;)V

    .line 1933060
    iget-object v0, p0, LX/Cll;->c:LX/Chi;

    .line 1933061
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1933062
    iput-object v0, p0, LX/Cll;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 1933063
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0623

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Cll;->h:I

    .line 1933064
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cll;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object p0

    check-cast p0, LX/Chi;

    iput-object v1, p1, LX/Cll;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/Cll;->c:LX/Chi;

    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1933041
    iget-object v0, p0, LX/Cll;->d:Landroid/content/Context;

    return-object v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1933042
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    .line 1933043
    iget-object v2, p0, LX/Cll;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/Cll;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->w()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1933044
    iget-object v2, p0, LX/Cll;->e:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->w()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;

    move-result-object v2

    .line 1933045
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1933046
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v0

    .line 1933047
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;->c()Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;->SIMPLE_UNDERLINE:Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;

    if-ne v2, v3, :cond_3

    .line 1933048
    :cond_1
    :goto_0
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1933049
    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1933050
    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    iget v1, p0, LX/Cll;->h:I

    if-eq v0, v1, :cond_2

    .line 1933051
    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    iput v0, p0, LX/Cll;->g:I

    .line 1933052
    :cond_2
    iget-boolean v0, p0, LX/Cll;->f:Z

    if-eqz v0, :cond_4

    iget v0, p0, LX/Cll;->h:I

    :goto_1
    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 1933053
    return-void

    .line 1933054
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1933055
    :cond_4
    iget v0, p0, LX/Cll;->g:I

    goto :goto_1
.end method
