.class public LX/DQP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1My;

.field public final b:LX/0Sh;

.field public final c:LX/0se;

.field public final d:Landroid/content/res/Resources;

.field public final e:LX/0sX;


# direct methods
.method public constructor <init>(LX/1My;LX/0Sh;LX/0se;Landroid/content/res/Resources;LX/0sX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1994331
    iput-object p1, p0, LX/DQP;->a:LX/1My;

    .line 1994332
    iput-object p2, p0, LX/DQP;->b:LX/0Sh;

    .line 1994333
    iput-object p3, p0, LX/DQP;->c:LX/0se;

    .line 1994334
    iput-object p4, p0, LX/DQP;->d:Landroid/content/res/Resources;

    .line 1994335
    iput-object p5, p0, LX/DQP;->e:LX/0sX;

    .line 1994336
    return-void
.end method

.method public static b(LX/0QB;)LX/DQP;
    .locals 6

    .prologue
    .line 1994337
    new-instance v0, LX/DQP;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v1

    check-cast v1, LX/1My;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v5

    check-cast v5, LX/0sX;

    invoke-direct/range {v0 .. v5}, LX/DQP;-><init>(LX/1My;LX/0Sh;LX/0se;Landroid/content/res/Resources;LX/0sX;)V

    .line 1994338
    return-object v0
.end method
