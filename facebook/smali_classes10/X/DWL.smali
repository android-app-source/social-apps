.class public final LX/DWL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2006958
    const/4 v11, 0x0

    .line 2006959
    const/4 v10, 0x0

    .line 2006960
    const-wide/16 v8, 0x0

    .line 2006961
    const/4 v7, 0x0

    .line 2006962
    const/4 v6, 0x0

    .line 2006963
    const/4 v5, 0x0

    .line 2006964
    const/4 v4, 0x0

    .line 2006965
    const/4 v3, 0x0

    .line 2006966
    const/4 v2, 0x0

    .line 2006967
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_c

    .line 2006968
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2006969
    const/4 v2, 0x0

    .line 2006970
    :goto_0
    return v2

    .line 2006971
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_a

    .line 2006972
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2006973
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2006974
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 2006975
    const-string v13, "__type__"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    const-string v13, "__typename"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 2006976
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 2006977
    :cond_2
    const-string v13, "bylines"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2006978
    invoke-static/range {p0 .. p1}, LX/DWT;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2006979
    :cond_3
    const-string v13, "communicationRank"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 2006980
    const/4 v2, 0x1

    .line 2006981
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 2006982
    :cond_4
    const-string v13, "id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2006983
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2006984
    :cond_5
    const-string v13, "name"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2006985
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 2006986
    :cond_6
    const-string v13, "name_search_tokens"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 2006987
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 2006988
    :cond_7
    const-string v13, "profilePicture50"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 2006989
    invoke-static/range {p0 .. p1}, LX/DWV;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 2006990
    :cond_8
    const-string v13, "structured_name"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2006991
    invoke-static/range {p0 .. p1}, LX/DWS;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2006992
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2006993
    :cond_a
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2006994
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2006995
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2006996
    if-eqz v2, :cond_b

    .line 2006997
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2006998
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2006999
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2007000
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2007001
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2007002
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2007003
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v12, v7

    move v7, v11

    move v11, v6

    move v6, v10

    move v10, v5

    move-wide v15, v8

    move v8, v3

    move v9, v4

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 2007004
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2007005
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2007006
    if-eqz v0, :cond_0

    .line 2007007
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007008
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2007009
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2007010
    if-eqz v0, :cond_1

    .line 2007011
    const-string v1, "bylines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007012
    invoke-static {p0, v0, p2, p3}, LX/DWT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2007013
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2007014
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_2

    .line 2007015
    const-string v2, "communicationRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007016
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2007017
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2007018
    if-eqz v0, :cond_3

    .line 2007019
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007020
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2007021
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2007022
    if-eqz v0, :cond_4

    .line 2007023
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007024
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2007025
    :cond_4
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2007026
    if-eqz v0, :cond_5

    .line 2007027
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007028
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2007029
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2007030
    if-eqz v0, :cond_6

    .line 2007031
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007032
    invoke-static {p0, v0, p2}, LX/DWV;->a(LX/15i;ILX/0nX;)V

    .line 2007033
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2007034
    if-eqz v0, :cond_7

    .line 2007035
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007036
    invoke-static {p0, v0, p2, p3}, LX/DWS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2007037
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2007038
    return-void
.end method
