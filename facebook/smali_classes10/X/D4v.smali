.class public LX/D4v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xb;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1xe;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1xe;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1963091
    iput-object p2, p0, LX/D4v;->a:LX/1xe;

    .line 1963092
    const v0, 0x7f081015

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/D4v;->b:LX/0Ot;

    .line 1963093
    const v0, 0x7f081014

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/D4v;->c:LX/0Ot;

    .line 1963094
    return-void
.end method

.method public static a(LX/0QB;)LX/D4v;
    .locals 5

    .prologue
    .line 1963103
    const-class v1, LX/D4v;

    monitor-enter v1

    .line 1963104
    :try_start_0
    sget-object v0, LX/D4v;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963105
    sput-object v2, LX/D4v;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963106
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963107
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963108
    new-instance p0, LX/D4v;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1xe;->a(LX/0QB;)LX/1xe;

    move-result-object v4

    check-cast v4, LX/1xe;

    invoke-direct {p0, v3, v4}, LX/D4v;-><init>(Landroid/content/Context;LX/1xe;)V

    .line 1963109
    move-object v0, p0

    .line 1963110
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963111
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963112
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;I)LX/1z4;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/TextPaint;",
            "I)",
            "LX/1z4;"
        }
    .end annotation

    .prologue
    .line 1963095
    iget-object v0, p0, LX/D4v;->a:LX/1xe;

    invoke-virtual {v0, p1}, LX/1xe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 1963096
    invoke-static {p1}, LX/14w;->t(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    .line 1963097
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1963098
    :cond_0
    const/4 v2, 0x0

    .line 1963099
    :goto_0
    move-object v0, v2

    .line 1963100
    invoke-static {p1}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1963101
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 1963102
    new-instance v2, LX/1z4;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, LX/1z4;-><init>(ILjava/lang/CharSequence;I)V

    return-object v2

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/D4v;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/D4v;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0
.end method
