.class public final LX/EX0;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EWz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EX2;",
        "LX/EX0;",
        ">;",
        "LX/EWz;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2132197
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2132198
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EX0;->b:Z

    .line 2132199
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132200
    invoke-direct {p0}, LX/EX0;->w()V

    .line 2132201
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2132192
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2132193
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EX0;->b:Z

    .line 2132194
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132195
    invoke-direct {p0}, LX/EX0;->w()V

    .line 2132196
    return-void
.end method

.method private A()LX/EX2;
    .locals 2

    .prologue
    .line 2132188
    invoke-virtual {p0}, LX/EX0;->l()LX/EX2;

    move-result-object v0

    .line 2132189
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2132190
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2132191
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132181
    iget-object v0, p0, LX/EX0;->d:LX/EZ2;

    if-nez v0, :cond_0

    .line 2132182
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EX0;->c:Ljava/util/List;

    iget v0, p0, LX/EX0;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2132183
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2132184
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EX0;->d:LX/EZ2;

    .line 2132185
    const/4 v0, 0x0

    iput-object v0, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132186
    :cond_0
    iget-object v0, p0, LX/EX0;->d:LX/EZ2;

    return-object v0

    .line 2132187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EX0;
    .locals 1

    .prologue
    .line 2132177
    instance-of v0, p1, LX/EX2;

    if-eqz v0, :cond_0

    .line 2132178
    check-cast p1, LX/EX2;

    invoke-virtual {p0, p1}, LX/EX0;->a(LX/EX2;)LX/EX0;

    move-result-object p0

    .line 2132179
    :goto_0
    return-object p0

    .line 2132180
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EX0;
    .locals 4

    .prologue
    .line 2132165
    const/4 v2, 0x0

    .line 2132166
    :try_start_0
    sget-object v0, LX/EX2;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EX2;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2132167
    if-eqz v0, :cond_0

    .line 2132168
    invoke-virtual {p0, v0}, LX/EX0;->a(LX/EX2;)LX/EX0;

    .line 2132169
    :cond_0
    return-object p0

    .line 2132170
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2132171
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2132172
    check-cast v0, LX/EX2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2132173
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2132174
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2132175
    invoke-virtual {p0, v1}, LX/EX0;->a(LX/EX2;)LX/EX0;

    :cond_1
    throw v0

    .line 2132176
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2132162
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2132163
    invoke-direct {p0}, LX/EX0;->D()LX/EZ2;

    .line 2132164
    :cond_0
    return-void
.end method

.method public static x()LX/EX0;
    .locals 1

    .prologue
    .line 2132161
    new-instance v0, LX/EX0;

    invoke-direct {v0}, LX/EX0;-><init>()V

    return-object v0
.end method

.method private y()LX/EX0;
    .locals 2

    .prologue
    .line 2132160
    invoke-static {}, LX/EX0;->x()LX/EX0;

    move-result-object v0

    invoke-virtual {p0}, LX/EX0;->l()LX/EX2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EX0;->a(LX/EX2;)LX/EX0;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2132159
    invoke-direct {p0, p1}, LX/EX0;->d(LX/EWY;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2132083
    invoke-direct {p0, p1, p2}, LX/EX0;->d(LX/EWd;LX/EYZ;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EX2;)LX/EX0;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2132129
    sget-object v1, LX/EX2;->c:LX/EX2;

    move-object v1, v1

    .line 2132130
    if-ne p1, v1, :cond_0

    .line 2132131
    :goto_0
    return-object p0

    .line 2132132
    :cond_0
    const/4 v1, 0x1

    .line 2132133
    iget v2, p1, LX/EX2;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_8

    :goto_1
    move v1, v1

    .line 2132134
    if-eqz v1, :cond_1

    .line 2132135
    iget-boolean v1, p1, LX/EX2;->allowAlias_:Z

    move v1, v1

    .line 2132136
    iget v2, p0, LX/EX0;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/EX0;->a:I

    .line 2132137
    iput-boolean v1, p0, LX/EX0;->b:Z

    .line 2132138
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132139
    :cond_1
    iget-object v1, p0, LX/EX0;->d:LX/EZ2;

    if-nez v1, :cond_5

    .line 2132140
    iget-object v0, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2132141
    iget-object v0, p0, LX/EX0;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2132142
    iget-object v0, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132143
    iget v0, p0, LX/EX0;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/EX0;->a:I

    .line 2132144
    :goto_2
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132145
    :cond_2
    :goto_3
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2132146
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2132147
    :cond_3
    iget v0, p0, LX/EX0;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    .line 2132148
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EX0;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132149
    iget v0, p0, LX/EX0;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EX0;->a:I

    .line 2132150
    :cond_4
    iget-object v0, p0, LX/EX0;->c:Ljava/util/List;

    iget-object v1, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 2132151
    :cond_5
    iget-object v1, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2132152
    iget-object v1, p0, LX/EX0;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2132153
    iget-object v1, p0, LX/EX0;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2132154
    iput-object v0, p0, LX/EX0;->d:LX/EZ2;

    .line 2132155
    iget-object v1, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132156
    iget v1, p0, LX/EX0;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/EX0;->a:I

    .line 2132157
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_6

    invoke-direct {p0}, LX/EX0;->D()LX/EZ2;

    move-result-object v0

    :cond_6
    iput-object v0, p0, LX/EX0;->d:LX/EZ2;

    goto :goto_3

    .line 2132158
    :cond_7
    iget-object v0, p0, LX/EX0;->d:LX/EZ2;

    iget-object v1, p1, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2132116
    move v0, v1

    .line 2132117
    :goto_0
    iget-object v2, p0, LX/EX0;->d:LX/EZ2;

    if-nez v2, :cond_3

    .line 2132118
    iget-object v2, p0, LX/EX0;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2132119
    :goto_1
    move v2, v2

    .line 2132120
    if-ge v0, v2, :cond_2

    .line 2132121
    iget-object v2, p0, LX/EX0;->d:LX/EZ2;

    if-nez v2, :cond_4

    .line 2132122
    iget-object v2, p0, LX/EX0;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2132123
    :goto_2
    move-object v2, v2

    .line 2132124
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2132125
    :cond_0
    :goto_3
    return v1

    .line 2132126
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2132127
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132128
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EX0;->d:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EX0;->d:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2132115
    invoke-direct {p0, p1, p2}, LX/EX0;->d(LX/EWd;LX/EYZ;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2132114
    invoke-direct {p0}, LX/EX0;->y()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2132082
    invoke-direct {p0, p1, p2}, LX/EX0;->d(LX/EWd;LX/EYZ;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2132084
    invoke-direct {p0}, LX/EX0;->y()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2132085
    invoke-direct {p0, p1}, LX/EX0;->d(LX/EWY;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132086
    invoke-direct {p0}, LX/EX0;->y()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2132087
    sget-object v0, LX/EYC;->z:LX/EYn;

    const-class v1, LX/EX2;

    const-class v2, LX/EX0;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2132088
    sget-object v0, LX/EYC;->y:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2132089
    invoke-direct {p0}, LX/EX0;->y()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2132090
    invoke-virtual {p0}, LX/EX0;->l()LX/EX2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2132091
    invoke-direct {p0}, LX/EX0;->A()LX/EX2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2132092
    invoke-virtual {p0}, LX/EX0;->l()LX/EX2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2132093
    invoke-direct {p0}, LX/EX0;->A()LX/EX2;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EX2;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2132094
    new-instance v2, LX/EX2;

    invoke-direct {v2, p0}, LX/EX2;-><init>(LX/EWy;)V

    .line 2132095
    iget v3, p0, LX/EX0;->a:I

    .line 2132096
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    .line 2132097
    :goto_0
    iget-boolean v1, p0, LX/EX0;->b:Z

    .line 2132098
    iput-boolean v1, v2, LX/EX2;->allowAlias_:Z

    .line 2132099
    iget-object v1, p0, LX/EX0;->d:LX/EZ2;

    if-nez v1, :cond_1

    .line 2132100
    iget v1, p0, LX/EX0;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2132101
    iget-object v1, p0, LX/EX0;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132102
    iget v1, p0, LX/EX0;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/EX0;->a:I

    .line 2132103
    :cond_0
    iget-object v1, p0, LX/EX0;->c:Ljava/util/List;

    .line 2132104
    iput-object v1, v2, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132105
    :goto_1
    iput v0, v2, LX/EX2;->bitField0_:I

    .line 2132106
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2132107
    return-object v2

    .line 2132108
    :cond_1
    iget-object v1, p0, LX/EX0;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2132109
    iput-object v1, v2, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132110
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2132111
    invoke-direct {p0}, LX/EX0;->y()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132112
    sget-object v0, LX/EX2;->c:LX/EX2;

    move-object v0, v0

    .line 2132113
    return-object v0
.end method
