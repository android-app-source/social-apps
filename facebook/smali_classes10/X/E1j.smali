.class public LX/E1j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/E1m;

.field private final b:LX/1DS;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/ReactionRootPartDefinition",
            "<",
            "LX/3U6;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/E1m;LX/1DS;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1m;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/ReactionRootPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2071254
    iput-object p1, p0, LX/E1j;->a:LX/E1m;

    .line 2071255
    iput-object p2, p0, LX/E1j;->b:LX/1DS;

    .line 2071256
    iput-object p3, p0, LX/E1j;->c:LX/0Ot;

    .line 2071257
    return-void
.end method

.method public static a(LX/0QB;)LX/E1j;
    .locals 4

    .prologue
    .line 2071258
    new-instance v2, LX/E1j;

    invoke-static {p0}, LX/E1m;->a(LX/0QB;)LX/E1m;

    move-result-object v0

    check-cast v0, LX/E1m;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v1

    check-cast v1, LX/1DS;

    const/16 v3, 0x30a2

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/E1j;-><init>(LX/E1m;LX/1DS;LX/0Ot;)V

    .line 2071259
    move-object v0, v2

    .line 2071260
    return-object v0
.end method


# virtual methods
.method public final a(LX/3U6;LX/1DZ;LX/E1l;LX/0g8;)LX/1Rq;
    .locals 2
    .param p2    # LX/1DZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0g8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2071261
    iget-object v0, p0, LX/E1j;->b:LX/1DS;

    iget-object v1, p0, LX/E1j;->c:LX/0Ot;

    invoke-virtual {v0, v1, p3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 2071262
    iput-object p1, v0, LX/1Ql;->f:LX/1PW;

    .line 2071263
    move-object v0, v0

    .line 2071264
    if-eqz p2, :cond_0

    .line 2071265
    iput-object p2, v0, LX/1Ql;->e:LX/1DZ;

    .line 2071266
    :cond_0
    if-eqz p4, :cond_1

    .line 2071267
    invoke-virtual {v0, p4}, LX/1Ql;->a(LX/0g8;)LX/1Ql;

    .line 2071268
    :cond_1
    invoke-virtual {v0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v0

    return-object v0
.end method
