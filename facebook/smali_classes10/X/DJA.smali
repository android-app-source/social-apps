.class public LX/DJA;
.super LX/1P1;
.source ""


# static fields
.field public static final a:Landroid/graphics/Rect;

.field public static final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1984947
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/DJA;->a:Landroid/graphics/Rect;

    .line 1984948
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, LX/DJA;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1984969
    invoke-direct {p0, v0, v0}, LX/1P1;-><init>(IZ)V

    .line 1984970
    return-void
.end method


# virtual methods
.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1984949
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_0

    .line 1984950
    invoke-super {p0, p1, p2, p3, p4}, LX/1P1;->a(LX/1Od;LX/1Ok;II)V

    .line 1984951
    :goto_0
    return-void

    .line 1984952
    :cond_0
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_2

    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1984953
    :goto_1
    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v2

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v3

    add-int/2addr v3, v2

    move v2, v1

    .line 1984954
    :goto_2
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 1984955
    invoke-virtual {p1, v1}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v5

    .line 1984956
    const/4 v4, 0x0

    .line 1984957
    if-eqz v5, :cond_1

    .line 1984958
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, LX/1a3;

    .line 1984959
    iget v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v3

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p4, v6, v7}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v6

    .line 1984960
    sget v7, LX/DJA;->b:I

    invoke-virtual {v5, v7, v6}, Landroid/view/View;->measure(II)V

    .line 1984961
    sget-object v6, LX/DJA;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v5, v6}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1984962
    invoke-static {v5}, LX/1OR;->h(Landroid/view/View;)I

    move-result v6

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v6, v7

    iget v4, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v6

    .line 1984963
    invoke-virtual {p1, v5}, LX/1Od;->a(Landroid/view/View;)V

    .line 1984964
    :cond_1
    move v4, v4

    .line 1984965
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1984966
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1984967
    :cond_2
    const v0, 0x7fffffff

    goto :goto_1

    .line 1984968
    :cond_3
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    add-int/2addr v2, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/1OR;->e(II)V

    goto :goto_0
.end method
