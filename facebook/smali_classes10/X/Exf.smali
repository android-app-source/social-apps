.class public LX/Exf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/Exd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/components/list/fb/datasources/ImpressionLogger",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/EuJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/2hd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2185136
    new-instance v0, LX/Exd;

    invoke-direct {v0, p0}, LX/Exd;-><init>(LX/Exf;)V

    iput-object v0, p0, LX/Exf;->a:LX/Exd;

    .line 2185137
    return-void
.end method

.method public static a(LX/0QB;)LX/Exf;
    .locals 5

    .prologue
    .line 2185122
    const-class v1, LX/Exf;

    monitor-enter v1

    .line 2185123
    :try_start_0
    sget-object v0, LX/Exf;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2185124
    sput-object v2, LX/Exf;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2185125
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185126
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2185127
    new-instance p0, LX/Exf;

    invoke-direct {p0}, LX/Exf;-><init>()V

    .line 2185128
    invoke-static {v0}, LX/EuJ;->a(LX/0QB;)LX/EuJ;

    move-result-object v3

    check-cast v3, LX/EuJ;

    invoke-static {v0}, LX/2hd;->a(LX/0QB;)LX/2hd;

    move-result-object v4

    check-cast v4, LX/2hd;

    .line 2185129
    iput-object v3, p0, LX/Exf;->b:LX/EuJ;

    iput-object v4, p0, LX/Exf;->c:LX/2hd;

    .line 2185130
    move-object v0, p0

    .line 2185131
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2185132
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Exf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2185133
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2185134
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
