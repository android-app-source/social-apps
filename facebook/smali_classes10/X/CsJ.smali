.class public final LX/CsJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;I)V
    .locals 0

    .prologue
    .line 1942460
    iput-object p1, p0, LX/CsJ;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    iput p2, p0, LX/CsJ;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1942461
    iget-object v0, p0, LX/CsJ;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c:Ljava/util/List;

    iget v2, p0, LX/CsJ;->a:I

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1942462
    iget-object v0, p0, LX/CsJ;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942463
    return-void
.end method
