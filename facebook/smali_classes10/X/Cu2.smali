.class public LX/Cu2;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/richdocument/view/widget/SlideshowView;

.field public final c:Landroid/widget/ImageView;

.field private final d:I

.field public final e:LX/68u;

.field public final f:I

.field private final g:Landroid/view/View;

.field public final h:LX/CuR;

.field private final i:Landroid/view/View$OnAttachStateChangeListener;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 4

    .prologue
    .line 1945919
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945920
    new-instance v0, LX/CuR;

    invoke-direct {v0, p0}, LX/CuR;-><init>(LX/Cu2;)V

    iput-object v0, p0, LX/Cu2;->h:LX/CuR;

    .line 1945921
    new-instance v0, LX/CuP;

    invoke-direct {v0, p0}, LX/CuP;-><init>(LX/Cu2;)V

    iput-object v0, p0, LX/Cu2;->i:Landroid/view/View$OnAttachStateChangeListener;

    .line 1945922
    const-class v0, LX/Cu2;

    invoke-static {v0, p0}, LX/Cu2;->a(Ljava/lang/Class;LX/02k;)V

    .line 1945923
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, LX/Cu2;->g:Landroid/view/View;

    .line 1945924
    iget-object v0, p0, LX/Cu2;->g:Landroid/view/View;

    const v1, 0x7f0d16c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    .line 1945925
    iget-object v0, p0, LX/Cu2;->a:LX/Cju;

    const v1, 0x7f0d011d

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, LX/Cu2;->d:I

    .line 1945926
    const/4 v0, 0x1

    sget v1, LX/CoL;->p:I

    int-to-float v1, v1

    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/Cu2;->f:I

    .line 1945927
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/Cu2;->e:LX/68u;

    .line 1945928
    iget-object v0, p0, LX/Cu2;->e:LX/68u;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/68u;->a(Landroid/view/animation/Interpolator;)V

    .line 1945929
    iget-object v0, p0, LX/Cu2;->e:LX/68u;

    const/4 v1, -0x1

    .line 1945930
    iput v1, v0, LX/68u;->x:I

    .line 1945931
    iget-object v0, p0, LX/Cu2;->e:LX/68u;

    sget v1, LX/CoL;->o:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1945932
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1945933
    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    iput-object v0, p0, LX/Cu2;->b:Lcom/facebook/richdocument/view/widget/SlideshowView;

    .line 1945934
    iget-object v0, p0, LX/Cu2;->b:Lcom/facebook/richdocument/view/widget/SlideshowView;

    new-instance v1, LX/CuQ;

    invoke-direct {v1, p0}, LX/CuQ;-><init>(LX/Cu2;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1945935
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Cu2;->i:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1945936
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cu2;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object p0, p1, LX/Cu2;->a:LX/Cju;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1945915
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1945916
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1945917
    iget-object v0, p0, LX/Cu2;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1945918
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1945910
    iget-object v0, p0, LX/Cu2;->b:Lcom/facebook/richdocument/view/widget/SlideshowView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 1945911
    if-nez v0, :cond_0

    .line 1945912
    iget-object v0, p0, LX/Cu2;->b:Lcom/facebook/richdocument/view/widget/SlideshowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/Cu2;->b(I)V

    .line 1945913
    :goto_0
    return-void

    .line 1945914
    :cond_0
    invoke-virtual {p0}, LX/Cu2;->a()V

    goto :goto_0
.end method

.method public final a(LX/CrS;)V
    .locals 5

    .prologue
    .line 1945892
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1945893
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1945894
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, LX/Cu2;->d:I

    sub-int/2addr v1, v2

    .line 1945895
    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    iget-object v2, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 1945896
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1945897
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1945898
    iget-object v1, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-interface {v0, v1, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1945899
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 1945905
    iget-object v0, p0, LX/Cu2;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 1945906
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float v3, p1

    div-float v0, v3, v0

    sub-float v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1945907
    iget-object v1, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1945908
    iget-object v0, p0, LX/Cu2;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1945909
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1945902
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Cu2;->i:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1945903
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Cu2;->i:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1945904
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1945900
    iget-object v0, p0, LX/Cu2;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Cu2;->i:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1945901
    return-void
.end method
