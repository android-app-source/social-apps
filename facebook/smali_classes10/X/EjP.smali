.class public final LX/EjP;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "LX/EjO;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 2161423
    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 2161424
    iput-object p1, p0, LX/EjP;->a:Landroid/database/Cursor;

    .line 2161425
    return-void
.end method

.method public static a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2161361
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-static {v0, p1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/EjP;LX/EjO;)V
    .locals 2

    .prologue
    .line 2161393
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v1, "mimetype"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161394
    const-string v1, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2161395
    const-string v0, "data1"

    invoke-static {p0, v0}, LX/EjP;->a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161396
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2161397
    iget-object p0, p1, LX/EjO;->c:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2161398
    :cond_0
    :goto_0
    return-void

    .line 2161399
    :cond_1
    const-string v1, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2161400
    const-string v0, "data1"

    invoke-static {p0, v0}, LX/EjP;->a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161401
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2161402
    iget-object p0, p1, LX/EjO;->b:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2161403
    :cond_2
    goto :goto_0

    .line 2161404
    :cond_3
    const-string v1, "vnd.android.cursor.item/name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2161405
    const-string v0, "data1"

    invoke-static {p0, v0}, LX/EjP;->a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161406
    iput-object v0, p1, LX/EjO;->d:Ljava/lang/String;

    .line 2161407
    const-string v0, "data2"

    invoke-static {p0, v0}, LX/EjP;->a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161408
    iput-object v0, p1, LX/EjO;->e:Ljava/lang/String;

    .line 2161409
    const-string v0, "data3"

    invoke-static {p0, v0}, LX/EjP;->a(LX/EjP;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2161410
    iput-object v0, p1, LX/EjO;->f:Ljava/lang/String;

    .line 2161411
    goto :goto_0
.end method

.method public static g(LX/EjP;)I
    .locals 4

    .prologue
    .line 2161412
    const/4 v0, 0x0

    .line 2161413
    :goto_0
    const/4 v1, 0x0

    .line 2161414
    iget-object v2, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2161415
    :cond_0
    :goto_1
    move v1, v1

    .line 2161416
    if-eqz v1, :cond_1

    .line 2161417
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2161418
    :cond_1
    return v0

    .line 2161419
    :cond_2
    iget-object v2, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v3, "deleted"

    invoke-static {v2, v3}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 2161420
    if-eqz v2, :cond_3

    .line 2161421
    iget-object v3, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 2161422
    :cond_3
    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2161377
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2161378
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 2161379
    :cond_0
    invoke-static {p0}, LX/EjP;->g(LX/EjP;)I

    .line 2161380
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2161381
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjO;

    .line 2161382
    :goto_0
    return-object v0

    .line 2161383
    :cond_1
    iget-object v1, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v2, "contact_id"

    invoke-static {v1, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2161384
    new-instance v1, LX/EjO;

    invoke-direct {v1, v2}, LX/EjO;-><init>(Ljava/lang/String;)V

    .line 2161385
    :cond_2
    invoke-static {p0}, LX/EjP;->g(LX/EjP;)I

    .line 2161386
    iget-object v3, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2161387
    iget-object v3, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v4, "contact_id"

    invoke-static {v3, v4}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2161388
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2161389
    invoke-static {p0, v1}, LX/EjP;->a(LX/EjP;LX/EjO;)V

    .line 2161390
    iget-object v3, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2161391
    :cond_3
    move-object v0, v1

    .line 2161392
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    .line 2161364
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 2161365
    const-wide/16 v2, -0x1

    .line 2161366
    const/4 v0, 0x0

    .line 2161367
    iget-object v4, p0, LX/EjP;->a:Landroid/database/Cursor;

    const/4 v5, -0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2161368
    :cond_0
    :goto_0
    iget-object v4, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2161369
    iget-object v4, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v5, "deleted"

    invoke-static {v4, v5}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 2161370
    if-nez v4, :cond_0

    .line 2161371
    iget-object v4, p0, LX/EjP;->a:Landroid/database/Cursor;

    const-string v5, "contact_id"

    invoke-static {v4, v5}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2161372
    cmp-long v6, v4, v2

    if-eqz v6, :cond_0

    .line 2161373
    add-int/lit8 v0, v0, 0x1

    move-wide v2, v4

    .line 2161374
    goto :goto_0

    .line 2161375
    :cond_1
    iget-object v2, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2161376
    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2161362
    iget-object v0, p0, LX/EjP;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2161363
    return-void
.end method
