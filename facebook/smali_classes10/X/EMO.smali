.class public final LX/EMO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EMQ;


# direct methods
.method public constructor <init>(LX/EMQ;)V
    .locals 0

    .prologue
    .line 2110603
    iput-object p1, p0, LX/EMO;->a:LX/EMQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2110604
    const/4 v5, 0x0

    .line 2110605
    iget-object v0, p0, LX/EMO;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dj;

    iget-object v0, p0, LX/EMO;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2h8;->SEARCH:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
