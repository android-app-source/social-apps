.class public final LX/Ed4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ecx;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/Ed3;


# direct methods
.method public constructor <init>(Ljava/util/List;LX/Ed3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;",
            "LX/Ed3;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2148246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148247
    iput-object p1, p0, LX/Ed4;->a:Ljava/util/List;

    .line 2148248
    iput-object p2, p0, LX/Ed4;->b:LX/Ed3;

    .line 2148249
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148250
    iget-object v0, p0, LX/Ed4;->b:LX/Ed3;

    invoke-virtual {v0}, LX/Ed3;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148251
    iget-object v0, p0, LX/Ed4;->b:LX/Ed3;

    invoke-virtual {v0}, LX/Ed3;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2148252
    iget-object v0, p0, LX/Ed4;->b:LX/Ed3;

    invoke-virtual {v0}, LX/Ed3;->c()I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2148253
    iget-object v1, p0, LX/Ed4;->a:Ljava/util/List;

    monitor-enter v1

    .line 2148254
    :try_start_0
    iget-object v2, p0, LX/Ed4;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_0

    .line 2148255
    iget-object v0, p0, LX/Ed4;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2148256
    iget-object v0, p0, LX/Ed4;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2148257
    const/4 v0, 0x1

    .line 2148258
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148259
    if-eqz v0, :cond_1

    .line 2148260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set APN [MMSC="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ed4;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PROXY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/Ed4;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PORT="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/Ed4;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] to be first"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148261
    :cond_1
    return-void

    .line 2148262
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
