.class public LX/D2N;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/2ua;


# direct methods
.method public constructor <init>(LX/2ua;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958613
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1958614
    iput-object p1, p0, LX/D2N;->a:LX/2ua;

    .line 1958615
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 3
    .param p3    # Lcom/facebook/interstitial/manager/InterstitialTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1958616
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/D2N;->a:LX/2ua;

    invoke-virtual {v1}, LX/2ua;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1958617
    :cond_0
    :goto_0
    return v0

    .line 1958618
    :cond_1
    iget-object v1, p3, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    const-string v2, "context_profile_has_profile_video"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1958619
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
