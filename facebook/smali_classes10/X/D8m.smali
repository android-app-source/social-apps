.class public LX/D8m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0hB;

.field public final b:LX/2mn;


# direct methods
.method public constructor <init>(LX/0hB;LX/2mn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969359
    iput-object p1, p0, LX/D8m;->a:LX/0hB;

    .line 1969360
    iput-object p2, p0, LX/D8m;->b:LX/2mn;

    .line 1969361
    return-void
.end method

.method public static a(LX/D8m;Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)F
    .locals 3

    .prologue
    .line 1969347
    iget-object v0, p0, LX/D8m;->b:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;F)LX/3FO;

    move-result-object v0

    .line 1969348
    iget v0, v0, LX/3FO;->d:I

    int-to-float v0, v0

    move v0, v0

    .line 1969349
    const/high16 p2, 0x40000000    # 2.0f

    .line 1969350
    const v1, 0x7f0b1c95

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1969351
    const v2, 0x7f0b0051

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 1969352
    mul-float/2addr v1, p2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    move v1, v1

    .line 1969353
    add-float/2addr v0, v1

    .line 1969354
    if-eqz p3, :cond_0

    .line 1969355
    const v1, 0x7f0b1c90

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    move v1, v1

    .line 1969356
    add-float/2addr v0, v1

    .line 1969357
    :cond_0
    iget-object v1, p0, LX/D8m;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    return v0
.end method

.method public static a(Landroid/content/res/Resources;)F
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1969362
    const v0, 0x7f0b1c8e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1969363
    const v1, 0x7f0b0051

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1969364
    mul-float/2addr v0, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public static a(LX/0QB;)LX/D8m;
    .locals 5

    .prologue
    .line 1969335
    const-class v1, LX/D8m;

    monitor-enter v1

    .line 1969336
    :try_start_0
    sget-object v0, LX/D8m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1969337
    sput-object v2, LX/D8m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1969338
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969339
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1969340
    new-instance p0, LX/D8m;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v4

    check-cast v4, LX/2mn;

    invoke-direct {p0, v3, v4}, LX/D8m;-><init>(LX/0hB;LX/2mn;)V

    .line 1969341
    move-object v0, p0

    .line 1969342
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1969343
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D8m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1969344
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1969345
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(Landroid/content/res/Resources;)F
    .locals 1

    .prologue
    .line 1969346
    const v0, 0x7f0b1c93

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method
