.class public final LX/DcC;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/0bP;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;)V
    .locals 0

    .prologue
    .line 2017986
    iput-object p1, p0, LX/DcC;->a:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2017987
    const-class v0, LX/0bP;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2017988
    check-cast p1, LX/0bP;

    .line 2017989
    sget-object v0, LX/8LS;->MENU_PHOTO:LX/8LS;

    .line 2017990
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 2017991
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v2

    .line 2017992
    invoke-virtual {v0, v1}, LX/8LS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/8KZ;->UPLOADING:LX/8KZ;

    .line 2017993
    iget-object v1, p1, LX/0b5;->b:LX/8KZ;

    move-object v1, v1

    .line 2017994
    invoke-virtual {v0, v1}, LX/8KZ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2017995
    iget-object v0, p0, LX/DcC;->a:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    .line 2017996
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2017997
    if-eqz v0, :cond_1

    .line 2017998
    iget-object v0, p0, LX/DcC;->a:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2017999
    :cond_0
    :goto_0
    return-void

    .line 2018000
    :cond_1
    iget-object v0, p0, LX/DcC;->a:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    const/4 v1, 0x1

    .line 2018001
    iput-boolean v1, v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->l:Z

    .line 2018002
    goto :goto_0
.end method
