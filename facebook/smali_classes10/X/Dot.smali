.class public LX/Dot;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile f:LX/Dot;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oy;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042654
    const-class v0, LX/Dot;

    sput-object v0, LX/Dot;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Oy;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042706
    iput-object p1, p0, LX/Dot;->b:LX/0Ot;

    .line 2042707
    iput-object p3, p0, LX/Dot;->d:LX/0Or;

    .line 2042708
    iput-object p2, p0, LX/Dot;->c:LX/0Ot;

    .line 2042709
    iput-object p4, p0, LX/Dot;->e:LX/0Or;

    .line 2042710
    return-void
.end method

.method public static a(LX/0QB;)LX/Dot;
    .locals 7

    .prologue
    .line 2042692
    sget-object v0, LX/Dot;->f:LX/Dot;

    if-nez v0, :cond_1

    .line 2042693
    const-class v1, LX/Dot;

    monitor-enter v1

    .line 2042694
    :try_start_0
    sget-object v0, LX/Dot;->f:LX/Dot;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042695
    if-eqz v2, :cond_0

    .line 2042696
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042697
    new-instance v3, LX/Dot;

    const/16 v4, 0xf9a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xdc2

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2a16

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/Dot;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V

    .line 2042698
    move-object v0, v3

    .line 2042699
    sput-object v0, LX/Dot;->f:LX/Dot;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042700
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042701
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042702
    :cond_1
    sget-object v0, LX/Dot;->f:LX/Dot;

    return-object v0

    .line 2042703
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Dot;)V
    .locals 5

    .prologue
    .line 2042711
    iget-object v0, p0, LX/Dot;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2042712
    if-nez v0, :cond_0

    .line 2042713
    sget-object v0, LX/Dot;->a:Ljava/lang/Class;

    const-string v1, "Could not destroy legacy pre-key ID data as user is not logged in"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2042714
    :goto_0
    return-void

    .line 2042715
    :cond_0
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2042716
    iget-object v0, p0, LX/Dot;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v0, LX/Don;->c:LX/0Tn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v2, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/Don;->d:LX/0Tn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v2, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private static a(LX/Dot;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2042668
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT id FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    move v1, v3

    .line 2042669
    :goto_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042670
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2042671
    invoke-static {v4, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 2042672
    const-string v5, "id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 2042673
    if-le v0, v1, :cond_6

    if-gt v0, p4, :cond_6

    :goto_1
    move v1, v0

    .line 2042674
    goto :goto_0

    .line 2042675
    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2042676
    :cond_1
    if-ne v1, v3, :cond_4

    .line 2042677
    :goto_2
    return-void

    .line 2042678
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042679
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v4, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 2042680
    :cond_4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2042681
    add-int/lit8 v0, v1, 0x1

    .line 2042682
    const v1, 0x7fffffff

    if-ge p4, v1, :cond_5

    .line 2042683
    rem-int/2addr v0, p4

    .line 2042684
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 2042685
    iget-object v0, p0, LX/Dot;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oy;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, p5, v1}, LX/2Oy;->a([B[B)[B

    move-result-object v0

    .line 2042686
    const-string v1, "encrypted_value"

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2042687
    sget-object v0, LX/2Iu;->a:LX/0U1;

    .line 2042688
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2042689
    invoke-virtual {v3, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042690
    const-string v0, "properties"

    const v1, -0x1d01aa8

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x28c8d4a2

    invoke-static {v0}, LX/03h;->a(I)V

    goto :goto_2

    .line 2042691
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2042655
    iget-object v0, p0, LX/Dot;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dok;

    invoke-static {p1, v0}, LX/Dow;->a(Landroid/database/sqlite/SQLiteDatabase;LX/Dok;)[B

    move-result-object v5

    .line 2042656
    if-nez v5, :cond_0

    .line 2042657
    sget-object v0, LX/Dot;->a:Ljava/lang/Class;

    const-string v1, "Failed to retrieve master key during v27 db upgrade"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v6

    .line 2042658
    :goto_0
    return v0

    .line 2042659
    :cond_0
    :try_start_0
    const-string v2, "pre_keys"

    const-string v3, "next_pkid"

    const v4, 0xfffffe

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/Dot;->a(LX/Dot;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 2042660
    const-string v2, "signed_pre_keys"

    const-string v3, "next_spkid"

    const v4, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/Dot;->a(LX/Dot;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 2042661
    invoke-static {p0}, LX/Dot;->a(LX/Dot;)V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2042662
    const/4 v0, 0x1

    goto :goto_0

    .line 2042663
    :catch_0
    move-exception v0

    .line 2042664
    :goto_1
    sget-object v1, LX/Dot;->a:Ljava/lang/Class;

    const-string v2, "Failed to migrate pre key id during v27 db upgrade"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2042665
    invoke-static {p0}, LX/Dot;->a(LX/Dot;)V

    move v0, v6

    .line 2042666
    goto :goto_0

    .line 2042667
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method
