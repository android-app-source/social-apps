.class public final LX/Dy9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2063989
    iput-object p1, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2063990
    iget-object v0, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    .line 2063991
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2063992
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2063993
    if-eqz p1, :cond_0

    .line 2063994
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2063995
    if-nez v0, :cond_1

    .line 2063996
    :cond_0
    iget-object v0, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    .line 2063997
    :goto_0
    return-void

    .line 2063998
    :cond_1
    iget-object v0, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    const/4 v1, 0x1

    .line 2063999
    iput-boolean v1, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->j:Z

    .line 2064000
    iget-object v1, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    new-instance v2, Ljava/util/ArrayList;

    .line 2064001
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2064002
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2064003
    iput-object v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    .line 2064004
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2064005
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2064006
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2064007
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    .line 2064008
    :cond_2
    iget-object v0, p0, LX/Dy9;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->m(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    goto :goto_0
.end method
