.class public final LX/DoC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/4yu",
        "<",
        "Lcom/facebook/messaging/model/threads/ThreadSummary;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2041508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2041509
    check-cast p1, LX/4yu;

    check-cast p2, LX/4yu;

    .line 2041510
    invoke-interface {p1}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2041511
    const/4 v0, 0x0

    .line 2041512
    :goto_0
    return v0

    .line 2041513
    :cond_0
    invoke-interface {p1}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2041514
    const/4 v0, 0x1

    goto :goto_0

    .line 2041515
    :cond_1
    invoke-interface {p2}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2041516
    const/4 v0, -0x1

    goto :goto_0

    .line 2041517
    :cond_2
    sget-object v0, LX/DoE;->a:Ljava/util/Comparator;

    invoke-interface {p1}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
