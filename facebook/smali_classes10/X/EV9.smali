.class public final LX/EV9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETZ;

.field public final synthetic b:LX/EVB;


# direct methods
.method public constructor <init>(LX/EVB;LX/ETZ;)V
    .locals 0

    .prologue
    .line 2127524
    iput-object p1, p0, LX/EV9;->b:LX/EVB;

    iput-object p2, p0, LX/EV9;->a:LX/ETZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2127501
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 2127502
    iget-object v0, p0, LX/EV9;->b:LX/EVB;

    iget-object v0, v0, LX/EVB;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETa;

    .line 2127503
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2127504
    :goto_0
    return-object v0

    .line 2127505
    :cond_0
    iget-object v3, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, v3, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127506
    if-eqz v3, :cond_5

    if-eqz p1, :cond_5

    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2127507
    const/4 v4, 0x1

    .line 2127508
    :goto_1
    move v4, v4

    .line 2127509
    move v2, v4

    .line 2127510
    if-eqz v2, :cond_1

    .line 2127511
    iget-object v2, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, v3, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127512
    iput-object v3, v2, LX/EVB;->i:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127513
    :goto_2
    iget-object v2, p0, LX/EV9;->b:LX/EVB;

    iget-object v2, v2, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 2127514
    goto :goto_0

    .line 2127515
    :cond_1
    iget-object v2, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, v3, LX/EVB;->i:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127516
    iput-object v3, v2, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127517
    goto :goto_2

    .line 2127518
    :cond_2
    iget-object v2, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, p0, LX/EV9;->b:LX/EVB;

    iget-object v3, v3, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iget-object v4, p0, LX/EV9;->a:LX/ETZ;

    invoke-interface {v0, v3, v4}, LX/ETa;->a(Ljava/lang/Object;LX/ETZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127519
    iput-object v0, v2, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127520
    iget-object v0, p0, LX/EV9;->b:LX/EVB;

    iget-object v0, v0, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EV9;->b:LX/EVB;

    iget-object v0, v0, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2127521
    :goto_3
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2127522
    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 2127523
    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method
