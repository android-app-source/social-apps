.class public final LX/E3e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E3f;


# direct methods
.method public constructor <init>(LX/E3f;)V
    .locals 0

    .prologue
    .line 2074944
    iput-object p1, p0, LX/E3e;->a:LX/E3f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, -0x5b81fd22

    invoke-static {v5, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2074945
    new-array v1, v5, [I

    .line 2074946
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2074947
    aget v2, v1, v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v4, v4, 0x2

    if-lt v2, v4, :cond_0

    aget v1, v1, v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v1, v2, :cond_1

    .line 2074948
    :cond_0
    const v0, -0x735ee2f9

    invoke-static {v5, v5, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2074949
    :goto_0
    return-void

    .line 2074950
    :cond_1
    iget-object v1, p0, LX/E3e;->a:LX/E3f;

    iget-object v1, v1, LX/E3f;->b:LX/E3g;

    iget-object v1, v1, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2074951
    iget v2, v1, LX/E2f;->d:I

    move v4, v2

    .line 2074952
    move v1, v0

    move v2, v0

    .line 2074953
    :goto_1
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget v0, v0, LX/E3f;->a:I

    if-ge v2, v0, :cond_3

    .line 2074954
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    invoke-virtual {v0, v2}, LX/E2f;->d(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2074955
    if-ne v1, v4, :cond_2

    .line 2074956
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    .line 2074957
    iget-object v5, v0, LX/E2f;->f:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2074958
    const/4 v5, 0x1

    .line 2074959
    iget v6, v0, LX/E2f;->c:I

    if-ne v6, v5, :cond_6

    :goto_2
    iput-boolean v5, v0, LX/E2f;->g:Z

    .line 2074960
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->c:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v5

    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074961
    iget-object v6, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2074962
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074963
    iget-object v7, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2074964
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v0

    sget-object v8, LX/Cfc;->XOUT_TAP:LX/Cfc;

    invoke-virtual {v5, v6, v7, v0, v8}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2074965
    :cond_2
    add-int/lit8 v0, v1, 0x1

    .line 2074966
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 2074967
    :cond_3
    add-int/lit8 v0, v1, -0x1

    if-ne v4, v0, :cond_4

    .line 2074968
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->f:LX/E2f;

    add-int/lit8 v1, v4, -0x1

    .line 2074969
    iput v1, v0, LX/E2f;->d:I

    .line 2074970
    :cond_4
    iget-object v0, p0, LX/E3e;->a:LX/E3f;

    iget-object v0, v0, LX/E3f;->b:LX/E3g;

    iget-object v0, v0, LX/E3g;->c:LX/1Pn;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2074971
    const v0, 0x32ae3c04

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_3

    .line 2074972
    :cond_6
    const/4 v5, 0x0

    goto :goto_2
.end method
