.class public LX/DOI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1992179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;LX/7B5;LX/8ht;LX/0Uh;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1992180
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1992181
    :cond_0
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1992182
    :cond_1
    :goto_0
    return-object v0

    .line 1992183
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v0, v1, :cond_4

    .line 1992184
    :cond_3
    iget-boolean v0, p3, LX/8ht;->h:Z

    move v0, v0

    .line 1992185
    if-nez v0, :cond_7

    .line 1992186
    :cond_4
    sget-object v4, LX/7B5;->SINGLE_STATE:LX/7B5;

    .line 1992187
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 1992188
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->GROUP:LX/103;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v5, LX/7B5;->TAB:LX/7B5;

    if-ne v5, v4, :cond_5

    move v5, v6

    :goto_2
    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 1992189
    invoke-static {p0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v1

    invoke-static {v1}, LX/DJw;->a(LX/DZC;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1992190
    sget-object v1, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    new-instance v2, LX/7B8;

    invoke-direct {v2}, LX/7B8;-><init>()V

    .line 1992191
    iput-boolean v6, v2, LX/7B8;->a:Z

    .line 1992192
    move-object v2, v2

    .line 1992193
    iput-boolean v6, v2, LX/7B8;->b:Z

    .line 1992194
    move-object v2, v2

    .line 1992195
    const-string v3, "available"

    .line 1992196
    iput-object v3, v2, LX/7B8;->d:Ljava/lang/String;

    .line 1992197
    move-object v2, v2

    .line 1992198
    sget v3, LX/2SU;->W:I

    invoke-virtual {p4, v3, v7}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 1992199
    iput-boolean v3, v2, LX/7B8;->c:Z

    .line 1992200
    move-object v2, v2

    .line 1992201
    invoke-virtual {v2}, LX/7B8;->a()Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_5
    move v5, v7

    .line 1992202
    goto :goto_2

    .line 1992203
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v1, v2, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 1992204
    if-eqz v1, :cond_1

    .line 1992205
    sget-object v1, LX/7B4;->GROUP_COMMUNITY:LX/7B4;

    new-instance v2, LX/DNd;

    invoke-direct {v2}, LX/DNd;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    .line 1992206
    iput-object v3, v2, LX/DNd;->a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1992207
    move-object v2, v2

    .line 1992208
    invoke-virtual {v2}, LX/DNd;->a()Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    goto/16 :goto_0

    :cond_7
    move-object v4, p2

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 4

    .prologue
    .line 1992209
    sget-object v0, LX/7B5;->TAB:LX/7B5;

    invoke-static {p0, p1, v0, p3, p4}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;LX/7B5;LX/8ht;LX/0Uh;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 1992210
    new-instance v1, LX/7BC;

    invoke-direct {v1}, LX/7BC;-><init>()V

    .line 1992211
    iput-boolean p2, v1, LX/7BC;->b:Z

    .line 1992212
    move-object v1, v1

    .line 1992213
    sget-short v2, LX/100;->k:S

    const/4 v3, 0x0

    invoke-interface {p5, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v2

    invoke-static {v2}, LX/DJw;->a(LX/DZC;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1992214
    const/4 v2, 0x1

    .line 1992215
    iput-boolean v2, v1, LX/7BC;->a:Z

    .line 1992216
    :cond_0
    sget-object v2, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v1}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    .line 1992217
    move-object v0, v0

    .line 1992218
    return-object v0
.end method
