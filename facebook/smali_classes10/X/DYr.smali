.class public final LX/DYr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 2012520
    const/4 v11, 0x0

    .line 2012521
    const/4 v10, 0x0

    .line 2012522
    const/4 v9, 0x0

    .line 2012523
    const/4 v8, 0x0

    .line 2012524
    const/4 v5, 0x0

    .line 2012525
    const-wide/16 v6, 0x0

    .line 2012526
    const/4 v4, 0x0

    .line 2012527
    const/4 v3, 0x0

    .line 2012528
    const/4 v2, 0x0

    .line 2012529
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 2012530
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2012531
    const/4 v2, 0x0

    .line 2012532
    :goto_0
    return v2

    .line 2012533
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_9

    .line 2012534
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2012535
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2012536
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 2012537
    const-string v13, "groups"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2012538
    invoke-static/range {p0 .. p1}, LX/DYm;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2012539
    :cond_1
    const-string v13, "id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 2012540
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 2012541
    :cond_2
    const-string v13, "mutual_friends"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2012542
    invoke-static/range {p0 .. p1}, LX/DYn;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 2012543
    :cond_3
    const-string v13, "name"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 2012544
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 2012545
    :cond_4
    const-string v13, "profile_picture"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2012546
    invoke-static/range {p0 .. p1}, LX/DYo;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2012547
    :cond_5
    const-string v13, "registration_time"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2012548
    const/4 v2, 0x1

    .line 2012549
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 2012550
    :cond_6
    const-string v13, "url"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 2012551
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 2012552
    :cond_7
    const-string v13, "work_info"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2012553
    invoke-static/range {p0 .. p1}, LX/DYq;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2012554
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2012555
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2012556
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 2012557
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 2012558
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2012559
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2012560
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2012561
    if-eqz v2, :cond_a

    .line 2012562
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2012563
    :cond_a
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2012564
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2012565
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v4

    move v15, v8

    move v8, v3

    move-wide/from16 v16, v6

    move v6, v5

    move v7, v15

    move-wide/from16 v4, v16

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 2012566
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012567
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012568
    if-eqz v0, :cond_1

    .line 2012569
    const-string v1, "groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012570
    const/4 v1, 0x0

    .line 2012571
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012572
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2012573
    if-eqz v1, :cond_0

    .line 2012574
    const-string v4, "open_group_count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012575
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2012576
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012577
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012578
    if-eqz v0, :cond_2

    .line 2012579
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012580
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012581
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012582
    if-eqz v0, :cond_4

    .line 2012583
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012584
    const/4 v1, 0x0

    .line 2012585
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012586
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2012587
    if-eqz v1, :cond_3

    .line 2012588
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012589
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2012590
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012591
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012592
    if-eqz v0, :cond_5

    .line 2012593
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012594
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012595
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012596
    if-eqz v0, :cond_7

    .line 2012597
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012598
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012599
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2012600
    if-eqz v1, :cond_6

    .line 2012601
    const-string v4, "uri"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012602
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012603
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012604
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2012605
    cmp-long v2, v0, v2

    if-eqz v2, :cond_8

    .line 2012606
    const-string v2, "registration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012607
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2012608
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012609
    if-eqz v0, :cond_9

    .line 2012610
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012611
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012612
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012613
    if-eqz v0, :cond_a

    .line 2012614
    const-string v1, "work_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012615
    invoke-static {p0, v0, p2, p3}, LX/DYq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2012616
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012617
    return-void
.end method
