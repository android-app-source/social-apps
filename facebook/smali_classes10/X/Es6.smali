.class public final LX/Es6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2174480
    iput-object p1, p0, LX/Es6;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2174481
    iput-object p2, p0, LX/Es6;->b:Ljava/lang/String;

    .line 2174482
    iput-object p3, p0, LX/Es6;->c:Ljava/lang/String;

    .line 2174483
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x713977f1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174484
    iget-object v1, p0, LX/Es6;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->f:LX/1Cn;

    iget-object v2, p0, LX/Es6;->c:Ljava/lang/String;

    .line 2174485
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/3lw;->GOODWILL_THROWBACK_SHARE_MESSAGE_OPEN:LX/3lw;

    iget-object v5, v5, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "goodwill"

    .line 2174486
    iput-object v5, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2174487
    move-object v3, v3

    .line 2174488
    const-string v5, "campaign_id"

    invoke-virtual {v3, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2174489
    iget-object v5, v1, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v5, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2174490
    sget-object v1, LX/0ax;->ai:Ljava/lang/String;

    iget-object v2, p0, LX/Es6;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2174491
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2174492
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2174493
    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2174494
    iget-object v1, p0, LX/Es6;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2174495
    const v1, -0x1069349a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
