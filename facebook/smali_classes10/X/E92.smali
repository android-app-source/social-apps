.class public final LX/E92;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:LX/E93;


# direct methods
.method public constructor <init>(LX/E93;)V
    .locals 0

    .prologue
    .line 2083627
    iput-object p1, p0, LX/E92;->a:LX/E93;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 2083628
    check-cast p1, LX/1Zj;

    const/4 v6, 0x0

    .line 2083629
    iget-object v0, p1, LX/1Zj;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, LX/1Zj;->a:Ljava/lang/String;

    .line 2083630
    :goto_0
    iget-object v1, p0, LX/E92;->a:LX/E93;

    iget-object v1, v1, LX/E93;->x:LX/E1l;

    const/4 v3, 0x0

    .line 2083631
    if-nez v0, :cond_2

    move-object v2, v3

    .line 2083632
    :goto_1
    move-object v1, v2

    .line 2083633
    if-nez v1, :cond_1

    .line 2083634
    :goto_2
    return-void

    .line 2083635
    :cond_0
    iget-object v0, p1, LX/1Zj;->d:Ljava/lang/String;

    goto :goto_0

    .line 2083636
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    .line 2083637
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2083638
    iget-object v1, p0, LX/E92;->a:LX/E93;

    iget-object v1, v1, LX/E93;->A:LX/967;

    iget-boolean v3, p1, LX/1Zj;->e:Z

    iget-wide v4, p1, LX/1Zj;->g:J

    new-instance v8, LX/E91;

    invoke-direct {v8, p0, v0, p1, v7}, LX/E91;-><init>(LX/E92;Ljava/lang/String;LX/1Zj;Ljava/lang/String;)V

    move-object v7, v6

    invoke-virtual/range {v1 .. v8}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZJLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    goto :goto_2

    .line 2083639
    :cond_2
    iget-object v2, v1, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cfo;

    .line 2083640
    invoke-interface {v2}, LX/Cfo;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2083641
    if-eqz v2, :cond_3

    .line 2083642
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_1

    :cond_4
    move-object v2, v3

    .line 2083643
    goto :goto_1
.end method
