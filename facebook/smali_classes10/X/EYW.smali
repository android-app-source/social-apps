.class public final LX/EYW;
.super LX/EWY;
.source ""


# instance fields
.field public final a:LX/EYF;

.field public final b:LX/EYc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/EZQ;

.field private d:I


# direct methods
.method public constructor <init>(LX/EYF;LX/EYc;LX/EZQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYF;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EZQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137530
    invoke-direct {p0}, LX/EWY;-><init>()V

    .line 2137531
    const/4 v0, -0x1

    iput v0, p0, LX/EYW;->d:I

    .line 2137532
    iput-object p1, p0, LX/EYW;->a:LX/EYF;

    .line 2137533
    iput-object p2, p0, LX/EYW;->b:LX/EYc;

    .line 2137534
    iput-object p3, p0, LX/EYW;->c:LX/EZQ;

    .line 2137535
    return-void
.end method

.method public static a(LX/EYF;)LX/EYW;
    .locals 3

    .prologue
    .line 2137536
    new-instance v0, LX/EYW;

    .line 2137537
    sget-object v1, LX/EYc;->d:LX/EYc;

    move-object v1, v1

    .line 2137538
    sget-object v2, LX/EZQ;->a:LX/EZQ;

    move-object v2, v2

    .line 2137539
    invoke-direct {v0, p0, v1, v2}, LX/EYW;-><init>(LX/EYF;LX/EYc;LX/EZQ;)V

    return-object v0
.end method

.method public static b(LX/EYF;LX/EYc;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYF;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2137540
    invoke-virtual {p0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137541
    invoke-virtual {v0}, LX/EYP;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2137542
    invoke-virtual {p1, v0}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2137543
    const/4 v0, 0x0

    .line 2137544
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LX/EYc;->h()Z

    move-result v0

    goto :goto_0
.end method

.method private c(LX/EYP;)V
    .locals 2

    .prologue
    .line 2137545
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2137546
    iget-object v1, p0, LX/EYW;->a:LX/EYF;

    if-eq v0, v1, :cond_0

    .line 2137547
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137548
    :cond_0
    return-void
.end method

.method private j()LX/EYV;
    .locals 3

    .prologue
    .line 2137549
    new-instance v0, LX/EYV;

    iget-object v1, p0, LX/EYW;->a:LX/EYF;

    invoke-direct {v0, v1}, LX/EYV;-><init>(LX/EYF;)V

    return-object v0
.end method

.method private k()LX/EYV;
    .locals 1

    .prologue
    .line 2137550
    invoke-direct {p0}, LX/EYW;->j()LX/EYV;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EYV;->d(LX/EWY;)LX/EYV;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    .line 2137551
    iget-object v0, p0, LX/EYW;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2137552
    iget-boolean v1, v0, LX/EXf;->messageSetWireFormat_:Z

    move v0, v1

    .line 2137553
    if-eqz v0, :cond_2

    .line 2137554
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    .line 2137555
    const/4 v1, 0x0

    :goto_0
    iget-object v2, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2}, LX/EZ8;->c()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2137556
    iget-object v2, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2, v1}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v2, p1}, LX/EYc;->a(Ljava/util/Map$Entry;LX/EWf;)V

    .line 2137557
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2137558
    :cond_0
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2137559
    invoke-static {v1, p1}, LX/EYc;->a(Ljava/util/Map$Entry;LX/EWf;)V

    goto :goto_1

    .line 2137560
    :cond_1
    iget-object v0, p0, LX/EYW;->c:LX/EZQ;

    invoke-virtual {v0, p1}, LX/EZQ;->b(LX/EWf;)V

    .line 2137561
    :goto_2
    return-void

    .line 2137562
    :cond_2
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    .line 2137563
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->c()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 2137564
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1, v2}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 2137565
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v3, p1}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;LX/EWf;)V

    .line 2137566
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2137567
    :cond_3
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2137568
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYP;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1, p1}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;LX/EWf;)V

    goto :goto_4

    .line 2137569
    :cond_4
    iget-object v0, p0, LX/EYW;->c:LX/EZQ;

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    goto :goto_2
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2137570
    iget-object v0, p0, LX/EYW;->a:LX/EYF;

    iget-object v1, p0, LX/EYW;->b:LX/EYc;

    invoke-static {v0, v1}, LX/EYW;->b(LX/EYF;LX/EYc;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2137509
    invoke-direct {p0, p1}, LX/EYW;->c(LX/EYP;)V

    .line 2137510
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 2137511
    iget v0, p0, LX/EYW;->d:I

    .line 2137512
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2137513
    :goto_0
    return v0

    .line 2137514
    :cond_0
    iget-object v0, p0, LX/EYW;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2137515
    iget-boolean v1, v0, LX/EXf;->messageSetWireFormat_:Z

    move v0, v1

    .line 2137516
    if-eqz v0, :cond_3

    .line 2137517
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    const/4 v1, 0x0

    .line 2137518
    move v2, v1

    .line 2137519
    :goto_1
    iget-object v3, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v3}, LX/EZ8;->c()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 2137520
    iget-object v3, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v3, v1}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-static {v3}, LX/EYc;->c(Ljava/util/Map$Entry;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2137521
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2137522
    :cond_1
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2137523
    invoke-static {v1}, LX/EYc;->c(Ljava/util/Map$Entry;)I

    move-result v1

    add-int/2addr v2, v1

    .line 2137524
    goto :goto_2

    .line 2137525
    :cond_2
    move v0, v2

    .line 2137526
    iget-object v1, p0, LX/EYW;->c:LX/EZQ;

    invoke-virtual {v1}, LX/EZQ;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 2137527
    :goto_3
    iput v0, p0, LX/EYW;->d:I

    goto :goto_0

    .line 2137528
    :cond_3
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->i()I

    move-result v0

    .line 2137529
    iget-object v1, p0, LX/EYW;->c:LX/EZQ;

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3
.end method

.method public final b(LX/EYP;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2137500
    invoke-direct {p0, p1}, LX/EYW;->c(LX/EYP;)V

    .line 2137501
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    .line 2137502
    if-nez v0, :cond_0

    .line 2137503
    invoke-virtual {p1}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2137504
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 2137505
    :cond_0
    :goto_0
    return-object v0

    .line 2137506
    :cond_1
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_2

    .line 2137507
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    goto :goto_0

    .line 2137508
    :cond_2
    invoke-virtual {p1}, LX/EYP;->p()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2137499
    iget-object v0, p0, LX/EYW;->a:LX/EYF;

    return-object v0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2137498
    iget-object v0, p0, LX/EYW;->c:LX/EZQ;

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EYW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137497
    new-instance v0, LX/EYU;

    invoke-direct {v0, p0}, LX/EYU;-><init>(LX/EYW;)V

    return-object v0
.end method

.method public final kb_()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137496
    iget-object v0, p0, LX/EYW;->b:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->f()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2137495
    invoke-direct {p0}, LX/EYW;->k()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()LX/EWU;
    .locals 1

    .prologue
    .line 2137494
    invoke-direct {p0}, LX/EYW;->j()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2137493
    invoke-direct {p0}, LX/EYW;->k()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2137492
    iget-object v0, p0, LX/EYW;->a:LX/EYF;

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    return-object v0
.end method
