.class public LX/D9C;
.super Landroid/app/TimePickerDialog;
.source ""


# instance fields
.field public final a:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field public final b:LX/D95;

.field public c:Landroid/widget/TimePicker;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V
    .locals 7

    .prologue
    .line 1969788
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/D9C;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZLX/D95;)V

    .line 1969789
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZLX/D95;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 1969790
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    .line 1969791
    iput p3, p0, LX/D9C;->d:I

    .line 1969792
    iput p4, p0, LX/D9C;->e:I

    .line 1969793
    iput-object p2, p0, LX/D9C;->a:Landroid/app/TimePickerDialog$OnTimeSetListener;

    .line 1969794
    iput-object p6, p0, LX/D9C;->b:LX/D95;

    .line 1969795
    invoke-virtual {p0, v8}, LX/D9C;->setCancelable(Z)V

    .line 1969796
    invoke-virtual {p0, v8}, LX/D9C;->setCanceledOnTouchOutside(Z)V

    .line 1969797
    if-eqz p6, :cond_0

    .line 1969798
    const v0, 0x7f082947

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/D98;

    invoke-direct {v1, p0}, LX/D98;-><init>(LX/D9C;)V

    invoke-virtual {p0, v7, v0, v1}, LX/D9C;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969799
    const v0, 0x7f082948

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/D99;

    invoke-direct {v1, p0}, LX/D99;-><init>(LX/D9C;)V

    invoke-virtual {p0, v6, v0, v1}, LX/D9C;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969800
    :goto_0
    return-void

    .line 1969801
    :cond_0
    const v0, 0x7f080016

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/D9A;

    invoke-direct {v1, p0}, LX/D9A;-><init>(LX/D9C;)V

    invoke-virtual {p0, v7, v0, v1}, LX/D9C;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969802
    const v0, 0x7f080017

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/D9B;

    invoke-direct {v1, p0}, LX/D9B;-><init>(LX/D9C;)V

    invoke-virtual {p0, v6, v0, v1}, LX/D9C;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 0

    .prologue
    .line 1969803
    iput-object p1, p0, LX/D9C;->c:Landroid/widget/TimePicker;

    .line 1969804
    iput p2, p0, LX/D9C;->d:I

    .line 1969805
    iput p3, p0, LX/D9C;->e:I

    .line 1969806
    return-void
.end method
