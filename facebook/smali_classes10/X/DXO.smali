.class public final LX/DXO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

.field public final synthetic b:LX/DXP;


# direct methods
.method public constructor <init>(LX/DXP;Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)V
    .locals 0

    .prologue
    .line 2010136
    iput-object p1, p0, LX/DXO;->b:LX/DXP;

    iput-object p2, p0, LX/DXO;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2010137
    iget-object v0, p0, LX/DXO;->b:LX/DXP;

    iget-object v0, v0, LX/DXP;->d:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083098

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2010138
    iget-object v0, p0, LX/DXO;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v0, :cond_0

    .line 2010139
    iget-object v0, p0, LX/DXO;->b:LX/DXP;

    iget-object v0, v0, LX/DXP;->f:LX/0TF;

    iget-object v1, p0, LX/DXO;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2010140
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2010141
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2010142
    if-eqz p1, :cond_0

    .line 2010143
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2010144
    if-eqz v0, :cond_0

    .line 2010145
    iget-object v0, p0, LX/DXO;->b:LX/DXP;

    iget-object v1, v0, LX/DXP;->f:LX/0TF;

    .line 2010146
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2010147
    check-cast v0, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;->a()Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2010148
    :goto_0
    return-void

    .line 2010149
    :cond_0
    iget-object v0, p0, LX/DXO;->b:LX/DXP;

    iget-object v0, v0, LX/DXP;->f:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
