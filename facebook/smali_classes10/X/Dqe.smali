.class public LX/Dqe;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1rp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1rU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2048458
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2048459
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Dqe;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    const/16 v4, 0xe5e

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v6

    check-cast v6, LX/1rp;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v0

    check-cast v0, LX/1rU;

    iput-object v3, v2, LX/Dqe;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v4, v2, LX/Dqe;->b:LX/0Ot;

    iput-object v5, v2, LX/Dqe;->c:LX/0Or;

    iput-object v6, v2, LX/Dqe;->d:LX/1rp;

    iput-object p1, v2, LX/Dqe;->e:LX/0kL;

    iput-object v0, v2, LX/Dqe;->f:LX/1rU;

    .line 2048460
    return-void
.end method
