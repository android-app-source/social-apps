.class public final enum LX/CtU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CtU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CtU;

.field public static final enum ACCEPTING_MOVE_EVENTS:LX/CtU;

.field public static final enum WAITING_FOR_DOWN:LX/CtU;

.field public static final enum WAITING_FOR_MOVES:LX/CtU;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1944835
    new-instance v0, LX/CtU;

    const-string v1, "WAITING_FOR_DOWN"

    invoke-direct {v0, v1, v2}, LX/CtU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    .line 1944836
    new-instance v0, LX/CtU;

    const-string v1, "WAITING_FOR_MOVES"

    invoke-direct {v0, v1, v3}, LX/CtU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtU;->WAITING_FOR_MOVES:LX/CtU;

    .line 1944837
    new-instance v0, LX/CtU;

    const-string v1, "ACCEPTING_MOVE_EVENTS"

    invoke-direct {v0, v1, v4}, LX/CtU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtU;->ACCEPTING_MOVE_EVENTS:LX/CtU;

    .line 1944838
    const/4 v0, 0x3

    new-array v0, v0, [LX/CtU;

    sget-object v1, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    aput-object v1, v0, v2

    sget-object v1, LX/CtU;->WAITING_FOR_MOVES:LX/CtU;

    aput-object v1, v0, v3

    sget-object v1, LX/CtU;->ACCEPTING_MOVE_EVENTS:LX/CtU;

    aput-object v1, v0, v4

    sput-object v0, LX/CtU;->$VALUES:[LX/CtU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1944834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CtU;
    .locals 1

    .prologue
    .line 1944833
    const-class v0, LX/CtU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CtU;

    return-object v0
.end method

.method public static values()[LX/CtU;
    .locals 1

    .prologue
    .line 1944832
    sget-object v0, LX/CtU;->$VALUES:[LX/CtU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CtU;

    return-object v0
.end method
