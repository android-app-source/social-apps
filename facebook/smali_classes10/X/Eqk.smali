.class public final LX/Eqk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OZ;


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V
    .locals 0

    .prologue
    .line 2171802
    iput-object p1, p0, LX/Eqk;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 2171803
    iget-object v0, p0, LX/Eqk;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2171804
    invoke-virtual {v0, p3}, LX/1OM;->getItemViewType(I)I

    move-result v1

    if-eqz v1, :cond_2

    .line 2171805
    :cond_0
    :goto_0
    move v0, v2

    .line 2171806
    if-eqz v0, :cond_1

    .line 2171807
    iget-object v0, p0, LX/Eqk;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    invoke-virtual {v0}, LX/ErB;->e()V

    .line 2171808
    :cond_1
    return-void

    .line 2171809
    :cond_2
    invoke-static {v0, p3}, LX/Er0;->g(LX/Er0;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2171810
    invoke-virtual {v1}, LX/8QK;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2171811
    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object p1

    .line 2171812
    iget-object p2, v0, LX/Er0;->b:Ljava/util/Set;

    invoke-interface {p2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    .line 2171813
    if-eqz p2, :cond_4

    .line 2171814
    iget-object p4, v0, LX/Er0;->b:Ljava/util/Set;

    invoke-interface {p4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2171815
    iget-object p1, v0, LX/Er0;->g:LX/ErW;

    sget-object p4, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {p1, p4, v3}, LX/ErW;->b(LX/ErX;I)V

    .line 2171816
    :goto_1
    iget-object p1, v0, LX/Er0;->a:LX/EqG;

    if-nez p2, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p1, v1, v2, v3}, LX/EqG;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;ZZ)V

    .line 2171817
    invoke-virtual {v0, p3}, LX/1OM;->i_(I)V

    move v2, v3

    .line 2171818
    goto :goto_0

    .line 2171819
    :cond_4
    iget-object p4, v0, LX/Er0;->b:Ljava/util/Set;

    invoke-interface {p4}, Ljava/util/Set;->size()I

    move-result p4

    iget p5, v0, LX/Er0;->d:I

    if-lt p4, p5, :cond_5

    .line 2171820
    iget-object v1, v0, LX/Er0;->f:LX/0kL;

    new-instance v3, LX/27k;

    const p1, 0x7f080be4

    invoke-direct {v3, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2171821
    goto :goto_0

    .line 2171822
    :cond_5
    iget-object p4, v0, LX/Er0;->b:Ljava/util/Set;

    invoke-interface {p4, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2171823
    iget-object p1, v0, LX/Er0;->g:LX/ErW;

    sget-object p4, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {p1, p4, v3}, LX/ErW;->a(LX/ErX;I)V

    goto :goto_1
.end method
