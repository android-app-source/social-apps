.class public final LX/E62;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/E67;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic h:LX/1Pn;

.field public final synthetic i:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;ZLX/E67;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2079460
    iput-object p1, p0, LX/E62;->i:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    iput-boolean p2, p0, LX/E62;->a:Z

    iput-object p3, p0, LX/E62;->b:LX/E67;

    iput-object p4, p0, LX/E62;->c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object p5, p0, LX/E62;->d:Ljava/lang/String;

    iput-object p6, p0, LX/E62;->e:Ljava/lang/String;

    iput-object p7, p0, LX/E62;->f:Ljava/lang/String;

    iput-object p8, p0, LX/E62;->g:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object p9, p0, LX/E62;->h:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x560b6893

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2079461
    iget-boolean v0, p0, LX/E62;->a:Z

    if-eqz v0, :cond_2

    .line 2079462
    iget-object v0, p0, LX/E62;->i:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->d:LX/7vW;

    iget-object v1, p0, LX/E62;->b:LX/E67;

    iget-object v1, v1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E62;->c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2079463
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v3, :cond_4

    .line 2079464
    :cond_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2079465
    :goto_0
    move-object v2, v3

    .line 2079466
    iget-object v3, p0, LX/E62;->d:Ljava/lang/String;

    iget-object v4, p0, LX/E62;->e:Ljava/lang/String;

    iget-object v5, p0, LX/E62;->f:Ljava/lang/String;

    iget-object v6, p0, LX/E62;->b:LX/E67;

    iget-object v6, v6, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079467
    iget-object p1, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, p1

    .line 2079468
    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2079469
    :goto_1
    if-eqz v0, :cond_1

    .line 2079470
    iget-object v1, p0, LX/E62;->i:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->g:LX/1Ck;

    new-instance v2, LX/E61;

    invoke-direct {v2, p0}, LX/E61;-><init>(LX/E62;)V

    invoke-virtual {v1, p0, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2079471
    :cond_1
    iget-object v0, p0, LX/E62;->h:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/E62;->b:LX/E67;

    iget-object v1, v1, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079472
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2079473
    iget-object v2, p0, LX/E62;->b:LX/E67;

    iget-object v2, v2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079474
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2079475
    iget-object v3, p0, LX/E62;->b:LX/E67;

    iget-object v3, v3, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079476
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v4

    .line 2079477
    invoke-interface {v3}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->JOIN_EVENT_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2079478
    const v0, -0x154ce6e9

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 2079479
    :cond_2
    iget-object v0, p0, LX/E62;->i:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionJoinEventActionPartDefinition;->e:LX/7vZ;

    iget-object v1, p0, LX/E62;->b:LX/E67;

    iget-object v1, v1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E62;->g:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2079480
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v3, :cond_5

    .line 2079481
    :cond_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2079482
    :goto_2
    move-object v2, v3

    .line 2079483
    iget-object v3, p0, LX/E62;->d:Ljava/lang/String;

    iget-object v4, p0, LX/E62;->e:Ljava/lang/String;

    iget-object v5, p0, LX/E62;->f:Ljava/lang/String;

    iget-object v6, p0, LX/E62;->b:LX/E67;

    iget-object v6, v6, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079484
    iget-object p1, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, p1

    .line 2079485
    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_2
.end method
