.class public final LX/EOk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;LX/1Pn;Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 0

    .prologue
    .line 2115185
    iput-object p1, p0, LX/EOk;->c:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    iput-object p2, p0, LX/EOk;->a:LX/1Pn;

    iput-object p3, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x6bd941f1

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2115186
    iget-object v0, p0, LX/EOk;->a:LX/1Pn;

    check-cast v0, LX/Cxe;

    iget-object v1, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v1}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2115187
    iget-object v0, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2115188
    iget-object v0, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v4

    .line 2115189
    iget-object v0, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2115190
    iget-object v0, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j()Ljava/lang/String;

    move-result-object v5

    .line 2115191
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EOk;->c:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->i:LX/EJ5;

    iget-object v1, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/EOk;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/EOk;->a:LX/1Pn;

    check-cast v6, LX/CxV;

    invoke-virtual/range {v0 .. v6}, LX/EJ5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CxV;)V

    .line 2115192
    const v0, 0x4873384

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method
