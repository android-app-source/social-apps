.class public LX/EA4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EA3;
.implements LX/1DI;


# instance fields
.field public final a:LX/0bH;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/Ch5;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/EAs;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/EB0;

.field private h:Z

.field public i:Z

.field public j:Z

.field public k:LX/E9N;

.field public l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public n:Lcom/facebook/reviews/ui/UserReviewsFragment;

.field public o:LX/1B1;

.field public p:LX/1B1;

.field public q:LX/E9P;

.field public r:LX/E9U;

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/E9Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0bH;Landroid/content/res/Resources;LX/Ch5;LX/0Or;LX/EAs;LX/0Or;LX/EB0;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "Landroid/content/res/Resources;",
            "LX/Ch5;",
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;",
            "LX/EAs;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/EB0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085270
    iput-object p1, p0, LX/EA4;->a:LX/0bH;

    .line 2085271
    iput-object p2, p0, LX/EA4;->b:Landroid/content/res/Resources;

    .line 2085272
    iput-object p3, p0, LX/EA4;->c:LX/Ch5;

    .line 2085273
    iput-object p4, p0, LX/EA4;->d:LX/0Or;

    .line 2085274
    iput-object p5, p0, LX/EA4;->e:LX/EAs;

    .line 2085275
    iput-object p6, p0, LX/EA4;->f:LX/0Or;

    .line 2085276
    iput-object p7, p0, LX/EA4;->g:LX/EB0;

    .line 2085277
    return-void
.end method

.method private h()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2085247
    iget-object v0, p0, LX/EA4;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2085248
    :goto_0
    return-void

    .line 2085249
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EA4;->h:Z

    .line 2085250
    iget-object v0, p0, LX/EA4;->e:LX/EAs;

    iget-boolean v2, p0, LX/EA4;->j:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/EA4;->b:Landroid/content/res/Resources;

    const v3, 0x7f0814fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, LX/EA4;->r:LX/E9U;

    iget-object v4, p0, LX/EA4;->o:LX/1B1;

    iget-object v5, p0, LX/EA4;->p:LX/1B1;

    move-object v6, p0

    .line 2085251
    iget-object v7, v0, LX/EAs;->d:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/E9Y;

    .line 2085252
    invoke-virtual {v7, v1}, LX/E9Y;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;)V

    .line 2085253
    iput-object v2, v7, LX/E9Y;->h:Ljava/lang/String;

    .line 2085254
    iget-object v8, v7, LX/E9Y;->b:LX/E9R;

    invoke-virtual {v8, v3}, LX/E9Q;->a(LX/E9T;)V

    .line 2085255
    iget-object v8, v7, LX/E9Y;->b:LX/E9R;

    move-object v8, v8

    .line 2085256
    invoke-virtual {v8}, LX/E9Q;->b()V

    .line 2085257
    const/4 v8, 0x3

    new-array v8, v8, [LX/0b2;

    const/4 v9, 0x0

    new-instance v10, LX/EAl;

    invoke-direct {v10, v0, v7, v3}, LX/EAl;-><init>(LX/EAs;LX/E9Y;LX/E9T;)V

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, LX/EAm;

    invoke-direct {v10, v0, v7, v3, v6}, LX/EAm;-><init>(LX/EAs;LX/E9Y;LX/E9T;LX/EA4;)V

    aput-object v10, v8, v9

    const/4 v9, 0x2

    new-instance v10, LX/EAn;

    invoke-direct {v10, v0, v7, v3}, LX/EAn;-><init>(LX/EAs;LX/E9Y;LX/E9T;)V

    aput-object v10, v8, v9

    invoke-virtual {v4, v8}, LX/1B1;->a([LX/0b2;)V

    .line 2085258
    new-instance v8, LX/EAo;

    invoke-direct {v8, v0, v7, v3}, LX/EAo;-><init>(LX/EAs;LX/E9Y;LX/E9T;)V

    invoke-virtual {v5, v8}, LX/1B1;->a(LX/0b2;)Z

    .line 2085259
    move-object v0, v7

    .line 2085260
    const/4 v1, 0x0

    .line 2085261
    iput-boolean v1, v0, LX/E9Y;->k:Z

    .line 2085262
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/EA4;->s:LX/0am;

    .line 2085263
    iget-object v1, p0, LX/EA4;->r:LX/E9U;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/E9U;->a(Ljava/util/List;)V

    .line 2085264
    iget-object v0, p0, LX/EA4;->o:LX/1B1;

    iget-object v1, p0, LX/EA4;->c:LX/Ch5;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2085265
    iget-object v0, p0, LX/EA4;->p:LX/1B1;

    iget-object v1, p0, LX/EA4;->a:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2085266
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    new-instance v1, LX/EA1;

    invoke-direct {v1, p0}, LX/EA1;-><init>(LX/EA4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/UserReviewsFragment;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2085267
    goto/16 :goto_0

    :cond_1
    move-object v2, v1

    .line 2085268
    goto :goto_1
.end method

.method public static j(LX/EA4;)V
    .locals 8

    .prologue
    .line 2085240
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EA4;->i:Z

    .line 2085241
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/UserReviewsFragment;->b()V

    .line 2085242
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2085243
    iget-object v1, p0, LX/EA4;->g:LX/EB0;

    iget-object v2, p0, LX/EA4;->m:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9Y;

    .line 2085244
    iget-object v4, v0, LX/E9Y;->i:Ljava/lang/String;

    move-object v0, v4

    .line 2085245
    iget-object v4, v1, LX/EB0;->f:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "key_load_initial_user_reviews_data"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/EAw;

    invoke-direct {v6, v1, v2, v3, v0}, LX/EAw;-><init>(LX/EB0;Ljava/lang/String;ILjava/lang/String;)V

    new-instance v7, LX/EAx;

    invoke-direct {v7, v1, p0}, LX/EAx;-><init>(LX/EB0;LX/EA3;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2085246
    :cond_0
    return-void
.end method

.method public static k(LX/EA4;)V
    .locals 2

    .prologue
    .line 2085237
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    new-instance v1, LX/EA2;

    invoke-direct {v1, p0}, LX/EA2;-><init>(LX/EA4;)V

    .line 2085238
    iget-object p0, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->f:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {p0, v1}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2085239
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2085230
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2085231
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9Y;

    const/4 v1, 0x1

    .line 2085232
    iput-boolean v1, v0, LX/E9Y;->k:Z

    .line 2085233
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9Y;

    .line 2085234
    iget-object v1, v0, LX/E9Y;->b:LX/E9R;

    move-object v0, v1

    .line 2085235
    invoke-virtual {v0}, LX/E9Q;->b()V

    .line 2085236
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 2085189
    iput-boolean v0, p0, LX/EA4;->h:Z

    .line 2085190
    iput-boolean v0, p0, LX/EA4;->i:Z

    .line 2085191
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EA4;->s:LX/0am;

    .line 2085192
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/UserReviewsFragment;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2085193
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/UserReviewsFragment;->c()V

    .line 2085194
    iget-object v0, p0, LX/EA4;->r:LX/E9U;

    .line 2085195
    iget-object v1, v0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2085196
    const v1, 0x6eb16e68

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2085197
    iget-object v0, p0, LX/EA4;->g:LX/EB0;

    invoke-virtual {v0}, LX/EB0;->a()V

    .line 2085198
    iget-boolean v0, p0, LX/EA4;->j:Z

    if-eqz v0, :cond_0

    .line 2085199
    iget-object v2, p0, LX/EA4;->e:LX/EAs;

    iget-object v3, p0, LX/EA4;->r:LX/E9U;

    iget-object v4, p0, LX/EA4;->o:LX/1B1;

    .line 2085200
    iget-object v5, v2, LX/EAs;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/E9P;

    .line 2085201
    iput-object v3, v5, LX/E9P;->d:LX/E9T;

    .line 2085202
    const/4 v6, 0x2

    new-array v6, v6, [LX/0b2;

    const/4 v7, 0x0

    new-instance v8, LX/EAj;

    invoke-direct {v8, v2, v5, v3}, LX/EAj;-><init>(LX/EAs;LX/E9P;LX/E9T;)V

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, LX/EAk;

    invoke-direct {v8, v2, v5, v3}, LX/EAk;-><init>(LX/EAs;LX/E9P;LX/E9T;)V

    aput-object v8, v6, v7

    invoke-virtual {v4, v6}, LX/1B1;->a([LX/0b2;)V

    .line 2085203
    move-object v2, v5

    .line 2085204
    iput-object v2, p0, LX/EA4;->q:LX/E9P;

    .line 2085205
    iget-object v2, p0, LX/EA4;->e:LX/EAs;

    const/4 v3, 0x0

    iget-object v4, p0, LX/EA4;->r:LX/E9U;

    iget-object v5, p0, LX/EA4;->o:LX/1B1;

    .line 2085206
    iget-object v6, v2, LX/EAs;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/E9N;

    .line 2085207
    invoke-virtual {v6, v3}, LX/E9N;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;)V

    .line 2085208
    iget-object v7, v6, LX/E9N;->a:LX/E9V;

    move-object v7, v7

    .line 2085209
    new-instance v8, LX/EAp;

    invoke-direct {v8, v2, p0}, LX/EAp;-><init>(LX/EAs;LX/EA4;)V

    .line 2085210
    invoke-static {v8}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v9

    iput-object v9, v7, LX/E9V;->b:LX/0am;

    .line 2085211
    const/4 v7, 0x2

    new-array v7, v7, [LX/0b2;

    const/4 v8, 0x0

    new-instance v9, LX/EAq;

    invoke-direct {v9, v2, v6, p0, v4}, LX/EAq;-><init>(LX/EAs;LX/E9N;LX/EA4;LX/E9T;)V

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, LX/EAr;

    invoke-direct {v9, v2, v6, p0, v4}, LX/EAr;-><init>(LX/EAs;LX/E9N;LX/EA4;LX/E9T;)V

    aput-object v9, v7, v8

    invoke-virtual {v5, v7}, LX/1B1;->a([LX/0b2;)V

    .line 2085212
    iget-object v7, v6, LX/E9N;->a:LX/E9V;

    invoke-virtual {v7, v4}, LX/E9Q;->a(LX/E9T;)V

    .line 2085213
    move-object v2, v6

    .line 2085214
    iput-object v2, p0, LX/EA4;->k:LX/E9N;

    .line 2085215
    iget-object v2, p0, LX/EA4;->k:LX/E9N;

    iget-object v3, p0, LX/EA4;->b:Landroid/content/res/Resources;

    const v4, 0x7f0814fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2085216
    iput-object v3, v2, LX/E9N;->f:Ljava/lang/String;

    .line 2085217
    iget-object v2, p0, LX/EA4;->k:LX/E9N;

    .line 2085218
    iget-object v3, v2, LX/E9N;->a:LX/E9V;

    move-object v2, v3

    .line 2085219
    invoke-virtual {v2}, LX/E9Q;->b()V

    .line 2085220
    iget-object v2, p0, LX/EA4;->r:LX/E9U;

    iget-object v3, p0, LX/EA4;->q:LX/E9P;

    iget-object v4, p0, LX/EA4;->k:LX/E9N;

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E9U;->a(Ljava/util/List;)V

    .line 2085221
    iget-object v2, p0, LX/EA4;->o:LX/1B1;

    iget-object v3, p0, LX/EA4;->c:LX/Ch5;

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b4;)V

    .line 2085222
    iget-object v2, p0, LX/EA4;->l:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2085223
    iget-object v2, p0, LX/EA4;->g:LX/EB0;

    iget-object v3, p0, LX/EA4;->l:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, LX/EA4;->m:Ljava/lang/String;

    const/16 v5, 0xa

    move-object v6, p0

    move-object v7, p0

    .line 2085224
    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v9

    move-object v8, v2

    move-object v10, v4

    move v11, v5

    move-object v12, v6

    move-object v13, v7

    invoke-static/range {v8 .. v13}, LX/EB0;->a(LX/EB0;LX/0am;Ljava/lang/String;ILX/EA4;LX/EA4;)V

    .line 2085225
    :goto_0
    return-void

    .line 2085226
    :cond_0
    invoke-direct {p0}, LX/EA4;->h()V

    goto :goto_0

    .line 2085227
    :cond_1
    iget-object v2, p0, LX/EA4;->g:LX/EB0;

    iget-object v3, p0, LX/EA4;->m:Ljava/lang/String;

    const/4 v4, 0x3

    .line 2085228
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v9

    const/4 v12, 0x0

    move-object v8, v2

    move-object v10, v3

    move v11, v4

    move-object v13, p0

    invoke-static/range {v8 .. v13}, LX/EB0;->a(LX/EB0;LX/0am;Ljava/lang/String;ILX/EA4;LX/EA4;)V

    .line 2085229
    goto :goto_0
.end method

.method public final a(I)V
    .locals 7

    .prologue
    .line 2085278
    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    .line 2085279
    iget-object v1, v0, LX/E9N;->a:LX/E9V;

    move-object v0, v1

    .line 2085280
    invoke-virtual {v0}, LX/E9Q;->b()V

    .line 2085281
    iget-object v0, p0, LX/EA4;->g:LX/EB0;

    iget-object v1, p0, LX/EA4;->m:Ljava/lang/String;

    iget-object v2, p0, LX/EA4;->k:LX/E9N;

    .line 2085282
    iget-object v3, v2, LX/E9N;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2085283
    iget-object v3, v0, LX/EB0;->f:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "key_load_more_places_to_review"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/EAu;

    invoke-direct {v5, v0, v1, p1, v2}, LX/EAu;-><init>(LX/EB0;Ljava/lang/String;ILjava/lang/String;)V

    new-instance v6, LX/EAv;

    invoke-direct {v6, v0, p0}, LX/EAv;-><init>(LX/EB0;LX/EA4;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2085284
    return-void
.end method

.method public final a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;)V
    .locals 2
    .param p1    # Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085180
    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    .line 2085181
    iget-object v1, v0, LX/E9N;->a:LX/E9V;

    move-object v0, v1

    .line 2085182
    invoke-virtual {v0}, LX/E9Q;->c()V

    .line 2085183
    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E9N;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;)V

    .line 2085184
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/UserReviewsFragment;->b()V

    .line 2085185
    iget-object v0, p0, LX/EA4;->r:LX/E9U;

    const v1, 0x31aadeac

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2085186
    iget-boolean v0, p0, LX/EA4;->h:Z

    if-nez v0, :cond_0

    .line 2085187
    invoke-direct {p0}, LX/EA4;->h()V

    .line 2085188
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel;)V
    .locals 2

    .prologue
    .line 2085168
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EA4;->i:Z

    .line 2085169
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2085170
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9Y;

    .line 2085171
    iget-object v1, v0, LX/E9Y;->b:LX/E9R;

    move-object v1, v1

    .line 2085172
    invoke-virtual {v1}, LX/E9Q;->c()V

    .line 2085173
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E9Y;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;)V

    .line 2085174
    iget-boolean v1, v0, LX/E9Y;->j:Z

    move v0, v1

    .line 2085175
    if-nez v0, :cond_0

    .line 2085176
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/UserReviewsFragment;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2085177
    :cond_0
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/UserReviewsFragment;->c()V

    .line 2085178
    invoke-virtual {p0}, LX/EA4;->f()V

    .line 2085179
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2085162
    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    .line 2085163
    iget-object v1, v0, LX/E9N;->a:LX/E9V;

    move-object v0, v1

    .line 2085164
    invoke-virtual {v0}, LX/E9Q;->c()V

    .line 2085165
    iget-boolean v0, p0, LX/EA4;->h:Z

    if-nez v0, :cond_0

    .line 2085166
    invoke-direct {p0}, LX/EA4;->h()V

    .line 2085167
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 2085154
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2085155
    iget-object v0, p0, LX/EA4;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9Y;

    .line 2085156
    iget-object v1, v0, LX/E9Y;->b:LX/E9R;

    move-object v0, v1

    .line 2085157
    invoke-virtual {v0}, LX/E9Q;->c()V

    .line 2085158
    :cond_0
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    .line 2085159
    iget-object v1, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2085160
    iget-object v1, v0, Lcom/facebook/reviews/ui/UserReviewsFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v2, 0x7f0814f9

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/facebook/reviews/ui/UserReviewsFragment$1;

    invoke-direct {v3, v0}, Lcom/facebook/reviews/ui/UserReviewsFragment$1;-><init>(Lcom/facebook/reviews/ui/UserReviewsFragment;)V

    invoke-virtual {v1, v2, p0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V

    .line 2085161
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2085145
    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    .line 2085146
    iget-object v1, v0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 2085147
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EA4;->k:LX/E9N;

    .line 2085148
    iget-boolean v1, v0, LX/E9N;->d:Z

    move v0, v1

    .line 2085149
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2085150
    :goto_0
    if-nez v0, :cond_0

    .line 2085151
    invoke-direct {p0}, LX/EA4;->l()V

    .line 2085152
    :cond_0
    return-void

    .line 2085153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kS_()V
    .locals 1

    .prologue
    .line 2085141
    iget-object v0, p0, LX/EA4;->n:Lcom/facebook/reviews/ui/UserReviewsFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/UserReviewsFragment;->c()V

    .line 2085142
    invoke-direct {p0}, LX/EA4;->l()V

    .line 2085143
    invoke-static {p0}, LX/EA4;->j(LX/EA4;)V

    .line 2085144
    return-void
.end method
