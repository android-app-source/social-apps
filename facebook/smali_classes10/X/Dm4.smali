.class public final LX/Dm4;
.super LX/Dm1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Dm1",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V
    .locals 0

    .prologue
    .line 2038853
    iput-object p1, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-direct {p0, p2, p3, p4}, LX/Dm1;-><init>(Landroid/view/View;ZLX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 3

    .prologue
    .line 2038854
    const v0, 0x7f02080b

    iget-object v1, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082b91

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/Dlw;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2038855
    invoke-static {p1}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2038856
    iput-object v0, p0, LX/Dm1;->l:Ljava/lang/Object;

    .line 2038857
    return-void
.end method

.method public bridge synthetic onClick(Landroid/view/View;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2038858
    check-cast p2, Ljava/lang/String;

    .line 2038859
    iget-object v0, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->f:LX/Dih;

    .line 2038860
    iget-object v1, v0, LX/Dih;->a:LX/0Zb;

    const-string v2, "profservices_booking_consumer_open_message_thread"

    invoke-static {v2, p2}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2038861
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2038862
    iget-object v1, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->i:LX/17Y;

    iget-object v2, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2038863
    iget-object v1, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dm4;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2038864
    return-void
.end method
