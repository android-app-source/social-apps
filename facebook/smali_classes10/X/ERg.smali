.class public final LX/ERg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ERr;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2TK;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2121290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121291
    invoke-static {p0, p1}, LX/ERg;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2121292
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 2121289
    invoke-direct {p0, p1}, LX/ERg;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a()LX/0Uq;
    .locals 1

    .prologue
    .line 2121279
    iget-object v0, p0, LX/ERg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    .line 2121280
    return-object v0
.end method

.method public static synthetic a(LX/ERg;)LX/0Uq;
    .locals 1

    .prologue
    .line 2121288
    invoke-direct {p0}, LX/ERg;->a()LX/0Uq;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/ERg;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ERg;",
            "LX/0Or",
            "<",
            "LX/ERr;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2TK;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Uq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121287
    iput-object p1, p0, LX/ERg;->a:LX/0Or;

    iput-object p2, p0, LX/ERg;->b:LX/0Or;

    iput-object p3, p0, LX/ERg;->c:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/ERg;

    const/16 v1, 0x37a9

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x12df

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x29f

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, LX/ERg;->a(LX/ERg;LX/0Or;LX/0Or;LX/0Or;)V

    return-void
.end method

.method private b()LX/2TK;
    .locals 1

    .prologue
    .line 2121285
    iget-object v0, p0, LX/ERg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2TK;

    .line 2121286
    return-object v0
.end method

.method public static synthetic b(LX/ERg;)LX/2TK;
    .locals 1

    .prologue
    .line 2121284
    invoke-direct {p0}, LX/ERg;->b()LX/2TK;

    move-result-object v0

    return-object v0
.end method

.method private c()LX/ERr;
    .locals 1

    .prologue
    .line 2121282
    iget-object v0, p0, LX/ERg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERr;

    .line 2121283
    return-object v0
.end method

.method public static synthetic c(LX/ERg;)LX/ERr;
    .locals 1

    .prologue
    .line 2121281
    invoke-direct {p0}, LX/ERg;->c()LX/ERr;

    move-result-object v0

    return-object v0
.end method
