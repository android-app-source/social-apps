.class public LX/DDJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/DDR;

.field public final d:LX/DVF;

.field public final e:LX/1g8;

.field public final f:LX/DD1;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/DDR;LX/DVF;LX/1g8;LX/DD1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975334
    iput-object p1, p0, LX/DDJ;->a:Landroid/content/res/Resources;

    .line 1975335
    iput-object p2, p0, LX/DDJ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1975336
    iput-object p3, p0, LX/DDJ;->c:LX/DDR;

    .line 1975337
    iput-object p4, p0, LX/DDJ;->d:LX/DVF;

    .line 1975338
    iput-object p5, p0, LX/DDJ;->e:LX/1g8;

    .line 1975339
    iput-object p6, p0, LX/DDJ;->f:LX/DD1;

    .line 1975340
    return-void
.end method

.method public static a(LX/0QB;)LX/DDJ;
    .locals 10

    .prologue
    .line 1975341
    const-class v1, LX/DDJ;

    monitor-enter v1

    .line 1975342
    :try_start_0
    sget-object v0, LX/DDJ;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975343
    sput-object v2, LX/DDJ;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975344
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975345
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975346
    new-instance v3, LX/DDJ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/DDR;->a(LX/0QB;)LX/DDR;

    move-result-object v6

    check-cast v6, LX/DDR;

    invoke-static {v0}, LX/Jaq;->b(LX/0QB;)LX/Jaq;

    move-result-object v7

    check-cast v7, LX/DVF;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v8

    check-cast v8, LX/1g8;

    invoke-static {v0}, LX/DD1;->b(LX/0QB;)LX/DD1;

    move-result-object v9

    check-cast v9, LX/DD1;

    invoke-direct/range {v3 .. v9}, LX/DDJ;-><init>(Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/DDR;LX/DVF;LX/1g8;LX/DD1;)V

    .line 1975347
    move-object v0, v3

    .line 1975348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
