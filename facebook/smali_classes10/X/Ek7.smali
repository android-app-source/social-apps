.class public final enum LX/Ek7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ek7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ek7;

.field public static final enum BACKGROUND_CONFIRM_START:LX/Ek7;

.field public static final enum INVALID_NUMBER:LX/Ek7;

.field public static final enum PHONE_NUMBER_ADD_ATTEMPT:LX/Ek7;

.field public static final enum QP_CHANGE_COUNTRY:LX/Ek7;

.field public static final enum QP_DISMISS:LX/Ek7;

.field public static final enum QP_HELP_CENTER:LX/Ek7;

.field public static final enum QP_IMPRESSION:LX/Ek7;

.field public static final enum VALID_NUMBER:LX/Ek7;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2163043
    new-instance v0, LX/Ek7;

    const-string v1, "QP_IMPRESSION"

    const-string v2, "qp_impression"

    invoke-direct {v0, v1, v4, v2}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->QP_IMPRESSION:LX/Ek7;

    .line 2163044
    new-instance v0, LX/Ek7;

    const-string v1, "QP_HELP_CENTER"

    const-string v2, "qp_help_center"

    invoke-direct {v0, v1, v5, v2}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->QP_HELP_CENTER:LX/Ek7;

    .line 2163045
    new-instance v0, LX/Ek7;

    const-string v1, "QP_DISMISS"

    const-string v2, "qp_dismiss"

    invoke-direct {v0, v1, v6, v2}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->QP_DISMISS:LX/Ek7;

    .line 2163046
    new-instance v0, LX/Ek7;

    const-string v1, "QP_CHANGE_COUNTRY"

    const-string v2, "qp_change_country"

    invoke-direct {v0, v1, v7, v2}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->QP_CHANGE_COUNTRY:LX/Ek7;

    .line 2163047
    new-instance v0, LX/Ek7;

    const-string v1, "PHONE_NUMBER_ADD_ATTEMPT"

    const-string v2, "phone_number_add_attempt"

    invoke-direct {v0, v1, v8, v2}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->PHONE_NUMBER_ADD_ATTEMPT:LX/Ek7;

    .line 2163048
    new-instance v0, LX/Ek7;

    const-string v1, "INVALID_NUMBER"

    const/4 v2, 0x5

    const-string v3, "invalid_number"

    invoke-direct {v0, v1, v2, v3}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->INVALID_NUMBER:LX/Ek7;

    .line 2163049
    new-instance v0, LX/Ek7;

    const-string v1, "VALID_NUMBER"

    const/4 v2, 0x6

    const-string v3, "valid_number"

    invoke-direct {v0, v1, v2, v3}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->VALID_NUMBER:LX/Ek7;

    .line 2163050
    new-instance v0, LX/Ek7;

    const-string v1, "BACKGROUND_CONFIRM_START"

    const/4 v2, 0x7

    const-string v3, "background_confirm_start"

    invoke-direct {v0, v1, v2, v3}, LX/Ek7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ek7;->BACKGROUND_CONFIRM_START:LX/Ek7;

    .line 2163051
    const/16 v0, 0x8

    new-array v0, v0, [LX/Ek7;

    sget-object v1, LX/Ek7;->QP_IMPRESSION:LX/Ek7;

    aput-object v1, v0, v4

    sget-object v1, LX/Ek7;->QP_HELP_CENTER:LX/Ek7;

    aput-object v1, v0, v5

    sget-object v1, LX/Ek7;->QP_DISMISS:LX/Ek7;

    aput-object v1, v0, v6

    sget-object v1, LX/Ek7;->QP_CHANGE_COUNTRY:LX/Ek7;

    aput-object v1, v0, v7

    sget-object v1, LX/Ek7;->PHONE_NUMBER_ADD_ATTEMPT:LX/Ek7;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Ek7;->INVALID_NUMBER:LX/Ek7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ek7;->VALID_NUMBER:LX/Ek7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ek7;->BACKGROUND_CONFIRM_START:LX/Ek7;

    aput-object v2, v0, v1

    sput-object v0, LX/Ek7;->$VALUES:[LX/Ek7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2163052
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2163053
    iput-object p3, p0, LX/Ek7;->mEventName:Ljava/lang/String;

    .line 2163054
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ek7;
    .locals 1

    .prologue
    .line 2163055
    const-class v0, LX/Ek7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ek7;

    return-object v0
.end method

.method public static values()[LX/Ek7;
    .locals 1

    .prologue
    .line 2163056
    sget-object v0, LX/Ek7;->$VALUES:[LX/Ek7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ek7;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2163057
    iget-object v0, p0, LX/Ek7;->mEventName:Ljava/lang/String;

    return-object v0
.end method
