.class public LX/EVj;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;
.implements LX/1a7;


# instance fields
.field public a:Z

.field private b:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128912
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EVj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128913
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128908
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128909
    const v0, 0x7f0315cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128910
    const v0, 0x7f0d3125

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/EVj;->b:Landroid/widget/LinearLayout;

    .line 2128911
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2128907
    iget-boolean v0, p0, LX/EVj;->a:Z

    return v0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2128905
    iget-object v0, p0, LX/EVj;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2128906
    return-void
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2128904
    const/4 v0, 0x1

    return v0
.end method

.method public getContainer()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2128903
    iget-object v0, p0, LX/EVj;->b:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x57a2b0a7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128893
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2128894
    const/4 v1, 0x1

    .line 2128895
    iput-boolean v1, p0, LX/EVj;->a:Z

    .line 2128896
    const/16 v1, 0x2d

    const v2, -0x874471c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5ce83c0d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128899
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2128900
    const/4 v1, 0x0

    .line 2128901
    iput-boolean v1, p0, LX/EVj;->a:Z

    .line 2128902
    const/16 v1, 0x2d

    const v2, -0x70d122e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 2128897
    iget-object v0, p0, LX/EVj;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2128898
    return-void
.end method
