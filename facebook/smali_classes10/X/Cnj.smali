.class public LX/Cnj;
.super LX/CnT;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/InlinePageLikeCtaBlockView;",
        "LX/Clx;",
        ">;",
        "LX/02k;"
    }
.end annotation


# instance fields
.field public d:LX/CjA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalPageLiker;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;)V
    .locals 6

    .prologue
    .line 1934302
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934303
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Cnj;

    invoke-static {v0}, LX/CjA;->b(LX/0QB;)LX/CjA;

    move-result-object v3

    check-cast v3, LX/CjA;

    const/16 v4, 0x12b1

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3225

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p1, 0x3216

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v0

    check-cast v0, LX/Chi;

    iput-object v3, v2, LX/Cnj;->d:LX/CjA;

    iput-object v4, v2, LX/Cnj;->e:LX/0Ot;

    iput-object v5, v2, LX/Cnj;->f:LX/0Ot;

    iput-object p1, v2, LX/Cnj;->g:LX/0Ot;

    iput-object v0, v2, LX/Cnj;->h:LX/Chi;

    .line 1934304
    return-void
.end method

.method public static a$redex0(LX/Cnj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1934305
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1934306
    const-string v0, "is_page_liked"

    iget-boolean v2, p0, LX/Cnj;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1934307
    iget-object v0, p0, LX/Cnj;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    invoke-virtual {v0, p1, v1}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1934308
    return-void
.end method

.method public static b(LX/Cnj;)V
    .locals 2

    .prologue
    .line 1934309
    iget-boolean v0, p0, LX/Cnj;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Cnj;->i:Z

    .line 1934310
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934311
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-boolean v1, p0, LX/Cnj;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->a(Ljava/lang/Boolean;)V

    .line 1934312
    return-void

    .line 1934313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 8

    .prologue
    .line 1934314
    check-cast p1, LX/Clx;

    .line 1934315
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934316
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    const/4 v3, 0x0

    .line 1934317
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934318
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1934319
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1934320
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1934321
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934322
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1934323
    const-string v2, ""

    invoke-virtual {v1, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934324
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934325
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1934326
    const-string v2, ""

    invoke-virtual {v1, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934327
    invoke-interface {p1}, LX/Clx;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v0

    iput-object v0, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    .line 1934328
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934329
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-object v1, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1934330
    if-eqz v1, :cond_2

    .line 1934331
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934332
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934333
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-object v1, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->et_()Ljava/lang/String;

    move-result-object v1

    .line 1934334
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934335
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1934336
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934337
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934338
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1934339
    const v3, 0x7f081c7a

    invoke-virtual {v2, v3}, LX/CtG;->setText(I)V

    .line 1934340
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934341
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-object v1, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;->a()I

    move-result v2

    iget-object v3, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->eu_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$PageLikersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$PageLikersModel;->a()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->a(LX/0Px;II)V

    .line 1934342
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934343
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-object v1, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel;->a()I

    iget-object v2, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->eu_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$PageLikersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$PageLikersModel;->a()I

    const/4 v4, 0x0

    .line 1934344
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1934345
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1934346
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    .line 1934347
    new-instance v7, LX/6UY;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel$ProfilePicture32Model;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel$ProfilePicture32Model;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v7, v2}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1934348
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1934349
    :cond_0
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1934350
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1934351
    :cond_1
    iget-object v0, p0, LX/Cnj;->j:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/Cnj;->i:Z

    .line 1934352
    const-string v0, "android_native_article_page_like_cta_view"

    invoke-static {p0, v0}, LX/Cnj;->a$redex0(LX/Cnj;Ljava/lang/String;)V

    .line 1934353
    iget-object v0, p0, LX/Cnj;->d:LX/CjA;

    invoke-interface {p1}, LX/Clx;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cnj;->h:LX/Chi;

    .line 1934354
    iget-object v3, v2, LX/Chi;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1934355
    invoke-virtual {v0, v1, v2}, LX/CjA;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934356
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934357
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    iget-boolean v1, p0, LX/Cnj;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->a(Ljava/lang/Boolean;)V

    .line 1934358
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934359
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    new-instance v1, LX/Cni;

    invoke-direct {v1, p0, p1}, LX/Cni;-><init>(LX/Cnj;LX/Clx;)V

    .line 1934360
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934361
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934362
    return-void

    .line 1934363
    :cond_2
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_0
.end method
