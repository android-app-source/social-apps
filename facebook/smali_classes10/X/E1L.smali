.class public final LX/E1L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;)V
    .locals 0

    .prologue
    .line 2069713
    iput-object p1, p0, LX/E1L;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x245135e7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2069714
    iget-object v1, p0, LX/E1L;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    .line 2069715
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v3

    sget-object p0, LX/9jG;->PLACE_TIPS_EMPLOYEE_SETTINGS:LX/9jG;

    .line 2069716
    iput-object p0, v3, LX/9jF;->q:LX/9jG;

    .line 2069717
    move-object v3, v3

    .line 2069718
    invoke-virtual {v3}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object p0

    .line 2069719
    iget-object v3, v1, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object p0

    const/4 p1, 0x1

    invoke-interface {v3, p0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2069720
    const v1, -0x4184d998

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
