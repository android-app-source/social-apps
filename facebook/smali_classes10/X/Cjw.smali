.class public LX/Cjw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cju;


# static fields
.field public static final a:[I

.field private static final b:[F

.field public static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/16 v5, 0x10

    const/16 v4, 0xd

    const/16 v3, 0xb

    const/4 v2, 0x3

    .line 1930037
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/Cjw;->a:[I

    .line 1930038
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, LX/Cjw;->b:[F

    .line 1930039
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/Cjw;->c:Ljava/util/Map;

    .line 1930040
    const v0, 0x7f0d011b

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930041
    const v0, 0x7f0d011c

    new-array v1, v2, [I

    fill-array-data v1, :array_3

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930042
    const v0, 0x7f0d011d

    new-array v1, v2, [I

    fill-array-data v1, :array_4

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930043
    const v0, 0x7f0d011e

    new-array v1, v2, [I

    fill-array-data v1, :array_5

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930044
    const v0, 0x7f0d011f

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930045
    const v0, 0x7f0d012a

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930046
    const v0, 0x7f0d013c

    new-array v1, v2, [I

    fill-array-data v1, :array_8

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930047
    const v0, 0x7f0d012b

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930048
    const v0, 0x7f0d013d

    new-array v1, v2, [I

    fill-array-data v1, :array_a

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930049
    const v0, 0x7f0d012c

    new-array v1, v2, [I

    fill-array-data v1, :array_b

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930050
    const v0, 0x7f0d013e

    new-array v1, v2, [I

    fill-array-data v1, :array_c

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930051
    const v0, 0x7f0d012d

    new-array v1, v2, [I

    fill-array-data v1, :array_d

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930052
    const v0, 0x7f0d013f

    new-array v1, v2, [I

    fill-array-data v1, :array_e

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930053
    const v0, 0x7f0d012e

    new-array v1, v2, [I

    fill-array-data v1, :array_f

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930054
    const v0, 0x7f0d0140

    new-array v1, v2, [I

    fill-array-data v1, :array_10

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930055
    const v0, 0x7f0d012f

    new-array v1, v2, [I

    fill-array-data v1, :array_11

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930056
    const v0, 0x7f0d0141

    new-array v1, v2, [I

    fill-array-data v1, :array_12

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930057
    const v0, 0x7f0d0130

    new-array v1, v2, [I

    fill-array-data v1, :array_13

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930058
    const v0, 0x7f0d0142

    new-array v1, v2, [I

    fill-array-data v1, :array_14

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930059
    const v0, 0x7f0d0131

    new-array v1, v2, [I

    fill-array-data v1, :array_15

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930060
    const v0, 0x7f0d0143

    new-array v1, v2, [I

    fill-array-data v1, :array_16

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930061
    const v0, 0x7f0d0133

    new-array v1, v2, [I

    fill-array-data v1, :array_17

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930062
    const v0, 0x7f0d0145

    new-array v1, v2, [I

    fill-array-data v1, :array_18

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930063
    const v0, 0x7f0d0135

    new-array v1, v2, [I

    fill-array-data v1, :array_19

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930064
    const v0, 0x7f0d0147

    new-array v1, v2, [I

    fill-array-data v1, :array_1a

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930065
    const v0, 0x7f0d0136

    new-array v1, v2, [I

    fill-array-data v1, :array_1b

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930066
    const v0, 0x7f0d0148

    new-array v1, v2, [I

    fill-array-data v1, :array_1c

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930067
    const v0, 0x7f0d0137

    new-array v1, v2, [I

    fill-array-data v1, :array_1d

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930068
    const v0, 0x7f0d0149

    new-array v1, v2, [I

    fill-array-data v1, :array_1e

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930069
    const v0, 0x7f0d0138

    new-array v1, v2, [I

    fill-array-data v1, :array_1f

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930070
    const v0, 0x7f0d014a

    new-array v1, v2, [I

    fill-array-data v1, :array_20

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930071
    const v0, 0x7f0d0139

    new-array v1, v2, [I

    fill-array-data v1, :array_21

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930072
    const v0, 0x7f0d014b

    new-array v1, v2, [I

    fill-array-data v1, :array_22

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930073
    const v0, 0x7f0d013a

    new-array v1, v2, [I

    fill-array-data v1, :array_23

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930074
    const v0, 0x7f0d014c

    new-array v1, v2, [I

    fill-array-data v1, :array_24

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930075
    const v0, 0x7f0d013b

    new-array v1, v2, [I

    fill-array-data v1, :array_25

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930076
    const v0, 0x7f0d014d

    new-array v1, v2, [I

    fill-array-data v1, :array_26

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930077
    const v0, 0x7f0d0132

    new-array v1, v2, [I

    fill-array-data v1, :array_27

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930078
    const v0, 0x7f0d0144

    new-array v1, v2, [I

    fill-array-data v1, :array_28

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930079
    const v0, 0x7f0d0134

    new-array v1, v2, [I

    fill-array-data v1, :array_29

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930080
    const v0, 0x7f0d0146

    new-array v1, v2, [I

    fill-array-data v1, :array_2a

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930081
    const v0, 0x7f0d014e

    new-array v1, v2, [I

    fill-array-data v1, :array_2b

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930082
    const v0, 0x7f0d014f

    new-array v1, v2, [I

    fill-array-data v1, :array_2c

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930083
    const v0, 0x7f0d0150

    new-array v1, v2, [I

    fill-array-data v1, :array_2d

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930084
    const v0, 0x7f0d0151

    new-array v1, v2, [I

    fill-array-data v1, :array_2e

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930085
    const v0, 0x7f0d0152

    new-array v1, v2, [I

    fill-array-data v1, :array_2f

    invoke-static {v0, v1}, LX/Cjw;->a(I[I)V

    .line 1930086
    const v0, 0x7f0d0153

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930087
    const v0, 0x7f0d0154

    const/16 v1, 0xa

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930088
    const v0, 0x7f0d0155

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930089
    const v0, 0x7f0d0156

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930090
    const v0, 0x7f0d0157

    const/16 v1, 0x8

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930091
    const v0, 0x7f0d0158

    const/16 v1, 0xe

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930092
    const v0, 0x7f0d0159

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930093
    const v0, 0x7f0d015a

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930094
    const v0, 0x7f0d015b

    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930095
    const v0, 0x7f0d015c

    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930096
    const v0, 0x7f0d015d

    invoke-static {v0, v4}, LX/Cjw;->a(II)V

    .line 1930097
    const v0, 0x7f0d015e

    invoke-static {v0, v4}, LX/Cjw;->a(II)V

    .line 1930098
    const v0, 0x7f0d015f

    invoke-static {v0, v4}, LX/Cjw;->a(II)V

    .line 1930099
    const v0, 0x7f0d0160

    invoke-static {v0, v4}, LX/Cjw;->a(II)V

    .line 1930100
    const v0, 0x7f0d0161

    const/16 v1, 0x11

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930101
    const v0, 0x7f0d0162

    invoke-static {v0, v5}, LX/Cjw;->a(II)V

    .line 1930102
    const v0, 0x7f0d0163

    invoke-static {v0, v4}, LX/Cjw;->a(II)V

    .line 1930103
    const v0, 0x7f0d0164

    const/16 v1, 0x20

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930104
    const v0, 0x7f0d0165

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930105
    const v0, 0x7f0d0166

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930106
    const v0, 0x7f0d0167

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930107
    const v0, 0x7f0d0168

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930108
    const v0, 0x7f0d0169

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930109
    const v0, 0x7f0d016a

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930110
    const v0, 0x7f0d016b

    const/16 v1, 0xf

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930111
    const v0, 0x7f0d016c

    invoke-static {v0, v3}, LX/Cjw;->a(II)V

    .line 1930112
    const v0, 0x7f0d016d

    invoke-static {v0, v6}, LX/Cjw;->a(II)V

    .line 1930113
    const v0, 0x7f0d016e

    invoke-static {v0, v6}, LX/Cjw;->a(II)V

    .line 1930114
    const v0, 0x7f0d016f

    const/16 v1, 0x8

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930115
    const v0, 0x7f0d0170

    invoke-static {v0, v5}, LX/Cjw;->a(II)V

    .line 1930116
    const v0, 0x7f0d0171

    invoke-static {v0, v5}, LX/Cjw;->a(II)V

    .line 1930117
    const v0, 0x7f0d0172

    const/16 v1, 0xa

    invoke-static {v0, v1}, LX/Cjw;->a(II)V

    .line 1930118
    const v0, 0x7f0d0173

    invoke-static {v0, v2}, LX/Cjw;->a(II)V

    .line 1930119
    return-void

    :array_0
    .array-data 4
        0x0
        0x190
        0x300
    .end array-data

    .line 1930120
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3fc00000    # 1.5f
    .end array-data

    .line 1930121
    :array_2
    .array-data 4
        0x8
        0x9
        0xc
    .end array-data

    .line 1930122
    :array_3
    .array-data 4
        0x10
        0x12
        0x18
    .end array-data

    .line 1930123
    :array_4
    .array-data 4
        0x17
        0x1a
        0x20
    .end array-data

    .line 1930124
    :array_5
    .array-data 4
        0x1f
        0x23
        0x30
    .end array-data

    .line 1930125
    :array_6
    .array-data 4
        0x2f
        0x35
        0x48
    .end array-data

    .line 1930126
    :array_7
    .array-data 4
        0x11
        0x11
        0x14
    .end array-data

    .line 1930127
    :array_8
    .array-data 4
        0x18
        0x1b
        0x1e
    .end array-data

    .line 1930128
    :array_9
    .array-data 4
        0xc
        0xe
        0x10
    .end array-data

    .line 1930129
    :array_a
    .array-data 4
        0xe
        0x11
        0x12
    .end array-data

    .line 1930130
    :array_b
    .array-data 4
        0x1d
        0x1b
        0x24
    .end array-data

    .line 1930131
    :array_c
    .array-data 4
        0x23
        0x23
        0x30
    .end array-data

    .line 1930132
    :array_d
    .array-data 4
        0x13
        0x14
        0x14
    .end array-data

    .line 1930133
    :array_e
    .array-data 4
        0x18
        0x1b
        0x1e
    .end array-data

    .line 1930134
    :array_f
    .array-data 4
        0x17
        0x15
        0x1c
    .end array-data

    .line 1930135
    :array_10
    .array-data 4
        0x1b
        0x1c
        0x24
    .end array-data

    .line 1930136
    :array_11
    .array-data 4
        0x13
        0x14
        0x14
    .end array-data

    .line 1930137
    :array_12
    .array-data 4
        0x18
        0x1b
        0x1e
    .end array-data

    .line 1930138
    :array_13
    .array-data 4
        0x11
        0x11
        0x14
    .end array-data

    .line 1930139
    :array_14
    .array-data 4
        0x18
        0x1b
        0x1e
    .end array-data

    .line 1930140
    :array_15
    .array-data 4
        0x17
        0x15
        0x1c
    .end array-data

    .line 1930141
    :array_16
    .array-data 4
        0x20
        0x1f
        0x28
    .end array-data

    .line 1930142
    :array_17
    .array-data 4
        0xa
        0xb
        0xc
    .end array-data

    .line 1930143
    :array_18
    .array-data 4
        0xe
        0x10
        0x10
    .end array-data

    .line 1930144
    :array_19
    .array-data 4
        0xc
        0xe
        0xe
    .end array-data

    .line 1930145
    :array_1a
    .array-data 4
        0x10
        0x14
        0x14
    .end array-data

    .line 1930146
    :array_1b
    .array-data 4
        0x11
        0x11
        0x14
    .end array-data

    .line 1930147
    :array_1c
    .array-data 4
        0x18
        0x1b
        0x1e
    .end array-data

    .line 1930148
    :array_1d
    .array-data 4
        0x13
        0x14
        0x18
    .end array-data

    .line 1930149
    :array_1e
    .array-data 4
        0x16
        0x19
        0x1e
    .end array-data

    .line 1930150
    :array_1f
    .array-data 4
        0x1b
        0x1a
        0x20
    .end array-data

    .line 1930151
    :array_20
    .array-data 4
        0x1f
        0x20
        0x2c
    .end array-data

    .line 1930152
    :array_21
    .array-data 4
        0xf
        0x10
        0x10
    .end array-data

    .line 1930153
    :array_22
    .array-data 4
        0x15
        0x15
        0x18
    .end array-data

    .line 1930154
    :array_23
    .array-data 4
        0xd
        0xd
        0x10
    .end array-data

    .line 1930155
    :array_24
    .array-data 4
        0x11
        0x13
        0x16
    .end array-data

    .line 1930156
    :array_25
    .array-data 4
        0xf
        0x10
        0x12
    .end array-data

    .line 1930157
    :array_26
    .array-data 4
        0x12
        0x12
        0x16
    .end array-data

    .line 1930158
    :array_27
    .array-data 4
        0xc
        0xe
        0x10
    .end array-data

    .line 1930159
    :array_28
    .array-data 4
        0xe
        0x10
        0x14
    .end array-data

    .line 1930160
    :array_29
    .array-data 4
        0xc
        0xe
        0x10
    .end array-data

    .line 1930161
    :array_2a
    .array-data 4
        0x10
        0x14
        0x18
    .end array-data

    .line 1930162
    :array_2b
    .array-data 4
        0x11
        0x13
        0x1b
    .end array-data

    .line 1930163
    :array_2c
    .array-data 4
        0xe
        0x11
        0x17
    .end array-data

    .line 1930164
    :array_2d
    .array-data 4
        0xc
        0xe
        0x13
    .end array-data

    .line 1930165
    :array_2e
    .array-data 4
        0x14
        0x14
        0x20
    .end array-data

    .line 1930166
    :array_2f
    .array-data 4
        0x12
        0x12
        0x1d
    .end array-data
.end method

.method public constructor <init>(LX/0hB;)V
    .locals 1

    .prologue
    .line 1930032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930033
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cjw;->d:Ljava/util/Map;

    .line 1930034
    iput-object p1, p0, LX/Cjw;->e:LX/0hB;

    .line 1930035
    invoke-virtual {p0}, LX/Cjw;->a()V

    .line 1930036
    return-void
.end method

.method public static a(IF)F
    .locals 2

    .prologue
    .line 1930022
    const/high16 v0, 0x43a00000    # 320.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 1930023
    const/high16 v0, 0x41880000    # 17.0f

    .line 1930024
    :goto_0
    return v0

    .line 1930025
    :cond_0
    const/high16 v0, 0x43c80000    # 400.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 1930026
    const/high16 v0, 0x41a00000    # 20.0f

    goto :goto_0

    .line 1930027
    :cond_1
    const/high16 v0, 0x44160000    # 600.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 1930028
    const/high16 v0, 0x41a80000    # 21.0f

    goto :goto_0

    .line 1930029
    :cond_2
    const v0, 0x442d8000    # 694.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 1930030
    const/high16 v0, 0x42000000    # 32.0f

    goto :goto_0

    .line 1930031
    :cond_3
    int-to-float v0, p0

    sub-float v0, p1, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private static a(II)V
    .locals 4

    .prologue
    .line 1930016
    sget-object v0, LX/Cjw;->a:[I

    array-length v0, v0

    new-array v1, v0, [I

    .line 1930017
    const/4 v0, 0x0

    :goto_0
    sget-object v2, LX/Cjw;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1930018
    int-to-float v2, p1

    sget-object v3, LX/Cjw;->b:[F

    aget v3, v3, v0

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v0

    .line 1930019
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1930020
    :cond_0
    sget-object v0, LX/Cjw;->c:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930021
    return-void
.end method

.method private static varargs a(I[I)V
    .locals 2

    .prologue
    .line 1930014
    sget-object v0, LX/Cjw;->c:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930015
    return-void
.end method

.method public static a(LX/Cjw;IIFF)V
    .locals 3

    .prologue
    .line 1930010
    invoke-static {p2, p3}, LX/Cjw;->a(IF)F

    move-result v0

    .line 1930011
    mul-float/2addr v0, p4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 1930012
    iget-object v1, p0, LX/Cjw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930013
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1929959
    iget-object v0, p0, LX/Cjw;->e:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LX/Cjw;->e:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    div-float/2addr v0, v1

    .line 1929960
    iget-object v1, p0, LX/Cjw;->e:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    .line 1929961
    const/4 v2, 0x1

    :goto_0
    sget-object v3, LX/Cjw;->a:[I

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 1929962
    sget-object v3, LX/Cjw;->a:[I

    aget v3, v3, v2

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_2

    .line 1929963
    add-int/lit8 v2, v2, -0x1

    .line 1929964
    :goto_1
    move v4, v2

    .line 1929965
    sget-object v2, LX/Cjw;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1929966
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 1929967
    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    aget v3, v3, v6

    .line 1929968
    int-to-float v3, v3

    mul-float/2addr v3, v1

    .line 1929969
    iget-object v6, p0, LX/Cjw;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v6, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1929970
    :cond_0
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v3, 0x7f0d011b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 1929971
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v4, 0x7f0d011d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 1929972
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v5, 0x7f0d011e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 1929973
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v6, 0x7f0d011f

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1929974
    iget-object v6, p0, LX/Cjw;->d:Ljava/util/Map;

    const v7, 0x7f0d0174

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929975
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v6, 0x7f0d0175

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    add-float/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929976
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v5, 0x7f0d0176

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929977
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v4, 0x7f0d0177

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929978
    const/16 v2, 0x276

    invoke-static {v2, v0}, LX/Cjw;->a(IF)F

    move-result v2

    .line 1929979
    const/high16 v3, 0x43a00000    # 320.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_4

    .line 1929980
    const/high16 v3, 0x41880000    # 17.0f

    .line 1929981
    :goto_3
    move v3, v3

    .line 1929982
    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    .line 1929983
    mul-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    .line 1929984
    iget-object v4, p0, LX/Cjw;->d:Ljava/util/Map;

    const v5, 0x7f0d0120

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929985
    iget-object v4, p0, LX/Cjw;->d:Ljava/util/Map;

    const v5, 0x7f0d0121

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929986
    iget-object v4, p0, LX/Cjw;->d:Ljava/util/Map;

    const v5, 0x7f0d0122

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929987
    iget-object v2, p0, LX/Cjw;->d:Ljava/util/Map;

    const v4, 0x7f0d0123

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929988
    sget-object v2, LX/8bZ;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8bX;

    .line 1929989
    iget v4, v2, LX/8bX;->a:I

    iget v2, v2, LX/8bX;->b:I

    invoke-static {p0, v4, v2, v0, v1}, LX/Cjw;->a(LX/Cjw;IIFF)V

    goto :goto_4

    .line 1929990
    :cond_1
    const v2, 0x7f0d0128

    const/16 v3, 0x300

    invoke-static {p0, v2, v3, v0, v1}, LX/Cjw;->a(LX/Cjw;IIFF)V

    .line 1929991
    const v2, 0x7f0d0127

    const/16 v3, 0x190

    invoke-static {p0, v2, v3, v0, v1}, LX/Cjw;->a(LX/Cjw;IIFF)V

    .line 1929992
    return-void

    .line 1929993
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1929994
    :cond_3
    sget-object v2, LX/Cjw;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_1

    .line 1929995
    :cond_4
    const/high16 v3, 0x43c80000    # 400.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_5

    .line 1929996
    const/high16 v3, 0x41a00000    # 20.0f

    goto/16 :goto_3

    .line 1929997
    :cond_5
    const/high16 v3, 0x44160000    # 600.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_6

    .line 1929998
    const/high16 v3, 0x41a80000    # 21.0f

    goto/16 :goto_3

    .line 1929999
    :cond_6
    const v3, 0x442d8000    # 694.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_7

    .line 1930000
    const/high16 v3, 0x42000000    # 32.0f

    goto/16 :goto_3

    .line 1930001
    :cond_7
    const/high16 v3, 0x42280000    # 42.0f

    goto/16 :goto_3
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 1930009
    iget-object v0, p0, LX/Cjw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(I)F
    .locals 3

    .prologue
    .line 1930003
    if-nez p1, :cond_0

    .line 1930004
    const/4 v0, 0x0

    .line 1930005
    :goto_0
    return v0

    .line 1930006
    :cond_0
    invoke-virtual {p0, p1}, LX/Cjw;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1930007
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Metric id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1930008
    :cond_1
    iget-object v0, p0, LX/Cjw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1930002
    invoke-virtual {p0, p1}, LX/Cjw;->b(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method
