.class public final LX/EIP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EHM;


# instance fields
.field public final synthetic a:LX/EIU;


# direct methods
.method public constructor <init>(LX/EIU;)V
    .locals 0

    .prologue
    .line 2101525
    iput-object p1, p0, LX/EIP;->a:LX/EIU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2101523
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->e()V

    .line 2101524
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2101512
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101513
    iget-boolean v1, v0, LX/EDx;->bZ:Z

    move v0, v1

    .line 2101514
    if-eqz v0, :cond_0

    .line 2101515
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, p1}, LX/EDx;->a(Z)V

    .line 2101516
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2101517
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    iget-object v1, p0, LX/EIP;->a:LX/EIU;

    iget-object v1, v1, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->bo()LX/EGp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setModeAndShow(LX/EGp;)V

    .line 2101518
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    invoke-static {v0}, LX/EIU;->I(LX/EIU;)V

    .line 2101519
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->a()V

    .line 2101520
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    const/16 v1, 0x1388

    invoke-static {v0, v1}, LX/EIU;->d(LX/EIU;I)V

    .line 2101521
    :goto_0
    return-void

    .line 2101522
    :cond_0
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->e()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2101509
    iget-object v0, p0, LX/EIP;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101510
    iput-boolean p1, v0, LX/EDx;->ai:Z

    .line 2101511
    return-void
.end method
