.class public final LX/Cu5;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/CuA;


# direct methods
.method public constructor <init>(LX/CuA;)V
    .locals 0

    .prologue
    .line 1945962
    iput-object p1, p0, LX/Cu5;->a:LX/CuA;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 5

    .prologue
    .line 1945963
    invoke-super {p0, p1}, LX/0xh;->a(LX/0wd;)V

    .line 1945964
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1945965
    iput v1, v0, LX/CuA;->m:F

    .line 1945966
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    iget-object v1, p0, LX/Cu5;->a:LX/CuA;

    .line 1945967
    iget-object v2, v1, LX/Cts;->a:LX/Ctg;

    move-object v1, v2

    .line 1945968
    invoke-interface {v1}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v1

    .line 1945969
    if-eqz v1, :cond_0

    .line 1945970
    invoke-virtual {v0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {v1, v2}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v2

    .line 1945971
    invoke-virtual {v0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v3

    invoke-static {v1, v3}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v3

    .line 1945972
    invoke-virtual {v2}, LX/CrW;->e()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 1945973
    invoke-virtual {v3}, LX/CrW;->f()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 1945974
    new-instance p1, LX/CrW;

    invoke-direct {p1, v2, v4, v2, v4}, LX/CrW;-><init>(IIII)V

    .line 1945975
    iget v2, v0, LX/CuA;->m:F

    invoke-virtual {v3, p1, v2}, LX/CrW;->a(LX/CqY;F)LX/CqY;

    .line 1945976
    :cond_0
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-virtual {v0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1945977
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    .line 1945978
    iget-object v1, v0, LX/Cts;->a:LX/Ctg;

    move-object v0, v1

    .line 1945979
    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1945980
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 1945981
    invoke-super {p0, p1}, LX/0xh;->b(LX/0wd;)V

    .line 1945982
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1945983
    iput v1, v0, LX/CuA;->m:F

    .line 1945984
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    iget v0, v0, LX/CuA;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1945985
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-static {v0}, LX/CuA;->s(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-static {v0}, LX/CuA;->v(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945986
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    .line 1945987
    iget-object v1, v0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    move-object v0, v1

    .line 1945988
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/3BP;->setVisibility(I)V

    .line 1945989
    :cond_0
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-static {v0}, LX/CuA;->u(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945990
    iget-object v0, p0, LX/Cu5;->a:LX/CuA;

    invoke-static {v0}, LX/CuA;->k(LX/CuA;)V

    .line 1945991
    :cond_1
    return-void
.end method
