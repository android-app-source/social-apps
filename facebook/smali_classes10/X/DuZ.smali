.class public final enum LX/DuZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DuZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DuZ;

.field public static final enum FAILURE:LX/DuZ;

.field public static final enum INTRODUCTION:LX/DuZ;

.field public static final enum PENDING_MANUAL_REVIEW:LX/DuZ;

.field public static final enum REQUEST_CC_CVV:LX/DuZ;

.field public static final enum REQUEST_CC_FIRST_SIX:LX/DuZ;

.field public static final enum REQUEST_LEGAL_NAME:LX/DuZ;

.field public static final enum REQUEST_SSN_LAST_FOUR:LX/DuZ;

.field public static final enum SUCCESS:LX/DuZ;

.field public static final enum UNKNOWN_SCREEN:LX/DuZ;


# instance fields
.field public final isTerminal:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2057317
    new-instance v0, LX/DuZ;

    const-string v1, "INTRODUCTION"

    invoke-direct {v0, v1, v3, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->INTRODUCTION:LX/DuZ;

    .line 2057318
    new-instance v0, LX/DuZ;

    const-string v1, "REQUEST_CC_CVV"

    invoke-direct {v0, v1, v4, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->REQUEST_CC_CVV:LX/DuZ;

    .line 2057319
    new-instance v0, LX/DuZ;

    const-string v1, "REQUEST_CC_FIRST_SIX"

    invoke-direct {v0, v1, v5, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->REQUEST_CC_FIRST_SIX:LX/DuZ;

    .line 2057320
    new-instance v0, LX/DuZ;

    const-string v1, "REQUEST_LEGAL_NAME"

    invoke-direct {v0, v1, v6, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->REQUEST_LEGAL_NAME:LX/DuZ;

    .line 2057321
    new-instance v0, LX/DuZ;

    const-string v1, "REQUEST_SSN_LAST_FOUR"

    invoke-direct {v0, v1, v7, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->REQUEST_SSN_LAST_FOUR:LX/DuZ;

    .line 2057322
    new-instance v0, LX/DuZ;

    const-string v1, "PENDING_MANUAL_REVIEW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->PENDING_MANUAL_REVIEW:LX/DuZ;

    .line 2057323
    new-instance v0, LX/DuZ;

    const-string v1, "FAILURE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->FAILURE:LX/DuZ;

    .line 2057324
    new-instance v0, LX/DuZ;

    const-string v1, "SUCCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->SUCCESS:LX/DuZ;

    .line 2057325
    new-instance v0, LX/DuZ;

    const-string v1, "UNKNOWN_SCREEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/DuZ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DuZ;->UNKNOWN_SCREEN:LX/DuZ;

    .line 2057326
    const/16 v0, 0x9

    new-array v0, v0, [LX/DuZ;

    sget-object v1, LX/DuZ;->INTRODUCTION:LX/DuZ;

    aput-object v1, v0, v3

    sget-object v1, LX/DuZ;->REQUEST_CC_CVV:LX/DuZ;

    aput-object v1, v0, v4

    sget-object v1, LX/DuZ;->REQUEST_CC_FIRST_SIX:LX/DuZ;

    aput-object v1, v0, v5

    sget-object v1, LX/DuZ;->REQUEST_LEGAL_NAME:LX/DuZ;

    aput-object v1, v0, v6

    sget-object v1, LX/DuZ;->REQUEST_SSN_LAST_FOUR:LX/DuZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DuZ;->PENDING_MANUAL_REVIEW:LX/DuZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DuZ;->FAILURE:LX/DuZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DuZ;->SUCCESS:LX/DuZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DuZ;->UNKNOWN_SCREEN:LX/DuZ;

    aput-object v2, v0, v1

    sput-object v0, LX/DuZ;->$VALUES:[LX/DuZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 2057327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2057328
    iput-boolean p3, p0, LX/DuZ;->isTerminal:Z

    .line 2057329
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/DuZ;
    .locals 5

    .prologue
    .line 2057330
    invoke-static {}, LX/DuZ;->values()[LX/DuZ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2057331
    invoke-virtual {v0}, LX/DuZ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2057332
    :goto_1
    return-object v0

    .line 2057333
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2057334
    :cond_1
    sget-object v0, LX/DuZ;->UNKNOWN_SCREEN:LX/DuZ;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DuZ;
    .locals 1

    .prologue
    .line 2057335
    const-class v0, LX/DuZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DuZ;

    return-object v0
.end method

.method public static values()[LX/DuZ;
    .locals 1

    .prologue
    .line 2057336
    sget-object v0, LX/DuZ;->$VALUES:[LX/DuZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DuZ;

    return-object v0
.end method
