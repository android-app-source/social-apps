.class public LX/DnT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/11S;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Locale;

.field private final d:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11S;Ljava/util/Locale;Ljava/util/TimeZone;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2040436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2040437
    iput-object p1, p0, LX/DnT;->b:Landroid/content/Context;

    .line 2040438
    iput-object p2, p0, LX/DnT;->a:LX/11S;

    .line 2040439
    iput-object p3, p0, LX/DnT;->c:Ljava/util/Locale;

    .line 2040440
    iput-object p4, p0, LX/DnT;->d:Ljava/util/TimeZone;

    .line 2040441
    return-void
.end method

.method public static b(LX/0QB;)LX/DnT;
    .locals 5

    .prologue
    .line 2040434
    new-instance v4, LX/DnT;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-static {p0}, LX/11U;->a(LX/0QB;)Ljava/util/TimeZone;

    move-result-object v3

    check-cast v3, Ljava/util/TimeZone;

    invoke-direct {v4, v0, v1, v2, v3}, LX/DnT;-><init>(Landroid/content/Context;LX/11S;Ljava/util/Locale;Ljava/util/TimeZone;)V

    .line 2040435
    return-object v4
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2040422
    invoke-virtual {p0, p1, p2}, LX/DnT;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 2040423
    invoke-virtual {p0, p1, p2}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v1

    .line 2040424
    iget-object v2, p0, LX/DnT;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080086

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040433
    iget-object v0, p0, LX/DnT;->b:Landroid/content/Context;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    const v1, 0x1001a

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040432
    iget-object v0, p0, LX/DnT;->a:LX/11S;

    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040430
    invoke-virtual {p0, p1, p2}, LX/DnT;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 2040431
    iget-object v1, p0, LX/DnT;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bbc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040429
    iget-object v0, p0, LX/DnT;->b:Landroid/content/Context;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    const/4 v1, 0x2

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040428
    iget-object v0, p0, LX/DnT;->b:Landroid/content/Context;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    const v1, 0x10028

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2040425
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    iget-object v2, p0, LX/DnT;->c:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2040426
    iget-object v1, p0, LX/DnT;->d:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2040427
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
