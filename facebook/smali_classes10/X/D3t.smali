.class public final LX/D3t;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/3Q3;


# direct methods
.method public constructor <init>(LX/3Q3;LX/0Ve;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1961378
    iput-object p1, p0, LX/D3t;->c:LX/3Q3;

    iput-object p2, p0, LX/D3t;->a:LX/0Ve;

    iput-object p3, p0, LX/D3t;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1961379
    iget-object v0, p0, LX/D3t;->c:LX/3Q3;

    iget-object v1, p0, LX/D3t;->b:Ljava/lang/String;

    iget-object v2, p0, LX/D3t;->a:LX/0Ve;

    .line 1961380
    iget-object v3, v0, LX/3Q3;->e:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fetch_notification_story"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/3Q3;->a:LX/3H7;

    sget-object p0, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    sget-object p1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-virtual {v5, v1, p0, p1}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance p0, LX/D3u;

    invoke-direct {p0, v0, v2}, LX/D3u;-><init>(LX/3Q3;LX/0Ve;)V

    invoke-virtual {v3, v4, v5, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1961381
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1961382
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1961383
    if-nez p1, :cond_0

    .line 1961384
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched notifStory in db was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/D3t;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1961385
    :goto_0
    return-void

    .line 1961386
    :cond_0
    iget-object v0, p0, LX/D3t;->c:LX/3Q3;

    invoke-static {v0, p1}, LX/3Q3;->a(LX/3Q3;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1961387
    if-nez v0, :cond_1

    .line 1961388
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched notifStory in db doesn\'t have a story"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/D3t;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1961389
    :cond_1
    iget-object v1, p0, LX/D3t;->a:LX/0Ve;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
