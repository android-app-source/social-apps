.class public LX/DdE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/6ek;

.field public final c:LX/2OW;

.field public final d:LX/0qp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qp",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Z

.field private h:J

.field public i:Lcom/facebook/messaging/model/folders/FolderCounts;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019264
    const-class v0, LX/DdE;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DdE;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6ek;LX/2OW;)V
    .locals 2

    .prologue
    .line 2019257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019258
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/DdE;->h:J

    .line 2019259
    sget-object v0, Lcom/facebook/messaging/model/folders/FolderCounts;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v0, p0, LX/DdE;->i:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2019260
    iput-object p1, p0, LX/DdE;->b:LX/6ek;

    .line 2019261
    iput-object p2, p0, LX/DdE;->c:LX/2OW;

    .line 2019262
    new-instance v0, LX/0qp;

    new-instance v1, LX/6g7;

    invoke-direct {v1}, LX/6g7;-><init>()V

    invoke-direct {v0, v1}, LX/0qp;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/DdE;->d:LX/0qp;

    .line 2019263
    return-void
.end method


# virtual methods
.method public final a()LX/0qp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qp",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2019307
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019308
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2019312
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019313
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 2019309
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019310
    iput-wide p1, p0, LX/DdE;->h:J

    .line 2019311
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 2

    .prologue
    .line 2019300
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019301
    iget-object v0, p0, LX/DdE;->b:LX/6ek;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v0, v1}, LX/6ek;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2019302
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, p1}, LX/0qp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019303
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2019304
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019305
    iput-boolean p1, p0, LX/DdE;->e:Z

    .line 2019306
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2019281
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019282
    iget-object v0, p0, LX/DdE;->b:LX/6ek;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v0, v1}, LX/6ek;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2019283
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2019284
    iget-object v7, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v7}, LX/2OW;->b()V

    .line 2019285
    iget-object v7, p0, LX/DdE;->d:LX/0qp;

    .line 2019286
    iget-object v8, v7, LX/0qp;->e:Ljava/util/List;

    move-object v7, v8

    .line 2019287
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    const-wide/16 v7, -0x1

    :goto_0
    move-wide v2, v7

    .line 2019288
    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    .line 2019289
    invoke-virtual {p0, p1}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2019290
    :cond_0
    :goto_1
    return-void

    .line 2019291
    :cond_1
    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v1, v4, v2

    if-nez v1, :cond_2

    .line 2019292
    invoke-virtual {p0, v0}, LX/DdE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2019293
    if-eqz v1, :cond_0

    .line 2019294
    iget-object v1, p0, LX/DdE;->d:LX/0qp;

    invoke-virtual {v1, v0, p1}, LX/0qp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2019295
    :cond_2
    invoke-virtual {p0, v0}, LX/DdE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2019296
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2019297
    invoke-virtual {p0}, LX/DdE;->g()V

    goto :goto_1

    .line 2019298
    :cond_3
    iput-boolean v6, p0, LX/DdE;->e:Z

    .line 2019299
    iput-boolean v6, p0, LX/DdE;->g:Z

    goto :goto_1

    :cond_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-wide v7, v7, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2019278
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019279
    iput-boolean p1, p0, LX/DdE;->f:Z

    .line 2019280
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2019275
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019276
    iput-boolean p1, p0, LX/DdE;->g:Z

    .line 2019277
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2019272
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019273
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->clear()V

    .line 2019274
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2019265
    iget-object v0, p0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 2019266
    iget-object v0, p0, LX/DdE;->d:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->clear()V

    .line 2019267
    iput-boolean v2, p0, LX/DdE;->f:Z

    .line 2019268
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/DdE;->h:J

    .line 2019269
    iput-boolean v2, p0, LX/DdE;->g:Z

    .line 2019270
    sget-object v0, Lcom/facebook/messaging/model/folders/FolderCounts;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v0, p0, LX/DdE;->i:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2019271
    return-void
.end method
