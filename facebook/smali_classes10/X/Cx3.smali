.class public LX/Cx3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cwx;


# instance fields
.field public final a:LX/CxA;

.field public final b:LX/1Pq;

.field public final c:LX/1Pr;

.field private final d:LX/Cys;


# direct methods
.method public constructor <init>(LX/CxA;LX/1Pq;LX/1Pr;LX/Cys;)V
    .locals 0
    .param p1    # LX/CxA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951397
    iput-object p1, p0, LX/Cx3;->a:LX/CxA;

    .line 1951398
    iput-object p2, p0, LX/Cx3;->b:LX/1Pq;

    .line 1951399
    iput-object p3, p0, LX/Cx3;->c:LX/1Pr;

    .line 1951400
    iput-object p4, p0, LX/Cx3;->d:LX/Cys;

    .line 1951401
    return-void
.end method


# virtual methods
.method public final a(LX/D0P;Ljava/lang/String;LX/CzL;LX/0gW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/D0P;",
            "Ljava/lang/String;",
            "LX/CzL",
            "<+TT;>;",
            "LX/0gW",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 1951402
    iget-object v0, p0, LX/Cx3;->c:LX/1Pr;

    new-instance v1, LX/Cxq;

    invoke-direct {v1, p2}, LX/Cxq;-><init>(Ljava/lang/String;)V

    new-instance v2, LX/Cx2;

    invoke-direct {v2, p0, p2}, LX/Cx2;-><init>(LX/Cx3;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cxr;

    move-object v0, v0

    .line 1951403
    iget-boolean v1, v0, LX/Cxr;->a:Z

    move v1, v1

    .line 1951404
    if-eqz v1, :cond_0

    .line 1951405
    :goto_0
    return-void

    .line 1951406
    :cond_0
    const/4 v1, 0x1

    .line 1951407
    iput-boolean v1, v0, LX/Cxr;->a:Z

    .line 1951408
    new-instance v1, LX/Cwz;

    invoke-direct {v1, p0, p3, v0}, LX/Cwz;-><init>(LX/Cx3;LX/CzL;LX/Cxr;)V

    .line 1951409
    new-instance v2, LX/Cx1;

    invoke-direct {v2, p0, v0}, LX/Cx1;-><init>(LX/Cx3;LX/Cxr;)V

    .line 1951410
    iget-object v0, p0, LX/Cx3;->d:LX/Cys;

    invoke-virtual {v0, p1, p4, v1, v2}, LX/Cys;->a(LX/D0P;LX/0gW;LX/Cwy;LX/Cx0;)V

    goto :goto_0
.end method
