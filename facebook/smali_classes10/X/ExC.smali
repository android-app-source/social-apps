.class public final LX/ExC;
.super LX/62n;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2184183
    iput-object p1, p0, LX/ExC;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {p0}, LX/62n;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Z)V
    .locals 4

    .prologue
    .line 2184184
    if-eqz p1, :cond_0

    .line 2184185
    iget-object v0, p0, LX/ExC;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->d()V

    .line 2184186
    iget-object v0, p0, LX/ExC;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    .line 2184187
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 2184188
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 2184189
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->A:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    .line 2184190
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    iget-object v2, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/Exj;->a(Ljava/lang/String;)V

    .line 2184191
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v1, :cond_1

    .line 2184192
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    const/16 v2, 0x14

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/2kW;->c(ILjava/lang/Object;)V

    .line 2184193
    :cond_0
    :goto_0
    return-void

    .line 2184194
    :cond_1
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->M:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2184195
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->b()V

    .line 2184196
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-static {v0, v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;LX/0zS;)V

    goto :goto_0
.end method
