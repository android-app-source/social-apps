.class public final LX/Eyd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/FriendRequest;

.field public final synthetic b:LX/2iU;


# direct methods
.method public constructor <init>(LX/2iU;Lcom/facebook/friends/model/FriendRequest;)V
    .locals 0

    .prologue
    .line 2186407
    iput-object p1, p0, LX/Eyd;->b:LX/2iU;

    iput-object p2, p0, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2186408
    iget-object v0, p0, LX/Eyd;->b:LX/2iU;

    iget-object v1, v0, LX/2iU;->a:LX/2dj;

    iget-object v0, p0, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v4, LX/2h8;->SUGGESTION:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2186409
    iget-object v1, p0, LX/Eyd;->b:LX/2iU;

    iget-object v2, p0, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2186410
    iget-object v1, p0, LX/Eyd;->b:LX/2iU;

    iget-object v1, v1, LX/2iU;->h:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ADD_FRIEND_IGNORE_WARN"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v3}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Eyb;

    invoke-direct {v3, p0, v0}, LX/Eyb;-><init>(LX/Eyd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/Eyc;

    invoke-direct {v0, p0}, LX/Eyc;-><init>(LX/Eyd;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2186411
    return-void
.end method
