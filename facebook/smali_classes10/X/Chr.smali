.class public LX/Chr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/Chr;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Chq;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/Chm;

.field public final c:LX/Cho;

.field public d:LX/Chv;

.field public e:Z

.field public f:LX/Cqw;

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1928596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928597
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Chr;->a:Ljava/util/Set;

    .line 1928598
    new-instance v0, LX/Chn;

    invoke-direct {v0, p0}, LX/Chn;-><init>(LX/Chr;)V

    iput-object v0, p0, LX/Chr;->b:LX/Chm;

    .line 1928599
    new-instance v0, LX/Chp;

    invoke-direct {v0, p0}, LX/Chp;-><init>(LX/Chr;)V

    iput-object v0, p0, LX/Chr;->c:LX/Cho;

    .line 1928600
    iput-boolean v1, p0, LX/Chr;->e:Z

    .line 1928601
    iput-boolean v1, p0, LX/Chr;->g:Z

    .line 1928602
    return-void
.end method

.method public static a(LX/0QB;)LX/Chr;
    .locals 3

    .prologue
    .line 1928603
    sget-object v0, LX/Chr;->i:LX/Chr;

    if-nez v0, :cond_1

    .line 1928604
    const-class v1, LX/Chr;

    monitor-enter v1

    .line 1928605
    :try_start_0
    sget-object v0, LX/Chr;->i:LX/Chr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1928606
    if-eqz v2, :cond_0

    .line 1928607
    :try_start_1
    new-instance v0, LX/Chr;

    invoke-direct {v0}, LX/Chr;-><init>()V

    .line 1928608
    move-object v0, v0

    .line 1928609
    sput-object v0, LX/Chr;->i:LX/Chr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1928611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1928612
    :cond_1
    sget-object v0, LX/Chr;->i:LX/Chr;

    return-object v0

    .line 1928613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1928614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
