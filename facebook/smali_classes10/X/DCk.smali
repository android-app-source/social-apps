.class public final LX/DCk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLCustomizedStory;LX/1Po;)V
    .locals 0

    .prologue
    .line 1974514
    iput-object p1, p0, LX/DCk;->c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    iput-object p2, p0, LX/DCk;->a:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    iput-object p3, p0, LX/DCk;->b:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 5

    .prologue
    .line 1974515
    sget-object v0, LX/20X;->SHARE:LX/20X;

    if-ne p2, v0, :cond_0

    .line 1974516
    iget-object v0, p0, LX/DCk;->c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    iget-object v1, p0, LX/DCk;->a:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    iget-object v2, p0, LX/DCk;->b:LX/1Po;

    const/4 p1, 0x1

    .line 1974517
    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v3}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v3

    .line 1974518
    const-string v4, "customizedStoryFooter"

    invoke-static {v1}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLCustomizedStory;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object p0

    invoke-static {p0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object p0

    invoke-virtual {p0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p0

    invoke-static {v3, v4, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    .line 1974519
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v4, "newsfeed_composer"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1974520
    iget-object v4, v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->c:LX/1Kf;

    const/4 p0, 0x0

    const/16 p1, 0x6dc

    iget-object p2, v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->a:Landroid/app/Activity;

    invoke-interface {v4, p0, v3, p1, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1974521
    :cond_0
    return-void
.end method
