.class public LX/Cr5;
.super LX/Cqf;
.source ""


# direct methods
.method public constructor <init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V
    .locals 0

    .prologue
    .line 1940642
    invoke-direct/range {p0 .. p7}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    .line 1940643
    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 7

    .prologue
    .line 1940600
    sget-object v1, LX/Cqw;->a:LX/Cqw;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    .line 1940601
    return-void
.end method


# virtual methods
.method public final g()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1940602
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1940603
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v0

    .line 1940604
    int-to-float v3, v2

    div-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1940605
    invoke-virtual {p0}, LX/Cr5;->p()I

    move-result v0

    .line 1940606
    if-le v3, v0, :cond_0

    .line 1940607
    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 1940608
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    add-int/lit8 v2, v2, 0x0

    add-int/2addr v3, v0

    invoke-direct {v4, v1, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940609
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v4}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940610
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1940611
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940612
    invoke-virtual {v0}, LX/CrW;->f()I

    move-result v0

    invoke-virtual {p0}, LX/Cr5;->p()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1940613
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940614
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    new-instance v2, LX/CrW;

    invoke-direct {v2, v1}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940615
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    new-instance v1, LX/CrP;

    .line 1940616
    iget-object v2, p0, LX/Cqf;->a:LX/Cqt;

    move-object v2, v2

    .line 1940617
    invoke-virtual {v2}, LX/Cqt;->getDegree()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v2}, LX/CrP;-><init>(F)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940618
    return-void
.end method

.method public final i()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1940619
    invoke-super {p0}, LX/Cqf;->i()V

    .line 1940620
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    .line 1940621
    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1940622
    invoke-interface {v0}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v2

    .line 1940623
    iget-object v3, v2, LX/ClU;->a:LX/ClT;

    move-object v2, v3

    .line 1940624
    invoke-interface {v0}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v3

    .line 1940625
    iget-object v4, v3, LX/ClU;->e:LX/ClR;

    move-object v3, v4

    .line 1940626
    sget-object v4, LX/ClT;->COPYRIGHT:LX/ClT;

    if-ne v2, v4, :cond_2

    sget-object v4, LX/ClR;->TOP:LX/ClR;

    if-eq v3, v4, :cond_0

    sget-object v4, LX/ClR;->CENTER:LX/ClR;

    if-eq v3, v4, :cond_0

    sget-object v4, LX/ClR;->BOTTOM:LX/ClR;

    if-ne v3, v4, :cond_2

    .line 1940627
    :cond_0
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/CrV;

    invoke-direct {v3, v5}, LX/CrV;-><init>(F)V

    invoke-virtual {p0, v2, v3}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940628
    :cond_1
    :goto_1
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/CqZ;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, LX/CqZ;-><init>(Z)V

    invoke-virtual {p0, v0, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    goto :goto_0

    .line 1940629
    :cond_2
    sget-object v3, LX/ClT;->VIDEO_CONTROL:LX/ClT;

    if-ne v2, v3, :cond_1

    .line 1940630
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/CrV;

    invoke-direct {v3, v5}, LX/CrV;-><init>(F)V

    invoke-virtual {p0, v2, v3}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    goto :goto_1

    .line 1940631
    :cond_3
    return-void
.end method

.method public final j()LX/CrW;
    .locals 3

    .prologue
    .line 1940632
    new-instance v0, Landroid/graphics/Rect;

    .line 1940633
    iget-object v1, p0, LX/Cqf;->i:LX/Ctg;

    move-object v1, v1

    .line 1940634
    invoke-interface {v1}, LX/Ctg;->getOverlayView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940635
    iget-object v2, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v1, v2

    .line 1940636
    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1940637
    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1940638
    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940639
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1940640
    new-instance v1, LX/CrW;

    invoke-direct {v1, v0}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    return-object v1
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1940641
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3faaaaab

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method
