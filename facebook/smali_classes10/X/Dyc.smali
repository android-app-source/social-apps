.class public final LX/Dyc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064791
    iput-object p1, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2064784
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-boolean v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2064785
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->Q:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2064786
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082940

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2064787
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v1, 0x0

    .line 2064788
    iput-boolean v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    .line 2064789
    sget-object v0, Lcom/facebook/places/checkin/PlacePickerFragment;->F:Ljava/lang/Class;

    const-string v1, "Could not flag place"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064790
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2064778
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-boolean v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2064779
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->Q:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2064780
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08293e

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2064781
    iget-object v0, p0, LX/Dyc;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v1, 0x0

    .line 2064782
    iput-boolean v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    .line 2064783
    return-void
.end method
