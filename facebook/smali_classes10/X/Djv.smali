.class public LX/Djv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1Ck;

.field public final c:LX/0kL;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2033845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033846
    iput-object p1, p0, LX/Djv;->a:LX/0tX;

    .line 2033847
    iput-object p2, p0, LX/Djv;->b:LX/1Ck;

    .line 2033848
    iput-object p3, p0, LX/Djv;->c:LX/0kL;

    .line 2033849
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dju;)V
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PagesPlatformNoteUpdateAction;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/Dju;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033850
    iget-object v9, p0, LX/Djv;->b:LX/1Ck;

    const-string v10, "admin_modify_appointment_note"

    new-instance v1, LX/Djs;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/Djs;-><init>(LX/Djv;Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, LX/Djt;

    move-object/from16 v0, p7

    invoke-direct {v2, p0, v0, p1}, LX/Djt;-><init>(LX/Djv;LX/Dju;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2033851
    return-void
.end method
