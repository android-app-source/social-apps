.class public final LX/Dtm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2054204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;
    .locals 14

    .prologue
    .line 2054205
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2054206
    iget-object v1, p0, LX/Dtm;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2054207
    iget-object v2, p0, LX/Dtm;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2054208
    iget-object v2, p0, LX/Dtm;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2054209
    iget-object v2, p0, LX/Dtm;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2054210
    iget-object v2, p0, LX/Dtm;->f:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 2054211
    iget-object v2, p0, LX/Dtm;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2054212
    iget-object v2, p0, LX/Dtm;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2054213
    iget-object v2, p0, LX/Dtm;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2054214
    iget-object v2, p0, LX/Dtm;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2054215
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2054216
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2054217
    const/4 v1, 0x1

    iget-wide v2, p0, LX/Dtm;->b:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054218
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2054219
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2054220
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2054221
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2054222
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2054223
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2054224
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2054225
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2054226
    const/16 v1, 0xa

    iget-wide v2, p0, LX/Dtm;->k:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2054227
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2054228
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2054229
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2054230
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2054231
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2054232
    new-instance v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-direct {v1, v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;-><init>(LX/15i;)V

    .line 2054233
    return-object v1
.end method
