.class public final LX/DkL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DkM;


# direct methods
.method public constructor <init>(LX/DkM;)V
    .locals 0

    .prologue
    .line 2034112
    iput-object p1, p0, LX/DkL;->a:LX/DkM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2034113
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;

    check-cast p2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;

    .line 2034114
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method
