.class public final enum LX/EIo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EIo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EIo;

.field public static final enum NO:LX/EIo;

.field public static final enum YES:LX/EIo;

.field public static final enum YES_DUE_TO_TIMEOUT:LX/EIo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2102345
    new-instance v0, LX/EIo;

    const-string v1, "NO"

    invoke-direct {v0, v1, v2}, LX/EIo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIo;->NO:LX/EIo;

    .line 2102346
    new-instance v0, LX/EIo;

    const-string v1, "YES"

    invoke-direct {v0, v1, v3}, LX/EIo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIo;->YES:LX/EIo;

    .line 2102347
    new-instance v0, LX/EIo;

    const-string v1, "YES_DUE_TO_TIMEOUT"

    invoke-direct {v0, v1, v4}, LX/EIo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIo;->YES_DUE_TO_TIMEOUT:LX/EIo;

    .line 2102348
    const/4 v0, 0x3

    new-array v0, v0, [LX/EIo;

    sget-object v1, LX/EIo;->NO:LX/EIo;

    aput-object v1, v0, v2

    sget-object v1, LX/EIo;->YES:LX/EIo;

    aput-object v1, v0, v3

    sget-object v1, LX/EIo;->YES_DUE_TO_TIMEOUT:LX/EIo;

    aput-object v1, v0, v4

    sput-object v0, LX/EIo;->$VALUES:[LX/EIo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2102351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EIo;
    .locals 1

    .prologue
    .line 2102350
    const-class v0, LX/EIo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EIo;

    return-object v0
.end method

.method public static values()[LX/EIo;
    .locals 1

    .prologue
    .line 2102349
    sget-object v0, LX/EIo;->$VALUES:[LX/EIo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EIo;

    return-object v0
.end method
