.class public LX/EPw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:LX/3d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d2",
            "<",
            "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/Integer;LX/2Sb;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118067
    iput-object p1, p0, LX/EPw;->b:Ljava/lang/String;

    .line 2118068
    new-instance v0, LX/3d2;

    const-class v1, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;

    new-instance v2, LX/EPv;

    invoke-direct {v2, p0, p4, p2, p3}, LX/EPv;-><init>(LX/EPw;LX/2Sb;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/Integer;)V

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/EPw;->c:LX/3d2;

    .line 2118069
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 2118070
    iget-object v0, p0, LX/EPw;->c:LX/3d2;

    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2118071
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, LX/EPw;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2118072
    const-string v0, "AddRecentSearchCacheVisitor"

    return-object v0
.end method
