.class public final synthetic LX/Dyg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2064806
    invoke-static {}, LX/Dyp;->values()[LX/Dyp;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dyg;->c:[I

    :try_start_0
    sget-object v0, LX/Dyg;->c:[I

    sget-object v1, LX/Dyp;->SUGGEST_EDITS:LX/Dyp;

    invoke-virtual {v1}, LX/Dyp;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, LX/Dyg;->c:[I

    sget-object v1, LX/Dyp;->REPORT_DUPLICATES:LX/Dyp;

    invoke-virtual {v1}, LX/Dyp;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, LX/Dyg;->c:[I

    sget-object v1, LX/Dyp;->FLAG:LX/Dyp;

    invoke-virtual {v1}, LX/Dyp;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 2064807
    :goto_2
    invoke-static {}, LX/Dyq;->values()[LX/Dyq;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dyg;->b:[I

    :try_start_3
    sget-object v0, LX/Dyg;->b:[I

    sget-object v1, LX/Dyq;->LONG_PRESS:LX/Dyq;

    invoke-virtual {v1}, LX/Dyq;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, LX/Dyg;->b:[I

    sget-object v1, LX/Dyq;->EDIT_MENU:LX/Dyq;

    invoke-virtual {v1}, LX/Dyq;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 2064808
    :goto_4
    invoke-static {}, LX/Dyr;->values()[LX/Dyr;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dyg;->a:[I

    :try_start_5
    sget-object v0, LX/Dyg;->a:[I

    sget-object v1, LX/Dyr;->SUGGEST_EDITS:LX/Dyr;

    invoke-virtual {v1}, LX/Dyr;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, LX/Dyg;->a:[I

    sget-object v1, LX/Dyr;->REPORT_DUPLICATES:LX/Dyr;

    invoke-virtual {v1}, LX/Dyr;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, LX/Dyg;->a:[I

    sget-object v1, LX/Dyr;->INAPPROPRIATE_CONTENT:LX/Dyr;

    invoke-virtual {v1}, LX/Dyr;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, LX/Dyg;->a:[I

    sget-object v1, LX/Dyr;->NOT_A_PUBLIC_PLACE:LX/Dyr;

    invoke-virtual {v1}, LX/Dyr;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    goto :goto_8

    :catch_1
    goto :goto_7

    :catch_2
    goto :goto_6

    :catch_3
    goto :goto_5

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_3

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_1

    :catch_8
    goto :goto_0
.end method
