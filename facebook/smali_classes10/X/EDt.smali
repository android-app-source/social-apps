.class public final enum LX/EDt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDt;

.field public static final enum BOTTOM_LEFT:LX/EDt;

.field public static final enum BOTTOM_RIGHT:LX/EDt;

.field public static final enum TOP_LEFT:LX/EDt;

.field public static final enum TOP_RIGHT:LX/EDt;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2091968
    new-instance v0, LX/EDt;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v2}, LX/EDt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDt;->TOP_LEFT:LX/EDt;

    .line 2091969
    new-instance v0, LX/EDt;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, LX/EDt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDt;->TOP_RIGHT:LX/EDt;

    .line 2091970
    new-instance v0, LX/EDt;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v4}, LX/EDt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDt;->BOTTOM_RIGHT:LX/EDt;

    .line 2091971
    new-instance v0, LX/EDt;

    const-string v1, "BOTTOM_LEFT"

    invoke-direct {v0, v1, v5}, LX/EDt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDt;->BOTTOM_LEFT:LX/EDt;

    .line 2091972
    const/4 v0, 0x4

    new-array v0, v0, [LX/EDt;

    sget-object v1, LX/EDt;->TOP_LEFT:LX/EDt;

    aput-object v1, v0, v2

    sget-object v1, LX/EDt;->TOP_RIGHT:LX/EDt;

    aput-object v1, v0, v3

    sget-object v1, LX/EDt;->BOTTOM_RIGHT:LX/EDt;

    aput-object v1, v0, v4

    sget-object v1, LX/EDt;->BOTTOM_LEFT:LX/EDt;

    aput-object v1, v0, v5

    sput-object v0, LX/EDt;->$VALUES:[LX/EDt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getCorner(ZZ)LX/EDt;
    .locals 1

    .prologue
    .line 2091974
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 2091975
    sget-object v0, LX/EDt;->TOP_LEFT:LX/EDt;

    .line 2091976
    :goto_0
    return-object v0

    .line 2091977
    :cond_0
    if-nez p0, :cond_1

    if-eqz p1, :cond_1

    .line 2091978
    sget-object v0, LX/EDt;->TOP_RIGHT:LX/EDt;

    goto :goto_0

    .line 2091979
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_2

    .line 2091980
    sget-object v0, LX/EDt;->BOTTOM_LEFT:LX/EDt;

    goto :goto_0

    .line 2091981
    :cond_2
    sget-object v0, LX/EDt;->BOTTOM_RIGHT:LX/EDt;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDt;
    .locals 1

    .prologue
    .line 2091982
    const-class v0, LX/EDt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDt;

    return-object v0
.end method

.method public static values()[LX/EDt;
    .locals 1

    .prologue
    .line 2091983
    sget-object v0, LX/EDt;->$VALUES:[LX/EDt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDt;

    return-object v0
.end method


# virtual methods
.method public final isLeft()Z
    .locals 1

    .prologue
    .line 2091984
    sget-object v0, LX/EDt;->TOP_LEFT:LX/EDt;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/EDt;->BOTTOM_LEFT:LX/EDt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isTop()Z
    .locals 1

    .prologue
    .line 2091985
    sget-object v0, LX/EDt;->TOP_LEFT:LX/EDt;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/EDt;->TOP_RIGHT:LX/EDt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
