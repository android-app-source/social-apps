.class public final LX/E45;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pS;


# instance fields
.field public final synthetic a:LX/E46;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;LX/E46;LX/2km;)V
    .locals 0

    .prologue
    .line 2075945
    iput-object p1, p0, LX/E45;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    iput-object p2, p0, LX/E45;->a:LX/E46;

    iput-object p3, p0, LX/E45;->b:LX/2km;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 13

    .prologue
    .line 2075946
    iget-object v0, p0, LX/E45;->a:LX/E46;

    iget-object v0, v0, LX/E46;->c:Ljava/lang/String;

    iget-object v1, p0, LX/E45;->a:LX/E46;

    iget-object v1, v1, LX/E46;->a:Ljava/lang/String;

    iget-object v2, p0, LX/E45;->a:LX/E46;

    iget-object v2, v2, LX/E46;->b:Ljava/lang/String;

    sget-object v3, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    .line 2075947
    sget-object v5, LX/21D;->LOCAL_SERP:LX/21D;

    const-string v6, "rating_bar"

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const-string v11, "review_button"

    const-string v12, "after_party"

    move-object v10, v2

    invoke-static/range {v5 .. v12}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    new-instance v5, LX/89K;

    invoke-direct {v5}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v5

    invoke-static {v5}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const-string v5, "ANDROID_AFTER_PARTY_COMPOSER"

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionUnitId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    .line 2075948
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "composer_configuration"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    .line 2075949
    new-instance v5, LX/Cfl;

    invoke-direct {v5, v1, v3, v4}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    move-object v0, v5

    .line 2075950
    iget-object v1, p0, LX/E45;->b:LX/2km;

    iget-object v2, p0, LX/E45;->a:LX/E46;

    iget-object v2, v2, LX/E46;->c:Ljava/lang/String;

    iget-object v3, p0, LX/E45;->a:LX/E46;

    iget-object v3, v3, LX/E46;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2075951
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2075952
    return-void
.end method
