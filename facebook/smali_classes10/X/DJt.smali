.class public final LX/DJt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DJu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/DJu;


# direct methods
.method public constructor <init>(LX/DJu;)V
    .locals 1

    .prologue
    .line 1985930
    iput-object p1, p0, LX/DJt;->e:LX/DJu;

    .line 1985931
    move-object v0, p1

    .line 1985932
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1985933
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1985954
    const-string v0, "SalePostPreviewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1985934
    if-ne p0, p1, :cond_1

    .line 1985935
    :cond_0
    :goto_0
    return v0

    .line 1985936
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1985937
    goto :goto_0

    .line 1985938
    :cond_3
    check-cast p1, LX/DJt;

    .line 1985939
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1985940
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1985941
    if-eq v2, v3, :cond_0

    .line 1985942
    iget-object v2, p0, LX/DJt;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DJt;->a:Ljava/lang/String;

    iget-object v3, p1, LX/DJt;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1985943
    goto :goto_0

    .line 1985944
    :cond_5
    iget-object v2, p1, LX/DJt;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1985945
    :cond_6
    iget-object v2, p0, LX/DJt;->b:Ljava/lang/Long;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DJt;->b:Ljava/lang/Long;

    iget-object v3, p1, LX/DJt;->b:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1985946
    goto :goto_0

    .line 1985947
    :cond_8
    iget-object v2, p1, LX/DJt;->b:Ljava/lang/Long;

    if-nez v2, :cond_7

    .line 1985948
    :cond_9
    iget-object v2, p0, LX/DJt;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DJt;->c:Ljava/lang/String;

    iget-object v3, p1, LX/DJt;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1985949
    goto :goto_0

    .line 1985950
    :cond_b
    iget-object v2, p1, LX/DJt;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1985951
    :cond_c
    iget-object v2, p0, LX/DJt;->d:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/DJt;->d:Ljava/lang/String;

    iget-object v3, p1, LX/DJt;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1985952
    goto :goto_0

    .line 1985953
    :cond_d
    iget-object v2, p1, LX/DJt;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
