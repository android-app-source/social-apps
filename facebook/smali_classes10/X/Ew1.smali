.class public final LX/Ew1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;)V
    .locals 0

    .prologue
    .line 2182688
    iput-object p1, p0, LX/Ew1;->a:LX/EwG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 2182689
    iget-object v0, p0, LX/Ew1;->a:LX/EwG;

    .line 2182690
    iget-object v1, v0, LX/EwG;->q:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    .line 2182691
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->K:Z

    .line 2182692
    invoke-static {v1}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2182693
    const v1, -0x65f5c641

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182694
    iget-object v1, v0, LX/EwG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3UJ;

    iget-object v2, v0, LX/EwG;->t:LX/5P1;

    sget-object v3, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    new-instance v4, LX/Ew4;

    invoke-direct {v4, v0}, LX/Ew4;-><init>(LX/EwG;)V

    .line 2182695
    iget-object p0, v1, LX/3UJ;->b:LX/2do;

    new-instance p1, LX/2iC;

    invoke-direct {p1}, LX/2iC;-><init>()V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2182696
    iget-object p0, v1, LX/3UJ;->d:LX/1Ck;

    const-string p1, "DELETE_ALL_FRIEND_REQUESTS_TASK"

    new-instance p2, LX/84B;

    invoke-direct {p2, v1, v3, v2}, LX/84B;-><init>(LX/3UJ;LX/2hA;LX/5P1;)V

    new-instance v0, LX/84C;

    invoke-direct {v0, v1, v4}, LX/84C;-><init>(LX/3UJ;LX/84H;)V

    invoke-virtual {p0, p1, p2, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2182697
    return-void
.end method
