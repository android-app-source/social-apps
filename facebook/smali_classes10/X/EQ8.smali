.class public final LX/EQ8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;",
        ">;",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2Sa;


# direct methods
.method public constructor <init>(LX/2Sa;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2118262
    iput-object p1, p0, LX/EQ8;->b:LX/2Sa;

    iput-object p2, p0, LX/EQ8;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2118263
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2118264
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2118265
    check-cast v0, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2118266
    new-instance v0, LX/Cw5;

    .line 2118267
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2118268
    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/Cw5;-><init>(LX/0Px;J)V

    .line 2118269
    :goto_0
    return-object v0

    .line 2118270
    :cond_0
    if-eqz p1, :cond_3

    .line 2118271
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2118272
    if-eqz v0, :cond_3

    .line 2118273
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2118274
    check-cast v0, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;->j()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2118275
    iget-object v0, p0, LX/EQ8;->b:LX/2Sa;

    iget-object v1, v0, LX/2Sa;->c:LX/2Sb;

    .line 2118276
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2118277
    check-cast v0, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel;->j()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;->a()LX/0Px;

    move-result-object v0

    iget-object v2, p0, LX/EQ8;->a:Ljava/lang/String;

    .line 2118278
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2118279
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    .line 2118280
    :try_start_0
    invoke-static {v1, v3, v2}, LX/2Sb;->a(LX/2Sb;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;Ljava/lang/String;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v3

    .line 2118281
    if-nez v3, :cond_1

    .line 2118282
    iget-object v3, v1, LX/2Sb;->a:LX/2Sc;

    sget-object v7, LX/3Ql;->FETCH_NULL_STATE_RECENT_SEARCHES_FAIL:LX/3Ql;

    const-string p0, "Edge or node is null in one of the recent search results"

    invoke-virtual {v3, v7, p0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2118283
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2118284
    :cond_1
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2118285
    :catch_0
    move-exception v3

    .line 2118286
    iget-object v7, v1, LX/2Sb;->a:LX/2Sc;

    invoke-virtual {v7, v3}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_2

    .line 2118287
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 2118288
    :goto_3
    new-instance v1, LX/Cw5;

    .line 2118289
    iget-wide v5, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v5

    .line 2118290
    iget-object v4, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 2118291
    invoke-direct {v1, v0, v2, v3, v4}, LX/Cw5;-><init>(LX/0Px;JLX/0ta;)V

    move-object v0, v1

    goto :goto_0

    .line 2118292
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2118293
    goto :goto_3
.end method
