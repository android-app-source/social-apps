.class public final LX/DHE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DHF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/3Qx;

.field public final synthetic d:LX/DHF;


# direct methods
.method public constructor <init>(LX/DHF;)V
    .locals 1

    .prologue
    .line 1981549
    iput-object p1, p0, LX/DHE;->d:LX/DHF;

    .line 1981550
    move-object v0, p1

    .line 1981551
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1981552
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1981548
    const-string v0, "SingleCreatorSetFooterComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/DHF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1981545
    check-cast p1, LX/DHE;

    .line 1981546
    iget-object v0, p1, LX/DHE;->c:LX/3Qx;

    iput-object v0, p0, LX/DHE;->c:LX/3Qx;

    .line 1981547
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1981528
    if-ne p0, p1, :cond_1

    .line 1981529
    :cond_0
    :goto_0
    return v0

    .line 1981530
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1981531
    goto :goto_0

    .line 1981532
    :cond_3
    check-cast p1, LX/DHE;

    .line 1981533
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1981534
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1981535
    if-eq v2, v3, :cond_0

    .line 1981536
    iget-object v2, p0, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1981537
    goto :goto_0

    .line 1981538
    :cond_5
    iget-object v2, p1, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1981539
    :cond_6
    iget-object v2, p0, LX/DHE;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DHE;->b:LX/1Po;

    iget-object v3, p1, LX/DHE;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1981540
    goto :goto_0

    .line 1981541
    :cond_7
    iget-object v2, p1, LX/DHE;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1981542
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/DHE;

    .line 1981543
    const/4 v1, 0x0

    iput-object v1, v0, LX/DHE;->c:LX/3Qx;

    .line 1981544
    return-object v0
.end method
