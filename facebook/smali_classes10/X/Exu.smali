.class public LX/Exu;
.super LX/Exj;
.source ""


# instance fields
.field private final c:LX/2h7;

.field private d:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2h7;LX/Exs;LX/0Ot;)V
    .locals 1
    .param p1    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2h7;",
            "LX/Exs;",
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2185621
    invoke-direct {p0, p2, p3}, LX/Exj;-><init>(LX/Exs;LX/0Ot;)V

    .line 2185622
    sget-object v0, LX/2kL;->a:LX/2kM;

    iput-object v0, p0, LX/Exu;->d:LX/2kM;

    .line 2185623
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Exu;->e:Ljava/util/List;

    .line 2185624
    iput-object p1, p0, LX/Exu;->c:LX/2h7;

    .line 2185625
    const/4 v0, 0x1

    .line 2185626
    iput-boolean v0, p2, LX/Exs;->t:Z

    .line 2185627
    return-void
.end method

.method private a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Eus;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2185590
    invoke-virtual {p1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2185591
    invoke-virtual {p1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    .line 2185592
    invoke-virtual {p1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2185593
    new-instance v6, LX/Euq;

    invoke-direct {v6}, LX/Euq;-><init>()V

    .line 2185594
    iput-wide v2, v6, LX/Euq;->a:J

    .line 2185595
    move-object v2, v6

    .line 2185596
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2185597
    :goto_0
    iput-object v0, v2, LX/Euq;->c:Ljava/lang/String;

    .line 2185598
    move-object v0, v2

    .line 2185599
    invoke-virtual {p1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2185600
    iput-object v2, v0, LX/Euq;->d:Ljava/lang/String;

    .line 2185601
    move-object v2, v0

    .line 2185602
    if-eqz v4, :cond_1

    invoke-virtual {v5, v4, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2185603
    :goto_1
    iput v0, v2, LX/Euq;->e:I

    .line 2185604
    move-object v0, v2

    .line 2185605
    iget-object v1, p0, LX/Exu;->c:LX/2h7;

    .line 2185606
    iput-object v1, v0, LX/Euq;->f:LX/2h7;

    .line 2185607
    move-object v0, v0

    .line 2185608
    invoke-virtual {p1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2185609
    iput-object v1, v0, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2185610
    move-object v0, v0

    .line 2185611
    invoke-virtual {v0}, LX/Euq;->b()LX/Eus;

    move-result-object v0

    .line 2185612
    return-object v0

    .line 2185613
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2185614
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(J)I
    .locals 5

    .prologue
    .line 2185615
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2185616
    invoke-virtual {p0, v0}, LX/Exu;->a(I)LX/Eus;

    move-result-object v1

    .line 2185617
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 2185618
    :goto_1
    return v0

    .line 2185619
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2185620
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(I)LX/Eus;
    .locals 2

    .prologue
    .line 2185580
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/Exu;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 2185581
    :goto_0
    if-eqz v0, :cond_2

    .line 2185582
    const/4 v0, 0x0

    .line 2185583
    :cond_0
    :goto_1
    return-object v0

    .line 2185584
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2185585
    :cond_2
    iget-object v0, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    .line 2185586
    if-nez v0, :cond_0

    .line 2185587
    iget-object v0, p0, LX/Exu;->d:LX/2kM;

    invoke-interface {v0, p1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2185588
    invoke-direct {p0, v0}, LX/Exu;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Eus;

    move-result-object v0

    .line 2185589
    iget-object v1, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(LX/2kM;LX/0Px;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 2185544
    iput-object p1, p0, LX/Exu;->d:LX/2kM;

    .line 2185545
    if-nez p2, :cond_0

    .line 2185546
    iget-object v1, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2185547
    :goto_0
    iget-object v1, p0, LX/Exu;->d:LX/2kM;

    invoke-interface {v1}, LX/2kM;->c()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 2185548
    iget-object v1, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2185549
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2185550
    :cond_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v7

    move v6, v0

    move v2, v0

    :goto_1
    if-ge v6, v7, :cond_5

    invoke-virtual {p2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 2185551
    sget-object v1, LX/Ext;->a:[I

    iget-object v3, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v3}, LX/3Ca;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v4, v2

    .line 2185552
    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v2, v4

    goto :goto_1

    .line 2185553
    :pswitch_0
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    .line 2185554
    :goto_3
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v2

    iget v3, v0, LX/3CY;->b:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 2185555
    iget-object v2, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v2, v1, v10}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2185556
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v4, v5

    .line 2185557
    goto :goto_2

    .line 2185558
    :pswitch_1
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v1

    iget v2, v0, LX/3CY;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    .line 2185559
    :goto_4
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 2185560
    iget-object v2, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2185561
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_3
    move v4, v5

    .line 2185562
    goto :goto_2

    .line 2185563
    :pswitch_2
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    move v3, v1

    move v4, v2

    .line 2185564
    :goto_5
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    iget v2, v0, LX/3CY;->b:I

    add-int/2addr v1, v2

    if-ge v3, v1, :cond_1

    .line 2185565
    invoke-interface {p1, v3}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2185566
    iget-object v2, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eus;

    .line 2185567
    if-nez v2, :cond_4

    .line 2185568
    invoke-direct {p0, v1}, LX/Exu;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Eus;

    move-result-object v1

    .line 2185569
    iget-object v2, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v2, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v2, v5

    .line 2185570
    :goto_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto :goto_5

    .line 2185571
    :cond_4
    invoke-virtual {v2}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    if-eq v8, v9, :cond_8

    .line 2185572
    invoke-virtual {v2}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2185573
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2185574
    invoke-virtual {v2, v1}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185575
    invoke-virtual {v2, v4}, LX/Eus;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move v2, v5

    .line 2185576
    goto :goto_6

    :cond_5
    move v5, v2

    .line 2185577
    :cond_6
    if-eqz v5, :cond_7

    .line 2185578
    const v0, 0x440e421f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2185579
    :cond_7
    return-void

    :cond_8
    move v2, v4

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2185540
    iget-object v0, p0, LX/Exu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2185541
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 2185542
    :goto_0
    add-int/2addr v0, v1

    return v0

    .line 2185543
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
