.class public LX/Ene;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/End;


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/0Px;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2167329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167330
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 2167331
    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2167332
    const-string v3, "AvailableIdsLoader doesn\'t support duplicate IDs"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2167333
    if-ltz p2, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2167334
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge p3, v0, :cond_1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2167335
    iput-object p1, p0, LX/Ene;->a:LX/0Px;

    .line 2167336
    iput p2, p0, LX/Ene;->b:I

    .line 2167337
    iput p3, p0, LX/Ene;->c:I

    .line 2167338
    return-void

    :cond_0
    move v0, v2

    .line 2167339
    goto :goto_1

    :cond_1
    move v1, v2

    .line 2167340
    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/EnQ;)V
    .locals 3

    .prologue
    .line 2167327
    iget-object v0, p1, LX/EnQ;->a:LX/0Px;

    iget v1, p1, LX/EnQ;->d:I

    iget v2, p1, LX/EnQ;->e:I

    invoke-direct {p0, v0, v1, v2}, LX/Ene;-><init>(LX/0Px;II)V

    .line 2167328
    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167326
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Enq;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2167310
    invoke-virtual {p0, p1, p2}, LX/Ene;->b(LX/Enq;I)LX/En3;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2167341
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/Ene;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Ene;->a:LX/0Px;

    .line 2167342
    return-void
.end method

.method public final a(LX/Enq;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2167321
    sget-object v2, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v2, :cond_2

    .line 2167322
    iget v2, p0, LX/Ene;->b:I

    if-lez v2, :cond_1

    .line 2167323
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2167324
    goto :goto_0

    .line 2167325
    :cond_2
    iget v2, p0, LX/Ene;->c:I

    iget-object v3, p0, LX/Ene;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2167320
    iget-object v0, p0, LX/Ene;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/Enq;I)LX/En3;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Enq;",
            "I)",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167312
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v0, :cond_0

    .line 2167313
    const/4 v0, 0x0

    iget v1, p0, LX/Ene;->b:I

    sub-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2167314
    iget-object v0, p0, LX/Ene;->a:LX/0Px;

    iget v2, p0, LX/Ene;->b:I

    invoke-virtual {v0, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, LX/En3;->a(ILX/0Px;)LX/En3;

    move-result-object v0

    .line 2167315
    iput v1, p0, LX/Ene;->b:I

    .line 2167316
    :goto_0
    return-object v0

    .line 2167317
    :cond_0
    iget-object v0, p0, LX/Ene;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/Ene;->c:I

    add-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2167318
    iget v0, p0, LX/Ene;->c:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, LX/Ene;->a:LX/0Px;

    iget v3, p0, LX/Ene;->c:I

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v3, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-static {v0, v2}, LX/En3;->a(ILX/0Px;)LX/En3;

    move-result-object v0

    .line 2167319
    iput v1, p0, LX/Ene;->c:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2167311
    const-class v0, LX/Ene;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "startIndex"

    iget v2, p0, LX/Ene;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "endIndex"

    iget v2, p0, LX/Ene;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "availableIds"

    iget-object v2, p0, LX/Ene;->a:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
