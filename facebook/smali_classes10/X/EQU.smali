.class public final LX/EQU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/widget/WrapperListAdapter;


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/EQT;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public final c:Landroid/database/DataSetObservable;

.field private final d:Landroid/widget/ListAdapter;

.field private e:I

.field private final f:Z


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/EQT;",
            ">;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2118733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118734
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, LX/EQU;->c:Landroid/database/DataSetObservable;

    .line 2118735
    const/4 v0, 0x1

    iput v0, p0, LX/EQU;->e:I

    .line 2118736
    iput-object p2, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    .line 2118737
    instance-of v0, p2, Landroid/widget/Filterable;

    iput-boolean v0, p0, LX/EQU;->f:Z

    .line 2118738
    if-nez p1, :cond_0

    .line 2118739
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "headerViewInfos cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2118740
    :cond_0
    iput-object p1, p0, LX/EQU;->a:Ljava/util/ArrayList;

    .line 2118741
    iget-object v0, p0, LX/EQU;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    .line 2118742
    if-eqz v0, :cond_2

    .line 2118743
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    move p1, v2

    :goto_0
    if-ge p1, p2, :cond_2

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EQT;

    .line 2118744
    iget-boolean v1, v1, LX/EQT;->c:Z

    if-nez v1, :cond_1

    move v1, v2

    .line 2118745
    :goto_1
    move v0, v1

    .line 2118746
    iput-boolean v0, p0, LX/EQU;->b:Z

    .line 2118747
    return-void

    .line 2118748
    :cond_1
    add-int/lit8 v1, p1, 0x1

    move p1, v1

    goto :goto_0

    .line 2118749
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private a()I
    .locals 1

    .prologue
    .line 2118750
    iget-object v0, p0, LX/EQU;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2118751
    if-gtz p1, :cond_0

    .line 2118752
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of columns must be 1 or more"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2118753
    :cond_0
    iget v0, p0, LX/EQU;->e:I

    if-eq v0, p1, :cond_1

    .line 2118754
    iput p1, p0, LX/EQU;->e:I

    .line 2118755
    iget-object v0, p0, LX/EQU;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 2118756
    :cond_1
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2118763
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 2118764
    iget-boolean v1, p0, LX/EQU;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2118765
    :cond_0
    :goto_0
    return v0

    .line 2118766
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2118757
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 2118758
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 2118759
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 2118760
    iget-boolean v0, p0, LX/EQU;->f:Z

    if-eqz v0, :cond_0

    .line 2118761
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/Filterable;

    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    .line 2118762
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118714
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    .line 2118715
    if-ge p1, v0, :cond_1

    .line 2118716
    iget v0, p0, LX/EQU;->e:I

    rem-int v0, p1, v0

    if-nez v0, :cond_0

    .line 2118717
    iget-object v0, p0, LX/EQU;->a:Ljava/util/ArrayList;

    iget v1, p0, LX/EQU;->e:I

    div-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQT;

    iget-object v0, v0, LX/EQT;->b:Ljava/lang/Object;

    .line 2118718
    :goto_0
    return-object v0

    .line 2118719
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2118720
    :cond_1
    sub-int v0, p1, v0

    .line 2118721
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_2

    .line 2118722
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2118723
    if-ge v0, v1, :cond_2

    .line 2118724
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2118725
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2118726
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    .line 2118727
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    if-lt p1, v0, :cond_0

    .line 2118728
    sub-int v0, p1, v0

    .line 2118729
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2118730
    if-ge v0, v1, :cond_0

    .line 2118731
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 2118732
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2118661
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    .line 2118662
    if-ge p1, v0, :cond_1

    iget v1, p0, LX/EQU;->e:I

    rem-int v1, p1, v1

    if-eqz v1, :cond_1

    .line 2118663
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    .line 2118664
    :goto_0
    return v0

    .line 2118665
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2118666
    :cond_1
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_2

    if-lt p1, v0, :cond_2

    .line 2118667
    sub-int v0, p1, v0

    .line 2118668
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2118669
    if-ge v0, v1, :cond_2

    .line 2118670
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2118671
    :cond_2
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2118672
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    .line 2118673
    if-ge p1, v0, :cond_2

    .line 2118674
    iget-object v0, p0, LX/EQU;->a:Ljava/util/ArrayList;

    iget v1, p0, LX/EQU;->e:I

    div-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQT;

    iget-object v0, v0, LX/EQT;->a:Landroid/view/ViewGroup;

    .line 2118675
    iget v1, p0, LX/EQU;->e:I

    rem-int v1, p1, v1

    if-nez v1, :cond_0

    move-object p2, v0

    .line 2118676
    :goto_0
    return-object p2

    .line 2118677
    :cond_0
    if-nez p2, :cond_1

    .line 2118678
    new-instance p2, Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2118679
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2118680
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_0

    .line 2118681
    :cond_2
    sub-int v0, p1, v0

    .line 2118682
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_3

    .line 2118683
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2118684
    if-ge v0, v1, :cond_3

    .line 2118685
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 2118686
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2118687
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 2118688
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2118689
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 2118690
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2118691
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 2118692
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    .line 2118693
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 2118694
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2118695
    invoke-direct {p0}, LX/EQU;->a()I

    move-result v0

    iget v1, p0, LX/EQU;->e:I

    mul-int/2addr v0, v1

    .line 2118696
    if-ge p1, v0, :cond_1

    .line 2118697
    iget v0, p0, LX/EQU;->e:I

    rem-int v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EQU;->a:Ljava/util/ArrayList;

    iget v1, p0, LX/EQU;->e:I

    div-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQT;

    iget-boolean v0, v0, LX/EQT;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2118698
    :goto_0
    return v0

    .line 2118699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2118700
    :cond_1
    sub-int v0, p1, v0

    .line 2118701
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_2

    .line 2118702
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2118703
    if-ge v0, v1, :cond_2

    .line 2118704
    iget-object v1, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    .line 2118705
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 2118706
    iget-object v0, p0, LX/EQU;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 2118707
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 2118708
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2118709
    :cond_0
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 2118710
    iget-object v0, p0, LX/EQU;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 2118711
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 2118712
    iget-object v0, p0, LX/EQU;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2118713
    :cond_0
    return-void
.end method
