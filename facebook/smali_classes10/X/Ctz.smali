.class public LX/Ctz;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private b:LX/Ctg;

.field private c:Landroid/graphics/Point;

.field public d:Z


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 4

    .prologue
    .line 1945845
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945846
    iput-object p1, p0, LX/Ctz;->b:LX/Ctg;

    .line 1945847
    iget-object v0, p0, LX/Ctz;->b:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d16c8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1945848
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1945849
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b12aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1945850
    iget-object v2, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v1, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1945851
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1945852
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1945853
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, LX/Ctz;->c:Landroid/graphics/Point;

    .line 1945854
    iget-object v1, p0, LX/Ctz;->c:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1945855
    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 5

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 1945856
    iget-object v0, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-nez v0, :cond_0

    .line 1945857
    :goto_0
    return-void

    .line 1945858
    :cond_0
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 1945859
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 1945860
    iget-object v2, p0, LX/Ctz;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v0, v2, v0

    .line 1945861
    iget-boolean v2, p0, LX/Ctz;->d:Z

    if-eqz v2, :cond_1

    .line 1945862
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 1945863
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 1945864
    iget-object v2, p0, LX/Ctz;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v0, v2, v0

    .line 1945865
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1945866
    iget-object v0, p0, LX/Ctz;->b:LX/Ctg;

    iget-object v1, p0, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-interface {v0, v1, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method
