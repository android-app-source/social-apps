.class public final enum LX/D4P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D4P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D4P;

.field public static final enum DIMMED:LX/D4P;

.field public static final enum FOCUS_DIMMED:LX/D4P;

.field public static final enum IMMERSED:LX/D4P;

.field public static final enum UNDIMMED:LX/D4P;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1962113
    new-instance v0, LX/D4P;

    const-string v1, "DIMMED"

    invoke-direct {v0, v1, v2}, LX/D4P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D4P;->DIMMED:LX/D4P;

    new-instance v0, LX/D4P;

    const-string v1, "UNDIMMED"

    invoke-direct {v0, v1, v3}, LX/D4P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D4P;->UNDIMMED:LX/D4P;

    new-instance v0, LX/D4P;

    const-string v1, "FOCUS_DIMMED"

    invoke-direct {v0, v1, v4}, LX/D4P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D4P;->FOCUS_DIMMED:LX/D4P;

    new-instance v0, LX/D4P;

    const-string v1, "IMMERSED"

    invoke-direct {v0, v1, v5}, LX/D4P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D4P;->IMMERSED:LX/D4P;

    .line 1962114
    const/4 v0, 0x4

    new-array v0, v0, [LX/D4P;

    sget-object v1, LX/D4P;->DIMMED:LX/D4P;

    aput-object v1, v0, v2

    sget-object v1, LX/D4P;->UNDIMMED:LX/D4P;

    aput-object v1, v0, v3

    sget-object v1, LX/D4P;->FOCUS_DIMMED:LX/D4P;

    aput-object v1, v0, v4

    sget-object v1, LX/D4P;->IMMERSED:LX/D4P;

    aput-object v1, v0, v5

    sput-object v0, LX/D4P;->$VALUES:[LX/D4P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1962112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D4P;
    .locals 1

    .prologue
    .line 1962115
    const-class v0, LX/D4P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D4P;

    return-object v0
.end method

.method public static values()[LX/D4P;
    .locals 1

    .prologue
    .line 1962111
    sget-object v0, LX/D4P;->$VALUES:[LX/D4P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D4P;

    return-object v0
.end method
