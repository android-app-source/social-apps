.class public LX/EsW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EsU;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EsX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2175726
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EsW;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EsX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175727
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2175728
    iput-object p1, p0, LX/EsW;->b:LX/0Ot;

    .line 2175729
    return-void
.end method

.method public static a(LX/0QB;)LX/EsW;
    .locals 4

    .prologue
    .line 2175730
    const-class v1, LX/EsW;

    monitor-enter v1

    .line 2175731
    :try_start_0
    sget-object v0, LX/EsW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175732
    sput-object v2, LX/EsW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175733
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175734
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175735
    new-instance v3, LX/EsW;

    const/16 p0, 0x1f20

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EsW;-><init>(LX/0Ot;)V

    .line 2175736
    move-object v0, v3

    .line 2175737
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175738
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EsW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175739
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2175741
    check-cast p2, LX/EsV;

    .line 2175742
    iget-object v0, p0, LX/EsW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EsX;

    iget-object v1, p2, LX/EsV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175743
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2175744
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iget-object v3, v0, LX/EsX;->a:LX/1e4;

    iget-object v4, v0, LX/EsX;->b:LX/0wM;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2175745
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v5

    .line 2175746
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v6

    .line 2175747
    invoke-virtual {v3, v6}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v6

    .line 2175748
    new-instance v7, Landroid/text/SpannableString;

    if-lez v6, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p2, "  "

    invoke-direct {p0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-direct {v7, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2175749
    if-lez v6, :cond_1

    .line 2175750
    const v5, -0x6e685d

    invoke-virtual {v4, v6, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2175751
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p0

    invoke-virtual {v5, v0, v0, v6, p0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2175752
    new-instance v6, Landroid/text/style/ImageSpan;

    invoke-direct {v6, v5, v1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2175753
    const/16 v5, 0x12

    invoke-interface {v7, v6, v0, v1, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2175754
    :cond_1
    move-object v2, v7

    .line 2175755
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a015e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2175756
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2175757
    invoke-static {}, LX/1dS;->b()V

    .line 2175758
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/EsU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2175759
    new-instance v1, LX/EsV;

    invoke-direct {v1, p0}, LX/EsV;-><init>(LX/EsW;)V

    .line 2175760
    sget-object v2, LX/EsW;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EsU;

    .line 2175761
    if-nez v2, :cond_0

    .line 2175762
    new-instance v2, LX/EsU;

    invoke-direct {v2}, LX/EsU;-><init>()V

    .line 2175763
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EsU;->a$redex0(LX/EsU;LX/1De;IILX/EsV;)V

    .line 2175764
    move-object v1, v2

    .line 2175765
    move-object v0, v1

    .line 2175766
    return-object v0
.end method
