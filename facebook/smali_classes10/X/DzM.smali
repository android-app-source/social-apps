.class public LX/DzM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DzM;


# instance fields
.field public a:LX/6aG;


# direct methods
.method public constructor <init>(LX/6aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066817
    iput-object p1, p0, LX/DzM;->a:LX/6aG;

    .line 2066818
    return-void
.end method

.method public static a(LX/0QB;)LX/DzM;
    .locals 4

    .prologue
    .line 2066819
    sget-object v0, LX/DzM;->b:LX/DzM;

    if-nez v0, :cond_1

    .line 2066820
    const-class v1, LX/DzM;

    monitor-enter v1

    .line 2066821
    :try_start_0
    sget-object v0, LX/DzM;->b:LX/DzM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2066822
    if-eqz v2, :cond_0

    .line 2066823
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2066824
    new-instance p0, LX/DzM;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v3

    check-cast v3, LX/6aG;

    invoke-direct {p0, v3}, LX/DzM;-><init>(LX/6aG;)V

    .line 2066825
    move-object v0, p0

    .line 2066826
    sput-object v0, LX/DzM;->b:LX/DzM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2066827
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2066828
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2066829
    :cond_1
    sget-object v0, LX/DzM;->b:LX/DzM;

    return-object v0

    .line 2066830
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2066831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 2066832
    invoke-virtual {p1, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2066833
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 2066834
    :cond_0
    :goto_0
    return-object p1

    .line 2066835
    :cond_1
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2066836
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 2066837
    :goto_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2066838
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x11

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object p1, v2

    .line 2066839
    goto :goto_0

    .line 2066840
    :cond_2
    const-string v2, "[-\'&/\\+,\\.]"

    const/4 v3, 0x2

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-le v2, v10, :cond_3

    .line 2066841
    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2066842
    if-eq v1, v9, :cond_0

    .line 2066843
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_1

    .line 2066844
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    new-array v4, v2, [I

    .line 2066845
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    new-array v5, v2, [C

    .line 2066846
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    array-length v7, v6

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v7, :cond_4

    aget-char v0, v6, v2

    .line 2066847
    const-string v8, "-\'&/+,."

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ne v8, v9, :cond_5

    .line 2066848
    aput-char v0, v5, v1

    .line 2066849
    add-int/lit8 v0, v1, 0x1

    aput v3, v4, v1

    .line 2066850
    :goto_3
    add-int/lit8 v3, v3, 0x1

    .line 2066851
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 2066852
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2066853
    if-eq v0, v9, :cond_0

    .line 2066854
    aget v1, v4, v0

    .line 2066855
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    aget v0, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3
.end method
