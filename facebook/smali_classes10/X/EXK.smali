.class public final enum LX/EXK;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/EXH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EXK;",
        ">;",
        "LX/EXH;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EXK;

.field public static final enum TYPE_BOOL:LX/EXK;

.field public static final enum TYPE_BYTES:LX/EXK;

.field public static final enum TYPE_DOUBLE:LX/EXK;

.field public static final enum TYPE_ENUM:LX/EXK;

.field public static final enum TYPE_FIXED32:LX/EXK;

.field public static final enum TYPE_FIXED64:LX/EXK;

.field public static final enum TYPE_FLOAT:LX/EXK;

.field public static final enum TYPE_GROUP:LX/EXK;

.field public static final enum TYPE_INT32:LX/EXK;

.field public static final enum TYPE_INT64:LX/EXK;

.field public static final enum TYPE_MESSAGE:LX/EXK;

.field public static final enum TYPE_SFIXED32:LX/EXK;

.field public static final enum TYPE_SFIXED64:LX/EXK;

.field public static final enum TYPE_SINT32:LX/EXK;

.field public static final enum TYPE_SINT64:LX/EXK;

.field public static final enum TYPE_STRING:LX/EXK;

.field public static final enum TYPE_UINT32:LX/EXK;

.field public static final enum TYPE_UINT64:LX/EXK;

.field private static final VALUES:[LX/EXK;

.field private static internalValueMap:LX/EXE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EXE",
            "<",
            "LX/EXK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 2133002
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_DOUBLE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_DOUBLE:LX/EXK;

    .line 2133003
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_FLOAT"

    invoke-direct {v0, v1, v5, v5, v6}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_FLOAT:LX/EXK;

    .line 2133004
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_INT64"

    invoke-direct {v0, v1, v6, v6, v7}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_INT64:LX/EXK;

    .line 2133005
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_UINT64"

    invoke-direct {v0, v1, v7, v7, v8}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_UINT64:LX/EXK;

    .line 2133006
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_INT32"

    invoke-direct {v0, v1, v8, v8, v9}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_INT32:LX/EXK;

    .line 2133007
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_FIXED64"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v9, v9, v2}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_FIXED64:LX/EXK;

    .line 2133008
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_FIXED32"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_FIXED32:LX/EXK;

    .line 2133009
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_BOOL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_BOOL:LX/EXK;

    .line 2133010
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_STRING"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_STRING:LX/EXK;

    .line 2133011
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_GROUP"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_GROUP:LX/EXK;

    .line 2133012
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_MESSAGE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_MESSAGE:LX/EXK;

    .line 2133013
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_BYTES"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_BYTES:LX/EXK;

    .line 2133014
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_UINT32"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_UINT32:LX/EXK;

    .line 2133015
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_ENUM"

    const/16 v2, 0xd

    const/16 v3, 0xd

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_ENUM:LX/EXK;

    .line 2133016
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_SFIXED32"

    const/16 v2, 0xe

    const/16 v3, 0xe

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_SFIXED32:LX/EXK;

    .line 2133017
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_SFIXED64"

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_SFIXED64:LX/EXK;

    .line 2133018
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_SINT32"

    const/16 v2, 0x10

    const/16 v3, 0x10

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_SINT32:LX/EXK;

    .line 2133019
    new-instance v0, LX/EXK;

    const-string v1, "TYPE_SINT64"

    const/16 v2, 0x11

    const/16 v3, 0x11

    const/16 v4, 0x12

    invoke-direct {v0, v1, v2, v3, v4}, LX/EXK;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXK;->TYPE_SINT64:LX/EXK;

    .line 2133020
    const/16 v0, 0x12

    new-array v0, v0, [LX/EXK;

    const/4 v1, 0x0

    sget-object v2, LX/EXK;->TYPE_DOUBLE:LX/EXK;

    aput-object v2, v0, v1

    sget-object v1, LX/EXK;->TYPE_FLOAT:LX/EXK;

    aput-object v1, v0, v5

    sget-object v1, LX/EXK;->TYPE_INT64:LX/EXK;

    aput-object v1, v0, v6

    sget-object v1, LX/EXK;->TYPE_UINT64:LX/EXK;

    aput-object v1, v0, v7

    sget-object v1, LX/EXK;->TYPE_INT32:LX/EXK;

    aput-object v1, v0, v8

    sget-object v1, LX/EXK;->TYPE_FIXED64:LX/EXK;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, LX/EXK;->TYPE_FIXED32:LX/EXK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EXK;->TYPE_BOOL:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EXK;->TYPE_STRING:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EXK;->TYPE_GROUP:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EXK;->TYPE_MESSAGE:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/EXK;->TYPE_BYTES:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/EXK;->TYPE_UINT32:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/EXK;->TYPE_ENUM:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/EXK;->TYPE_SFIXED32:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/EXK;->TYPE_SFIXED64:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/EXK;->TYPE_SINT32:LX/EXK;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/EXK;->TYPE_SINT64:LX/EXK;

    aput-object v2, v0, v1

    sput-object v0, LX/EXK;->$VALUES:[LX/EXK;

    .line 2133021
    new-instance v0, LX/EXJ;

    invoke-direct {v0}, LX/EXJ;-><init>()V

    sput-object v0, LX/EXK;->internalValueMap:LX/EXE;

    .line 2133022
    invoke-static {}, LX/EXK;->values()[LX/EXK;

    move-result-object v0

    sput-object v0, LX/EXK;->VALUES:[LX/EXK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2132998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2132999
    iput p3, p0, LX/EXK;->index:I

    .line 2133000
    iput p4, p0, LX/EXK;->value:I

    .line 2133001
    return-void
.end method

.method public static final getDescriptor()LX/EYL;
    .locals 2

    .prologue
    .line 2132997
    sget-object v0, LX/EYC;->i:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->g()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYL;

    return-object v0
.end method

.method public static internalGetValueMap()LX/EXE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EXE",
            "<",
            "LX/EXK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132996
    sget-object v0, LX/EXK;->internalValueMap:LX/EXE;

    return-object v0
.end method

.method public static valueOf(I)LX/EXK;
    .locals 1

    .prologue
    .line 2132976
    packed-switch p0, :pswitch_data_0

    .line 2132977
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2132978
    :pswitch_0
    sget-object v0, LX/EXK;->TYPE_DOUBLE:LX/EXK;

    goto :goto_0

    .line 2132979
    :pswitch_1
    sget-object v0, LX/EXK;->TYPE_FLOAT:LX/EXK;

    goto :goto_0

    .line 2132980
    :pswitch_2
    sget-object v0, LX/EXK;->TYPE_INT64:LX/EXK;

    goto :goto_0

    .line 2132981
    :pswitch_3
    sget-object v0, LX/EXK;->TYPE_UINT64:LX/EXK;

    goto :goto_0

    .line 2132982
    :pswitch_4
    sget-object v0, LX/EXK;->TYPE_INT32:LX/EXK;

    goto :goto_0

    .line 2132983
    :pswitch_5
    sget-object v0, LX/EXK;->TYPE_FIXED64:LX/EXK;

    goto :goto_0

    .line 2132984
    :pswitch_6
    sget-object v0, LX/EXK;->TYPE_FIXED32:LX/EXK;

    goto :goto_0

    .line 2132985
    :pswitch_7
    sget-object v0, LX/EXK;->TYPE_BOOL:LX/EXK;

    goto :goto_0

    .line 2132986
    :pswitch_8
    sget-object v0, LX/EXK;->TYPE_STRING:LX/EXK;

    goto :goto_0

    .line 2132987
    :pswitch_9
    sget-object v0, LX/EXK;->TYPE_GROUP:LX/EXK;

    goto :goto_0

    .line 2132988
    :pswitch_a
    sget-object v0, LX/EXK;->TYPE_MESSAGE:LX/EXK;

    goto :goto_0

    .line 2132989
    :pswitch_b
    sget-object v0, LX/EXK;->TYPE_BYTES:LX/EXK;

    goto :goto_0

    .line 2132990
    :pswitch_c
    sget-object v0, LX/EXK;->TYPE_UINT32:LX/EXK;

    goto :goto_0

    .line 2132991
    :pswitch_d
    sget-object v0, LX/EXK;->TYPE_ENUM:LX/EXK;

    goto :goto_0

    .line 2132992
    :pswitch_e
    sget-object v0, LX/EXK;->TYPE_SFIXED32:LX/EXK;

    goto :goto_0

    .line 2132993
    :pswitch_f
    sget-object v0, LX/EXK;->TYPE_SFIXED64:LX/EXK;

    goto :goto_0

    .line 2132994
    :pswitch_10
    sget-object v0, LX/EXK;->TYPE_SINT32:LX/EXK;

    goto :goto_0

    .line 2132995
    :pswitch_11
    sget-object v0, LX/EXK;->TYPE_SINT64:LX/EXK;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static valueOf(LX/EYM;)LX/EXK;
    .locals 2

    .prologue
    .line 2133023
    iget-object v0, p0, LX/EYM;->e:LX/EYL;

    move-object v0, v0

    .line 2133024
    invoke-static {}, LX/EXK;->getDescriptor()LX/EYL;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2133025
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EnumValueDescriptor is not for this type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2133026
    :cond_0
    sget-object v0, LX/EXK;->VALUES:[LX/EXK;

    .line 2133027
    iget v1, p0, LX/EYM;->a:I

    move v1, v1

    .line 2133028
    aget-object v0, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EXK;
    .locals 1

    .prologue
    .line 2132975
    const-class v0, LX/EXK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EXK;

    return-object v0
.end method

.method public static values()[LX/EXK;
    .locals 1

    .prologue
    .line 2132974
    sget-object v0, LX/EXK;->$VALUES:[LX/EXK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EXK;

    return-object v0
.end method


# virtual methods
.method public final getDescriptorForType()LX/EYL;
    .locals 1

    .prologue
    .line 2132973
    invoke-static {}, LX/EXK;->getDescriptor()LX/EYL;

    move-result-object v0

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .prologue
    .line 2132971
    iget v0, p0, LX/EXK;->value:I

    return v0
.end method

.method public final getValueDescriptor()LX/EYM;
    .locals 2

    .prologue
    .line 2132972
    invoke-static {}, LX/EXK;->getDescriptor()LX/EYL;

    move-result-object v0

    invoke-virtual {v0}, LX/EYL;->d()Ljava/util/List;

    move-result-object v0

    iget v1, p0, LX/EXK;->index:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYM;

    return-object v0
.end method
