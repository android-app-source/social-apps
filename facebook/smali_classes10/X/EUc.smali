.class public LX/EUc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:J

.field public final f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIIJJLcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;)V
    .locals 0
    .param p8    # Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2126253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126254
    iput-wide p4, p0, LX/EUc;->d:J

    .line 2126255
    iput p1, p0, LX/EUc;->a:I

    .line 2126256
    iput p2, p0, LX/EUc;->b:I

    .line 2126257
    iput p3, p0, LX/EUc;->c:I

    .line 2126258
    iput-wide p6, p0, LX/EUc;->e:J

    .line 2126259
    iput-object p8, p0, LX/EUc;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126260
    return-void
.end method
