.class public final LX/DIL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/87n;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;)V
    .locals 0

    .prologue
    .line 1983282
    iput-object p1, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;ILX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;I",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1983283
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983284
    iput-object p1, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    .line 1983285
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1983286
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1983287
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1983288
    iget-object v3, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v3, v3, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1983289
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1983290
    :cond_0
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v1, p2

    invoke-static {v0, v1}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;I)V

    .line 1983291
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v1, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->a(Ljava/lang/String;)V

    .line 1983292
    iget-object v0, p0, LX/DIL;->a:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1983293
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1983294
    return-void
.end method
