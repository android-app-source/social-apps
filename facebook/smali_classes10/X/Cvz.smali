.class public final enum LX/Cvz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cvz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cvz;

.field public static final enum RESULTS_LOAD:LX/Cvz;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1949826
    new-instance v0, LX/Cvz;

    const-string v1, "RESULTS_LOAD"

    const-string v2, "search_results_load"

    invoke-direct {v0, v1, v3, v2}, LX/Cvz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cvz;->RESULTS_LOAD:LX/Cvz;

    .line 1949827
    const/4 v0, 0x1

    new-array v0, v0, [LX/Cvz;

    sget-object v1, LX/Cvz;->RESULTS_LOAD:LX/Cvz;

    aput-object v1, v0, v3

    sput-object v0, LX/Cvz;->$VALUES:[LX/Cvz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949823
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1949824
    iput-object p3, p0, LX/Cvz;->name:Ljava/lang/String;

    .line 1949825
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cvz;
    .locals 1

    .prologue
    .line 1949821
    const-class v0, LX/Cvz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cvz;

    return-object v0
.end method

.method public static values()[LX/Cvz;
    .locals 1

    .prologue
    .line 1949822
    sget-object v0, LX/Cvz;->$VALUES:[LX/Cvz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cvz;

    return-object v0
.end method
