.class public final LX/DU3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2001323
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2001324
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001325
    :goto_0
    return v1

    .line 2001326
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 2001327
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2001328
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2001329
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 2001330
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2001331
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2001332
    :cond_1
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2001333
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2001334
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 2001335
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 2001336
    invoke-static {p0, p1}, LX/DU1;->b(LX/15w;LX/186;)I

    move-result v6

    .line 2001337
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2001338
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2001339
    goto :goto_1

    .line 2001340
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2001341
    invoke-static {p0, p1}, LX/DU2;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2001342
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2001343
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2001344
    if-eqz v0, :cond_6

    .line 2001345
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 2001346
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2001347
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2001348
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2001349
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001350
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2001351
    if-eqz v0, :cond_0

    .line 2001352
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001353
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2001354
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001355
    if-eqz v0, :cond_2

    .line 2001356
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001357
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2001358
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2001359
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/DU1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2001360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2001361
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2001362
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001363
    if-eqz v0, :cond_8

    .line 2001364
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001365
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001366
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001367
    if-eqz v1, :cond_3

    .line 2001368
    const-string v2, "delta_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001369
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001370
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001371
    if-eqz v1, :cond_4

    .line 2001372
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001373
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001374
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2001375
    if-eqz v1, :cond_5

    .line 2001376
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001377
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2001378
    :cond_5
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2001379
    if-eqz v1, :cond_6

    .line 2001380
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001381
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2001382
    :cond_6
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001383
    if-eqz v1, :cond_7

    .line 2001384
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001385
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001386
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001387
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001388
    return-void
.end method
