.class public LX/EOQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOO;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EOU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2114495
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EOQ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EOU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114496
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2114497
    iput-object p1, p0, LX/EOQ;->b:LX/0Ot;

    .line 2114498
    return-void
.end method

.method public static a(LX/0QB;)LX/EOQ;
    .locals 4

    .prologue
    .line 2114499
    const-class v1, LX/EOQ;

    monitor-enter v1

    .line 2114500
    :try_start_0
    sget-object v0, LX/EOQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114501
    sput-object v2, LX/EOQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114502
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114503
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114504
    new-instance v3, LX/EOQ;

    const/16 p0, 0x3462

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOQ;-><init>(LX/0Ot;)V

    .line 2114505
    move-object v0, v3

    .line 2114506
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114507
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114508
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2114510
    check-cast p2, LX/EOP;

    .line 2114511
    iget-object v0, p0, LX/EOQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EOU;

    iget-object v1, p2, LX/EOP;->a:LX/CzL;

    iget-object v2, p2, LX/EOP;->b:LX/EOS;

    iget v3, p2, LX/EOP;->c:I

    const/16 p2, 0x8

    const/4 v5, 0x0

    .line 2114512
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v6, -0x2

    invoke-interface {v4, p2, v6}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v8

    .line 2114513
    iget-object v4, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2114514
    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fh_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v9

    .line 2114515
    invoke-static {p1}, LX/EOU;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2114516
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v5

    move v7, v5

    move-object v5, v4

    :goto_0
    if-ge v6, v10, :cond_0

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2114517
    iget-object p0, v0, LX/EOU;->a:LX/EOj;

    invoke-virtual {p0, p1}, LX/EOj;->c(LX/1De;)LX/EOh;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->a()LX/8d2;

    move-result-object v4

    invoke-virtual {v1, v4, v7}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/EOh;->a(LX/CzL;)LX/EOh;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/EOh;->a(LX/EOS;)LX/EOh;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p2, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114518
    rem-int v4, v7, v3

    add-int/lit8 p0, v3, -0x1

    if-ne v4, p0, :cond_3

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2114519
    if-eqz v4, :cond_2

    .line 2114520
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114521
    invoke-static {p1}, LX/EOU;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2114522
    :goto_2
    add-int/lit8 v7, v7, 0x1

    .line 2114523
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move-object v5, v4

    goto :goto_0

    .line 2114524
    :cond_0
    :goto_3
    rem-int v4, v7, v3

    if-eqz v4, :cond_1

    .line 2114525
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v4, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114526
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2114527
    :cond_1
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114528
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2114529
    return-object v0

    :cond_2
    move-object v4, v5

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2114530
    invoke-static {}, LX/1dS;->b()V

    .line 2114531
    const/4 v0, 0x0

    return-object v0
.end method
