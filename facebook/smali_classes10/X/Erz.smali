.class public LX/Erz;
.super LX/16T;
.source ""


# static fields
.field private static final e:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Fn;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/13n;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2173861
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_AIRPORT_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Erz;->e:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2173862
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 2173863
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173864
    iput-object v0, p0, LX/Erz;->a:LX/0Ot;

    .line 2173865
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173866
    iput-object v0, p0, LX/Erz;->b:LX/0Ot;

    .line 2173867
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173868
    iput-object v0, p0, LX/Erz;->c:LX/0Ot;

    .line 2173869
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 2173870
    iget-object v0, p0, LX/Erz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    .line 2173871
    iget-object v2, v0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W3;

    sget-wide v4, LX/0X5;->ea:J

    invoke-interface {v2, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v2

    move-wide v0, v2

    .line 2173872
    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 2173873
    iget-object v0, p0, LX/Erz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173874
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 2173875
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2173876
    const-string v0, "4504"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173877
    sget-object v0, LX/Erz;->e:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
