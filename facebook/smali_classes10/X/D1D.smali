.class public final LX/D1D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/D1G;

.field public final synthetic b:LX/D1E;


# direct methods
.method public constructor <init>(LX/D1E;LX/D1G;)V
    .locals 0

    .prologue
    .line 1956390
    iput-object p1, p0, LX/D1D;->b:LX/D1E;

    iput-object p2, p0, LX/D1D;->a:LX/D1G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x1

    const v1, -0x4743c8c9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1956391
    iget-object v0, p0, LX/D1D;->b:LX/D1E;

    iget-object v0, v0, LX/D1E;->a:LX/D1G;

    iget-object v0, v0, LX/D14;->e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0, p1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b(Landroid/view/View;)V

    .line 1956392
    const v0, 0x7f0d01cf

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 1956393
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1956394
    const v0, 0x2fa03aa2

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1956395
    :goto_0
    return-void

    .line 1956396
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;

    invoke-virtual {v1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1956397
    :goto_1
    iget-object v3, p0, LX/D1D;->b:LX/D1E;

    iget-object v3, v3, LX/D1E;->a:LX/D1G;

    iget-object v3, v3, LX/D14;->e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;)V

    .line 1956398
    const v0, -0x76b6983

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1956399
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;

    invoke-virtual {v1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_1
.end method
