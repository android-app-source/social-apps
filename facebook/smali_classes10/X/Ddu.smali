.class public final LX/Ddu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2020441
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 2020442
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2020443
    :goto_0
    return v1

    .line 2020444
    :cond_0
    const-string v10, "is_messenger_cymk_hidden"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2020445
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 2020446
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 2020447
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2020448
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2020449
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 2020450
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2020451
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2020452
    :cond_2
    const-string v10, "mutual_contacts_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2020453
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2020454
    :cond_3
    const-string v10, "squareProfilePicBig"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2020455
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2020456
    :cond_4
    const-string v10, "structured_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2020457
    invoke-static {p0, p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2020458
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2020459
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2020460
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2020461
    if-eqz v3, :cond_7

    .line 2020462
    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 2020463
    :cond_7
    if-eqz v0, :cond_8

    .line 2020464
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 2020465
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2020466
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2020467
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2020468
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2020469
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2020470
    if-eqz v0, :cond_0

    .line 2020471
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020472
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2020473
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2020474
    if-eqz v0, :cond_1

    .line 2020475
    const-string v1, "is_messenger_cymk_hidden"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020476
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2020477
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2020478
    if-eqz v0, :cond_2

    .line 2020479
    const-string v1, "mutual_contacts_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020480
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2020481
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2020482
    if-eqz v0, :cond_3

    .line 2020483
    const-string v1, "squareProfilePicBig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020484
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2020485
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2020486
    if-eqz v0, :cond_4

    .line 2020487
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2020488
    invoke-static {p0, v0, p2, p3}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2020489
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2020490
    return-void
.end method
