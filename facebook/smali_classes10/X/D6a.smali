.class public final LX/D6a;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;)V
    .locals 0

    .prologue
    .line 1965959
    iput-object p1, p0, LX/D6a;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;B)V
    .locals 0

    .prologue
    .line 1965960
    invoke-direct {p0, p1}, LX/D6a;-><init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;)V

    return-void
.end method

.method private a(LX/2ou;)V
    .locals 2

    .prologue
    .line 1965961
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1965962
    iget-object v0, p0, LX/D6a;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1965963
    :cond_0
    :goto_0
    return-void

    .line 1965964
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1965965
    iget-object v0, p0, LX/D6a;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;->o:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1965966
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1965967
    check-cast p1, LX/2ou;

    invoke-direct {p0, p1}, LX/D6a;->a(LX/2ou;)V

    return-void
.end method
