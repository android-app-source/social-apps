.class public final LX/DUo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2004061
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2004062
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2004063
    :goto_0
    return v1

    .line 2004064
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2004065
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2004066
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2004067
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2004068
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2004069
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2004070
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2004071
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2004072
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2004073
    invoke-static {p0, p1}, LX/DUn;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2004074
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2004075
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2004076
    goto :goto_1

    .line 2004077
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2004078
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2004079
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_f

    .line 2004080
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2004081
    :goto_3
    move v0, v3

    .line 2004082
    goto :goto_1

    .line 2004083
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2004084
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2004085
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2004086
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2004087
    :cond_6
    const-string v12, "has_next_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2004088
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v4

    .line 2004089
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 2004090
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2004091
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2004092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_7

    if-eqz v11, :cond_7

    .line 2004093
    const-string v12, "delta_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2004094
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_4

    .line 2004095
    :cond_8
    const-string v12, "end_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2004096
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2004097
    :cond_9
    const-string v12, "has_previous_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2004098
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v4

    goto :goto_4

    .line 2004099
    :cond_a
    const-string v12, "start_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2004100
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2004101
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2004102
    :cond_c
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2004103
    invoke-virtual {p1, v3, v10}, LX/186;->b(II)V

    .line 2004104
    invoke-virtual {p1, v4, v9}, LX/186;->b(II)V

    .line 2004105
    if-eqz v5, :cond_d

    .line 2004106
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v8}, LX/186;->a(IZ)V

    .line 2004107
    :cond_d
    if-eqz v0, :cond_e

    .line 2004108
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 2004109
    :cond_e
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2004110
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_f
    move v0, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    move v10, v3

    goto/16 :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2004111
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2004112
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2004113
    if-eqz v0, :cond_1

    .line 2004114
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004115
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2004116
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2004117
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/DUn;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2004118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2004119
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2004120
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2004121
    if-eqz v0, :cond_7

    .line 2004122
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004123
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2004124
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2004125
    if-eqz v1, :cond_2

    .line 2004126
    const-string v2, "delta_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004127
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2004128
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2004129
    if-eqz v1, :cond_3

    .line 2004130
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004131
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2004132
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2004133
    if-eqz v1, :cond_4

    .line 2004134
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004135
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2004136
    :cond_4
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2004137
    if-eqz v1, :cond_5

    .line 2004138
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004139
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2004140
    :cond_5
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2004141
    if-eqz v1, :cond_6

    .line 2004142
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2004143
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2004144
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2004145
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2004146
    return-void
.end method
