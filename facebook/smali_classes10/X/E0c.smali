.class public final LX/E0c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$HomeResidenceQuery$;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 0

    .prologue
    .line 2068756
    iput-object p1, p0, LX/E0c;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;)V
    .locals 1
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068757
    if-eqz p1, :cond_0

    .line 2068758
    iget-object v0, p0, LX/E0c;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    .line 2068759
    invoke-static {v0, p1}, Lcom/facebook/places/create/home/HomeEditActivity;->a$redex0(Lcom/facebook/places/create/home/HomeEditActivity;Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;)V

    .line 2068760
    :goto_0
    return-void

    .line 2068761
    :cond_0
    iget-object v0, p0, LX/E0c;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-static {v0}, Lcom/facebook/places/create/home/HomeEditActivity;->x(Lcom/facebook/places/create/home/HomeEditActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2068762
    iget-object v0, p0, LX/E0c;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-static {v0}, Lcom/facebook/places/create/home/HomeEditActivity;->x(Lcom/facebook/places/create/home/HomeEditActivity;)V

    .line 2068763
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068764
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    invoke-direct {p0, p1}, LX/E0c;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;)V

    return-void
.end method
