.class public final LX/E0J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 2068195
    iput-object p1, p0, LX/E0J;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x52081cc2

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2068196
    iget-object v1, p0, LX/E0J;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068197
    iget-object v3, v1, LX/E0T;->a:LX/0Zb;

    const-string p1, "home_%s_privacy_tapped"

    invoke-static {v1, p1}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v3, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068198
    iget-object v1, p0, LX/E0J;->a:Lcom/facebook/places/create/home/HomeActivity;

    .line 2068199
    invoke-virtual {v1}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068200
    new-instance v3, Landroid/content/Intent;

    const-class p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;

    invoke-direct {v3, v1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2068201
    const-string p0, "extra_initial_privacy"

    iget-object p1, v1, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object p1, p1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v3, p0, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2068202
    iget-object p0, v1, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0xd

    invoke-interface {p0, v3, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2068203
    const v1, -0x3fea27a7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
