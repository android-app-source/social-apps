.class public final LX/E9X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/E9Y;


# direct methods
.method public constructor <init>(LX/E9Y;I)V
    .locals 0

    .prologue
    .line 2084494
    iput-object p1, p0, LX/E9X;->b:LX/E9Y;

    iput p2, p0, LX/E9X;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2084495
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v3, 0x0

    .line 2084496
    if-nez p1, :cond_0

    .line 2084497
    :goto_0
    return-object v3

    .line 2084498
    :cond_0
    iget-object v0, p0, LX/E9X;->b:LX/E9Y;

    iget-object v0, v0, LX/E9Y;->c:Ljava/util/List;

    iget v1, p0, LX/E9X;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v0

    invoke-static {v0, p1}, LX/BNG;->a(LX/5tj;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/5tj;

    move-result-object v1

    .line 2084499
    iget-object v0, p0, LX/E9X;->b:LX/E9Y;

    iget-object v0, v0, LX/E9Y;->c:Ljava/util/List;

    iget v2, p0, LX/E9X;->a:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-static {v0, v1}, LX/BNH;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;LX/5tj;)Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    move-result-object v0

    .line 2084500
    iget-object v1, p0, LX/E9X;->b:LX/E9Y;

    iget-object v1, v1, LX/E9Y;->c:Ljava/util/List;

    iget v2, p0, LX/E9X;->a:I

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2084501
    iget-object v1, p0, LX/E9X;->b:LX/E9Y;

    invoke-static {v1, v0}, LX/E9Y;->a$redex0(LX/E9Y;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V

    .line 2084502
    iget-object v0, p0, LX/E9X;->b:LX/E9Y;

    iget v1, p0, LX/E9X;->a:I

    invoke-static {v0, v1, p1}, LX/E9Y;->a$redex0(LX/E9Y;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2084503
    iget-object v0, p0, LX/E9X;->b:LX/E9Y;

    iget-object v0, v0, LX/E9Y;->b:LX/E9R;

    invoke-virtual {v0}, LX/E9Q;->c()V

    goto :goto_0
.end method
