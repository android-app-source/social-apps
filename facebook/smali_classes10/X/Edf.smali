.class public LX/Edf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:Z

.field private static e:[B

.field private static f:[B


# instance fields
.field private b:Ljava/io/ByteArrayInputStream;

.field private c:LX/Ede;

.field private d:LX/EdY;

.field private final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2150654
    const-class v0, LX/Edf;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/Edf;->a:Z

    .line 2150655
    sput-object v1, LX/Edf;->e:[B

    .line 2150656
    sput-object v1, LX/Edf;->f:[B

    return-void

    .line 2150657
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([BZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2150647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2150648
    iput-object v0, p0, LX/Edf;->b:Ljava/io/ByteArrayInputStream;

    .line 2150649
    iput-object v0, p0, LX/Edf;->c:LX/Ede;

    .line 2150650
    iput-object v0, p0, LX/Edf;->d:LX/EdY;

    .line 2150651
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v0, p0, LX/Edf;->b:Ljava/io/ByteArrayInputStream;

    .line 2150652
    iput-boolean p2, p0, LX/Edf;->g:Z

    .line 2150653
    return-void
.end method

.method private static a(LX/Edg;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2150634
    sget-boolean v2, LX/Edf;->a:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150635
    :cond_0
    sget-object v2, LX/Edf;->e:[B

    if-nez v2, :cond_2

    sget-object v2, LX/Edf;->f:[B

    if-nez v2, :cond_2

    .line 2150636
    :cond_1
    :goto_0
    return v0

    .line 2150637
    :cond_2
    sget-object v2, LX/Edf;->f:[B

    if-eqz v2, :cond_3

    .line 2150638
    invoke-virtual {p0}, LX/Edg;->c()[B

    move-result-object v2

    .line 2150639
    if-eqz v2, :cond_1

    .line 2150640
    sget-object v3, LX/Edf;->f:[B

    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 2150641
    goto :goto_0

    .line 2150642
    :cond_3
    sget-object v2, LX/Edf;->e:[B

    if-eqz v2, :cond_1

    .line 2150643
    invoke-virtual {p0}, LX/Edg;->g()[B

    move-result-object v2

    .line 2150644
    if-eqz v2, :cond_1

    .line 2150645
    sget-object v3, LX/Edf;->e:[B

    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 2150646
    goto :goto_0
.end method

.method private static a(Ljava/io/ByteArrayInputStream;)LX/Ede;
    .locals 13

    .prologue
    const/16 v12, 0x80

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2150481
    if-nez p0, :cond_0

    move-object v0, v2

    .line 2150482
    :goto_0
    return-object v0

    .line 2150483
    :cond_0
    new-instance v3, LX/Ede;

    invoke-direct {v3}, LX/Ede;-><init>()V

    move v0, v6

    .line 2150484
    :cond_1
    :goto_1
    if-eqz v0, :cond_e

    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v4

    if-lez v4, :cond_e

    .line 2150485
    invoke-virtual {p0, v6}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150486
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v7

    .line 2150487
    const/16 v4, 0x20

    if-lt v7, v4, :cond_2

    const/16 v4, 0x7f

    if-gt v7, v4, :cond_2

    .line 2150488
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150489
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    goto :goto_1

    .line 2150490
    :cond_2
    packed-switch v7, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2150491
    :pswitch_1
    invoke-static {p0}, LX/Edf;->e(Ljava/io/ByteArrayInputStream;)LX/EdS;

    move-result-object v5

    .line 2150492
    if-eqz v5, :cond_1

    .line 2150493
    invoke-virtual {v5}, LX/EdS;->b()[B

    move-result-object v8

    .line 2150494
    if-eqz v8, :cond_4

    .line 2150495
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v8}, Ljava/lang/String;-><init>([B)V

    .line 2150496
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 2150497
    if-lez v8, :cond_3

    .line 2150498
    invoke-virtual {v4, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2150499
    :cond_3
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, LX/EdS;->a([B)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_b

    .line 2150500
    :cond_4
    :try_start_1
    invoke-virtual {v3, v5, v7}, LX/Ede;->b(LX/EdS;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_c

    goto :goto_1

    .line 2150501
    :catch_0
    goto :goto_1

    .line 2150502
    :pswitch_2
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    .line 2150503
    packed-switch v4, :pswitch_data_1

    .line 2150504
    :try_start_2
    invoke-virtual {v3, v4, v7}, LX/Ede;->a(II)V
    :try_end_2
    .catch LX/EdU; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 2150505
    :catch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set invalid Octet value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " into the header filed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150506
    goto :goto_0

    :pswitch_3
    move-object v0, v2

    .line 2150507
    goto :goto_0

    .line 2150508
    :catch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Octet header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150509
    goto/16 :goto_0

    .line 2150510
    :pswitch_4
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    .line 2150511
    :try_start_3
    invoke-virtual {v3, v4, v7}, LX/Ede;->a(II)V
    :try_end_3
    .catch LX/EdU; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_1

    .line 2150512
    :catch_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set invalid Octet value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " into the header filed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150513
    goto/16 :goto_0

    .line 2150514
    :catch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Octet header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150515
    goto/16 :goto_0

    .line 2150516
    :pswitch_5
    :try_start_4
    invoke-static {p0}, LX/Edf;->h(Ljava/io/ByteArrayInputStream;)J

    move-result-wide v4

    .line 2150517
    invoke-virtual {v3, v4, v5, v7}, LX/Ede;->a(JI)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_1

    .line 2150518
    :catch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Long-Integer header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150519
    goto/16 :goto_0

    .line 2150520
    :pswitch_6
    :try_start_5
    invoke-static {p0}, LX/Edf;->i(Ljava/io/ByteArrayInputStream;)J

    move-result-wide v4

    .line 2150521
    invoke-virtual {v3, v4, v5, v7}, LX/Ede;->a(JI)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_6

    goto/16 :goto_1

    .line 2150522
    :catch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Long-Integer header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150523
    goto/16 :goto_0

    .line 2150524
    :pswitch_7
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v4

    .line 2150525
    if-eqz v4, :cond_1

    .line 2150526
    :try_start_6
    invoke-virtual {v3, v4, v7}, LX/Ede;->a([BI)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_8

    goto/16 :goto_1

    .line 2150527
    :catch_7
    goto/16 :goto_1

    .line 2150528
    :catch_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Text-String header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150529
    goto/16 :goto_0

    .line 2150530
    :pswitch_8
    invoke-static {p0}, LX/Edf;->e(Ljava/io/ByteArrayInputStream;)LX/EdS;

    move-result-object v4

    .line 2150531
    if-eqz v4, :cond_1

    .line 2150532
    :try_start_7
    invoke-virtual {v3, v4, v7}, LX/Ede;->a(LX/EdS;I)V
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_a

    goto/16 :goto_1

    .line 2150533
    :catch_9
    goto/16 :goto_1

    .line 2150534
    :catch_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Encoded-String-Value header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150535
    goto/16 :goto_0

    .line 2150536
    :catch_b
    move-object v0, v2

    goto/16 :goto_0

    .line 2150537
    :catch_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Encoded-String-Value header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150538
    goto/16 :goto_0

    .line 2150539
    :pswitch_9
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150540
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v8

    .line 2150541
    :try_start_8
    invoke-static {p0}, LX/Edf;->h(Ljava/io/ByteArrayInputStream;)J
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_e

    move-result-wide v4

    .line 2150542
    const/16 v9, 0x81

    if-ne v9, v8, :cond_5

    .line 2150543
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    .line 2150544
    :cond_5
    :try_start_9
    invoke-virtual {v3, v4, v5, v7}, LX/Ede;->a(JI)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_d

    goto/16 :goto_1

    .line 2150545
    :catch_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Long-Integer header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150546
    goto/16 :goto_0

    .line 2150547
    :catch_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Long-Integer header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150548
    goto/16 :goto_0

    .line 2150549
    :pswitch_a
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150550
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    .line 2150551
    if-ne v12, v4, :cond_8

    .line 2150552
    invoke-static {p0}, LX/Edf;->e(Ljava/io/ByteArrayInputStream;)LX/EdS;

    move-result-object v4

    .line 2150553
    if-eqz v4, :cond_7

    .line 2150554
    invoke-virtual {v4}, LX/EdS;->b()[B

    move-result-object v8

    .line 2150555
    if-eqz v8, :cond_7

    .line 2150556
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>([B)V

    .line 2150557
    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 2150558
    if-lez v8, :cond_6

    .line 2150559
    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 2150560
    :cond_6
    :try_start_a
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, LX/EdS;->a([B)V
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_10

    .line 2150561
    :cond_7
    :goto_2
    const/16 v5, 0x89

    :try_start_b
    invoke-virtual {v3, v4, v5}, LX/Ede;->a(LX/EdS;I)V
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_12

    goto/16 :goto_1

    .line 2150562
    :catch_f
    goto/16 :goto_1

    .line 2150563
    :catch_10
    move-object v0, v2

    goto/16 :goto_0

    .line 2150564
    :cond_8
    :try_start_c
    new-instance v4, LX/EdS;

    const-string v5, "insert-address-token"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v4, v5}, LX/EdS;-><init>([B)V
    :try_end_c
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_11

    goto :goto_2

    .line 2150565
    :catch_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Encoded-String-Value header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150566
    goto/16 :goto_0

    .line 2150567
    :catch_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Encoded-String-Value header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150568
    goto/16 :goto_0

    .line 2150569
    :pswitch_b
    invoke-virtual {p0, v6}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150570
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    .line 2150571
    if-lt v4, v12, :cond_c

    .line 2150572
    if-ne v12, v4, :cond_9

    .line 2150573
    :try_start_d
    const-string v4, "personal"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/16 v5, 0x8a

    invoke-virtual {v3, v4, v5}, LX/Ede;->a([BI)V

    goto/16 :goto_1

    .line 2150574
    :catch_13
    goto/16 :goto_1

    .line 2150575
    :cond_9
    const/16 v5, 0x81

    if-ne v5, v4, :cond_a

    .line 2150576
    const-string v4, "advertisement"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/16 v5, 0x8a

    invoke-virtual {v3, v4, v5}, LX/Ede;->a([BI)V
    :try_end_d
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_13
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_14

    goto/16 :goto_1

    .line 2150577
    :catch_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Text-String header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150578
    goto/16 :goto_0

    .line 2150579
    :cond_a
    const/16 v5, 0x82

    if-ne v5, v4, :cond_b

    .line 2150580
    :try_start_e
    const-string v4, "informational"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/16 v5, 0x8a

    invoke-virtual {v3, v4, v5}, LX/Ede;->a([BI)V

    goto/16 :goto_1

    .line 2150581
    :cond_b
    const/16 v5, 0x83

    if-ne v5, v4, :cond_1

    .line 2150582
    const-string v4, "auto"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/16 v5, 0x8a

    invoke-virtual {v3, v4, v5}, LX/Ede;->a([BI)V
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_e} :catch_13
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_14

    goto/16 :goto_1

    .line 2150583
    :cond_c
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150584
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v4

    .line 2150585
    if-eqz v4, :cond_1

    .line 2150586
    const/16 v5, 0x8a

    :try_start_f
    invoke-virtual {v3, v4, v5}, LX/Ede;->a([BI)V
    :try_end_f
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_f} :catch_15
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_16

    goto/16 :goto_1

    .line 2150587
    :catch_15
    goto/16 :goto_1

    .line 2150588
    :catch_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Text-String header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150589
    goto/16 :goto_0

    .line 2150590
    :pswitch_c
    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    .line 2150591
    const/16 v5, 0x8d

    :try_start_10
    invoke-virtual {v3, v4, v5}, LX/Ede;->a(II)V
    :try_end_10
    .catch LX/EdU; {:try_start_10 .. :try_end_10} :catch_17
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_18

    goto/16 :goto_1

    .line 2150592
    :catch_17
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set invalid Octet value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " into the header filed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150593
    goto/16 :goto_0

    .line 2150594
    :catch_18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Octet header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150595
    goto/16 :goto_0

    .line 2150596
    :pswitch_d
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150597
    :try_start_11
    invoke-static {p0}, LX/Edf;->i(Ljava/io/ByteArrayInputStream;)J
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_1a

    .line 2150598
    invoke-static {p0}, LX/Edf;->e(Ljava/io/ByteArrayInputStream;)LX/EdS;

    move-result-object v4

    .line 2150599
    if-eqz v4, :cond_1

    .line 2150600
    const/16 v5, 0xa0

    :try_start_12
    invoke-virtual {v3, v4, v5}, LX/Ede;->a(LX/EdS;I)V
    :try_end_12
    .catch Ljava/lang/NullPointerException; {:try_start_12 .. :try_end_12} :catch_19
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_1b

    goto/16 :goto_1

    .line 2150601
    :catch_19
    goto/16 :goto_1

    .line 2150602
    :catch_1a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not Integer-Value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150603
    goto/16 :goto_0

    .line 2150604
    :catch_1b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Encoded-String-Value header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150605
    goto/16 :goto_0

    .line 2150606
    :pswitch_e
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150607
    :try_start_13
    invoke-static {p0}, LX/Edf;->i(Ljava/io/ByteArrayInputStream;)J
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_1d

    .line 2150608
    :try_start_14
    invoke-static {p0}, LX/Edf;->h(Ljava/io/ByteArrayInputStream;)J

    move-result-wide v4

    .line 2150609
    const/16 v8, 0xa1

    invoke-virtual {v3, v4, v5, v8}, LX/Ede;->a(JI)V
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_1c

    goto/16 :goto_1

    .line 2150610
    :catch_1c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Long-Integer header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150611
    goto/16 :goto_0

    .line 2150612
    :catch_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not Integer-Value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150613
    goto/16 :goto_0

    .line 2150614
    :pswitch_f
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150615
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    .line 2150616
    invoke-static {p0}, LX/Edf;->e(Ljava/io/ByteArrayInputStream;)LX/EdS;

    goto/16 :goto_1

    .line 2150617
    :pswitch_10
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150618
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    .line 2150619
    :try_start_15
    invoke-static {p0}, LX/Edf;->i(Ljava/io/ByteArrayInputStream;)J
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_15} :catch_1e

    goto/16 :goto_1

    .line 2150620
    :catch_1e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not Integer-Value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150621
    goto/16 :goto_0

    .line 2150622
    :pswitch_11
    invoke-static {p0, v2}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;)[B

    goto/16 :goto_1

    .line 2150623
    :pswitch_12
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    .line 2150624
    invoke-static {p0, v4}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;)[B

    move-result-object v0

    .line 2150625
    if-eqz v0, :cond_d

    .line 2150626
    const/16 v5, 0x84

    :try_start_16
    invoke-virtual {v3, v0, v5}, LX/Ede;->a([BI)V
    :try_end_16
    .catch Ljava/lang/NullPointerException; {:try_start_16 .. :try_end_16} :catch_20
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_1f

    .line 2150627
    :cond_d
    :goto_3
    const/16 v0, 0x99

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    sput-object v0, LX/Edf;->f:[B

    .line 2150628
    const/16 v0, 0x83

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    sput-object v0, LX/Edf;->e:[B

    move v0, v1

    .line 2150629
    goto/16 :goto_1

    .line 2150630
    :catch_1f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is not Text-String header field!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 2150631
    goto/16 :goto_0

    :cond_e
    move-object v0, v3

    .line 2150632
    goto/16 :goto_0

    .line 2150633
    :catch_20
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x81
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_12
        :pswitch_5
        :pswitch_4
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_7
        :pswitch_2
        :pswitch_c
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_1
        :pswitch_7
        :pswitch_4
        :pswitch_8
        :pswitch_4
        :pswitch_4
        :pswitch_9
        :pswitch_7
        :pswitch_5
        :pswitch_d
        :pswitch_e
        :pswitch_4
        :pswitch_4
        :pswitch_f
        :pswitch_4
        :pswitch_8
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_10
        :pswitch_4
        :pswitch_10
        :pswitch_6
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_11
        :pswitch_6
        :pswitch_4
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_7
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x89
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;Ljava/lang/Integer;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/ByteArrayInputStream;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v9, 0x7f

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/16 v6, 0x81

    const/4 v1, 0x0

    .line 2150042
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150043
    :cond_0
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150044
    :cond_1
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v2

    .line 2150045
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2150046
    :goto_0
    if-lez v0, :cond_c

    .line 2150047
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    .line 2150048
    sget-boolean v4, LX/Edf;->a:Z

    if-nez v4, :cond_2

    if-ne v7, v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150049
    :cond_2
    add-int/lit8 v0, v0, -0x1

    .line 2150050
    sparse-switch v3, :sswitch_data_0

    .line 2150051
    invoke-static {p0, v0}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;I)I

    move-result v3

    if-ne v7, v3, :cond_b

    .line 2150052
    const-string v3, "PduParser"

    const-string v4, "Corrupt Content-Type"

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2150053
    :sswitch_0
    invoke-virtual {p0, v8}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150054
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150055
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150056
    if-le v0, v9, :cond_4

    .line 2150057
    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150058
    sget-object v3, LX/Edd;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2150059
    sget-object v3, LX/Edd;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2150060
    const/16 v3, 0x83

    invoke-virtual {p1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150061
    :cond_3
    :goto_1
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150062
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v0, v2, v0

    sub-int v0, v3, v0

    .line 2150063
    goto :goto_0

    .line 2150064
    :cond_4
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150065
    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 2150066
    const/16 v3, 0x83

    invoke-virtual {p1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 2150067
    :sswitch_1
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150068
    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    .line 2150069
    const/16 v3, 0x99

    invoke-virtual {p1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150070
    :cond_5
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150071
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v0, v2, v0

    sub-int v0, v3, v0

    .line 2150072
    goto :goto_0

    .line 2150073
    :sswitch_2
    invoke-virtual {p0, v8}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150074
    invoke-static {p0}, LX/Edf;->f(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150075
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150076
    const/16 v3, 0x20

    if-le v0, v3, :cond_6

    if-lt v0, v9, :cond_7

    :cond_6
    if-nez v0, :cond_9

    .line 2150077
    :cond_7
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v3

    .line 2150078
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0}, LX/EdP;->a(Ljava/lang/String;)I

    move-result v0

    .line 2150079
    const/16 v4, 0x81

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2150080
    :cond_8
    :goto_2
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150081
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v0, v2, v0

    sub-int v0, v3, v0

    .line 2150082
    goto/16 :goto_0

    .line 2150083
    :catch_0
    move-exception v0

    .line 2150084
    const-string v4, "PduParser"

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2150085
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 2150086
    :cond_9
    invoke-static {p0}, LX/Edf;->i(Ljava/io/ByteArrayInputStream;)J

    move-result-wide v4

    long-to-int v0, v4

    .line 2150087
    if-eqz p1, :cond_8

    .line 2150088
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 2150089
    :sswitch_3
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150090
    if-eqz v0, :cond_a

    if-eqz p1, :cond_a

    .line 2150091
    const/16 v3, 0x97

    invoke-virtual {p1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150092
    :cond_a
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150093
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v0, v2, v0

    sub-int v0, v3, v0

    .line 2150094
    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 2150095
    goto/16 :goto_0

    .line 2150096
    :cond_c
    if-eqz v0, :cond_d

    .line 2150097
    const-string v0, "PduParser"

    const-string v1, "Corrupt Content-Type"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150098
    :cond_d
    return-void

    :sswitch_data_0
    .sparse-switch
        0x81 -> :sswitch_2
        0x83 -> :sswitch_0
        0x85 -> :sswitch_3
        0x89 -> :sswitch_0
        0x8a -> :sswitch_1
        0x97 -> :sswitch_3
        0x99 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Ljava/io/ByteArrayInputStream;LX/Edg;I)Z
    .locals 9

    .prologue
    const/16 v8, 0x7f

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2150390
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150391
    :cond_0
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150392
    :cond_1
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_2

    if-gtz p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150393
    :cond_2
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v3

    move v0, p3

    .line 2150394
    :cond_3
    :goto_0
    if-lez v0, :cond_11

    .line 2150395
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v4

    .line 2150396
    sget-boolean v5, LX/Edf;->a:Z

    if-nez v5, :cond_4

    if-ne v7, v4, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150397
    :cond_4
    add-int/lit8 v0, v0, -0x1

    .line 2150398
    if-le v4, v8, :cond_d

    .line 2150399
    sparse-switch v4, :sswitch_data_0

    .line 2150400
    invoke-static {p1, v0}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;I)I

    move-result v0

    if-ne v7, v0, :cond_c

    .line 2150401
    const-string v0, "PduParser"

    const-string v2, "Corrupt Part headers"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150402
    :goto_1
    return v1

    .line 2150403
    :sswitch_0
    invoke-static {p1, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150404
    if-eqz v0, :cond_5

    .line 2150405
    invoke-virtual {p2, v0}, LX/Edg;->c([B)V

    .line 2150406
    :cond_5
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150407
    sub-int v0, v3, v0

    sub-int v0, p3, v0

    .line 2150408
    goto :goto_0

    .line 2150409
    :sswitch_1
    invoke-static {p1, v2}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150410
    if-eqz v0, :cond_6

    .line 2150411
    invoke-virtual {p2, v0}, LX/Edg;->b([B)V

    .line 2150412
    :cond_6
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150413
    sub-int v0, v3, v0

    sub-int v0, p3, v0

    .line 2150414
    goto :goto_0

    .line 2150415
    :sswitch_2
    iget-boolean v4, p0, LX/Edf;->g:Z

    if-eqz v4, :cond_3

    .line 2150416
    invoke-static {p1}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150417
    invoke-virtual {p1, v2}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150418
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v4

    .line 2150419
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v5

    .line 2150420
    const/16 v6, 0x80

    if-ne v5, v6, :cond_9

    .line 2150421
    sget-object v5, LX/Edg;->a:[B

    invoke-virtual {p2, v5}, LX/Edg;->d([B)V

    .line 2150422
    :goto_2
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v5

    .line 2150423
    sub-int v5, v4, v5

    if-ge v5, v0, :cond_8

    .line 2150424
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v5

    .line 2150425
    const/16 v6, 0x98

    if-ne v5, v6, :cond_7

    .line 2150426
    invoke-static {p1, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v5

    invoke-virtual {p2, v5}, LX/Edg;->h([B)V

    .line 2150427
    :cond_7
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v5

    .line 2150428
    sub-int v6, v4, v5

    if-ge v6, v0, :cond_8

    .line 2150429
    sub-int/2addr v4, v5

    sub-int/2addr v0, v4

    .line 2150430
    new-array v4, v0, [B

    .line 2150431
    invoke-virtual {p1, v4, v1, v0}, Ljava/io/ByteArrayInputStream;->read([BII)I

    .line 2150432
    :cond_8
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150433
    sub-int v0, v3, v0

    sub-int v0, p3, v0

    .line 2150434
    goto/16 :goto_0

    .line 2150435
    :cond_9
    const/16 v6, 0x81

    if-ne v5, v6, :cond_a

    .line 2150436
    sget-object v5, LX/Edg;->b:[B

    invoke-virtual {p2, v5}, LX/Edg;->d([B)V

    goto :goto_2

    .line 2150437
    :cond_a
    const/16 v6, 0x82

    if-ne v5, v6, :cond_b

    .line 2150438
    sget-object v5, LX/Edg;->c:[B

    invoke-virtual {p2, v5}, LX/Edg;->d([B)V

    goto :goto_2

    .line 2150439
    :cond_b
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150440
    invoke-static {p1, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v5

    invoke-virtual {p2, v5}, LX/Edg;->d([B)V

    goto :goto_2

    :cond_c
    move v0, v1

    .line 2150441
    goto/16 :goto_0

    .line 2150442
    :cond_d
    const/16 v5, 0x20

    if-lt v4, v5, :cond_f

    if-gt v4, v8, :cond_f

    .line 2150443
    invoke-static {p1, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150444
    invoke-static {p1, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v4

    .line 2150445
    const-string v5, "Content-Transfer-Encoding"

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-ne v2, v0, :cond_e

    .line 2150446
    invoke-virtual {p2, v4}, LX/Edg;->f([B)V

    .line 2150447
    :cond_e
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150448
    sub-int v0, v3, v0

    sub-int v0, p3, v0

    .line 2150449
    goto/16 :goto_0

    .line 2150450
    :cond_f
    invoke-static {p1, v0}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;I)I

    move-result v0

    if-ne v7, v0, :cond_10

    .line 2150451
    const-string v0, "PduParser"

    const-string v2, "Corrupt Part headers"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_10
    move v0, v1

    .line 2150452
    goto/16 :goto_0

    .line 2150453
    :cond_11
    if-eqz v0, :cond_12

    .line 2150454
    const-string v0, "PduParser"

    const-string v2, "Corrupt Part headers"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_12
    move v1, v2

    .line 2150455
    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x8e -> :sswitch_0
        0xae -> :sswitch_2
        0xc0 -> :sswitch_1
        0xc5 -> :sswitch_2
    .end sparse-switch
.end method

.method private static a(Ljava/io/ByteArrayInputStream;I)[B
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2150380
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150381
    :cond_0
    invoke-virtual {p0, v2}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150382
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150383
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    const/4 v1, -0x1

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150384
    :cond_1
    if-ne v2, p1, :cond_2

    const/16 v1, 0x22

    if-ne v1, v0, :cond_2

    .line 2150385
    invoke-virtual {p0, v2}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150386
    :goto_0
    invoke-static {p0, p1}, LX/Edf;->b(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    return-object v0

    .line 2150387
    :cond_2
    if-nez p1, :cond_3

    const/16 v1, 0x7f

    if-ne v1, v0, :cond_3

    .line 2150388
    invoke-virtual {p0, v2}, Ljava/io/ByteArrayInputStream;->mark(I)V

    goto :goto_0

    .line 2150389
    :cond_3
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    goto :goto_0
.end method

.method private static a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/ByteArrayInputStream;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)[B"
        }
    .end annotation

    .prologue
    const/16 v7, 0x20

    const/4 v3, 0x1

    const/4 v6, -0x1

    const/16 v5, 0x7f

    const/4 v4, 0x0

    .line 2150345
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150346
    :cond_0
    invoke-virtual {p0, v3}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150347
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150348
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    if-ne v6, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150349
    :cond_1
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150350
    and-int/lit16 v0, v0, 0xff

    .line 2150351
    if-ge v0, v7, :cond_8

    .line 2150352
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    move-result v1

    .line 2150353
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v2

    .line 2150354
    invoke-virtual {p0, v3}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150355
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150356
    sget-boolean v3, LX/Edf;->a:Z

    if-nez v3, :cond_2

    if-ne v6, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150357
    :cond_2
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150358
    and-int/lit16 v0, v0, 0xff

    .line 2150359
    if-lt v0, v7, :cond_5

    if-gt v0, v5, :cond_5

    .line 2150360
    invoke-static {p0, v4}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    .line 2150361
    :goto_0
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v3

    .line 2150362
    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 2150363
    if-lez v1, :cond_3

    .line 2150364
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p0, p1, v2}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;Ljava/lang/Integer;)V

    .line 2150365
    :cond_3
    if-gez v1, :cond_4

    .line 2150366
    const-string v0, "PduParser"

    const-string v1, "Corrupt MMS message"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150367
    sget-object v0, LX/Edd;->a:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2150368
    :cond_4
    :goto_1
    return-object v0

    .line 2150369
    :cond_5
    if-le v0, v5, :cond_7

    .line 2150370
    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150371
    sget-object v3, LX/Edd;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 2150372
    sget-object v3, LX/Edd;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0

    .line 2150373
    :cond_6
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150374
    invoke-static {p0, v4}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    goto :goto_0

    .line 2150375
    :cond_7
    const-string v0, "PduParser"

    const-string v1, "Corrupt content-type"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150376
    sget-object v0, LX/Edd;->a:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_1

    .line 2150377
    :cond_8
    if-gt v0, v5, :cond_9

    .line 2150378
    invoke-static {p0, v4}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v0

    goto :goto_1

    .line 2150379
    :cond_9
    sget-object v0, LX/Edd;->a:[Ljava/lang/String;

    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_1
.end method

.method private b(Ljava/io/ByteArrayInputStream;)LX/EdY;
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2150273
    if-nez p1, :cond_0

    move-object v0, v2

    .line 2150274
    :goto_0
    return-object v0

    .line 2150275
    :cond_0
    invoke-static {p1}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;)I

    move-result v6

    .line 2150276
    new-instance v3, LX/EdY;

    invoke-direct {v3}, LX/EdY;-><init>()V

    move v4, v5

    .line 2150277
    :goto_1
    if-ge v4, v6, :cond_e

    .line 2150278
    invoke-static {p1}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;)I

    move-result v7

    .line 2150279
    invoke-static {p1}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;)I

    move-result v8

    .line 2150280
    new-instance v1, LX/Edg;

    invoke-direct {v1}, LX/Edg;-><init>()V

    .line 2150281
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v9

    .line 2150282
    if-gtz v9, :cond_1

    move-object v0, v2

    .line 2150283
    goto :goto_0

    .line 2150284
    :cond_1
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    .line 2150285
    invoke-static {p1, v10}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;Landroid/util/SparseArray;)[B

    move-result-object v0

    .line 2150286
    if-eqz v0, :cond_4

    .line 2150287
    invoke-virtual {v1, v0}, LX/Edg;->e([B)V

    .line 2150288
    :goto_2
    const/16 v0, 0x97

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    .line 2150289
    if-eqz v0, :cond_2

    .line 2150290
    invoke-virtual {v1, v0}, LX/Edg;->g([B)V

    .line 2150291
    :cond_2
    const/16 v0, 0x81

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2150292
    if-eqz v0, :cond_3

    .line 2150293
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/Edg;->a(I)V

    .line 2150294
    :cond_3
    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 2150295
    sub-int v0, v9, v0

    sub-int v0, v7, v0

    .line 2150296
    if-lez v0, :cond_5

    .line 2150297
    invoke-direct {p0, p1, v1, v0}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;LX/Edg;I)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v2

    .line 2150298
    goto :goto_0

    .line 2150299
    :cond_4
    sget-object v0, LX/Edd;->a:[Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Edg;->e([B)V

    goto :goto_2

    .line 2150300
    :cond_5
    if-gez v0, :cond_6

    move-object v0, v2

    .line 2150301
    goto :goto_0

    .line 2150302
    :cond_6
    invoke-virtual {v1}, LX/Edg;->e()[B

    move-result-object v0

    if-nez v0, :cond_7

    invoke-virtual {v1}, LX/Edg;->i()[B

    move-result-object v0

    if-nez v0, :cond_7

    invoke-virtual {v1}, LX/Edg;->j()[B

    move-result-object v0

    if-nez v0, :cond_7

    invoke-virtual {v1}, LX/Edg;->c()[B

    move-result-object v0

    if-nez v0, :cond_7

    .line 2150303
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toOctalString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Edg;->c([B)V

    .line 2150304
    :cond_7
    if-lez v8, :cond_c

    .line 2150305
    new-array v0, v8, [B

    .line 2150306
    new-instance v7, Ljava/lang/String;

    invoke-virtual {v1}, LX/Edg;->g()[B

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/String;-><init>([B)V

    .line 2150307
    invoke-virtual {p1, v0, v5, v8}, Ljava/io/ByteArrayInputStream;->read([BII)I

    .line 2150308
    const-string v8, "application/vnd.wap.multipart.alternative"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2150309
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v1}, LX/Edf;->b(Ljava/io/ByteArrayInputStream;)LX/EdY;

    move-result-object v0

    .line 2150310
    invoke-virtual {v0, v5}, LX/EdY;->a(I)LX/Edg;

    move-result-object v0

    .line 2150311
    :goto_3
    invoke-static {v0}, LX/Edf;->a(LX/Edg;)I

    move-result v1

    if-nez v1, :cond_d

    .line 2150312
    invoke-virtual {v3, v5, v0}, LX/EdY;->a(ILX/Edg;)V

    .line 2150313
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    .line 2150314
    :cond_8
    iget-object v7, v1, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v8, 0xc8

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    check-cast v7, [B

    move-object v7, v7

    .line 2150315
    if-eqz v7, :cond_9

    .line 2150316
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    .line 2150317
    const-string v7, "base64"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2150318
    invoke-static {v0}, LX/EdO;->a([B)[B

    move-result-object v0

    .line 2150319
    :cond_9
    :goto_5
    if-nez v0, :cond_b

    move-object v0, v2

    .line 2150320
    goto/16 :goto_0

    .line 2150321
    :cond_a
    const-string v7, "quoted-printable"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2150322
    const/4 v13, -0x1

    const/4 v8, 0x0

    .line 2150323
    if-nez v0, :cond_f

    move-object v7, v8

    .line 2150324
    :goto_6
    move-object v0, v7

    .line 2150325
    goto :goto_5

    .line 2150326
    :cond_b
    iput-object v0, v1, LX/Edg;->f:[B

    .line 2150327
    :cond_c
    move-object v0, v1

    goto :goto_3

    .line 2150328
    :cond_d
    invoke-virtual {v3, v0}, LX/EdY;->a(LX/Edg;)Z

    goto :goto_4

    :cond_e
    move-object v0, v3

    .line 2150329
    goto/16 :goto_0

    .line 2150330
    :cond_f
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2150331
    const/4 v7, 0x0

    :goto_7
    array-length v10, v0

    if-ge v7, v10, :cond_14

    .line 2150332
    aget-byte v10, v0, v7

    .line 2150333
    sget-byte v11, LX/Edi;->a:B

    if-ne v10, v11, :cond_13

    .line 2150334
    const/16 v10, 0xd

    add-int/lit8 v11, v7, 0x1

    :try_start_0
    aget-byte v11, v0, v11

    int-to-char v11, v11

    if-ne v10, v11, :cond_10

    const/16 v10, 0xa

    add-int/lit8 v11, v7, 0x2

    aget-byte v11, v0, v11

    int-to-char v11, v11

    if-ne v10, v11, :cond_10

    .line 2150335
    add-int/lit8 v7, v7, 0x2

    .line 2150336
    :goto_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 2150337
    :cond_10
    add-int/lit8 v7, v7, 0x1

    aget-byte v10, v0, v7

    int-to-char v10, v10

    const/16 v11, 0x10

    invoke-static {v10, v11}, Ljava/lang/Character;->digit(CI)I

    move-result v10

    .line 2150338
    add-int/lit8 v7, v7, 0x1

    aget-byte v11, v0, v7

    int-to-char v11, v11

    const/16 v12, 0x10

    invoke-static {v11, v12}, Ljava/lang/Character;->digit(CI)I

    move-result v11

    .line 2150339
    if-eq v10, v13, :cond_11

    if-ne v11, v13, :cond_12

    :cond_11
    move-object v7, v8

    .line 2150340
    goto :goto_6

    .line 2150341
    :cond_12
    shl-int/lit8 v10, v10, 0x4

    add-int/2addr v10, v11

    int-to-char v10, v10

    invoke-virtual {v9, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 2150342
    :catch_0
    move-object v7, v8

    goto :goto_6

    .line 2150343
    :cond_13
    invoke-virtual {v9, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_8

    .line 2150344
    :cond_14
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    goto :goto_6
.end method

.method private static b(Ljava/io/ByteArrayInputStream;I)[B
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 2150456
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150457
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2150458
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150459
    sget-boolean v2, LX/Edf;->a:Z

    if-nez v2, :cond_1

    if-ne v3, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150460
    :cond_1
    if-eq v3, v0, :cond_7

    if-eqz v0, :cond_7

    .line 2150461
    const/4 v2, 0x2

    if-ne p1, v2, :cond_4

    .line 2150462
    const/4 v2, 0x0

    .line 2150463
    const/16 v4, 0x21

    if-lt v0, v4, :cond_2

    const/16 v4, 0x7e

    if-le v0, v4, :cond_9

    .line 2150464
    :cond_2
    :goto_0
    :sswitch_0
    move v2, v2

    .line 2150465
    if-eqz v2, :cond_3

    .line 2150466
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 2150467
    :cond_3
    :goto_1
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150468
    sget-boolean v2, LX/Edf;->a:Z

    if-nez v2, :cond_1

    if-ne v3, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150469
    :cond_4
    const/4 v2, 0x1

    .line 2150470
    const/16 v4, 0x20

    if-lt v0, v4, :cond_5

    const/16 v4, 0x7e

    if-le v0, v4, :cond_6

    :cond_5
    const/16 v4, 0x80

    if-lt v0, v4, :cond_a

    const/16 v4, 0xff

    if-gt v0, v4, :cond_a

    .line 2150471
    :cond_6
    :goto_2
    :pswitch_0
    move v2, v2

    .line 2150472
    if-eqz v2, :cond_3

    .line 2150473
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_1

    .line 2150474
    :cond_7
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 2150475
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 2150476
    :goto_3
    return-object v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 2150477
    :cond_9
    sparse-switch v0, :sswitch_data_0

    .line 2150478
    const/4 v2, 0x1

    goto :goto_0

    .line 2150479
    :cond_a
    packed-switch v0, :pswitch_data_0

    .line 2150480
    :pswitch_1
    const/4 v2, 0x0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2c -> :sswitch_0
        0x2f -> :sswitch_0
        0x3a -> :sswitch_0
        0x3b -> :sswitch_0
        0x3c -> :sswitch_0
        0x3d -> :sswitch_0
        0x3e -> :sswitch_0
        0x3f -> :sswitch_0
        0x40 -> :sswitch_0
        0x5b -> :sswitch_0
        0x5c -> :sswitch_0
        0x5d -> :sswitch_0
        0x7b -> :sswitch_0
        0x7d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static c(Ljava/io/ByteArrayInputStream;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2150260
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150261
    :cond_0
    const/4 v1, 0x0

    .line 2150262
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150263
    if-ne v0, v3, :cond_1

    .line 2150264
    :goto_0
    return v0

    .line 2150265
    :cond_1
    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_2

    .line 2150266
    shl-int/lit8 v1, v1, 0x7

    .line 2150267
    and-int/lit8 v0, v0, 0x7f

    or-int/2addr v1, v0

    .line 2150268
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150269
    if-ne v0, v3, :cond_1

    goto :goto_0

    .line 2150270
    :cond_2
    shl-int/lit8 v1, v1, 0x7

    .line 2150271
    and-int/lit8 v0, v0, 0x7f

    or-int/2addr v0, v1

    .line 2150272
    goto :goto_0
.end method

.method private static c(Ljava/io/ByteArrayInputStream;I)I
    .locals 2

    .prologue
    .line 2150254
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150255
    :cond_0
    new-array v0, p1, [B

    .line 2150256
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v0

    .line 2150257
    if-ge v0, p1, :cond_1

    .line 2150258
    const/4 v0, -0x1

    .line 2150259
    :cond_1
    return v0
.end method

.method private static d(Ljava/io/ByteArrayInputStream;)I
    .locals 2

    .prologue
    .line 2150245
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150246
    :cond_0
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150247
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    const/4 v1, -0x1

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150248
    :cond_1
    and-int/lit16 v0, v0, 0xff

    .line 2150249
    const/16 v1, 0x1e

    if-gt v0, v1, :cond_2

    .line 2150250
    :goto_0
    return v0

    .line 2150251
    :cond_2
    const/16 v1, 0x1f

    if-ne v0, v1, :cond_3

    .line 2150252
    invoke-static {p0}, LX/Edf;->c(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    goto :goto_0

    .line 2150253
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Value length > LENGTH_QUOTE!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e(Ljava/io/ByteArrayInputStream;)LX/EdS;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2150229
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150230
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150231
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150232
    sget-boolean v3, LX/Edf;->a:Z

    if-nez v3, :cond_1

    const/4 v3, -0x1

    if-ne v3, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150233
    :cond_1
    and-int/lit16 v0, v0, 0xff

    .line 2150234
    if-nez v0, :cond_2

    move-object v0, v2

    .line 2150235
    :goto_0
    return-object v0

    .line 2150236
    :cond_2
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150237
    const/16 v3, 0x20

    if-ge v0, v3, :cond_4

    .line 2150238
    invoke-static {p0}, LX/Edf;->d(Ljava/io/ByteArrayInputStream;)I

    .line 2150239
    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    .line 2150240
    :goto_1
    invoke-static {p0, v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;I)[B

    move-result-object v3

    .line 2150241
    if-eqz v0, :cond_3

    .line 2150242
    :try_start_0
    new-instance v1, LX/EdS;

    invoke-direct {v1, v0, v3}, LX/EdS;-><init>(I[B)V

    move-object v0, v1

    goto :goto_0

    .line 2150243
    :cond_3
    new-instance v0, LX/EdS;

    invoke-direct {v0, v3}, LX/EdS;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2150244
    :catch_0
    move-object v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private static f(Ljava/io/ByteArrayInputStream;)I
    .locals 2

    .prologue
    .line 2150225
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150226
    :cond_0
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150227
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    const/4 v1, -0x1

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150228
    :cond_1
    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static g(Ljava/io/ByteArrayInputStream;)I
    .locals 2

    .prologue
    .line 2150221
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150222
    :cond_0
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150223
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    const/4 v1, -0x1

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150224
    :cond_1
    and-int/lit8 v0, v0, 0x7f

    return v0
.end method

.method private static h(Ljava/io/ByteArrayInputStream;)J
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, -0x1

    .line 2150207
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150208
    :cond_0
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150209
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    if-ne v6, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150210
    :cond_1
    and-int/lit16 v1, v0, 0xff

    .line 2150211
    if-le v1, v7, :cond_2

    .line 2150212
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Octet count greater than 8 and I can\'t represent that!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150213
    :cond_2
    const-wide/16 v2, 0x0

    .line 2150214
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_4

    .line 2150215
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v4

    .line 2150216
    sget-boolean v5, LX/Edf;->a:Z

    if-nez v5, :cond_3

    if-ne v6, v4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150217
    :cond_3
    shl-long/2addr v2, v7

    .line 2150218
    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 2150219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2150220
    :cond_4
    return-wide v2
.end method

.method private static i(Ljava/io/ByteArrayInputStream;)J
    .locals 2

    .prologue
    .line 2150199
    sget-boolean v0, LX/Edf;->a:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150200
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 2150201
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    .line 2150202
    sget-boolean v1, LX/Edf;->a:Z

    if-nez v1, :cond_1

    const/4 v1, -0x1

    if-ne v1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2150203
    :cond_1
    invoke-virtual {p0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 2150204
    const/16 v1, 0x7f

    if-le v0, v1, :cond_2

    .line 2150205
    invoke-static {p0}, LX/Edf;->g(Ljava/io/ByteArrayInputStream;)I

    move-result v0

    int-to-long v0, v0

    .line 2150206
    :goto_0
    return-wide v0

    :cond_2
    invoke-static {p0}, LX/Edf;->h(Ljava/io/ByteArrayInputStream;)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/EdM;
    .locals 15

    .prologue
    const/16 v5, 0x99

    const/4 v6, 0x0

    const/16 v3, 0x80

    const/4 v0, 0x0

    .line 2150099
    iget-object v1, p0, LX/Edf;->b:Ljava/io/ByteArrayInputStream;

    if-nez v1, :cond_1

    .line 2150100
    :cond_0
    :goto_0
    return-object v0

    .line 2150101
    :cond_1
    iget-object v1, p0, LX/Edf;->b:Ljava/io/ByteArrayInputStream;

    invoke-static {v1}, LX/Edf;->a(Ljava/io/ByteArrayInputStream;)LX/Ede;

    move-result-object v1

    iput-object v1, p0, LX/Edf;->c:LX/Ede;

    .line 2150102
    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    if-eqz v1, :cond_0

    .line 2150103
    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    const/16 v2, 0x8c

    invoke-virtual {v1, v2}, LX/Ede;->a(I)I

    move-result v4

    .line 2150104
    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    const/16 v12, 0x89

    const/16 v10, 0x85

    const-wide/16 v13, -0x1

    const/16 v11, 0x98

    const/4 v7, 0x0

    .line 2150105
    if-nez v1, :cond_9

    .line 2150106
    :cond_2
    :goto_1
    move v1, v7

    .line 2150107
    if-eqz v1, :cond_0

    .line 2150108
    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    .line 2150109
    iget-object v2, v1, LX/Ede;->a:Landroid/util/SparseArray;

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v7}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2150110
    if-eqz v1, :cond_5

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-virtual {v1, v5}, LX/Ede;->a(I)I

    move-result v1

    move v2, v1

    .line 2150111
    :goto_3
    if-eq v3, v4, :cond_3

    const/16 v1, 0x84

    if-ne v1, v4, :cond_4

    if-ne v2, v3, :cond_4

    .line 2150112
    :cond_3
    iget-object v1, p0, LX/Edf;->b:Ljava/io/ByteArrayInputStream;

    invoke-direct {p0, v1}, LX/Edf;->b(Ljava/io/ByteArrayInputStream;)LX/EdY;

    move-result-object v1

    iput-object v1, p0, LX/Edf;->d:LX/EdY;

    .line 2150113
    iget-object v1, p0, LX/Edf;->d:LX/EdY;

    if-eqz v1, :cond_0

    .line 2150114
    :cond_4
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 2150115
    :pswitch_0
    new-instance v0, LX/Edn;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    iget-object v2, p0, LX/Edf;->d:LX/EdY;

    invoke-direct {v0, v1, v2}, LX/Edn;-><init>(LX/Ede;LX/EdY;)V

    goto :goto_0

    :cond_5
    move v2, v3

    .line 2150116
    goto :goto_3

    .line 2150117
    :pswitch_1
    new-instance v0, LX/Edm;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/Edm;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2150118
    :pswitch_2
    new-instance v0, LX/EdW;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/EdW;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2150119
    :pswitch_3
    new-instance v0, LX/EdX;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/EdX;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2150120
    :pswitch_4
    new-instance v1, LX/Edl;

    iget-object v4, p0, LX/Edf;->c:LX/Ede;

    iget-object v5, p0, LX/Edf;->d:LX/EdY;

    invoke-direct {v1, v4, v5}, LX/Edl;-><init>(LX/Ede;LX/EdY;)V

    .line 2150121
    if-eq v2, v3, :cond_6

    move-object v0, v1

    .line 2150122
    goto/16 :goto_0

    .line 2150123
    :cond_6
    iget-object v2, v1, LX/EdM;->a:LX/Ede;

    const/16 v3, 0x84

    invoke-virtual {v2, v3}, LX/Ede;->b(I)[B

    move-result-object v2

    move-object v2, v2

    .line 2150124
    if-eqz v2, :cond_0

    .line 2150125
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    .line 2150126
    const-string v2, "application/vnd.wap.multipart.mixed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "application/vnd.wap.multipart.related"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "application/vnd.wap.multipart.alternative"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    move-object v0, v1

    .line 2150127
    goto/16 :goto_0

    .line 2150128
    :cond_8
    const-string v2, "application/vnd.wap.multipart.alternative"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2150129
    iget-object v0, p0, LX/Edf;->d:LX/EdY;

    invoke-virtual {v0, v6}, LX/EdY;->a(I)LX/Edg;

    move-result-object v0

    .line 2150130
    iget-object v2, p0, LX/Edf;->d:LX/EdY;

    .line 2150131
    iget-object v3, v2, LX/EdY;->a:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    .line 2150132
    iget-object v2, p0, LX/Edf;->d:LX/EdY;

    invoke-virtual {v2, v6, v0}, LX/EdY;->a(ILX/Edg;)V

    move-object v0, v1

    .line 2150133
    goto/16 :goto_0

    .line 2150134
    :pswitch_5
    new-instance v0, LX/EdR;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/EdR;-><init>(LX/Ede;)V

    goto/16 :goto_0

    .line 2150135
    :pswitch_6
    new-instance v0, LX/EdN;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/EdN;-><init>(LX/Ede;)V

    goto/16 :goto_0

    .line 2150136
    :pswitch_7
    new-instance v0, LX/Edj;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/Edj;-><init>(LX/Ede;)V

    goto/16 :goto_0

    .line 2150137
    :pswitch_8
    new-instance v0, LX/Edk;

    iget-object v1, p0, LX/Edf;->c:LX/Ede;

    invoke-direct {v0, v1}, LX/Edk;-><init>(LX/Ede;)V

    goto/16 :goto_0

    .line 2150138
    :cond_9
    const/16 v8, 0x8c

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150139
    const/16 v9, 0x8d

    invoke-virtual {v1, v9}, LX/Ede;->a(I)I

    move-result v9

    .line 2150140
    if-eqz v9, :cond_2

    .line 2150141
    packed-switch v8, :pswitch_data_1

    goto/16 :goto_1

    .line 2150142
    :pswitch_9
    const/16 v8, 0x84

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150143
    if-eqz v8, :cond_2

    .line 2150144
    invoke-virtual {v1, v12}, LX/Ede;->c(I)LX/EdS;

    move-result-object v8

    .line 2150145
    if-eqz v8, :cond_2

    .line 2150146
    invoke-virtual {v1, v11}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150147
    if-eqz v8, :cond_2

    .line 2150148
    :cond_a
    const/4 v7, 0x1

    goto/16 :goto_1

    .line 2150149
    :pswitch_a
    const/16 v8, 0x92

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150150
    if-eqz v8, :cond_2

    .line 2150151
    invoke-virtual {v1, v11}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150152
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150153
    :pswitch_b
    const/16 v8, 0x83

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150154
    if-eqz v8, :cond_2

    .line 2150155
    const/16 v8, 0x88

    invoke-virtual {v1, v8}, LX/Ede;->e(I)J

    move-result-wide v9

    .line 2150156
    cmp-long v8, v13, v9

    if-eqz v8, :cond_2

    .line 2150157
    const/16 v8, 0x8a

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150158
    if-eqz v8, :cond_2

    .line 2150159
    const/16 v8, 0x8e

    invoke-virtual {v1, v8}, LX/Ede;->e(I)J

    move-result-wide v9

    .line 2150160
    cmp-long v8, v13, v9

    if-eqz v8, :cond_2

    .line 2150161
    invoke-virtual {v1, v11}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150162
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150163
    :pswitch_c
    const/16 v8, 0x95

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150164
    if-eqz v8, :cond_2

    .line 2150165
    invoke-virtual {v1, v11}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150166
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150167
    :pswitch_d
    const/16 v8, 0x84

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150168
    if-eqz v8, :cond_2

    .line 2150169
    invoke-virtual {v1, v10}, LX/Ede;->e(I)J

    move-result-wide v9

    .line 2150170
    cmp-long v8, v13, v9

    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150171
    :pswitch_e
    invoke-virtual {v1, v10}, LX/Ede;->e(I)J

    move-result-wide v9

    .line 2150172
    cmp-long v8, v13, v9

    if-eqz v8, :cond_2

    .line 2150173
    const/16 v8, 0x8b

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150174
    if-eqz v8, :cond_2

    .line 2150175
    const/16 v8, 0x95

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150176
    if-eqz v8, :cond_2

    .line 2150177
    const/16 v8, 0x97

    invoke-virtual {v1, v8}, LX/Ede;->d(I)[LX/EdS;

    move-result-object v8

    .line 2150178
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150179
    :pswitch_f
    invoke-virtual {v1, v11}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150180
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150181
    :pswitch_10
    invoke-virtual {v1, v10}, LX/Ede;->e(I)J

    move-result-wide v9

    .line 2150182
    cmp-long v8, v13, v9

    if-eqz v8, :cond_2

    .line 2150183
    invoke-virtual {v1, v12}, LX/Ede;->c(I)LX/EdS;

    move-result-object v8

    .line 2150184
    if-eqz v8, :cond_2

    .line 2150185
    const/16 v8, 0x8b

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150186
    if-eqz v8, :cond_2

    .line 2150187
    const/16 v8, 0x9b

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150188
    if-eqz v8, :cond_2

    .line 2150189
    const/16 v8, 0x97

    invoke-virtual {v1, v8}, LX/Ede;->d(I)[LX/EdS;

    move-result-object v8

    .line 2150190
    if-nez v8, :cond_a

    goto/16 :goto_1

    .line 2150191
    :pswitch_11
    invoke-virtual {v1, v12}, LX/Ede;->c(I)LX/EdS;

    move-result-object v8

    .line 2150192
    if-eqz v8, :cond_2

    .line 2150193
    const/16 v8, 0x8b

    invoke-virtual {v1, v8}, LX/Ede;->b(I)[B

    move-result-object v8

    .line 2150194
    if-eqz v8, :cond_2

    .line 2150195
    const/16 v8, 0x9b

    invoke-virtual {v1, v8}, LX/Ede;->a(I)I

    move-result v8

    .line 2150196
    if-eqz v8, :cond_2

    .line 2150197
    const/16 v8, 0x97

    invoke-virtual {v1, v8}, LX/Ede;->d(I)[LX/EdS;

    move-result-object v8

    .line 2150198
    if-nez v8, :cond_a

    goto/16 :goto_1

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x80
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_e
        :pswitch_11
        :pswitch_10
    .end packed-switch
.end method
