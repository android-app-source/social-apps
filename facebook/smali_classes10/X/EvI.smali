.class public final LX/EvI;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2181135
    const-class v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    const v0, 0x70be25a3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FriendsCenterFetchContextualPYMKSuggestionsQuery"

    const-string v6, "2633da64ff6b9d5d6e0ca5291b4568e2"

    const-string v7, "viewer"

    const-string v8, "10155259866171729"

    const-string v9, "10155260460081729"

    .line 2181136
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2181137
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2181138
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2181139
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2181140
    sparse-switch v0, :sswitch_data_0

    .line 2181141
    :goto_0
    return-object p1

    .line 2181142
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2181143
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2181144
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2181145
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2181146
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2181147
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1e8378b -> :sswitch_5
        0x58705dc -> :sswitch_0
        0x5ced2b0 -> :sswitch_1
        0x38b735af -> :sswitch_2
        0x475168b0 -> :sswitch_4
        0x6c6f579a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2181148
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2181149
    :goto_1
    return v0

    .line 2181150
    :pswitch_0
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2181151
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
