.class public final LX/EZ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWa;


# instance fields
.field public a:I

.field public final synthetic b:LX/EZ6;

.field private final c:LX/EZ3;

.field private d:LX/EWa;


# direct methods
.method public constructor <init>(LX/EZ6;)V
    .locals 2

    .prologue
    .line 2138503
    iput-object p1, p0, LX/EZ4;->b:LX/EZ6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138504
    new-instance v0, LX/EZ3;

    invoke-direct {v0, p1}, LX/EZ3;-><init>(LX/EWc;)V

    iput-object v0, p0, LX/EZ4;->c:LX/EZ3;

    .line 2138505
    iget-object v0, p0, LX/EZ4;->c:LX/EZ3;

    invoke-virtual {v0}, LX/EZ3;->a()LX/EYy;

    move-result-object v0

    invoke-virtual {v0}, LX/EYy;->a()LX/EWa;

    move-result-object v0

    iput-object v0, p0, LX/EZ4;->d:LX/EWa;

    .line 2138506
    invoke-virtual {p1}, LX/EZ6;->b()I

    move-result v0

    iput v0, p0, LX/EZ4;->a:I

    .line 2138507
    return-void
.end method


# virtual methods
.method public final a()B
    .locals 1

    .prologue
    .line 2138508
    iget-object v0, p0, LX/EZ4;->d:LX/EWa;

    invoke-interface {v0}, LX/EWa;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2138509
    iget-object v0, p0, LX/EZ4;->c:LX/EZ3;

    invoke-virtual {v0}, LX/EZ3;->a()LX/EYy;

    move-result-object v0

    invoke-virtual {v0}, LX/EYy;->a()LX/EWa;

    move-result-object v0

    iput-object v0, p0, LX/EZ4;->d:LX/EWa;

    .line 2138510
    :cond_0
    iget v0, p0, LX/EZ4;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EZ4;->a:I

    .line 2138511
    iget-object v0, p0, LX/EZ4;->d:LX/EWa;

    invoke-interface {v0}, LX/EWa;->a()B

    move-result v0

    return v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 2138512
    iget v0, p0, LX/EZ4;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138513
    invoke-virtual {p0}, LX/EZ4;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 2138514
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
