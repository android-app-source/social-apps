.class public LX/Dom;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "[B>;"
        }
    .end annotation
.end field

.field private final b:LX/2P4;

.field private final c:LX/2Oy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042494
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dom;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2P4;LX/2Oy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042490
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Dom;->a:Ljava/util/Map;

    .line 2042491
    iput-object p1, p0, LX/Dom;->b:LX/2P4;

    .line 2042492
    iput-object p2, p0, LX/Dom;->c:LX/2Oy;

    .line 2042493
    return-void
.end method

.method public static a(LX/0QB;)LX/Dom;
    .locals 8

    .prologue
    .line 2042495
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042496
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042497
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042498
    if-nez v1, :cond_0

    .line 2042499
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042500
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042501
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042502
    sget-object v1, LX/Dom;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042503
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042504
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042505
    :cond_1
    if-nez v1, :cond_4

    .line 2042506
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042507
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042508
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042509
    new-instance p0, LX/Dom;

    invoke-static {v0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v1

    check-cast v1, LX/2P4;

    invoke-static {v0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v7

    check-cast v7, LX/2Oy;

    invoke-direct {p0, v1, v7}, LX/Dom;-><init>(LX/2P4;LX/2Oy;)V

    .line 2042510
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042511
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042512
    if-nez v1, :cond_2

    .line 2042513
    sget-object v0, LX/Dom;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dom;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042514
    :goto_1
    if-eqz v0, :cond_3

    .line 2042515
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042516
    :goto_3
    check-cast v0, LX/Dom;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042517
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042518
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042519
    :catchall_1
    move-exception v0

    .line 2042520
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042521
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042522
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042523
    :cond_2
    :try_start_8
    sget-object v0, LX/Dom;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dom;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2042478
    iget-object v0, p0, LX/Dom;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042479
    iget-object v0, p0, LX/Dom;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 2042480
    :goto_0
    return-object v0

    .line 2042481
    :cond_0
    iget-object v0, p0, LX/Dom;->b:LX/2P4;

    invoke-virtual {v0, p1}, LX/2P4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v0

    .line 2042482
    if-nez v0, :cond_1

    .line 2042483
    const/4 v0, 0x0

    goto :goto_0

    .line 2042484
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/Dom;->c:LX/2Oy;

    invoke-virtual {v1, v0}, LX/2Oy;->b([B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2042485
    iget-object v1, p0, LX/Dom;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2042486
    :catch_0
    move-exception v0

    .line 2042487
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to decrypt thread key"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042488
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2042476
    iget-object v0, p0, LX/Dom;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2042477
    return-void
.end method
