.class public LX/Dxa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Z

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2062989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2062990
    iput-boolean v0, p0, LX/Dxa;->a:Z

    .line 2062991
    iput-boolean v0, p0, LX/Dxa;->b:Z

    .line 2062992
    return-void
.end method

.method public static a(LX/0QB;)LX/Dxa;
    .locals 3

    .prologue
    .line 2062993
    const-class v1, LX/Dxa;

    monitor-enter v1

    .line 2062994
    :try_start_0
    sget-object v0, LX/Dxa;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2062995
    sput-object v2, LX/Dxa;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2062996
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062997
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2062998
    new-instance v0, LX/Dxa;

    invoke-direct {v0}, LX/Dxa;-><init>()V

    .line 2062999
    move-object v0, v0

    .line 2063000
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2063001
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Dxa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2063002
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2063003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2063004
    if-eqz p1, :cond_2

    .line 2063005
    const-string v1, "pick_hc_pic"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, LX/Dxa;->a:Z

    .line 2063006
    const-string v1, "pick_pic_lite"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, LX/Dxa;->b:Z

    .line 2063007
    iget-boolean v1, p0, LX/Dxa;->a:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/Dxa;->b:Z

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v1, "Cannot have both modes at the same time"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2063008
    :cond_2
    return-void
.end method
