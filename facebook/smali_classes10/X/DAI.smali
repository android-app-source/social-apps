.class public LX/DAI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/DAH;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971008
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1971009
    check-cast p1, LX/DAH;

    .line 1971010
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "setting"

    invoke-virtual {p1}, LX/DAH;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1971011
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "FriendFinderCallLogSettingPost"

    .line 1971012
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1971013
    move-object v1, v1

    .line 1971014
    const-string v2, "POST"

    .line 1971015
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1971016
    move-object v1, v1

    .line 1971017
    const-string v2, "method/FriendFinderCallLogSettingPost"

    .line 1971018
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1971019
    move-object v1, v1

    .line 1971020
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1971021
    move-object v0, v1

    .line 1971022
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1971023
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1971024
    move-object v0, v0

    .line 1971025
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1971026
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1971027
    const/4 v0, 0x0

    return-object v0
.end method
