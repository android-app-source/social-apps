.class public final LX/D1L;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field public final synthetic val$extraData:Lcom/facebook/graphql/error/GraphQLError;


# direct methods
.method public constructor <init>(Lcom/facebook/storelocator/StoreLocatorMapDelegate;Lcom/facebook/graphql/error/GraphQLError;)V
    .locals 2

    .prologue
    .line 1956491
    iput-object p1, p0, LX/D1L;->this$0:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iput-object p2, p0, LX/D1L;->val$extraData:Lcom/facebook/graphql/error/GraphQLError;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1956492
    const-string v0, "error_code"

    iget-object v1, p0, LX/D1L;->val$extraData:Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/D1L;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956493
    const-string v0, "error_message"

    iget-object v1, p0, LX/D1L;->val$extraData:Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/D1L;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956494
    return-void
.end method
