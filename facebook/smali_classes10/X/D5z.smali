.class public LX/D5z;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3FP;
.implements LX/392;
.implements LX/3FR;
.implements LX/3FS;
.implements LX/3FT;
.implements LX/1a7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Pk;",
        ":",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        ">",
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/3FP;",
        "LX/392;",
        "LX/3FR;",
        "LX/3FS;",
        "LX/3FT;",
        "LX/1a7;"
    }
.end annotation


# instance fields
.field public A:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/9DA;

.field private final D:LX/3It;

.field public a:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D49;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2mq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/9Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/9Du;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/9F8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/9Da;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1EP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:Lcom/facebook/video/player/RichVideoPlayer;

.field public p:Lcom/facebook/video/player/RichVideoPlayer;

.field public q:LX/2pa;

.field public r:Ljava/lang/String;

.field public s:LX/D4U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private t:LX/7QP;

.field private u:Z

.field private v:Z

.field public w:Landroid/view/ViewStub;

.field public x:Landroid/view/ViewStub;

.field public y:LX/9F7;

.field public z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1964928
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D5z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1964929
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1964930
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D5z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1964931
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1964932
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1964933
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1964934
    iput-object v0, p0, LX/D5z;->z:LX/0Ot;

    .line 1964935
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1964936
    iput-object v0, p0, LX/D5z;->B:LX/0Ot;

    .line 1964937
    new-instance v0, LX/D5u;

    invoke-direct {v0, p0}, LX/D5u;-><init>(LX/D5z;)V

    iput-object v0, p0, LX/D5z;->D:LX/3It;

    .line 1964938
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/D5z;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1964939
    const v0, 0x7f030274

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1964940
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964941
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, LX/D5z;->getPlayerType()LX/04G;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1964942
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04H;->ELIGIBLE:LX/04H;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1964943
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/D5z;->D:LX/3It;

    .line 1964944
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1964945
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/D5z;->o:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964946
    iget-object v0, p0, LX/D5z;->h:LX/0ad;

    sget-short v1, LX/0ws;->eC:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/D5z;->v:Z

    .line 1964947
    const/4 v2, 0x0

    .line 1964948
    const v0, 0x7f0d0918

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/D5z;->w:Landroid/view/ViewStub;

    .line 1964949
    iget-object v0, p0, LX/D5z;->i:LX/9Do;

    iget-object v1, p0, LX/D5z;->w:Landroid/view/ViewStub;

    .line 1964950
    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/9Do;->a(Landroid/view/ViewStub;LX/9D1;)V

    .line 1964951
    iget-object v0, p0, LX/D5z;->i:LX/9Do;

    new-instance v1, LX/D5v;

    invoke-direct {v1, p0}, LX/D5v;-><init>(LX/D5z;)V

    .line 1964952
    iput-object v1, v0, LX/9Do;->e:Landroid/view/View$OnClickListener;

    .line 1964953
    const v0, 0x7f0d009f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/D5z;->x:Landroid/view/ViewStub;

    .line 1964954
    iget-object v0, p0, LX/D5z;->j:LX/9Du;

    iget-object v1, p0, LX/D5z;->x:Landroid/view/ViewStub;

    .line 1964955
    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/9Du;->a(Landroid/view/ViewStub;LX/9D1;)V

    .line 1964956
    iget-object v0, p0, LX/D5z;->j:LX/9Du;

    iget-object v1, p0, LX/D5z;->i:LX/9Do;

    invoke-virtual {v0, v1}, LX/9Bh;->a(LX/9Bh;)V

    .line 1964957
    iget-object v0, p0, LX/D5z;->k:LX/9F8;

    iget-object v1, p0, LX/D5z;->j:LX/9Du;

    invoke-virtual {v0, v1, v2, v2}, LX/9F8;->a(LX/9Du;LX/9D1;LX/9CC;)LX/9F7;

    move-result-object v0

    iput-object v0, p0, LX/D5z;->y:LX/9F7;

    .line 1964958
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/D5w;

    invoke-direct {v1, p0}, LX/D5w;-><init>(LX/D5z;)V

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1964959
    return-void
.end method

.method private static a(LX/D5z;LX/1C2;LX/D49;LX/2mn;Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;LX/2mq;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/0sV;LX/0ad;LX/9Do;LX/9Du;LX/9F8;LX/0Ot;LX/9Da;LX/0Zb;LX/1EP;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D5z;",
            "LX/1C2;",
            "LX/D49;",
            "LX/2mn;",
            "Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;",
            "LX/2mq;",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;",
            "LX/0sV;",
            "LX/0ad;",
            "LX/9Do;",
            "LX/9Du;",
            "LX/9F8;",
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;",
            "LX/9Da;",
            "LX/0Zb;",
            "LX/1EP;",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1964960
    iput-object p1, p0, LX/D5z;->a:LX/1C2;

    iput-object p2, p0, LX/D5z;->b:LX/D49;

    iput-object p3, p0, LX/D5z;->c:LX/2mn;

    iput-object p4, p0, LX/D5z;->d:Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    iput-object p5, p0, LX/D5z;->e:LX/2mq;

    iput-object p6, p0, LX/D5z;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object p7, p0, LX/D5z;->g:LX/0sV;

    iput-object p8, p0, LX/D5z;->h:LX/0ad;

    iput-object p9, p0, LX/D5z;->i:LX/9Do;

    iput-object p10, p0, LX/D5z;->j:LX/9Du;

    iput-object p11, p0, LX/D5z;->k:LX/9F8;

    iput-object p12, p0, LX/D5z;->z:LX/0Ot;

    iput-object p13, p0, LX/D5z;->l:LX/9Da;

    iput-object p14, p0, LX/D5z;->m:LX/0Zb;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/D5z;->n:LX/1EP;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/D5z;->B:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 19

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, LX/D5z;

    invoke-static/range {v17 .. v17}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static/range {v17 .. v17}, LX/D49;->a(LX/0QB;)LX/D49;

    move-result-object v3

    check-cast v3, LX/D49;

    invoke-static/range {v17 .. v17}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v4

    check-cast v4, LX/2mn;

    invoke-static/range {v17 .. v17}, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    invoke-static/range {v17 .. v17}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v6

    check-cast v6, LX/2mq;

    invoke-static/range {v17 .. v17}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static/range {v17 .. v17}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v8

    check-cast v8, LX/0sV;

    invoke-static/range {v17 .. v17}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static/range {v17 .. v17}, LX/9Do;->a(LX/0QB;)LX/9Do;

    move-result-object v10

    check-cast v10, LX/9Do;

    invoke-static/range {v17 .. v17}, LX/9Du;->a(LX/0QB;)LX/9Du;

    move-result-object v11

    check-cast v11, LX/9Du;

    const-class v12, LX/9F8;

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/9F8;

    const/16 v13, 0x3851

    move-object/from16 v0, v17

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v17 .. v17}, LX/9Da;->a(LX/0QB;)LX/9Da;

    move-result-object v14

    check-cast v14, LX/9Da;

    invoke-static/range {v17 .. v17}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v15

    check-cast v15, LX/0Zb;

    invoke-static/range {v17 .. v17}, LX/1EP;->a(LX/0QB;)LX/1EP;

    move-result-object v16

    check-cast v16, LX/1EP;

    const/16 v18, 0x7bc

    invoke-static/range {v17 .. v18}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v1 .. v17}, LX/D5z;->a(LX/D5z;LX/1C2;LX/D49;LX/2mn;Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;LX/2mq;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/0sV;LX/0ad;LX/9Do;LX/9Du;LX/9F8;LX/0Ot;LX/9Da;LX/0Zb;LX/1EP;LX/0Ot;)V

    return-void
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 1964961
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public static k(LX/D5z;)Z
    .locals 2

    .prologue
    .line 1964962
    iget-object v0, p0, LX/D5z;->o:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1964963
    invoke-static {p0}, LX/D5z;->k(LX/D5z;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1964964
    invoke-virtual {p0}, LX/D5z;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964965
    iget-object v0, p0, LX/D5z;->o:Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1964966
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1964967
    :cond_0
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 1

    .prologue
    .line 1964968
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1964969
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1964970
    return-void
.end method

.method public final a(LX/7QP;)V
    .locals 2

    .prologue
    .line 1964971
    iget-object v0, p0, LX/D5z;->t:LX/7QP;

    if-eq v0, p1, :cond_0

    .line 1964972
    iput-object p1, p0, LX/D5z;->t:LX/7QP;

    .line 1964973
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/7QP;)V

    .line 1964974
    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Z)V

    .line 1964975
    :cond_0
    return-void

    .line 1964976
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1964977
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;ILcom/facebook/feed/rows/core/props/FeedProps;LX/2oO;LX/D4U;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2oO;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1964978
    iput-object p5, p0, LX/D5z;->s:LX/D4U;

    .line 1964979
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1964980
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1964981
    if-eqz p1, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v4, 0x4ed245b

    if-ne v1, v4, :cond_5

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1964982
    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, LX/D5z;->r:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1964983
    iget-boolean v1, p0, LX/D5z;->v:Z

    if-nez v1, :cond_0

    .line 1964984
    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1964985
    :cond_0
    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v1, p0, LX/D5z;->r:Ljava/lang/String;

    .line 1964986
    :cond_1
    iget-object v1, p0, LX/D5z;->e:LX/2mq;

    invoke-virtual {v1, v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v1

    .line 1964987
    iget-object v4, p0, LX/D5z;->c:LX/2mn;

    invoke-virtual {v4, p3, v1}, LX/2mn;->a(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v4

    .line 1964988
    iget v5, v4, LX/3FO;->a:I

    iget v4, v4, LX/3FO;->b:I

    .line 1964989
    iget-object v6, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 1964990
    if-eqz v6, :cond_2

    instance-of v7, v6, Landroid/widget/FrameLayout$LayoutParams;

    if-nez v7, :cond_3

    .line 1964991
    :cond_2
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v6, v5, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1964992
    :cond_3
    iput v4, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1964993
    iput v5, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1964994
    iget-object v7, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v7, v6}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1964995
    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    iput-object v4, p0, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964996
    iget-object v4, p0, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/D49;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0P2;

    move-result-object v4

    const-string v5, "IsAutoplayKey"

    invoke-virtual {p4}, LX/2oO;->k()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    const-string v5, "SeekPositionMsKey"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    .line 1964997
    new-instance v5, LX/2pZ;

    invoke-direct {v5}, LX/2pZ;-><init>()V

    .line 1964998
    iput-object p1, v5, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1964999
    move-object v5, v5

    .line 1965000
    invoke-virtual {v5, v4}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v4

    iget-object v5, p0, LX/D5z;->b:LX/D49;

    iget-object v6, p0, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v5, v6}, LX/D49;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v4

    invoke-virtual {v4}, LX/2pZ;->b()LX/2pa;

    move-result-object v4

    .line 1965001
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 1965002
    iget-object v6, p0, LX/D5z;->d:Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    iget-object v7, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v8, LX/D5y;

    iget-object v9, p0, LX/D5z;->s:LX/D4U;

    invoke-direct {v8, p0, v9}, LX/D5y;-><init>(LX/D5z;LX/D4U;)V

    invoke-virtual {v6, v7, v4, v8}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1965003
    iget-object v6, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v5}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_6

    :goto_1
    invoke-virtual {v6, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1965004
    iget-boolean v1, p0, LX/D5z;->v:Z

    if-eqz v1, :cond_7

    .line 1965005
    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v4}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2pa;)V

    .line 1965006
    :goto_2
    iput-object v4, p0, LX/D5z;->q:LX/2pa;

    .line 1965007
    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1965008
    const/4 v1, 0x1

    .line 1965009
    iget-object v2, p0, LX/D5z;->g:LX/0sV;

    iget-boolean v2, v2, LX/0sV;->t:Z

    if-nez v2, :cond_8

    .line 1965010
    const/4 v1, 0x0

    .line 1965011
    :goto_3
    move v1, v1

    .line 1965012
    if-nez v1, :cond_4

    .line 1965013
    iget-object v1, p0, LX/D5z;->e:LX/2mq;

    iget-object v2, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v2, v0}, LX/2mq;->a(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1965014
    :cond_4
    return-void

    :cond_5
    move v1, v3

    .line 1965015
    goto/16 :goto_0

    :cond_6
    move v2, v3

    .line 1965016
    goto :goto_1

    .line 1965017
    :cond_7
    iget-object v1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v4}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_2

    .line 1965018
    :cond_8
    iget-object v2, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1965019
    iget-object v2, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setNeedCentering(Z)V

    goto :goto_3
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 1965020
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    if-ne v0, p1, :cond_0

    .line 1965021
    :goto_0
    return-void

    .line 1965022
    :cond_0
    iget-object v0, p0, LX/D5z;->o:Lcom/facebook/video/player/RichVideoPlayer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1965023
    iput-object p1, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1965024
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/D5z;->r:Ljava/lang/String;

    .line 1965025
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1965026
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1965027
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LX/D5z;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1965028
    invoke-virtual {p0}, LX/D5z;->requestLayout()V

    .line 1965029
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/D5z;->D:LX/3It;

    .line 1965030
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1965031
    goto :goto_0
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 1964925
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1964926
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1964927
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1964881
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1964882
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1964883
    :cond_0
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 1964884
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1964885
    iget-object v0, p0, LX/D5z;->o:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1964886
    iget-object v0, p0, LX/D5z;->i:LX/9Do;

    invoke-virtual {v0}, LX/9Bh;->b()V

    .line 1964887
    return-void
.end method

.method public bridge synthetic getAdditionalPlugins()Ljava/util/List;
    .locals 1

    .prologue
    .line 1964888
    const/4 v0, 0x0

    move-object v0, v0

    .line 1964889
    return-object v0
.end method

.method public getAndClearShowLiveCommentDialogFragment()Z
    .locals 2

    .prologue
    .line 1964890
    iget-boolean v0, p0, LX/D5z;->u:Z

    .line 1964891
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/D5z;->u:Z

    .line 1964892
    return v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1964893
    sget-object v0, LX/04G;->CHANNEL_PLAYER:LX/04G;

    return-object v0
.end method

.method public getPluginSelector()LX/3Ge;
    .locals 1

    .prologue
    .line 1964894
    iget-object v0, p0, LX/D5z;->d:Lcom/facebook/video/channelfeed/ChannelInlineRichVideoPlayerPluginSelector;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1964880
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 1964895
    iget-object v0, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public final hasTransientState()Z
    .locals 1

    .prologue
    .line 1964896
    invoke-static {p0}, LX/D5z;->k(LX/D5z;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1964897
    const/4 v0, 0x0

    .line 1964898
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->hasTransientState()Z

    move-result v0

    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1964899
    invoke-virtual {p0}, LX/D5z;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setShowLiveCommentDialogFragment(Z)V
    .locals 0

    .prologue
    .line 1964900
    iput-boolean p1, p0, LX/D5z;->u:Z

    .line 1964901
    return-void
.end method

.method public final x()V
    .locals 9

    .prologue
    .line 1964910
    iget-object v0, p0, LX/D5z;->g:LX/0sV;

    invoke-virtual {v0}, LX/0sV;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1964911
    :cond_0
    :goto_0
    iget-object v1, p0, LX/D5z;->a:LX/1C2;

    iget-object v2, p0, LX/D5z;->q:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04G;->CHANNEL_PLAYER:LX/04G;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v5

    iget-object v6, p0, LX/D5z;->r:Ljava/lang/String;

    iget-object v7, p0, LX/D5z;->s:LX/D4U;

    check-cast v7, LX/D4U;

    .line 1964912
    iget-object v8, v7, LX/D4U;->q:LX/04D;

    move-object v7, v8

    .line 1964913
    iget-object v8, p0, LX/D5z;->q:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v1 .. v8}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1964914
    invoke-virtual {p0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->x()V

    .line 1964915
    return-void

    .line 1964916
    :cond_1
    iget-object v0, p0, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964917
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1964918
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1964919
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1964920
    iget-object v1, p0, LX/D5z;->C:LX/9DA;

    if-nez v1, :cond_2

    .line 1964921
    new-instance v1, LX/D5x;

    invoke-direct {v1, p0}, LX/D5x;-><init>(LX/D5z;)V

    iput-object v1, p0, LX/D5z;->C:LX/9DA;

    .line 1964922
    :cond_2
    iget-object v1, p0, LX/D5z;->l:LX/9Da;

    iget-object v2, p0, LX/D5z;->C:LX/9DA;

    .line 1964923
    iput-object v2, v1, LX/9Da;->u:LX/9DA;

    .line 1964924
    iget-object v1, p0, LX/D5z;->l:LX/9Da;

    iget-object v2, p0, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9Da;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method

.method public final y()V
    .locals 9

    .prologue
    .line 1964902
    invoke-virtual {p0}, LX/D5z;->e()V

    .line 1964903
    iget-object v0, p0, LX/D5z;->g:LX/0sV;

    invoke-virtual {v0}, LX/0sV;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1964904
    :goto_0
    iget-object v1, p0, LX/D5z;->a:LX/1C2;

    iget-object v2, p0, LX/D5z;->q:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04G;->CHANNEL_PLAYER:LX/04G;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v5

    iget-object v6, p0, LX/D5z;->r:Ljava/lang/String;

    iget-object v7, p0, LX/D5z;->s:LX/D4U;

    check-cast v7, LX/D4U;

    .line 1964905
    iget-object v8, v7, LX/D4U;->q:LX/04D;

    move-object v7, v8

    .line 1964906
    iget-object v8, p0, LX/D5z;->q:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v1 .. v8}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1964907
    invoke-virtual {p0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->y()V

    .line 1964908
    return-void

    .line 1964909
    :cond_0
    iget-object v0, p0, LX/D5z;->l:LX/9Da;

    invoke-virtual {v0}, LX/9Da;->a()V

    goto :goto_0
.end method
