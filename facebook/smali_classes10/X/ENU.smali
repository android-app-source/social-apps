.class public final LX/ENU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ENV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "LX/8d6;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public final synthetic d:LX/ENV;


# direct methods
.method public constructor <init>(LX/ENV;)V
    .locals 1

    .prologue
    .line 2112702
    iput-object p1, p0, LX/ENU;->d:LX/ENV;

    .line 2112703
    move-object v0, p1

    .line 2112704
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2112705
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2112706
    const-string v0, "SearchResultsFlexibleNewsContextFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2112707
    if-ne p0, p1, :cond_1

    .line 2112708
    :cond_0
    :goto_0
    return v0

    .line 2112709
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2112710
    goto :goto_0

    .line 2112711
    :cond_3
    check-cast p1, LX/ENU;

    .line 2112712
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2112713
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2112714
    if-eq v2, v3, :cond_0

    .line 2112715
    iget-object v2, p0, LX/ENU;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENU;->a:LX/CzL;

    iget-object v3, p1, LX/ENU;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2112716
    goto :goto_0

    .line 2112717
    :cond_5
    iget-object v2, p1, LX/ENU;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2112718
    :cond_6
    iget-object v2, p0, LX/ENU;->b:LX/1Pg;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ENU;->b:LX/1Pg;

    iget-object v3, p1, LX/ENU;->b:LX/1Pg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2112719
    goto :goto_0

    .line 2112720
    :cond_8
    iget-object v2, p1, LX/ENU;->b:LX/1Pg;

    if-nez v2, :cond_7

    .line 2112721
    :cond_9
    iget v2, p0, LX/ENU;->c:I

    iget v3, p1, LX/ENU;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2112722
    goto :goto_0
.end method
