.class public abstract LX/DHd;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2fC;
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 1982228
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1982229
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1982230
    return-void
.end method


# virtual methods
.method public final a(IZZ)V
    .locals 0

    .prologue
    .line 1982231
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1982232
    iget-boolean v0, p0, LX/DHd;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3bd09197

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982233
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1982234
    const/4 v1, 0x1

    .line 1982235
    iput-boolean v1, p0, LX/DHd;->a:Z

    .line 1982236
    const/16 v1, 0x2d

    const v2, -0x213571d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3e9513f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982237
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1982238
    const/4 v1, 0x0

    .line 1982239
    iput-boolean v1, p0, LX/DHd;->a:Z

    .line 1982240
    const/16 v1, 0x2d

    const v2, -0x28bb5e0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setWidth(I)V
    .locals 1

    .prologue
    .line 1982241
    invoke-virtual {p0}, LX/DHd;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DHd;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, p1, :cond_0

    .line 1982242
    invoke-virtual {p0}, LX/DHd;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1982243
    :cond_0
    return-void
.end method
