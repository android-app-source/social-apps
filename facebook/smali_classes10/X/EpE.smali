.class public final LX/EpE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/2dj;

.field public final synthetic b:J

.field public final synthetic c:LX/2h8;

.field public final synthetic d:LX/5P2;

.field public final synthetic e:LX/2do;

.field public final synthetic f:LX/2hZ;

.field public final synthetic g:Landroid/content/Context;

.field public final synthetic h:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/2dj;JLX/2h8;LX/5P2;LX/2do;LX/2hZ;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2170084
    iput-object p1, p0, LX/EpE;->a:LX/2dj;

    iput-wide p2, p0, LX/EpE;->b:J

    iput-object p4, p0, LX/EpE;->c:LX/2h8;

    iput-object p5, p0, LX/EpE;->d:LX/5P2;

    iput-object p6, p0, LX/EpE;->e:LX/2do;

    iput-object p7, p0, LX/EpE;->f:LX/2hZ;

    iput-object p8, p0, LX/EpE;->g:Landroid/content/Context;

    iput-object p9, p0, LX/EpE;->h:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 2170085
    iget-object v1, p0, LX/EpE;->a:LX/2dj;

    iget-wide v2, p0, LX/EpE;->b:J

    iget-object v4, p0, LX/EpE;->c:LX/2h8;

    const/4 v5, 0x0

    iget-object v6, p0, LX/EpE;->d:LX/5P2;

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2170086
    iget-object v1, p0, LX/EpE;->e:LX/2do;

    new-instance v2, LX/2f2;

    iget-wide v4, p0, LX/EpE;->b:J

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v6, 0x1

    invoke-direct {v2, v4, v5, v3, v6}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2170087
    new-instance v1, LX/EpD;

    invoke-direct {v1, p0}, LX/EpD;-><init>(LX/EpE;)V

    iget-object v2, p0, LX/EpE;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2170088
    return-void
.end method
