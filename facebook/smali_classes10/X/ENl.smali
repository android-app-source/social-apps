.class public final LX/ENl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:LX/1Pg;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;LX/1Pg;LX/CzL;)V
    .locals 0

    .prologue
    .line 2113351
    iput-object p1, p0, LX/ENl;->c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    iput-object p2, p0, LX/ENl;->a:LX/1Pg;

    iput-object p3, p0, LX/ENl;->b:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 9

    .prologue
    .line 2113352
    sget-object v0, LX/20X;->SHARE:LX/20X;

    if-ne p2, v0, :cond_0

    .line 2113353
    iget-object v0, p0, LX/ENl;->c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->c:LX/CvY;

    iget-object v1, p0, LX/ENl;->a:LX/1Pg;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->SHARE:LX/8ch;

    iget-object v3, p0, LX/ENl;->a:LX/1Pg;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ENl;->b:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ENl;->b:LX/CzL;

    iget-object v5, p0, LX/ENl;->a:LX/1Pg;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v5, p0, LX/ENl;->a:LX/1Pg;

    check-cast v5, LX/CxP;

    iget-object v7, p0, LX/ENl;->b:LX/CzL;

    invoke-interface {v5, v7}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    iget-object v7, p0, LX/ENl;->b:LX/CzL;

    .line 2113354
    iget-object v8, v7, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v7, v8

    .line 2113355
    invoke-static {v7}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v5, v7, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2113356
    iget-object v1, p0, LX/ENl;->c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    iget-object v0, p0, LX/ENl;->b:LX/CzL;

    .line 2113357
    iget-object v2, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 2113358
    check-cast v0, LX/8d6;

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 2113359
    sget-object v4, LX/21D;->SEARCH:LX/21D;

    const-string v5, "searchShareButton"

    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-interface {v0}, LX/8d6;->bi()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v2, v3

    :goto_0
    invoke-static {v2}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v4, v5, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 2113360
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const-string v4, "search_ufi"

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2113361
    iget-object v4, v1, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->b:LX/1Kf;

    const/16 v5, 0x6dc

    iget-object v6, v1, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->a:Landroid/app/Activity;

    invoke-interface {v4, v3, v2, v5, v6}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2113362
    :cond_0
    return-void

    .line 2113363
    :cond_1
    invoke-interface {v0}, LX/8d6;->bi()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->j()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
