.class public LX/EBG;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:I

.field public h:I

.field private i:I

.field public j:Z

.field public k:I

.field private l:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2087045
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2087046
    const/4 v0, 0x0

    iput v0, p0, LX/EBG;->i:I

    .line 2087047
    invoke-direct {p0}, LX/EBG;->a()V

    .line 2087048
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2087050
    const-class v0, LX/EBG;

    invoke-static {v0, p0}, LX/EBG;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2087051
    const v0, 0x7f03018e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2087052
    const v0, 0x7f0d06c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    .line 2087053
    const v0, 0x7f0d06c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EBG;->e:Landroid/view/View;

    .line 2087054
    const v0, 0x7f0d06c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EBG;->f:Landroid/widget/TextView;

    .line 2087055
    iput-boolean v1, p0, LX/EBG;->j:Z

    .line 2087056
    iput v1, p0, LX/EBG;->k:I

    .line 2087057
    const/4 v0, 0x0

    iput v0, p0, LX/EBG;->l:F

    .line 2087058
    return-void
.end method

.method private static a(LX/EBG;LX/0hL;LX/0wM;LX/154;)V
    .locals 0

    .prologue
    .line 2087049
    iput-object p1, p0, LX/EBG;->a:LX/0hL;

    iput-object p2, p0, LX/EBG;->b:LX/0wM;

    iput-object p3, p0, LX/EBG;->c:LX/154;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/EBG;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/EBG;

    invoke-static {v2}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v2}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v2

    check-cast v2, LX/154;

    invoke-static {p0, v0, v1, v2}, LX/EBG;->a(LX/EBG;LX/0hL;LX/0wM;LX/154;)V

    return-void
.end method


# virtual methods
.method public final a(LX/EBF;IIII)V
    .locals 4

    .prologue
    .line 2087027
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    .line 2087028
    iget-object v1, p1, LX/EBF;->b:Landroid/text/SpannableString;

    move-object v1, v1

    .line 2087029
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087030
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setWidth(I)V

    .line 2087031
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, LX/EBG;->l:F

    .line 2087032
    iput p4, p0, LX/EBG;->g:I

    .line 2087033
    iget-object v0, p0, LX/EBG;->e:Landroid/view/View;

    iget-object v1, p0, LX/EBG;->b:LX/0wM;

    .line 2087034
    iget v2, p1, LX/EBF;->a:I

    move v2, v2

    .line 2087035
    const/4 v3, 0x0

    invoke-virtual {v1, p5, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2087036
    iget-object v1, p0, LX/EBG;->f:Landroid/widget/TextView;

    .line 2087037
    iget v0, p1, LX/EBF;->c:I

    move v0, v0

    .line 2087038
    if-lez v0, :cond_1

    iget-object v0, p0, LX/EBG;->c:LX/154;

    .line 2087039
    iget v2, p1, LX/EBF;->c:I

    move v2, v2

    .line 2087040
    invoke-virtual {v0, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2087041
    return-void

    .line 2087042
    :cond_0
    iget v0, p1, LX/EBF;->c:I

    move v0, v0

    .line 2087043
    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    goto :goto_0

    .line 2087044
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 2087015
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 2087016
    iget v0, p0, LX/EBG;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2087017
    :cond_0
    :goto_0
    return-void

    .line 2087018
    :cond_1
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    .line 2087019
    iget-object v0, p0, LX/EBG;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, LX/EBG;->g:I

    .line 2087020
    :goto_1
    invoke-virtual {p0}, LX/EBG;->getWidth()I

    move-result v2

    sub-int v1, v2, v1

    sub-int v0, v1, v0

    iget v1, p0, LX/EBG;->i:I

    sub-int/2addr v0, v1

    .line 2087021
    iget v1, p0, LX/EBG;->l:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v1, p0, LX/EBG;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2087022
    new-instance v2, LX/4mW;

    iget-object v1, p0, LX/EBG;->e:Landroid/view/View;

    iget v3, p0, LX/EBG;->h:I

    invoke-direct {v2, v1, v3, v0}, LX/4mW;-><init>(Landroid/view/View;II)V

    .line 2087023
    iget-boolean v0, p0, LX/EBG;->j:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/EBG;->k:I

    int-to-long v0, v0

    :goto_2
    invoke-virtual {v2, v0, v1}, LX/4mW;->setDuration(J)V

    .line 2087024
    invoke-virtual {p0, v2}, LX/EBG;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 2087025
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2087026
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_2
.end method

.method public setBarAnimationEnabled(Z)V
    .locals 0

    .prologue
    .line 2087059
    iput-boolean p1, p0, LX/EBG;->j:Z

    .line 2087060
    return-void
.end method

.method public setBarAnimationTime(I)V
    .locals 0

    .prologue
    .line 2087013
    iput p1, p0, LX/EBG;->k:I

    .line 2087014
    return-void
.end method

.method public setBarHeight(I)V
    .locals 2

    .prologue
    .line 2087009
    iget-object v0, p0, LX/EBG;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2087010
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2087011
    iget-object v1, p0, LX/EBG;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2087012
    return-void
.end method

.method public setBarMinWidth(I)V
    .locals 0

    .prologue
    .line 2087007
    iput p1, p0, LX/EBG;->h:I

    .line 2087008
    return-void
.end method

.method public setLabelBarSpacing(I)V
    .locals 3

    .prologue
    .line 2086990
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2086991
    iget-object v1, p0, LX/EBG;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2086992
    iget-object v2, p0, LX/EBG;->a:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2086993
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2086994
    iput p1, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2086995
    :goto_0
    iget-object v2, p0, LX/EBG;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2086996
    iget-object v0, p0, LX/EBG;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2086997
    iget-object v0, p0, LX/EBG;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    mul-int/lit8 p1, p1, 0x2

    :cond_0
    iput p1, p0, LX/EBG;->i:I

    .line 2086998
    return-void

    .line 2086999
    :cond_1
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2087000
    iput p1, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method public setLabelTextColor(I)V
    .locals 1

    .prologue
    .line 2087005
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2087006
    return-void
.end method

.method public setLabelTextsize(I)V
    .locals 3

    .prologue
    .line 2087003
    iget-object v0, p0, LX/EBG;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2087004
    return-void
.end method

.method public setValueVisibility(I)V
    .locals 1

    .prologue
    .line 2087001
    iget-object v0, p0, LX/EBG;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2087002
    return-void
.end method
