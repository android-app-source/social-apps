.class public LX/D9e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/D9b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field private final c:LX/D9c;

.field private final d:LX/D9d;

.field private final e:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(LX/0Sh;Landroid/content/Context;Landroid/view/WindowManager;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970364
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/D9e;->a:Ljava/util/Set;

    .line 1970365
    new-instance v0, LX/D9c;

    invoke-direct {v0, p0}, LX/D9c;-><init>(LX/D9e;)V

    iput-object v0, p0, LX/D9e;->c:LX/D9c;

    .line 1970366
    iput-object p1, p0, LX/D9e;->b:LX/0Sh;

    .line 1970367
    new-instance v0, LX/D9d;

    invoke-direct {v0, p0, p2}, LX/D9d;-><init>(LX/D9e;Landroid/content/Context;)V

    iput-object v0, p0, LX/D9e;->d:LX/D9d;

    .line 1970368
    iput-object p3, p0, LX/D9e;->e:Landroid/view/WindowManager;

    .line 1970369
    return-void
.end method
