.class public LX/E1i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile r:LX/E1i;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/Bla;

.field private final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final f:LX/1nG;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E1d;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/EAc;

.field private final j:LX/1nD;

.field private final k:LX/B9y;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0hy;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/BOb;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1b2;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2070894
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MAPS_LAUNCH_EXTERNAL_MAP_APP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/E1i;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;LX/Bla;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/0Ot;LX/0Ot;LX/EAc;LX/1nD;LX/B9y;LX/0Ot;LX/0hy;LX/0Ot;LX/BOb;LX/0Ot;)V
    .locals 4
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/Bla;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/1nG;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/E1d;",
            ">;",
            "LX/EAc;",
            "LX/1nD;",
            "LX/B9y;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0hy;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/BOb;",
            "LX/0Ot",
            "<",
            "LX/1b2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2070895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2070896
    iput-object p1, p0, LX/E1i;->b:LX/0Ot;

    .line 2070897
    iput-object p2, p0, LX/E1i;->c:LX/0Or;

    .line 2070898
    iput-object p3, p0, LX/E1i;->d:LX/Bla;

    .line 2070899
    iput-object p4, p0, LX/E1i;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2070900
    iput-object p5, p0, LX/E1i;->f:LX/1nG;

    .line 2070901
    iput-object p6, p0, LX/E1i;->g:LX/0Ot;

    .line 2070902
    iput-object p7, p0, LX/E1i;->h:LX/0Ot;

    .line 2070903
    iput-object p8, p0, LX/E1i;->i:LX/EAc;

    .line 2070904
    iput-object p9, p0, LX/E1i;->j:LX/1nD;

    .line 2070905
    iput-object p10, p0, LX/E1i;->k:LX/B9y;

    .line 2070906
    iput-object p11, p0, LX/E1i;->l:LX/0Ot;

    .line 2070907
    move-object/from16 v0, p12

    iput-object v0, p0, LX/E1i;->m:LX/0hy;

    .line 2070908
    move-object/from16 v0, p13

    iput-object v0, p0, LX/E1i;->n:LX/0Ot;

    .line 2070909
    move-object/from16 v0, p14

    iput-object v0, p0, LX/E1i;->o:LX/BOb;

    .line 2070910
    move-object/from16 v0, p15

    iput-object v0, p0, LX/E1i;->p:LX/0Ot;

    .line 2070911
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/E1i;->q:LX/0Px;

    .line 2070912
    return-void
.end method

.method public static final a()LX/Cfl;
    .locals 3

    .prologue
    .line 2070913
    sget-object v0, LX/0ax;->eb:Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/Cfc;->NEARBY_FRIENDS_TAP:LX/Cfc;

    invoke-static {v0, v1, v2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZLX/Cfc;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070914
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {p3, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070915
    const-string v1, "extra_show_attribution"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070916
    const-string v1, "mediaset_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070917
    const-string v1, "owner_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070918
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p2, p6, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static final a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;
    .locals 8
    .param p0    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2070919
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v6}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;J[JLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)LX/Cfl;
    .locals 5
    .param p0    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070920
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2070921
    const-string v1, "fullscreen_gallery_source"

    sget-object v2, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    invoke-virtual {v2}, LX/74S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070922
    const-string v1, "photo_fbid"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070923
    const-string v1, "extra_photo_set_fb_id_array"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2070924
    const-string v1, "extra_show_attribution"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070925
    const-string v1, "photo_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070926
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070927
    new-instance v1, LX/Cfl;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->PHOTO_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;)LX/Cfl;
    .locals 4
    .param p1    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070928
    if-nez p1, :cond_0

    const-string v0, "page_events_list"

    .line 2070929
    :goto_0
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_CREATE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/Long;)Landroid/content/Intent;

    move-result-object v0

    .line 2070930
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->PAGE_EVENT_CREATE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    .line 2070931
    :cond_0
    iget-object v0, p1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070882
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->t()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2070883
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launch_activity_for_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reaction_request_code"

    const/16 v2, 0xc34

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2070884
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->ADD_PAGE_COVER_PHOTO:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070936
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    .line 2070937
    iput-object p1, v0, LX/5m9;->f:Ljava/lang/String;

    .line 2070938
    move-object v0, v0

    .line 2070939
    iput-object p2, v0, LX/5m9;->h:Ljava/lang/String;

    .line 2070940
    move-object v0, v0

    .line 2070941
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2070942
    sget-object v1, LX/21D;->REACTION:LX/21D;

    const-string v2, "addPhotosFromReaction"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070943
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->REACTION:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    .line 2070944
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2070945
    move-object v0, v1

    .line 2070946
    invoke-static {p0, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launch_activity_for_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reaction_request_code"

    const/16 v2, 0x6dc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2070947
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->ADD_PHOTOS_AT_PLACE_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 5

    .prologue
    .line 2070948
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "reactionNonAdminPagePhoto"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2070949
    iput-object p2, v1, LX/89I;->c:Ljava/lang/String;

    .line 2070950
    move-object v1, v1

    .line 2070951
    iput-object p3, v1, LX/89I;->d:Ljava/lang/String;

    .line 2070952
    move-object v1, v1

    .line 2070953
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070954
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PAGE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    .line 2070955
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2070956
    move-object v0, v1

    .line 2070957
    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    .line 2070958
    invoke-static {p0, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2070959
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/net/Uri;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070960
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2070961
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2070962
    const-string v1, "launch_external_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070963
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->CALL_PHONE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/net/Uri;LX/Cfc;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070964
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2070965
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2070966
    const-string v1, "launch_external_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070967
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Landroid/content/Context;)LX/Cfl;
    .locals 4
    .param p0    # Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2070968
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2070969
    :cond_0
    const/4 v0, 0x0

    .line 2070970
    :goto_0
    return-object v0

    .line 2070971
    :cond_1
    sget-object v0, LX/04D;->REACTION_OVERLAY:LX/04D;

    invoke-static {p1, v0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/content/Context;LX/04D;)Landroid/content/Intent;

    move-result-object v1

    .line 2070972
    const-string v0, "video_graphql_object"

    invoke-static {v1, v0, p0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2070973
    new-instance v0, LX/Cfl;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->VIDEO_TAP:LX/Cfc;

    invoke-direct {v0, v2, v3, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;Ljava/lang/String;)LX/Cfl;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070974
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2070975
    sget-object v1, LX/21D;->REACTION:LX/21D;

    invoke-static {v1, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2070976
    if-nez p0, :cond_0

    .line 2070977
    const/4 v2, 0x0

    .line 2070978
    :goto_0
    move-object v2, v2

    .line 2070979
    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070980
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2070981
    new-instance v1, LX/Cfl;

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    .line 2070982
    :cond_0
    new-instance v2, LX/4X0;

    invoke-direct {v2}, LX/4X0;-><init>()V

    .line 2070983
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2070984
    iput-object v3, v2, LX/4X0;->b:Ljava/lang/String;

    .line 2070985
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v3

    .line 2070986
    if-nez v3, :cond_1

    .line 2070987
    const/4 v4, 0x0

    .line 2070988
    :goto_1
    move-object v3, v4

    .line 2070989
    iput-object v3, v2, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2070990
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->d()LX/5LG;

    move-result-object v3

    .line 2070991
    if-nez v3, :cond_2

    .line 2070992
    const/4 v4, 0x0

    .line 2070993
    :goto_2
    move-object v3, v4

    .line 2070994
    iput-object v3, v2, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 2070995
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$TaggableActivityIconModel;

    move-result-object v3

    .line 2070996
    if-nez v3, :cond_6

    .line 2070997
    const/4 v4, 0x0

    .line 2070998
    :goto_3
    move-object v3, v4

    .line 2070999
    iput-object v3, v2, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 2071000
    invoke-virtual {v2}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    goto :goto_0

    .line 2071001
    :cond_1
    new-instance v4, LX/4XR;

    invoke-direct {v4}, LX/4XR;-><init>()V

    .line 2071002
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2071003
    iput-object v5, v4, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2071004
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2071005
    iput-object v5, v4, LX/4XR;->fO:Ljava/lang/String;

    .line 2071006
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2071007
    iput-object v5, v4, LX/4XR;->iB:Ljava/lang/String;

    .line 2071008
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 2071009
    iput-object v5, v4, LX/4XR;->oU:Ljava/lang/String;

    .line 2071010
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    goto :goto_1

    .line 2071011
    :cond_2
    new-instance v4, LX/4Z3;

    invoke-direct {v4}, LX/4Z3;-><init>()V

    .line 2071012
    invoke-interface {v3}, LX/5LG;->B()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v5

    .line 2071013
    if-nez v5, :cond_3

    .line 2071014
    const/4 v6, 0x0

    .line 2071015
    :goto_4
    move-object v5, v6

    .line 2071016
    iput-object v5, v4, LX/4Z3;->b:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 2071017
    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v5

    .line 2071018
    if-nez v5, :cond_4

    .line 2071019
    const/4 v6, 0x0

    .line 2071020
    :goto_5
    move-object v5, v6

    .line 2071021
    iput-object v5, v4, LX/4Z3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2071022
    invoke-interface {v3}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v5

    .line 2071023
    if-nez v5, :cond_5

    .line 2071024
    const/4 v6, 0x0

    .line 2071025
    :goto_6
    move-object v5, v6

    .line 2071026
    iput-object v5, v4, LX/4Z3;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2071027
    invoke-interface {v3}, LX/5LG;->j()Ljava/lang/String;

    move-result-object v5

    .line 2071028
    iput-object v5, v4, LX/4Z3;->e:Ljava/lang/String;

    .line 2071029
    invoke-interface {v3}, LX/5LG;->k()Z

    move-result v5

    .line 2071030
    iput-boolean v5, v4, LX/4Z3;->f:Z

    .line 2071031
    invoke-interface {v3}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v5

    .line 2071032
    iput-object v5, v4, LX/4Z3;->g:Ljava/lang/String;

    .line 2071033
    invoke-interface {v3}, LX/5LG;->m()I

    move-result v5

    .line 2071034
    iput v5, v4, LX/4Z3;->h:I

    .line 2071035
    invoke-interface {v3}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v5

    .line 2071036
    iput-object v5, v4, LX/4Z3;->i:Ljava/lang/String;

    .line 2071037
    invoke-interface {v3}, LX/5LG;->y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071038
    iput-object v5, v4, LX/4Z3;->j:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071039
    invoke-interface {v3}, LX/5LG;->x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071040
    iput-object v5, v4, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071041
    invoke-interface {v3}, LX/5LG;->w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071042
    iput-object v5, v4, LX/4Z3;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071043
    invoke-interface {v3}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071044
    iput-object v5, v4, LX/4Z3;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071045
    invoke-interface {v3}, LX/5LG;->u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071046
    iput-object v5, v4, LX/4Z3;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071047
    invoke-interface {v3}, LX/5LG;->t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    .line 2071048
    iput-object v5, v4, LX/4Z3;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2071049
    invoke-interface {v3}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v5

    .line 2071050
    iput-object v5, v4, LX/4Z3;->u:Ljava/lang/String;

    .line 2071051
    invoke-interface {v3}, LX/5LG;->p()Z

    move-result v5

    .line 2071052
    iput-boolean v5, v4, LX/4Z3;->v:Z

    .line 2071053
    invoke-interface {v3}, LX/5LG;->q()Z

    move-result v5

    .line 2071054
    iput-boolean v5, v4, LX/4Z3;->w:Z

    .line 2071055
    invoke-interface {v3}, LX/5LG;->r()Z

    move-result v5

    .line 2071056
    iput-boolean v5, v4, LX/4Z3;->x:Z

    .line 2071057
    invoke-virtual {v4}, LX/4Z3;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v4

    goto/16 :goto_2

    .line 2071058
    :cond_3
    new-instance v6, LX/4Z4;

    invoke-direct {v6}, LX/4Z4;-><init>()V

    .line 2071059
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;->a()I

    move-result p1

    .line 2071060
    iput p1, v6, LX/4Z4;->b:I

    .line 2071061
    invoke-virtual {v6}, LX/4Z4;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v6

    goto/16 :goto_4

    .line 2071062
    :cond_4
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2071063
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 2071064
    iput-object p1, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2071065
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto/16 :goto_5

    .line 2071066
    :cond_5
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2071067
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 2071068
    iput-object p1, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2071069
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto/16 :goto_6

    .line 2071070
    :cond_6
    new-instance v4, LX/4Z5;

    invoke-direct {v4}, LX/4Z5;-><init>()V

    .line 2071071
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$TaggableActivityIconModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v5

    .line 2071072
    if-nez v5, :cond_7

    .line 2071073
    const/4 v6, 0x0

    .line 2071074
    :goto_7
    move-object v5, v6

    .line 2071075
    iput-object v5, v4, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2071076
    invoke-virtual {v4}, LX/4Z5;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v4

    goto/16 :goto_3

    .line 2071077
    :cond_7
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2071078
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$TaggableActivityIconModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2071079
    iput-object v3, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2071080
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_7
.end method

.method public static a(Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071081
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "hide_ads_after_party_aymt_tip"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2071082
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->HIDE_ADS_AFTER_PARTY_AYMT_TIP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;)LX/Cfl;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2071083
    sget-object v0, LX/E1h;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2071084
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2071085
    :goto_0
    const/4 v1, 0x0

    sget-object v2, LX/Cfc;->NEARBY_FRIENDS_NUX_TAP:LX/Cfc;

    invoke-static {v0, v1, v2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0

    .line 2071086
    :pswitch_0
    sget-object v0, LX/0ax;->ea:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "nearby_friends_undecided"

    invoke-static {v0, p0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2071087
    :pswitch_1
    sget-object v0, LX/0ax;->ea:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "informational"

    invoke-static {v0, p0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;)LX/Cfl;
    .locals 9

    .prologue
    .line 2070860
    new-instance v6, LX/Cfl;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->d()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/Cfc;->FUNDRAISER_COMPOSER:LX/Cfc;

    new-instance v0, LX/Cg8;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->hc_()LX/2rX;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;->a(LX/2rX;)Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v5

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/Cg8;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;)V

    invoke-direct {v6, v7, v8, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v6
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;)LX/Cfl;
    .locals 10

    .prologue
    .line 2070766
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2070767
    const-string v1, "replacement_unit"

    .line 2070768
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2070769
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->d()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2070770
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    .line 2070771
    new-instance v7, LX/9vz;

    invoke-direct {v7}, LX/9vz;-><init>()V

    new-instance v8, LX/9vl;

    invoke-direct {v8}, LX/9vl;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->j()LX/174;

    move-result-object v9

    invoke-interface {v9}, LX/174;->a()Ljava/lang/String;

    move-result-object v9

    .line 2070772
    iput-object v9, v8, LX/9vl;->c:Ljava/lang/String;

    .line 2070773
    move-object v8, v8

    .line 2070774
    invoke-virtual {v8}, LX/9vl;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v8

    .line 2070775
    iput-object v8, v7, LX/9vz;->bt:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    .line 2070776
    move-object v7, v7

    .line 2070777
    new-instance v8, LX/9vy;

    invoke-direct {v8}, LX/9vy;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k()LX/174;

    move-result-object v9

    invoke-interface {v9}, LX/174;->a()Ljava/lang/String;

    move-result-object v9

    .line 2070778
    iput-object v9, v8, LX/9vy;->b:Ljava/lang/String;

    .line 2070779
    move-object v8, v8

    .line 2070780
    invoke-virtual {v8}, LX/9vy;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v8

    .line 2070781
    iput-object v8, v7, LX/9vz;->cS:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    .line 2070782
    move-object v7, v7

    .line 2070783
    new-instance v8, LX/5sq;

    invoke-direct {v8}, LX/5sq;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->hg_()LX/5sY;

    move-result-object v9

    invoke-interface {v9}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v9

    .line 2070784
    iput-object v9, v8, LX/5sq;->a:Ljava/lang/String;

    .line 2070785
    move-object v8, v8

    .line 2070786
    invoke-virtual {v8}, LX/5sq;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v8

    .line 2070787
    iput-object v8, v7, LX/9vz;->aL:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2070788
    move-object v7, v7

    .line 2070789
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v8

    .line 2070790
    iput-object v8, v7, LX/9vz;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2070791
    move-object v7, v7

    .line 2070792
    new-instance v8, LX/9sr;

    invoke-direct {v8}, LX/9sr;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v9

    .line 2070793
    iput-object v9, v8, LX/9sr;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 2070794
    move-object v8, v8

    .line 2070795
    invoke-virtual {v8}, LX/9sr;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v8

    .line 2070796
    iput-object v8, v7, LX/9vz;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2070797
    move-object v7, v7

    .line 2070798
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->d()I

    move-result v2

    .line 2070799
    iput v2, v7, LX/9vz;->l:I

    .line 2070800
    move-object v2, v7

    .line 2070801
    invoke-virtual {v2}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v2

    .line 2070802
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2070803
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    .line 2070804
    :cond_0
    new-instance v2, LX/9qr;

    invoke-direct {v2}, LX/9qr;-><init>()V

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2070805
    iput-object v3, v2, LX/9qr;->i:LX/0Px;

    .line 2070806
    move-object v2, v2

    .line 2070807
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->e()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v3

    .line 2070808
    iput-object v3, v2, LX/9qr;->l:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 2070809
    move-object v2, v2

    .line 2070810
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->he_()Ljava/lang/String;

    move-result-object v3

    .line 2070811
    iput-object v3, v2, LX/9qr;->m:Ljava/lang/String;

    .line 2070812
    move-object v2, v2

    .line 2070813
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2070814
    iput-object v3, v2, LX/9qr;->d:Ljava/lang/String;

    .line 2070815
    move-object v2, v2

    .line 2070816
    invoke-virtual {v2}, LX/9qr;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    move-object v2, v2

    .line 2070817
    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2070818
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->REPLACE_UNIT_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070819
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2070820
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2070821
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070822
    new-instance v0, LX/CgE;

    invoke-direct {v0, p0, p3}, LX/CgE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070823
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070824
    new-instance v0, LX/CgJ;

    invoke-direct {v0, p0, p3, p4}, LX/CgJ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2070825
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/Cfl;LX/Cfc;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070826
    new-instance v0, LX/CgC;

    invoke-direct {v0, p0, p1, p2, p3}, LX/CgC;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/Cfl;)V

    .line 2070827
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070828
    new-instance v0, LX/CgH;

    invoke-direct {v0, p0, p1, p2, p3}, LX/CgH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2070829
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;
    .locals 6

    .prologue
    .line 2070830
    new-instance v0, LX/CgG;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/CgG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2070831
    new-instance v2, LX/Cfl;

    const/4 v3, 0x0

    if-eqz p4, :cond_0

    sget-object v1, LX/Cfc;->OPEN_PHOTO_COMPOSER_TAP:LX/Cfc;

    :goto_0
    invoke-direct {v2, v3, v1, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v2

    :cond_0
    sget-object v1, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Cfc;)LX/Cfl;
    .locals 5

    .prologue
    .line 2070722
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "reactionNonAdminPagePost"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2070723
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2070724
    move-object v1, v1

    .line 2070725
    iput-object p2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2070726
    move-object v1, v1

    .line 2070727
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070728
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2070729
    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070730
    new-instance v0, LX/Cfl;

    invoke-direct {v0, p0, p4, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v0

    .line 2070731
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/auth/viewercontext/ViewerContext;LX/Cfc;)LX/Cfl;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2070832
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "reactionAdminPagePost"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2070833
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2070834
    move-object v1, v1

    .line 2070835
    iput-object p2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2070836
    move-object v1, v1

    .line 2070837
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setIsPageVerified(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_PAGE_ADMIN_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070838
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2070839
    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070840
    new-instance v0, LX/Cfl;

    invoke-direct {v0, p0, p5, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/Cfl;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionActionFatFields$RelatedUsers;",
            ">;)",
            "LX/Cfl;"
        }
    .end annotation

    .prologue
    .line 2070841
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "reactionCheckin"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2070842
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    .line 2070843
    iput-object p0, v0, LX/5m9;->f:Ljava/lang/String;

    .line 2070844
    move-object v0, v0

    .line 2070845
    iput-object p1, v0, LX/5m9;->h:Ljava/lang/String;

    .line 2070846
    move-object v0, v0

    .line 2070847
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2070848
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const-string v3, "ANDROID_AFTER_PARTY_COMPOSER"

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2070849
    if-eqz p2, :cond_1

    .line 2070850
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2070851
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;

    .line 2070852
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2070853
    iput-object v0, v4, LX/5Rc;->b:Ljava/lang/String;

    .line 2070854
    move-object v0, v4

    .line 2070855
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    .line 2070856
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2070857
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2070858
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2070859
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->CHECKIN_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(LX/0QB;)LX/E1i;
    .locals 3

    .prologue
    .line 2070756
    sget-object v0, LX/E1i;->r:LX/E1i;

    if-nez v0, :cond_1

    .line 2070757
    const-class v1, LX/E1i;

    monitor-enter v1

    .line 2070758
    :try_start_0
    sget-object v0, LX/E1i;->r:LX/E1i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2070759
    if-eqz v2, :cond_0

    .line 2070760
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/E1i;->b(LX/0QB;)LX/E1i;

    move-result-object v0

    sput-object v0, LX/E1i;->r:LX/E1i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2070761
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2070762
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2070763
    :cond_1
    sget-object v0, LX/E1i;->r:LX/E1i;

    return-object v0

    .line 2070764
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2070765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070861
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070862
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2070863
    if-nez v1, :cond_0

    .line 2070864
    const/4 v0, 0x0

    .line 2070865
    :goto_0
    return-object v0

    .line 2070866
    :cond_0
    const-string v0, "extra_page_name"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070867
    new-instance v0, LX/Cfl;

    invoke-direct {v0, p3, p5, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070868
    const v0, 0x7f080146

    const-string v1, "pages_manager_overview_tab"

    invoke-static {p0, v0, p2, p1, v1}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070869
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->BOOST_POST_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070870
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2070871
    const-string v1, "copy_to_clipboard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070872
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->COPY_LINK_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070873
    new-instance v0, LX/CgQ;

    invoke-direct {v0, p0, p1}, LX/CgQ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070874
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->MESSAGE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 1

    .prologue
    .line 2070875
    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070876
    new-instance v0, LX/Cfl;

    sget-object v1, LX/Cfc;->LEAVE_FUNDRAISER:LX/Cfc;

    new-instance v2, LX/CgA;

    invoke-direct {v2, p0, p1, p2}, LX/CgA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 9

    .prologue
    .line 2070877
    sget-object v1, LX/21D;->LOCAL_SERP:LX/21D;

    const-string v2, "review_unit"

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v7, "review_button"

    const-string v8, "after_party"

    move-object v6, p2

    invoke-static/range {v1 .. v8}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_AFTER_PARTY_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionUnitId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070878
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2070879
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p1, p3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method private static b(LX/0QB;)LX/E1i;
    .locals 18

    .prologue
    .line 2070880
    new-instance v2, LX/E1i;

    const/16 v3, 0x18c5

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xc

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/Bla;->a(LX/0QB;)LX/Bla;

    move-result-object v5

    check-cast v5, LX/Bla;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v7

    check-cast v7, LX/1nG;

    const/16 v8, 0xbd2

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3097

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/EAc;->a(LX/0QB;)LX/EAc;

    move-result-object v10

    check-cast v10, LX/EAc;

    invoke-static/range {p0 .. p0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v11

    check-cast v11, LX/1nD;

    invoke-static/range {p0 .. p0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v12

    check-cast v12, LX/B9y;

    const/16 v13, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v14

    check-cast v14, LX/0hy;

    const/16 v15, 0x1032

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/BOb;->a(LX/0QB;)LX/BOb;

    move-result-object v16

    check-cast v16, LX/BOb;

    const/16 v17, 0x533

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, LX/E1i;-><init>(LX/0Ot;LX/0Or;LX/Bla;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/0Ot;LX/0Ot;LX/EAc;LX/1nD;LX/B9y;LX/0Ot;LX/0hy;LX/0Ot;LX/BOb;LX/0Ot;)V

    .line 2070881
    return-object v2
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071192
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->t()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2071193
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launch_activity_for_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reaction_request_code"

    const/16 v2, 0xc35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2071194
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->ADD_PAGE_PROFILE_PIC:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071197
    const v0, 0x7f080146

    const-string v1, "pages_manager_overview_tab"

    invoke-static {p0, v0, p2, p1, v1}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071198
    const v1, 0x7f080145

    const-string v2, "pages_manager_overview_tab"

    invoke-static {p0, v1, v2, p2, v0}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 2071199
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->VIEW_POST_INSIGHTS:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static c(Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071200
    new-instance v0, LX/Cfl;

    const/4 v1, 0x0

    new-instance v2, LX/CgL;

    invoke-direct {v2, p0}, LX/CgL;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, p1, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2071201
    const v0, -0x4e6785e3

    invoke-static {p0, v0}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2071202
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    sget-object v3, LX/21D;->REACTION:LX/21D;

    const-string v4, "fundraiserPersonToCharityShare"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v3, v4, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071203
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SHARE_FUNDRAISER:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method private static c(LX/E1i;)Z
    .locals 3

    .prologue
    .line 2071204
    iget-object v0, p0, LX/E1i;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    sget-object v1, LX/E1i;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kY;

    invoke-virtual {v0, v1, v2}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071205
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "shareProfileIntent"

    const v2, 0x25d6af

    invoke-static {p0, v2}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-static {v2}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2071206
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071207
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p0, p1, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071208
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "ogObjectShare"

    invoke-static {p1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2071209
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071210
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    .line 2071211
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "sharePageIntent"

    const v2, 0x25d6af

    invoke-static {p0, v2}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-static {v2}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2071212
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2071213
    move-object v1, v1

    .line 2071214
    iput-object p2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2071215
    move-object v1, v1

    .line 2071216
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2071217
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071218
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071219
    new-instance v0, LX/Cg3;

    invoke-direct {v0, p0, p2}, LX/Cg3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071220
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_EVENT_POSTS:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071248
    invoke-static {p1}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v0

    .line 2071249
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071250
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_APPOINTMENT_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    .line 2071221
    sget-object v0, LX/21D;->REACTION:LX/21D;

    const-string v1, "reactionWriteTimeline"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->USER:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2071222
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2071223
    move-object v1, v1

    .line 2071224
    iput-object p2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2071225
    move-object v1, v1

    .line 2071226
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_AFTER_PARTY_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2071227
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071228
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static final h(Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2071251
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2071252
    const/4 v1, 0x0

    sget-object v2, LX/Cfc;->NEARBY_FRIENDS_TAP:LX/Cfc;

    invoke-static {v0, v1, v2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    .line 2071242
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2071243
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2071244
    const-string v1, "android.intent.extra.TEXT"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "ti"

    const-string v4, "as"

    invoke-static {v2, v3, v4}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071245
    const v1, 0x7f082237

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 2071246
    const-string v1, "launch_external_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2071247
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->EXTERNAL_PROFILE_LINK_SHARE:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static final i(Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071235
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2071236
    sget-object v1, LX/Cfc;->FACEWEB_URL_TAP:LX/Cfc;

    .line 2071237
    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2071238
    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2071239
    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2071240
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    .line 2071241
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, LX/E1i;->a(Landroid/net/Uri;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public static j(Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071233
    new-instance v0, LX/CgT;

    invoke-direct {v0, p0}, LX/CgT;-><init>(Ljava/lang/String;)V

    .line 2071234
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->VIEW_PAGE_SETTINGS:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static l(Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071231
    new-instance v0, LX/CgO;

    invoke-direct {v0, p0}, LX/CgO;-><init>(Ljava/lang/String;)V

    .line 2071232
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->VIEW_PAGE_INSIGHTS:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static n(Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071195
    new-instance v0, LX/CgN;

    invoke-direct {v0, p0}, LX/CgN;-><init>(Ljava/lang/String;)V

    .line 2071196
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static o(Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071229
    new-instance v0, LX/CgP;

    invoke-direct {v0, p0}, LX/CgP;-><init>(Ljava/lang/String;)V

    .line 2071230
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->VIEW_PAGE_PROFILE:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;LX/2jR;)V

    return-object v1
.end method

.method public static p(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071090
    invoke-static {p1}, LX/DkQ;->e(Ljava/lang/String;)LX/DkQ;

    move-result-object v0

    .line 2071091
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071092
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static p(Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071093
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "launch_media_gallery"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2071094
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p0, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method


# virtual methods
.method public final A(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071095
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aX:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071096
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final B(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071097
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aZ:Ljava/lang/String;

    const-string v2, "ReactionAction"

    invoke-static {v1, p2, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071098
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final C(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071099
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->bo:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071100
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_FRIEND_INVITER:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(LX/1yA;)LX/Cfl;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2071101
    iget-object v0, p0, LX/E1i;->f:LX/1nG;

    invoke-virtual {v0, p1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v0

    .line 2071102
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2071103
    const/4 v0, 0x0

    .line 2071104
    :goto_0
    return-object v0

    .line 2071105
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2071106
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2071107
    new-instance v0, LX/Cfl;

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->TEXT_ENTITY_TAP:LX/Cfc;

    invoke-direct {v0, v2, v3, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(LX/2jY;)LX/Cfl;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2071108
    if-nez p1, :cond_1

    .line 2071109
    :cond_0
    :goto_0
    return-object v0

    .line 2071110
    :cond_1
    iget-object v1, p1, LX/2jY;->s:Ljava/lang/String;

    move-object v1, v1

    .line 2071111
    iget-object v2, p1, LX/2jY;->t:Ljava/lang/String;

    move-object v2, v2

    .line 2071112
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2071113
    iget-wide v6, p1, LX/2jY;->u:J

    move-wide v4, v6

    .line 2071114
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2071115
    :try_start_0
    const-string v4, "aymt_hot_post_afterparty"

    .line 2071116
    sget-object v5, LX/0ax;->e:Ljava/lang/String;

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string p0, "utf-8"

    invoke-static {v3, p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    const-string p0, "utf-8"

    invoke-static {v2, p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v6, v7

    const/4 v7, 0x2

    const-string p0, "utf-8"

    invoke-static {v1, p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v6, v7

    const/4 v7, 0x3

    const-string p0, "utf-8"

    invoke-static {v4, p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v6, v7

    const/4 v7, 0x4

    const-string p0, "UNKNOWN"

    aput-object p0, v6, v7

    const/4 v7, 0x5

    const-string p0, "UNKNOWN"

    aput-object p0, v6, v7

    const/4 v7, 0x6

    const-string p0, "UNKNOWN"

    aput-object p0, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 2071117
    invoke-static {v1}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2071118
    :catch_0
    goto :goto_0
.end method

.method public final a(LX/5sc;LX/Cfc;)LX/Cfl;
    .locals 4
    .param p1    # LX/5sc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2071119
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2071120
    :cond_0
    const/4 v0, 0x0

    .line 2071121
    :goto_0
    return-object v0

    .line 2071122
    :cond_1
    invoke-interface {p1}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_2

    .line 2071123
    invoke-interface {p1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/E1i;->a(Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0

    .line 2071124
    :cond_2
    invoke-interface {p1}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x403827a

    if-ne v0, v1, :cond_3

    .line 2071125
    invoke-interface {p1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/E1i;->c(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    goto :goto_0

    .line 2071126
    :cond_3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2071127
    invoke-interface {p1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {p1}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2071128
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-interface {p1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    .line 2071129
    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, p2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v3

    .line 2071130
    if-eqz v1, :cond_4

    .line 2071131
    iget-object p0, v3, LX/Cfl;->d:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2071132
    :cond_4
    move-object v0, v3

    .line 2071133
    move-object v0, v0

    .line 2071134
    goto :goto_0

    .line 2071135
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)LX/Cfl;
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2071088
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->fb:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "referrer"

    sget-object v2, LX/BZf;->REACTION:LX/BZf;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071089
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_MORE_APPS_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;DD)LX/Cfl;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2071149
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2071150
    const-string v0, "latitude"

    invoke-virtual {v2, v0, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2071151
    const-string v0, "longitude"

    invoke-virtual {v2, v0, p4, p5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2071152
    const-string v0, "zoom"

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2071153
    const-string v0, "curation_surface"

    const-string v3, "native_reaction"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071154
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v3, LX/0ax;->gH:Ljava/lang/String;

    invoke-interface {v0, p1, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2071155
    if-nez v3, :cond_0

    move-object v0, v1

    .line 2071156
    :goto_0
    return-object v0

    .line 2071157
    :cond_0
    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2071158
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_MAP_TAP:LX/Cfc;

    invoke-direct {v0, v1, v2, v3}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2071159
    invoke-static {p0}, LX/E1i;->c(LX/E1i;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2071160
    invoke-static/range {p1 .. p6}, LX/6Zi;->a(Landroid/content/Context;DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    move-object v2, p1

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p6

    invoke-static/range {v2 .. v9}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2071161
    :goto_0
    new-instance v3, LX/Cfl;

    sget-object v4, LX/Cfc;->OPEN_PAGE_MAP_TAP:LX/Cfc;

    move-object/from16 v0, p8

    invoke-direct {v3, v0, v4, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    move-object v2, v3

    :goto_1
    return-object v2

    .line 2071162
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2071163
    const-string v2, "place_name"

    move-object/from16 v0, p7

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071164
    const-string v2, "extra_page_name"

    move-object/from16 v0, p7

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071165
    const-string v2, "address"

    move-object/from16 v0, p6

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071166
    const-string v2, "latitude"

    invoke-virtual {v3, v2, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2071167
    const-string v2, "longitude"

    invoke-virtual {v3, v2, p4, p5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2071168
    const-string v2, "zoom"

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2071169
    const-string v2, "curation_surface"

    const-string v4, "native_page_profile"

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071170
    iget-object v2, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17Y;

    sget-object v4, LX/0ax;->gH:Ljava/lang/String;

    invoke-interface {v2, p1, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2071171
    if-nez v2, :cond_1

    .line 2071172
    const/4 v2, 0x0

    goto :goto_1

    .line 2071173
    :cond_1
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;JLjava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071174
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aH:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071175
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2071176
    const-string v1, "page_service_id_extra"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071177
    const-string v1, "extra_service_launched_from_page"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2071178
    new-instance v1, LX/Cfl;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->VIEW_PAGE_SERVICE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2071179
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->eD:Ljava/lang/String;

    invoke-static {v1, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "local_content_entry_point"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071180
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 8
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2071136
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v2, LX/0ax;->cc:Ljava/lang/String;

    invoke-interface {v0, p1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2071137
    if-nez v2, :cond_0

    move-object v0, v1

    .line 2071138
    :goto_0
    return-object v0

    .line 2071139
    :cond_0
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2071140
    new-instance v0, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    invoke-direct {v0, v3, p3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    .line 2071141
    new-instance v3, Lcom/facebook/nearby/common/SearchSuggestion;

    iget-object v4, v0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/facebook/nearby/common/SearchSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/common/NearbyTopic;)V

    .line 2071142
    const-string v0, "result_place_search_suggestion"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2071143
    :cond_1
    const-string v0, "nearby_places_entry"

    sget-object v3, LX/CQB;->REACTION:LX/CQB;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2071144
    const-string v0, "nearby_places_query_topic"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071145
    if-eqz p2, :cond_2

    .line 2071146
    const-string v0, "nearby_places_location_id"

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071147
    const-string v0, "nearby_places_location_name"

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071148
    :cond_2
    new-instance v0, LX/Cfl;

    sget-object v3, LX/Cfc;->NEARBY_PLACES_TAP:LX/Cfc;

    invoke-direct {v0, v1, v3, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 3

    .prologue
    .line 2071181
    sget-object v0, LX/8Dq;->F:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2071182
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071183
    const-string v1, "titlebar_with_modal_done"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2071184
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p2, p3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;
    .locals 3
    .param p3    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2071185
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2071186
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_reaction_analytics_params"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2071187
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->EVENT_CARD_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/7iP;)LX/Cfl;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/7iP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2071188
    sget-object v1, LX/0ax;->fS:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, v2, v0

    const/4 v0, 0x1

    const-string v3, "0"

    aput-object v3, v2, v0

    const/4 v3, 0x2

    if-nez p4, :cond_1

    const-string v0, "unknown"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x3

    if-nez p3, :cond_0

    const-string p3, "0"

    :cond_0
    aput-object p3, v2, v0

    const/4 v0, 0x4

    const-string v3, "0"

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2071189
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2071190
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    .line 2071191
    :cond_1
    iget-object v0, p4, LX/7iP;->value:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070932
    const-string v0, ""

    invoke-static {p2, v0, p3}, LX/8wJ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2070933
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070934
    const-string v1, "titlebar_with_modal_done"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070935
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p2, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070885
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->LIKES_AP:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 2070886
    iget-object v0, p0, LX/E1i;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E1d;

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3, p2}, LX/E1d;->a(Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;JLjava/lang/String;)LX/2jY;

    .line 2070887
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v2, LX/0ax;->aO:Ljava/lang/String;

    invoke-static {v2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070888
    const-string v2, "reaction_session_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070889
    const-string v2, "owner_id"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070890
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070891
    const-string v2, "source_name"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070892
    const-string v2, "page_context_item_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2070893
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_PAGE_LIKES_TAP:LX/Cfc;

    invoke-direct {v1, p3, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070488
    iget-object v0, p0, LX/E1i;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b2;

    invoke-virtual {v0, p1}, LX/1b2;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2070489
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    new-instance v2, LX/89I;

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    .line 2070490
    iput-object p4, v2, LX/89I;->c:Ljava/lang/String;

    .line 2070491
    move-object v2, v2

    .line 2070492
    iput-object p5, v2, LX/89I;->d:Ljava/lang/String;

    .line 2070493
    move-object v2, v2

    .line 2070494
    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/facebook/facecast/FacecastActivity;->a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Z)Landroid/os/Bundle;

    move-result-object v1

    .line 2070495
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2070496
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->PAGE_BROADCASTER_GO_LIVE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/9rO;)LX/Cfl;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2070499
    invoke-interface {p2}, LX/9rO;->gZ_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2070500
    const/4 v0, 0x0

    .line 2070501
    :goto_0
    return-object v0

    .line 2070502
    :cond_0
    invoke-interface {p2}, LX/9rO;->e()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewEventGuestListActionFieldsModel$EventModel$EventViewerCapabilityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/9rO;->e()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewEventGuestListActionFieldsModel$EventModel$EventViewerCapabilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewEventGuestListActionFieldsModel$EventModel$EventViewerCapabilityModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2070503
    :goto_1
    new-instance v2, LX/Blg;

    invoke-interface {p2}, LX/9rO;->gZ_()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Blg;-><init>(Ljava/lang/String;)V

    .line 2070504
    sget-object v3, Lcom/facebook/events/common/EventActionContext;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2070505
    iput-object v3, v2, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2070506
    move-object v3, v2

    .line 2070507
    invoke-interface {p2}, LX/9rO;->j()Ljava/lang/String;

    move-result-object v4

    .line 2070508
    iput-object v4, v3, LX/Blg;->c:Ljava/lang/String;

    .line 2070509
    move-object v3, v3

    .line 2070510
    invoke-interface {p2}, LX/9rO;->c()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    .line 2070511
    iput-object v4, v3, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2070512
    move-object v3, v3

    .line 2070513
    invoke-interface {p2}, LX/9rO;->b()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    .line 2070514
    iput-object v4, v3, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2070515
    move-object v3, v3

    .line 2070516
    invoke-interface {p2}, LX/9rO;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    .line 2070517
    iput-object v4, v3, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2070518
    move-object v3, v3

    .line 2070519
    iput-boolean v0, v3, LX/Blg;->g:Z

    .line 2070520
    move-object v3, v3

    .line 2070521
    invoke-interface {p2}, LX/9rO;->b()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    invoke-interface {p2}, LX/9rO;->c()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v5

    .line 2070522
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v4, v6, :cond_3

    .line 2070523
    new-instance v6, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v7, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    invoke-direct {v6, v7}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v7, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v8, LX/Blc;->PUBLIC_GOING:LX/Blc;

    invoke-direct {v7, v8}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v8, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v9, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    invoke-direct {v8, v9}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    invoke-static {v6, v7, v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 2070524
    :goto_2
    move-object v0, v6

    .line 2070525
    iput-object v0, v3, LX/Blg;->h:LX/0Px;

    .line 2070526
    move-object v0, v3

    .line 2070527
    iput-boolean v1, v0, LX/Blg;->i:Z

    .line 2070528
    invoke-virtual {v2}, LX/Blg;->a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v0, :cond_2

    sget-object v0, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    :goto_3
    invoke-static {v1, v0}, LX/Ble;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;)Landroid/os/Bundle;

    move-result-object v1

    .line 2070529
    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 2070530
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2070531
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2070532
    const-string v0, "target_fragment"

    sget-object v1, LX/0cQ;->EVENTS_GUEST_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070533
    new-instance v0, LX/Cfl;

    invoke-interface {p2}, LX/9rO;->gZ_()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/Cfc;->VIEW_EVENT_GUEST_LIST:LX/Cfc;

    invoke-direct {v0, v1, v3, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 2070534
    goto/16 :goto_1

    .line 2070535
    :cond_2
    sget-object v0, LX/Blc;->PUBLIC_GOING:LX/Blc;

    goto :goto_3

    :cond_3
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eq v5, v6, :cond_4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v5, v6, :cond_5

    :cond_4
    if-eqz v0, :cond_5

    new-instance v6, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v7, LX/Blc;->PRIVATE_GOING:LX/Blc;

    invoke-direct {v6, v7}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v7, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v8, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    invoke-direct {v7, v8}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v8, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v9, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    invoke-direct {v8, v9}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v9, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v10, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    invoke-direct {v9, v10}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    invoke-static {v6, v7, v8, v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    goto :goto_2

    :cond_5
    new-instance v6, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v7, LX/Blc;->PRIVATE_GOING:LX/Blc;

    invoke-direct {v6, v7}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v7, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v8, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    invoke-direct {v7, v8}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    new-instance v8, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    sget-object v9, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    invoke-direct {v8, v9}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    invoke-static {v6, v7, v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070536
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070537
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->REACTION_SHOW_MORE_ATTACHMENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070538
    const-string v1, "attachment_style"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2070539
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070540
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070541
    const-string v1, "show_more_title"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070542
    const-string v1, "reaction_unit_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070543
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p5, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;Ljava/lang/String;)LX/Cfl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2070544
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2070545
    invoke-interface {v6, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2070546
    iget-object v0, p0, LX/E1i;->k:LX/B9y;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->gZ_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->k()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->k()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->b()Ljava/lang/String;

    move-result-object v4

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->ha_()Ljava/lang/String;

    move-result-object v5

    const-string v7, "event"

    invoke-virtual/range {v0 .. v7}, LX/B9y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070547
    new-instance v1, LX/Cfl;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->gZ_()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    :cond_1
    move-object v3, v4

    .line 2070548
    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;Ljava/lang/String;)LX/Cfl;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070549
    new-instance v0, LX/89I;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2070550
    iput-object v1, v0, LX/89I;->c:Ljava/lang/String;

    .line 2070551
    move-object v0, v0

    .line 2070552
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->d()LX/2rX;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v0

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2070553
    sget-object v1, LX/21D;->REACTION:LX/21D;

    invoke-static {v1, p2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v0, p0, LX/E1i;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2070554
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "composer_configuration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2070555
    new-instance v1, LX/Cfl;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 1

    .prologue
    .line 2070556
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070557
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->K:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070558
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070559
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_GROUP_EVENTS_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070560
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070561
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FUNDRAISER_GUESTLIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070562
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070563
    const-string v1, "extra_view_fundraiser_supporters_connection_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2070564
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_FUNDRAISER_SUPPORTERS:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;)LX/Cfl;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2070565
    iget-object v0, p0, LX/E1i;->d:LX/Bla;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$ParentGroupModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$ParentGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$ParentGroupModel;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v6

    :goto_1
    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/Bla;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070566
    const-string v1, "launch_activity_for_result"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070567
    const-string v1, "reaction_request_code"

    const/16 v2, 0x1f5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070568
    new-instance v1, LX/Cfl;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/Cfc;->INVITE_FRIENDS_TO_EVENT:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    .line 2070569
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070498
    iget-object v0, p0, LX/E1i;->o:LX/BOb;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/BOb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/Cfc;->INVITE_FUNDRAISER_GUEST:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2070571
    new-instance v0, LX/Cfl;

    sget-object v8, LX/Cfc;->SEE_ALL_RATINGS_TAP:LX/Cfc;

    iget-object v1, p0, LX/E1i;->i:LX/EAc;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v5, p2

    move-object v6, v4

    move-object v7, p3

    invoke-virtual/range {v1 .. v7}, LX/EAc;->b(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, p1, v8, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2070572
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    .line 2070573
    invoke-static {v1}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v7

    .line 2070574
    :goto_0
    return-object v0

    .line 2070575
    :cond_0
    iget-object v0, p0, LX/E1i;->j:LX/1nD;

    const-string v4, "content"

    sget-object v5, LX/8ci;->y:LX/8ci;

    sget-object v6, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v2, p3

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    .line 2070576
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->BROWSE_QUERY_TAP:LX/Cfc;

    invoke-direct {v0, v7, v2, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Cfc;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2070577
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    .line 2070578
    iput-object p1, v0, LX/89k;->b:Ljava/lang/String;

    .line 2070579
    move-object v0, v0

    .line 2070580
    iput-object p2, v0, LX/89k;->c:Ljava/lang/String;

    .line 2070581
    move-object v0, v0

    .line 2070582
    invoke-static {p3}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v1

    .line 2070583
    iput-object v1, v0, LX/89k;->i:LX/21y;

    .line 2070584
    move-object v0, v0

    .line 2070585
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2070586
    new-instance v1, LX/Cfl;

    iget-object v2, p0, LX/E1i;->m:LX/0hy;

    invoke-interface {v2, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, p1, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070587
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070588
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_DISCOVERY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070589
    const-string v1, "extra_events_discovery_title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070590
    const-string v1, "extra_events_discovery_single_tab_title"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070591
    const-string v1, "extra_events_discovery_subtitle"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070592
    const-string v1, "extra_events_discovery_suggestion_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070593
    const-string v1, "extra_reaction_analytics_params"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070594
    const-string v1, "event_suggestion_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070595
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_ALL_SUGGESTED_EVENTS_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070596
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070597
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FUNDRAISER_MESSAGE_GUESTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070598
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070599
    const-string v1, "fundraiser_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070600
    const-string v1, "owner_profile_picture_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070601
    const-string v1, "extra_fundraiser_subtitle"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070602
    const-string v1, "source"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070603
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->CONTACT_FUNDRAISER_SUPPORTERS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070604
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070605
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->REACTION_SHOW_MORE_COMPONENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070606
    const-string v1, "component_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "reaction_session_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "reaction_surface"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_more_title"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "unit_type_token"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "reaction_unit_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070607
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_ALL_COMPONENTS:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070608
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    .line 2070609
    iput-object p4, v0, LX/89k;->b:Ljava/lang/String;

    .line 2070610
    move-object v0, v0

    .line 2070611
    iput-object p3, v0, LX/89k;->c:Ljava/lang/String;

    .line 2070612
    move-object v0, v0

    .line 2070613
    invoke-static {p5}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v1

    .line 2070614
    iput-object v1, v0, LX/89k;->i:LX/21y;

    .line 2070615
    move-object v0, v0

    .line 2070616
    iput-object p1, v0, LX/89k;->f:Ljava/lang/String;

    .line 2070617
    move-object v0, v0

    .line 2070618
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2070619
    iput-object v1, v0, LX/89k;->n:Ljava/lang/Boolean;

    .line 2070620
    move-object v0, v0

    .line 2070621
    if-eqz p2, :cond_0

    .line 2070622
    iput-object p2, v0, LX/89k;->d:Ljava/lang/String;

    .line 2070623
    :cond_0
    iget-object v1, p0, LX/E1i;->m:LX/0hy;

    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 2070624
    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2070625
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_COMMENT_TAP:LX/Cfc;

    invoke-direct {v0, p4, v2, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070626
    iget-object v0, p0, LX/E1i;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GAMETIME_PLAYS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_more_title"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feed_type_name"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->A:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 2070627
    iget-object p0, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, p0

    .line 2070628
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scoring_plays"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2070629
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z
    .locals 1

    .prologue
    .line 2070570
    iget-object v0, p0, LX/E1i;->q:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/Cfl;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2070432
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2070433
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2070434
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2070435
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2070436
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2070437
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 2070438
    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 2070439
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070440
    const-string v2, "target_fragment"

    sget-object v3, LX/0cQ;->EVENTS_UPCOMING_BIRTHDAYS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "birthday_view_referrer_param"

    const-string v4, "today_notification"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "birthday_view_start_date"

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070441
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->OPEN_UPCOMING_BIRTHDAYS_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Landroid/content/Context;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070442
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->cl:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070443
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->VIEW_EVENTS_DASHBOARD_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Landroid/content/Context;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 12

    .prologue
    .line 2070444
    invoke-static/range {p2 .. p6}, LX/6Zi;->a(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "launch_external_activity"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 2070445
    new-instance v10, LX/Cfl;

    sget-object v11, LX/Cfc;->OPEN_PAGE_NAVIGATION_TAP:LX/Cfc;

    invoke-static {p0}, LX/E1i;->c(LX/E1i;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p6

    invoke-static/range {v2 .. v9}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    :cond_0
    move-object/from16 v0, p8

    invoke-direct {v10, v0, v11, v3}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v10
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2070446
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->ba:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070447
    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070448
    const-string v1, "page_call_to_action_isadmin_extra"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "launch_activity_for_result"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "reaction_request_code"

    const/16 v3, 0x2781

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070449
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->ADD_PAGE_CTA:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2070450
    sget-object v0, LX/0ax;->h:Ljava/lang/String;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "UNKNOWN"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "UNKNOWN"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "UNKNOWN"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070451
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070452
    const-string v1, "titlebar_with_modal_done"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070453
    new-instance v1, LX/Cfl;

    invoke-direct {v1, p2, p4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070454
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aF:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070455
    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070456
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2070457
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070458
    :cond_0
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2070459
    const-string v1, "page_clicked_item_id_extra"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070460
    :cond_1
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 1

    .prologue
    .line 2070461
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070462
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->L:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070463
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070464
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_GROUP_PHOTOS_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070465
    sget-object v0, LX/0ax;->hF:Ljava/lang/String;

    const-string v1, "0"

    const-string v2, "0"

    invoke-static {v0, p1, p2, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070466
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p3, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070467
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2070468
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_OFFER_DETAIL:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final c(Landroid/content/Context;)LX/Cfl;
    .locals 5

    .prologue
    .line 2070469
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->cW:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/5Oz;->REACTION_FRIEND_REQUESTS_CARD:LX/5Oz;

    invoke-virtual {v4}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v4}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070470
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_ALL_FRIEND_REQUESTS:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;
    .locals 6

    .prologue
    .line 2070471
    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/E1i;->b(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070472
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    sget-object v1, LX/Cfc;->EVENT_CARD_TAP:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070429
    sget-object v0, LX/0ax;->iQ:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070430
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070431
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_JOB_DETAIL:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2070473
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2070474
    iget-object v0, p0, LX/E1i;->j:LX/1nD;

    sget-object v4, LX/8ci;->B:LX/8ci;

    invoke-static {p3}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v5, "ANDROID_SEARCH_LOCAL_LOCALSERP"

    :goto_0
    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/1nD;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v0

    .line 2070475
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_LOCAL_SEARCH_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    .line 2070476
    :cond_0
    const-string v5, "ANDROID_SEARCH_LOCAL_OTHER"

    goto :goto_0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070477
    sget-object v0, LX/0ax;->aM:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070478
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2070479
    if-nez v1, :cond_0

    .line 2070480
    const/4 v0, 0x0

    .line 2070481
    :goto_0
    return-object v0

    .line 2070482
    :cond_0
    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2070483
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_PAGE_INFO_TAP:LX/Cfc;

    invoke-direct {v0, p2, v2, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2070484
    iget-object v0, p0, LX/E1i;->k:LX/B9y;

    const-string v5, "event_invite_sheet_reaction"

    move-object v1, p1

    move-object v2, p3

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/B9y;->b(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2070485
    if-nez v1, :cond_0

    .line 2070486
    const/4 v0, 0x0

    .line 2070487
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    invoke-direct {v0, p2, v2, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070497
    sget-object v0, LX/0ax;->cE:Ljava/lang/String;

    sget-object v1, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070703
    invoke-static {p2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    .line 2070704
    if-eqz v0, :cond_0

    .line 2070705
    sget-object v1, LX/Cfc;->APPLINK_TAP:LX/Cfc;

    .line 2070706
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v0

    .line 2070707
    iput-object p2, v0, LX/47H;->a:Ljava/lang/String;

    .line 2070708
    move-object v0, v0

    .line 2070709
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v2

    .line 2070710
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v2}, LX/17Y;->a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;

    move-result-object v2

    .line 2070711
    const-string v0, "launch_external_activity"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070712
    new-instance v0, LX/Cfl;

    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v1, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    .line 2070713
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070681
    sget-object v0, LX/0ax;->fU:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070682
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070683
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 2070684
    const-string v1, "merchant_page_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070685
    :cond_0
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final e(Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070686
    sget-object v0, LX/0ax;->cF:Ljava/lang/String;

    sget-object v1, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070687
    iget-object v0, p0, LX/E1i;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b()Landroid/content/Intent;

    move-result-object v0

    .line 2070688
    const-string v1, "reaction_feed_story_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070689
    const-string v1, "reaction_feed_title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070690
    const-string v1, "feed_type_name"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->i:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 2070691
    iget-object p0, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, p0

    .line 2070692
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070693
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 6

    .prologue
    .line 2070694
    sget-object v2, LX/0ax;->aQ:Ljava/lang/String;

    sget-object v5, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/E1i;->b(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)LX/Cfl;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2070695
    sget-object v0, LX/0ax;->C:Ljava/lang/String;

    sget-object v1, LX/Cfc;->VIEW_GROUP_TAP:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    .line 2070696
    new-instance v0, LX/Cfl;

    sget-object v1, LX/Cfc;->TOPIC_TAP:LX/Cfc;

    iget-object v2, p0, LX/E1i;->j:LX/1nD;

    sget-object v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    sget-object v4, LX/8ci;->B:LX/8ci;

    invoke-virtual {v2, v3, p1, p2, v4}, LX/1nD;->a(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v0
.end method

.method public final g(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070697
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2070698
    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {v0}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2070699
    :cond_1
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2070700
    new-instance v0, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->OPEN_PAGE_PROMOTION:LX/Cfc;

    invoke-direct {v0, v2, v3, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    .line 2070701
    :goto_0
    return-object v0

    :cond_2
    invoke-static {p2}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 6

    .prologue
    .line 2070702
    sget-object v2, LX/0ax;->aR:Ljava/lang/String;

    sget-object v5, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/E1i;->b(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)LX/Cfl;
    .locals 2

    .prologue
    .line 2070680
    sget-object v0, LX/8Dq;->c:Ljava/lang/String;

    sget-object v1, LX/Cfc;->EDIT_PAGE_INFO_TAP:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070714
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2070715
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FUNDRAISER_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070716
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070717
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070718
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_FUNDRAISER:LX/Cfc;

    invoke-direct {v1, p1, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final h(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070719
    sget-object v0, LX/0ax;->iz:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070720
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070721
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_VIDEO_CHANNEL:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 6

    .prologue
    .line 2070732
    sget-object v2, LX/0ax;->aP:Ljava/lang/String;

    sget-object v5, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/E1i;->b(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2070733
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    .line 2070734
    iput-object p1, v0, LX/89k;->b:Ljava/lang/String;

    .line 2070735
    move-object v0, v0

    .line 2070736
    iput-object p2, v0, LX/89k;->c:Ljava/lang/String;

    .line 2070737
    move-object v0, v0

    .line 2070738
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 2070739
    new-instance v2, LX/Cfl;

    sget-object v3, LX/Cfc;->STORY_TAP:LX/Cfc;

    iget-object v4, p0, LX/E1i;->m:LX/0hy;

    iget-object v0, p0, LX/E1i;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-interface {v4, v0, v1}, LX/0hy;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v2, p1, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v2
.end method

.method public final i(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2070740
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->ba:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070741
    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070742
    const-string v1, "page_call_to_action_isadmin_extra"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070743
    const-string v1, "launch_activity_for_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070744
    const-string v1, "reaction_request_code"

    sget v2, LX/CYO;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070745
    invoke-static {}, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a()Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    move-result-object v1

    .line 2070746
    const-string v2, "extra_optional_admin_flow_control_params"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070747
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->PAGE_OPEN_CTA_SETUP_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final j(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2070748
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->iX:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070749
    const-string v1, "extra_should_use_client_default_get_quote_description"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070750
    const-string v1, "launch_activity_for_result"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070751
    const-string v1, "reaction_request_code"

    sget v2, LX/CYO;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070752
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->PAGE_OPEN_GET_QUOTE_SETUP_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070753
    sget-object v0, LX/0ax;->aT:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070754
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_page_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_ref_module"

    const-string v2, "reaction_dialog"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_ref_mechanism"

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PLACE_TIPS:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070755
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_EVENTS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final k(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2070652
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->ba:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070653
    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2070654
    const-string v1, "page_call_to_action_isadmin_extra"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070655
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const/4 v3, 0x0

    .line 2070656
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2070657
    new-instance v2, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    invoke-direct {v2, v3, v1}, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;-><init>(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)V

    move-object v1, v2

    .line 2070658
    const-string v2, "extra_optional_admin_flow_control_params"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2070659
    const-string v1, "launch_activity_for_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2070660
    const-string v1, "reaction_request_code"

    sget v2, LX/CYO;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2070661
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->PAGE_OPEN_REQUEST_APPOINTMENT_SETUP_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1

    :cond_0
    move v2, v3

    .line 2070662
    goto :goto_0
.end method

.method public final k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070632
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->eF:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070633
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070634
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_STRUCTURE_MENU_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final k(Ljava/lang/String;)LX/Cfl;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2070635
    iget-object v0, p0, LX/E1i;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7j6;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/7j6;->b(J)Landroid/content/Intent;

    move-result-object v1

    .line 2070636
    if-nez v1, :cond_0

    .line 2070637
    const/4 v0, 0x0

    .line 2070638
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_PAGE_ADD_PRODUCT_TAP:LX/Cfc;

    invoke-direct {v0, p1, v2, v1}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final l(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070639
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/8Dq;->H:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070640
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->NUX_CONTINUE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070641
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aS:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_page_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 2070642
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->OPEN_PAGE_CHILD_LOCATIONS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final m(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070643
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->fN:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070644
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->NUX_CONTINUE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final m(Ljava/lang/String;)LX/Cfl;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2070645
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    sget-object v1, LX/Cfc;->MESSAGE_TAP:LX/Cfc;

    invoke-static {v0, p1, v1}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final n(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070646
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->eE:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070647
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_PHOTO_MENU_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final o(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 4

    .prologue
    .line 2070648
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->bc:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070649
    new-instance v1, LX/Cfl;

    const/4 v2, 0x0

    sget-object v3, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, v2, v3, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final q(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070650
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    const-string v1, "fb://pma/newlikes"

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070651
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_PAGE_LIKERS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final r(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070630
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    const-string v1, "faceweb/f?href=/%s/activity_feed/?type=mention"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070631
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_PAGE_MENTIONS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final s(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070663
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    const-string v1, "faceweb/f?href=/%s/activity_feed/?type=checkin"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070664
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_PAGE_CHECKINS:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final t(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070665
    sget-object v0, LX/0ax;->aW:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070666
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070667
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final u(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070668
    sget-object v0, LX/0ax;->aU:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2070669
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_title"

    const v2, 0x7f081801

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070670
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final v(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070671
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aV:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070672
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final w(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070673
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    const-string v1, "faceweb/f?href=/%s/activity_feed/?type=share"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070674
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->VIEW_PAGE_SHARES:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final x(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 3

    .prologue
    .line 2070675
    iget-object v0, p0, LX/E1i;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->aC:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2070676
    const-string v1, "owner_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070677
    new-instance v1, LX/Cfl;

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-direct {v1, p2, v2, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    return-object v1
.end method

.method public final y(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 7

    .prologue
    .line 2070678
    sget-object v3, LX/0ax;->bt:Ljava/lang/String;

    sget-object v0, LX/5hJ;->PHOTOS_TAKEN_HERE:LX/5hJ;

    invoke-virtual {v0}, LX/5hJ;->ordinal()I

    move-result v4

    const/4 v5, 0x1

    sget-object v6, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, LX/E1i;->a(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZLX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final z(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;
    .locals 7

    .prologue
    .line 2070679
    sget-object v3, LX/0ax;->bu:Ljava/lang/String;

    sget-object v0, LX/5hJ;->PHOTOS_TAKEN_OF:LX/5hJ;

    invoke-virtual {v0}, LX/5hJ;->ordinal()I

    move-result v4

    const/4 v5, 0x1

    sget-object v6, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, LX/E1i;->a(LX/E1i;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZLX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method
