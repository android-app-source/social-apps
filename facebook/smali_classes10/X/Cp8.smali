.class public final LX/Cp8;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1936599
    iput-object p1, p0, LX/Cp8;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iput-object p2, p0, LX/Cp8;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1936600
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1936601
    iget-object v1, p0, LX/Cp8;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1936602
    iget-object v1, p0, LX/Cp8;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1936603
    invoke-static {v1}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1936604
    :goto_0
    return-void

    .line 1936605
    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1936606
    :cond_1
    const-string v1, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1936607
    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1936608
    :try_start_0
    iget-object v1, p0, LX/Cp8;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Cp8;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v2}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1936609
    :catch_0
    move-exception v0

    .line 1936610
    iget-object v1, p0, LX/Cp8;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_onClick"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error trying to launch url:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/Cp8;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1936611
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936612
    move-object v0, v2

    .line 1936613
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1936614
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1936615
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1936616
    return-void
.end method
