.class public final LX/EbE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2143165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/EYQ;)LX/EYa;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2143166
    sput-object p1, LX/EbV;->m:LX/EYQ;

    .line 2143167
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143168
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143169
    sput-object v0, LX/EbV;->a:LX/EYF;

    .line 2143170
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->a:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "RatchetKey"

    aput-object v3, v2, v5

    const-string v3, "Counter"

    aput-object v3, v2, v6

    const-string v3, "PreviousCounter"

    aput-object v3, v2, v7

    const-string v3, "Ciphertext"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143171
    sput-object v0, LX/EbV;->b:LX/EYn;

    .line 2143172
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143173
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143174
    sput-object v0, LX/EbV;->c:LX/EYF;

    .line 2143175
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->c:LX/EYF;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "RegistrationId"

    aput-object v3, v2, v5

    const-string v3, "PreKeyId"

    aput-object v3, v2, v6

    const-string v3, "SignedPreKeyId"

    aput-object v3, v2, v7

    const-string v3, "BaseKey"

    aput-object v3, v2, v8

    const-string v3, "IdentityKey"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Message"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143176
    sput-object v0, LX/EbV;->d:LX/EYn;

    .line 2143177
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143178
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143179
    sput-object v0, LX/EbV;->e:LX/EYF;

    .line 2143180
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->e:LX/EYF;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Id"

    aput-object v3, v2, v5

    const-string v3, "BaseKey"

    aput-object v3, v2, v6

    const-string v3, "RatchetKey"

    aput-object v3, v2, v7

    const-string v3, "IdentityKey"

    aput-object v3, v2, v8

    const-string v3, "BaseKeySignature"

    aput-object v3, v2, v9

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143181
    sput-object v0, LX/EbV;->f:LX/EYn;

    .line 2143182
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143183
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143184
    sput-object v0, LX/EbV;->g:LX/EYF;

    .line 2143185
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->g:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Id"

    aput-object v3, v2, v5

    const-string v3, "Iteration"

    aput-object v3, v2, v6

    const-string v3, "Ciphertext"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143186
    sput-object v0, LX/EbV;->h:LX/EYn;

    .line 2143187
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143188
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143189
    sput-object v0, LX/EbV;->i:LX/EYF;

    .line 2143190
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->i:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "Id"

    aput-object v3, v2, v5

    const-string v3, "Iteration"

    aput-object v3, v2, v6

    const-string v3, "ChainKey"

    aput-object v3, v2, v7

    const-string v3, "SigningKey"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143191
    sput-object v0, LX/EbV;->j:LX/EYn;

    .line 2143192
    sget-object v0, LX/EbV;->m:LX/EYQ;

    move-object v0, v0

    .line 2143193
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2143194
    sput-object v0, LX/EbV;->k:LX/EYF;

    .line 2143195
    new-instance v0, LX/EYn;

    sget-object v1, LX/EbV;->k:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Generation"

    aput-object v3, v2, v5

    const-string v3, "Signature"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2143196
    sput-object v0, LX/EbV;->l:LX/EYn;

    .line 2143197
    const/4 v0, 0x0

    return-object v0
.end method
