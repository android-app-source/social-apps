.class public final LX/DXL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DXP;


# direct methods
.method public constructor <init>(LX/DXP;)V
    .locals 0

    .prologue
    .line 2010107
    iput-object p1, p0, LX/DXL;->a:LX/DXP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2010108
    iget-object v0, p0, LX/DXL;->a:LX/DXP;

    iget-object v0, v0, LX/DXP;->f:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2010109
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2010110
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2010111
    if-eqz p1, :cond_0

    .line 2010112
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2010113
    if-eqz v0, :cond_0

    .line 2010114
    iget-object v0, p0, LX/DXL;->a:LX/DXP;

    iget-object v1, v0, LX/DXP;->f:LX/0TF;

    .line 2010115
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2010116
    check-cast v0, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkModel;->j()Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2010117
    :goto_0
    return-void

    .line 2010118
    :cond_0
    iget-object v0, p0, LX/DXL;->a:LX/DXP;

    iget-object v0, v0, LX/DXP;->f:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
