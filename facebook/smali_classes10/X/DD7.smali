.class public final LX/DD7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DD9;


# direct methods
.method public constructor <init>(LX/DD9;)V
    .locals 0

    .prologue
    .line 1974985
    iput-object p1, p0, LX/DD7;->a:LX/DD9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1974895
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1974896
    if-nez p1, :cond_0

    .line 1974897
    const/4 v0, 0x0

    .line 1974898
    :goto_0
    return-object v0

    .line 1974899
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1974900
    check-cast v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    move-result-object v0

    .line 1974901
    if-nez v0, :cond_1

    .line 1974902
    const/4 v1, 0x0

    .line 1974903
    :goto_1
    move-object v0, v1

    .line 1974904
    goto :goto_0

    .line 1974905
    :cond_1
    new-instance v3, LX/4Xh;

    invoke-direct {v3}, LX/4Xh;-><init>()V

    .line 1974906
    invoke-virtual {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1974907
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1974908
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1974909
    invoke-virtual {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;

    .line 1974910
    if-nez v1, :cond_4

    .line 1974911
    const/4 v5, 0x0

    .line 1974912
    :goto_3
    move-object v1, v5

    .line 1974913
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1974914
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1974915
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1974916
    iput-object v1, v3, LX/4Xh;->b:LX/0Px;

    .line 1974917
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;->b()LX/0us;

    move-result-object v1

    .line 1974918
    if-nez v1, :cond_9

    .line 1974919
    const/4 v2, 0x0

    .line 1974920
    :goto_4
    move-object v1, v2

    .line 1974921
    iput-object v1, v3, LX/4Xh;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1974922
    invoke-virtual {v3}, LX/4Xh;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v1

    goto :goto_1

    .line 1974923
    :cond_4
    new-instance v5, LX/4Xi;

    invoke-direct {v5}, LX/4Xi;-><init>()V

    .line 1974924
    invoke-virtual {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->a()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v6

    .line 1974925
    if-nez v6, :cond_5

    .line 1974926
    const/4 v7, 0x0

    .line 1974927
    :goto_5
    move-object v6, v7

    .line 1974928
    iput-object v6, v5, LX/4Xi;->b:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1974929
    invoke-virtual {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 1974930
    iput-object v6, v5, LX/4Xi;->c:Ljava/lang/String;

    .line 1974931
    invoke-virtual {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->c()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v6

    .line 1974932
    if-nez v6, :cond_7

    .line 1974933
    const/4 v7, 0x0

    .line 1974934
    :goto_6
    move-object v6, v7

    .line 1974935
    iput-object v6, v5, LX/4Xi;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1974936
    invoke-virtual {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->d()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v6

    .line 1974937
    if-nez v6, :cond_8

    .line 1974938
    const/4 v7, 0x0

    .line 1974939
    :goto_7
    move-object v6, v7

    .line 1974940
    iput-object v6, v5, LX/4Xi;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1974941
    invoke-virtual {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e()Ljava/lang/String;

    move-result-object v6

    .line 1974942
    iput-object v6, v5, LX/4Xi;->f:Ljava/lang/String;

    .line 1974943
    new-instance v6, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;-><init>(LX/4Xi;)V

    .line 1974944
    move-object v5, v6

    .line 1974945
    goto :goto_3

    .line 1974946
    :cond_5
    new-instance v7, LX/33O;

    invoke-direct {v7}, LX/33O;-><init>()V

    .line 1974947
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object p0

    .line 1974948
    iput-object p0, v7, LX/33O;->U:Ljava/lang/String;

    .line 1974949
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object p0

    .line 1974950
    iput-object p0, v7, LX/33O;->aI:Ljava/lang/String;

    .line 1974951
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->d()LX/1Fb;

    move-result-object p0

    .line 1974952
    if-nez p0, :cond_6

    .line 1974953
    const/4 p1, 0x0

    .line 1974954
    :goto_8
    move-object p0, p1

    .line 1974955
    iput-object p0, v7, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1974956
    invoke-virtual {v7}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    goto :goto_5

    .line 1974957
    :cond_6
    new-instance p1, LX/2dc;

    invoke-direct {p1}, LX/2dc;-><init>()V

    .line 1974958
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v6

    .line 1974959
    iput v6, p1, LX/2dc;->c:I

    .line 1974960
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    .line 1974961
    iput-object v6, p1, LX/2dc;->h:Ljava/lang/String;

    .line 1974962
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v6

    .line 1974963
    iput v6, p1, LX/2dc;->i:I

    .line 1974964
    invoke-virtual {p1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    goto :goto_8

    .line 1974965
    :cond_7
    new-instance v7, LX/173;

    invoke-direct {v7}, LX/173;-><init>()V

    .line 1974966
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 1974967
    iput-object p0, v7, LX/173;->f:Ljava/lang/String;

    .line 1974968
    invoke-virtual {v7}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    goto :goto_6

    .line 1974969
    :cond_8
    new-instance v7, LX/4Wm;

    invoke-direct {v7}, LX/4Wm;-><init>()V

    .line 1974970
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;->b()Ljava/lang/String;

    move-result-object p0

    .line 1974971
    iput-object p0, v7, LX/4Wm;->R:Ljava/lang/String;

    .line 1974972
    invoke-virtual {v6}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;->c()Ljava/lang/String;

    move-result-object p0

    .line 1974973
    iput-object p0, v7, LX/4Wm;->ac:Ljava/lang/String;

    .line 1974974
    invoke-virtual {v7}, LX/4Wm;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    goto :goto_7

    .line 1974975
    :cond_9
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 1974976
    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v4

    .line 1974977
    iput-object v4, v2, LX/17L;->c:Ljava/lang/String;

    .line 1974978
    invoke-interface {v1}, LX/0us;->b()Z

    move-result v4

    .line 1974979
    iput-boolean v4, v2, LX/17L;->d:Z

    .line 1974980
    invoke-interface {v1}, LX/0us;->c()Z

    move-result v4

    .line 1974981
    iput-boolean v4, v2, LX/17L;->e:Z

    .line 1974982
    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v4

    .line 1974983
    iput-object v4, v2, LX/17L;->f:Ljava/lang/String;

    .line 1974984
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_4
.end method
