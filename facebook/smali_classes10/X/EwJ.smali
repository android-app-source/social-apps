.class public final LX/EwJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/Euc;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183128
    iput-object p1, p0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2183129
    const/16 v2, 0x14

    .line 2183130
    iget-object v0, p0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2183131
    iget-object v0, p0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/2h7;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

    iget-object v3, v3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    sget-object v4, LX/0zS;->a:LX/0zS;

    new-instance v5, LX/EwI;

    invoke-direct {v5, p0}, LX/EwI;-><init>(LX/EwJ;)V

    invoke-virtual/range {v0 .. v5}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2183132
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/2h7;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

    iget-object v3, v3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
