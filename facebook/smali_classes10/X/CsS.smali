.class public abstract LX/CsS;
.super Landroid/support/design/widget/CoordinatorLayout;
.source ""

# interfaces
.implements LX/0hc;
.implements LX/CqD;


# instance fields
.field public g:Landroid/support/v4/view/ViewPager;

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0hc;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CqC;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/database/DataSetObserver;

.field private k:LX/CsR;

.field public l:LX/CsN;

.field private m:LX/CsQ;

.field private n:Z

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942781
    invoke-direct {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    .line 1942782
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    .line 1942783
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->i:Ljava/util/Set;

    .line 1942784
    new-instance v0, LX/CsP;

    invoke-direct {v0, p0}, LX/CsP;-><init>(LX/CsS;)V

    iput-object v0, p0, LX/CsS;->j:Landroid/database/DataSetObserver;

    .line 1942785
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsS;->n:Z

    .line 1942786
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1942787
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942788
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    .line 1942789
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->i:Ljava/util/Set;

    .line 1942790
    new-instance v0, LX/CsP;

    invoke-direct {v0, p0}, LX/CsP;-><init>(LX/CsS;)V

    iput-object v0, p0, LX/CsS;->j:Landroid/database/DataSetObserver;

    .line 1942791
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsS;->n:Z

    .line 1942792
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1942793
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942794
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    .line 1942795
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CsS;->i:Ljava/util/Set;

    .line 1942796
    new-instance v0, LX/CsP;

    invoke-direct {v0, p0}, LX/CsP;-><init>(LX/CsS;)V

    iput-object v0, p0, LX/CsS;->j:Landroid/database/DataSetObserver;

    .line 1942797
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsS;->n:Z

    .line 1942798
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 1942799
    iget-boolean v0, p0, LX/CsS;->n:Z

    invoke-virtual {p0, p1, v0}, LX/CsS;->a(IZ)V

    .line 1942800
    iget v0, p0, LX/CsS;->o:I

    invoke-virtual {p0, v0}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 1942801
    if-eqz v0, :cond_0

    .line 1942802
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->o()V

    .line 1942803
    :cond_0
    invoke-virtual {p0, p1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 1942804
    if-eqz v0, :cond_1

    .line 1942805
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->n()V

    .line 1942806
    :cond_1
    iput p1, p0, LX/CsS;->o:I

    .line 1942807
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 1942808
    iget-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1942809
    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    goto :goto_0

    .line 1942810
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)I
    .locals 1

    .prologue
    .line 1942811
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0, p1}, LX/CqD;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)I

    move-result v0

    return v0
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 1942812
    iget-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1942813
    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    goto :goto_0

    .line 1942814
    :cond_0
    return-void
.end method

.method public a(IZ)V
    .locals 0

    .prologue
    .line 1942815
    return-void
.end method

.method public final a(LX/0hc;)V
    .locals 1

    .prologue
    .line 1942816
    if-eqz p1, :cond_0

    .line 1942817
    iget-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1942818
    :cond_0
    return-void
.end method

.method public final a(LX/CqC;)V
    .locals 1

    .prologue
    .line 1942819
    if-eqz p1, :cond_0

    .line 1942820
    iget-object v0, p0, LX/CsS;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1942821
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V
    .locals 3

    .prologue
    .line 1942822
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1942823
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to add a fragment when the viewpager or pager adapter is null: mViewPager="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " adapter="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/CsS;->m:LX/CsQ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1942824
    :cond_0
    invoke-virtual {p1, p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->setFragmentPager(LX/CqD;)V

    .line 1942825
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0, p1, p2}, LX/CqD;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V

    .line 1942826
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1942767
    iget-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1942768
    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    goto :goto_0

    .line 1942769
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1942770
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 1942771
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CsS;->n:Z

    .line 1942772
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1942773
    iget v1, p0, LX/CsS;->o:I

    if-eq v0, v1, :cond_1

    .line 1942774
    invoke-direct {p0, v0}, LX/CsS;->d(I)V

    goto :goto_1

    .line 1942775
    :pswitch_2
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1942776
    iget v1, p0, LX/CsS;->o:I

    if-eq v1, v0, :cond_2

    .line 1942777
    invoke-direct {p0, v0}, LX/CsS;->d(I)V

    .line 1942778
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsS;->n:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/0hc;)V
    .locals 1

    .prologue
    .line 1942779
    iget-object v0, p0, LX/CsS;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1942780
    return-void
.end method

.method public final b(LX/CqC;)V
    .locals 1

    .prologue
    .line 1942723
    iget-object v0, p0, LX/CsS;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1942724
    return-void
.end method

.method public final b(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V
    .locals 1

    .prologue
    .line 1942725
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0}, LX/CsQ;->a()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/CsS;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V

    .line 1942726
    return-void
.end method

.method public final c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;
    .locals 1

    .prologue
    .line 1942727
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    if-eqz v0, :cond_0

    .line 1942728
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0, p1}, LX/CqD;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 1942729
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveFragmentIndex()I
    .locals 1

    .prologue
    .line 1942730
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 1942731
    const/4 v0, -0x1

    .line 1942732
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    goto :goto_0
.end method

.method public getFragmentCount()I
    .locals 1

    .prologue
    .line 1942733
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    if-nez v0, :cond_0

    .line 1942734
    const/4 v0, 0x0

    .line 1942735
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0}, LX/CqD;->getFragmentCount()I

    move-result v0

    goto :goto_0
.end method

.method public getHeader()LX/CsR;
    .locals 1

    .prologue
    .line 1942736
    iget-object v0, p0, LX/CsS;->k:LX/CsR;

    if-eqz v0, :cond_0

    .line 1942737
    iget-object v0, p0, LX/CsS;->k:LX/CsR;

    .line 1942738
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/CsS;->getHeaderResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, LX/CsS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CsR;

    goto :goto_0
.end method

.method public abstract getHeaderResourceId()I
.end method

.method public abstract getPageIndicatorResourceId()I
.end method

.method public getViewPager()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 1942720
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 1942721
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    .line 1942722
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/CsS;->getViewPagerResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, LX/CsS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    goto :goto_0
.end method

.method public abstract getViewPagerResourceId()I
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x677c5f38

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1942747
    invoke-super {p0}, Landroid/support/design/widget/CoordinatorLayout;->onFinishInflate()V

    .line 1942748
    invoke-virtual {p0}, LX/CsS;->getHeader()LX/CsR;

    move-result-object v0

    iput-object v0, p0, LX/CsS;->k:LX/CsR;

    .line 1942749
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    if-eqz v0, :cond_3

    .line 1942750
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    .line 1942751
    :goto_0
    move-object v0, v0

    .line 1942752
    iput-object v0, p0, LX/CsS;->l:LX/CsN;

    .line 1942753
    invoke-virtual {p0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    .line 1942754
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 1942755
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1942756
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    instance-of v0, v0, LX/Che;

    if-eqz v0, :cond_0

    .line 1942757
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    check-cast v0, LX/Che;

    invoke-interface {v0, p0}, LX/Che;->setFragmentPager(LX/CqD;)V

    .line 1942758
    :cond_0
    iget-object v0, p0, LX/CsS;->k:LX/CsR;

    if-eqz v0, :cond_1

    .line 1942759
    iget-object v0, p0, LX/CsS;->k:LX/CsR;

    invoke-virtual {p0, v0}, LX/CsS;->a(LX/0hc;)V

    .line 1942760
    iget-object v0, p0, LX/CsS;->k:LX/CsR;

    invoke-interface {v0, p0}, LX/Che;->setFragmentPager(LX/CqD;)V

    .line 1942761
    :cond_1
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    if-eqz v0, :cond_2

    .line 1942762
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    iget-object v2, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    invoke-interface {v0, v2}, LX/CsN;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1942763
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    invoke-virtual {p0, v0}, LX/CsS;->a(LX/0hc;)V

    .line 1942764
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    invoke-virtual {p0, v0}, LX/CsS;->a(LX/CqC;)V

    .line 1942765
    iget-object v0, p0, LX/CsS;->l:LX/CsN;

    invoke-interface {v0, p0}, LX/Che;->setFragmentPager(LX/CqD;)V

    .line 1942766
    :cond_2
    const/16 v0, 0x2d

    const v2, 0x76a24de3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    invoke-virtual {p0}, LX/CsS;->getPageIndicatorResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, LX/CsS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CsN;

    goto :goto_0
.end method

.method public setPagerAdapter(LX/CsQ;)V
    .locals 2

    .prologue
    .line 1942739
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    if-eqz v0, :cond_0

    .line 1942740
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0}, LX/CsQ;->a()LX/0gG;

    move-result-object v0

    iget-object v1, p0, LX/CsS;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 1942741
    :cond_0
    iput-object p1, p0, LX/CsS;->m:LX/CsQ;

    .line 1942742
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v0}, LX/CsQ;->a()LX/0gG;

    move-result-object v0

    iget-object v1, p0, LX/CsS;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 1942743
    invoke-virtual {p0}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, LX/CsS;->m:LX/CsQ;

    invoke-interface {v1}, LX/CsQ;->a()LX/0gG;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1942744
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    instance-of v0, v0, LX/Che;

    if-eqz v0, :cond_1

    .line 1942745
    iget-object v0, p0, LX/CsS;->m:LX/CsQ;

    check-cast v0, LX/Che;

    invoke-interface {v0, p0}, LX/Che;->setFragmentPager(LX/CqD;)V

    .line 1942746
    :cond_1
    return-void
.end method
