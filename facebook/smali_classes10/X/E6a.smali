.class public final LX/E6a;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/E6b;


# direct methods
.method public constructor <init>(LX/E6b;)V
    .locals 0

    .prologue
    .line 2080323
    iput-object p1, p0, LX/E6a;->a:LX/E6b;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2080324
    iget-object v0, p0, LX/E6a;->a:LX/E6b;

    iget-object v0, v0, LX/E6b;->a:LX/E6d;

    .line 2080325
    iget-object p0, v0, LX/Cfk;->a:LX/2jb;

    invoke-interface {p0, p1}, LX/2jb;->a(Ljava/lang/Throwable;)V

    .line 2080326
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2080327
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2080328
    if-eqz p1, :cond_2

    .line 2080329
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2080330
    if-eqz v0, :cond_2

    .line 2080331
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2080332
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2080333
    iget-object v1, p0, LX/E6a;->a:LX/E6b;

    .line 2080334
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2080335
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2080336
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;->b()LX/0us;

    move-result-object v2

    invoke-static {v1, v2}, LX/E6b;->a(LX/E6b;LX/0us;)V

    .line 2080337
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v4

    .line 2080338
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;

    .line 2080339
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/E6b;->a:LX/E6d;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object p1

    .line 2080340
    invoke-virtual {p0, p1}, LX/Cfk;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result v0

    move p0, v0

    .line 2080341
    if-eqz p0, :cond_0

    .line 2080342
    iget-object p0, v1, LX/E6b;->c:Ljava/util/List;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2080343
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2080344
    :cond_1
    iget-object v2, v1, LX/E6b;->a:LX/E6d;

    iget-object v3, v1, LX/E6b;->g:Ljava/lang/String;

    iget-object v5, v1, LX/E6b;->e:Ljava/lang/String;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    iget-object p0, v1, LX/E6b;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    .line 2080345
    invoke-virtual {v2, v3, v5, v4, p0}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2080346
    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 2080347
    :cond_2
    return-void
.end method
