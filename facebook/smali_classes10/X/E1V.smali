.class public final LX/E1V;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2069810
    iput-object p1, p0, LX/E1V;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    iput-object p2, p0, LX/E1V;->a:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2069802
    iget-object v0, p0, LX/E1V;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2069803
    iget-object v0, p0, LX/E1V;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    iget-object v0, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2069804
    iget-object v0, p0, LX/E1V;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    .line 2069805
    iget-object v1, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2cy;

    invoke-virtual {v1}, LX/2cy;->c()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Z)V

    .line 2069806
    iget-object v2, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->u:Landroid/widget/TextView;

    iget-object v1, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2cy;

    invoke-virtual {v1}, LX/2cy;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2069807
    iget-object v0, p0, LX/E1V;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    iget-object v0, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->c:LX/03V;

    const-string v1, "place_tips_settings_save"

    const-string v2, "Failed to update gravity settings"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2069808
    return-void

    .line 2069809
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2069798
    check-cast p1, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 2069799
    iget-object v0, p0, LX/E1V;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2069800
    iget-object v0, p0, LX/E1V;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    invoke-static {v0, p1}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    .line 2069801
    return-void
.end method
