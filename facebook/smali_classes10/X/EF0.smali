.class public LX/EF0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/webrtc/ConferenceCall$Listener;
.implements Lcom/facebook/webrtc/IWebrtcUiInterface;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final i:Ljava/lang/Object;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/0Uh;

.field private final d:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

.field private e:LX/0Sh;

.field public f:LX/EEv;

.field private g:Lcom/facebook/webrtc/ConferenceCall;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2094601
    const-class v0, LX/EF0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EF0;->a:Ljava/lang/String;

    .line 2094602
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EF0;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2094603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2094604
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EF0;->h:Ljava/util/List;

    .line 2094605
    iput-object p1, p0, LX/EF0;->b:LX/0ad;

    .line 2094606
    iput-object p2, p0, LX/EF0;->c:LX/0Uh;

    .line 2094607
    iput-object p3, p0, LX/EF0;->d:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    .line 2094608
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EF0;->h:Ljava/util/List;

    .line 2094609
    iput-object p4, p0, LX/EF0;->e:LX/0Sh;

    .line 2094610
    return-void
.end method


# virtual methods
.method public final OnUserStateUpdate(Lcom/facebook/webrtc/ConferenceCall;[Ljava/lang/String;[I)V
    .locals 0

    .prologue
    .line 2094611
    return-void
.end method

.method public final addRemoteVideoTrack(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2094612
    return-void
.end method

.method public final handleError(I)V
    .locals 0

    .prologue
    .line 2094613
    return-void
.end method

.method public final hideCallUI(ILjava/lang/String;JZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2094614
    return-void
.end method

.method public final initializeCall(JJZ)V
    .locals 0

    .prologue
    .line 2094615
    return-void
.end method

.method public final localMediaStateChanged(ZZZ)V
    .locals 0

    .prologue
    .line 2094616
    return-void
.end method

.method public final onAudioLevel(II)V
    .locals 0

    .prologue
    .line 2094617
    return-void
.end method

.method public final onAudioLevelsUpdate(Lcom/facebook/webrtc/ConferenceCall;[Ljava/lang/String;[I)V
    .locals 0

    .prologue
    .line 2094618
    return-void
.end method

.method public final onCallEnded(Lcom/facebook/webrtc/ConferenceCall;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2094684
    return-void
.end method

.method public final onCallJoined(Lcom/facebook/webrtc/ConferenceCall;)V
    .locals 0

    .prologue
    .line 2094619
    return-void
.end method

.method public final onDataMessage(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 2094620
    return-void
.end method

.method public final onDataReceived(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 2094621
    return-void
.end method

.method public final onDiscoveryRequest(JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2094622
    return-void
.end method

.method public final onDiscoveryResponse(JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2094623
    return-void
.end method

.method public final onDominantSpeakerUpdate(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2094624
    return-void
.end method

.method public final onEscalationRequest(Z)V
    .locals 0

    .prologue
    .line 2094625
    return-void
.end method

.method public final onEscalationResponse(Z)V
    .locals 0

    .prologue
    .line 2094626
    return-void
.end method

.method public final onEscalationSuccess()V
    .locals 0

    .prologue
    .line 2094579
    return-void
.end method

.method public final onEscalationTimeout()V
    .locals 0

    .prologue
    .line 2094627
    return-void
.end method

.method public final onGameUpdate([B)V
    .locals 0

    .prologue
    .line 2094600
    return-void
.end method

.method public final onIncomingCall(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;[Ljava/lang/String;IZ)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2094628
    invoke-virtual {p1}, Lcom/facebook/webrtc/ConferenceCall;->callId()J

    move-result-wide v2

    .line 2094629
    new-instance v5, LX/EEw;

    invoke-direct {v5}, LX/EEw;-><init>()V

    move-object v5, v5

    .line 2094630
    iput-wide v2, v5, LX/EEw;->a:J

    .line 2094631
    move-object v5, v5

    .line 2094632
    const/4 v6, 0x1

    .line 2094633
    iput-boolean v6, v5, LX/EEw;->l:Z

    .line 2094634
    move-object v5, v5

    .line 2094635
    sget-object v6, LX/EEy;->FbWebRTCCallStateInit:LX/EEy;

    .line 2094636
    iput-object v6, v5, LX/EEw;->r:LX/EEy;

    .line 2094637
    move-object v5, v5

    .line 2094638
    new-instance v6, LX/EEv;

    invoke-direct {v6, v5}, LX/EEv;-><init>(LX/EEw;)V

    move-object v5, v6

    .line 2094639
    iput-object v5, p0, LX/EF0;->f:LX/EEv;

    .line 2094640
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEs;->INBOUND:LX/EEs;

    iput-object v3, v2, LX/EEv;->e:LX/EEs;

    .line 2094641
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEt;->MULTIWAY:LX/EEt;

    iput-object v3, v2, LX/EEv;->f:LX/EEt;

    .line 2094642
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    invoke-virtual {p1}, Lcom/facebook/webrtc/ConferenceCall;->conferenceName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/EEv;->g:Ljava/lang/String;

    .line 2094643
    packed-switch p4, :pswitch_data_0

    .line 2094644
    sget-object v2, LX/EF0;->a:Ljava/lang/String;

    const-string v3, "Invalid incoming conference call type %d"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2094645
    :goto_0
    return-void

    .line 2094646
    :pswitch_0
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEu;->CONFERENCE_VOICE:LX/EEu;

    iput-object v3, v2, LX/EEv;->d:LX/EEu;

    .line 2094647
    :goto_1
    if-eqz p3, :cond_1

    .line 2094648
    invoke-static {p3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2094649
    iget-object v3, p0, LX/EF0;->f:LX/EEv;

    iput-object v2, v3, LX/EEv;->b:LX/0Px;

    .line 2094650
    if-nez v2, :cond_5

    .line 2094651
    sget-object v3, LX/EF0;->a:Ljava/lang/String;

    const-string v4, "initConferenceUserStates encounters null user list"

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094652
    iget-object v3, p0, LX/EF0;->f:LX/EEv;

    .line 2094653
    sget-object v4, LX/0Rg;->a:LX/0Rg;

    move-object v4, v4

    .line 2094654
    iput-object v4, v3, LX/EEv;->c:LX/0P1;

    .line 2094655
    :goto_2
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    iget-object v2, v2, LX/EEv;->d:LX/EEu;

    invoke-static {v2}, LX/EEu;->isConferenceCall(LX/EEu;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2094656
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    iget-object v3, p0, LX/EF0;->d:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-virtual {v3}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->shouldEnableVideo()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/EF0;->c:LX/0Uh;

    const/16 v4, 0x5f6

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_3
    iput-boolean v0, v2, LX/EEv;->m:Z

    .line 2094657
    :goto_4
    iget-object v0, p0, LX/EF0;->f:LX/EEv;

    iget-object v0, v0, LX/EEv;->d:LX/EEu;

    invoke-static {v0}, LX/EEu;->isNonInstantVideoCall(LX/EEu;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EF0;->f:LX/EEv;

    iget-boolean v0, v0, LX/EEv;->m:Z

    if-eqz v0, :cond_4

    .line 2094658
    sget-object v0, LX/EEy;->FbWebRTCCallStateInitInboundDirectVideo:LX/EEy;

    .line 2094659
    :goto_5
    iput-object p1, p0, LX/EF0;->g:Lcom/facebook/webrtc/ConferenceCall;

    .line 2094660
    iget-object v1, p0, LX/EF0;->f:LX/EEv;

    iget-object v1, v1, LX/EEv;->r:LX/EEy;

    if-eq v1, v0, :cond_0

    .line 2094661
    iget-object v1, p0, LX/EF0;->f:LX/EEv;

    iget-object v1, v1, LX/EEv;->r:LX/EEy;

    invoke-static {v1, v0}, LX/EEy;->validateStateTransition(LX/EEy;LX/EEy;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2094662
    sget-object v1, LX/EF0;->a:Ljava/lang/String;

    const-string v2, "Invalid state transition from %s to %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/EF0;->f:LX/EEv;

    iget-object v5, v5, LX/EEv;->r:LX/EEy;

    invoke-virtual {v5}, LX/EEy;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, LX/EEy;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2094663
    :cond_0
    :goto_6
    goto/16 :goto_0

    .line 2094664
    :pswitch_1
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEu;->VOICE:LX/EEu;

    iput-object v3, v2, LX/EEv;->d:LX/EEu;

    goto/16 :goto_1

    .line 2094665
    :pswitch_2
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEu;->DIRECT_VIDEO:LX/EEu;

    iput-object v3, v2, LX/EEv;->d:LX/EEu;

    goto/16 :goto_1

    .line 2094666
    :pswitch_3
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    sget-object v3, LX/EEu;->CONFERENCE_VIDEO:LX/EEu;

    iput-object v3, v2, LX/EEv;->d:LX/EEu;

    goto/16 :goto_1

    .line 2094667
    :cond_1
    iget-object v2, p0, LX/EF0;->f:LX/EEv;

    .line 2094668
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2094669
    iput-object v3, v2, LX/EEv;->b:LX/0Px;

    goto/16 :goto_2

    :cond_2
    move v0, v1

    .line 2094670
    goto :goto_3

    .line 2094671
    :cond_3
    iget-object v0, p0, LX/EF0;->f:LX/EEv;

    iget-object v1, p0, LX/EF0;->d:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-virtual {v1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->shouldEnableVideo()Z

    move-result v1

    iput-boolean v1, v0, LX/EEv;->m:Z

    goto :goto_4

    .line 2094672
    :cond_4
    sget-object v0, LX/EEy;->FbWebRTCCallStateInitInbound:LX/EEy;

    goto :goto_5

    .line 2094673
    :cond_5
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 2094674
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_7
    if-ge v4, v6, :cond_7

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2094675
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 2094676
    sget-object v7, LX/EEz;->UNKNOWN:LX/EEz;

    invoke-virtual {v5, v3, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2094677
    :cond_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_7

    .line 2094678
    :cond_7
    iget-object v3, p0, LX/EF0;->f:LX/EEv;

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    iput-object v4, v3, LX/EEv;->c:LX/0P1;

    goto/16 :goto_2

    .line 2094679
    :cond_8
    iget-object v1, p0, LX/EF0;->f:LX/EEv;

    iput-object v0, v1, LX/EEv;->r:LX/EEy;

    .line 2094680
    iget-object v1, p0, LX/EF0;->f:LX/EEv;

    .line 2094681
    new-instance v2, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;

    invoke-direct {v2, v1}, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;-><init>(LX/EEv;)V

    .line 2094682
    iget-object v1, p0, LX/EF0;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_8

    .line 2094683
    :cond_9
    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final onIncomingMissedCall(JJ)V
    .locals 0

    .prologue
    .line 2094578
    return-void
.end method

.method public final onMediaConnectionUpdate(Lcom/facebook/webrtc/ConferenceCall;IIZI)V
    .locals 0

    .prologue
    .line 2094580
    return-void
.end method

.method public final onMediaStatusUpdate(Lcom/facebook/webrtc/ConferenceCall;[J[Ljava/lang/String;[Ljava/lang/String;[I[Z)V
    .locals 0

    .prologue
    .line 2094581
    return-void
.end method

.method public final onPingAckMessageReceived(JJ)V
    .locals 0

    .prologue
    .line 2094582
    return-void
.end method

.method public final remoteMediaStateChanged(ZZZ)V
    .locals 0

    .prologue
    .line 2094583
    return-void
.end method

.method public final removeRemoteVideoTrack(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2094584
    return-void
.end method

.method public final setWebrtcManager(LX/2Tm;)V
    .locals 0

    .prologue
    .line 2094585
    return-void
.end method

.method public final showConnectionDetails(ZIIZZIIIZ)V
    .locals 0

    .prologue
    .line 2094586
    return-void
.end method

.method public final switchToContactingUI()V
    .locals 0

    .prologue
    .line 2094587
    return-void
.end method

.method public final switchToIncomingCallUI(JJZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2094588
    return-void
.end method

.method public final switchToRingingUI()V
    .locals 0

    .prologue
    .line 2094589
    return-void
.end method

.method public final switchToStreamingUI(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2094590
    return-void
.end method

.method public final updateRemoteVideoSupport(ZJ)V
    .locals 0

    .prologue
    .line 2094591
    return-void
.end method

.method public final updateStatesAndCallDuration()V
    .locals 0

    .prologue
    .line 2094592
    return-void
.end method

.method public final webRTCControlRPC_AcceptIncomingCall(J)V
    .locals 0

    .prologue
    .line 2094593
    return-void
.end method

.method public final webRTCControlRPC_DisableVideo()V
    .locals 0

    .prologue
    .line 2094594
    return-void
.end method

.method public final webRTCControlRPC_EnableVideo()V
    .locals 0

    .prologue
    .line 2094595
    return-void
.end method

.method public final webRTCControlRPC_StartOutgoingCall(JZ)V
    .locals 0

    .prologue
    .line 2094596
    return-void
.end method

.method public final webRTCControlRPC_ToggleSpeakerPhone()V
    .locals 0

    .prologue
    .line 2094597
    return-void
.end method

.method public final webRTCControlRPC_VolumeDown()V
    .locals 0

    .prologue
    .line 2094598
    return-void
.end method

.method public final webRTCControlRPC_VolumeUp()V
    .locals 0

    .prologue
    .line 2094599
    return-void
.end method
