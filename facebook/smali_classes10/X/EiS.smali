.class public final LX/EiS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V
    .locals 0

    .prologue
    .line 2159442
    iput-object p1, p0, LX/EiS;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x48dd82ea

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2159443
    iget-object v1, p0, LX/EiS;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159444
    iget-boolean v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v1, v2

    .line 2159445
    if-nez v1, :cond_1

    .line 2159446
    iget-object v1, p0, LX/EiS;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    .line 2159447
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2159448
    const-string v3, "confirmationSendConfirmationCodeParams"

    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159449
    iget-object v6, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v4, v6

    .line 2159450
    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2159451
    iget-object v3, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159452
    iget-object v4, v3, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v3, v4

    .line 2159453
    iget-object v3, v3, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v4, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v3, v4, :cond_3

    .line 2159454
    const v4, 0x7f0833f2

    .line 2159455
    const v3, 0x7f0833dd

    move v9, v3

    move v10, v4

    .line 2159456
    :goto_0
    iget-object v3, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159457
    iget-object v6, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v4, v6

    .line 2159458
    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159459
    iget-object v6, v3, LX/2U9;->a:LX/0Zb;

    sget-object v7, LX/Eiw;->RESEND_CODE_ATTEMPT:LX/Eiw;

    invoke-virtual {v7}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2159460
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2159461
    const-string v7, "confirmation"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159462
    const-string v7, "current_contactpoint_type"

    invoke-virtual {v4}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159463
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2159464
    :cond_0
    iget-object v3, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->b:LX/0aG;

    const-string v4, "confirmation_send_confirmation_code"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    iget-object v7, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->u:Lcom/facebook/common/callercontext/CallerContext;

    const v8, 0x3414ddf8

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    new-instance v4, LX/4At;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v10}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v3, v4}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2159465
    new-instance v4, LX/EiQ;

    invoke-direct {v4, v1, v9}, LX/EiQ;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;I)V

    iget-object v5, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2159466
    :goto_1
    const v1, 0x401e4a6f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2159467
    :cond_1
    iget-object v1, p0, LX/EiS;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    const/4 v5, 0x0

    .line 2159468
    iget-object v3, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    sget-object v4, LX/Eiw;->RESEND_CODE:LX/Eiw;

    invoke-virtual {v3, v4, v5, v5}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159469
    new-instance v3, LX/4JB;

    invoke-direct {v3}, LX/4JB;-><init>()V

    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159470
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v4, v5

    .line 2159471
    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    .line 2159472
    const-string v5, "country"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159473
    move-object v3, v3

    .line 2159474
    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159475
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v4, v5

    .line 2159476
    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2159477
    const-string v5, "contact_point"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159478
    move-object v3, v3

    .line 2159479
    const-string v4, "PHONE_ACQUISITION_PROMO"

    .line 2159480
    const-string v5, "source"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159481
    move-object v3, v3

    .line 2159482
    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159483
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    move-object v4, v5

    .line 2159484
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "null"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2159485
    :cond_2
    const-string v5, "ACQUISITION"

    .line 2159486
    :goto_2
    move-object v4, v5

    .line 2159487
    const-string v5, "promo_type"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159488
    move-object v3, v3

    .line 2159489
    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159490
    iget-object v5, v4, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2159491
    const-string v5, "qp_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159492
    move-object v3, v3

    .line 2159493
    new-instance v4, LX/Eip;

    invoke-direct {v4}, LX/Eip;-><init>()V

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 2159494
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 2159495
    iget-object v8, v4, LX/Eip;->a:Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel$ViewerModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2159496
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 2159497
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 2159498
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 2159499
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 2159500
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2159501
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2159502
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2159503
    new-instance v8, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;

    invoke-direct {v8, v7}, Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;-><init>(LX/15i;)V

    .line 2159504
    move-object v4, v8

    .line 2159505
    new-instance v5, LX/Ein;

    invoke-direct {v5}, LX/Ein;-><init>()V

    move-object v5, v5

    .line 2159506
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/Ein;

    .line 2159507
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v3

    .line 2159508
    iget-object v4, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->i:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2159509
    new-instance v4, LX/EiR;

    invoke-direct {v4, v1}, LX/EiR;-><init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159510
    iget-object v5, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->n:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2159511
    goto/16 :goto_1

    .line 2159512
    :cond_3
    const v4, 0x7f0833f3

    .line 2159513
    const v3, 0x7f0833dc

    move v9, v3

    move v10, v4

    goto/16 :goto_0

    :cond_4
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2
.end method
