.class public LX/E4T;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field public b:Landroid/widget/LinearLayout;

.field public final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2076728
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/E4T;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2076729
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2076722
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2076723
    const v0, 0x7f031158

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2076724
    const v0, 0x7f0d28ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/E4T;->b:Landroid/widget/LinearLayout;

    .line 2076725
    const v0, 0x7f0d28f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/E4T;->c:Landroid/view/View;

    .line 2076726
    iget-object v0, p0, LX/E4T;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 2076727
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2076721
    iget-boolean v0, p0, LX/E4T;->a:Z

    return v0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2076730
    iget-object v0, p0, LX/E4T;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2076731
    return-void
.end method

.method public getXOutView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2076720
    iget-object v0, p0, LX/E4T;->c:Landroid/view/View;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x30460fd7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076716
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2076717
    const/4 v1, 0x1

    .line 2076718
    iput-boolean v1, p0, LX/E4T;->a:Z

    .line 2076719
    const/16 v1, 0x2d

    const v2, -0x3abaedfb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x68a8ea32

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076710
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2076711
    const/4 v1, 0x0

    .line 2076712
    iput-boolean v1, p0, LX/E4T;->a:Z

    .line 2076713
    const/16 v1, 0x2d

    const v2, -0x389da049

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 2076714
    iget-object v0, p0, LX/E4T;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2076715
    return-void
.end method
