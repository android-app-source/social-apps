.class public LX/Dcp;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Dcp;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019039
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2019040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->eD:Ljava/lang/String;

    const-string v2, "{com.facebook.katana.profile.id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGE_PHOTOS_BY_CATEGORY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2019041
    return-void
.end method

.method public static a(LX/0QB;)LX/Dcp;
    .locals 3

    .prologue
    .line 2019042
    sget-object v0, LX/Dcp;->a:LX/Dcp;

    if-nez v0, :cond_1

    .line 2019043
    const-class v1, LX/Dcp;

    monitor-enter v1

    .line 2019044
    :try_start_0
    sget-object v0, LX/Dcp;->a:LX/Dcp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2019045
    if-eqz v2, :cond_0

    .line 2019046
    :try_start_1
    new-instance v0, LX/Dcp;

    invoke-direct {v0}, LX/Dcp;-><init>()V

    .line 2019047
    move-object v0, v0

    .line 2019048
    sput-object v0, LX/Dcp;->a:LX/Dcp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2019049
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2019050
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2019051
    :cond_1
    sget-object v0, LX/Dcp;->a:LX/Dcp;

    return-object v0

    .line 2019052
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2019053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
