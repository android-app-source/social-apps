.class public final LX/ESQ;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

.field public b:[F

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 1

    .prologue
    .line 2123016
    iput-object p1, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 2123017
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/ESQ;->b:[F

    .line 2123018
    const/4 v0, 0x0

    iput v0, p0, LX/ESQ;->c:I

    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2123019
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getHeight()I

    move-result v1

    .line 2123020
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2123021
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v0

    .line 2123022
    iget-object v2, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->B:LX/ESc;

    sget-object v3, LX/ESc;->VISIBLE:LX/ESc;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->s:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, p4, v2

    if-gez v2, :cond_1

    neg-int v1, v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 2123023
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-virtual {v0, v4}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(Z)V

    .line 2123024
    :cond_0
    :goto_0
    return v4

    .line 2123025
    :cond_1
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->B:LX/ESc;

    sget-object v1, LX/ESc;->HIDDEN:LX/ESc;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->s:I

    int-to-float v0, v0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_0

    .line 2123026
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(Z)V

    goto :goto_0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .prologue
    const/high16 v7, 0x40a00000    # 5.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2123027
    iget-object v0, p0, LX/ESQ;->b:[F

    iget v1, p0, LX/ESQ;->c:I

    aput p4, v0, v1

    .line 2123028
    iget v0, p0, LX/ESQ;->c:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/ESQ;->b:[F

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, LX/ESQ;->c:I

    .line 2123029
    const/4 p2, 0x0

    const/4 v1, 0x0

    .line 2123030
    move v0, v1

    move v2, v1

    move v3, v1

    .line 2123031
    :goto_0
    iget-object p1, p0, LX/ESQ;->b:[F

    array-length p1, p1

    if-ge v0, p1, :cond_1

    .line 2123032
    iget-object p1, p0, LX/ESQ;->b:[F

    aget p1, p1, v0

    cmpl-float p1, p1, p2

    if-lez p1, :cond_0

    .line 2123033
    add-int/lit8 v3, v3, 0x1

    .line 2123034
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2123035
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2123036
    :cond_1
    cmpl-float v0, p4, p2

    if-ltz v0, :cond_2

    if-gt v3, v2, :cond_3

    :cond_2
    cmpg-float v0, p4, p2

    if-gez v0, :cond_4

    if-le v2, v3, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    move v0, v1

    .line 2123037
    if-nez v0, :cond_6

    .line 2123038
    :cond_5
    :goto_2
    return v6

    .line 2123039
    :cond_6
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getHeight()I

    move-result v1

    .line 2123040
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v3, -0x1

    invoke-direct {v0, v3, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 2123041
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2123042
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v0

    .line 2123043
    neg-int v3, v1

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_7

    cmpl-float v3, p4, v4

    if-gtz v3, :cond_5

    :cond_7
    cmpl-float v3, v0, v4

    if-ltz v3, :cond_8

    cmpg-float v3, p4, v4

    if-ltz v3, :cond_5

    .line 2123044
    :cond_8
    cmpg-float v3, p4, v4

    if-gez v3, :cond_9

    .line 2123045
    mul-float v1, p4, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2123046
    :goto_3
    iget-object v1, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    .line 2123047
    iput v0, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->t:I

    .line 2123048
    invoke-virtual {v2, v5, v0, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2123049
    iget-object v0, p0, LX/ESQ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0, v2}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 2123050
    :cond_9
    mul-float v3, p4, v7

    sub-float/2addr v0, v3

    float-to-int v0, v0

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_3
.end method
