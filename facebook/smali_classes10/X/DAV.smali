.class public LX/DAV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3OQ;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1971318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971319
    iput-object p1, p0, LX/DAV;->a:Ljava/lang/String;

    .line 1971320
    iput-object p2, p0, LX/DAV;->b:Ljava/lang/String;

    .line 1971321
    iput-object p3, p0, LX/DAV;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1971322
    iput-object p4, p0, LX/DAV;->d:Ljava/lang/String;

    .line 1971323
    iput-object p5, p0, LX/DAV;->e:LX/0Px;

    .line 1971324
    return-void
.end method


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971325
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAV;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
