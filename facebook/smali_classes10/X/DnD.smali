.class public final LX/DnD;
.super LX/DnA;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2040083
    iput-object p1, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-direct {p0, p2}, LX/DnA;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2040084
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b1e6f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2040085
    iget-object v3, p0, LX/DnA;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v0, v0, v0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 2040086
    iget-object v0, p0, LX/DnA;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->f:LX/0wM;

    iget-object v4, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0207fd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00a3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2040087
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    .line 2040088
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f082bf5

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->d:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v2, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->d:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2040089
    :goto_1
    iget-object v1, p0, LX/DnA;->n:Landroid/widget/TextView;

    iget-object v2, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082bf1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040090
    iget-object v1, p0, LX/DnA;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040091
    return-void

    :cond_0
    move v0, v2

    .line 2040092
    goto :goto_0

    .line 2040093
    :cond_1
    iget-object v0, p0, LX/DnD;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->d:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
