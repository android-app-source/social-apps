.class public final LX/CtR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V
    .locals 0

    .prologue
    .line 1944815
    iput-object p1, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 1944808
    iget-object v0, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget-boolean v0, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v0, :cond_0

    .line 1944809
    iget-object v0, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    iget-object v1, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 1944810
    :goto_0
    iget-object v0, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    const/4 v1, 0x0

    const v2, 0x449c4000    # 1250.0f

    .line 1944811
    invoke-static {v0, v1, v2}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V

    .line 1944812
    iget-object v0, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {v0, p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1944813
    return-void

    .line 1944814
    :cond_0
    iget-object v0, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    iget-object v1, p0, LX/CtR;->a:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    goto :goto_0
.end method
