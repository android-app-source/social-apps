.class public final LX/Eys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/jewel/protocol/SetFriendRequestsAudienceMutationModels$SetFriendRequestsAudienceMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;)V
    .locals 0

    .prologue
    .line 2186607
    iput-object p1, p0, LX/Eys;->a:Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2186608
    iget-object v0, p0, LX/Eys;->a:Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsPrivacySettingService;->c:LX/03V;

    const-string v1, "friend_requests_restrict_audience"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Mutation call for new privacy setting returned an error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/4 v2, 0x1

    .line 2186609
    iput v2, v1, LX/0VK;->e:I

    .line 2186610
    move-object v1, v1

    .line 2186611
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2186612
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186613
    return-void
.end method
