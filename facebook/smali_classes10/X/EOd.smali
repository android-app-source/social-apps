.class public LX/EOd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOb;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EOg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2114873
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EOd;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EOg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114874
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2114875
    iput-object p1, p0, LX/EOd;->b:LX/0Ot;

    .line 2114876
    return-void
.end method

.method public static a(LX/0QB;)LX/EOd;
    .locals 4

    .prologue
    .line 2114877
    const-class v1, LX/EOd;

    monitor-enter v1

    .line 2114878
    :try_start_0
    sget-object v0, LX/EOd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114879
    sput-object v2, LX/EOd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114880
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114881
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114882
    new-instance v3, LX/EOd;

    const/16 p0, 0x346b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOd;-><init>(LX/0Ot;)V

    .line 2114883
    move-object v0, v3

    .line 2114884
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2114888
    check-cast p2, LX/EOc;

    .line 2114889
    iget-object v0, p0, LX/EOd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EOg;

    iget-object v1, p2, LX/EOc;->a:LX/CzL;

    iget-object v2, p2, LX/EOc;->b:LX/EOS;

    iget v3, p2, LX/EOc;->c:I

    const/16 p2, 0x8

    const/4 v5, 0x0

    .line 2114890
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v6, -0x2

    invoke-interface {v4, p2, v6}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v9

    .line 2114891
    iget-object v4, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2114892
    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bN()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v10

    .line 2114893
    invoke-static {p1}, LX/EOg;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    .line 2114894
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v7, v5

    move v8, v5

    :goto_0
    if-ge v7, v11, :cond_2

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2114895
    iget-object v5, v0, LX/EOg;->a:LX/EOj;

    invoke-virtual {v5, p1}, LX/EOj;->c(LX/1De;)LX/EOh;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->a()LX/8d2;

    move-result-object p0

    invoke-virtual {v1, p0, v8}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/EOh;->a(LX/CzL;)LX/EOh;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->p()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->p()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->a()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {p0, v5}, LX/EOh;->b(Ljava/lang/String;)LX/EOh;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->p()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->p()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->b()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-virtual {p0, v5}, LX/EOh;->c(Ljava/lang/String;)LX/EOh;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    .line 2114896
    iget-object p0, v5, LX/EOh;->a:LX/EOi;

    iput-object v4, p0, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2114897
    move-object v4, v5

    .line 2114898
    invoke-virtual {v4, v2}, LX/EOh;->a(LX/EOS;)LX/EOh;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, p2, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2114899
    rem-int v4, v8, v3

    add-int/lit8 v5, v3, -0x1

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 2114900
    if-eqz v4, :cond_4

    .line 2114901
    invoke-interface {v9, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2114902
    invoke-static {p1}, LX/EOg;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2114903
    :goto_4
    add-int/lit8 v6, v8, 0x1

    .line 2114904
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v8, v6

    move-object v6, v4

    goto :goto_0

    .line 2114905
    :cond_0
    const-string v5, ""

    goto :goto_1

    :cond_1
    const-string v5, ""

    goto :goto_2

    .line 2114906
    :cond_2
    :goto_5
    rem-int v4, v8, v3

    if-eqz v4, :cond_3

    .line 2114907
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2114908
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 2114909
    :cond_3
    invoke-interface {v9, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2114910
    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2114911
    return-object v0

    :cond_4
    move-object v4, v6

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2114912
    invoke-static {}, LX/1dS;->b()V

    .line 2114913
    const/4 v0, 0x0

    return-object v0
.end method
