.class public LX/DVG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DVF;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2004913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2004914
    return-void
.end method

.method private static b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2004915
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2004916
    const-string v1, "title"

    const v2, 0x7f083081

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2004917
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_MEMBER_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2004918
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2004919
    if-eqz p1, :cond_0

    .line 2004920
    const-string v1, "group_visibility"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2004921
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2004922
    invoke-static {p1, p2, p3}, LX/DVG;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2004923
    invoke-static {p1, p2, p4}, LX/DVG;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2004924
    const-string v1, "enable_email_invite"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2004925
    return-object v0
.end method
