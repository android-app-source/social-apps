.class public final LX/DxX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Landroid/support/v4/app/Fragment;

.field public final synthetic c:LX/DxY;


# direct methods
.method public constructor <init>(LX/DxY;Landroid/app/Activity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2062949
    iput-object p1, p0, LX/DxX;->c:LX/DxY;

    iput-object p2, p0, LX/DxX;->a:Landroid/app/Activity;

    iput-object p3, p0, LX/DxX;->b:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2062950
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2062951
    const v1, 0x7f0d31f9

    if-ne v0, v1, :cond_1

    .line 2062952
    iget-object v0, p0, LX/DxX;->c:LX/DxY;

    iget-object v1, p0, LX/DxX;->a:Landroid/app/Activity;

    .line 2062953
    iget-object v2, v0, LX/DxY;->j:LX/DxW;

    iget-object v3, v0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2, v3, v1}, LX/DxW;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/app/Activity;)V

    .line 2062954
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2062955
    :cond_1
    const v1, 0x7f0d31fa

    if-ne v0, v1, :cond_2

    .line 2062956
    iget-object v0, p0, LX/DxX;->c:LX/DxY;

    iget-object v0, v0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v2, 0x0

    .line 2062957
    invoke-static {v0}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v1

    .line 2062958
    iput-object v2, v1, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2062959
    move-object v1, v1

    .line 2062960
    iput-object v2, v1, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2062961
    move-object v1, v1

    .line 2062962
    iput-object v2, v1, LX/4Vp;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2062963
    move-object v1, v1

    .line 2062964
    iput-object v2, v1, LX/4Vp;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 2062965
    move-object v1, v1

    .line 2062966
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2062967
    move-object v0, v1

    .line 2062968
    iget-object v1, p0, LX/DxX;->c:LX/DxY;

    .line 2062969
    invoke-virtual {v1}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v2

    move-object v1, v2

    .line 2062970
    invoke-static {v1, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAlbum;)Landroid/content/Intent;

    move-result-object v0

    .line 2062971
    iget-object v1, p0, LX/DxX;->c:LX/DxY;

    iget-object v1, v1, LX/DxY;->i:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x4

    iget-object v3, p0, LX/DxX;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2062972
    :cond_2
    const v1, 0x7f0d31fb

    if-ne v0, v1, :cond_3

    .line 2062973
    iget-object v0, p0, LX/DxX;->c:LX/DxY;

    .line 2062974
    invoke-virtual {v0}, LX/9b8;->a()V

    .line 2062975
    goto :goto_0

    .line 2062976
    :cond_3
    const v1, 0x7f0d31fc

    if-ne v0, v1, :cond_0

    .line 2062977
    iget-object v0, p0, LX/DxX;->c:LX/DxY;

    iget-object v1, p0, LX/DxX;->a:Landroid/app/Activity;

    .line 2062978
    invoke-virtual {v0, v1}, LX/9b8;->a(Landroid/app/Activity;)V

    .line 2062979
    goto :goto_0
.end method
