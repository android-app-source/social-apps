.class public final enum LX/CkZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CkZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CkZ;

.field public static final enum AFTER:LX/CkZ;

.field public static final enum BEFORE:LX/CkZ;

.field public static final enum END:LX/CkZ;

.field public static final enum EXACT:LX/CkZ;

.field public static final enum MAX:LX/CkZ;

.field private static final sAfterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CkZ;",
            ">;"
        }
    .end annotation
.end field

.field private static final sBeforeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CkZ;",
            ">;"
        }
    .end annotation
.end field

.field private static final sEndMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CkZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1931089
    new-instance v0, LX/CkZ;

    const-string v1, "BEFORE"

    invoke-direct {v0, v1, v2}, LX/CkZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CkZ;->BEFORE:LX/CkZ;

    .line 1931090
    new-instance v0, LX/CkZ;

    const-string v1, "AFTER"

    invoke-direct {v0, v1, v3}, LX/CkZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CkZ;->AFTER:LX/CkZ;

    .line 1931091
    new-instance v0, LX/CkZ;

    const-string v1, "END"

    invoke-direct {v0, v1, v4}, LX/CkZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CkZ;->END:LX/CkZ;

    .line 1931092
    new-instance v0, LX/CkZ;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v5}, LX/CkZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CkZ;->EXACT:LX/CkZ;

    .line 1931093
    new-instance v0, LX/CkZ;

    const-string v1, "MAX"

    invoke-direct {v0, v1, v6}, LX/CkZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CkZ;->MAX:LX/CkZ;

    .line 1931094
    const/4 v0, 0x5

    new-array v0, v0, [LX/CkZ;

    sget-object v1, LX/CkZ;->BEFORE:LX/CkZ;

    aput-object v1, v0, v2

    sget-object v1, LX/CkZ;->AFTER:LX/CkZ;

    aput-object v1, v0, v3

    sget-object v1, LX/CkZ;->END:LX/CkZ;

    aput-object v1, v0, v4

    sget-object v1, LX/CkZ;->EXACT:LX/CkZ;

    aput-object v1, v0, v5

    sget-object v1, LX/CkZ;->MAX:LX/CkZ;

    aput-object v1, v0, v6

    sput-object v0, LX/CkZ;->$VALUES:[LX/CkZ;

    .line 1931095
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931096
    sput-object v0, LX/CkZ;->sBeforeMap:Ljava/util/Map;

    const-string v1, "before"

    sget-object v2, LX/CkZ;->BEFORE:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931097
    sget-object v0, LX/CkZ;->sBeforeMap:Ljava/util/Map;

    const-string v1, "above"

    sget-object v2, LX/CkZ;->BEFORE:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931098
    sget-object v0, LX/CkZ;->sBeforeMap:Ljava/util/Map;

    const-string v1, "left_of"

    sget-object v2, LX/CkZ;->BEFORE:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931099
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931100
    sput-object v0, LX/CkZ;->sAfterMap:Ljava/util/Map;

    const-string v1, "after"

    sget-object v2, LX/CkZ;->AFTER:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931101
    sget-object v0, LX/CkZ;->sAfterMap:Ljava/util/Map;

    const-string v1, "below"

    sget-object v2, LX/CkZ;->AFTER:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931102
    sget-object v0, LX/CkZ;->sAfterMap:Ljava/util/Map;

    const-string v1, "right_of"

    sget-object v2, LX/CkZ;->AFTER:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931104
    sput-object v0, LX/CkZ;->sEndMap:Ljava/util/Map;

    const-string v1, "end"

    sget-object v2, LX/CkZ;->END:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931105
    sget-object v0, LX/CkZ;->sEndMap:Ljava/util/Map;

    const-string v1, "bottom"

    sget-object v2, LX/CkZ;->END:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931106
    sget-object v0, LX/CkZ;->sEndMap:Ljava/util/Map;

    const-string v1, "right"

    sget-object v2, LX/CkZ;->END:LX/CkZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931107
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1931108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CkZ;
    .locals 1

    .prologue
    .line 1931109
    if-nez p0, :cond_1

    .line 1931110
    sget-object v0, LX/CkZ;->BEFORE:LX/CkZ;

    .line 1931111
    :cond_0
    :goto_0
    return-object v0

    .line 1931112
    :cond_1
    sget-object v0, LX/CkZ;->sBeforeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkZ;

    .line 1931113
    if-nez v0, :cond_0

    .line 1931114
    sget-object v0, LX/CkZ;->sAfterMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkZ;

    .line 1931115
    if-nez v0, :cond_0

    .line 1931116
    sget-object v0, LX/CkZ;->sEndMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkZ;

    .line 1931117
    if-nez v0, :cond_0

    .line 1931118
    const-string v0, "exact"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1931119
    sget-object v0, LX/CkZ;->EXACT:LX/CkZ;

    goto :goto_0

    .line 1931120
    :cond_2
    const-string v0, "max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1931121
    sget-object v0, LX/CkZ;->MAX:LX/CkZ;

    goto :goto_0

    .line 1931122
    :cond_3
    sget-object v0, LX/CkZ;->BEFORE:LX/CkZ;

    goto :goto_0
.end method

.method public static hasDimensionArgument(LX/CkZ;)Z
    .locals 1

    .prologue
    .line 1931123
    sget-object v0, LX/CkZ;->EXACT:LX/CkZ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/CkZ;->MAX:LX/CkZ;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasElementArgument(LX/CkZ;)Z
    .locals 1

    .prologue
    .line 1931124
    sget-object v0, LX/CkZ;->BEFORE:LX/CkZ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/CkZ;->AFTER:LX/CkZ;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CkZ;
    .locals 1

    .prologue
    .line 1931125
    const-class v0, LX/CkZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CkZ;

    return-object v0
.end method

.method public static values()[LX/CkZ;
    .locals 1

    .prologue
    .line 1931126
    sget-object v0, LX/CkZ;->$VALUES:[LX/CkZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CkZ;

    return-object v0
.end method
