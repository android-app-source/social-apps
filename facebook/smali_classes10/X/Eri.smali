.class public LX/Eri;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1R0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R0",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/FSR;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FSS;LX/FSF;LX/1R0;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2173343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173344
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173345
    iput-object v0, p0, LX/Eri;->a:LX/0Ot;

    .line 2173346
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173347
    iput-object v0, p0, LX/Eri;->b:LX/0Ot;

    .line 2173348
    iput-object p4, p0, LX/Eri;->c:LX/1R0;

    .line 2173349
    const-string v0, "native_newsfeed"

    invoke-virtual {p2, p1, v0, p3}, LX/FSS;->a(Landroid/content/Context;Ljava/lang/String;LX/1PT;)LX/FSR;

    move-result-object v0

    iput-object v0, p0, LX/Eri;->d:LX/FSR;

    .line 2173350
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)V
    .locals 3

    .prologue
    .line 2173351
    iget-object v1, p0, LX/Eri;->d:LX/FSR;

    .line 2173352
    iget-object v0, p0, LX/Eri;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wd;

    new-instance v2, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;

    invoke-direct {v2, p0, p1, v1}, Lcom/facebook/feed/offlinefeed/ImageFetcherHelper$1;-><init>(LX/Eri;Lcom/facebook/feed/model/ClientFeedUnitEdge;LX/1Pf;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2173353
    return-void
.end method
