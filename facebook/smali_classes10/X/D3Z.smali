.class public LX/D3Z;
.super Landroid/preference/Preference;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1Bj;

.field private final d:LX/03V;

.field public final e:Landroid/os/Handler;

.field public final f:Landroid/os/Handler;

.field public g:Landroid/webkit/CookieManager;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1960102
    const-class v0, LX/D3Z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D3Z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Bj;LX/03V;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959978
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1959979
    iput-object p1, p0, LX/D3Z;->b:Landroid/content/Context;

    .line 1959980
    iput-object p2, p0, LX/D3Z;->c:LX/1Bj;

    .line 1959981
    iput-object p3, p0, LX/D3Z;->d:LX/03V;

    .line 1959982
    iput-object p4, p0, LX/D3Z;->e:Landroid/os/Handler;

    .line 1959983
    iput-object p5, p0, LX/D3Z;->f:Landroid/os/Handler;

    .line 1959984
    const-string p1, "Start Browser Cookie Accessor Test"

    invoke-virtual {p0, p1}, LX/D3Z;->setTitle(Ljava/lang/CharSequence;)V

    .line 1959985
    const-string p1, "Verify our cookie access logic is correct. This test will clear your cookies!"

    invoke-virtual {p0, p1}, LX/D3Z;->setSummary(Ljava/lang/CharSequence;)V

    .line 1959986
    new-instance p1, LX/D3Y;

    invoke-direct {p1, p0}, LX/D3Y;-><init>(LX/D3Z;)V

    invoke-virtual {p0, p1}, LX/D3Z;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1959987
    return-void
.end method

.method public static a(LX/0QB;)LX/D3Z;
    .locals 7

    .prologue
    .line 1960099
    new-instance v1, LX/D3Z;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1Bj;->b(LX/0QB;)LX/1Bj;

    move-result-object v3

    check-cast v3, LX/1Bj;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-direct/range {v1 .. v6}, LX/D3Z;-><init>(Landroid/content/Context;LX/1Bj;LX/03V;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1960100
    move-object v0, v1

    .line 1960101
    return-object v0
.end method

.method public static a(LX/D3Z;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1960097
    iget-object v0, p0, LX/D3Z;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ui/browser/prefs/BrowserCookieTestPreference$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/ui/browser/prefs/BrowserCookieTestPreference$2;-><init>(LX/D3Z;Ljava/lang/String;)V

    const v2, -0x73b0a230

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1960098
    return-void
.end method

.method public static a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1960085
    iput-boolean v3, p0, LX/D3Z;->h:Z

    .line 1960086
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    .line 1960087
    const-string v1, "Test %s cookie mismatch! From CookieManager: %s, from CookieAccessor: %s"

    invoke-static {v1, p1, p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1960088
    const-string v2, "android_browser_cookie_test_failure"

    invoke-static {v2, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1960089
    iput v3, v2, LX/0VK;->e:I

    .line 1960090
    move-object v2, v2

    .line 1960091
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1960092
    move-object v2, v2

    .line 1960093
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    .line 1960094
    iget-object v3, p0, LX/D3Z;->d:LX/03V;

    invoke-virtual {v3, v2}, LX/03V;->a(LX/0VG;)V

    .line 1960095
    sget-object v2, LX/D3Z;->a:Ljava/lang/String;

    invoke-static {v2, v1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1960096
    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1960081
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1960082
    if-nez p0, :cond_0

    .line 1960083
    :goto_0
    return-object v0

    .line 1960084
    :cond_0
    const-string v1, ";\\s*"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static b$redex0(LX/D3Z;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960103
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    .line 1960104
    iget-object v0, p0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    .line 1960105
    iget-object v0, p0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flush()V

    .line 1960106
    return-void
.end method

.method public static d$redex0(LX/D3Z;)V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960067
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1960068
    const-wide/32 v2, 0x240c8400

    add-long v16, v14, v2

    .line 1960069
    new-instance v3, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "facebook.com"

    const-string v7, "name1"

    const-string v8, "value1"

    const-string v9, "/"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960070
    new-instance v5, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "google.com"

    const-string v9, "name2"

    const-string v10, "value2"

    const-string v11, "/"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960071
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960072
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960073
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v2}, Landroid/webkit/CookieManager;->flush()V

    .line 1960074
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v3, 0x1

    const-string v4, "http://www.facebook.com"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1960075
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const-string v4, "http://www.facebook.com"

    invoke-virtual {v3, v4}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1960076
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1960077
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/D3Z;->h:Z

    .line 1960078
    const-string v4, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960079
    const-string v2, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960080
    :cond_0
    return-void
.end method

.method public static e$redex0(LX/D3Z;)V
    .locals 20
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960050
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1960051
    const-wide/32 v2, 0x240c8400

    add-long v18, v16, v2

    .line 1960052
    new-instance v3, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "facebook.com"

    const-string v7, "name1"

    const-string v8, "value1"

    const-string v9, "/"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960053
    new-instance v5, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "facebook.com"

    const-string v9, "name2"

    const-string v10, "value2"

    const-string v11, "/"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960054
    new-instance v7, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v8

    const-string v10, "google.com"

    const-string v11, "name3"

    const-string v12, "value3"

    const-string v13, "/"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v14

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v7 .. v17}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960055
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960056
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960057
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v7}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960058
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v2}, Landroid/webkit/CookieManager;->flush()V

    .line 1960059
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v3, 0x1

    const-string v4, "http://www.facebook.com"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1960060
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const-string v4, "http://www.facebook.com"

    invoke-virtual {v3, v4}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1960061
    invoke-static {v2}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 1960062
    invoke-static {v3}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 1960063
    invoke-interface {v4, v5}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1960064
    const-string v4, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960065
    const-string v2, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960066
    :cond_0
    return-void
.end method

.method public static f$redex0(LX/D3Z;)V
    .locals 20
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1960034
    const-wide/32 v2, 0x240c8400

    add-long v18, v16, v2

    .line 1960035
    new-instance v3, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "facebook.com"

    const-string v7, "name1"

    const-string v8, "value1"

    const-string v9, "/path1"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960036
    new-instance v5, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "facebook.com"

    const-string v9, "name2"

    const-string v10, "value2"

    const-string v11, "/path1/subpath2"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960037
    new-instance v7, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v8

    const-string v10, "facebook.com"

    const-string v11, "name3"

    const-string v12, "value3"

    const-string v13, "/path1/subpath2/subsubpath3"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v14

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v7 .. v17}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960038
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960039
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960040
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v7}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960041
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v2}, Landroid/webkit/CookieManager;->flush()V

    .line 1960042
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v3, 0x1

    const-string v4, "http://www.facebook.com/path1/subpath2"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1960043
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const-string v4, "http://www.facebook.com/path1/subpath2"

    invoke-virtual {v3, v4}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1960044
    invoke-static {v2}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 1960045
    invoke-static {v3}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 1960046
    invoke-interface {v4, v5}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1960047
    const-string v4, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960048
    const-string v2, "getSimpleCookie"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960049
    :cond_0
    return-void
.end method

.method public static g$redex0(LX/D3Z;)V
    .locals 20
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960016
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1960017
    const-wide/32 v2, 0x240c8400

    add-long v18, v16, v2

    .line 1960018
    new-instance v3, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "facebook.com"

    const-string v7, "name1"

    const-string v8, "value1"

    const-string v9, "/path1"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960019
    new-instance v5, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "our.intern.facebook.com"

    const-string v9, "name2"

    const-string v10, "value2"

    const-string v11, "/path1"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960020
    new-instance v7, LX/0EC;

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v8

    const-string v10, "instagram.com"

    const-string v11, "name3"

    const-string v12, "value3"

    const-string v13, "/path1"

    invoke-static/range {v18 .. v19}, LX/0EC;->a(J)J

    move-result-wide v14

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v7 .. v17}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960021
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960022
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960023
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v7}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960024
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v2}, Landroid/webkit/CookieManager;->flush()V

    .line 1960025
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v3, 0x1

    const-string v4, "http://facebook.com/path1"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1960026
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const-string v4, "http://facebook.com/path1"

    invoke-virtual {v3, v4}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1960027
    invoke-static {v2}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 1960028
    invoke-static {v3}, LX/D3Z;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 1960029
    invoke-interface {v4, v5}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1960030
    const-string v4, "getSubDomain"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960031
    const-string v2, "getSubDomain"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960032
    :cond_0
    return-void
.end method

.method public static h$redex0(LX/D3Z;)V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1960002
    const-string v2, "http://www.facebook.com/path1/sub path2"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1960003
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1960004
    const-wide/32 v4, 0x240c8400

    add-long v16, v14, v4

    .line 1960005
    new-instance v3, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "www.facebook.com"

    const-string v7, "name"

    const-string v8, "value"

    const-string v9, "/path1/sub%20path2"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960006
    new-instance v5, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "www.facebook.com"

    const-string v9, "name"

    const-string v10, "value"

    const-string v11, "/path1/subpath2"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1960007
    move-object/from16 v0, p0

    iget-object v4, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960008
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1960009
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, Landroid/webkit/CookieManager;->flush()V

    .line 1960010
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1960011
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    const-string v4, "http://www.facebook.com/path1/sub path2"

    invoke-virtual {v3, v4}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1960012
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1960013
    const-string v4, "getEncoding"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960014
    const-string v2, "getEncoding"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960015
    :cond_0
    return-void
.end method

.method public static i$redex0(LX/D3Z;)V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1959988
    const-string v2, "http://www.facebook.com/path1/\u4e2d\u6587"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1959989
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1959990
    const-wide/32 v4, 0x240c8400

    add-long v16, v14, v4

    .line 1959991
    new-instance v3, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v4

    const-string v6, "www.facebook.com"

    const-string v7, "name"

    const-string v8, "value"

    const-string v9, "/path1/%E4%B8%AD%E6%96%87"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1959992
    new-instance v5, LX/0EC;

    invoke-static {v14, v15}, LX/0EC;->a(J)J

    move-result-wide v6

    const-string v8, "www.facebook.com"

    const-string v9, "name"

    const-string v10, "value"

    const-string v11, "/path1/subpath2"

    invoke-static/range {v16 .. v17}, LX/0EC;->a(J)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-direct/range {v5 .. v15}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1959993
    move-object/from16 v0, p0

    iget-object v4, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1959994
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v5}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1959995
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v3}, Landroid/webkit/CookieManager;->flush()V

    .line 1959996
    move-object/from16 v0, p0

    iget-object v3, v0, LX/D3Z;->c:LX/1Bj;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 1959997
    move-object/from16 v0, p0

    iget-object v4, v0, LX/D3Z;->g:Landroid/webkit/CookieManager;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1959998
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1959999
    const-string v4, "pathNonAscii"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v2, v3}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960000
    const-string v2, "pathNonAscii"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/D3Z;->a(LX/D3Z;Ljava/lang/String;)V

    .line 1960001
    :cond_0
    return-void
.end method
