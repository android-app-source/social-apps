.class public final LX/EMa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/1Pn;LX/CzL;LX/CzL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2110783
    iput-object p1, p0, LX/EMa;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iput-object p2, p0, LX/EMa;->a:LX/1Pn;

    iput-object p3, p0, LX/EMa;->b:LX/CzL;

    iput-object p4, p0, LX/EMa;->c:LX/CzL;

    iput-object p5, p0, LX/EMa;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2110784
    iget-object v0, p0, LX/EMa;->a:LX/1Pn;

    check-cast v0, LX/CxA;

    iget-object v1, p0, LX/EMa;->b:LX/CzL;

    invoke-interface {v0, v1}, LX/CxA;->a(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110785
    iget-object v0, p0, LX/EMa;->a:LX/1Pn;

    check-cast v0, LX/CxA;

    iget-object v1, p0, LX/EMa;->b:LX/CzL;

    iget-object v2, p0, LX/EMa;->c:LX/CzL;

    invoke-interface {v0, v1, v2}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110786
    iget-object v0, p0, LX/EMa;->a:LX/1Pn;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2110787
    :cond_0
    iget-object v0, p0, LX/EMa;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "warn_override_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EMa;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EMY;

    invoke-direct {v2, p0}, LX/EMY;-><init>(LX/EMa;)V

    new-instance v3, LX/EMZ;

    invoke-direct {v3, p0}, LX/EMZ;-><init>(LX/EMa;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2110788
    return-void
.end method
