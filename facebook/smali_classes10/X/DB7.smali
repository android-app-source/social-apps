.class public final LX/DB7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DB8;

.field public final synthetic b:LX/DB9;


# direct methods
.method public constructor <init>(LX/DB9;LX/DB8;)V
    .locals 0

    .prologue
    .line 1972273
    iput-object p1, p0, LX/DB7;->b:LX/DB9;

    iput-object p2, p0, LX/DB7;->a:LX/DB8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1972274
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->c:LX/4BY;

    if-eqz v0, :cond_0

    .line 1972275
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->c:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1972276
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    const/4 v1, 0x0

    .line 1972277
    iput-object v1, v0, LX/DB9;->c:LX/4BY;

    .line 1972278
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1972279
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->h:LX/Bl6;

    new-instance v1, LX/BlC;

    iget-object v2, p0, LX/DB7;->b:LX/DB9;

    iget-object v2, v2, LX/DB9;->a:Lcom/facebook/events/model/Event;

    .line 1972280
    iget-object p1, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1972281
    invoke-direct {v1, v2}, LX/BlC;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1972282
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->i:LX/0TD;

    new-instance v1, Lcom/facebook/events/widget/eventactionitems/ActionItemDelete$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/events/widget/eventactionitems/ActionItemDelete$2$1;-><init>(LX/DB7;)V

    const v2, -0xb13619f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1972283
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->c:LX/4BY;

    if-eqz v0, :cond_0

    .line 1972284
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    iget-object v0, v0, LX/DB9;->c:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1972285
    iget-object v0, p0, LX/DB7;->b:LX/DB9;

    const/4 v1, 0x0

    .line 1972286
    iput-object v1, v0, LX/DB9;->c:LX/4BY;

    .line 1972287
    :cond_0
    return-void
.end method
