.class public final LX/Et7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLImageOverlay;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2177241
    iput-object p1, p0, LX/Et7;->d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    iput-object p2, p0, LX/Et7;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Et7;->b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object p4, p0, LX/Et7;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x4b4d9f97

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2177242
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Et7;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Et7;->b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-static {v3}, LX/B5O;->a(Lcom/facebook/graphql/model/GraphQLImageOverlay;)LX/5QV;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->a(LX/5QV;)Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)Landroid/content/Intent;

    move-result-object v1

    .line 2177243
    iget-object v2, p0, LX/Et7;->d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->d:LX/1Cn;

    iget-object v3, p0, LX/Et7;->c:Ljava/lang/String;

    .line 2177244
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/3lw;->GOODWILL_THROWBACK_ADD_PROFILE_FRAME_CLICK:LX/3lw;

    iget-object v6, v6, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "goodwill"

    .line 2177245
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2177246
    move-object v5, v5

    .line 2177247
    const-string v6, "campaign_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2177248
    iget-object v6, v2, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2177249
    iget-object v2, p0, LX/Et7;->d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2177250
    const v1, 0x395582b9

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
