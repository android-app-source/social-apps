.class public LX/Chv;
.super LX/0b4;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/ChM;",
        "LX/Chu;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1928680
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1928681
    return-void
.end method

.method public static a(LX/0QB;)LX/Chv;
    .locals 3

    .prologue
    .line 1928682
    const-class v1, LX/Chv;

    monitor-enter v1

    .line 1928683
    :try_start_0
    sget-object v0, LX/Chv;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1928684
    sput-object v2, LX/Chv;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1928685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1928686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1928687
    new-instance v0, LX/Chv;

    invoke-direct {v0}, LX/Chv;-><init>()V

    .line 1928688
    move-object v0, v0

    .line 1928689
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1928690
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Chv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928691
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1928692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
