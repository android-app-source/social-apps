.class public final LX/EbQ;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EbO;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbQ;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EbQ;


# instance fields
.field public bitField0_:I

.field public ciphertext_:LX/EWc;

.field public id_:I

.field public iteration_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2143807
    new-instance v0, LX/EbN;

    invoke-direct {v0}, LX/EbN;-><init>()V

    sput-object v0, LX/EbQ;->a:LX/EWZ;

    .line 2143808
    new-instance v0, LX/EbQ;

    invoke-direct {v0}, LX/EbQ;-><init>()V

    .line 2143809
    sput-object v0, LX/EbQ;->c:LX/EbQ;

    invoke-direct {v0}, LX/EbQ;->w()V

    .line 2143810
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2143765
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143766
    iput-byte v0, p0, LX/EbQ;->memoizedIsInitialized:B

    .line 2143767
    iput v0, p0, LX/EbQ;->memoizedSerializedSize:I

    .line 2143768
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2143769
    iput-object v0, p0, LX/EbQ;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2143770
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143771
    iput-byte v0, p0, LX/EbQ;->memoizedIsInitialized:B

    .line 2143772
    iput v0, p0, LX/EbQ;->memoizedSerializedSize:I

    .line 2143773
    invoke-direct {p0}, LX/EbQ;->w()V

    .line 2143774
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2143775
    const/4 v0, 0x0

    .line 2143776
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2143777
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2143778
    sparse-switch v3, :sswitch_data_0

    .line 2143779
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2143780
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2143781
    goto :goto_0

    .line 2143782
    :sswitch_1
    iget v3, p0, LX/EbQ;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EbQ;->bitField0_:I

    .line 2143783
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbQ;->id_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2143784
    :catch_0
    move-exception v0

    .line 2143785
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143786
    move-object v0, v0

    .line 2143787
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2143788
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EbQ;->unknownFields:LX/EZQ;

    .line 2143789
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2143790
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EbQ;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EbQ;->bitField0_:I

    .line 2143791
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbQ;->iteration_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2143792
    :catch_1
    move-exception v0

    .line 2143793
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2143794
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143795
    move-object v0, v1

    .line 2143796
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2143797
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/EbQ;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/EbQ;->bitField0_:I

    .line 2143798
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbQ;->ciphertext_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2143799
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbQ;->unknownFields:LX/EZQ;

    .line 2143800
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2143801
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2143802
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2143803
    iput-byte v1, p0, LX/EbQ;->memoizedIsInitialized:B

    .line 2143804
    iput v1, p0, LX/EbQ;->memoizedSerializedSize:I

    .line 2143805
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbQ;->unknownFields:LX/EZQ;

    .line 2143806
    return-void
.end method

.method private static a(LX/EbQ;)LX/EbP;
    .locals 1

    .prologue
    .line 2143826
    invoke-static {}, LX/EbP;->u()LX/EbP;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EbP;->a(LX/EbQ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2143811
    iput v0, p0, LX/EbQ;->id_:I

    .line 2143812
    iput v0, p0, LX/EbQ;->iteration_:I

    .line 2143813
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbQ;->ciphertext_:LX/EWc;

    .line 2143814
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2143815
    new-instance v0, LX/EbP;

    invoke-direct {v0, p1}, LX/EbP;-><init>(LX/EYd;)V

    .line 2143816
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2143817
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2143818
    iget v0, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2143819
    iget v0, p0, LX/EbQ;->id_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2143820
    :cond_0
    iget v0, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2143821
    iget v0, p0, LX/EbQ;->iteration_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->c(II)V

    .line 2143822
    :cond_1
    iget v0, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2143823
    const/4 v0, 0x3

    iget-object v1, p0, LX/EbQ;->ciphertext_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2143824
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2143825
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2143748
    iget-byte v1, p0, LX/EbQ;->memoizedIsInitialized:B

    .line 2143749
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2143750
    :goto_0
    return v0

    .line 2143751
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2143752
    :cond_1
    iput-byte v0, p0, LX/EbQ;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2143753
    iget v0, p0, LX/EbQ;->memoizedSerializedSize:I

    .line 2143754
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2143755
    :goto_0
    return v0

    .line 2143756
    :cond_0
    const/4 v0, 0x0

    .line 2143757
    iget v1, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2143758
    iget v0, p0, LX/EbQ;->id_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2143759
    :cond_1
    iget v1, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2143760
    iget v1, p0, LX/EbQ;->iteration_:I

    invoke-static {v3, v1}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143761
    :cond_2
    iget v1, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2143762
    const/4 v1, 0x3

    iget-object v2, p0, LX/EbQ;->ciphertext_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143763
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2143764
    iput v0, p0, LX/EbQ;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2143738
    iget-object v0, p0, LX/EbQ;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2143739
    sget-object v0, LX/EbV;->h:LX/EYn;

    const-class v1, LX/EbQ;

    const-class v2, LX/EbP;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2143740
    sget-object v0, LX/EbQ;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2143741
    iget v1, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2143742
    iget v0, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2143743
    iget v0, p0, LX/EbQ;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2143747
    invoke-static {p0}, LX/EbQ;->a(LX/EbQ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2143744
    invoke-static {}, LX/EbP;->u()LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2143745
    invoke-static {p0}, LX/EbQ;->a(LX/EbQ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143746
    sget-object v0, LX/EbQ;->c:LX/EbQ;

    return-object v0
.end method
