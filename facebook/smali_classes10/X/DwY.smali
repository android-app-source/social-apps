.class public final LX/DwY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Dwa;


# direct methods
.method public constructor <init>(LX/Dwa;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2060586
    iput-object p1, p0, LX/DwY;->b:LX/Dwa;

    iput-object p2, p0, LX/DwY;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2060587
    iget-object v0, p0, LX/DwY;->b:LX/Dwa;

    iget-object v0, v0, LX/Dwa;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dy7;

    iget-object v1, p0, LX/DwY;->a:Ljava/lang/String;

    const/4 v2, 0x0

    .line 2060588
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2060589
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "album Node ID cannot be null."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Exception;)LX/52H;

    move-result-object v3

    .line 2060590
    :goto_0
    move-object v0, v3

    .line 2060591
    return-object v0

    .line 2060592
    :cond_0
    new-instance v3, LX/5ga;

    invoke-direct {v3}, LX/5ga;-><init>()V

    move-object v4, v3

    .line 2060593
    const-string v3, "node_id"

    invoke-virtual {v4, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string p0, "media_type"

    iget-object v3, v0, LX/Dy7;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-virtual {v3}, LX/0rq;->a()LX/0wF;

    move-result-object v3

    invoke-virtual {v5, p0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v5, "contributor_pic_width"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060594
    iget-object v3, v0, LX/Dy7;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    goto :goto_0
.end method
