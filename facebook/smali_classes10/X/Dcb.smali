.class public final LX/Dcb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2018592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018593
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2018594
    if-eqz p1, :cond_0

    .line 2018595
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018596
    if-nez v0, :cond_1

    .line 2018597
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2018598
    :goto_0
    return-object v0

    .line 2018599
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018600
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;

    .line 2018601
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2018602
    :cond_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2018603
    :goto_1
    move-object v1, v1

    .line 2018604
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018605
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;

    .line 2018606
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2018607
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-nez v3, :cond_6

    .line 2018608
    :cond_3
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2018609
    :goto_2
    move-object v0, v2

    .line 2018610
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2018611
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2018612
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    .line 2018613
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2018614
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2018615
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_1

    .line 2018616
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$PhotosByCategoryQueryModel$PhotosByCategoryModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    .line 2018617
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v4

    .line 2018618
    iput-object v4, v2, LX/17L;->f:Ljava/lang/String;

    .line 2018619
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2018620
    iput-object v4, v2, LX/17L;->c:Ljava/lang/String;

    .line 2018621
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v4

    .line 2018622
    iput-boolean v4, v2, LX/17L;->d:Z

    .line 2018623
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2018624
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2018625
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto :goto_2
.end method
