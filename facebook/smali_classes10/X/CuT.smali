.class public final enum LX/CuT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CuT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CuT;

.field public static final enum SCROLLING_SLIDESHOW:LX/CuT;

.field public static final enum WAITING_FOR_DOWN:LX/CuT;

.field public static final enum WAITING_TO_STEAL_GESTURE:LX/CuT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1946696
    new-instance v0, LX/CuT;

    const-string v1, "WAITING_FOR_DOWN"

    invoke-direct {v0, v1, v2}, LX/CuT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuT;->WAITING_FOR_DOWN:LX/CuT;

    .line 1946697
    new-instance v0, LX/CuT;

    const-string v1, "WAITING_TO_STEAL_GESTURE"

    invoke-direct {v0, v1, v3}, LX/CuT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuT;->WAITING_TO_STEAL_GESTURE:LX/CuT;

    .line 1946698
    new-instance v0, LX/CuT;

    const-string v1, "SCROLLING_SLIDESHOW"

    invoke-direct {v0, v1, v4}, LX/CuT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuT;->SCROLLING_SLIDESHOW:LX/CuT;

    .line 1946699
    const/4 v0, 0x3

    new-array v0, v0, [LX/CuT;

    sget-object v1, LX/CuT;->WAITING_FOR_DOWN:LX/CuT;

    aput-object v1, v0, v2

    sget-object v1, LX/CuT;->WAITING_TO_STEAL_GESTURE:LX/CuT;

    aput-object v1, v0, v3

    sget-object v1, LX/CuT;->SCROLLING_SLIDESHOW:LX/CuT;

    aput-object v1, v0, v4

    sput-object v0, LX/CuT;->$VALUES:[LX/CuT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1946695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CuT;
    .locals 1

    .prologue
    .line 1946694
    const-class v0, LX/CuT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CuT;

    return-object v0
.end method

.method public static values()[LX/CuT;
    .locals 1

    .prologue
    .line 1946693
    sget-object v0, LX/CuT;->$VALUES:[LX/CuT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CuT;

    return-object v0
.end method
