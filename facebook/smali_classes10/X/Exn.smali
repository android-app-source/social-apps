.class public final LX/Exn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/83X;

.field public final synthetic b:LX/EyQ;

.field public final synthetic c:Z

.field public final synthetic d:LX/Exs;


# direct methods
.method public constructor <init>(LX/Exs;LX/83X;LX/EyQ;Z)V
    .locals 0

    .prologue
    .line 2185317
    iput-object p1, p0, LX/Exn;->d:LX/Exs;

    iput-object p2, p0, LX/Exn;->a:LX/83X;

    iput-object p3, p0, LX/Exn;->b:LX/EyQ;

    iput-boolean p4, p0, LX/Exn;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0xa97613d

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2185318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v1, p0, LX/Exn;->a:LX/83X;

    invoke-interface {v1}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185319
    iget-object v0, p0, LX/Exn;->d:LX/Exs;

    iget-object v0, v0, LX/Exs;->c:Landroid/content/Context;

    iget-object v1, p0, LX/Exn;->d:LX/Exs;

    iget-object v1, v1, LX/Exs;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    iget-object v2, p0, LX/Exn;->a:LX/83X;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, LX/Exn;->a:LX/83X;

    invoke-interface {v2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Exm;

    invoke-direct {v5, p0}, LX/Exm;-><init>(LX/Exn;)V

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/Ey3;->a(Landroid/content/Context;LX/17W;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;LX/Eu0;)V

    .line 2185320
    const v0, 0x17161a69

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2185321
    :goto_0
    return-void

    .line 2185322
    :cond_0
    iget-object v0, p0, LX/Exn;->d:LX/Exs;

    iget-object v1, p0, LX/Exn;->b:LX/EyQ;

    iget-object v2, p0, LX/Exn;->a:LX/83X;

    iget-boolean v3, p0, LX/Exn;->c:Z

    invoke-static {v0, v1, v2, v3}, LX/Exs;->e(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185323
    const v0, 0x34db159f

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
