.class public final LX/Dyj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064823
    iput-object p1, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x384e9979

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2064824
    iget-object v1, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v1, v1, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064825
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    move-object v1, v2

    .line 2064826
    if-eqz v1, :cond_0

    .line 2064827
    iget-object v1, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v1, v1, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v2, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v2, v2, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064828
    iget-object p0, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    move-object v2, p0

    .line 2064829
    invoke-virtual {v1, v2}, LX/Dyt;->b(Ljava/lang/String;)V

    .line 2064830
    const v1, 0x27e42b48

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2064831
    :goto_0
    return-void

    .line 2064832
    :cond_0
    iget-object v1, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v1, v1, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v2, p0, LX/Dyj;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v2, v2, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064833
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v2, v3

    .line 2064834
    invoke-virtual {v1, v2}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2064835
    const v1, -0x54a33f8f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
