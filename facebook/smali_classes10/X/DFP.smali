.class public final LX/DFP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2ep;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Fa;

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978131
    const-string v0, "PersonYouMayKnowBlacklistComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978111
    if-ne p0, p1, :cond_1

    .line 1978112
    :cond_0
    :goto_0
    return v0

    .line 1978113
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978114
    goto :goto_0

    .line 1978115
    :cond_3
    check-cast p1, LX/DFP;

    .line 1978116
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978117
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978118
    if-eq v2, v3, :cond_0

    .line 1978119
    iget-object v2, p0, LX/DFP;->a:LX/2ep;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFP;->a:LX/2ep;

    iget-object v3, p1, LX/DFP;->a:LX/2ep;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978120
    goto :goto_0

    .line 1978121
    :cond_5
    iget-object v2, p1, LX/DFP;->a:LX/2ep;

    if-nez v2, :cond_4

    .line 1978122
    :cond_6
    iget-object v2, p0, LX/DFP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DFP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DFP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1978123
    goto :goto_0

    .line 1978124
    :cond_8
    iget-object v2, p1, LX/DFP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1978125
    :cond_9
    iget-object v2, p0, LX/DFP;->c:LX/1Fa;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DFP;->c:LX/1Fa;

    iget-object v3, p1, LX/DFP;->c:LX/1Fa;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1978126
    goto :goto_0

    .line 1978127
    :cond_b
    iget-object v2, p1, LX/DFP;->c:LX/1Fa;

    if-nez v2, :cond_a

    .line 1978128
    :cond_c
    iget-object v2, p0, LX/DFP;->d:LX/1Pq;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/DFP;->d:LX/1Pq;

    iget-object v3, p1, LX/DFP;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978129
    goto :goto_0

    .line 1978130
    :cond_d
    iget-object v2, p1, LX/DFP;->d:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
