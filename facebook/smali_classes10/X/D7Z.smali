.class public LX/D7Z;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

.field public b:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

.field public c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/15X;LX/2ft;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1967354
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1967355
    const v0, 0x7f03043d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1967356
    const v0, 0x7f0d0cd8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, LX/D7Z;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 1967357
    const v0, 0x7f0d0cd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    iput-object v0, p0, LX/D7Z;->a:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    .line 1967358
    iget-object v0, p0, LX/D7Z;->a:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    invoke-virtual {p0}, LX/D7Z;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/15X;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setTitle(Ljava/lang/CharSequence;)V

    .line 1967359
    iget-object v0, p0, LX/D7Z;->a:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    invoke-virtual {v0, p4}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setDescription(Ljava/lang/CharSequence;)V

    .line 1967360
    const v0, 0x7f0d0cda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    iput-object v0, p0, LX/D7Z;->b:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    .line 1967361
    iget-object v0, p0, LX/D7Z;->b:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    invoke-virtual {p0}, LX/D7Z;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/15X;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setTitle(Ljava/lang/CharSequence;)V

    .line 1967362
    iget-object v0, p0, LX/D7Z;->b:Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;

    invoke-virtual {p0}, LX/D7Z;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/15X;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/video/downloadmanager/view/DownloadRadioButton;->setDescription(Ljava/lang/CharSequence;)V

    .line 1967363
    iget-object p1, p0, LX/D7Z;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    sget-object v0, LX/2ft;->NONE:LX/2ft;

    if-ne p3, v0, :cond_0

    const v0, 0x7f0d0cd9

    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 1967364
    return-void

    .line 1967365
    :cond_0
    const v0, 0x7f0d0cda

    goto :goto_0
.end method
