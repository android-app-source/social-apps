.class public LX/DZA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2013180
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "groups/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2013181
    sput-object v0, LX/DZA;->a:LX/0Tn;

    const-string v1, "nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2013182
    sput-object v0, LX/DZA;->b:LX/0Tn;

    const-string v1, "mall_visits_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->c:LX/0Tn;

    .line 2013183
    sget-object v0, LX/DZA;->b:LX/0Tn;

    const-string v1, "mall_friends_nag_seen/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->d:LX/0Tn;

    .line 2013184
    sget-object v0, LX/DZA;->b:LX/0Tn;

    const-string v1, "friends_nag_impressions/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->e:LX/0Tn;

    .line 2013185
    sget-object v0, LX/DZA;->b:LX/0Tn;

    const-string v1, "custom_invite_shown/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->f:LX/0Tn;

    .line 2013186
    sget-object v0, LX/DZA;->b:LX/0Tn;

    const-string v1, "share_link_shown/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->g:LX/0Tn;

    .line 2013187
    sget-object v0, LX/DZA;->a:LX/0Tn;

    const-string v1, "groups_sort_filter/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DZA;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2013189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2013190
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2013188
    sget-object v0, LX/DZA;->c:LX/0Tn;

    sget-object v1, LX/DZA;->d:LX/0Tn;

    sget-object v2, LX/DZA;->e:LX/0Tn;

    sget-object v3, LX/DZA;->h:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
