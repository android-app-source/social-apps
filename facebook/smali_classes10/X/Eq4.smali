.class public final LX/Eq4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/events/invite/EventInviteeToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;Z)V
    .locals 0

    .prologue
    .line 2170947
    iput-object p1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iput-boolean p2, p0, LX/Eq4;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2170948
    iget-boolean v0, p0, LX/Eq4;->a:Z

    if-nez v0, :cond_2

    .line 2170949
    iget-object v0, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->F:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v1}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->O(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)LX/0Px;

    move-result-object v1

    .line 2170950
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 2170951
    move-object v0, v0

    .line 2170952
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 2170953
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 2170954
    move-object v0, v0

    .line 2170955
    :goto_0
    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->w:LX/2RC;

    sget-object v2, LX/2RV;->CONTACT:LX/2RV;

    invoke-virtual {v1, v0, v2}, LX/2RC;->a(LX/2RR;LX/2RV;)Landroid/database/Cursor;

    move-result-object v0

    .line 2170956
    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->y:LX/Era;

    invoke-virtual {v1, v0}, LX/Era;->a(Landroid/database/Cursor;)LX/ErZ;

    move-result-object v2

    .line 2170957
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    .line 2170958
    :cond_0
    :goto_1
    :try_start_0
    invoke-virtual {v2}, LX/2TZ;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2170959
    invoke-virtual {v2}, LX/ErZ;->b()Landroid/util/Pair;

    move-result-object v1

    .line 2170960
    if-eqz v1, :cond_0

    .line 2170961
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2170962
    new-instance v4, Lcom/facebook/events/invite/EventInviteeToken;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v4, v0, v1}, Lcom/facebook/events/invite/EventInviteeToken;-><init>(Lcom/facebook/contacts/graphql/Contact;Ljava/lang/String;)V

    .line 2170963
    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v1, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2170964
    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170965
    invoke-virtual {v1, v4}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(LX/8QL;)Z

    move-result v5

    move v1, v5

    .line 2170966
    if-nez v1, :cond_1

    .line 2170967
    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v5, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v5, v5, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2170968
    invoke-virtual {v1, v4, v5}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 2170969
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2170970
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/2TZ;->close()V

    throw v0

    .line 2170971
    :cond_2
    iget-object v0, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->F:LX/2RQ;

    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/Eq4;->b:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v1}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->O(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)LX/0Px;

    move-result-object v1

    .line 2170972
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 2170973
    move-object v0, v0

    .line 2170974
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 2170975
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 2170976
    move-object v0, v0

    .line 2170977
    goto :goto_0

    .line 2170978
    :cond_3
    invoke-virtual {v2}, LX/2TZ;->close()V

    .line 2170979
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
