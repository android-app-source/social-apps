.class public LX/Et6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Et6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2177152
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2177153
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Et6;->b:LX/0Zi;

    .line 2177154
    iput-object p1, p0, LX/Et6;->a:LX/0Ot;

    .line 2177155
    return-void
.end method

.method public static a(LX/0QB;)LX/Et6;
    .locals 4

    .prologue
    .line 2177156
    const-class v1, LX/Et6;

    monitor-enter v1

    .line 2177157
    :try_start_0
    sget-object v0, LX/Et6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2177158
    sput-object v2, LX/Et6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2177159
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177160
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2177161
    new-instance v3, LX/Et6;

    const/16 p0, 0x1f39

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Et6;-><init>(LX/0Ot;)V

    .line 2177162
    move-object v0, v3

    .line 2177163
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2177164
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Et6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177165
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2177166
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2177135
    check-cast p2, LX/Et5;

    .line 2177136
    iget-object v0, p0, LX/Et6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;

    iget-object v1, p2, LX/Et5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 2177137
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2177138
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2177139
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v4

    .line 2177140
    invoke-static {v2}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 2177141
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v2, ""

    move-object v3, v2

    .line 2177142
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {v0, p1, v2}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLImage;)LX/1Dg;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x0

    const/4 v1, 0x3

    const/4 p0, 0x2

    .line 2177143
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v6, v8}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a00e1

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v9, 0x7f0b1d92

    invoke-interface {v8, v9}, LX/1Di;->i(I)LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    const/16 v10, 0xc

    invoke-interface {v8, v9, v10}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x6

    invoke-interface {v8, v1, v9}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2177144
    if-eqz v5, :cond_0

    .line 2177145
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    const v10, 0x7f0b0050

    invoke-virtual {v9, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    const v10, 0x7f0a00e6

    invoke-virtual {v9, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v9

    const v10, 0x3f666666    # 0.9f

    invoke-virtual {v9, v10}, LX/1ne;->j(F)LX/1ne;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->e:LX/1vg;

    invoke-virtual {v10, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v10

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00e6

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v10, p0}, LX/2xv;->i(I)LX/2xv;

    move-result-object v10

    const p0, 0x7f0207eb

    invoke-virtual {v10, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v10

    invoke-virtual {v10}, LX/1n6;->b()LX/1dc;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x4

    invoke-interface {v8, v1, v9}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v8

    .line 2177146
    const v9, -0x7c48b037

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2177147
    invoke-interface {v8, v9}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2177148
    :cond_0
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v3, v6

    .line 2177149
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {v0, p1, v2}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLImage;)LX/1Dg;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2177150
    return-object v0

    .line 2177151
    :cond_1
    iget-object v3, v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->c:LX/1Uf;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v7, v6}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2177121
    invoke-static {}, LX/1dS;->b()V

    .line 2177122
    iget v0, p1, LX/1dQ;->b:I

    .line 2177123
    packed-switch v0, :pswitch_data_0

    .line 2177124
    :goto_0
    return-object v2

    .line 2177125
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2177126
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2177127
    check-cast v1, LX/Et5;

    .line 2177128
    iget-object v3, p0, LX/Et6;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;

    iget-object p1, v1, LX/Et5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177129
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 2177130
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p2}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 2177131
    if-eqz p0, :cond_0

    const/4 p2, 0x1

    :goto_1
    const-string v1, "Subtitle should not be visible and clickable when actionLink is invalid."

    invoke-static {p2, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2177132
    iget-object p2, v3, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->d:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, v1, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2177133
    goto :goto_0

    .line 2177134
    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x7c48b037
        :pswitch_0
    .end packed-switch
.end method
