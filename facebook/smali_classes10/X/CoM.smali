.class public LX/CoM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1Bv;

.field private final b:LX/0oz;


# direct methods
.method public constructor <init>(LX/1Bv;LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1935564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1935565
    iput-object p1, p0, LX/CoM;->a:LX/1Bv;

    .line 1935566
    iput-object p2, p0, LX/CoM;->b:LX/0oz;

    .line 1935567
    return-void
.end method

.method public static a(LX/0QB;)LX/CoM;
    .locals 5

    .prologue
    .line 1935568
    const-class v1, LX/CoM;

    monitor-enter v1

    .line 1935569
    :try_start_0
    sget-object v0, LX/CoM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1935570
    sput-object v2, LX/CoM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1935571
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935572
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1935573
    new-instance p0, LX/CoM;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v3

    check-cast v3, LX/1Bv;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-direct {p0, v3, v4}, LX/CoM;-><init>(LX/1Bv;LX/0oz;)V

    .line 1935574
    move-object v0, p0

    .line 1935575
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1935576
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CoM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1935577
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1935578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1935579
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1935580
    iget-object v2, p0, LX/CoM;->a:LX/1Bv;

    invoke-virtual {v2, v1}, LX/1Bv;->a(Ljava/util/Set;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1935581
    sget-object v2, LX/046;->DISABLED_BY_ALREADY_SEEN:LX/046;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1935582
    sget-object v2, LX/046;->DISABLED_BY_UNKNOWN_AUTOPLAY_SETTINGS:LX/046;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1935583
    sget-object v2, LX/046;->DISABLED_BY_AUTOPLAY_SETTING:LX/046;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1935584
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1935585
    :cond_0
    :goto_0
    return v0

    .line 1935586
    :cond_1
    iget-object v1, p0, LX/CoM;->b:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    sget-object v2, LX/0p3;->EXCELLENT:LX/0p3;

    if-ne v1, v2, :cond_0

    .line 1935587
    const/4 v0, 0x1

    goto :goto_0
.end method
