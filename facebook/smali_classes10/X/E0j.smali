.class public LX/E0j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/places/create/home/HomeUpdateParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2068922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2068923
    check-cast p1, Lcom/facebook/places/create/home/HomeUpdateParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2068924
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2068925
    iget-wide v4, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068926
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068927
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "name"

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068928
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    .line 2068929
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "privacy"

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068930
    :cond_0
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2068931
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "neighborhood_name"

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->d:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068932
    :cond_1
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2068933
    :cond_3
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->c()LX/0m9;

    move-result-object v0

    .line 2068934
    const-string v4, "city"

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2068935
    const-string v4, "street"

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2068936
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "address"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068937
    :cond_4
    iget-wide v4, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->g:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    .line 2068938
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "city_id"

    iget-wide v6, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->g:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068939
    :cond_5
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "format"

    const-string v5, "json"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068940
    const/4 v0, 0x0

    .line 2068941
    iget-object v4, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v4, :cond_8

    .line 2068942
    new-instance v0, Ljava/io/File;

    iget-object v4, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2068943
    new-instance v4, LX/4ct;

    iget-object v5, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v0, v5, v6}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 2068944
    new-instance v0, LX/4cQ;

    const-string v5, "file"

    invoke-direct {v0, v5, v4}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2068945
    :goto_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v4

    const-string v5, "home_update"

    .line 2068946
    iput-object v5, v4, LX/14O;->b:Ljava/lang/String;

    .line 2068947
    move-object v4, v4

    .line 2068948
    const-string v5, "POST"

    .line 2068949
    iput-object v5, v4, LX/14O;->c:Ljava/lang/String;

    .line 2068950
    move-object v4, v4

    .line 2068951
    iget-wide v6, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 2068952
    iput-object v5, v4, LX/14O;->d:Ljava/lang/String;

    .line 2068953
    move-object v4, v4

    .line 2068954
    iput-object v3, v4, LX/14O;->g:Ljava/util/List;

    .line 2068955
    move-object v3, v4

    .line 2068956
    sget-object v4, LX/14S;->JSON:LX/14S;

    .line 2068957
    iput-object v4, v3, LX/14O;->k:LX/14S;

    .line 2068958
    move-object v3, v3

    .line 2068959
    if-eqz v0, :cond_6

    .line 2068960
    new-array v1, v1, [LX/4cQ;

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2068961
    iput-object v0, v3, LX/14O;->l:Ljava/util/List;

    .line 2068962
    :cond_6
    invoke-virtual {v3}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v2

    .line 2068963
    goto/16 :goto_0

    .line 2068964
    :cond_8
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "remove_photo"

    iget-boolean v6, p1, Lcom/facebook/places/create/home/HomeUpdateParams;->i:Z

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2068965
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    .line 2068966
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
