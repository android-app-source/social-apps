.class public final LX/EWv;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EWt;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWv;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EWv;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/lang/Object;

.field public options_:LX/EX2;

.field private final unknownFields:LX/EZQ;

.field public value_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EX6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2131939
    new-instance v0, LX/EWs;

    invoke-direct {v0}, LX/EWs;-><init>()V

    sput-object v0, LX/EWv;->a:LX/EWZ;

    .line 2131940
    new-instance v0, LX/EWv;

    invoke-direct {v0}, LX/EWv;-><init>()V

    .line 2131941
    sput-object v0, LX/EWv;->c:LX/EWv;

    invoke-direct {v0}, LX/EWv;->q()V

    .line 2131942
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2131943
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131944
    iput-byte v0, p0, LX/EWv;->memoizedIsInitialized:B

    .line 2131945
    iput v0, p0, LX/EWv;->memoizedSerializedSize:I

    .line 2131946
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2131947
    iput-object v0, p0, LX/EWv;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x2

    .line 2131948
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131949
    iput-byte v1, p0, LX/EWv;->memoizedIsInitialized:B

    .line 2131950
    iput v1, p0, LX/EWv;->memoizedSerializedSize:I

    .line 2131951
    invoke-direct {p0}, LX/EWv;->q()V

    .line 2131952
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 2131953
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 2131954
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2131955
    sparse-switch v0, :sswitch_data_0

    .line 2131956
    invoke-virtual {p0, p1, v5, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 2131957
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2131958
    goto :goto_0

    .line 2131959
    :sswitch_1
    iget v0, p0, LX/EWv;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EWv;->bitField0_:I

    .line 2131960
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2131961
    :catch_0
    move-exception v0

    .line 2131962
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131963
    move-object v0, v0

    .line 2131964
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2131965
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 2131966
    iget-object v1, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWv;->value_:Ljava/util/List;

    .line 2131967
    :cond_1
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EWv;->unknownFields:LX/EZQ;

    .line 2131968
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2131969
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v6, :cond_2

    .line 2131970
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    .line 2131971
    or-int/lit8 v1, v1, 0x2

    .line 2131972
    :cond_2
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    sget-object v2, LX/EX6;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2131973
    :catch_1
    move-exception v0

    .line 2131974
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2131975
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131976
    move-object v0, v2

    .line 2131977
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2131978
    :sswitch_3
    const/4 v0, 0x0

    .line 2131979
    :try_start_4
    iget v2, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_6

    .line 2131980
    iget-object v0, p0, LX/EWv;->options_:LX/EX2;

    invoke-virtual {v0}, LX/EX2;->l()LX/EX0;

    move-result-object v0

    move-object v2, v0

    .line 2131981
    :goto_1
    sget-object v0, LX/EX2;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EX2;

    iput-object v0, p0, LX/EWv;->options_:LX/EX2;

    .line 2131982
    if-eqz v2, :cond_3

    .line 2131983
    iget-object v0, p0, LX/EWv;->options_:LX/EX2;

    invoke-virtual {v2, v0}, LX/EX0;->a(LX/EX2;)LX/EX0;

    .line 2131984
    invoke-virtual {v2}, LX/EX0;->l()LX/EX2;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->options_:LX/EX2;

    .line 2131985
    :cond_3
    iget v0, p0, LX/EWv;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EWv;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2131986
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_5

    .line 2131987
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    .line 2131988
    :cond_5
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->unknownFields:LX/EZQ;

    .line 2131989
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2131990
    return-void

    :cond_6
    move-object v2, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2131991
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2131992
    iput-byte v1, p0, LX/EWv;->memoizedIsInitialized:B

    .line 2131993
    iput v1, p0, LX/EWv;->memoizedSerializedSize:I

    .line 2131994
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->unknownFields:LX/EZQ;

    .line 2131995
    return-void
.end method

.method private static c(LX/EWv;)LX/EWu;
    .locals 1

    .prologue
    .line 2131996
    invoke-static {}, LX/EWu;->n()LX/EWu;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EWu;->a(LX/EWv;)LX/EWu;

    move-result-object v0

    return-object v0
.end method

.method private p()LX/EWc;
    .locals 2

    .prologue
    .line 2131997
    iget-object v0, p0, LX/EWv;->name_:Ljava/lang/Object;

    .line 2131998
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2131999
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2132000
    iput-object v0, p0, LX/EWv;->name_:Ljava/lang/Object;

    .line 2132001
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2132002
    const-string v0, ""

    iput-object v0, p0, LX/EWv;->name_:Ljava/lang/Object;

    .line 2132003
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    .line 2132004
    sget-object v0, LX/EX2;->c:LX/EX2;

    move-object v0, v0

    .line 2132005
    iput-object v0, p0, LX/EWv;->options_:LX/EX2;

    .line 2132006
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2132007
    new-instance v0, LX/EWu;

    invoke-direct {v0, p1}, LX/EWu;-><init>(LX/EYd;)V

    .line 2132008
    return-object v0
.end method

.method public final a(I)LX/EX6;
    .locals 1

    .prologue
    .line 2132009
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EX6;

    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2132010
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2132011
    iget v0, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2132012
    invoke-direct {p0}, LX/EWv;->p()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2132013
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2132014
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2132015
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2132016
    :cond_1
    iget v0, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 2132017
    const/4 v0, 0x3

    iget-object v1, p0, LX/EWv;->options_:LX/EX2;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2132018
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2132019
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2131914
    iget-byte v0, p0, LX/EWv;->memoizedIsInitialized:B

    .line 2131915
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2131916
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2131917
    :goto_1
    invoke-virtual {p0}, LX/EWv;->l()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 2131918
    invoke-virtual {p0, v0}, LX/EWv;->a(I)LX/EX6;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2131919
    iput-byte v1, p0, LX/EWv;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131920
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2131921
    :cond_3
    invoke-virtual {p0}, LX/EWv;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2131922
    iget-object v0, p0, LX/EWv;->options_:LX/EX2;

    move-object v0, v0

    .line 2131923
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2131924
    iput-byte v1, p0, LX/EWv;->memoizedIsInitialized:B

    goto :goto_0

    .line 2131925
    :cond_4
    iput-byte v2, p0, LX/EWv;->memoizedIsInitialized:B

    move v1, v2

    .line 2131926
    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2131927
    iget v0, p0, LX/EWv;->memoizedSerializedSize:I

    .line 2131928
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2131929
    :goto_0
    return v0

    .line 2131930
    :cond_0
    iget v0, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2131931
    invoke-direct {p0}, LX/EWv;->p()LX/EWc;

    move-result-object v0

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    .line 2131932
    :goto_2
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2131933
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2131934
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2131935
    :cond_1
    iget v0, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 2131936
    const/4 v0, 0x3

    iget-object v1, p0, LX/EWv;->options_:LX/EX2;

    invoke-static {v0, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2131937
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2131938
    iput v0, p0, LX/EWv;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2131896
    iget-object v0, p0, LX/EWv;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2131897
    sget-object v0, LX/EYC;->l:LX/EYn;

    const-class v1, LX/EWv;

    const-class v2, LX/EWu;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131898
    sget-object v0, LX/EWv;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2131899
    iget-object v0, p0, LX/EWv;->name_:Ljava/lang/Object;

    .line 2131900
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2131901
    check-cast v0, Ljava/lang/String;

    .line 2131902
    :goto_0
    return-object v0

    .line 2131903
    :cond_0
    check-cast v0, LX/EWc;

    .line 2131904
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2131905
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2131906
    iput-object v1, p0, LX/EWv;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2131907
    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 2131908
    iget-object v0, p0, LX/EWv;->value_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2131909
    iget v0, p0, LX/EWv;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2131910
    invoke-static {p0}, LX/EWv;->c(LX/EWv;)LX/EWu;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2131911
    invoke-static {}, LX/EWu;->n()LX/EWu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2131912
    invoke-static {p0}, LX/EWv;->c(LX/EWv;)LX/EWu;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2131913
    sget-object v0, LX/EWv;->c:LX/EWv;

    return-object v0
.end method
