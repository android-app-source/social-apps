.class public final LX/Cq4;
.super LX/Cq3;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:LX/CqA;


# direct methods
.method public constructor <init>(LX/CqA;)V
    .locals 1

    .prologue
    .line 1939069
    iput-object p1, p0, LX/Cq4;->b:LX/CqA;

    invoke-direct {p0, p1}, LX/Cq3;-><init>(LX/CqA;)V

    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1939068
    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v1, v1, LX/CqA;->X:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->n:LX/0Uh;

    const/16 v2, 0x508

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/adnw_request"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "adtype"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "banner300x250"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1939005
    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->n:LX/0Uh;

    const/16 v2, 0x509

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "jpg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "jpeg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gif"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "png"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "bmp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static a$redex0(LX/Cq4;)V
    .locals 2

    .prologue
    .line 1939066
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->f:LX/Ckw;

    const-string v1, "android_native_article_report_bad_ad_network_request"

    invoke-virtual {v0, v1}, LX/Ckw;->b(Ljava/lang/String;)V

    .line 1939067
    return-void
.end method

.method private b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 1939065
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/plugins/ad.php"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {p1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1939064
    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->n:LX/0Uh;

    const/16 v2, 0x509

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "mp4"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "webm"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ogg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static d(LX/Cq4;Landroid/webkit/WebView;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1939037
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v0, v0, LX/CqA;->am:Z

    if-nez v0, :cond_0

    .line 1939038
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->j:LX/ClH;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->Y:Ljava/lang/String;

    iget-object v2, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v2, v2, LX/CqA;->ao:Z

    invoke-virtual {v0, v1, v2, v7}, LX/ClH;->a(Ljava/lang/String;ZLandroid/webkit/WebView;)V

    .line 1939039
    :cond_0
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->y:LX/CqH;

    check-cast p1, LX/CsX;

    invoke-virtual {v0, p1}, LX/CqH;->a(LX/CsX;)V

    .line 1939040
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    .line 1939041
    iput-boolean v8, v0, LX/CqA;->am:Z

    .line 1939042
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308bd

    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1939043
    iget-object v2, p0, LX/Cq4;->b:LX/CqA;

    new-instance v3, LX/Cmy;

    new-instance v4, LX/Cmw;

    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->A:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/Cmv;->c:LX/Cmv;

    :goto_0
    sget-object v5, LX/Cmv;->b:LX/Cmv;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->A:LX/8bZ;

    invoke-virtual {v1}, LX/8bZ;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/Cmv;->c:LX/Cmv;

    :goto_1
    sget-object v6, LX/Cmv;->b:LX/Cmv;

    invoke-direct {v4, v0, v5, v1, v6}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    invoke-direct {v3, v4, v7, v7, v8}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    invoke-virtual {v2, v3}, LX/Cod;->a(LX/Cml;)V

    .line 1939044
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d16d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 1939045
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1939046
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v1}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, LX/CpO;->a(Landroid/view/View;)LX/CpO;

    move-result-object v1

    .line 1939047
    iput-object v1, v0, LX/CqA;->ad:LX/CpO;

    .line 1939048
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ad:LX/CpO;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->Y:Ljava/lang/String;

    .line 1939049
    iput-object v1, v0, LX/CpO;->y:Ljava/lang/String;

    .line 1939050
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->k:LX/Ckv;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Ckv;->a(Ljava/lang/String;)V

    .line 1939051
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->a()V

    .line 1939052
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-wide v0, v0, LX/CqA;->T:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1939053
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ad:LX/CpO;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-wide v2, v1, LX/CqA;->T:J

    invoke-virtual {v0, v2, v3}, LX/CpO;->a(J)V

    .line 1939054
    :cond_1
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->Y:Ljava/lang/String;

    iget-object v1, p0, LX/Cq4;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->ad:LX/CpO;

    .line 1939055
    iget-object v3, p0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v3}, LX/CpO;->k()V

    .line 1939056
    iget-object v3, p0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->o:LX/CjD;

    iget-object v4, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v4}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LX/CpO;->h()I

    move-result v7

    invoke-virtual {v2}, LX/CpO;->i()I

    move-result v8

    move-object v5, v0

    move-object v6, v1

    const/high16 v2, 0x41000000    # 8.0f

    .line 1939057
    iget-object p1, v3, LX/CjD;->a:LX/0tX;

    .line 1939058
    new-instance v9, LX/8ay;

    invoke-direct {v9}, LX/8ay;-><init>()V

    move-object v9, v9

    .line 1939059
    const-string v0, "graphQLID"

    invoke-virtual {v9, v0, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v0, "placementID"

    invoke-virtual {v9, v0, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v0, "imageWidth"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string v0, "imageHeight"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string v0, "iconHeight"

    invoke-static {v4, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string v0, "iconWidth"

    invoke-static {v4, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string v0, "scale"

    sget-object v1, LX/0wC;->NUMBER_1:LX/0wC;

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v9

    check-cast v9, LX/8ay;

    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v9

    .line 1939060
    new-instance p1, LX/CjC;

    invoke-direct {p1, v3}, LX/CjC;-><init>(LX/CjD;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-static {v9, p1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object v3, v9

    .line 1939061
    new-instance v4, LX/Cq2;

    invoke-direct {v4, p0}, LX/Cq2;-><init>(LX/Cq4;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1939062
    return-void

    .line 1939063
    :cond_2
    sget-object v0, LX/Cmv;->b:LX/Cmv;

    goto/16 :goto_0

    :cond_3
    sget-object v1, LX/Cmv;->b:LX/Cmv;

    goto/16 :goto_1
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1939070
    invoke-super {p0, p1, p2}, LX/Cq3;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1939071
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_2

    .line 1939072
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1939073
    const-string v0, "document.body.style.margin=0; document.body.style.padding=0; var viewport = document.querySelector(\'meta[name=viewport]\');if (viewport != null) {  viewport.setAttribute(\'content\', \"width=device-width\");}"

    invoke-static {p1, v0}, LX/CqA;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1939074
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$5;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$5;-><init>(LX/Cq4;)V

    const-wide/16 v2, 0xbb8

    const v4, -0x131fbcb9

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1939075
    :goto_0
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    const/4 v1, 0x1

    .line 1939076
    iput-boolean v1, v0, LX/CqA;->am:Z

    .line 1939077
    sget-boolean v0, LX/CqA;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v0, v0, LX/CqA;->an:Z

    if-nez v0, :cond_0

    .line 1939078
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    invoke-static {v0}, LX/CqA;->q(LX/CqA;)V

    .line 1939079
    :cond_0
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v0, v0, LX/CqA;->al:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-boolean v0, v0, LX/CqA;->an:Z

    if-eqz v0, :cond_1

    .line 1939080
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    .line 1939081
    iget-object v1, v0, LX/CqA;->aa:LX/CsX;

    if-nez v1, :cond_3

    .line 1939082
    :cond_1
    :goto_1
    return-void

    .line 1939083
    :cond_2
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1939084
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xbb

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1939085
    :goto_2
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xb5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1939086
    :goto_3
    goto :goto_0

    .line 1939087
    :cond_3
    iget-object v1, v0, LX/CqA;->aa:LX/CsX;

    .line 1939088
    iget v2, v1, LX/CsX;->n:I

    if-nez v2, :cond_5

    iget v2, v1, LX/CsX;->j:F

    const/4 v0, 0x0

    cmpl-float v2, v2, v0

    if-lez v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 1939089
    if-eqz v2, :cond_4

    .line 1939090
    invoke-virtual {v1}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1939091
    const/4 v0, -0x2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1939092
    const/4 v0, 0x0

    iput v0, v1, LX/CsX;->j:F

    .line 1939093
    invoke-virtual {v1, v2}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939094
    :cond_4
    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 1939095
    :cond_6
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 1939096
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 1939097
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xba

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_7

    .line 1939098
    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 1939099
    :cond_7
    const-string v0, "var documentHead = document.getElementsByTagName(\'head\')[0];var metas = documentHead.getElementsByTagName(\'meta\');var hasViewportMetaTag = false;for (var i = 0; i < metas.length; i++) {var meta = metas[i];if (meta.getAttribute(\'name\') === \'viewport\') {hasViewportMetaTag = true;break;}}if (!hasViewportMetaTag) {var viewportTag = document.createElement(\'meta\');viewportTag.name = \'viewport\';viewportTag.content = \'width=device-width\';documentHead.appendChild(viewportTag);}"

    invoke-static {p1, v0}, LX/CqA;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_2

    .line 1939100
    :cond_8
    const-string v0, "var documentBody = document.getElementsByTagName(\'body\')[0];var documentBodyStyle = window.getComputedStyle(documentBody);if (documentBodyStyle.margin === \'8px\') {documentBody.style.margin = 0;}"

    invoke-static {p1, v0}, LX/CqA;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1939033
    invoke-super {p0, p1, p2, p3}, LX/Cq3;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1939034
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    const/4 v1, 0x0

    .line 1939035
    iput-boolean v1, v0, LX/CqA;->am:Z

    .line 1939036
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1939029
    invoke-super {p0, p1, p2, p3, p4}, LX/Cq3;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1939030
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->i:LX/ClK;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->f:LX/Ckw;

    iget-object v2, p0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->g(LX/Ckw;Ljava/lang/String;)V

    .line 1939031
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->c:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/CqA;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".IAWebViewClient.onReceivedError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load URI. errorCode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|failingUrl:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|description:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1939032
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .prologue
    .line 1939024
    invoke-super {p0, p1, p2, p3}, LX/Cq3;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 1939025
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->i:LX/ClK;

    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->f:LX/Ckw;

    iget-object v2, p0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->g(LX/Ckw;Ljava/lang/String;)V

    .line 1939026
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 1939027
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->c:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/CqA;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".IAWebViewClient.onReceivedSslError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got SSL Error loading URI. error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/net/http/SslError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1939028
    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1939006
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    .line 1939007
    invoke-super {p0, p1, p2}, LX/Cq3;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 1939008
    :goto_0
    return-object v0

    .line 1939009
    :cond_0
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, LX/Cq4;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1939010
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$1;-><init>(LX/Cq4;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebView;)V

    const v3, -0x3f70ef2f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1939011
    :cond_1
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, LX/Cq4;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1939012
    new-instance v1, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$2;-><init>(LX/Cq4;Landroid/webkit/WebView;)V

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1939013
    :cond_2
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Cq4;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1939014
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->aj:Ljava/util/List;

    if-nez v0, :cond_3

    .line 1939015
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1939016
    iput-object v1, v0, LX/CqA;->aj:Ljava/util/List;

    .line 1939017
    :cond_3
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->aj:Ljava/util/List;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1939018
    :cond_4
    :goto_1
    invoke-super {p0, p1, p2}, LX/Cq3;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0

    .line 1939019
    :cond_5
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Cq4;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1939020
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ak:Ljava/util/List;

    if-nez v0, :cond_6

    .line 1939021
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1939022
    iput-object v1, v0, LX/CqA;->ak:Ljava/util/List;

    .line 1939023
    :cond_6
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ak:Ljava/util/List;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1938989
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, LX/Cq4;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1938990
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;

    invoke-direct {v2, p0, p2, p1}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;-><init>(LX/Cq4;Ljava/lang/String;Landroid/webkit/WebView;)V

    const v3, 0x4d6e66fc    # 2.49982912E8f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1938991
    :goto_0
    return-object v0

    .line 1938992
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, LX/Cq4;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1938993
    new-instance v1, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;-><init>(LX/Cq4;Landroid/webkit/WebView;)V

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1938994
    :cond_1
    invoke-direct {p0, p2}, LX/Cq4;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1938995
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->aj:Ljava/util/List;

    if-nez v0, :cond_2

    .line 1938996
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1938997
    iput-object v1, v0, LX/CqA;->aj:Ljava/util/List;

    .line 1938998
    :cond_2
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->aj:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1938999
    :cond_3
    :goto_1
    invoke-super {p0, p1, p2}, LX/Cq3;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0

    .line 1939000
    :cond_4
    invoke-direct {p0, p2}, LX/Cq4;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1939001
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ak:Ljava/util/List;

    if-nez v0, :cond_5

    .line 1939002
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1939003
    iput-object v1, v0, LX/CqA;->ak:Ljava/util/List;

    .line 1939004
    :cond_5
    iget-object v0, p0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ak:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1938985
    move-object v0, p1

    check-cast v0, LX/CsX;

    .line 1938986
    iget-object v1, p0, LX/Cq4;->b:LX/CqA;

    invoke-virtual {v1}, LX/CqA;->g()F

    move-result v1

    .line 1938987
    invoke-virtual {v0, v1}, LX/CsX;->setFallbackAspectRatio(F)V

    .line 1938988
    invoke-super {p0, p1, p2}, LX/Cq3;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
