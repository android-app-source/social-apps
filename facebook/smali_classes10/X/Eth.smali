.class public LX/Eth;
.super LX/3pF;
.source ""

# interfaces
.implements LX/6Ub;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/EtZ;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5P0;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field private final f:Z

.field private final g:Z

.field private final h:LX/Evy;

.field private final i:LX/Eup;


# direct methods
.method public constructor <init>(LX/Evy;LX/0gc;Landroid/content/Context;LX/EtZ;LX/0ad;LX/0Px;ZZLX/Eup;)V
    .locals 1
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EtZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/Eup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Evy;",
            "LX/0gc;",
            "Landroid/content/Context;",
            "LX/EtZ;",
            "LX/0ad;",
            "LX/0Px",
            "<",
            "LX/5P0;",
            ">;ZZ",
            "LX/Eup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2178510
    invoke-direct {p0, p2}, LX/3pF;-><init>(LX/0gc;)V

    .line 2178511
    iput-object p1, p0, LX/Eth;->h:LX/Evy;

    .line 2178512
    iput-object p3, p0, LX/Eth;->a:Landroid/content/Context;

    .line 2178513
    iput-object p4, p0, LX/Eth;->b:LX/EtZ;

    .line 2178514
    iput-object p5, p0, LX/Eth;->e:LX/0ad;

    .line 2178515
    iput-object p6, p0, LX/Eth;->c:LX/0Px;

    .line 2178516
    iput-boolean p7, p0, LX/Eth;->f:Z

    .line 2178517
    iput-boolean p8, p0, LX/Eth;->g:Z

    .line 2178518
    iput-object p9, p0, LX/Eth;->i:LX/Eup;

    .line 2178519
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Eth;->d:Landroid/util/SparseArray;

    .line 2178520
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2178521
    iget-object v0, p0, LX/Eth;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/Eth;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P0;

    iget v0, v0, LX/5P0;->titleResId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6Ua;
    .locals 1

    .prologue
    .line 2178522
    iget-object v0, p0, LX/Eth;->b:LX/EtZ;

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2178523
    sget-object v1, LX/Etg;->a:[I

    iget-object v0, p0, LX/Eth;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P0;

    invoke-virtual {v0}, LX/5P0;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2178524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Native tab that doesn\'t provide a native fragment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2178525
    :pswitch_0
    iget-boolean v0, p0, LX/Eth;->f:Z

    if-eqz v0, :cond_0

    .line 2178526
    sget-object v0, LX/89v;->FRIENDS_CENTER:LX/89v;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(LX/89v;Z)Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    move-result-object v0

    .line 2178527
    :goto_0
    iget-object v1, p0, LX/Eth;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2178528
    return-object v0

    .line 2178529
    :cond_0
    sget-object v0, LX/89v;->FRIENDS_CENTER:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-boolean v2, p0, LX/Eth;->g:Z

    invoke-static {v2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    invoke-static {v0, v1, v3, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;ZLX/03R;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    goto :goto_0

    .line 2178530
    :pswitch_1
    new-instance v0, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/friends/FriendsCenterFriendsLKFragment;-><init>()V

    goto :goto_0

    .line 2178531
    :pswitch_2
    iget-object v0, p0, LX/Eth;->i:LX/Eup;

    if-nez v0, :cond_1

    .line 2178532
    new-instance v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;-><init>()V

    goto :goto_0

    .line 2178533
    :cond_1
    iget-object v0, p0, LX/Eth;->i:LX/Eup;

    .line 2178534
    new-instance v4, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {v4}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;-><init>()V

    .line 2178535
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2178536
    const-string v6, "attachment"

    .line 2178537
    iget-object v7, v0, LX/Eup;->a:LX/5Oy;

    move-object v7, v7

    .line 2178538
    invoke-virtual {v7}, LX/5Oy;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178539
    const-string v6, "user_id"

    invoke-virtual {v0}, LX/Eup;->a()J

    move-result-wide v8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2178540
    invoke-virtual {v0}, LX/Eup;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2178541
    const-string v6, "name"

    invoke-virtual {v0}, LX/Eup;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178542
    :cond_2
    invoke-virtual {v0}, LX/Eup;->d()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2178543
    const-string v6, "profile_pic"

    invoke-virtual {v0}, LX/Eup;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178544
    :cond_3
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2178545
    move-object v0, v4

    .line 2178546
    goto :goto_0

    .line 2178547
    :pswitch_3
    new-instance v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;-><init>()V

    goto :goto_0

    .line 2178548
    :pswitch_4
    iget-object v0, p0, LX/Eth;->e:LX/0ad;

    sget-short v1, LX/2hr;->z:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2178549
    new-instance v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;-><init>()V

    goto :goto_0

    .line 2178550
    :cond_4
    iget-object v0, p0, LX/Eth;->e:LX/0ad;

    sget-short v1, LX/2hr;->v:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2178551
    new-instance v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsLCFragment;-><init>()V

    goto/16 :goto_0

    .line 2178552
    :cond_5
    new-instance v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;-><init>()V

    goto/16 :goto_0

    .line 2178553
    :pswitch_5
    sget-object v0, LX/89v;->FRIENDS_CENTER:LX/89v;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a(LX/89v;)Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2178554
    iget-object v0, p0, LX/Eth;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
