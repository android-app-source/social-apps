.class public final enum LX/Cqa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cqa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cqa;

.field public static final enum HIDDEN:LX/Cqa;

.field public static final enum VISIBLE:LX/Cqa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1940077
    new-instance v0, LX/Cqa;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, LX/Cqa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqa;->VISIBLE:LX/Cqa;

    .line 1940078
    new-instance v0, LX/Cqa;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v3}, LX/Cqa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    .line 1940079
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cqa;

    sget-object v1, LX/Cqa;->VISIBLE:LX/Cqa;

    aput-object v1, v0, v2

    sget-object v1, LX/Cqa;->HIDDEN:LX/Cqa;

    aput-object v1, v0, v3

    sput-object v0, LX/Cqa;->$VALUES:[LX/Cqa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cqa;
    .locals 1

    .prologue
    .line 1940072
    const-class v0, LX/Cqa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cqa;

    return-object v0
.end method

.method public static values()[LX/Cqa;
    .locals 1

    .prologue
    .line 1940075
    sget-object v0, LX/Cqa;->$VALUES:[LX/Cqa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cqa;

    return-object v0
.end method


# virtual methods
.method public final getVisibility()I
    .locals 1

    .prologue
    .line 1940074
    sget-object v0, LX/Cqa;->VISIBLE:LX/Cqa;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final inverse()LX/Cqa;
    .locals 1

    .prologue
    .line 1940073
    sget-object v0, LX/Cqa;->VISIBLE:LX/Cqa;

    if-ne p0, v0, :cond_0

    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Cqa;->VISIBLE:LX/Cqa;

    goto :goto_0
.end method
