.class public final enum LX/CuG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CuG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CuG;

.field public static final enum INACTIVE:LX/CuG;

.field public static final enum SENSOR:LX/CuG;

.field public static final enum TOUCH:LX/CuG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1946247
    new-instance v0, LX/CuG;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v2}, LX/CuG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuG;->INACTIVE:LX/CuG;

    .line 1946248
    new-instance v0, LX/CuG;

    const-string v1, "SENSOR"

    invoke-direct {v0, v1, v3}, LX/CuG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuG;->SENSOR:LX/CuG;

    .line 1946249
    new-instance v0, LX/CuG;

    const-string v1, "TOUCH"

    invoke-direct {v0, v1, v4}, LX/CuG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CuG;->TOUCH:LX/CuG;

    .line 1946250
    const/4 v0, 0x3

    new-array v0, v0, [LX/CuG;

    sget-object v1, LX/CuG;->INACTIVE:LX/CuG;

    aput-object v1, v0, v2

    sget-object v1, LX/CuG;->SENSOR:LX/CuG;

    aput-object v1, v0, v3

    sget-object v1, LX/CuG;->TOUCH:LX/CuG;

    aput-object v1, v0, v4

    sput-object v0, LX/CuG;->$VALUES:[LX/CuG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1946251
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CuG;
    .locals 1

    .prologue
    .line 1946252
    const-class v0, LX/CuG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CuG;

    return-object v0
.end method

.method public static values()[LX/CuG;
    .locals 1

    .prologue
    .line 1946253
    sget-object v0, LX/CuG;->$VALUES:[LX/CuG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CuG;

    return-object v0
.end method


# virtual methods
.method public final isActive()Z
    .locals 1

    .prologue
    .line 1946254
    sget-object v0, LX/CuG;->SENSOR:LX/CuG;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/CuG;->TOUCH:LX/CuG;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
