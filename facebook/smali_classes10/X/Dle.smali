.class public final LX/Dle;
.super LX/62U;
.source ""


# instance fields
.field private m:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2038378
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 2038379
    const v0, 0x7f0d0590

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Dle;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2038380
    return-void
.end method


# virtual methods
.method public final c(I)V
    .locals 3

    .prologue
    .line 2038381
    iget-object v0, p0, LX/Dle;->m:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    .line 2038382
    packed-switch p1, :pswitch_data_0

    .line 2038383
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment index out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2038384
    :pswitch_0
    iget-object v0, p0, LX/Dle;->m:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082bc8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2038385
    :cond_0
    :goto_0
    return-void

    .line 2038386
    :pswitch_1
    iget-object v0, p0, LX/Dle;->m:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082bc9

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
