.class public final LX/EsB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;

.field public h:Ljava/lang/String;

.field public i:LX/EsC;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/EsC;)V
    .locals 0

    .prologue
    .line 2174534
    iput-object p1, p0, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2174535
    iput-object p2, p0, LX/EsB;->b:Ljava/lang/String;

    .line 2174536
    iput-object p3, p0, LX/EsB;->c:Ljava/lang/String;

    .line 2174537
    iput-object p4, p0, LX/EsB;->d:Ljava/lang/String;

    .line 2174538
    iput-object p5, p0, LX/EsB;->e:Ljava/lang/String;

    .line 2174539
    iput-object p6, p0, LX/EsB;->f:Landroid/net/Uri;

    .line 2174540
    iput-object p7, p0, LX/EsB;->g:Landroid/net/Uri;

    .line 2174541
    iput-object p8, p0, LX/EsB;->h:Ljava/lang/String;

    .line 2174542
    iput-object p9, p0, LX/EsB;->i:LX/EsC;

    .line 2174543
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v0, 0x1

    const v1, 0x68e72e9

    invoke-static {v13, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2174544
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2174545
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    sget-object v2, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    const-string v2, "goodwillBirthdayPromo"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v12

    .line 2174546
    new-instance v0, LX/EtE;

    iget-object v2, p0, LX/EsB;->f:Landroid/net/Uri;

    iget-object v3, p0, LX/EsB;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082a19

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v5, v5, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->i:LX/1Er;

    iget-object v6, p0, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v6, v6, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->j:LX/9iU;

    sget-object v7, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v8, p0, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v8, v8, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->k:LX/1Fs;

    iget-object v9, p0, LX/EsB;->a:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v9, v9, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    new-instance v10, LX/EsA;

    invoke-direct {v10, p0, p1, v12, v1}, LX/EsA;-><init>(LX/EsB;Landroid/view/View;Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;Landroid/content/Context;)V

    invoke-direct/range {v0 .. v10}, LX/EtE;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1Er;LX/9iU;Lcom/facebook/common/callercontext/CallerContext;LX/1Fs;LX/0if;LX/EsA;)V

    .line 2174547
    invoke-static {}, LX/EtA;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2174548
    invoke-virtual {v0}, LX/EtA;->a()V

    .line 2174549
    :goto_0
    const v0, -0x798d28

    invoke-static {v13, v13, v0, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2174550
    :cond_0
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v1

    new-instance v2, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$1;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$1;-><init>(LX/EtA;)V

    const v3, -0x3b77c80f

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
