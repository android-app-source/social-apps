.class public final LX/D1Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/D1T;

.field public f:LX/697;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

.field public i:Z

.field public j:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1956768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1956769
    iput-object p1, p0, LX/D1Q;->a:Landroid/content/Context;

    .line 1956770
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1Q;->b:Ljava/lang/String;

    .line 1956771
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1Q;->c:Ljava/lang/String;

    .line 1956772
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1Q;->d:Ljava/lang/String;

    .line 1956773
    sget-object v0, LX/D1T;->STORE_VISITS_AD:LX/D1T;

    iput-object v0, p0, LX/D1Q;->e:LX/D1T;

    .line 1956774
    iput-object v1, p0, LX/D1Q;->f:LX/697;

    .line 1956775
    iput-object v1, p0, LX/D1Q;->g:Landroid/view/View;

    .line 1956776
    iput-object v1, p0, LX/D1Q;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1956777
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D1Q;->i:Z

    .line 1956778
    return-void
.end method


# virtual methods
.method public final a()LX/D1R;
    .locals 2

    .prologue
    .line 1956779
    iget-object v0, p0, LX/D1Q;->f:LX/697;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1Q;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1Q;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    if-nez v0, :cond_1

    .line 1956780
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "All the query arguments need to be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1956781
    :cond_1
    new-instance v0, LX/D1R;

    invoke-direct {v0, p0}, LX/D1R;-><init>(LX/D1Q;)V

    return-object v0
.end method
