.class public final LX/EZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWR;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/EZO;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:LX/EZN;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2139291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)LX/EZN;
    .locals 2

    .prologue
    .line 2139278
    iget-object v0, p0, LX/EZM;->c:LX/EZN;

    if-eqz v0, :cond_1

    .line 2139279
    iget v0, p0, LX/EZM;->b:I

    if-ne p1, v0, :cond_0

    .line 2139280
    iget-object v0, p0, LX/EZM;->c:LX/EZN;

    .line 2139281
    :goto_0
    return-object v0

    .line 2139282
    :cond_0
    iget v0, p0, LX/EZM;->b:I

    iget-object v1, p0, LX/EZM;->c:LX/EZN;

    invoke-virtual {v1}, LX/EZN;->a()LX/EZO;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/EZM;->b(ILX/EZO;)LX/EZM;

    .line 2139283
    :cond_1
    if-nez p1, :cond_2

    .line 2139284
    const/4 v0, 0x0

    goto :goto_0

    .line 2139285
    :cond_2
    iget-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZO;

    .line 2139286
    iput p1, p0, LX/EZM;->b:I

    .line 2139287
    invoke-static {}, LX/EZN;->c()LX/EZN;

    move-result-object v1

    iput-object v1, p0, LX/EZM;->c:LX/EZN;

    .line 2139288
    if-eqz v0, :cond_3

    .line 2139289
    iget-object v1, p0, LX/EZM;->c:LX/EZN;

    invoke-virtual {v1, v0}, LX/EZN;->a(LX/EZO;)LX/EZN;

    .line 2139290
    :cond_3
    iget-object v0, p0, LX/EZM;->c:LX/EZN;

    goto :goto_0
.end method

.method private b(ILX/EZO;)LX/EZM;
    .locals 2

    .prologue
    .line 2139269
    if-nez p1, :cond_0

    .line 2139270
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139271
    :cond_0
    iget-object v0, p0, LX/EZM;->c:LX/EZN;

    if-eqz v0, :cond_1

    iget v0, p0, LX/EZM;->b:I

    if-ne v0, p1, :cond_1

    .line 2139272
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZM;->c:LX/EZN;

    .line 2139273
    const/4 v0, 0x0

    iput v0, p0, LX/EZM;->b:I

    .line 2139274
    :cond_1
    iget-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2139275
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    .line 2139276
    :cond_2
    iget-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2139277
    return-object p0
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 2139266
    if-nez p1, :cond_0

    .line 2139267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139268
    :cond_0
    iget v0, p0, LX/EZM;->b:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()LX/EZM;
    .locals 2

    .prologue
    .line 2139261
    new-instance v0, LX/EZM;

    invoke-direct {v0}, LX/EZM;-><init>()V

    .line 2139262
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, LX/EZM;->a:Ljava/util/Map;

    .line 2139263
    const/4 v1, 0x0

    iput v1, v0, LX/EZM;->b:I

    .line 2139264
    const/4 v1, 0x0

    iput-object v1, v0, LX/EZM;->c:LX/EZN;

    .line 2139265
    return-object v0
.end method


# virtual methods
.method public final a(II)LX/EZM;
    .locals 4

    .prologue
    .line 2139257
    if-nez p1, :cond_0

    .line 2139258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139259
    :cond_0
    invoke-direct {p0, p1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/EZN;->a(J)LX/EZN;

    .line 2139260
    return-object p0
.end method

.method public final a(ILX/EZO;)LX/EZM;
    .locals 2

    .prologue
    .line 2139243
    if-nez p1, :cond_0

    .line 2139244
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139245
    :cond_0
    invoke-direct {p0, p1}, LX/EZM;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2139246
    invoke-direct {p0, p1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/EZN;->a(LX/EZO;)LX/EZN;

    .line 2139247
    :goto_0
    return-object p0

    .line 2139248
    :cond_1
    invoke-direct {p0, p1, p2}, LX/EZM;->b(ILX/EZO;)LX/EZM;

    goto :goto_0
.end method

.method public final a(LX/EWd;)LX/EZM;
    .locals 1

    .prologue
    .line 2139254
    :cond_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2139255
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0, p1}, LX/EZM;->a(ILX/EWd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2139256
    :cond_1
    return-object p0
.end method

.method public final a(LX/EZQ;)LX/EZM;
    .locals 3

    .prologue
    .line 2139249
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2139250
    if-eq p1, v0, :cond_0

    .line 2139251
    iget-object v0, p1, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2139252
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZO;

    invoke-virtual {p0, v1, v0}, LX/EZM;->a(ILX/EZO;)LX/EZM;

    goto :goto_0

    .line 2139253
    :cond_0
    return-object p0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2139292
    const/4 v0, 0x1

    return v0
.end method

.method public final a(ILX/EWd;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2139193
    ushr-int/lit8 v1, p1, 0x3

    move v1, v1

    .line 2139194
    and-int/lit8 v2, p1, 0x7

    move v2, v2

    .line 2139195
    packed-switch v2, :pswitch_data_0

    .line 2139196
    invoke-static {}, LX/EYr;->g()LX/EYr;

    move-result-object v0

    throw v0

    .line 2139197
    :pswitch_0
    invoke-direct {p0, v1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v1

    invoke-virtual {p2}, LX/EWd;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/EZN;->a(J)LX/EZN;

    .line 2139198
    :goto_0
    return v0

    .line 2139199
    :pswitch_1
    invoke-direct {p0, v1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v1

    invoke-virtual {p2}, LX/EWd;->g()J

    move-result-wide v2

    .line 2139200
    iget-object p0, v1, LX/EZN;->a:LX/EZO;

    iget-object p0, p0, LX/EZO;->d:Ljava/util/List;

    if-nez p0, :cond_0

    .line 2139201
    iget-object p0, v1, LX/EZN;->a:LX/EZO;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2139202
    iput-object p1, p0, LX/EZO;->d:Ljava/util/List;

    .line 2139203
    :cond_0
    iget-object p0, v1, LX/EZN;->a:LX/EZO;

    iget-object p0, p0, LX/EZO;->d:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139204
    goto :goto_0

    .line 2139205
    :pswitch_2
    invoke-direct {p0, v1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v1

    invoke-virtual {p2}, LX/EWd;->k()LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EZN;->a(LX/EWc;)LX/EZN;

    goto :goto_0

    .line 2139206
    :pswitch_3
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2139207
    sget-object v3, LX/EYa;->c:LX/EYa;

    move-object v3, v3

    .line 2139208
    invoke-virtual {p2, v1, v2, v3}, LX/EWd;->a(ILX/EWR;LX/EYZ;)V

    .line 2139209
    invoke-direct {p0, v1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v1

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v2

    .line 2139210
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    iget-object v3, v3, LX/EZO;->f:Ljava/util/List;

    if-nez v3, :cond_1

    .line 2139211
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 2139212
    iput-object p0, v3, LX/EZO;->f:Ljava/util/List;

    .line 2139213
    :cond_1
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    iget-object v3, v3, LX/EZO;->f:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139214
    goto :goto_0

    .line 2139215
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2139216
    :pswitch_5
    invoke-direct {p0, v1}, LX/EZM;->a(I)LX/EZN;

    move-result-object v1

    invoke-virtual {p2}, LX/EWd;->h()I

    move-result v2

    .line 2139217
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    iget-object v3, v3, LX/EZO;->c:Ljava/util/List;

    if-nez v3, :cond_2

    .line 2139218
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 2139219
    iput-object p0, v3, LX/EZO;->c:Ljava/util/List;

    .line 2139220
    :cond_2
    iget-object v3, v1, LX/EZN;->a:LX/EZO;

    iget-object v3, v3, LX/EZO;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139221
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b([B)LX/EWR;
    .locals 3

    .prologue
    .line 2139222
    :try_start_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, LX/EWd;->a([BII)LX/EWd;

    move-result-object v0

    move-object v0, v0

    .line 2139223
    invoke-virtual {p0, v0}, LX/EZM;->a(LX/EWd;)LX/EZM;

    .line 2139224
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EWd;->a(I)V
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2139225
    return-object p0

    .line 2139226
    :catch_0
    move-exception v0

    .line 2139227
    throw v0

    .line 2139228
    :catch_1
    move-exception v0

    .line 2139229
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()LX/EZQ;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2139230
    invoke-direct {p0, v2}, LX/EZM;->a(I)LX/EZN;

    .line 2139231
    iget-object v0, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2139232
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2139233
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/EZM;->a:Ljava/util/Map;

    .line 2139234
    return-object v0

    .line 2139235
    :cond_0
    new-instance v0, LX/EZQ;

    iget-object v1, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EZQ;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2139236
    invoke-virtual {p0, p1}, LX/EZM;->a(LX/EWd;)LX/EZM;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/EZQ;
    .locals 1

    .prologue
    .line 2139237
    invoke-virtual {p0}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    return-object v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2139238
    const/4 v3, 0x0

    .line 2139239
    invoke-direct {p0, v3}, LX/EZM;->a(I)LX/EZN;

    .line 2139240
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v0

    new-instance v1, LX/EZQ;

    iget-object v2, p0, LX/EZM;->a:Ljava/util/Map;

    invoke-direct {v1, v2}, LX/EZQ;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2139241
    invoke-virtual {p0}, LX/EZM;->c()LX/EZQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2139242
    invoke-virtual {p0}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    return-object v0
.end method
