.class public final enum LX/E0s;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E0s;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E0s;

.field public static final enum ERROR_PARAM:LX/E0s;

.field public static final enum INVALID_NAME:LX/E0s;

.field public static final enum INVALID_PHONE:LX/E0s;

.field public static final enum INVALID_WEBSITE:LX/E0s;

.field public static final enum LOCATION_INACCURATE:LX/E0s;

.field public static final enum OTHER:LX/E0s;

.field public static final enum SENTRY_FAIL:LX/E0s;

.field public static final enum SIMILAR_NAME:LX/E0s;

.field public static final enum TOO_MANY_PLACE:LX/E0s;


# instance fields
.field public final errorCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2069036
    new-instance v0, LX/E0s;

    const-string v1, "ERROR_PARAM"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v4, v2}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->ERROR_PARAM:LX/E0s;

    .line 2069037
    new-instance v0, LX/E0s;

    const-string v1, "SENTRY_FAIL"

    const/16 v2, 0x170

    invoke-direct {v0, v1, v5, v2}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->SENTRY_FAIL:LX/E0s;

    .line 2069038
    new-instance v0, LX/E0s;

    const-string v1, "TOO_MANY_PLACE"

    const/16 v2, 0x960

    invoke-direct {v0, v1, v6, v2}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->TOO_MANY_PLACE:LX/E0s;

    .line 2069039
    new-instance v0, LX/E0s;

    const-string v1, "SIMILAR_NAME"

    const/16 v2, 0x966

    invoke-direct {v0, v1, v7, v2}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->SIMILAR_NAME:LX/E0s;

    .line 2069040
    new-instance v0, LX/E0s;

    const-string v1, "INVALID_NAME"

    const/16 v2, 0x969

    invoke-direct {v0, v1, v8, v2}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->INVALID_NAME:LX/E0s;

    .line 2069041
    new-instance v0, LX/E0s;

    const-string v1, "LOCATION_INACCURATE"

    const/4 v2, 0x5

    const/16 v3, 0x968

    invoke-direct {v0, v1, v2, v3}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->LOCATION_INACCURATE:LX/E0s;

    .line 2069042
    new-instance v0, LX/E0s;

    const-string v1, "INVALID_PHONE"

    const/4 v2, 0x6

    const/16 v3, 0x96a

    invoke-direct {v0, v1, v2, v3}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->INVALID_PHONE:LX/E0s;

    .line 2069043
    new-instance v0, LX/E0s;

    const-string v1, "INVALID_WEBSITE"

    const/4 v2, 0x7

    const/16 v3, 0x96b

    invoke-direct {v0, v1, v2, v3}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->INVALID_WEBSITE:LX/E0s;

    .line 2069044
    new-instance v0, LX/E0s;

    const-string v1, "OTHER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/E0s;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/E0s;->OTHER:LX/E0s;

    .line 2069045
    const/16 v0, 0x9

    new-array v0, v0, [LX/E0s;

    sget-object v1, LX/E0s;->ERROR_PARAM:LX/E0s;

    aput-object v1, v0, v4

    sget-object v1, LX/E0s;->SENTRY_FAIL:LX/E0s;

    aput-object v1, v0, v5

    sget-object v1, LX/E0s;->TOO_MANY_PLACE:LX/E0s;

    aput-object v1, v0, v6

    sget-object v1, LX/E0s;->SIMILAR_NAME:LX/E0s;

    aput-object v1, v0, v7

    sget-object v1, LX/E0s;->INVALID_NAME:LX/E0s;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/E0s;->LOCATION_INACCURATE:LX/E0s;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/E0s;->INVALID_PHONE:LX/E0s;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/E0s;->INVALID_WEBSITE:LX/E0s;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/E0s;->OTHER:LX/E0s;

    aput-object v2, v0, v1

    sput-object v0, LX/E0s;->$VALUES:[LX/E0s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2069046
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2069047
    iput p3, p0, LX/E0s;->errorCode:I

    .line 2069048
    return-void
.end method

.method public static fromErrorCode(I)LX/E0s;
    .locals 5

    .prologue
    .line 2069049
    invoke-static {}, LX/E0s;->values()[LX/E0s;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2069050
    iget v4, v0, LX/E0s;->errorCode:I

    if-ne v4, p0, :cond_0

    .line 2069051
    :goto_1
    return-object v0

    .line 2069052
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2069053
    :cond_1
    sget-object v0, LX/E0s;->OTHER:LX/E0s;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/E0s;
    .locals 1

    .prologue
    .line 2069054
    const-class v0, LX/E0s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E0s;

    return-object v0
.end method

.method public static values()[LX/E0s;
    .locals 1

    .prologue
    .line 2069055
    sget-object v0, LX/E0s;->$VALUES:[LX/E0s;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E0s;

    return-object v0
.end method
