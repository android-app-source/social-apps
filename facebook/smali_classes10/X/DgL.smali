.class public final LX/DgL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V
    .locals 0

    .prologue
    .line 2029330
    iput-object p1, p0, LX/DgL;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x32b36f6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2029331
    iget-object v1, p0, LX/DgL;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    .line 2029332
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class p1, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-direct {v3, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2029333
    const-string p0, "extra_permissions"

    sget-object p1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r:[Ljava/lang/String;

    invoke-virtual {v3, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2029334
    iget-object p0, v1, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->o:Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0x676

    invoke-interface {p0, v3, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2029335
    const v1, 0x495b4678    # 898151.5f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
