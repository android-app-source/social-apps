.class public final LX/Ent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Enz;


# direct methods
.method public constructor <init>(LX/Enz;)V
    .locals 0

    .prologue
    .line 2167532
    iput-object p1, p0, LX/Ent;->a:LX/Enz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2167530
    const-class v0, LX/Enz;

    const-string v1, "subscription holder failure"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2167531
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2167526
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2167527
    if-eqz p1, :cond_0

    .line 2167528
    iget-object v0, p0, LX/Ent;->a:LX/Enz;

    iget-object v0, v0, LX/Enz;->r:LX/Enh;

    iget-object v1, p0, LX/Ent;->a:LX/Enz;

    invoke-virtual {v1}, LX/Enz;->j()LX/Eny;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/Enh;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/Eny;)V

    .line 2167529
    :cond_0
    return-void
.end method
