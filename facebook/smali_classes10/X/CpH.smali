.class public final LX/CpH;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;I)V
    .locals 0

    .prologue
    .line 1936845
    iput-object p1, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iput p2, p0, LX/CpH;->a:I

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 6
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1936846
    check-cast p2, LX/1ln;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1936847
    invoke-super {p0, p1, p2, p3}, LX/1cC;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    .line 1936848
    if-nez p2, :cond_0

    .line 1936849
    :goto_0
    return-void

    .line 1936850
    :cond_0
    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/CpH;->a:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1936851
    iget-object v3, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-virtual {v3}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 1936852
    int-to-float v2, v2

    div-float v2, v3, v2

    .line 1936853
    iget-object v4, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v4, v4, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->g:LX/8bK;

    invoke-virtual {v4}, LX/8bK;->a()LX/0p3;

    move-result-object v4

    .line 1936854
    sget-object v5, LX/0p3;->POOR:LX/0p3;

    if-ne v4, v5, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    iget-object v3, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v3, v3, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->h:LX/0Uh;

    const/16 v4, 0xb2

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v0

    .line 1936855
    :cond_1
    if-eqz v1, :cond_2

    const/4 v0, 0x2

    .line 1936856
    :cond_2
    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    int-to-float v3, v0

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 1936857
    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1936858
    iget-object v2, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1936859
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1936860
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1936861
    iget-object v0, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1936862
    invoke-super {p0, p1, p2}, LX/1cC;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1936863
    iget-object v0, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->e:LX/03V;

    if-eqz v0, :cond_0

    .line 1936864
    iget-object v0, p0, LX/CpH;->b:Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->e:LX/03V;

    const-string v2, "instant_articles"

    const-string v3, "IA unable to load logo: %s"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1936865
    :cond_0
    return-void

    .line 1936866
    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method
