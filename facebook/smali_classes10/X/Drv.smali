.class public LX/Drv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drt;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/Drv;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds0;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds2;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds3;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds5;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds6;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ds7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2049741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049742
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049743
    iput-object v0, p0, LX/Drv;->a:LX/0Ot;

    .line 2049744
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049745
    iput-object v0, p0, LX/Drv;->b:LX/0Ot;

    .line 2049746
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049747
    iput-object v0, p0, LX/Drv;->c:LX/0Ot;

    .line 2049748
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049749
    iput-object v0, p0, LX/Drv;->d:LX/0Ot;

    .line 2049750
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049751
    iput-object v0, p0, LX/Drv;->e:LX/0Ot;

    .line 2049752
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2049753
    iput-object v0, p0, LX/Drv;->f:LX/0Ot;

    .line 2049754
    return-void
.end method

.method public static a(LX/0QB;)LX/Drv;
    .locals 9

    .prologue
    .line 2049755
    sget-object v0, LX/Drv;->g:LX/Drv;

    if-nez v0, :cond_1

    .line 2049756
    const-class v1, LX/Drv;

    monitor-enter v1

    .line 2049757
    :try_start_0
    sget-object v0, LX/Drv;->g:LX/Drv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049758
    if-eqz v2, :cond_0

    .line 2049759
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049760
    new-instance v3, LX/Drv;

    invoke-direct {v3}, LX/Drv;-><init>()V

    .line 2049761
    const/16 v4, 0x2b09

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2b0a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2b0b

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2b0c

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2b0d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x2b0e

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2049762
    iput-object v4, v3, LX/Drv;->a:LX/0Ot;

    iput-object v5, v3, LX/Drv;->b:LX/0Ot;

    iput-object v6, v3, LX/Drv;->c:LX/0Ot;

    iput-object v7, v3, LX/Drv;->d:LX/0Ot;

    iput-object v8, v3, LX/Drv;->e:LX/0Ot;

    iput-object p0, v3, LX/Drv;->f:LX/0Ot;

    .line 2049763
    move-object v0, v3

    .line 2049764
    sput-object v0, LX/Drv;->g:LX/Drv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049765
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049766
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049767
    :cond_1
    sget-object v0, LX/Drv;->g:LX/Drv;

    return-object v0

    .line 2049768
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049769
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2049733
    sget-object v0, LX/Dru;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2049734
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2049735
    :pswitch_0
    iget-object v0, p0, LX/Drv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2049736
    :pswitch_1
    iget-object v0, p0, LX/Drv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2049737
    :pswitch_2
    iget-object v0, p0, LX/Drv;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2049738
    :pswitch_3
    iget-object v0, p0, LX/Drv;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2049739
    :pswitch_4
    iget-object v0, p0, LX/Drv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    .line 2049740
    :pswitch_5
    iget-object v0, p0, LX/Drv;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drs;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
