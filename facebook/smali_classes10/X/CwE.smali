.class public LX/CwE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CwB;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/Boolean;

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/search/model/ReactionSearchData;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:LX/103;


# direct methods
.method public constructor <init>(LX/CwD;)V
    .locals 1

    .prologue
    .line 1950119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950120
    iget-object v0, p1, LX/CwD;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950121
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CwE;->a:Ljava/lang/String;

    .line 1950122
    iget-object v0, p1, LX/CwD;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1950123
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CwE;->b:Ljava/lang/String;

    .line 1950124
    iget-object v0, p1, LX/CwD;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1950125
    iput-object v0, p0, LX/CwE;->c:Ljava/lang/String;

    .line 1950126
    iget-object v0, p1, LX/CwD;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1950127
    iput-object v0, p0, LX/CwE;->d:Ljava/lang/String;

    .line 1950128
    iget-object v0, p1, LX/CwD;->e:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1950129
    iput-object v0, p0, LX/CwE;->e:Ljava/lang/Boolean;

    .line 1950130
    iget-object v0, p1, LX/CwD;->f:LX/0Px;

    move-object v0, v0

    .line 1950131
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/CwE;->f:LX/0Px;

    .line 1950132
    iget-object v0, p1, LX/CwD;->g:LX/0P1;

    move-object v0, v0

    .line 1950133
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, LX/CwE;->g:LX/0P1;

    .line 1950134
    iget-object v0, p0, LX/CwE;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1950135
    iget-object v0, p1, LX/CwD;->h:Lcom/facebook/search/model/ReactionSearchData;

    move-object v0, v0

    .line 1950136
    iput-object v0, p0, LX/CwE;->h:Lcom/facebook/search/model/ReactionSearchData;

    .line 1950137
    iget-object v0, p1, LX/CwD;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1950138
    iput-object v0, p0, LX/CwE;->i:Ljava/lang/String;

    .line 1950139
    iget-object v0, p1, LX/CwD;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1950140
    iput-object v0, p0, LX/CwE;->j:Ljava/lang/String;

    .line 1950141
    iget-object v0, p1, LX/CwD;->k:LX/103;

    move-object v0, v0

    .line 1950142
    iput-object v0, p0, LX/CwE;->k:LX/103;

    .line 1950143
    return-void

    .line 1950144
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950154
    iget-object v0, p0, LX/CwE;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950155
    iget-object v0, p0, LX/CwE;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950160
    iget-object v0, p0, LX/CwE;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1950156
    iget-object v0, p0, LX/CwE;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950157
    instance-of v0, p1, LX/CwE;

    if-nez v0, :cond_0

    .line 1950158
    const/4 v0, 0x0

    .line 1950159
    :goto_0
    return v0

    :cond_0
    check-cast p1, LX/CwE;

    iget-object v0, p1, LX/CwE;->a:Ljava/lang/String;

    iget-object v1, p0, LX/CwE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final h()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950152
    iget-object v0, p0, LX/CwE;->g:LX/0P1;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950153
    iget-object v0, p0, LX/CwE;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950151
    iget-object v0, p0, LX/CwE;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950150
    iget-object v0, p0, LX/CwE;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final jA_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950149
    iget-object v0, p0, LX/CwE;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final jB_()LX/CwF;
    .locals 1

    .prologue
    .line 1950148
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    return-object v0
.end method

.method public final jC_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950147
    iget-object v0, p0, LX/CwE;->f:LX/0Px;

    return-object v0
.end method

.method public final k()LX/103;
    .locals 1

    .prologue
    .line 1950146
    iget-object v0, p0, LX/CwE;->k:LX/103;

    return-object v0
.end method

.method public final n()Lcom/facebook/search/model/ReactionSearchData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950145
    iget-object v0, p0, LX/CwE;->h:Lcom/facebook/search/model/ReactionSearchData;

    return-object v0
.end method
