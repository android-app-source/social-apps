.class public LX/D6B;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0SG;

.field public c:LX/D5X;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
    .end annotation
.end field

.field public final g:Z

.field public h:Z

.field public final i:LX/0g8;

.field public final j:LX/1Qq;

.field public final k:LX/D6U;

.field public final l:Ljava/util/concurrent/Executor;

.field private final m:LX/D59;

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field private final r:I

.field public s:I

.field public t:J

.field public u:LX/D6A;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/1Zp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1965199
    const-class v0, LX/D6B;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D6B;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0g8;LX/1Qq;LX/D5X;LX/D59;LX/0sV;LX/0SG;LX/0hB;Ljava/util/concurrent/Executor;LX/D6U;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1Qq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/D5X;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/D59;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965201
    iput-object p1, p0, LX/D6B;->d:Ljava/lang/String;

    .line 1965202
    iput-object p2, p0, LX/D6B;->e:Ljava/lang/String;

    .line 1965203
    iput-object p3, p0, LX/D6B;->f:Ljava/lang/String;

    .line 1965204
    iput-boolean p4, p0, LX/D6B;->g:Z

    .line 1965205
    iput-object p5, p0, LX/D6B;->i:LX/0g8;

    .line 1965206
    iput-object p6, p0, LX/D6B;->j:LX/1Qq;

    .line 1965207
    iput-object p7, p0, LX/D6B;->c:LX/D5X;

    .line 1965208
    iput-object p8, p0, LX/D6B;->m:LX/D59;

    .line 1965209
    iput-object p10, p0, LX/D6B;->b:LX/0SG;

    .line 1965210
    iput-object p12, p0, LX/D6B;->l:Ljava/util/concurrent/Executor;

    .line 1965211
    iput-object p13, p0, LX/D6B;->k:LX/D6U;

    .line 1965212
    iget v0, p9, LX/0sV;->b:I

    iput v0, p0, LX/D6B;->n:I

    .line 1965213
    iget v0, p9, LX/0sV;->c:I

    iput v0, p0, LX/D6B;->o:I

    .line 1965214
    iget v0, p9, LX/0sV;->d:I

    iput v0, p0, LX/D6B;->p:I

    .line 1965215
    iget v0, p9, LX/0sV;->e:I

    iput v0, p0, LX/D6B;->q:I

    .line 1965216
    invoke-virtual {p11}, LX/0hB;->d()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/D6B;->r:I

    .line 1965217
    const/4 v0, 0x0

    iput v0, p0, LX/D6B;->s:I

    .line 1965218
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/D6B;->t:J

    .line 1965219
    return-void
.end method

.method public static a$redex0(LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;)V
    .locals 5

    .prologue
    .line 1965220
    invoke-virtual {p1}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/D6B;->d:Ljava/lang/String;

    .line 1965221
    invoke-virtual {p1}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1965222
    iget-object v0, p0, LX/D6B;->c:LX/D5X;

    if-eqz v0, :cond_0

    .line 1965223
    iget-object v0, p0, LX/D6B;->c:LX/D5X;

    invoke-interface {v0, p0, p1}, LX/D5X;->a(LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;)V

    .line 1965224
    :cond_0
    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    iget-object v1, p0, LX/D6B;->i:LX/0g8;

    invoke-interface {v1}, LX/0g8;->h()I

    move-result v1

    iget-object v2, p0, LX/D6B;->i:LX/0g8;

    invoke-interface {v2}, LX/0g8;->g()I

    move-result v2

    iget-object v3, p0, LX/D6B;->i:LX/0g8;

    invoke-interface {v3}, LX/0g8;->i()I

    move-result v3

    iget v4, p0, LX/D6B;->r:I

    invoke-interface {v0, v1, v2, v3, v4}, LX/0g8;->a(IIII)V

    .line 1965225
    return-void
.end method

.method public static b(LX/D6B;I)I
    .locals 2

    .prologue
    .line 1965226
    iget-boolean v0, p0, LX/D6B;->h:Z

    if-nez v0, :cond_0

    .line 1965227
    const/4 v0, 0x5

    .line 1965228
    :goto_0
    return v0

    .line 1965229
    :cond_0
    iget v0, p0, LX/D6B;->p:I

    .line 1965230
    iget-object v1, p0, LX/D6B;->j:LX/1Qq;

    if-eqz v1, :cond_1

    .line 1965231
    iget-object v1, p0, LX/D6B;->j:LX/1Qq;

    invoke-interface {v1}, LX/1Qr;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1965232
    :cond_1
    if-gtz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/D6B;Z)V
    .locals 2

    .prologue
    .line 1965233
    const/4 v0, 0x0

    iput-object v0, p0, LX/D6B;->v:LX/1Zp;

    .line 1965234
    invoke-static {p0}, LX/D6B;->f(LX/D6B;)V

    .line 1965235
    if-eqz p1, :cond_0

    .line 1965236
    const/4 v0, 0x0

    iput v0, p0, LX/D6B;->s:I

    .line 1965237
    :goto_0
    return-void

    .line 1965238
    :cond_0
    iget v0, p0, LX/D6B;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/D6B;->s:I

    .line 1965239
    iget-object v0, p0, LX/D6B;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/D6B;->t:J

    goto :goto_0
.end method

.method public static e(LX/D6B;)V
    .locals 2

    .prologue
    .line 1965240
    iget-object v0, p0, LX/D6B;->m:LX/D59;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    invoke-interface {v0}, LX/0g8;->u()I

    move-result v0

    if-nez v0, :cond_0

    .line 1965241
    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    iget-object v1, p0, LX/D6B;->m:LX/D59;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 1965242
    :cond_0
    return-void
.end method

.method public static f(LX/D6B;)V
    .locals 2

    .prologue
    .line 1965243
    iget-object v0, p0, LX/D6B;->m:LX/D59;

    if-eqz v0, :cond_0

    .line 1965244
    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    iget-object v1, p0, LX/D6B;->m:LX/D59;

    invoke-interface {v0, v1}, LX/0g8;->b(Landroid/view/View;)V

    .line 1965245
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1965246
    if-eqz p1, :cond_2

    .line 1965247
    iget-object v0, p0, LX/D6B;->u:LX/D6A;

    if-nez v0, :cond_0

    .line 1965248
    new-instance v0, LX/D6A;

    invoke-direct {v0, p0}, LX/D6A;-><init>(LX/D6B;)V

    iput-object v0, p0, LX/D6B;->u:LX/D6A;

    .line 1965249
    :cond_0
    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    iget-object v1, p0, LX/D6B;->u:LX/D6A;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 1965250
    :cond_1
    :goto_0
    iput-boolean p1, p0, LX/D6B;->h:Z

    .line 1965251
    return-void

    .line 1965252
    :cond_2
    iget-object v0, p0, LX/D6B;->u:LX/D6A;

    if-eqz v0, :cond_1

    .line 1965253
    iget-object v0, p0, LX/D6B;->i:LX/0g8;

    iget-object v1, p0, LX/D6B;->u:LX/D6A;

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 9

    .prologue
    .line 1965254
    if-lez p1, :cond_0

    iget-object v0, p0, LX/D6B;->v:LX/1Zp;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1965255
    :cond_0
    const/4 v0, 0x0

    .line 1965256
    :goto_0
    return v0

    .line 1965257
    :cond_1
    invoke-static {p0}, LX/D6B;->e(LX/D6B;)V

    .line 1965258
    iget-object v0, p0, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    if-nez v0, :cond_3

    const/4 v3, 0x0

    .line 1965259
    :goto_1
    iget-object v0, p0, LX/D6B;->k:LX/D6U;

    iget-object v1, p0, LX/D6B;->d:Ljava/lang/String;

    iget-object v2, p0, LX/D6B;->e:Ljava/lang/String;

    iget-boolean v5, p0, LX/D6B;->g:Z

    move v4, p1

    .line 1965260
    const-string v6, "videoChannelId cannot be null"

    invoke-static {v1, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1965261
    new-instance v6, LX/7IP;

    invoke-direct {v6}, LX/7IP;-><init>()V

    move-object v6, v6

    .line 1965262
    invoke-static {v0, v6}, LX/D6U;->a(LX/D6U;LX/0gW;)LX/0gW;

    move-result-object v7

    const-string v8, "video_channel_id"

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "num_stories"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v7, v8, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "exclude_video_id"

    invoke-virtual {v7, v8, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965263
    if-eqz v3, :cond_2

    .line 1965264
    const-string v7, "after"

    invoke-virtual {v6, v7, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965265
    :cond_2
    invoke-static {v0, v6, v5}, LX/D6U;->a(LX/D6U;LX/0gW;Z)LX/1Zp;

    move-result-object v6

    move-object v0, v6

    .line 1965266
    new-instance v1, LX/D69;

    invoke-direct {v1, p0, v0}, LX/D69;-><init>(LX/D6B;LX/1Zp;)V

    iget-object v2, p0, LX/D6B;->l:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1965267
    iput-object v0, p0, LX/D6B;->v:LX/1Zp;

    .line 1965268
    const/4 v0, 0x1

    goto :goto_0

    .line 1965269
    :cond_3
    iget-object v0, p0, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public final b()Z
    .locals 10

    .prologue
    .line 1965270
    iget-object v0, p0, LX/D6B;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D6B;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1965271
    const/4 v0, 0x0

    .line 1965272
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D6B;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/D6B;->e:Ljava/lang/String;

    const/4 v1, 0x0

    .line 1965273
    iget-object v2, p0, LX/D6B;->v:LX/1Zp;

    if-eqz v2, :cond_4

    .line 1965274
    :cond_1
    :goto_1
    move v0, v1

    .line 1965275
    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/D6B;->d:Ljava/lang/String;

    iget-object v1, p0, LX/D6B;->e:Ljava/lang/String;

    const/4 v2, 0x0

    .line 1965276
    iget-object v3, p0, LX/D6B;->v:LX/1Zp;

    if-eqz v3, :cond_5

    .line 1965277
    :cond_3
    :goto_2
    move v0, v2

    .line 1965278
    goto :goto_0

    .line 1965279
    :cond_4
    iget v2, p0, LX/D6B;->n:I

    invoke-static {p0, v2}, LX/D6B;->b(LX/D6B;I)I

    move-result v2

    .line 1965280
    if-lez v2, :cond_1

    .line 1965281
    invoke-static {p0}, LX/D6B;->e(LX/D6B;)V

    .line 1965282
    iget-object v1, p0, LX/D6B;->k:LX/D6U;

    iget-object v3, p0, LX/D6B;->f:Ljava/lang/String;

    iget-boolean v4, p0, LX/D6B;->g:Z

    .line 1965283
    new-instance v5, LX/7IO;

    invoke-direct {v5}, LX/7IO;-><init>()V

    move-object v5, v5

    .line 1965284
    invoke-static {v1, v5}, LX/D6U;->a(LX/D6U;LX/0gW;)LX/0gW;

    move-result-object v6

    const-string v7, "video_id"

    invoke-virtual {v6, v7, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "video_channel_entry_point"

    invoke-virtual {v6, v7, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "num_stories"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "exclude_video_id"

    invoke-virtual {v6, v7, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965285
    invoke-static {v1, v5, v4}, LX/D6U;->a(LX/D6U;LX/0gW;Z)LX/1Zp;

    move-result-object v5

    move-object v1, v5

    .line 1965286
    new-instance v2, LX/D68;

    invoke-direct {v2, p0, v1}, LX/D68;-><init>(LX/D6B;LX/1Zp;)V

    iget-object v3, p0, LX/D6B;->l:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1965287
    iput-object v1, p0, LX/D6B;->v:LX/1Zp;

    .line 1965288
    const/4 v1, 0x1

    goto :goto_1

    .line 1965289
    :cond_5
    iget v3, p0, LX/D6B;->n:I

    invoke-static {p0, v3}, LX/D6B;->b(LX/D6B;I)I

    move-result v5

    .line 1965290
    if-lez v5, :cond_3

    .line 1965291
    invoke-static {p0}, LX/D6B;->e(LX/D6B;)V

    .line 1965292
    iget-object v2, p0, LX/D6B;->k:LX/D6U;

    iget-object v6, p0, LX/D6B;->f:Ljava/lang/String;

    iget-boolean v7, p0, LX/D6B;->g:Z

    move-object v3, v0

    move-object v4, v1

    .line 1965293
    const-string v8, "videoChannelId cannot be null"

    invoke-static {v3, v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1965294
    new-instance v8, LX/7IN;

    invoke-direct {v8}, LX/7IN;-><init>()V

    move-object v8, v8

    .line 1965295
    invoke-static {v2, v8}, LX/D6U;->a(LX/D6U;LX/0gW;)LX/0gW;

    move-result-object v9

    const-string v0, "video_channel_id"

    invoke-virtual {v9, v0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v0, "video_channel_entry_point"

    invoke-virtual {v9, v0, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v0, "num_stories"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1965296
    if-eqz v4, :cond_6

    .line 1965297
    const-string v9, "exclude_video_id"

    invoke-virtual {v8, v9, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965298
    :cond_6
    invoke-static {v2, v8, v7}, LX/D6U;->a(LX/D6U;LX/0gW;Z)LX/1Zp;

    move-result-object v8

    move-object v2, v8

    .line 1965299
    new-instance v3, LX/D67;

    invoke-direct {v3, p0, v2}, LX/D67;-><init>(LX/D6B;LX/1Zp;)V

    iget-object v4, p0, LX/D6B;->l:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1965300
    iput-object v2, p0, LX/D6B;->v:LX/1Zp;

    .line 1965301
    const/4 v2, 0x1

    goto/16 :goto_2
.end method
