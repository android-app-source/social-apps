.class public final LX/D5v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/D5z;


# direct methods
.method public constructor <init>(LX/D5z;)V
    .locals 0

    .prologue
    .line 1964817
    iput-object p1, p0, LX/D5v;->a:LX/D5z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6efeacb0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1964818
    iget-object v1, p0, LX/D5v;->a:LX/D5z;

    invoke-virtual {v1}, LX/D5z;->e()V

    .line 1964819
    iget-object v1, p0, LX/D5v;->a:LX/D5z;

    const/4 v2, 0x0

    .line 1964820
    iget-object v4, v1, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964821
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1964822
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1964823
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1964824
    :cond_0
    :goto_0
    const v1, 0x638535ad

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1964825
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    .line 1964826
    new-instance v4, LX/8qL;

    invoke-direct {v4}, LX/8qL;-><init>()V

    .line 1964827
    iput-object v7, v4, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1964828
    move-object v4, v4

    .line 1964829
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    .line 1964830
    iput-object v5, v4, LX/8qL;->d:Ljava/lang/String;

    .line 1964831
    move-object v4, v4

    .line 1964832
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    .line 1964833
    iput-object v5, v4, LX/8qL;->e:Ljava/lang/String;

    .line 1964834
    move-object v4, v4

    .line 1964835
    invoke-static {v7}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v5

    .line 1964836
    iput-boolean v5, v4, LX/8qL;->p:Z

    .line 1964837
    move-object v4, v4

    .line 1964838
    new-instance v5, LX/21A;

    invoke-direct {v5}, LX/21A;-><init>()V

    const-string v6, "video"

    .line 1964839
    iput-object v6, v5, LX/21A;->c:Ljava/lang/String;

    .line 1964840
    move-object v5, v5

    .line 1964841
    iget-object v6, v1, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v6}, LX/D5z;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 1964842
    iput-object v6, v5, LX/21A;->a:LX/162;

    .line 1964843
    move-object v5, v5

    .line 1964844
    sget-object v6, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    .line 1964845
    iput-object v6, v5, LX/21A;->i:LX/21D;

    .line 1964846
    move-object v5, v5

    .line 1964847
    invoke-virtual {v5}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v5

    .line 1964848
    iput-object v5, v4, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1964849
    move-object v4, v4

    .line 1964850
    iput-boolean v2, v4, LX/8qL;->i:Z

    .line 1964851
    move-object v5, v4

    .line 1964852
    iget-object v4, v1, LX/D5z;->i:LX/9Do;

    .line 1964853
    iget-object v6, v4, LX/9Do;->c:Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v6, :cond_2

    .line 1964854
    const/4 v6, 0x0

    .line 1964855
    :goto_1
    move-object v6, v6

    .line 1964856
    iget-object v4, v1, LX/D5z;->B:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1nI;

    invoke-virtual {v1}, LX/D5z;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v5}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v5

    invoke-interface {v4, v8, v5, v6}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Ljava/lang/String;)V

    .line 1964857
    iget-object v10, v1, LX/D5z;->m:LX/0Zb;

    iget-object v4, v1, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, LX/D5z;->A:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v8}, LX/D5z;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v8

    const-string v9, "video_channel_feed"

    invoke-static/range {v4 .. v9}, LX/1EP;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v10, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    :cond_2
    iget-object v6, v4, LX/9Do;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method
