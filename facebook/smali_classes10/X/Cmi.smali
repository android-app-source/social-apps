.class public LX/Cmi;
.super LX/Cm9;
.source ""

# interfaces
.implements LX/Clq;
.implements LX/Clr;
.implements LX/Clu;
.implements LX/Cm1;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:LX/8Yr;

.field public final g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field private final h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field private final i:Z


# direct methods
.method public constructor <init>(LX/Cmh;)V
    .locals 1

    .prologue
    .line 1933575
    invoke-direct {p0, p1}, LX/Cm9;-><init>(LX/Cm8;)V

    .line 1933576
    iget-object v0, p1, LX/Cmh;->a:Ljava/lang/String;

    iput-object v0, p0, LX/Cmi;->a:Ljava/lang/String;

    .line 1933577
    iget-object v0, p1, LX/Cmh;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Cmi;->b:Ljava/lang/String;

    .line 1933578
    iget-object v0, p1, LX/Cmh;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Cmi;->c:Ljava/lang/String;

    .line 1933579
    iget v0, p1, LX/Cmh;->e:I

    iput v0, p0, LX/Cmi;->e:I

    .line 1933580
    iget v0, p1, LX/Cmh;->d:I

    iput v0, p0, LX/Cmi;->d:I

    .line 1933581
    iget-object v0, p1, LX/Cmh;->f:LX/8Yr;

    iput-object v0, p0, LX/Cmi;->f:LX/8Yr;

    .line 1933582
    iget-object v0, p1, LX/Cmh;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    iput-object v0, p0, LX/Cmi;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 1933583
    iget-object v0, p1, LX/Cmh;->h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    iput-object v0, p0, LX/Cmi;->h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 1933584
    iget-boolean v0, p1, LX/Cmh;->i:Z

    move v0, v0

    .line 1933585
    iput-boolean v0, p0, LX/Cmi;->i:Z

    .line 1933586
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933574
    iget-object v0, p0, LX/Cmi;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1933573
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1933572
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1933571
    iget v0, p0, LX/Cmi;->d:I

    return v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933570
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->WEBVIEW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final ja_()Z
    .locals 1

    .prologue
    .line 1933569
    const/4 v0, 0x1

    return v0
.end method

.method public final jb_()I
    .locals 2

    .prologue
    .line 1933587
    iget-object v0, p0, LX/Cmi;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x2710

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1933568
    iget v0, p0, LX/Cmi;->e:I

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 1933567
    const/4 v0, 0x0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933566
    iget-object v0, p0, LX/Cmi;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933565
    iget-object v0, p0, LX/Cmi;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933564
    iget-object v0, p0, LX/Cmi;->h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 1933563
    iget-boolean v0, p0, LX/Cmi;->i:Z

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933562
    iget-object v0, p0, LX/Cmi;->c:Ljava/lang/String;

    return-object v0
.end method
