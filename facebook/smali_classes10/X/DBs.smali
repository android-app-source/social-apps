.class public final LX/DBs;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/res/Resources;

.field public final synthetic b:LX/DBx;


# direct methods
.method public constructor <init>(LX/DBx;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1973501
    iput-object p1, p0, LX/DBs;->b:LX/DBx;

    iput-object p2, p0, LX/DBs;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1973502
    iget-object v0, p0, LX/DBs;->b:LX/DBx;

    iget-object v0, v0, LX/DBx;->e:LX/1b1;

    invoke-virtual {v0}, LX/1b1;->e()V

    .line 1973503
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1973504
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1973505
    iget-object v0, p0, LX/DBs;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1973506
    return-void
.end method
