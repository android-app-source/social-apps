.class public LX/CuI;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Ctn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Float;",
        ">;",
        "Lcom/facebook/richdocument/view/transition/motion/MediaTiltEventListener;",
        "LX/Ctn;"
    }
.end annotation


# static fields
.field private static final d:LX/0wT;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/0wd;

.field public final f:LX/0wd;

.field public final g:Landroid/graphics/Paint;

.field private final h:Z

.field private i:LX/CuH;

.field public j:LX/CuG;

.field public k:F

.field private l:F

.field public m:LX/Cqa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1946390
    sget-wide v0, LX/CoL;->d:D

    sget-wide v2, LX/CoL;->e:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CuI;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1946368
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946369
    sget-object v0, LX/CuG;->INACTIVE:LX/CuG;

    iput-object v0, p0, LX/CuI;->j:LX/CuG;

    .line 1946370
    const-class v0, LX/CuI;

    invoke-static {v0, p0}, LX/CuI;->a(Ljava/lang/Class;LX/02k;)V

    .line 1946371
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    .line 1946372
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1946373
    new-instance v2, Landroid/graphics/Paint;

    const/4 p1, 0x1

    invoke-direct {v2, p1}, Landroid/graphics/Paint;-><init>(I)V

    .line 1946374
    const p1, 0x7f0a0616

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1946375
    sget-object p1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1946376
    const p1, 0x7f0b1275

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1946377
    move-object v1, v2

    .line 1946378
    iput-object v1, p0, LX/CuI;->g:Landroid/graphics/Paint;

    .line 1946379
    iget-object v1, p0, LX/CuI;->b:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/CuI;->d:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1946380
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 1946381
    move-object v1, v1

    .line 1946382
    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    new-instance v2, LX/CuD;

    invoke-direct {v2, p0, v0}, LX/CuD;-><init>(LX/CuI;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/CuI;->f:LX/0wd;

    .line 1946383
    iget-object v1, p0, LX/CuI;->b:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/CuI;->d:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1946384
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 1946385
    move-object v1, v1

    .line 1946386
    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    new-instance v2, LX/CuE;

    invoke-direct {v2, p0, v0}, LX/CuE;-><init>(LX/CuI;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/CuI;->e:LX/0wd;

    .line 1946387
    new-instance v0, LX/CuH;

    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/CuH;-><init>(LX/CuI;Landroid/content/Context;)V

    iput-object v0, p0, LX/CuI;->i:LX/CuH;

    .line 1946388
    iget-object v0, p0, LX/CuI;->c:LX/0ad;

    sget-short v1, LX/2yD;->j:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CuI;->h:Z

    .line 1946389
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CuI;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, LX/CuI;->a:LX/Chv;

    iput-object v2, p1, LX/CuI;->b:LX/0wW;

    iput-object p0, p1, LX/CuI;->c:LX/0ad;

    return-void
.end method

.method private l()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 1946327
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    .line 1946328
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v1

    .line 1946329
    iget-object v2, v1, LX/Cqw;->e:LX/Cqu;

    move-object v1, v2

    .line 1946330
    sget-object v2, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v1, v2, :cond_8

    iget-object v1, p0, LX/CuI;->m:LX/Cqa;

    sget-object v2, LX/Cqa;->HIDDEN:LX/Cqa;

    if-ne v1, v2, :cond_8

    const/4 v1, 0x0

    .line 1946331
    iget-object v2, p0, LX/Cts;->a:LX/Ctg;

    move-object v2, v2

    .line 1946332
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    sget-object v6, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v2, v6}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v2

    .line 1946333
    if-eqz v2, :cond_0

    .line 1946334
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v6

    invoke-static {v2, v6}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v6

    .line 1946335
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v7

    invoke-static {v2, v7}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v2

    .line 1946336
    invoke-virtual {v6}, LX/CrW;->e()I

    move-result v6

    invoke-virtual {v2}, LX/CrW;->e()I

    move-result v2

    if-le v6, v2, :cond_0

    const/4 v1, 0x1

    .line 1946337
    :cond_0
    move v1, v1

    .line 1946338
    if-eqz v1, :cond_8

    invoke-static {p0}, LX/CuI;->m(LX/CuI;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/richdocument/view/widget/SlideshowView;

    if-nez v1, :cond_8

    :cond_1
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1946339
    if-eqz v1, :cond_5

    .line 1946340
    invoke-static {p0}, LX/CuI;->m(LX/CuI;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1946341
    sget-object v1, LX/CuG;->SENSOR:LX/CuG;

    iput-object v1, p0, LX/CuI;->j:LX/CuG;

    .line 1946342
    :goto_1
    iget-object v1, p0, LX/CuI;->j:LX/CuG;

    if-eq v1, v0, :cond_3

    .line 1946343
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    sget-object v1, LX/CuG;->SENSOR:LX/CuG;

    if-ne v0, v1, :cond_6

    .line 1946344
    iget-object v0, p0, LX/CuI;->a:LX/Chv;

    new-instance v1, LX/CiI;

    sget-object v2, LX/CiH;->REGISTER:LX/CiH;

    invoke-direct {v1, p0, v2}, LX/CiI;-><init>(LX/CuI;LX/CiH;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1946345
    :goto_2
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    invoke-virtual {v0}, LX/CuG;->isActive()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1946346
    iget v0, p0, LX/CuI;->k:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    .line 1946347
    iget-object v0, p0, LX/CuI;->e:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1946348
    :cond_2
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v6

    .line 1946349
    iget-object v7, v6, LX/Cqw;->e:LX/Cqu;

    move-object v7, v7

    .line 1946350
    sget-object v8, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v7, v8, :cond_3

    .line 1946351
    iget v7, v6, LX/Cqw;->g:F

    move v6, v7

    .line 1946352
    float-to-double v6, v6

    sget-wide v8, LX/CoL;->g:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_3

    .line 1946353
    iget-object v6, p0, LX/CuI;->f:LX/0wd;

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, LX/0wd;->a(D)LX/0wd;

    .line 1946354
    iget-object v6, p0, LX/CuI;->f:LX/0wd;

    sget-wide v8, LX/CoL;->f:D

    invoke-virtual {v6, v8, v9}, LX/0wd;->b(D)LX/0wd;

    .line 1946355
    :cond_3
    :goto_3
    return-void

    .line 1946356
    :cond_4
    sget-object v1, LX/CuG;->TOUCH:LX/CuG;

    iput-object v1, p0, LX/CuI;->j:LX/CuG;

    goto :goto_1

    .line 1946357
    :cond_5
    sget-object v1, LX/CuG;->INACTIVE:LX/CuG;

    iput-object v1, p0, LX/CuI;->j:LX/CuG;

    goto :goto_1

    .line 1946358
    :cond_6
    iget-object v0, p0, LX/CuI;->a:LX/Chv;

    new-instance v1, LX/CiI;

    sget-object v2, LX/CiH;->UNREGISTER:LX/CiH;

    invoke-direct {v1, p0, v2}, LX/CiI;-><init>(LX/CuI;LX/CiH;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_2

    .line 1946359
    :cond_7
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v0

    .line 1946360
    iget-object v1, p0, LX/Cts;->a:LX/Ctg;

    move-object v1, v1

    .line 1946361
    invoke-interface {v1}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    .line 1946362
    if-eqz v0, :cond_3

    .line 1946363
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1946364
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v2

    invoke-static {v0, v2}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1946365
    invoke-virtual {v1}, LX/CrW;->e()I

    move-result v1

    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget v0, p0, LX/CuI;->k:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_3

    .line 1946366
    iget-object v0, p0, LX/CuI;->e:LX/0wd;

    iget v1, p0, LX/CuI;->k:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1946367
    iget-object v0, p0, LX/CuI;->e:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static m(LX/CuI;)Z
    .locals 2

    .prologue
    .line 1946325
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1946326
    iget-boolean v1, p0, LX/CuI;->h:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Cqw;)V
    .locals 0

    .prologue
    .line 1946323
    invoke-direct {p0}, LX/CuI;->l()V

    .line 1946324
    return-void
.end method

.method public final a(LX/Ctr;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1946319
    instance-of v0, p1, LX/CuA;

    if-eqz v0, :cond_0

    .line 1946320
    check-cast p2, LX/Cqa;

    iput-object p2, p0, LX/CuI;->m:LX/Cqa;

    .line 1946321
    invoke-direct {p0}, LX/CuI;->l()V

    .line 1946322
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1946391
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    invoke-virtual {v0}, LX/CuG;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1946392
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1946393
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946394
    invoke-interface {v0}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v0

    .line 1946395
    if-eqz v0, :cond_1

    .line 1946396
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    invoke-static {v0, v1}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1946397
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v2}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1946398
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1946399
    invoke-virtual {v1}, LX/CrW;->e()I

    move-result v2

    .line 1946400
    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v0

    .line 1946401
    int-to-float v3, v2

    int-to-float v4, v0

    div-float/2addr v3, v4

    .line 1946402
    int-to-float v4, v2

    mul-float/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1946403
    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, LX/CuI;->k:F

    add-float/2addr v5, v6

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1946404
    const/high16 v2, -0x40800000    # -1.0f

    mul-float/2addr v2, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1946405
    iget-object v2, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v2, v2

    .line 1946406
    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, LX/CrW;->f()I

    move-result v1

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, LX/CuI;->g:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1946407
    int-to-float v1, v0

    int-to-float v2, v5

    add-int/2addr v0, v4

    int-to-float v3, v0

    int-to-float v4, v5

    iget-object v5, p0, LX/CuI;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1946408
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1946409
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Float;)V
    .locals 4

    .prologue
    .line 1946313
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    invoke-virtual {v0}, LX/CuG;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, LX/CuI;->k:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1946314
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, LX/CuI;->l:F

    .line 1946315
    iget-object v0, p0, LX/CuI;->e:LX/0wd;

    iget v1, p0, LX/CuI;->k:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1946316
    iget-object v0, p0, LX/CuI;->e:LX/0wd;

    iget v1, p0, LX/CuI;->l:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1946317
    invoke-virtual {p0, p1}, LX/Cts;->a(Ljava/lang/Object;)V

    .line 1946318
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1946312
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    sget-object v1, LX/CuG;->TOUCH:LX/CuG;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/CuI;->i:LX/CuH;

    invoke-virtual {v0, p1}, LX/CuH;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/CrS;)V
    .locals 0

    .prologue
    .line 1946309
    invoke-direct {p0}, LX/CuI;->l()V

    .line 1946310
    invoke-virtual {p0, p1}, LX/CuI;->c(LX/CrS;)V

    .line 1946311
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1946308
    iget-object v0, p0, LX/CuI;->j:LX/CuG;

    sget-object v1, LX/CuG;->TOUCH:LX/CuG;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/CuI;->i:LX/CuH;

    invoke-virtual {v0, p1}, LX/CuH;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1946305
    const/4 v0, 0x0

    iput v0, p0, LX/CuI;->k:F

    .line 1946306
    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    iput-object v0, p0, LX/CuI;->m:LX/Cqa;

    .line 1946307
    return-void
.end method

.method public final c(LX/CrS;)V
    .locals 5

    .prologue
    .line 1946296
    if-eqz p1, :cond_0

    .line 1946297
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1946298
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v1

    invoke-static {p1, v1}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1946299
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1946300
    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v0

    .line 1946301
    invoke-virtual {v1}, LX/CrW;->e()I

    move-result v2

    .line 1946302
    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, LX/CuI;->k:F

    add-float/2addr v3, v4

    sub-int/2addr v0, v2

    int-to-float v0, v0

    mul-float/2addr v0, v3

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1946303
    invoke-virtual {v1, v0}, LX/CrW;->b(I)V

    .line 1946304
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1946294
    invoke-direct {p0}, LX/CuI;->l()V

    .line 1946295
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1946292
    invoke-direct {p0}, LX/CuI;->l()V

    .line 1946293
    return-void
.end method
