.class public LX/E1l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2071273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E1l;->a:Ljava/util/List;

    .line 2071274
    return-void
.end method

.method public static a(LX/0QB;)LX/E1l;
    .locals 1

    .prologue
    .line 2071275
    new-instance v0, LX/E1l;

    invoke-direct {v0}, LX/E1l;-><init>()V

    .line 2071276
    move-object v0, v0

    .line 2071277
    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2071278
    invoke-virtual {p0, p1}, LX/E1l;->b(I)LX/Cfo;

    move-result-object v1

    .line 2071279
    instance-of v0, v1, LX/E2P;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, LX/E2P;

    .line 2071280
    iget-boolean p0, v0, LX/E2P;->a:Z

    move v0, p0

    .line 2071281
    if-eqz v0, :cond_1

    .line 2071282
    :cond_0
    :goto_0
    return-object v1

    .line 2071283
    :cond_1
    invoke-interface {v1}, LX/Cfo;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2071284
    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0
.end method

.method public final a(ILX/Cfo;)V
    .locals 1

    .prologue
    .line 2071285
    iget-object v0, p0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2071286
    return-void
.end method

.method public final b(I)LX/Cfo;
    .locals 1

    .prologue
    .line 2071287
    iget-object v0, p0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfo;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2071288
    iget-object v0, p0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
