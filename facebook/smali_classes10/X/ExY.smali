.class public final LX/ExY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V
    .locals 0

    .prologue
    .line 2184844
    iput-object p1, p0, LX/ExY;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2184845
    iget-object v0, p0, LX/ExY;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v0, p4}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(LX/2kM;)V

    .line 2184846
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2184847
    return-void
.end method

.method public final bridge synthetic a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2184848
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2184849
    iget-object v0, p0, LX/ExY;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2184850
    iget-object v0, p0, LX/ExY;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    sget-object v1, LX/Ey6;->LOADING_STATE_ERROR:LX/Ey6;

    invoke-static {v0, v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    .line 2184851
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2184852
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 2184853
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-ne v0, v1, :cond_0

    .line 2184854
    iget-object v0, p0, LX/ExY;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2184855
    :cond_0
    return-void
.end method
