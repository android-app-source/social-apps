.class public final LX/Dxy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 0

    .prologue
    .line 2063527
    iput-object p1, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, -0x5192424a

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2063528
    iget-object v0, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063529
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    const-class v3, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2063530
    const-string v0, "input_album"

    iget-object v3, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v3, v3, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-static {v2, v0, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 2063531
    const-string v0, "selected_album_privacy"

    iget-object v3, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v3, v3, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v0, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2063532
    iget-object v0, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-interface {v0, v2, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2063533
    :goto_0
    const v0, -0x19a1e957

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2063534
    :cond_0
    new-instance v0, LX/8Q3;

    iget-object v2, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-direct {v0, v2}, LX/8Q3;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v2, v2, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v3, v3, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063535
    new-instance v5, Landroid/content/Intent;

    iget-object v6, v0, LX/8Q3;->a:Landroid/content/Context;

    const-class p1, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;

    invoke-direct {v5, v6, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2063536
    new-instance v6, LX/8QC;

    invoke-direct {v6}, LX/8QC;-><init>()V

    .line 2063537
    iput-object v2, v6, LX/8QC;->d:Ljava/lang/String;

    .line 2063538
    move-object v6, v6

    .line 2063539
    sget-object p1, LX/8QD;->ALBUM:LX/8QD;

    .line 2063540
    iput-object p1, v6, LX/8QC;->b:LX/8QD;

    .line 2063541
    move-object v6, v6

    .line 2063542
    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 2063543
    iput-object p1, v6, LX/8QC;->a:Ljava/lang/Boolean;

    .line 2063544
    move-object v6, v6

    .line 2063545
    const-string p1, "params"

    invoke-virtual {v6}, LX/8QC;->a()Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2063546
    const-string v6, "initial_privacy"

    invoke-static {v5, v6, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2063547
    move-object v2, v5

    .line 2063548
    iget-object v0, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dxy;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-interface {v0, v2, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
