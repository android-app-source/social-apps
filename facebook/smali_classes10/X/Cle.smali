.class public LX/Cle;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

.field public b:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public d:Ljava/lang/CharSequence;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1932690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932691
    iput-object p1, p0, LX/Cle;->c:Landroid/content/Context;

    .line 1932692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cle;->e:Ljava/util/List;

    .line 1932693
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cle;->g:Ljava/util/List;

    .line 1932694
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cle;->f:Ljava/util/List;

    .line 1932695
    const-class v0, LX/Cle;

    invoke-static {v0, p0}, LX/Cle;->a(Ljava/lang/Class;LX/02k;)V

    .line 1932696
    iget-object v0, p0, LX/Cle;->b:LX/Chi;

    .line 1932697
    iget-object p1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, p1

    .line 1932698
    iput-object v0, p0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 1932699
    return-void
.end method

.method public static a(LX/Cle;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentText$EntityRanges;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1932707
    if-nez p2, :cond_1

    .line 1932708
    :cond_0
    return-void

    .line 1932709
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;

    .line 1932710
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->d()I

    move-result v2

    .line 1932711
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->c()I

    move-result v3

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1932712
    invoke-virtual {p0, v0}, LX/Cle;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;)LX/Cll;

    move-result-object v0

    .line 1932713
    if-eqz v0, :cond_2

    .line 1932714
    add-int/2addr v3, v2

    const/16 v4, 0x11

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public static a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V
    .locals 1

    .prologue
    .line 1932705
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    .line 1932706
    return-void
.end method

.method public static a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V
    .locals 1

    .prologue
    .line 1932700
    if-eqz p1, :cond_0

    .line 1932701
    if-eqz p2, :cond_1

    .line 1932702
    iget-object v0, p0, LX/Cle;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1932703
    :cond_0
    :goto_0
    return-void

    .line 1932704
    :cond_1
    iget-object v0, p0, LX/Cle;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(LX/Cle;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/Clb;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentInlineStyleRange;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentText$EntityRanges;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineText$Ranges;",
            ">;",
            "LX/Clb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1932673
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1932674
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1932675
    invoke-static {v0, p2}, LX/Cle;->c(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 1932676
    invoke-static {p0, v0, p3}, LX/Cle;->a(LX/Cle;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 1932677
    if-nez p4, :cond_2

    .line 1932678
    :cond_0
    iput-object v0, p0, LX/Cle;->d:Ljava/lang/CharSequence;

    .line 1932679
    :cond_1
    invoke-virtual {p0, p5}, LX/Cle;->a(LX/Clb;)LX/Cle;

    .line 1932680
    return-void

    .line 1932681
    :cond_2
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;

    .line 1932682
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;->c()I

    move-result v3

    .line 1932683
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;->b()I

    move-result p1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    sub-int/2addr p2, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 1932684
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->E()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p2

    sparse-switch p2, :sswitch_data_0

    .line 1932685
    const/4 p2, 0x0

    :goto_1
    move-object v1, p2

    .line 1932686
    if-eqz v1, :cond_3

    .line 1932687
    add-int/2addr p1, v3

    const/16 p2, 0x11

    invoke-virtual {v0, v1, v3, p1, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1932688
    :sswitch_0
    new-instance p2, LX/Clm;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object p3

    iget-object p4, p0, LX/Cle;->c:Landroid/content/Context;

    invoke-direct {p2, p3, p4}, LX/Clm;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    goto :goto_1

    .line 1932689
    :sswitch_1
    new-instance p2, LX/Cln;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel$RangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object p3

    iget-object p4, p0, LX/Cle;->c:Landroid/content/Context;

    invoke-direct {p2, p3, p4}, LX/Cln;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_1
        0x1eaef984 -> :sswitch_0
        0x5fcedbf5 -> :sswitch_0
    .end sparse-switch
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cle;

    invoke-static {p0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object p0

    check-cast p0, LX/Chi;

    iput-object p0, p1, LX/Cle;->b:LX/Chi;

    return-void
.end method

.method public static c(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentInlineStyleRange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1932663
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1932664
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentInlineStyleRangeModel;

    .line 1932665
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentInlineStyleRangeModel;->c()I

    move-result v2

    .line 1932666
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentInlineStyleRangeModel;->b()I

    move-result v3

    .line 1932667
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentInlineStyleRangeModel;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v0

    .line 1932668
    invoke-static {v0}, LX/8bQ;->a(Lcom/facebook/graphql/enums/GraphQLInlineStyle;)Ljava/lang/Object;

    move-result-object v0

    .line 1932669
    if-eqz v0, :cond_0

    .line 1932670
    add-int/2addr v3, v2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1932671
    const/16 v4, 0x11

    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1932672
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)LX/Cle;
    .locals 2

    .prologue
    .line 1932715
    iget-object v0, p0, LX/Cle;->g:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1932716
    return-object p0
.end method

.method public final a(LX/8Z4;)LX/Cle;
    .locals 6

    .prologue
    .line 1932658
    if-nez p1, :cond_0

    .line 1932659
    :goto_0
    return-object p0

    .line 1932660
    :cond_0
    invoke-interface {p1}, LX/8Z4;->c()LX/0Px;

    move-result-object v2

    .line 1932661
    invoke-interface {p1}, LX/8Z4;->b()LX/0Px;

    move-result-object v3

    .line 1932662
    invoke-interface {p1}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {p1}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    invoke-static {v0}, LX/Clb;->from(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)LX/Clb;

    move-result-object v5

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/Cle;->a(LX/Cle;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/Clb;)V

    goto :goto_0
.end method

.method public a(LX/Clb;)LX/Cle;
    .locals 4

    .prologue
    .line 1932616
    if-nez p1, :cond_1

    .line 1932617
    :cond_0
    :goto_0
    return-object p0

    .line 1932618
    :cond_1
    sget-object v0, LX/Clf;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1932619
    sget-object v0, LX/Clf;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/Cle;->a(I)LX/Cle;

    .line 1932620
    :cond_2
    iget-object v0, p0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    if-eqz v0, :cond_0

    .line 1932621
    iget-object v0, p0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;

    move-result-object v0

    const/4 v3, 0x1

    .line 1932622
    if-nez v0, :cond_3

    .line 1932623
    :goto_1
    iget-object v0, p0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 1932624
    if-nez v0, :cond_4

    .line 1932625
    :goto_2
    goto :goto_0

    .line 1932626
    :cond_3
    sget-object v1, LX/Cld;->a:[I

    invoke-virtual {p1}, LX/Clb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 1932627
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932628
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932629
    :pswitch_2
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->w()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932630
    :pswitch_3
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932631
    :pswitch_4
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932632
    :pswitch_5
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932633
    :pswitch_6
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932634
    :pswitch_7
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932635
    :pswitch_8
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932636
    :pswitch_9
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932637
    :pswitch_a
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932638
    :pswitch_b
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->er_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932639
    :pswitch_c
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932640
    :pswitch_d
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto :goto_1

    .line 1932641
    :pswitch_e
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1, v3}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto/16 :goto_1

    .line 1932642
    :cond_4
    sget-object v1, LX/Cld;->a:[I

    invoke-virtual {p1}, LX/Clb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_2

    .line 1932643
    :pswitch_f
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932644
    :pswitch_10
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932645
    :pswitch_11
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->B()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932646
    :pswitch_12
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932647
    :pswitch_13
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932648
    :pswitch_14
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932649
    :pswitch_15
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Z)V

    goto/16 :goto_2

    .line 1932650
    :pswitch_16
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->z()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932651
    :pswitch_17
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932652
    :pswitch_18
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->A()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932653
    :pswitch_19
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932654
    :pswitch_1a
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932655
    :pswitch_1b
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932656
    :pswitch_1c
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    .line 1932657
    :pswitch_1d
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    invoke-static {p0, v1}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public final a(LX/GoW;)LX/Cle;
    .locals 6

    .prologue
    .line 1932608
    if-nez p1, :cond_0

    .line 1932609
    :goto_0
    return-object p0

    .line 1932610
    :cond_0
    iget-object v0, p1, LX/GoW;->a:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1932611
    iget-object v0, p1, LX/GoW;->a:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->c()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 1932612
    iget-object v0, p1, LX/GoW;->a:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->b()LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 1932613
    const/4 v4, 0x0

    .line 1932614
    iget-object v0, p1, LX/GoW;->a:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    invoke-static {v0}, LX/Clb;->from(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)LX/Clb;

    move-result-object v0

    move-object v5, v0

    .line 1932615
    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/Cle;->a(LX/Cle;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/Clb;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;)LX/Cle;
    .locals 6

    .prologue
    .line 1932597
    if-nez p1, :cond_0

    .line 1932598
    :goto_0
    return-object p0

    .line 1932599
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;->a()LX/0Px;

    move-result-object v2

    .line 1932600
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;->b()LX/0Px;

    move-result-object v4

    .line 1932601
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    sget-object v5, LX/Clb;->BYLINE:LX/Clb;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/Cle;->a(LX/Cle;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/Clb;)V

    goto :goto_0
.end method

.method public final a()LX/Clf;
    .locals 5

    .prologue
    .line 1932607
    new-instance v0, LX/Clf;

    iget-object v1, p0, LX/Cle;->d:Ljava/lang/CharSequence;

    iget-object v2, p0, LX/Cle;->e:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/Cle;->f:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/Cle;->g:Ljava/util/List;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/Clf;-><init>(Ljava/lang/CharSequence;LX/0Px;LX/0Px;LX/0Px;)V

    return-object v0
.end method

.method public a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;)LX/Cll;
    .locals 3

    .prologue
    .line 1932603
    sget-object v0, LX/Cld;->b:[I

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->b()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932604
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1932605
    :pswitch_0
    new-instance v0, LX/Clm;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object v1

    iget-object v2, p0, LX/Cle;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, LX/Clm;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    goto :goto_0

    .line 1932606
    :pswitch_1
    new-instance v0, LX/Cln;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object v1

    iget-object v2, p0, LX/Cle;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, LX/Cln;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1932602
    iget-object v0, p0, LX/Cle;->c:Landroid/content/Context;

    return-object v0
.end method
