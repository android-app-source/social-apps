.class public LX/DoJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/DoO;

.field public final b:LX/DoL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2041632
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/DoJ;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/DoO;LX/DoP;LX/DoR;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2041628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041629
    iput-object p1, p0, LX/DoJ;->a:LX/DoO;

    .line 2041630
    new-instance v0, LX/DoL;

    invoke-direct {v0, p1, p2, p3}, LX/DoL;-><init>(LX/DoO;LX/DoP;LX/DoR;)V

    iput-object v0, p0, LX/DoJ;->b:LX/DoL;

    .line 2041631
    return-void
.end method

.method public static a(LX/DoK;[BLX/IuD;Z)LX/DoI;
    .locals 3

    .prologue
    .line 2041611
    check-cast p0, LX/DoT;

    .line 2041612
    new-instance v0, LX/Eao;

    .line 2041613
    iget-object v1, p0, LX/DoT;->m:LX/Eap;

    move-object v1, v1

    .line 2041614
    invoke-direct {v0, p0, v1}, LX/Eao;-><init>(LX/DoS;LX/Eap;)V

    .line 2041615
    invoke-virtual {v0, p1}, LX/Eao;->a([B)LX/Eb9;

    move-result-object v0

    .line 2041616
    invoke-virtual {p0}, LX/DoT;->e()V

    .line 2041617
    invoke-virtual {p2, p0, p3}, LX/IuD;->a(LX/DoK;Z)V

    .line 2041618
    new-instance v1, LX/DoI;

    instance-of v2, v0, LX/EbA;

    invoke-interface {v0}, LX/Eb9;->a()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/DoI;-><init>(Z[B)V

    return-object v1
.end method

.method public static a(LX/0QB;)LX/DoJ;
    .locals 9

    .prologue
    .line 2041633
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2041634
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2041635
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2041636
    if-nez v1, :cond_0

    .line 2041637
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2041638
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2041639
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2041640
    sget-object v1, LX/DoJ;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2041641
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2041642
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2041643
    :cond_1
    if-nez v1, :cond_4

    .line 2041644
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2041645
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2041646
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2041647
    new-instance p0, LX/DoJ;

    invoke-static {v0}, LX/Dp3;->b(LX/0QB;)LX/Dp3;

    move-result-object v1

    check-cast v1, LX/DoO;

    invoke-static {v0}, LX/IuC;->a(LX/0QB;)LX/IuC;

    move-result-object v7

    check-cast v7, LX/DoP;

    invoke-static {v0}, LX/IuC;->a(LX/0QB;)LX/IuC;

    move-result-object v8

    check-cast v8, LX/DoR;

    invoke-direct {p0, v1, v7, v8}, LX/DoJ;-><init>(LX/DoO;LX/DoP;LX/DoR;)V

    .line 2041648
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2041649
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2041650
    if-nez v1, :cond_2

    .line 2041651
    sget-object v0, LX/DoJ;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2041652
    :goto_1
    if-eqz v0, :cond_3

    .line 2041653
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2041654
    :goto_3
    check-cast v0, LX/DoJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2041655
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2041656
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2041657
    :catchall_1
    move-exception v0

    .line 2041658
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2041659
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2041660
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2041661
    :cond_2
    :try_start_8
    sget-object v0, LX/DoJ;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/DoK;LX/DoU;[B[BLX/IuD;Z)[B
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2041619
    move-object v0, p0

    check-cast v0, LX/DoT;

    .line 2041620
    new-instance v1, LX/Eao;

    new-instance v2, LX/Eap;

    invoke-virtual {v0}, LX/DoT;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v1, v0, v2}, LX/Eao;-><init>(LX/DoS;LX/Eap;)V

    .line 2041621
    new-instance v2, LX/EbA;

    invoke-direct {v2, p2}, LX/EbA;-><init>([B)V

    invoke-virtual {v1, v2}, LX/Eao;->a(LX/EbA;)[B

    move-result-object v3

    .line 2041622
    invoke-virtual {v0}, LX/DoT;->e()V

    .line 2041623
    sget-object v1, LX/DoN;->RUNNING:LX/DoN;

    .line 2041624
    iput-object v1, v0, LX/DoT;->g:LX/DoN;

    .line 2041625
    move-object v0, p4

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move v5, p5

    .line 2041626
    invoke-virtual/range {v0 .. v5}, LX/IuD;->a(LX/DoK;LX/DoU;[B[BZ)V

    .line 2041627
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;[BI[BI[B[BLX/IuD;Z)V
    .locals 12

    .prologue
    .line 2041603
    iget-object v2, p0, LX/DoJ;->b:LX/DoL;

    invoke-virtual {v2, p1}, LX/DoL;->a(Ljava/lang/String;)LX/DoT;

    move-result-object v11

    .line 2041604
    invoke-virtual {v11, p2}, LX/DoT;->a(Ljava/lang/String;)V

    .line 2041605
    new-instance v2, LX/Ebf;

    iget-object v3, p0, LX/DoJ;->a:LX/DoO;

    invoke-interface {v3}, LX/DoO;->b()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v5}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v6

    const/4 v5, 0x0

    move-object/from16 v0, p7

    invoke-static {v0, v5}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v8

    new-instance v10, LX/Eae;

    const/4 v5, 0x0

    invoke-direct {v10, p3, v5}, LX/Eae;-><init>([BI)V

    move/from16 v5, p4

    move/from16 v7, p6

    move-object/from16 v9, p8

    invoke-direct/range {v2 .. v10}, LX/Ebf;-><init>(IIILX/Eat;ILX/Eat;[BLX/Eae;)V

    .line 2041606
    new-instance v3, LX/Eam;

    new-instance v4, LX/Eap;

    const/4 v5, 0x0

    invoke-direct {v4, p1, v5}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v3, v11, v4}, LX/Eam;-><init>(LX/DoS;LX/Eap;)V

    .line 2041607
    invoke-virtual {v3, v2}, LX/Eam;->a(LX/Ebf;)V

    .line 2041608
    sget-object v2, LX/DoN;->RUNNING:LX/DoN;

    invoke-virtual {v11, v2}, LX/DoT;->a(LX/DoN;)V

    .line 2041609
    move-object/from16 v0, p9

    move/from16 v1, p10

    invoke-virtual {v0, v11, v1}, LX/IuD;->a(LX/DoK;Z)V

    .line 2041610
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/DoU;[B[BLX/IuD;Z)[B
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2041599
    iget-object v0, p0, LX/DoJ;->b:LX/DoL;

    invoke-virtual {v0, p1}, LX/DoL;->a(Ljava/lang/String;)LX/DoT;

    move-result-object v0

    .line 2041600
    iput-object p2, v0, LX/DoT;->f:Ljava/lang/String;

    .line 2041601
    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move v5, p7

    .line 2041602
    invoke-static/range {v0 .. v5}, LX/DoJ;->a(LX/DoK;LX/DoU;[B[BLX/IuD;Z)[B

    move-result-object v0

    return-object v0
.end method
