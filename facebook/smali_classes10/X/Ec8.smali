.class public final LX/Ec8;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ec6;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ec8;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ec8;


# instance fields
.field public bitField0_:I

.field public iteration_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public seed_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2145633
    new-instance v0, LX/Ec5;

    invoke-direct {v0}, LX/Ec5;-><init>()V

    sput-object v0, LX/Ec8;->a:LX/EWZ;

    .line 2145634
    new-instance v0, LX/Ec8;

    invoke-direct {v0}, LX/Ec8;-><init>()V

    .line 2145635
    sput-object v0, LX/Ec8;->c:LX/Ec8;

    invoke-direct {v0}, LX/Ec8;->r()V

    .line 2145636
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2145593
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145594
    iput-byte v0, p0, LX/Ec8;->memoizedIsInitialized:B

    .line 2145595
    iput v0, p0, LX/Ec8;->memoizedSerializedSize:I

    .line 2145596
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2145597
    iput-object v0, p0, LX/Ec8;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2145598
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145599
    iput-byte v0, p0, LX/Ec8;->memoizedIsInitialized:B

    .line 2145600
    iput v0, p0, LX/Ec8;->memoizedSerializedSize:I

    .line 2145601
    invoke-direct {p0}, LX/Ec8;->r()V

    .line 2145602
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2145603
    const/4 v0, 0x0

    .line 2145604
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2145605
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2145606
    sparse-switch v3, :sswitch_data_0

    .line 2145607
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2145608
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2145609
    goto :goto_0

    .line 2145610
    :sswitch_1
    iget v3, p0, LX/Ec8;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Ec8;->bitField0_:I

    .line 2145611
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/Ec8;->iteration_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2145612
    :catch_0
    move-exception v0

    .line 2145613
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145614
    move-object v0, v0

    .line 2145615
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145616
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ec8;->unknownFields:LX/EZQ;

    .line 2145617
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2145618
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Ec8;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Ec8;->bitField0_:I

    .line 2145619
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ec8;->seed_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2145620
    :catch_1
    move-exception v0

    .line 2145621
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2145622
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145623
    move-object v0, v1

    .line 2145624
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2145625
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ec8;->unknownFields:LX/EZQ;

    .line 2145626
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2145627
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2145628
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2145629
    iput-byte v1, p0, LX/Ec8;->memoizedIsInitialized:B

    .line 2145630
    iput v1, p0, LX/Ec8;->memoizedSerializedSize:I

    .line 2145631
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ec8;->unknownFields:LX/EZQ;

    .line 2145632
    return-void
.end method

.method public static a(LX/Ec8;)LX/Ec7;
    .locals 1

    .prologue
    .line 2145646
    invoke-static {}, LX/Ec7;->w()LX/Ec7;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 2145567
    const/4 v0, 0x0

    iput v0, p0, LX/Ec8;->iteration_:I

    .line 2145568
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ec8;->seed_:LX/EWc;

    .line 2145569
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2145637
    new-instance v0, LX/Ec7;

    invoke-direct {v0, p1}, LX/Ec7;-><init>(LX/EYd;)V

    .line 2145638
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2145639
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145640
    iget v0, p0, LX/Ec8;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2145641
    iget v0, p0, LX/Ec8;->iteration_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2145642
    :cond_0
    iget v0, p0, LX/Ec8;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2145643
    iget-object v0, p0, LX/Ec8;->seed_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2145644
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2145645
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145578
    iget-byte v1, p0, LX/Ec8;->memoizedIsInitialized:B

    .line 2145579
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145580
    :goto_0
    return v0

    .line 2145581
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145582
    :cond_1
    iput-byte v0, p0, LX/Ec8;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2145583
    iget v0, p0, LX/Ec8;->memoizedSerializedSize:I

    .line 2145584
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2145585
    :goto_0
    return v0

    .line 2145586
    :cond_0
    const/4 v0, 0x0

    .line 2145587
    iget v1, p0, LX/Ec8;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2145588
    iget v0, p0, LX/Ec8;->iteration_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2145589
    :cond_1
    iget v1, p0, LX/Ec8;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2145590
    iget-object v1, p0, LX/Ec8;->seed_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2145591
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2145592
    iput v0, p0, LX/Ec8;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145577
    iget-object v0, p0, LX/Ec8;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145576
    sget-object v0, LX/Eck;->x:LX/EYn;

    const-class v1, LX/Ec8;

    const-class v2, LX/Ec7;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ec8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145575
    sget-object v0, LX/Ec8;->a:LX/EWZ;

    return-object v0
.end method

.method public final o()LX/Ec7;
    .locals 1

    .prologue
    .line 2145574
    invoke-static {p0}, LX/Ec8;->a(LX/Ec8;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145573
    invoke-virtual {p0}, LX/Ec8;->o()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145572
    invoke-static {}, LX/Ec7;->w()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145571
    invoke-virtual {p0}, LX/Ec8;->o()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145570
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    return-object v0
.end method
