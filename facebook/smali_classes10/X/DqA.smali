.class public LX/DqA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field public a:LX/0SG;

.field public final b:LX/1rx;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0dN;

.field public final e:LX/0iA;

.field public final f:LX/3D1;

.field private final g:LX/DqV;

.field private final h:LX/3Cl;

.field public final i:LX/2jO;

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/Runnable;

.field public m:LX/0i1;

.field public n:Z

.field public o:Z

.field public p:Landroid/view/View;

.field private q:Landroid/view/animation/ScaleAnimation;


# direct methods
.method public constructor <init>(LX/0SG;LX/1rx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/3D1;LX/DqV;LX/3Cl;LX/2jO;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2047894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047895
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DqA;->n:Z

    .line 2047896
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DqA;->o:Z

    .line 2047897
    iput-object p1, p0, LX/DqA;->a:LX/0SG;

    .line 2047898
    iput-object p2, p0, LX/DqA;->b:LX/1rx;

    .line 2047899
    iput-object p3, p0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2047900
    iput-object p4, p0, LX/DqA;->e:LX/0iA;

    .line 2047901
    iput-object p5, p0, LX/DqA;->f:LX/3D1;

    .line 2047902
    iput-object p6, p0, LX/DqA;->g:LX/DqV;

    .line 2047903
    iput-object p7, p0, LX/DqA;->h:LX/3Cl;

    .line 2047904
    iput-object p8, p0, LX/DqA;->i:LX/2jO;

    .line 2047905
    iget-object v0, p0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->K:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/DqA;->j:Z

    .line 2047906
    iget-object v0, p0, LX/DqA;->m:LX/0i1;

    if-nez v0, :cond_0

    .line 2047907
    iget-object v0, p0, LX/DqA;->e:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOTIFICATIONS_ADAPTER_CREATION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/DqW;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    iput-object v0, p0, LX/DqA;->m:LX/0i1;

    .line 2047908
    :cond_0
    iget-object v0, p0, LX/DqA;->m:LX/0i1;

    move-object v0, v0

    .line 2047909
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->J:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/DqA;->k:Z

    .line 2047910
    new-instance v0, LX/Dq6;

    invoke-direct {v0, p0}, LX/Dq6;-><init>(LX/DqA;)V

    iput-object v0, p0, LX/DqA;->d:LX/0dN;

    .line 2047911
    return-void

    .line 2047912
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2047913
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/DqA;
    .locals 12

    .prologue
    .line 2047934
    const-class v1, LX/DqA;

    monitor-enter v1

    .line 2047935
    :try_start_0
    sget-object v0, LX/DqA;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2047936
    sput-object v2, LX/DqA;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2047937
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047938
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2047939
    new-instance v3, LX/DqA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v5

    check-cast v5, LX/1rx;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {v0}, LX/3D1;->a(LX/0QB;)LX/3D1;

    move-result-object v8

    check-cast v8, LX/3D1;

    .line 2047940
    new-instance v10, LX/DqV;

    invoke-static {v0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v9

    check-cast v9, LX/1rx;

    invoke-direct {v10, v9}, LX/DqV;-><init>(LX/1rx;)V

    .line 2047941
    move-object v9, v10

    .line 2047942
    check-cast v9, LX/DqV;

    invoke-static {v0}, LX/3Cl;->a(LX/0QB;)LX/3Cl;

    move-result-object v10

    check-cast v10, LX/3Cl;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v11

    check-cast v11, LX/2jO;

    invoke-direct/range {v3 .. v11}, LX/DqA;-><init>(LX/0SG;LX/1rx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/3D1;LX/DqV;LX/3Cl;LX/2jO;)V

    .line 2047943
    move-object v0, v3

    .line 2047944
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2047945
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DqA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2047946
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2047947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/DqA;Landroid/content/Context;LX/3Cv;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/high16 v2, 0x447a0000    # 1000.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 2047926
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030c33

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DqA;->p:Landroid/view/View;

    .line 2047927
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, LX/DqA;->q:Landroid/view/animation/ScaleAnimation;

    .line 2047928
    iget-object v0, p0, LX/DqA;->q:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 2047929
    iget-object v0, p0, LX/DqA;->q:Landroid/view/animation/ScaleAnimation;

    new-instance v2, LX/Dq8;

    invoke-direct {v2, p0}, LX/Dq8;-><init>(LX/DqA;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2047930
    iget-object v0, p0, LX/DqA;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2047931
    iget-object v0, p0, LX/DqA;->p:Landroid/view/View;

    invoke-virtual {p2, v0}, LX/3Cv;->addView(Landroid/view/View;)V

    .line 2047932
    new-instance v0, LX/Dq9;

    invoke-direct {v0, p0}, LX/Dq9;-><init>(LX/DqA;)V

    invoke-virtual {p2, v0}, LX/3Cv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2047933
    return-void
.end method

.method public static k(LX/DqA;)V
    .locals 2

    .prologue
    .line 2047923
    iget-object v0, p0, LX/DqA;->p:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DqA;->q:Landroid/view/animation/ScaleAnimation;

    if-eqz v0, :cond_0

    .line 2047924
    iget-object v0, p0, LX/DqA;->p:Landroid/view/View;

    iget-object v1, p0, LX/DqA;->q:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2047925
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v7, 0x1

    .line 2047948
    iget-object v0, p0, LX/DqA;->g:LX/DqV;

    .line 2047949
    new-instance v1, LX/23u;

    invoke-direct {v1}, LX/23u;-><init>()V

    const-string v2, "inline_notification_id"

    .line 2047950
    iput-object v2, v1, LX/23u;->N:Ljava/lang/String;

    .line 2047951
    move-object v1, v1

    .line 2047952
    const-string v2, "inline_notification_cache_id"

    .line 2047953
    iput-object v2, v1, LX/23u;->m:Ljava/lang/String;

    .line 2047954
    move-object v1, v1

    .line 2047955
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2047956
    iput-object v2, v1, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2047957
    move-object v1, v1

    .line 2047958
    const/4 v3, 0x0

    .line 2047959
    iget-object v2, v0, LX/DqV;->a:LX/1rx;

    .line 2047960
    iget-object v4, v2, LX/1rx;->a:LX/0ad;

    sget-char v5, LX/15r;->r:C

    iget-object v6, v2, LX/1rx;->b:Landroid/content/res/Resources;

    const v8, 0x7f081179

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 2047961
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 2047962
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const-class v5, Landroid/text/style/StyleSpan;

    invoke-interface {v4, v3, v2, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/StyleSpan;

    .line 2047963
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2047964
    array-length v6, v2

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v8, v2, v3

    .line 2047965
    invoke-virtual {v8}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    .line 2047966
    invoke-interface {v4, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    .line 2047967
    invoke-interface {v4, v8}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    sub-int/2addr v8, v9

    .line 2047968
    new-instance v10, LX/4W6;

    invoke-direct {v10}, LX/4W6;-><init>()V

    .line 2047969
    iput v9, v10, LX/4W6;->d:I

    .line 2047970
    move-object v9, v10

    .line 2047971
    iput v8, v9, LX/4W6;->c:I

    .line 2047972
    move-object v8, v9

    .line 2047973
    invoke-virtual {v8}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2047974
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2047975
    :cond_1
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2047976
    iput-object v3, v2, LX/173;->f:Ljava/lang/String;

    .line 2047977
    move-object v2, v2

    .line 2047978
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2047979
    iput-object v3, v2, LX/173;->e:LX/0Px;

    .line 2047980
    move-object v2, v2

    .line 2047981
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object v2, v2

    .line 2047982
    iput-object v2, v1, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2047983
    move-object v1, v1

    .line 2047984
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2047985
    new-instance v2, LX/BDZ;

    invoke-direct {v2}, LX/BDZ;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 2047986
    iput-object v3, v2, LX/BDZ;->a:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 2047987
    move-object v2, v2

    .line 2047988
    invoke-virtual {v2}, LX/BDZ;->a()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v2

    .line 2047989
    new-instance v3, LX/BD0;

    invoke-direct {v3}, LX/BD0;-><init>()V

    .line 2047990
    iput-object v2, v3, LX/BD0;->b:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 2047991
    move-object v2, v3

    .line 2047992
    iget-object v3, v0, LX/DqV;->a:LX/1rx;

    .line 2047993
    iget-object v4, v3, LX/1rx;->a:LX/0ad;

    sget-char v5, LX/15r;->p:C

    iget-object v6, v3, LX/1rx;->b:Landroid/content/res/Resources;

    const v0, 0x7f08117b

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2047994
    new-instance v4, LX/BD4;

    invoke-direct {v4}, LX/BD4;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 2047995
    iput-object v5, v4, LX/BD4;->b:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 2047996
    move-object v4, v4

    .line 2047997
    invoke-virtual {v4}, LX/BD4;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v4

    .line 2047998
    new-instance v5, LX/BCy;

    invoke-direct {v5}, LX/BCy;-><init>()V

    new-instance v6, LX/4ae;

    invoke-direct {v6}, LX/4ae;-><init>()V

    .line 2047999
    iput-object v3, v6, LX/4ae;->a:Ljava/lang/String;

    .line 2048000
    move-object v3, v6

    .line 2048001
    invoke-virtual {v3}, LX/4ae;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    .line 2048002
    iput-object v3, v5, LX/BCy;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2048003
    move-object v3, v5

    .line 2048004
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->POPUP_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 2048005
    iput-object v5, v3, LX/BCy;->c:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 2048006
    move-object v3, v3

    .line 2048007
    invoke-virtual {v3}, LX/BCy;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v3

    .line 2048008
    new-instance v5, LX/BD3;

    invoke-direct {v5}, LX/BD3;-><init>()V

    .line 2048009
    iput-object v4, v5, LX/BD3;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    .line 2048010
    move-object v4, v5

    .line 2048011
    iput-object v3, v4, LX/BD3;->b:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    .line 2048012
    move-object v3, v4

    .line 2048013
    const-string v4, "server_action_hide_inline_notification_nux"

    .line 2048014
    iput-object v4, v3, LX/BD3;->c:Ljava/lang/String;

    .line 2048015
    move-object v3, v3

    .line 2048016
    invoke-virtual {v3}, LX/BD3;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v3

    .line 2048017
    new-instance v4, LX/BD2;

    invoke-direct {v4}, LX/BD2;-><init>()V

    .line 2048018
    iput-object v3, v4, LX/BD2;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 2048019
    move-object v3, v4

    .line 2048020
    invoke-virtual {v3}, LX/BD2;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object v3

    .line 2048021
    new-instance v4, LX/BD1;

    invoke-direct {v4}, LX/BD1;-><init>()V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2048022
    iput-object v3, v4, LX/BD1;->a:LX/0Px;

    .line 2048023
    move-object v3, v4

    .line 2048024
    invoke-virtual {v3}, LX/BD1;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v3

    move-object v3, v3

    .line 2048025
    iput-object v3, v2, LX/BD0;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 2048026
    move-object v2, v2

    .line 2048027
    invoke-virtual {v2}, LX/BD0;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v2

    .line 2048028
    new-instance v3, LX/BBH;

    invoke-direct {v3}, LX/BBH;-><init>()V

    invoke-virtual {v3, v2}, LX/BBH;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)LX/BBH;

    move-result-object v2

    invoke-virtual {v2}, LX/BBH;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v2

    .line 2048029
    new-instance v3, LX/BBG;

    invoke-direct {v3}, LX/BBG;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/BBG;->a(LX/0Px;)LX/BBG;

    move-result-object v2

    invoke-virtual {v2}, LX/BBG;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v2

    move-object v2, v2

    .line 2048030
    new-instance v3, LX/BBE;

    invoke-direct {v3}, LX/BBE;-><init>()V

    invoke-virtual {v3, v1}, LX/BBE;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)LX/BBE;

    move-result-object v1

    invoke-virtual {v1}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v1

    .line 2048031
    move-object v2, v1

    .line 2048032
    if-nez p2, :cond_3

    iget-object v0, p0, LX/DqA;->h:LX/3Cl;

    invoke-virtual {v0, p3}, LX/3Cl;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3Cv;

    move-object v4, v0

    .line 2048033
    :goto_1
    iget-object v0, v4, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    move-object v0, v0

    .line 2048034
    invoke-interface {v2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iget-object v3, p0, LX/DqA;->b:LX/1rx;

    .line 2048035
    iget-object v5, v3, LX/1rx;->a:LX/0ad;

    sget-char v6, LX/15r;->q:C

    iget-object v8, v3, LX/1rx;->b:Landroid/content/res/Resources;

    const v9, 0x7f08117a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v6, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 2048036
    const v5, 0x7f020dff

    const v6, 0x7f020701

    invoke-virtual {v0, v1, v3, v5, v6}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;II)V

    .line 2048037
    invoke-virtual {v4, v7}, LX/3Cv;->setLongClickable(Z)V

    .line 2048038
    new-instance v0, LX/Dq7;

    move-object v1, p0

    move-object v3, p3

    move v5, p1

    move v6, p4

    invoke-direct/range {v0 .. v6}, LX/Dq7;-><init>(LX/DqA;LX/2nq;Landroid/view/ViewGroup;LX/3Cv;II)V

    invoke-virtual {v4, v0}, LX/3Cv;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2048039
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0, v4}, LX/DqA;->a(LX/DqA;Landroid/content/Context;LX/3Cv;)V

    .line 2048040
    iput-boolean v7, p0, LX/DqA;->n:Z

    .line 2048041
    iget-boolean v0, p0, LX/DqA;->j:Z

    move v0, v0

    .line 2048042
    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/DqA;->o:Z

    if-eqz v0, :cond_2

    .line 2048043
    invoke-virtual {p0}, LX/DqA;->e()V

    .line 2048044
    invoke-virtual {p0}, LX/DqA;->h()V

    .line 2048045
    :cond_2
    return-object v4

    .line 2048046
    :cond_3
    check-cast p2, LX/3Cv;

    move-object v4, p2

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2047922
    iget-boolean v0, p0, LX/DqA;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 2047917
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DqA;->j:Z

    .line 2047918
    iget-object v0, p0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0hM;->K:LX/0Tn;

    iget-object v2, p0, LX/DqA;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2047919
    iget-object v0, p0, LX/DqA;->m:LX/0i1;

    if-eqz v0, :cond_0

    .line 2047920
    iget-object v0, p0, LX/DqA;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, LX/DqA;->m:LX/0i1;

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2047921
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 2047914
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2047915
    new-instance v1, Lcom/facebook/notifications/action/NotificationsInlineNotificationNuxManager$3;

    invoke-direct {v1, p0}, Lcom/facebook/notifications/action/NotificationsInlineNotificationNuxManager$3;-><init>(LX/DqA;)V

    const-wide/16 v2, 0x3e8

    const v4, -0x1a109b89

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2047916
    return-void
.end method
