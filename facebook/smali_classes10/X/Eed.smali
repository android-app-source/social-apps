.class public final LX/Eed;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeO;


# instance fields
.field public final synthetic a:LX/Eef;


# direct methods
.method public constructor <init>(LX/Eef;)V
    .locals 0

    .prologue
    .line 2153010
    iput-object p1, p0, LX/Eed;->a:LX/Eef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/EeX;)LX/EeY;
    .locals 11

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2153011
    iget-object v0, p0, LX/Eed;->a:LX/Eef;

    iget-boolean v0, v0, LX/Eef;->b:Z

    if-nez v0, :cond_0

    .line 2153012
    iget-object v0, p0, LX/Eed;->a:LX/Eef;

    .line 2153013
    iput-boolean v2, v0, LX/Eef;->c:Z

    .line 2153014
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    .line 2153015
    :goto_0
    return-object v0

    .line 2153016
    :cond_0
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/3CW;->a(I)I

    move-result v0

    invoke-static {v4}, LX/3CW;->a(I)I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2153017
    new-instance v0, LX/EeY;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, v8, v9}, LX/EeY;-><init>(LX/EeX;LX/EeO;J)V

    goto :goto_0

    .line 2153018
    :cond_1
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v4}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2153019
    iget-wide v4, p1, LX/EeX;->downloadId:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "In STATE_DOWNLOADING but downloadId is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, LX/EeX;->downloadId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/1wm;->a(ZLjava/lang/String;)V

    .line 2153020
    new-instance v0, Landroid/app/DownloadManager$Query;

    invoke-direct {v0}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 2153021
    new-array v3, v1, [J

    iget-wide v4, p1, LX/EeX;->downloadId:J

    aput-wide v4, v3, v2

    invoke-virtual {v0, v3}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 2153022
    iget-object v3, p0, LX/Eed;->a:LX/Eef;

    iget-object v3, v3, LX/Eef;->a:Landroid/app/DownloadManager;

    invoke-virtual {v3, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 2153023
    if-eqz v0, :cond_3

    :goto_2
    const-string v2, "Download cursor is null!"

    invoke-static {v1, v2}, LX/1wm;->a(ZLjava/lang/String;)V

    .line 2153024
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 2153025
    const-string v2, "Download not available for checking completion"

    invoke-static {v1, v2}, LX/1wm;->a(ZLjava/lang/String;)V

    .line 2153026
    const-string v1, "bytes_so_far"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2153027
    const-string v1, "total_size"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2153028
    const-string v1, "status"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2153029
    const-string v6, "reason"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 2153030
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2153031
    new-instance v0, LX/EeW;

    invoke-direct {v0, p1}, LX/EeW;-><init>(LX/EeX;)V

    invoke-virtual {v0, v2, v3}, LX/EeW;->b(J)LX/EeW;

    move-result-object v0

    .line 2153032
    iput-wide v4, v0, LX/EeW;->h:J

    .line 2153033
    move-object v0, v0

    .line 2153034
    iput v1, v0, LX/EeW;->l:I

    .line 2153035
    move-object v0, v0

    .line 2153036
    iput v6, v0, LX/EeW;->m:I

    .line 2153037
    move-object v0, v0

    .line 2153038
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153039
    new-instance v0, LX/EeY;

    invoke-direct {v0, v1, p0, v8, v9}, LX/EeY;-><init>(LX/EeX;LX/EeO;J)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 2153040
    goto/16 :goto_1

    :cond_3
    move v1, v2

    .line 2153041
    goto :goto_2

    .line 2153042
    :cond_4
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    goto/16 :goto_0
.end method
