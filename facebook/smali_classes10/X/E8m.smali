.class public abstract LX/E8m;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Vf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/0Vf;"
    }
.end annotation


# instance fields
.field public final a:LX/0o8;

.field public final b:LX/0SG;

.field public final c:LX/Cfw;

.field public d:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public f:LX/2jY;

.field private final g:Lcom/facebook/reaction/ReactionUtil;

.field public h:LX/E8s;

.field public i:LX/E8t;

.field private j:LX/E8q;


# direct methods
.method public constructor <init>(LX/0o8;LX/0SG;Lcom/facebook/reaction/ReactionUtil;LX/Cfw;)V
    .locals 1

    .prologue
    .line 2083443
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2083444
    const/4 v0, 0x0

    iput-object v0, p0, LX/E8m;->f:LX/2jY;

    .line 2083445
    iput-object p1, p0, LX/E8m;->a:LX/0o8;

    .line 2083446
    iput-object p2, p0, LX/E8m;->b:LX/0SG;

    .line 2083447
    iput-object p3, p0, LX/E8m;->g:Lcom/facebook/reaction/ReactionUtil;

    .line 2083448
    iput-object p4, p0, LX/E8m;->c:LX/Cfw;

    .line 2083449
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/E8q;
    .locals 1

    .prologue
    .line 2083450
    iget-object v0, p0, LX/E8m;->j:LX/E8q;

    if-nez v0, :cond_0

    .line 2083451
    new-instance v0, LX/E8q;

    invoke-direct {v0, p1}, LX/E8q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/E8m;->j:LX/E8q;

    .line 2083452
    :cond_0
    iget-object v0, p0, LX/E8m;->j:LX/E8q;

    return-object v0
.end method

.method public abstract a(LX/1DZ;)V
.end method

.method public abstract a(LX/9qT;)V
.end method

.method public final a(LX/E8t;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2083453
    iput-object p1, p0, LX/E8m;->i:LX/E8t;

    .line 2083454
    const/4 v0, 0x0

    iput-object v0, p0, LX/E8m;->h:LX/E8s;

    .line 2083455
    iget-object v0, p0, LX/E8m;->i:LX/E8t;

    invoke-virtual {p0, p2}, LX/E8m;->b(Landroid/content/Context;)LX/E8s;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2083456
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1OM;->j_(I)V

    .line 2083457
    return-void
.end method

.method public abstract a(Landroid/content/res/Configuration;)V
.end method

.method public abstract a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
.end method

.method public abstract a(LX/2jY;)Z
.end method

.method public abstract a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public b(Landroid/content/Context;)LX/E8s;
    .locals 2

    .prologue
    .line 2083458
    iget-object v0, p0, LX/E8m;->h:LX/E8s;

    if-nez v0, :cond_0

    .line 2083459
    new-instance v1, LX/E8s;

    iget-object v0, p0, LX/E8m;->i:LX/E8t;

    if-nez v0, :cond_1

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    invoke-direct {v1, v0}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v1, p0, LX/E8m;->h:LX/E8s;

    .line 2083460
    :cond_0
    iget-object v0, p0, LX/E8m;->h:LX/E8s;

    return-object v0

    .line 2083461
    :cond_1
    iget-object v0, p0, LX/E8m;->i:LX/E8t;

    goto :goto_0
.end method

.method public abstract b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
.end method

.method public abstract b(LX/2jY;)V
.end method

.method public abstract d()I
.end method

.method public abstract e()I
.end method

.method public abstract e(I)I
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2083425
    invoke-virtual {p0}, LX/E8m;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2083426
    iget-object v0, p0, LX/E8m;->j:LX/E8q;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2083427
    :goto_0
    return v0

    .line 2083428
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 2083429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 2083430
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v0

    invoke-virtual {p0}, LX/E8m;->d()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 2083434
    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    if-nez v0, :cond_1

    .line 2083435
    :cond_0
    :goto_0
    return-void

    .line 2083436
    :cond_1
    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    .line 2083437
    iget-boolean v1, v0, LX/2jY;->o:Z

    move v0, v1

    .line 2083438
    iget-object v1, p0, LX/E8m;->g:Lcom/facebook/reaction/ReactionUtil;

    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    invoke-virtual {v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/2jY;)Z

    move-result v1

    .line 2083439
    if-nez v1, :cond_0

    iget-object v1, p0, LX/E8m;->f:LX/2jY;

    .line 2083440
    iget-boolean v2, v1, LX/2jY;->o:Z

    move v1, v2

    .line 2083441
    if-eq v0, v1, :cond_0

    .line 2083442
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2083431
    invoke-virtual {p0}, LX/E8m;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E8m;->j:LX/E8q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public ij_()I
    .locals 2

    .prologue
    .line 2083432
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v0

    invoke-virtual {p0}, LX/E8m;->d()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 2083433
    iget-object v0, p0, LX/E8m;->i:LX/E8t;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
