.class public LX/EVo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0wT;


# instance fields
.field public final b:LX/0wW;

.field public final c:Landroid/graphics/Rect;

.field public final d:[I

.field public e:Landroid/view/View;

.field public f:LX/0wd;

.field public g:LX/EVm;

.field public h:LX/EVn;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2128995
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/EVo;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0wW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128990
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/EVo;->d:[I

    .line 2128991
    sget-object v0, LX/EVn;->HIDDEN:LX/EVn;

    iput-object v0, p0, LX/EVo;->h:LX/EVn;

    .line 2128992
    iput-object p1, p0, LX/EVo;->b:LX/0wW;

    .line 2128993
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/EVo;->c:Landroid/graphics/Rect;

    .line 2128994
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/0QB;)LX/EVo;
    .locals 2

    .prologue
    .line 2128986
    new-instance v1, LX/EVo;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-direct {v1, v0}, LX/EVo;-><init>(LX/0wW;)V

    .line 2128987
    move-object v0, v1

    .line 2128988
    return-object v0
.end method

.method public static e(LX/EVo;)V
    .locals 4

    .prologue
    .line 2128983
    invoke-static {p0}, LX/EVo;->i(LX/EVo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2128984
    :goto_0
    return-void

    .line 2128985
    :cond_0
    iget-object v0, p0, LX/EVo;->f:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public static h(LX/EVo;)Z
    .locals 2

    .prologue
    .line 2128982
    sget-object v0, LX/EVn;->REVEALING:LX/EVn;

    iget-object v1, p0, LX/EVo;->h:LX/EVn;

    invoke-virtual {v0, v1}, LX/EVn;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static i(LX/EVo;)Z
    .locals 2

    .prologue
    .line 2128980
    sget-object v0, LX/EVn;->SHOWN:LX/EVn;

    iget-object v1, p0, LX/EVo;->h:LX/EVn;

    invoke-virtual {v0, v1}, LX/EVn;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static k(LX/EVo;)Z
    .locals 2

    .prologue
    .line 2128981
    sget-object v0, LX/EVn;->HIDING:LX/EVn;

    iget-object v1, p0, LX/EVo;->h:LX/EVn;

    invoke-virtual {v0, v1}, LX/EVn;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
