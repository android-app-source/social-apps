.class public final LX/Dsg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dsh;


# direct methods
.method public constructor <init>(LX/Dsh;)V
    .locals 0

    .prologue
    .line 2050569
    iput-object p1, p0, LX/Dsg;->a:LX/Dsh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2050570
    check-cast p1, Lcom/facebook/user/model/User;

    check-cast p2, Lcom/facebook/user/model/User;

    .line 2050571
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2050572
    if-eqz v0, :cond_0

    .line 2050573
    iget-object v0, p2, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2050574
    if-nez v0, :cond_1

    .line 2050575
    :cond_0
    const/4 v0, 0x0

    .line 2050576
    :goto_0
    return v0

    .line 2050577
    :cond_1
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2050578
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    .line 2050579
    iget-object v1, p2, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 2050580
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v1

    .line 2050581
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
