.class public LX/EnJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EnI;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Emx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Sy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final d:LX/195;

.field private final e:Landroid/view/View;

.field public final f:Landroid/view/View;

.field private final g:LX/EnY;

.field public final h:Lcom/facebook/entitycards/intent/EntityCardsFragment;

.field public i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

.field public j:LX/Emm;

.field private k:Z

.field public l:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/195;Landroid/view/View;Landroid/view/View;LX/EnY;Lcom/facebook/entitycards/intent/EntityCardsFragment;)V
    .locals 1
    .param p1    # LX/195;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EnY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2166831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166832
    iput-boolean v0, p0, LX/EnJ;->k:Z

    .line 2166833
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/EnJ;->l:Ljava/lang/Integer;

    .line 2166834
    iput-object p1, p0, LX/EnJ;->d:LX/195;

    .line 2166835
    iput-object p2, p0, LX/EnJ;->e:Landroid/view/View;

    .line 2166836
    iput-object p3, p0, LX/EnJ;->f:Landroid/view/View;

    .line 2166837
    iput-object p4, p0, LX/EnJ;->g:LX/EnY;

    .line 2166838
    iput-object p5, p0, LX/EnJ;->h:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    .line 2166839
    return-void
.end method

.method private a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 2166826
    iput-object p1, p0, LX/EnJ;->l:Ljava/lang/Integer;

    .line 2166827
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166828
    :cond_0
    iget-object v0, p0, LX/EnJ;->c:LX/0Sy;

    iget-object v1, p0, LX/EnJ;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0Sy;->a(Landroid/view/View;)V

    .line 2166829
    :goto_0
    return-void

    .line 2166830
    :cond_1
    iget-object v0, p0, LX/EnJ;->c:LX/0Sy;

    iget-object v1, p0, LX/EnJ;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0Sy;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/EnJ;F)V
    .locals 3

    .prologue
    .line 2166821
    invoke-direct {p0}, LX/EnJ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2166822
    :goto_0
    return-void

    .line 2166823
    :cond_0
    iget-object v0, p0, LX/EnJ;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2166824
    iget-object v0, p0, LX/EnJ;->f:Landroid/view/View;

    neg-float v1, p1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2166825
    :cond_1
    iget-object v0, p0, LX/EnJ;->g:LX/EnY;

    invoke-interface {v0, p1}, LX/EnY;->a(F)V

    goto :goto_0
.end method

.method public static g(LX/EnJ;)V
    .locals 2

    .prologue
    .line 2166809
    iget-boolean v0, p0, LX/EnJ;->k:Z

    if-eqz v0, :cond_0

    .line 2166810
    :goto_0
    return-void

    .line 2166811
    :cond_0
    iget-object v0, p0, LX/EnJ;->d:LX/195;

    .line 2166812
    iget-boolean v1, v0, LX/195;->n:Z

    move v0, v1

    .line 2166813
    if-eqz v0, :cond_1

    .line 2166814
    iget-object v0, p0, LX/EnJ;->d:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2166815
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EnJ;->a$redex0(LX/EnJ;F)V

    .line 2166816
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    const/4 v1, 0x1

    .line 2166817
    iput-boolean v1, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 2166818
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, LX/EnJ;->a(Ljava/lang/Integer;)V

    .line 2166819
    iget-object v0, p0, LX/EnJ;->b:LX/Emx;

    sget-object v1, LX/Emv;->INTRO_ANIMATION_END:LX/Emv;

    invoke-virtual {v0, v1}, LX/Emx;->a(LX/Emv;)V

    .line 2166820
    iget-object v0, p0, LX/EnJ;->h:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-virtual {v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b()V

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 2166773
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;)Landroid/support/v4/view/ViewPager;
    .locals 3

    .prologue
    .line 2166799
    const v0, 0x7f03048b

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2166800
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2166801
    iget-object v0, p0, LX/EnJ;->e:Landroid/view/View;

    const v2, 0x7f0d0d89

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iput-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 2166802
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance v2, LX/EnD;

    invoke-direct {v2, p0}, LX/EnD;-><init>(LX/EnJ;)V

    .line 2166803
    iput-object v2, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 2166804
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance v2, LX/EnE;

    invoke-direct {v2, p0}, LX/EnE;-><init>(LX/EnJ;)V

    .line 2166805
    iput-object v2, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 2166806
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    new-instance v2, LX/EnF;

    invoke-direct {v2, p0}, LX/EnF;-><init>(LX/EnJ;)V

    .line 2166807
    iput-object v2, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 2166808
    const v0, 0x7f0d0d8b

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public final a()Landroid/view/View$OnTouchListener;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2166796
    iget-object v0, p0, LX/EnJ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 2166797
    new-instance v1, Landroid/view/GestureDetector;

    new-instance v2, LX/EnG;

    invoke-direct {v2, p0}, LX/EnG;-><init>(LX/EnJ;)V

    invoke-direct {v1, v0, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 2166798
    new-instance v0, LX/EnH;

    invoke-direct {v0, p0, v1}, LX/EnH;-><init>(LX/EnJ;Landroid/view/GestureDetector;)V

    return-object v0
.end method

.method public final a(ZLX/Emm;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 2166789
    iget-object v0, p0, LX/EnJ;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v2}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EnJ;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166790
    :cond_0
    :goto_0
    return-void

    .line 2166791
    :cond_1
    invoke-direct {p0}, LX/EnJ;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2166792
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, LX/EnJ;->a(Ljava/lang/Integer;)V

    .line 2166793
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EnJ;->a$redex0(LX/EnJ;F)V

    .line 2166794
    iput-object p2, p0, LX/EnJ;->j:LX/Emm;

    .line 2166795
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(LX/31M;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2166777
    iget-object v0, p0, LX/EnJ;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v2}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2166778
    :goto_0
    return-void

    .line 2166779
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, LX/EnJ;->a(Ljava/lang/Integer;)V

    .line 2166780
    iget-object v0, p0, LX/EnJ;->d:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 2166781
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 2166782
    iput-boolean v2, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 2166783
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->setVisibility(I)V

    .line 2166784
    iget-object v0, p0, LX/EnJ;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2166785
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, LX/EnJ;->a$redex0(LX/EnJ;F)V

    .line 2166786
    iget-object v0, p0, LX/EnJ;->b:LX/Emx;

    sget-object v1, LX/Emw;->INTRO_ANIMATION:LX/Emw;

    invoke-virtual {v0, v1}, LX/Emx;->a(LX/Emw;)V

    .line 2166787
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-object v1, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v1

    neg-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->scrollTo(II)V

    .line 2166788
    iget-object v0, p0, LX/EnJ;->i:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2166776
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2166774
    iget-object v0, p0, LX/EnJ;->d:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2166775
    return-void
.end method
