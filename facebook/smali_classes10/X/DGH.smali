.class public final LX/DGH;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 0

    .prologue
    .line 1979555
    iput-object p1, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iput-object p2, p0, LX/DGH;->a:LX/0Px;

    iput-object p3, p0, LX/DGH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput p4, p0, LX/DGH;->c:I

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method

.method private d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
    .locals 4

    .prologue
    .line 1979537
    iget-object v1, p0, LX/DGH;->a:LX/0Px;

    .line 1979538
    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1979539
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1979540
    invoke-static {v2}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1979541
    if-eqz v3, :cond_0

    .line 1979542
    sget-object v2, LX/DGJ;->NETEGO_FORSALE_PHOTO_STORY:LX/DGJ;

    .line 1979543
    :goto_0
    move-object v2, v2

    .line 1979544
    move-object v0, v2

    .line 1979545
    sget-object v1, LX/DGI;->a:[I

    invoke-virtual {v0}, LX/DGJ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1979546
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No case to handle PageType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1979547
    :pswitch_0
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->i:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    .line 1979548
    :goto_1
    return-object v0

    .line 1979549
    :pswitch_1
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->j:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    goto :goto_1

    .line 1979550
    :pswitch_2
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->k:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;

    goto :goto_1

    .line 1979551
    :cond_0
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1979552
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x4ed245b

    if-ne v2, v3, :cond_1

    .line 1979553
    sget-object v2, LX/DGJ;->NETEGO_VIDEO_STORY:LX/DGJ;

    goto :goto_0

    .line 1979554
    :cond_1
    sget-object v2, LX/DGJ;->NETEGO_PHOTO_STORY:LX/DGJ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(I)LX/1RC;
    .locals 1

    .prologue
    .line 1979556
    invoke-direct {p0, p1}, LX/DGH;->d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2eI;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "LX/1Pf;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1979532
    iget-object v0, p0, LX/DGH;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    move v0, v1

    .line 1979533
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1979534
    invoke-direct {p0, v0}, LX/DGH;->d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;

    move-result-object v3

    new-instance v4, LX/DGK;

    iget-object v5, p0, LX/DGH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v6, p0, LX/DGH;->c:I

    invoke-direct {v4, v5, v0, v6, v1}, LX/DGK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1979535
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1979536
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1979523
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979524
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->g:LX/1K9;

    new-instance v1, LX/6Vh;

    invoke-direct {v1, p1}, LX/6Vh;-><init>(I)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1979525
    :cond_0
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->f:LX/2xr;

    iget-object v0, p0, LX/DGH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979526
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1979527
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/2xr;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1979528
    iget-object v0, p0, LX/DGH;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->b:LX/1LV;

    iget-object v0, p0, LX/DGH;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979529
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1979530
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1979531
    return-void
.end method
