.class public LX/EfM;
.super LX/1a1;
.source ""


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2154449
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2154450
    return-void
.end method

.method private static a(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 2154451
    const v0, 0x7f0b1b6a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v1, 0x7f0b1b6b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(LX/EfM;IIZ)I
    .locals 4

    .prologue
    .line 2154452
    if-nez p3, :cond_1

    .line 2154453
    const/4 p2, 0x0

    .line 2154454
    :cond_0
    :goto_0
    return p2

    .line 2154455
    :cond_1
    if-eqz p1, :cond_0

    .line 2154456
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 2154457
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2154458
    invoke-static {v0}, LX/EfM;->a(Landroid/content/res/Resources;)I

    move-result v0

    sub-int/2addr p2, v0

    goto :goto_0

    .line 2154459
    :cond_2
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2154460
    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {v0}, LX/EfM;->a(Landroid/content/res/Resources;)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    sub-int/2addr p2, v0

    goto :goto_0
.end method
